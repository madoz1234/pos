<?php

include 'WebClientPrint.php';
include 'config.php';
include 'print_format.php';

use Neodynamic\SDK\Web\WebClientPrint;
use Neodynamic\SDK\Web\DefaultPrinter;
use Neodynamic\SDK\Web\InstalledPrinter;
use Neodynamic\SDK\Web\ClientPrintJob;

use \Phalcon\Db\Adapter\Pdo\Postgresql;

// Process request
// Generate ClientPrintJob? only if clientPrint param is in the query string
$urlParts = parse_url($_SERVER['REQUEST_URI']);

if (isset($urlParts['query'])) {
    $rawQuery = $urlParts['query'];
    parse_str($rawQuery, $qs);
    if (isset($qs[WebClientPrint::CLIENT_PRINT_JOB])) {
        
        $printerName = urldecode($qs['printerName']);
		$id_jual = urldecode($qs['id_jual']);
		
		$total = 0; $count = 0; $dpp = 0; $ppn = 0; 
		$total_barang = 0; $discount = 0; $cash = 0; $sisa = 0;

		$connection = new Postgresql($config);
		
		$sqlTMUK = " SELECT \"tmuk_id\", \"tmuk\", \"supplier_id\", \"escrow_account\", \"saldo\", \"telp\", \"name\", \"owner\", \"address1\", \"address2\", \"address3\" "
				." FROM \"m_tmuk\" ";
		
		$sqlJual = "SELECT \"id_jual\", \"id_member\", \"id_kasir\", \"tanggal\", \"subtotal\", \"tax\", \"discount\", \"total\", \"note\" "
				." , \"flag_delete\", \"kassa_id\", \"shift_id\", \"flag_sum\", \"id_reference\" "
				." FROM \"t_jual\" "
				." WHERE \"id_jual\"='".$id_jual."' ";

		$sqlJualDetail = "SELECT \"id_jual_item\", \"id_jual\", \"item_code\", \"description\", \"harga\", \"harga_member\", \"qty\", \"satuan\", \"discount\", \"total\" "
				." , \"promo\", \"barcode\", \"disc_percent\" "
				." FROM \"t_jual_detail\" "
				." WHERE \"id_jual\"='".$id_jual."' ";
				
		$sqlJualBayar = "SELECT \"id_bayar_item\", \"id_jual\", \"tipe\", \"bank_id\", \"bank_name\", \"total\", \"no_kartu\" "
				." FROM \"t_jual_bayar\" "
				." WHERE \"id_jual\"='".$id_jual."' ";
		
		$resultsTMUK = $connection->query($sqlTMUK);
		$resultsJual = $connection->query($sqlJual);
		$resultsJualDetail = $connection->query($sqlJualDetail);
		$resultsJualBayar = $connection->query($sqlJualBayar);
		
		$count = 0;
		$list_TMUK = null;
		$resultsTMUK->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($resultTMUK = $resultsTMUK->fetchArray()) {
			$list_TMUK[$count]['tmuk_id'] = $resultTMUK[0];
			$list_TMUK[$count]['tmuk'] = $resultTMUK[1];
			$list_TMUK[$count]['supplier_id'] = $resultTMUK[2];
			$list_TMUK[$count]['escrow_account'] = $resultTMUK[3];
			$list_TMUK[$count]['saldo'] = $resultTMUK[4];		
			$list_TMUK[$count]['telp'] = $resultTMUK[5];	
			$list_TMUK[$count]['name'] = $resultTMUK[6];	
			$list_TMUK[$count]['owner'] = $resultTMUK[7];	
			$list_TMUK[$count]['address1'] = $resultTMUK[8];	
			$list_TMUK[$count]['address2'] = $resultTMUK[9];	
			$list_TMUK[$count]['address3'] = $resultTMUK[10];	
		}
		
		$count = 0;
		$list_Jual = null;
		$resultsJual->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($resultJual = $resultsJual->fetchArray()) {
			$list_Jual[$count]['id_jual'] = $resultJual[0];
			$list_Jual[$count]['id_member'] = $resultJual[1];
			$list_Jual[$count]['id_kasir'] = $resultJual[2];
			$list_Jual[$count]['tanggal'] = $resultJual[3];
			$list_Jual[$count]['subtotal'] = $resultJual[4];			
			$list_Jual[$count]['tax'] = $resultJual[5];	
			$list_Jual[$count]['discount'] = $resultJual[6];	
			$list_Jual[$count]['total'] = $resultJual[7];	
			$list_Jual[$count]['note'] = $resultJual[8];	
			$list_Jual[$count]['flag_delete'] = $resultJual[9];
			$list_Jual[$count]['kassa_id'] = $resultJual[10];
			$list_Jual[$count]['shift_id'] = $resultJual[11];
			$list_Jual[$count]['flag_sum'] = $resultJual[12];
			$list_Jual[$count]['id_reference'] = $resultJual[13];
			
			$count++;
		}
		
		$count = 0;
		$list_JualDetail = null;
		$resultsJualDetail->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($resultJualDetail = $resultsJualDetail->fetchArray()) {
			$list_JualDetail[$count]['id_jual_item'] = $resultJualDetail[0];
			$list_JualDetail[$count]['id_jual'] = $resultJualDetail[1];
			$list_JualDetail[$count]['item_code'] = $resultJualDetail[2];
			$list_JualDetail[$count]['description'] = $resultJualDetail[3];
			$list_JualDetail[$count]['harga'] = $resultJualDetail[4];			
			$list_JualDetail[$count]['harga_member'] = $resultJualDetail[5];	
			$list_JualDetail[$count]['qty'] = $resultJualDetail[6];	
			$list_JualDetail[$count]['satuan'] = $resultJualDetail[7];	
			$list_JualDetail[$count]['discount'] = $resultJualDetail[8];	
			$list_JualDetail[$count]['total'] = $resultJualDetail[9];
			$list_JualDetail[$count]['promo'] = $resultJualDetail[10];
			$list_JualDetail[$count]['barcode'] = $resultJualDetail[11];
			$list_JualDetail[$count]['disc_percent'] = $resultJualDetail[12];
			
			$count++;
		}
		
		$count = 0;
		$list_JualBayar = null;
		$resultsJualBayar->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($resultJualBayar = $resultsJualBayar->fetchArray()) {
			$list_JualBayar[$count]['id_bayar_item'] = $resultJualBayar[0];
			$list_JualBayar[$count]['id_jual'] = $resultJualBayar[1];
			$list_JualBayar[$count]['tipe'] = $resultJualBayar[2];
			$list_JualBayar[$count]['bank_id'] = $resultJualBayar[3];
			$list_JualBayar[$count]['bank_name'] = $resultJualBayar[4];			
			$list_JualBayar[$count]['total'] = $resultJualBayar[5];	
			$list_JualBayar[$count]['no_kartu'] = $resultJualBayar[6];
			
			$count++;
		}
		
		foreach($list_TMUK as $listtm){
			$tmuk_name = $listtm['name'];
			$tmuk_code = $listtm['tmuk'];
			$tmuk_address1 = $listtm['address1']; 
			$tmuk_address2 = $listtm['address2']; 
			$tmuk_address3 = $listtm['address3']; 
			$tmuk_telp = $listtm['telp']; 
		}
		
		foreach($list_Jual as $listj){
			$dpp = $listj['subtotal'];
			$ppn = $listj['tax'];
			$total = $listj['total'];
			$discount = $listj['discount'];
			
			$kasir = $listj['id_kasir'];
			
			if($listj['id_member'] == ''){
				$hrg = "harga";
			}else{
				$hrg = "harga_member";
			}
		}
		
        //Create ESC/POS commands for sample receipt
        $esc = '0x1B'; //ESC byte in hex notation
        $newLine = '0x0A'; //LF byte in hex notation
        
        $cmds = '';
        $cmds = $esc . "@"; //Initializes the printer (ESC @)
        $cmds .= $esc . '!' . '0x00'; //Character font A selected (ESC ! 0)
		
		$cmds .= print_center($tmuk_name); $cmds .= $newLine; $cmds .= $newLine;
		$cmds .= print_center($tmuk_address1); $cmds .= $newLine;
		$cmds .= print_center($tmuk_address2); $cmds .= $newLine;
		$cmds .= print_center($tmuk_address3); $cmds .= $newLine; $cmds .= $newLine;
		$cmds .= print_space(3).print_left(date('d-m-Y H:i:s'), 19).print_space(4).print_right($kasir, 19).print_space(3); $cmds .= $newLine;
		$cmds .= '------------------------------------------------'; $cmds .= $newLine; 
		
		foreach($list_JualDetail as $listjd){
			$total_barang++;
			$cmds .= $listjd['description']; $cmds .= $newLine;
			$cmds .= print_space(10).print_right($listjd['qty'], 5).' x '.print_left(number_format($listjd[$hrg],0,",","."), 8).print_space(12).print_right(number_format($listjd['total'],0,",","."), 10); $cmds .= $newLine;
		}
		
		$cmds .= '------------------------------------------------'; $cmds .= $newLine; // 19 -> 25
		$cmds .= print_left('TOTAL PENJUALAN ['.$total_barang.'/'.$total_barang.']', 25).print_space(9).':'.print_space(3).print_right(number_format(($total - $discount),0,",","."), 10).''; $cmds .= $newLine; 
		
		$cmds .= print_left('DISKON', 10).print_space(24).':'.print_space(3).print_right(number_format($discount,0,",","."), 10).''; $cmds .= $newLine;
		
		foreach($list_JualBayar as $listjb){
			
			if($listjb['tipe'] == "DONASI"){ 
				$cash -= $listjb['total'];  
			}else{
				$cash += $listjb['total'];  
			}
			
			$sisa = $cash - $total;
			
			$cmds .= print_left($listjb['tipe'], 10).print_space(24).':'.print_space(3).print_right(number_format($listjb['total'],0,",","."), 10).''; $cmds .= $newLine;
		}
	
		$cmds .= 'KEMBALIAN'.print_space(25).':'.print_space(3).print_right(number_format($sisa,0,",","."), 10).''; $cmds .= $newLine; 
		$cmds .= '------------------------------------------------'; $cmds .= $newLine;
		$cmds .= print_left('DPP : '.number_format($dpp,0,",","."), 13).print_space(3).
				 print_left('PPN : '.number_format($ppn,0,",","."), 13).print_space(3).
				 print_left('NON-PPN : '.number_format('0',0,",","."), 16); $cmds .= $newLine;
		$cmds .= '------------------------------------------------'; $cmds .= $newLine; $cmds .= $newLine;
		
		$cmds .= print_center('THANK YOU FOR SHOPPING AT '.$tmuk_name); $cmds .= $newLine;
		$cmds .= print_center('imm@lottemart.co.id'); $cmds .= $newLine;
		$cmds .= print_center('HOTLINE IMM TMUK : '.$tmuk_telp); $cmds .= $newLine;
		$cmds .= print_center('Transaction ID : '.$id_jual); $cmds .= $newLine;
		$cmds .= $newLine;
		$cmds .= $newLine;
		$cmds .= $newLine;
		$cmds .= $newLine;
		$cmds .= $esc . "i"; // full cut paper

		//Create a ClientPrintJob obj that will be processed at the client side by the WCPP
		$cpj = new ClientPrintJob();
		//set ESCPOS commands to print...
		$cpj->printerCommands = $cmds;
        $cpj->formatHexValues = true;
		
		$cpj->clientPrinter = new InstalledPrinter($printerName);		

		//Send ClientPrintJob back to the client
		ob_start();
		ob_clean();
		header('Content-type: application/octet-stream');
		echo $cpj->sendToClient();
		ob_end_flush();
		exit();
        
    }
}
