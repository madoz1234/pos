<?php
date_default_timezone_set('Asia/Jakarta');
include 'WebClientPrint.php';
include 'config.php';
include 'print_format.php';

use Neodynamic\SDK\Web\WebClientPrint;
use Neodynamic\SDK\Web\DefaultPrinter;
use Neodynamic\SDK\Web\InstalledPrinter;
use Neodynamic\SDK\Web\ClientPrintJob;

use \Phalcon\Db\Adapter\Pdo\Postgresql;

// Process request
// Generate ClientPrintJob? only if clientPrint param is in the query string
$urlParts = parse_url($_SERVER['REQUEST_URI']);

if (isset($urlParts['query'])) {
    $rawQuery = $urlParts['query'];
    parse_str($rawQuery, $qs);
    if (isset($qs[WebClientPrint::CLIENT_PRINT_JOB])) {
        
        $printerName = urldecode($qs['printerName']);
		$username = urldecode($qs['username']);

		$connection = new Postgresql($config);
		
		$sqlTMUK = " SELECT \"tmuk_id\", \"tmuk\", \"supplier_id\", \"escrow_account\", \"saldo\", \"telp\", \"name\", \"owner\", \"address1\", \"address2\", \"address3\" "
				." FROM \"m_tmuk\" ";
				
		$sqlSED = "SELECT \"id\", \"tanggal\", \"user_id\", \"kassa_id\", \"kassa_desc\", \"shift_id\", \"shift_desc\", \"setoran\", \"flag_start\", \"flag_end\", \"cash_cashier\" "
				." FROM \"t_start_end_shift\" "
				." WHERE  \"tanggal\" = '".date('Y-m-d')."' AND \"flag_end\" = false AND \"user_id\" = '".$username."' "
				." LIMIT 1 ";			
				
		$resultsTMUK = $connection->query($sqlTMUK);
		$resultsSED = $connection->query($sqlSED);		

		$count = 0;
		$list_TMUK = null;
		$resultsTMUK->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($resultTMUK = $resultsTMUK->fetchArray()) {
			$list_TMUK[$count]['tmuk_id'] = $resultTMUK[0];
			$list_TMUK[$count]['tmuk'] = $resultTMUK[1];
			$list_TMUK[$count]['supplier_id'] = $resultTMUK[2];
			$list_TMUK[$count]['escrow_account'] = $resultTMUK[3];
			$list_TMUK[$count]['saldo'] = $resultTMUK[4];		
			$list_TMUK[$count]['telp'] = $resultTMUK[5];	
			$list_TMUK[$count]['name'] = $resultTMUK[6];	
			$list_TMUK[$count]['owner'] = $resultTMUK[7];	
			$list_TMUK[$count]['address1'] = $resultTMUK[8];	
			$list_TMUK[$count]['address2'] = $resultTMUK[9];	
			$list_TMUK[$count]['address3'] = $resultTMUK[10];	
		}
		
		$list_SED = null;
		$resultsSED->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($resultSED = $resultsSED->fetchArray()) {
			$list_SED[$count]['id'] = $resultSED[0];
			$list_SED[$count]['tanggal'] = $resultSED[1];
			$list_SED[$count]['user_id'] = $resultSED[2];
			$list_SED[$count]['kassa_id'] = $resultSED[3];
			$list_SED[$count]['kassa_desc'] = $resultSED[4];		
			$list_SED[$count]['shift_id'] = $resultSED[5];		
			$list_SED[$count]['shift_desc'] = $resultSED[6];		
			$list_SED[$count]['setoran'] = $resultSED[7];		
			$list_SED[$count]['flag_start'] = $resultSED[8];		
			$list_SED[$count]['flag_end'] = $resultSED[9];		
			$list_SED[$count]['cash_cashier'] = $resultSED[10];		
		}
		
		$tmuk_name = '';
		$tmuk_code = '';
		$tmuk_telp = ''; 
		$owner = '';
		if(isset($list_TMUK)){
			foreach($list_TMUK as $listtm){
				$tmuk_name = $listtm['name'];
				$tmuk_code = $listtm['tmuk'];
				$tmuk_telp = $listtm['telp']; 
				$owner = $listtm['owner'];
			}
		}
		
		$shift = '';
		$pos = '';
		$kasir = '';
		$tanggal = '';
		$setoran = 0;
		
		if(isset($list_SED)){
			foreach($list_SED as $listSED){
				$shift = $listSED['shift_desc'];			
				$pos = $listSED['kassa_desc'];			
				$kasir = $listSED['user_id'];			
				$tanggal = $listSED['tanggal'];			
				$setoran = $listSED['setoran'];			
			}
		}
		
		//echo " Setoran Awal: ".$setoran;
		
        //Create ESC/POS commands for sample receipt
        $esc = '0x1B'; //ESC byte in hex notation
        $newLine = '0x0A'; //LF byte in hex notation
        
        $cmds = '';
        $cmds = $esc . "@"; //Initializes the printer (ESC @)
        $cmds .= $esc . '!' . '0x00'; //Character font A selected (ESC ! 0)
		
		$cmds .= print_center('START OF SHIFT'); 
		$cmds .= $newLine;
		$cmds .= $newLine;
		
		$cmds .= 'Shift       : '.$shift; $cmds .= $newLine;
		$cmds .= 'POS         : '.$pos; $cmds .= $newLine;
		$cmds .= 'Kasir       : '.$kasir; $cmds .= $newLine;
		$cmds .= 'Store Code  : '.$tmuk_code; $cmds .= $newLine;
		$cmds .= 'Store Name  : '.$tmuk_name; $cmds .= $newLine;
		$cmds .= 'Date        : '.date('d-m-Y', strtotime($tanggal)); $cmds .= $newLine;
		$cmds .= $newLine;
		
		$cmds .= 'SALES'; $cmds .= $newLine;
		$cmds .= '------------------------------------------------'; $cmds .= $newLine; //48
		$cmds .= print_right('Setoran Awal', 30).print_right(number_format($setoran,0,",","."), 18); $cmds .= $newLine;
		$cmds .= $newLine;
		$cmds .= '================================================'; 
		$cmds .= print_center('Mengetahui Store Leader'); $cmds .= $newLine; $cmds .= $newLine; $cmds .= $newLine;
		$cmds .= print_center('(     '.$owner.'     )'); $cmds .= $newLine; 
		$cmds .= $newLine; 
		$cmds .= $newLine;
		$cmds .= '------------------------------------------------'; $cmds .= $newLine;
		$cmds .= print_center('Print Date : '.date('d-m-Y H:i:s')); $cmds .= $newLine;
		$cmds .= $newLine;
		$cmds .= $newLine;
		$cmds .= $newLine;
		$cmds .= $newLine;
		$cmds .= $esc . "i"; // full cut paper
		
		//Create a ClientPrintJob obj that will be processed at the client side by the WCPP
		$cpj = new ClientPrintJob();
		//set ESCPOS commands to print...
		$cpj->printerCommands = $cmds;
        $cpj->formatHexValues = true;
		
		$cpj->clientPrinter = new InstalledPrinter($printerName);		

		//Send ClientPrintJob back to the client
		ob_start();
		ob_clean();
		header('Content-type: application/octet-stream');
		echo $cpj->sendToClient();
		ob_end_flush();
		exit();
        
    }
}
