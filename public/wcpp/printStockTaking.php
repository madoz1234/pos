<?php

include 'WebClientPrint.php';
include 'config.php';
include 'print_format.php';

use Neodynamic\SDK\Web\WebClientPrint;
use Neodynamic\SDK\Web\DefaultPrinter;
use Neodynamic\SDK\Web\InstalledPrinter;
use Neodynamic\SDK\Web\ClientPrintJob;

use \Phalcon\Db\Adapter\Pdo\Postgresql;

// Process request
// Generate ClientPrintJob? only if clientPrint param is in the query string
$urlParts = parse_url($_SERVER['REQUEST_URI']);

if (isset($urlParts['query'])) {
    $rawQuery = $urlParts['query'];
    parse_str($rawQuery, $qs);
    if (isset($qs[WebClientPrint::CLIENT_PRINT_JOB])) {
        
        $printerName = urldecode($qs['printerName']);
		$so_id = urldecode($qs['so_id']);
		
		$connection = new Postgresql($config);
		
		//get data TMUK
		$sqlTMUK = " SELECT \"tmuk_id\", \"tmuk\", \"supplier_id\", \"escrow_account\", \"saldo\", \"telp\", \"name\", \"owner\", \"address1\", \"address2\", \"address3\" "
				." FROM \"m_tmuk\" ";
		
		$resultsTMUK = $connection->query($sqlTMUK);
		
		$count = 0;
		$list_TMUK = null;
		$resultsTMUK->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($resultTMUK = $resultsTMUK->fetchArray()) {
			$list_TMUK[$count]['tmuk_id'] = $resultTMUK[0];
			$list_TMUK[$count]['tmuk'] = $resultTMUK[1];
			$list_TMUK[$count]['supplier_id'] = $resultTMUK[2];
			$list_TMUK[$count]['escrow_account'] = $resultTMUK[3];
			$list_TMUK[$count]['saldo'] = $resultTMUK[4];		
			$list_TMUK[$count]['telp'] = $resultTMUK[5];	
			$list_TMUK[$count]['name'] = $resultTMUK[6];	
			$list_TMUK[$count]['owner'] = $resultTMUK[7];	
			$list_TMUK[$count]['address1'] = $resultTMUK[8];	
			$list_TMUK[$count]['address2'] = $resultTMUK[9];	
			$list_TMUK[$count]['address3'] = $resultTMUK[10];	
		}
		
		$tmuk_name = '';
		$tmuk_code = '';
		$tmuk_telp = ''; 
		$owner = '';
		if(isset($list_TMUK)){ 
			foreach($list_TMUK as $listtm){
				$tmuk_name = $listtm['name'];
				$tmuk_code = $listtm['tmuk'];
				$tmuk_telp = $listtm['telp']; 
				$owner = $listtm['owner'];
			}
		}
		
		//get data SO
		$sqlSO = " SELECT \"so_id\", \"kode_verifikasi\", \"user_id\", \"so_date\", \"status\", \"so_type\" "
				." FROM \"t_so\" WHERE \"so_id\" = '".$so_id."' ";
		
		$sqlSODetail = "SELECT \"so_detail_item\", \"so_id\", \"item_code\", \"description\", \"qty_tersedia\", \"qty_barcode\", \"unit_item\", \"unit_price\" "
					." , \"total_price\", \"flag_so_ok\", \"barcode_code\" "
					." FROM \"t_so_detail\" WHERE \"so_id\" = '".$so_id."' ";
		
		$resultsSO = $connection->query($sqlSO);
		$resultsSODetail = $connection->query($sqlSODetail);
		
		$count = 0;
		$lists_SO = null;
		$resultsSO->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($resultSO = $resultsSO->fetchArray()) {
			$lists_SO[$count]['so_id'] = $resultSO[0];
			$lists_SO[$count]['kode_verifikasi'] = $resultSO[1];
			$lists_SO[$count]['user_id'] = $resultSO[2];
			$lists_SO[$count]['so_date'] = $resultSO[3];
			$lists_SO[$count]['status'] = $resultSO[4];		
			$lists_SO[$count]['so_type'] = $resultSO[5];				
		}
		
		$list_SO = null;
		if(isset($lists_SO)){ 
			foreach($lists_SO as $line_so){
				$list_SO = $line_so;				
			}
		}
		
		$count = 0;
		$lists_SODetail = null;
		$resultsSODetail->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($resultSODetail = $resultsSODetail->fetchArray()) {
			$lists_SODetail[$count]['so_detail_item'] = $resultSODetail[0];
			$lists_SODetail[$count]['so_id'] = $resultSODetail[1];
			$lists_SODetail[$count]['item_code'] = $resultSODetail[2];
			$lists_SODetail[$count]['description'] = $resultSODetail[3];
			$lists_SODetail[$count]['qty_tersedia'] = $resultSODetail[4];		
			$lists_SODetail[$count]['qty_barcode'] = $resultSODetail[5];				
			$lists_SODetail[$count]['unit_item'] = $resultSODetail[6];				
			$lists_SODetail[$count]['unit_price'] = $resultSODetail[7];				
			$lists_SODetail[$count]['total_price'] = $resultSODetail[8];				
			$lists_SODetail[$count]['flag_so_ok'] = $resultSODetail[9];				
			$lists_SODetail[$count]['barcode_code'] = $resultSODetail[10];				
			
			$count++;
		}
		
		$n_qty_tersedia = 0;
		$n_qty_barcode = 0;
		$n_qty_selisih = 0;
		$n_nilai_tersedia = 0;
		$n_nilai_barcode = 0;
		$n_nilai_selisih = 0;
		if(isset($lists_SODetail)){ 
			foreach($lists_SODetail as $line_so_detail){
				$n_qty_tersedia = $n_qty_tersedia + $line_so_detail['qty_tersedia'];
				$n_qty_barcode = $n_qty_barcode + $line_so_detail['qty_barcode'];
				$n_nilai_tersedia = $n_nilai_tersedia + ($line_so_detail['qty_tersedia'] * $line_so_detail['unit_price']);
				$n_nilai_barcode = $n_nilai_barcode + ($line_so_detail['qty_barcode'] * $line_so_detail['unit_price']);
			}
		}
		
		$n_qty_selisih = $n_qty_tersedia - $n_qty_barcode;
		$n_nilai_selisih = $n_nilai_tersedia - $n_nilai_barcode;
		
		$n_nilai_tersedia = number_format($n_nilai_tersedia,0,",",".");
		$n_nilai_barcode = number_format($n_nilai_barcode,0,",",".");
		$n_nilai_selisih = number_format($n_nilai_selisih,0,",",".");
		
		//struk paper line 48
        //Create ESC/POS commands for sample receipt
        $esc = '0x1B'; //ESC byte in hex notation
        $newLine = '0x0A'; //LF byte in hex notation
        
        $cmds = '';
        $cmds = $esc . "@"; //Initializes the printer (ESC @)
        $cmds .= $esc . '!' . '0x00'; //Character font A selected (ESC ! 0)
		
		$cmds .= print_center('STOCK TAKING'); $cmds .= $newLine;
		$cmds .= $newLine;
		$cmds .= 'Store Code  : '.$tmuk_code; $cmds .= $newLine;
		$cmds .= 'Store Name  : '.$tmuk_name; $cmds .= $newLine;
		$cmds .= 'Date        : '.$list_SO['so_date']; $cmds .= $newLine;
		$cmds .= $newLine;
		
		$cmds .= 'Stock Taking : '.$list_SO['so_id']; 
		$cmds .= $newLine;
		$cmds .= '------------------------------------------------'; 
		$cmds .= $newLine; 
		$cmds .= print_space(12).print_right('Jumlah', 18).print_right('Nilai',18);
		$cmds .= $newLine;
		$cmds .= print_left('Sistem :',12).print_right($n_qty_tersedia, 18).print_right($n_nilai_tersedia,18);
		$cmds .= $newLine;
		$cmds .= print_left('Fisik :',12).print_right($n_qty_barcode, 18).print_right($n_nilai_barcode,18);
		$cmds .= $newLine;
		$cmds .= print_left('Selisih :',12).print_right($n_qty_selisih, 18).print_right($n_nilai_selisih,18);
		$cmds .= $newLine;		
		$cmds .= $newLine;
		$cmds .= '================================================'; 
		$cmds .= print_center('Mengetahui Store Leader'); $cmds .= $newLine; $cmds .= $newLine; $cmds .= $newLine;
		$cmds .= print_center('(     '.$owner.'     )'); $cmds .= $newLine; 
		$cmds .= $newLine; 
		$cmds .= $newLine;
		$cmds .= '------------------------------------------------'; $cmds .= $newLine;
		$cmds .= print_center('Print Date : '.date('d-m-Y H:i:s')); $cmds .= $newLine;
		$cmds .= $newLine;
		$cmds .= $newLine;
		$cmds .= $newLine;
		$cmds .= $newLine;
		$cmds .= $esc . "i"; // full cut paper

		//Create a ClientPrintJob obj that will be processed at the client side by the WCPP
		$cpj = new ClientPrintJob();
		//set ESCPOS commands to print...
		$cpj->printerCommands = $cmds;
        $cpj->formatHexValues = true;
		
		$cpj->clientPrinter = new InstalledPrinter($printerName);		

		//Send ClientPrintJob back to the client
		ob_start();
		ob_clean();
		header('Content-type: application/octet-stream');
		echo $cpj->sendToClient();
		ob_end_flush();
		exit();
        
    }
}
