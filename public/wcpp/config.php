<?php
	date_default_timezone_set('Asia/Jakarta');
	
	$config = [
			"host"     => "localhost",
            "username" => "postgres",
            "password" => "password",
            "dbname"   => "lotte-store",
			"port"	   => "5432"
		];
		
?>