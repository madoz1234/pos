<?php

include 'WebClientPrint.php';
include 'config.php';
include 'print_format.php';

use Neodynamic\SDK\Web\WebClientPrint;
use Neodynamic\SDK\Web\DefaultPrinter;
use Neodynamic\SDK\Web\InstalledPrinter;
use Neodynamic\SDK\Web\ClientPrintJob;

use \Phalcon\Db\Adapter\Pdo\Postgresql;

// Process request
// Generate ClientPrintJob? only if clientPrint param is in the query string
$urlParts = parse_url($_SERVER['REQUEST_URI']);

if (isset($urlParts['query'])) {
    $rawQuery = $urlParts['query'];
    parse_str($rawQuery, $qs);
    if (isset($qs[WebClientPrint::CLIENT_PRINT_JOB])) {
        
        $printerName = urldecode($qs['printerName']);
		
		$total = 0; $count = 0; $dpp = 0; $ppn = 0; $total_barang = 0; $net_sales = 0; $total_act = 0;
		$tunai = 0; $donasi = 0; $debet = 0; $credit = 0; $voucher = 0; $cash_income = 0; $kembalian = 0; $rec_count = 0;

		$connection = new Postgresql($config);
		
		$conditionSED = " ORDER BY id DESC LIMIT 1 ";		
		$sqlSED = "SELECT \"id\", \"tanggal\", \"user_id\", \"flag_start\", \"flag_end\" "
				." FROM \"t_start_end_day\" ".$conditionSED;
		$resultsSED = $connection->query($sqlSED);
		
		$tanggal = '';
		$count = 0;
		$list_SED = null;
		$resultsSED->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($resultSED = $resultsSED->fetchArray()) {
			$list_SED[$count]['id'] = $resultSED[0];
			$list_SED[$count]['tanggal'] = $resultSED[1];
			$list_SED[$count]['user_id'] = $resultSED[2];					
			$list_SED[$count]['flag_start'] = $resultSED[3];	
			$list_SED[$count]['flag_end'] = $resultSED[4];				
			$tanggal = $resultSED[1];
			
			$count++;
		}
		
		$conditionSES = " WHERE \"tanggal\" = '".$tanggal."' AND flag_end_day = false ";		
		$sqlSES = "SELECT \"id\", \"tanggal\", \"user_id\", \"kassa_id\", \"kassa_desc\", \"shift_id\", \"shift_desc\", \"setoran\", \"flag_start\", 
				\"flag_end\", \"cash_cashier\" "
				." FROM \"t_start_end_shift\" ".$conditionSES;
		$resultsSES = $connection->query($sqlSES);
				
		$count = 0;
		$list_SES = null;
		$resultsSES->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($resultSES = $resultsSES->fetchArray()) {
			$list_SES[$count]['id'] = $resultSES[0];
			$list_SES[$count]['tanggal'] = $resultSES[1];
			$list_SES[$count]['user_id'] = $resultSES[2];
			$list_SES[$count]['kassa_id'] = $resultSES[3];
			$list_SES[$count]['kassa_desc'] = $resultSES[4];		
			$list_SES[$count]['shift_id'] = $resultSES[5];	
			$list_SES[$count]['shift_desc'] = $resultSES[6];	
			$list_SES[$count]['setoran'] = $resultSES[7];	
			$list_SES[$count]['flag_start'] = $resultSES[8];	
			$list_SES[$count]['flag_end'] = $resultSES[9];	
			$list_SES[$count]['cash_cashier'] = $resultSES[10];				
			
			$count++;
		}
		
		if($tanggal == '') { return false; }
		
		$conditionJ = " WHERE tanggal = '".$tanggal."' ";
		$condition = " WHERE tanggal = '".$tanggal."' AND flag_sync = false ";
		
		$sqlTMUK = " SELECT \"tmuk_id\", \"tmuk\", \"supplier_id\", \"escrow_account\", \"saldo\", \"telp\", \"name\", \"owner\", \"address1\", \"address2\", \"address3\" "
				." FROM \"m_tmuk\" ";				
		
		$sqlJ = " SELECT count(*) as receipt_count "
			." FROM \"t_jual\" ".$conditionJ;
		
		$sqlJual = " SELECT \"id\", \"id_kasir\", \"tanggal\", \"kassa_id\", \"shift_id\", \"subtotal\", \"tax\", \"discount\", \"total\", \"flag_sync\", \"kembalian\" "
			." FROM \"t_jual_sum\" ".$condition;
			
		$sqlJualBayar = " SELECT \"id\", \"id_kasir\", \"kassa_id\", \"shift_id\", \"tanggal\", \"tipe\", \"total\", \"flag_sync\" "
			." FROM \"t_jual_bayar_sum\" ".$condition;
		
		$resultsTMUK = $connection->query($sqlTMUK);		
		$resultsJ = $connection->query($sqlJ);
		$resultsJual = $connection->query($sqlJual);
		$resultsJualBayar = $connection->query($sqlJualBayar);
		
		$count = 0;
		$list_TMUK = null;
		$resultsTMUK->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($resultTMUK = $resultsTMUK->fetchArray()) {
			$list_TMUK[$count]['tmuk_id'] = $resultTMUK[0];
			$list_TMUK[$count]['tmuk'] = $resultTMUK[1];
			$list_TMUK[$count]['supplier_id'] = $resultTMUK[2];
			$list_TMUK[$count]['escrow_account'] = $resultTMUK[3];
			$list_TMUK[$count]['saldo'] = $resultTMUK[4];		
			$list_TMUK[$count]['telp'] = $resultTMUK[5];	
			$list_TMUK[$count]['name'] = $resultTMUK[6];	
			$list_TMUK[$count]['owner'] = $resultTMUK[7];	
			$list_TMUK[$count]['address1'] = $resultTMUK[8];	
			$list_TMUK[$count]['address2'] = $resultTMUK[9];	
			$list_TMUK[$count]['address3'] = $resultTMUK[10];	
		}
		
		$count = 0;
		$list_J = null;
		$resultsJ->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($resultJ = $resultsJ->fetchArray()) {
			$list_J[$count]['receipt_count'] = $resultJ[0];
		}
		
		$count = 0;
		$list_Jual = null;
		$resultsJual->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($resultJual = $resultsJual->fetchArray()) {
			$list_Jual[$count]['id'] = $resultJual[0];
			$list_Jual[$count]['id_kasir'] = $resultJual[1];
			$list_Jual[$count]['tanggal'] = $resultJual[2];
			$list_Jual[$count]['kassa_id'] = $resultJual[3];
			$list_Jual[$count]['shift_id'] = $resultJual[4];		
			$list_Jual[$count]['subtotal'] = $resultJual[5];	
			$list_Jual[$count]['tax'] = $resultJual[6];	
			$list_Jual[$count]['discount'] = $resultJual[7];	
			$list_Jual[$count]['total'] = $resultJual[8];	
			$list_Jual[$count]['flag_sync'] = $resultJual[9];
			$list_Jual[$count]['kembalian'] = $resultJual[10];

			$count++;
		}
		
		$count = 0;
		$list_JualBayar = null;
		$resultsJualBayar->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($resultJualBayar = $resultsJualBayar->fetchArray()) {
			$list_JualBayar[$count]['id'] = $resultJualBayar[0];
			$list_JualBayar[$count]['id_kasir'] = $resultJualBayar[1];
			$list_JualBayar[$count]['kassa_id'] = $resultJualBayar[2];
			$list_JualBayar[$count]['shift_id'] = $resultJualBayar[3];
			$list_JualBayar[$count]['tanggal'] = $resultJualBayar[4];		
			$list_JualBayar[$count]['tipe'] = $resultJualBayar[5];	
			$list_JualBayar[$count]['total'] = $resultJualBayar[6];	
			$list_JualBayar[$count]['flag_sync'] = $resultJualBayar[7];	
			
			$count++;
		}
		
		foreach($list_TMUK as $listtm){
			$tmuk_name = $listtm['name'];
			$tmuk_code = $listtm['tmuk'];
			$tmuk_address1 = $listtm['address1']; 
			$tmuk_address2 = $listtm['address2']; 
			$tmuk_address3 = $listtm['address3']; 
			$tmuk_telp = $listtm['telp']; 
			$owner = $listtm['owner']; 
		}
		
		$setoran = 0;
		$cash = 0; 
		
		if(count($list_SES) > 0){
			foreach($list_SES as $listses){				
				$setoran += $listses['setoran'];
				$cash += $listses['cash_cashier'];
			}
		}
		
		if(count($list_J) > 0){
			foreach($list_J as $listjl){
				$rec_count = $listjl['receipt_count'];
			}
		}
		
		if(count($list_Jual) > 0){
			foreach($list_Jual as $listj){
				$ppn += $listj['tax'];
				$cash_income += $listj['total'] + $ppn;
				$net_sales += $listj['total'];
				$kembalian += $listj['kembalian'];
			}
		}
		
		if(count($list_JualBayar) > 0){		
			foreach($list_JualBayar as $listjb){
				if($listjb['tipe'] == 'TUNAI'){ $tunai += $listjb['total']; } 
				if($listjb['tipe'] == 'DONASI'){ $donasi += $listjb['total']; } 
				if($listjb['tipe'] == 'KREDIT'){ $credit += $listjb['total']; } 
				if($listjb['tipe'] == 'DEBIT'){ $debet += $listjb['total']; } 
				if($listjb['tipe'] == 'VOUCHER'){ $voucher += $listjb['total']; } 
				
				$total_act += $listjb['total'];
			}
		}	
		
		/*echo 'Initial : '.$setoran.'<br/>';
		echo 'Cash Income : '.$cash.'<br/>';
		echo 'Net Sales : '.$net_sales.'<br/>';
		echo 'Tunai : '.$tunai.'<br/>';
		echo 'Donasi : '.$donasi.'<br/>';
		echo 'Kredit : '.$credit.'<br/>';
		echo 'Debet : '.$debet.'<br/>';
		echo 'Voucher : '.$voucher.'<br/>';
		echo 'Total Actual : '.($tunai + $credit + $debet + $voucher).'<br/>';
		echo 'Variance : '.($cash - $net_sales).'<br/>';
		echo 'Receipt Count : '.$rec_count.'<br/>';*/
		
		//Create ESC/POS commands for sample receipt
        $esc = '0x1B'; //ESC byte in hex notation
        $newLine = '0x0A'; //LF byte in hex notation
        
        $cmds = '';
        $cmds = $esc . "@"; //Initializes the printer (ESC @)
        //$cmds .= $esc . '!' . '0x38'; //Emphasized + Double-height + Double-width mode selected (ESC ! (8 + 16 + 32)) 56 dec => 38 hex
        $cmds .= $esc . '!' . '0x00'; //Character font A selected (ESC ! 0)
		$cmds .= $newLine;
		
		$cmds .= print_center('END OF DAY'); $cmds .= $newLine; $cmds .= $newLine;
		$cmds .= 'Store Code  : '.$tmuk_code; $cmds .= $newLine;
		$cmds .= 'Store Name  : '.$tmuk_name; $cmds .= $newLine;
		$cmds .= 'Date        : '.date('d-m-Y'); $cmds .= $newLine; $cmds .= $newLine;
		
		$cmds .= 'SALES'; $cmds .= $newLine;
		$cmds .= '------------------------------------------------'; $cmds .= $newLine; //48
		$cmds .= print_right('Initial', 32).print_right(number_format($setoran,0,",","."), 16); $cmds .= $newLine; 
		$cmds .= print_right('Cash Income', 32).print_right(number_format($cash,0,",","."), 16); $cmds .= $newLine;      // cash income
		$cmds .= print_right('Net Sales', 32).print_right(number_format($net_sales,0,",","."), 16); $cmds .= $newLine;  // net sales
		$cmds .= print_right('Net Return', 32).print_right(number_format('0',0,",","."), 16); $cmds .= $newLine;
		$cmds .= print_right('PPN', 32).print_right(number_format('0',0,",","."), 16); $cmds .= $newLine;
		$cmds .= '------------------------------------------------'; $cmds .= $newLine;
		$cmds .= print_right('Total Sales', 32).print_right(number_format($net_sales,0,",","."), 16); $cmds .= $newLine;
		$cmds .= '================================================'; $cmds .= $newLine; $cmds .= $newLine;
		
		$cmds .= 'ACTUAL'; $cmds .= $newLine;
		$cmds .= '------------------------------------------------'; $cmds .= $newLine; 
		$cmds .= print_right('Cash', 32).print_right(number_format($net_sales,0,",","."), 16); $cmds .= $newLine; 					// net sales
		$cmds .= print_right('Cash Income', 32).print_right(number_format($cash,0,",","."), 16); $cmds .= $newLine; // $cash per kasir ($tunai - $kembalian)
		$cmds .= print_right('Donasi', 32).print_right(number_format($donasi,0,",","."), 16); $cmds .= $newLine;
		$cmds .= print_right('Kredit', 32).print_right(number_format($credit,0,",","."), 16); $cmds .= $newLine;
		$cmds .= print_right('Debit', 32).print_right(number_format($debet,0,",","."), 16); $cmds .= $newLine;
		$cmds .= print_right('Voucher', 32).print_right(number_format($voucher,0,",","."), 16); $cmds .= $newLine;
		$cmds .= '------------------------------------------------'; $cmds .= $newLine;
		$cmds .= print_right('Total Actual', 32).print_right(number_format((($tunai - $kembalian) + $credit + $debet + $voucher),0,",","."), 16); $cmds .= $newLine; 
		$cmds .= print_right('Variance', 32).print_right(number_format(($cash - $net_sales),0,",","."), 16); $cmds .= $newLine;  	
		$cmds .= print_right('Variance Paid', 32).print_right(number_format('0',0,",","."), 16); $cmds .= $newLine; 
		$cmds .= print_right('Receipt Count', 32).print_right(number_format($rec_count,0,",","."), 16); $cmds .= $newLine;
		$cmds .= '================================================'; $cmds .= $newLine; $cmds .= $newLine;
		
		$cmds .= print_center('Mengetahui Store Leader'); $cmds .= $newLine; $cmds .= $newLine; $cmds .= $newLine;
		$cmds .= print_center('(     '.$owner.'     )'); $cmds .= $newLine; $cmds .= $newLine;
		$cmds .= '------------------------------------------------'; $cmds .= $newLine;
		$cmds .= print_center('Print Date : '.date('d-m-Y H:i:s')); $cmds .= $newLine;
		$cmds .= $newLine;
		$cmds .= $newLine;
		$cmds .= $newLine;
		$cmds .= $newLine;
		$cmds .= $esc . "i"; // full cut paper
		
		$sqlUpdate_endDay = " UPDATE t_start_end_shift SET flag_end_day = 't' WHERE tanggal = '".$tanggal."' ";
		$sqlUpdate_jual = " UPDATE t_jual_sum SET flag_sync = 't' WHERE tanggal = '".$tanggal."' ";
		$sqlUpdate_jualdetail = " UPDATE t_jual_sum SET flag_sync = 't' WHERE tanggal = '".$tanggal."' ";
		$sqlUpdate_jualbayar = " UPDATE t_jual_bayar_sum SET flag_sync = 't' WHERE tanggal = '".$tanggal."' ";
		
		$connection->query($sqlUpdate_endDay);		
		$connection->query($sqlUpdate_jual);		
		$connection->query($sqlUpdate_jualdetail);		
		$connection->query($sqlUpdate_jualbayar);

		//Create a ClientPrintJob obj that will be processed at the client side by the WCPP
		$cpj = new ClientPrintJob();
		//set ESCPOS commands to print...
		$cpj->printerCommands = $cmds;
        $cpj->formatHexValues = true;
		
		$cpj->clientPrinter = new InstalledPrinter($printerName);		

		//Send ClientPrintJob back to the client
		ob_start();
		ob_clean();
		header('Content-type: application/octet-stream');
		echo $cpj->sendToClient();
		ob_end_flush();
		exit();
        
    }
}
