<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class PembayaranTransaksi extends Model
{

	public $id;
	public $inv_no;
	public $supp_id;
	public $tanggal;	
	public $flag;	
	public $status;	
	public $tipe;	
	public $payment_no;	
	public $memo;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"inv_no\", \"supp_id\", \"tanggal\", \"flag\", \"status\", \"payment_no\", \"memo\", \"tipe\", \"flag_ho\", \"bayar\" "
				." FROM t_inv ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['inv_no'] = $result[1];
			$lists[$count]['supp_id'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];	
			$lists[$count]['flag'] = $result[4];	
			$lists[$count]['status'] = $result[5];	
			$lists[$count]['payment_no'] = $result[6];	
			$lists[$count]['memo'] = $result[7];	
			$lists[$count]['tipe'] = $result[8];	
			$lists[$count]['flag_ho'] = $result[9];	
			$lists[$count]['bayar'] = $result[10];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"inv_no\", \"supp_id\", \"tanggal\", \"flag\", \"status\", \"payment_no\", \"memo\", \"tipe\", \"flag_ho\", \"bayar\" "
				." FROM t_inv "
				." WHERE payment_no = '".$object->payment_no."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['inv_no'] = $result[1];
			$lists[$count]['supp_id'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];	
			$lists[$count]['flag'] = $result[4];	
			$lists[$count]['status'] = $result[5];	
			$lists[$count]['payment_no'] = $result[6];	
			$lists[$count]['memo'] = $result[7];	
			$lists[$count]['tipe'] = $result[8];	
			$lists[$count]['flag_ho'] = $result[9];	
			$lists[$count]['bayar'] = $result[10];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"inv_no\", \"supp_id\", \"tanggal\", \"flag\", \"status\", \"payment_no\", \"memo\", \"tipe\", \"flag_ho\", \"bayar\" "
				." FROM t_inv ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['inv_no'] = $result[1];
			$lists[$count]['supp_id'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];	
			$lists[$count]['flag'] = $result[4];	
			$lists[$count]['status'] = $result[5];	
			$lists[$count]['payment_no'] = $result[6];	
			$lists[$count]['memo'] = $result[7];	
			$lists[$count]['tipe'] = $result[8];	
			$lists[$count]['flag_ho'] = $result[9];	
			$lists[$count]['bayar'] = $result[10];	
			
			$count++;
		}
		
		return $lists;
	}

	public function getFreeSQL2($conditionPYR1){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"inv_no\", \"supp_id\", \"tanggal\", \"flag\", \"status\", \"payment_no\", \"memo\", \"tipe\", \"flag_ho\", \"bayar\" "
				." FROM t_inv ".$conditionPYR1;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['inv_no'] = $result[1];
			$lists[$count]['supp_id'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];	
			$lists[$count]['flag'] = $result[4];	
			$lists[$count]['status'] = $result[5];	
			$lists[$count]['payment_no'] = $result[6];	
			$lists[$count]['memo'] = $result[7];	
			$lists[$count]['tipe'] = $result[8];	
			$lists[$count]['flag_ho'] = $result[9];	
			$lists[$count]['bayar'] = $result[10];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"inv_no\") "
				." FROM \"t_inv\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}

	public function getCountPYR($cond2){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"inv_no\") "
				." FROM \"t_inv\" ".$cond2;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "INSERT INTO \"t_inv\" (\"inv_no\", \"supp_id\", \"tanggal\", \"flag\", \"status\", \"payment_no\", \"memo\", \"tipe\", \"flag_ho\", \"bayar\") "
				." VALUES ('".$object->inv_no."',".$object->supp_id.",'".$object->tanggal."','f','".$object->status."','".$object->payment_no."','".$object->memo."','".$object->tipe."','".$object->flag_ho."','".$object->bayar."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_inv\" SET ";
		$flag = false;
		if($object->supp_id != '') { if($flag){ $sql .= ","; } $sql .= " \"supp_id\" = '".$object->supp_id."' "; $flag = true; }
		if($object->tanggal != '') { if($flag){ $sql .= ","; } $sql .= " \"tanggal\" = '".$object->tanggal."' "; $flag = true; }
		if($object->flag != '') { if($flag){ $sql .= ","; } $sql .= " \"flag\" = '".$object->flag."' "; $flag = true; }
		if($object->status != '') { if($flag){ $sql .= ","; } $sql .= " \"status\" = '".$object->status."' "; $flag = true; }
		if($object->payment_no != '') { if($flag){ $sql .= ","; } $sql .= " \"payment_no\" = '".$object->payment_no."' "; $flag = true; }
		if($object->memo != '') { if($flag){ $sql .= ","; } $sql .= " \"memo\" = '".$object->memo."' "; $flag = true; }
		if($object->tipe != '') { if($flag){ $sql .= ","; } $sql .= " \"tipe\" = '".$object->tipe."' "; $flag = true; }
		if($object->flag_ho != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_ho\" = '".$object->flag_ho."' "; $flag = true; }
		if($object->bayar != '') { if($flag){ $sql .= ","; } $sql .= " \"bayar\" = '".$object->bayar."' "; $flag = true; }
		$sql .= " WHERE \"payment_no\" = '".$object->payment_no."' ";		
		
		$success = $connection->execute($sql);
		
		return $sql;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "DELETE FROM \"t_inv\" "
				." WHERE \"payment_no\" = '".$object->payment_no."'";
				
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function getJoin_Supplier($condition){
		$connection = new Postgresql($this->di['db']);
				
		$sql = "SELECT \"id\", \"inv_no\", \"supp_name\", \"tanggal\", \"flag\", \"status\", \"payment_no\", \"address\", \"kecamatan\", \"supp_telp\", \"memo\", \"tipe\", \"flag_ho\", \"bayar\" "
				." FROM t_inv ti LEFT JOIN m_supplier ms ON ti.supp_id = ms.supplier_id ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['inv_no'] = $result[1];
			$lists[$count]['supp_name'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];		
			$lists[$count]['flag'] = json_encode($result[4]);		
			$lists[$count]['status'] = $result[5];		
			$lists[$count]['payment_no'] = $result[6];		
			$lists[$count]['address'] = $result[7];		
			$lists[$count]['kecamatan'] = $result[8];		
			$lists[$count]['supp_telp'] = $result[9];		
			$lists[$count]['memo'] = $result[10];		
			$lists[$count]['tipe'] = $result[11];		
			$lists[$count]['flag_ho'] = $result[12];		
			$lists[$count]['bayar'] = $result[13];		
			
			$count++;
		}
		
		return $lists;
	}
}