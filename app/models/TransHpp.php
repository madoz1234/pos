<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class TransHpp extends Model
{

	public $stock_id;
	public $qty;
	public $hpp;
	public $tanggal;	
	public $flag_sync;	
	public $barcode_code;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"stock_id\", \"qty\", \"hpp\", \"tanggal\", \"flag_sync\" "
				." FROM \"trans_hpp\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['qty'] = $result[1];
			$lists[$count]['hpp'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];
			$lists[$count]['flag_sync'] = $result[4];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"stock_id\", \"qty\", \"hpp\", \"tanggal\", \"flag_sync\" "
				." FROM \"trans_hpp\" "
				." WHERE \"stock_id\" = '".$object->stock_id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['qty'] = $result[1];
			$lists[$count]['hpp'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];
			$lists[$count]['flag_sync'] = $result[4];	
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"stock_id\", \"qty\", \"hpp\", \"tanggal\", \"flag_sync\" "
				." FROM \"trans_hpp\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['qty'] = $result[1];
			$lists[$count]['hpp'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];
			$lists[$count]['flag_sync'] = $result[4];	
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"stock_id\") "
				." FROM \"trans_hpp\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}

	public static function queryInsertHPP($object)
	{
		$sql = "INSERT INTO \"trans_hpp\" (\"stock_id\", \"qty\", \"hpp\" "
				.", \"tanggal\", \"flag_sync\" ) VALUES ";

		return $sql;
	}
	
	public static function insertSql($object)
	{
		if($object->qty == '') { $object->qty = '0'; }	
		if($object->hpp == '') { $object->hpp = '0'; }	
		if($object->flag_sync == '') { $object->flag_sync = '0'; }	

		$sql = "('".$object->stock_id."','".$object->qty."','".$object->hpp."','"
				.$object->tanggal."','".$object->flag_sync."'),";

		return $sql;
	}

	public static function InsertEndSql($object)
	{
		if($object->qty == '') { $object->qty = '0'; }	
		if($object->hpp == '') { $object->hpp = '0'; }	
		if($object->flag_sync == '') { $object->flag_sync = '0'; }	

		$sql = "('".$object->stock_id."','".$object->qty."','".$object->hpp."','"
				.$object->tanggal."','".$object->flag_sync."') ";

		return $sql;
	}

	public static function endSql($object)
	{
		$sql = "on conflict do nothing;";
		return $sql;
	}

	public function getHppPenjualanSync($tgl)
	{
		$connection = new Postgresql($this->di['db']);

		// $sql = "
		// 	select sum(jd.qty*pp.average_cost) as hpp
		// 	from t_jual j
		// 	join t_jual_detail jd on j.id_jual = jd.id_jual
		// 	join m_product_price pp on pp.stock_id = jd.item_code
		// 	where j.flag_sync = " . ($flag_sync ? 'true' : 'false') . "
		// ";		
		
		$sql = "
			select sum(qty*hpp) as hpp
			from trans_hpp
			where tanggal ='".$tgl."'
		";	
		
		$results = $connection->query($sql);
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists = $result[0];
		}
		
		return $lists;
	}
	
	public function goInsertItem($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->qty == '') { $object->qty = '0'; }
		if($object->unit_price == '') { $object->unit_price = '0'; }		
		if($object->flag_sync == '') { $object->flag_sync = '0'; }
		
		$sql = "INSERT INTO \"trans_hpp\" (\"stock_id\", \"qty\", \"hpp\", \"tanggal\", \"qty\", \"unit\", \"unit_price\", \"flag_sync\" "
				." , \"barcode_code\" ) "
				." VALUES ('".$object->stock_id."','".$object->qty."','".$object->hpp."','".pg_escape_string($object->tanggal)."','".$object->qty."','".$object->unit."','"
				.$object->unit_price."','".$object->flag_sync."','".$object->barcode_code."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"trans_hpp\" SET ";
		if($object->flag_sync != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = '".$object->flag_sync."' "; $flag = true; }	
		$sql .= " WHERE \"tanggal\" = '".$object->tanggal."' ";		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"trans_hpp\" ";
		$sql .= " WHERE \"qty\" = '".$object->qty."' ";		
		$sql .= " AND \"stock_id\" = '".$object->stock_id."' ";	
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goDeleteAdj($object){
		$connection = new Postgresql($this->di['db']);

		$sql = " DELETE FROM \"trans_hpp\" ";
		$sql .= " WHERE \"qty\" = '".$object->qty."' ";	
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function getJoin_Product($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"stock_id\", \"qty\", ad.\"hpp\", ad.\"tanggal\", ad.\"qty\", \"unit\", \"unit_price\", \"flag_sync\" "
				.", \"barcode_code\", 
				
				CASE
					WHEN uom1_htype_3 IS true THEN uom1_nm
					WHEN uom2_htype_3 IS true THEN uom2_nm
					WHEN uom3_htype_3 IS true THEN uom3_nm
					WHEN uom4_htype_3 IS true THEN uom4_nm
					ELSE ''
				END 
					AS uom_o_type,
					
				CASE
					WHEN uom1_htype_3 IS true THEN uom1_internal_barcode
					WHEN uom2_htype_3 IS true THEN uom2_internal_barcode
					WHEN uom3_htype_3 IS true THEN uom3_internal_barcode
					WHEN uom4_htype_3 IS true THEN uom4_internal_barcode
					ELSE ''
				END 
					AS uom_o_barcode,
					
				CASE
					WHEN uom1_htype_3 IS true THEN uom1_conversion
					WHEN uom2_htype_3 IS true THEN uom2_conversion
					WHEN uom3_htype_3 IS true THEN uom3_conversion
					WHEN uom4_htype_3 IS true THEN uom4_conversion
					ELSE ''
				END 
					AS uom_o_conversion, mc.\"l1_nm\", SUM(mpm.\"qty\")
				"
				." FROM \"trans_hpp\" ad INNER JOIN m_product mp ON ad.hpp = mp.stock_id INNER JOIN m_category mc ON mp.l1_cd = mc.l1_cd INNER JOIN m_product_move mpm ON ad.hpp = mpm.stock_id ".$condition."
					GROUP BY ad.stock_id, mp.uom1_htype_3, mp.uom1_nm, mp.uom2_htype_3, mp.uom2_nm, mp.uom3_htype_3, mp.uom3_nm, mp.uom4_htype_3, mp.uom4_nm, mp.uom1_internal_barcode, mp.uom2_internal_barcode, mp.uom3_internal_barcode, mp.uom4_internal_barcode, mp.uom1_conversion, mp.uom2_conversion, mp.uom3_conversion, mp.uom4_conversion, mc.l1_nm  ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['qty'] = $result[1];
			$lists[$count]['hpp'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];
			$lists[$count]['qty'] = $result[4];			
			$lists[$count]['unit'] = $result[5];	
			$lists[$count]['unit_price'] = $result[6];	
			$lists[$count]['flag_sync'] = $result[7];	
			$lists[$count]['barcode_code'] = $result[8];
			$lists[$count]['uom_o_type'] = $result[9];
			$lists[$count]['uom_o_barcode'] = $result[10];
			$lists[$count]['uom_o_conversion'] = $result[11];
			$lists[$count]['l1_nm'] = $result[12];
			$lists[$count]['qty_system'] = $result[13];
			
			$count++;
		}
		
		return $lists;
	}

	public function executeQuery($sql){
		$connection = new Postgresql($this->di['db']);
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
