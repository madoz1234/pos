<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class MemberPay extends Model
{

	public $id;
	public $branch_code;
	public $debtor_no;	
	public $id_member;	
	public $amount;
	public $create_by;
	public $create_date;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"branch_code\", \"debtor_no\", \"id_member\", \"amount\", \"create_by\", \"create_date\" "
				." FROM \"t_member_pay\" ";				
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['branch_code'] = $result[1];
			$lists[$count]['debtor_no'] = $result[2];
			$lists[$count]['id_member'] = $result[3];
			$lists[$count]['amount'] = $result[4];
			$lists[$count]['create_by'] = $result[5];
			$lists[$count]['create_date'] = $result[6];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql =  "SELECT \"id\", \"branch_code\", \"debtor_no\", \"id_member\", \"amount\", \"create_by\", \"create_date\" "
				." FROM \"t_member_pay\" "
				." WHERE \"id\" = '".$object->id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['branch_code'] = $result[1];
			$lists[$count]['debtor_no'] = $result[2];
			$lists[$count]['id_member'] = $result[3];
			$lists[$count]['amount'] = $result[4];
			$lists[$count]['create_by'] = $result[5];
			$lists[$count]['create_date'] = $result[6];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql =  "SELECT \"id\", \"branch_code\", \"debtor_no\", \"id_member\", \"amount\", \"create_by\", \"create_date\" "
				." FROM \"t_member_pay\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['branch_code'] = $result[1];
			$lists[$count]['debtor_no'] = $result[2];
			$lists[$count]['id_member'] = $result[3];
			$lists[$count]['amount'] = $result[4];
			$lists[$count]['create_by'] = $result[5];
			$lists[$count]['create_date'] = $result[6];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"id\") "
				." FROM \"t_member_pay\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->amount == '') { $object->point = '0'; }
		if($object->create_date == '') { $object->create_date = date('Y-m-d'); }
		
		$sql = "INSERT INTO \"t_member_pay\" (\"branch_code\", \"debtor_no\", \"id_member\", \"amount\", \"create_by\", \"create_date\") "
				." VALUES ('".$object->branch_code."','".$object->debtor_no."','".$object->id_member."','".$object->amount."','".$object->create_by
				."','".$object->create_date."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}	
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_member_pay\" SET ";
		$flag = false;
		if($object->branch_code != '') { if($flag){ $sql .= ","; } $sql .= " \"branch_code\" = '".$object->branch_code."' "; $flag = true; }
		if($object->debtor_no != '') { if($flag){ $sql .= ","; } $sql .= " \"debtor_no\" = '".$object->debtor_no."' "; $flag = true; }
		if($object->id_member != '') { if($flag){ $sql .= ","; } $sql .= " \"id_member\" = '".$object->id_member."' "; $flag = true; }		
		if($object->amount != '') { if($flag){ $sql .= ","; } $sql .= " \"amount\" = '".$object->amount."' "; $flag = true; }
		$sql .= " WHERE \"id\" = '".$object->id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_member_pay\" "
				." WHERE \"id\" = '".$object->id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}	
}
