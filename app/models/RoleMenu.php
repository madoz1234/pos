<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class RoleMenu extends Model
{

	public $role_id;
	public $menu_id;
	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"role_id\", \"menu_id\" "
				." FROM \"m_role_menu\" ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['role_id'] = $result[0];
			$lists[$count]['menu_id'] = $result[1];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"role_id\", \"menu_id\" "
				." FROM \"m_role_menu\" "
				." WHERE \"role_id\" = '".$object->role_id."' "
				." AND \"menu_id\" = '".$object->menu_id."' ";

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['role_id'] = $result[0];
			$lists[$count]['menu_id'] = $result[1];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"role_id\", \"menu_id\" "
				." FROM \"m_role_menu\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['role_id'] = $result[0];
			$lists[$count]['menu_id'] = $result[1];		
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"role_id\") "
				." FROM \"m_role_menu\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);	
		
		$sql = "INSERT INTO \"m_role_menu\" (\"role_id\", \"menu_id\") "
				." VALUES ('".$object->role_id."','".$object->menu_id."') ";
		
		$success = $connection->execute($sql);		
		
		return $sql;
	}
	
	public function goMultiInsert($object){
		$connection = new Postgresql($this->di['db']);	
		
		$sql = "INSERT INTO \"m_role_menu\" (\"role_id\", \"menu_id\") "
				." VALUES ".$object;
		
		$success = $connection->execute($sql);		
		
		return $sql;
	}
	
	public function goUpdate($object){
		
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_role_menu\" "
				." WHERE \"role_id\" = '".$object->role_id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goDeleteRole($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_role_menu\" "
				." WHERE \"role_id\" = '".$object->role_id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
