<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Shift extends Model
{

	public $shift_id;
	public $description;
	public $inactive;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"shift_id\", \"description\", \"inactive\" "
				." FROM \"m_shift\" "
				." WHERE \"inactive\" != 1 ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['shift_id'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$lists[$count]['inactive'] = $result[2];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"shift_id\", \"description\", \"inactive\" "
				." FROM \"m_shift\" "
				." WHERE \"shift_id\" = '".$object->shift_id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['shift_id'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$lists[$count]['inactive'] = $result[2];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"shift_id\", \"description\", \"inactive\" "
				." FROM \"m_shift\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['shift_id'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$lists[$count]['inactive'] = $result[2];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"shift_id\") "
				." FROM \"m_shift\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->inactive == '') { $object->inactive = '0'; }
		
		$sql = "INSERT INTO \"m_shift\" (\"description\", \"inactive\") "
				." VALUES ('".$object->description."','".$object->inactive."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_shift\" SET ";
		$flag = false;
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".$object->description."' "; $flag = true; }
		if($object->inactive != '') { if($flag){ $sql .= ","; } $sql .= " \"inactive\" = '".$object->inactive."' "; $flag = true; }
		$sql .= " WHERE \"shift_id\" = '".$object->shift_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_shift\" "
				." WHERE \"shift_id\" = '".$object->shift_id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
