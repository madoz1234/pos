<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class AdjDetail extends Model
{

	public $adj_detail_item;
	public $adj_id;
	public $item_code;
	public $description;	
	public $qty;
	public $unit;	
	public $unit_price;	
	public $total_price;	
	public $barcode_code;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"adj_detail_item\", \"adj_id\", \"item_code\", \"description\", \"qty\", \"unit\", \"unit_price\", \"total_price\" "
				." , \"barcode_code\" "
				." FROM \"t_adj_detail\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['adj_detail_item'] = $result[0];
			$lists[$count]['adj_id'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty'] = $result[4];			
			$lists[$count]['unit'] = $result[5];	
			$lists[$count]['unit_price'] = $result[6];	
			$lists[$count]['total_price'] = $result[7];	
			$lists[$count]['barcode_code'] = $result[8];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"adj_detail_item\", \"adj_id\", \"item_code\", \"description\", \"qty\", \"unit\", \"unit_price\", \"total_price\" "
				." , \"barcode_code\" "
				." FROM \"t_adj_detail\" "
				." WHERE \"adj_id\" = '".$object->adj_id."' "
				." AND \"adj_detail_item\" = '".$object->adj_detail_item."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['adj_detail_item'] = $result[0];
			$lists[$count]['adj_id'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty'] = $result[4];			
			$lists[$count]['unit'] = $result[5];	
			$lists[$count]['unit_price'] = $result[6];	
			$lists[$count]['total_price'] = $result[7];	
			$lists[$count]['barcode_code'] = $result[8];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"adj_detail_item\", \"adj_id\", \"item_code\", \"description\", \"qty\", \"unit\", \"unit_price\", \"total_price\" "
				." , \"barcode_code\" "
				." FROM \"t_adj_detail\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['adj_detail_item'] = $result[0];
			$lists[$count]['adj_id'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty'] = $result[4];			
			$lists[$count]['unit'] = $result[5];	
			$lists[$count]['unit_price'] = $result[6];	
			$lists[$count]['total_price'] = $result[7];	
			$lists[$count]['barcode_code'] = $result[8];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"adj_id\") "
				." FROM \"t_adj_detail\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->qty == '') { $object->qty = '0'; }
		if($object->unit_price == '') { $object->unit_price = '0'; }		
		if($object->total_price == '') { $object->total_price = '0'; }
		
		$sql = "INSERT INTO \"t_adj_detail\" (\"adj_id\", \"item_code\", \"description\", \"qty\", \"unit\", \"unit_price\", \"total_price\" "
				." , \"barcode_code\" ) "
				." VALUES ('".$object->adj_id."','".$object->item_code."','".pg_escape_string($object->description)."','".$object->qty."','".$object->unit."','"
				.$object->unit_price."','".$object->total_price."','".$object->barcode_code."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goInsertItem($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->qty == '') { $object->qty = '0'; }
		if($object->unit_price == '') { $object->unit_price = '0'; }		
		if($object->total_price == '') { $object->total_price = '0'; }
		
		$sql = "INSERT INTO \"t_adj_detail\" (\"adj_detail_item\", \"adj_id\", \"item_code\", \"description\", \"qty\", \"unit\", \"unit_price\", \"total_price\" "
				." , \"barcode_code\" ) "
				." VALUES ('".$object->adj_detail_item."','".$object->adj_id."','".$object->item_code."','".pg_escape_string($object->description)."','".$object->qty."','".$object->unit."','"
				.$object->unit_price."','".$object->total_price."','".$object->barcode_code."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_adj_detail\" SET ";
		$flag = false;
		if($object->item_code != '') { if($flag){ $sql .= ","; } $sql .= " \"item_code\" = '".$object->item_code."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".pg_escape_string($object->description)."' "; $flag = true; }
		if($object->qty != '') { if($flag){ $sql .= ","; } $sql .= " \"qty\" = '".$object->qty."' "; $flag = true; }
		if($object->unit != '') { if($flag){ $sql .= ","; } $sql .= " \"unit\" = '".$object->unit."' "; $flag = true; }
		if($object->unit_item != '') { if($flag){ $sql .= ","; } $sql .= " \"unit_item\" = '".$object->unit_item."' "; $flag = true; }
		if($object->unit_price != '') { if($flag){ $sql .= ","; } $sql .= " \"unit_price\" = '".$object->unit_price."' "; $flag = true; }
		if($object->total_price != '') { if($flag){ $sql .= ","; } $sql .= " \"total_price\" = '".$object->total_price."' "; $flag = true; }		
		if($object->barcode_code != '') { if($flag){ $sql .= ","; } $sql .= " \"barcode_code\" = '".$object->barcode_code."' "; $flag = true; }		
		$sql .= " WHERE \"adj_id\" = '".$object->so_id."' ";		
		$sql .= " AND \"adj_detail_item\" = '".$object->so_detail_item."' ";	
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_adj_detail\" ";
		$sql .= " WHERE \"adj_id\" = '".$object->adj_id."' ";		
		$sql .= " AND \"adj_detail_item\" = '".$object->adj_detail_item."' ";	
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goDeleteAdj($object){
		$connection = new Postgresql($this->di['db']);

		$sql = " DELETE FROM \"t_adj_detail\" ";
		$sql .= " WHERE \"adj_id\" = '".$object->adj_id."' ";	
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function getJoin_Product($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"adj_detail_item\", \"adj_id\", ad.\"item_code\", ad.\"description\", ad.\"qty\", \"unit\", \"unit_price\", \"total_price\" "
				.", \"barcode_code\", 
				
				CASE
					WHEN uom1_htype_3 IS true THEN uom1_nm
					WHEN uom2_htype_3 IS true THEN uom2_nm
					WHEN uom3_htype_3 IS true THEN uom3_nm
					WHEN uom4_htype_3 IS true THEN uom4_nm
					ELSE ''
				END 
					AS uom_o_type,
					
				CASE
					WHEN uom1_htype_3 IS true THEN uom1_internal_barcode
					WHEN uom2_htype_3 IS true THEN uom2_internal_barcode
					WHEN uom3_htype_3 IS true THEN uom3_internal_barcode
					WHEN uom4_htype_3 IS true THEN uom4_internal_barcode
					ELSE ''
				END 
					AS uom_o_barcode,
					
				CASE
					WHEN uom1_htype_3 IS true THEN uom1_conversion
					WHEN uom2_htype_3 IS true THEN uom2_conversion
					WHEN uom3_htype_3 IS true THEN uom3_conversion
					WHEN uom4_htype_3 IS true THEN uom4_conversion
					ELSE ''
				END 
					AS uom_o_conversion, mc.\"l1_nm\", SUM(mpm.\"qty\")
				"
				." FROM \"t_adj_detail\" ad INNER JOIN m_product mp ON ad.item_code = mp.stock_id INNER JOIN m_category mc ON mp.l1_cd = mc.l1_cd INNER JOIN m_product_move mpm ON ad.item_code = mpm.stock_id ".$condition."
					GROUP BY ad.adj_detail_item, mp.uom1_htype_3, mp.uom1_nm, mp.uom2_htype_3, mp.uom2_nm, mp.uom3_htype_3, mp.uom3_nm, mp.uom4_htype_3, mp.uom4_nm, mp.uom1_internal_barcode, mp.uom2_internal_barcode, mp.uom3_internal_barcode, mp.uom4_internal_barcode, mp.uom1_conversion, mp.uom2_conversion, mp.uom3_conversion, mp.uom4_conversion, mc.l1_nm  ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['adj_detail_item'] = $result[0];
			$lists[$count]['adj_id'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty'] = $result[4];			
			$lists[$count]['unit'] = $result[5];	
			$lists[$count]['unit_price'] = $result[6];	
			$lists[$count]['total_price'] = $result[7];	
			$lists[$count]['barcode_code'] = $result[8];
			$lists[$count]['uom_o_type'] = $result[9];
			$lists[$count]['uom_o_barcode'] = $result[10];
			$lists[$count]['uom_o_conversion'] = $result[11];
			$lists[$count]['l1_nm'] = $result[12];
			$lists[$count]['qty_system'] = $result[13];
			
			$count++;
		}
		
		return $lists;
	}
}
