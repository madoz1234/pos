<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class User extends Model
{

	public $user_id;
	public $user_name;
	public $role_id;
	public $real_name;	
	public $last_login;
	public $count_login;
	public $date_created;	
	public $user_password;
	public $is_active;
	public $images;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"user_id\", \"user_name\", \"role_id\", \"real_name\", \"last_login\", \"count_login\", \"date_created\", \"user_password\", \"is_active\", \"images\" "
				." FROM \"m_user\" "
				." WHERE \"is_active\" = 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['user_id'] = $result[0];
			$lists[$count]['user_name'] = $result[1];
			$lists[$count]['role_id'] = $result[2];
			$lists[$count]['real_name'] = $result[3];
			$lists[$count]['last_login'] = $result[4];			
			$lists[$count]['count_login'] = $result[5];	
			$lists[$count]['date_created'] = $result[6];	
			$lists[$count]['user_password'] = $result[7];	
			$lists[$count]['is_active'] = $result[8];
			$lists[$count]['images'] = $result[9];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"user_id\", \"user_name\", \"role_id\", \"real_name\", \"last_login\", \"count_login\", \"date_created\", \"user_password\", \"is_active\", \"images\" "
				." FROM \"m_user\" "
				." WHERE \"user_name\" = '".$object->user_name."' "
				." AND \"user_password\" = '".$object->user_password."' "
				." LIMIT 1 ";

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['user_id'] = $result[0];
			$lists[$count]['user_name'] = $result[1];
			$lists[$count]['role_id'] = $result[2];
			$lists[$count]['real_name'] = $result[3];
			$lists[$count]['last_login'] = $result[4];			
			$lists[$count]['count_login'] = $result[5];	
			$lists[$count]['date_created'] = $result[6];
			$lists[$count]['user_password'] = $result[7];	
			$lists[$count]['is_active'] = $result[8];
			$lists[$count]['images'] = $result[9];				
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"user_id\", \"user_name\", \"role_id\", \"real_name\", \"last_login\", \"count_login\", \"date_created\", \"user_password\", \"is_active\", \"images\" "
				." FROM \"m_user\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['user_id'] = $result[0];
			$lists[$count]['user_name'] = $result[1];
			$lists[$count]['role_id'] = $result[2];
			$lists[$count]['real_name'] = $result[3];
			$lists[$count]['last_login'] = $result[4];			
			$lists[$count]['count_login'] = $result[5];	
			$lists[$count]['date_created'] = $result[6];	
			$lists[$count]['user_password'] = $result[7];	
			$lists[$count]['is_active'] = $result[8];	
			$lists[$count]['images'] = $result[9];

			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"user_name\") "
				." FROM \"m_user\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->count_login == '') { $object->count_login = '0'; }
		if($object->is_active == '') { $object->is_active = '1'; }		
		if($object->last_login == '') { $object->last_login = date("Y-m-d"); } 
		if($object->date_created == '') { $object->date_created = date("Y-m-d"); } 
		
		if($object->user_password != '') { $object->user_password = base64_encode($object->user_password); }
		
		$sql = "INSERT INTO \"m_user\" (\"user_name\", \"role_id\", \"real_name\", \"last_login\", \"count_login\", \"date_created\", \"user_password\", \"is_active\", \"images\") "
				." VALUES ('".$object->user_name."','".$object->role_id."','".$object->real_name."','".$object->last_login."','"
				.$object->count_login."','".$object->date_created."','".$object->user_password."','".$object->is_active."','".$object->images."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $sql;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_user\" SET ";
		$flag = false;		
		if($object->role_id != '') { if($flag){ $sql .= ","; } $sql .= " \"role_id\" = '".$object->role_id."' "; $flag = true; }
		if($object->real_name != '') { if($flag){ $sql .= ","; } $sql .= " \"real_name\" = '".$object->real_name."' "; $flag = true; }
		if($object->user_name != '') { if($flag){ $sql .= ","; } $sql .= " \"user_name\" = '".$object->user_name."' "; $flag = true; }
		if($object->last_login != '') { if($flag){ $sql .= ","; } $sql .= " \"last_login\" = '".$object->last_login."' "; $flag = true; }
		if($object->count_login != '') { if($flag){ $sql .= ","; } $sql .= " \"count_login\" = '".$object->count_login."' "; $flag = true; }
		if($object->user_password != '') { if($flag){ $sql .= ","; } $sql .= " \"user_password\" = '".$object->user_password."' "; $flag = true; }
		if($object->images != '') { if($flag){ $sql .= ","; } $sql .= " \"images\" = '".$object->images."' "; $flag = true; }
		if($object->is_active != '') { if($flag){ $sql .= ","; } $sql .= " \"is_active\" = '".$object->is_active."' "; $flag = true; }
		$sql .= " WHERE \"user_id\" = '".$object->user_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_user\" "
				." WHERE \"user_id\" = '".$object->user_id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function getJoin_Role($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT u.\"user_id\", u.\"user_name\", u.\"role_id\", u.\"real_name\", u.\"last_login\", u.\"count_login\", u.\"date_created\", u.\"user_password\", u.\"is_active\", r.\"role_name\" "
				." FROM \"m_user\" u INNER JOIN \"m_role\" r ON u.role_id = r.role_id ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['user_id'] = $result[0];
			$lists[$count]['user_name'] = $result[1];
			$lists[$count]['role_id'] = $result[2];
			$lists[$count]['real_name'] = $result[3];
			$lists[$count]['last_login'] = $result[4];			
			$lists[$count]['count_login'] = $result[5];	
			$lists[$count]['date_created'] = $result[6];	
			$lists[$count]['user_password'] = $result[7];	
			$lists[$count]['is_active'] = $result[8];	
			$lists[$count]['role_name'] = $result[9];	
			
			$count++;
		}
		
		return $lists;
	}
}
