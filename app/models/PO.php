<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class PO extends Model
{

	public $po_no_immbo;
	public $po_no_ref;
	public $reference;
	public $supplier_id;	
	public $order_date;
	public $delivery_date;
	public $delivery_address;
	public $comments;
	public $pr_no_immpos;
	public $pr_no_immbo;
	public $gr_no_immpos;	
	public $total_pr;
	public $total_po;
	public $tax_included;
	public $gr_no_immbo;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT	\"po_no_immbo\", \"po_no_ref\", \"reference\", \"supplier_id\", \"order_date\", \"delivery_date\", \"delivery_address\", \"comments\", \"pr_no_immpos\", "
					."\"pr_no_immbo\", \"gr_no_immpos\", \"total_pr\", \"total_po\", \"tax_included\", \"gr_no_immbo\" "
					." FROM t_po ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['po_no_immbo'] = $result[0];
			$lists[$count]['po_no_ref'] = $result[1];
			$lists[$count]['reference'] = $result[2];
			$lists[$count]['supplier_id'] = $result[3];	
			$lists[$count]['order_date'] = $result[4];	
			$lists[$count]['delivery_date'] = $result[5];	
			$lists[$count]['delivery_address'] = $result[6];	
			$lists[$count]['comments'] = $result[7];	
			$lists[$count]['pr_no_immpos'] = $result[8];	
			$lists[$count]['pr_no_immbo'] = $result[9];	
			$lists[$count]['gr_no_immpos'] = $result[10];	
			$lists[$count]['total_pr'] = $result[11];	
			$lists[$count]['total_po'] = $result[12];	
			$lists[$count]['tax_included'] = $result[13];	
			$lists[$count]['gr_no_immbo'] = $result[14];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT	\"po_no_immbo\", \"po_no_ref\", \"reference\", \"supplier_id\", \"order_date\", \"delivery_date\", \"delivery_address\", \"comments\", \"pr_no_immpos\", "
					."\"pr_no_immbo\", \"gr_no_immpos\", \"total_pr\", \"total_po\", \"tax_included\", \"gr_no_immbo\" "
					." FROM t_po "
					." WHERE \"po_no_immbo\" = '".$object->po_no_immbo."' ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['po_no_immbo'] = $result[0];
			$lists[$count]['po_no_ref'] = $result[1];
			$lists[$count]['reference'] = $result[2];
			$lists[$count]['supplier_id'] = $result[3];	
			$lists[$count]['order_date'] = $result[4];	
			$lists[$count]['delivery_date'] = $result[5];	
			$lists[$count]['delivery_address'] = $result[6];	
			$lists[$count]['comments'] = $result[7];	
			$lists[$count]['pr_no_immpos'] = $result[8];	
			$lists[$count]['pr_no_immbo'] = $result[9];	
			$lists[$count]['gr_no_immpos'] = $result[10];	
			$lists[$count]['total_pr'] = $result[11];	
			$lists[$count]['total_po'] = $result[12];	
			$lists[$count]['tax_included'] = $result[13];
			$lists[$count]['gr_no_immbo'] = $result[14];			
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT	\"po_no_immbo\", \"po_no_ref\", \"reference\", \"supplier_id\", \"order_date\", \"delivery_date\", \"delivery_address\", \"comments\", \"pr_no_immpos\", "
					."\"pr_no_immbo\", \"gr_no_immpos\", \"total_pr\", \"total_po\", \"tax_included\", \"gr_no_immbo\" "
					." FROM t_po ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['po_no_immbo'] = $result[0];
			$lists[$count]['po_no_ref'] = $result[1];
			$lists[$count]['reference'] = $result[2];
			$lists[$count]['supplier_id'] = $result[3];	
			$lists[$count]['order_date'] = $result[4];	
			$lists[$count]['delivery_date'] = $result[5];	
			$lists[$count]['delivery_address'] = $result[6];	
			$lists[$count]['comments'] = $result[7];	
			$lists[$count]['pr_no_immpos'] = $result[8];	
			$lists[$count]['pr_no_immbo'] = $result[9];	
			$lists[$count]['gr_no_immpos'] = $result[10];	
			$lists[$count]['total_pr'] = $result[11];	
			$lists[$count]['total_po'] = $result[12];	
			$lists[$count]['tax_included'] = $result[13];	
			$lists[$count]['gr_no_immbo'] = $result[14];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"po_no_immbo\") "
				." FROM \"t_po\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->supplier_id == '') { $object->supplier_id = '0'; }		
		if($object->total_pr == '') { $object->total_pr = '0'; }	
		if($object->total_po == '') { $object->total_po = '0'; }	
		if($object->tax_included == '') { $object->tax_included = '0'; }	
		
		if($object->delivery_date == '') { $object->delivery_date = date("Y-m-d"); }	
		if($object->order_date == '') { $object->order_date = date("Y-m-d"); }
		
		$sql = "INSERT INTO \"t_po\" (\"po_no_immbo\", \"po_no_ref\", \"reference\", \"supplier_id\", \"order_date\", \"delivery_date\", \"delivery_address\" "
				." , \"comments\", \"pr_no_immpos\", \"pr_no_immbo\", \"gr_no_immpos\", \"total_pr\", \"total_po\", \"tax_included\", \"gr_no_immbo\") "
				." VALUES ('".$object->po_no_immbo."','".$object->po_no_ref."','".$object->reference."','".$object->supplier_id."','".$object->order_date."','"
				.$object->delivery_date."','".$object->delivery_address."','".$object->comments."','".$object->pr_no_immpos."','".$object->pr_no_immbo."','','"
				.$object->total_pr."','".$object->total_po."','".$object->tax_included."','".$object->gr_no_immbo."')";
		$success = $connection->execute($sql);	

		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_po\" SET ";
		$flag = false;
		if($object->po_no_ref != '') { if($flag){ $sql .= ","; } $sql .= " \"po_no_ref\" = '".$object->po_no_ref."' "; $flag = true; }
		if($object->reference != '') { if($flag){ $sql .= ","; } $sql .= " \"reference\" = '".$object->reference."' "; $flag = true; }
		if($object->supplier_id != '') { if($flag){ $sql .= ","; } $sql .= " \"supplier_id\" = '".$object->supplier_id."' "; $flag = true; }
		if($object->order_date != '') { if($flag){ $sql .= ","; } $sql .= " \"order_date\" = '".$object->order_date."' "; $flag = true; }
		if($object->delivery_date != '') { if($flag){ $sql .= ","; } $sql .= " \"delivery_date\" = '".$object->delivery_date."' "; $flag = true; }
		if($object->delivery_address != '') { if($flag){ $sql .= ","; } $sql .= " \"delivery_address\" = '".$object->delivery_address."' "; $flag = true; }
		if($object->pr_no_immpos != '') { if($flag){ $sql .= ","; } $sql .= " \"pr_no_immpos\" = '".$object->pr_no_immpos."' "; $flag = true; }		
		if($object->pr_no_immbo != '') { if($flag){ $sql .= ","; } $sql .= " \"pr_no_immbo\" = '".$object->pr_no_immbo."' "; $flag = true; }	
		if($object->gr_no_immpos != '') { if($flag){ $sql .= ","; } $sql .= " \"gr_no_immpos\" = '".$object->gr_no_immpos."' "; $flag = true; }	
		if($object->total_pr != '') { if($flag){ $sql .= ","; } $sql .= " \"total_pr\" = '".$object->total_pr."' "; $flag = true; }	
		if($object->total_po != '') { if($flag){ $sql .= ","; } $sql .= " \"total_po\" = '".$object->total_po."' "; $flag = true; }	
		if($object->tax_included != '') { if($flag){ $sql .= ","; } $sql .= " \"tax_included\" = '".$object->tax_included."' "; $flag = true; }	
		if($object->gr_no_immbo != '') { if($flag){ $sql .= ","; } $sql .= " \"gr_no_immbo\" = '".$object->gr_no_immbo."' "; $flag = true; }
		$sql .= " WHERE \"po_no_immbo\" = '".$object->po_no_immbo."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goUpdateClear($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_po\" SET ";
		$flag = false;
		if($object->po_no_ref == 'X') { if($flag){ $sql .= ","; } $sql .= " \"po_no_ref\" = '' "; $flag = true; }
		if($object->reference == 'X') { if($flag){ $sql .= ","; } $sql .= " \"reference\" = '' "; $flag = true; }
		if($object->supplier_id == 'X') { if($flag){ $sql .= ","; } $sql .= " \"supplier_id\" = '0' "; $flag = true; }
		if($object->delivery_address == 'X') { if($flag){ $sql .= ","; } $sql .= " \"delivery_address\" = '' "; $flag = true; }
		if($object->pr_no_immpos == 'X') { if($flag){ $sql .= ","; } $sql .= " \"pr_no_immpos\" = '' "; $flag = true; }		
		if($object->pr_no_immbo == 'X') { if($flag){ $sql .= ","; } $sql .= " \"pr_no_immbo\" = '' "; $flag = true; }	
		if($object->gr_no_immpos == 'X') { if($flag){ $sql .= ","; } $sql .= " \"gr_no_immpos\" = '' "; $flag = true; }	
		if($object->total_pr == 'X') { if($flag){ $sql .= ","; } $sql .= " \"total_pr\" = '0' "; $flag = true; }	
		if($object->total_po == 'X') { if($flag){ $sql .= ","; } $sql .= " \"total_po\" = '0' "; $flag = true; }	
		if($object->tax_included == 'X') { if($flag){ $sql .= ","; } $sql .= " \"tax_included\" = '0' "; $flag = true; }	
		if($object->gr_no_immbo == 'X') { if($flag){ $sql .= ","; } $sql .= " \"gr_no_immbo\" = '' "; $flag = true; }
		$sql .= " WHERE \"po_no_immbo\" = '".$object->po_no_immbo."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_po\" "
				." WHERE \"po_no_immbo\" = '".$object->po_no_immbo."' ";			
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function getJoin($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT po.\"po_no_immbo\", po.\"pr_no_immbo\", po.\"order_date\", po.\"delivery_date\", pod.\"item_code\", pod.\"description\", pod.\"qty_po\", pod.\"qty_pr\" "
		.", pod.\"po_unit_price\", pod.\"po_price\", po.\"total_po\", mp.\"uom2_nm\", mp.\"uom2_internal_barcode\", mp.\"uom1_nm\", mp.\"uom1_internal_barcode\", mp.\"uom2_conversion\", mp.\"uom1_conversion\", po.\"po_no_ref\", pod.\"unit_po\", pod.\"flag_gr_ok\" "
				
		." FROM t_po po INNER JOIN t_po_detail pod ON po.po_no_immbo = pod.po_no_immbo 
			INNER JOIN m_product mp ON pod.item_code = mp.stock_id ".$condition."";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['po_no_immbo'] = $result[0];
			$lists[$count]['pr_no_immbo'] = $result[1];
			$lists[$count]['order_date'] = $result[2];
			$lists[$count]['delivery_date'] = $result[3];	
			$lists[$count]['item_code'] = $result[4];	
			$lists[$count]['description'] = $result[5];	
			$lists[$count]['qty_po'] = $result[6];	
			$lists[$count]['qty_pr'] = $result[7] / $result[15];	
			$lists[$count]['po_unit_price'] = $result[8] / $result[6];	
			$lists[$count]['po_price'] = $result[9];	
			$lists[$count]['total_po'] = $result[10];	
			$lists[$count]['uom_o_type'] = $result[11];	
			$lists[$count]['uom_o_barcode'] = $result[12];	
			$lists[$count]['uom_s_type'] = $result[13];	
			$lists[$count]['uom_s_barcode'] = $result[14];	
			$lists[$count]['uom_o_conversion'] = $result[15];	
			$lists[$count]['uom_s_conversion'] = $result[16];
			$lists[$count]['po_no_ref'] = $result[17];
			$lists[$count]['unit_po'] = $result[18];
			$lists[$count]['flag_gr_ok'] = $result[19];
			
			$count++;
		}
		
		return $lists;
	}
	
	
	public function getJoin_PODetail($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql =  "SELECT	po.\"po_no_immbo\", \"po_no_ref\", \"reference\", \"supplier_id\", \"order_date\", \"delivery_date\", \"delivery_address\", \"comments\", \"pr_no_immpos\", "
				." \"pr_no_immbo\", \"gr_no_immpos\", \"total_pr\", \"total_po\", \"tax_included\", \"gr_no_immbo\",\"po_detail_item\", \"item_code\", \"description\", \"qty_po\", "
				." \"unit_po\", \"po_unit_price\", \"po_price\", \"flag_gr_ok\" , \"pr_detail_item\" "
				." FROM t_po po INNER JOIN t_po_detail pod ON po.po_no_immbo = pod.po_no_immbo ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['po_no_immbo'] = $result[0];
			$lists[$count]['po_no_ref'] = $result[1];
			$lists[$count]['reference'] = $result[2];
			$lists[$count]['supplier_id'] = $result[3];	
			$lists[$count]['order_date'] = $result[4];	
			$lists[$count]['delivery_date'] = $result[5];	
			$lists[$count]['delivery_address'] = $result[6];	
			$lists[$count]['comments'] = $result[7];	
			$lists[$count]['pr_no_immpos'] = $result[8];	
			$lists[$count]['pr_no_immbo'] = $result[9];	
			$lists[$count]['gr_no_immpos'] = $result[10];	
			$lists[$count]['total_pr'] = $result[11];	
			$lists[$count]['total_po'] = $result[12];	
			$lists[$count]['tax_included'] = $result[13];	
			$lists[$count]['gr_no_immbo'] = $result[14];	
			$lists[$count]['po_detail_item'] = $result[15];
			$lists[$count]['item_code'] = $result[16];
			$lists[$count]['description'] = $result[17];
			$lists[$count]['qty_po'] = $result[18];			
			$lists[$count]['unit_po'] = $result[19];	
			$lists[$count]['po_unit_price'] = $result[20];	
			$lists[$count]['po_price'] = $result[21];	
			$lists[$count]['flag_gr_ok'] = $result[22];	
			$lists[$count]['pr_detail_item'] = $result[23];		
			
			$count++;
		}
		
		return $lists;
	}
}
