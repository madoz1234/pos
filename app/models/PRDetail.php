<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class PRDetail extends Model
{

	public $pr_detail_item;
	public $request_no;
	public $item_code;
	public $description;		
	public $qty_order;
	public $unit_order;	
	public $qty_sell;
	public $unit_sell;
	public $qty_pr;
	public $unit_pr;
	public $unit_order_price;
	public $order_price;	
	public $con_order;
	public $con_sell;
	public $con_pr;
	public $barcode;
	public $supplied_by;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"pr_detail_item\", \"request_no\", \"item_code\", \"description\", \"qty_order\", \"unit_order\", \"qty_sell\" "
				." , \"unit_sell\", \"qty_pr\", \"unit_pr\", \"unit_order_price\", \"order_price\", \"con_order\", \"con_sell\", \"con_pr\", \"barcode\", \"supplied_by\""
				." FROM \"t_pr_detail\"";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['pr_detail_item'] = $result[0];
			$lists[$count]['request_no'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty_order'] = $result[4];			
			$lists[$count]['unit_order'] = $result[5];	
			$lists[$count]['qty_sell'] = $result[6];	
			$lists[$count]['unit_sell'] = $result[7];	
			$lists[$count]['qty_pr'] = $result[8];	
			$lists[$count]['unit_pr'] = $result[9];	
			$lists[$count]['unit_order_price'] = $result[10];	
			$lists[$count]['order_price'] = $result[11];
			$lists[$count]['con_order'] = $result[12];
			$lists[$count]['con_sell'] = $result[13];
			$lists[$count]['con_pr'] = $result[14];			
			$lists[$count]['barcode'] = $result[15];
			$lists[$count]['supplied_by'] = $result[16];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"pr_detail_item\", \"request_no\", \"item_code\", \"description\", \"qty_order\", \"unit_order\", \"qty_sell\" "
				." , \"unit_sell\", \"qty_pr\", \"unit_pr\", \"unit_order_price\", \"order_price\", \"con_order\", \"con_sell\", \"con_pr\", \"barcode\", \"supplied_by\" "
				." FROM \"t_pr_detail\" "
				." WHERE \"request_no\" = '".$object->request_no."' "
				." AND \"pr_detail_item\" = '".$object->pr_detail_item."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['pr_detail_item'] = $result[0];
			$lists[$count]['request_no'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty_order'] = $result[4];			
			$lists[$count]['unit_order'] = $result[5];	
			$lists[$count]['qty_sell'] = $result[6];	
			$lists[$count]['unit_sell'] = $result[7];	
			$lists[$count]['qty_pr'] = $result[8];	
			$lists[$count]['unit_pr'] = $result[9];	
			$lists[$count]['unit_order_price'] = $result[10];	
			$lists[$count]['order_price'] = $result[11];
			$lists[$count]['con_order'] = $result[12];
			$lists[$count]['con_sell'] = $result[13];
			$lists[$count]['con_pr'] = $result[14];
			$lists[$count]['barcode'] = $result[15];
			$lists[$count]['supplied_by'] = $result[16];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT a.\"pr_detail_item\", a.\"request_no\", a.\"item_code\", a.\"description\", a.\"qty_order\", a.\"unit_order\", a.\"qty_sell\" "
				." , a.\"unit_sell\", a.\"qty_pr\", a.\"unit_pr\", a.\"unit_order_price\", a.\"order_price\", a.\"con_order\", a.\"con_sell\", a.\"con_pr\", a.\"barcode\", a.\"supplied_by\",  b.\"uom2_internal_barcode\", b.\"uom2_conversion\", b.\"uom2_nm\", b.\"uom1_internal_barcode\", b.\"uom1_nm\" "
				." FROM \"t_pr_detail\" a join \"m_product\" b on a.item_code = b.stock_id ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['pr_detail_item'] = $result[0];
			$lists[$count]['request_no'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty_order'] = $result[4];			
			$lists[$count]['unit_order'] = $result[5];	
			$lists[$count]['qty_sell'] = $result[6];	
			$lists[$count]['unit_sell'] = $result[7];	
			$lists[$count]['qty_pr'] = $result[8];	
			$lists[$count]['unit_pr'] = $result[9];	
			$lists[$count]['unit_order_price'] = $result[10];	
			$lists[$count]['order_price'] = $result[11];
			$lists[$count]['con_order'] = $result[12];
			$lists[$count]['con_sell'] = $result[13];
			$lists[$count]['con_pr'] = $result[14];
			$lists[$count]['barcode'] = $result[15];
			$lists[$count]['supplied_by'] = $result[16];
			$lists[$count]['uom2_internal_barcode'] = $result[17];
			$lists[$count]['uom2_conversion'] = $result[18];
			$lists[$count]['uom2_nm'] = $result[19];
			$lists[$count]['uom1_internal_barcode'] = $result[20];
			$lists[$count]['uom1_nm'] = $result[21];
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"request_no\") "
				." FROM \"t_pr_detail\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->qty_order == '') { $object->qty_order = '0'; }
		if($object->qty_sell == '') { $object->qty_sell = '0'; }		
		if($object->qty_pr == '') { $object->qty_pr = '0'; }	
		if($object->unit_order_price == '') { $object->unit_order_price = '0'; }	
		if($object->order_price == '') { $object->order_price = '0'; }	
		
		$sql = "INSERT INTO \"t_pr_detail\" (\"request_no\", \"item_code\", \"description\", \"qty_order\", \"unit_order\", \"qty_sell\" "
				." , \"unit_sell\", \"qty_pr\", \"unit_pr\", \"unit_order_price\", \"order_price\", \"con_order\", \"con_sell\", \"con_pr\", \"barcode\", \"supplied_by\" ) "
				." VALUES ('".$object->request_no."','".$object->item_code."','".pg_escape_string($object->description)."','".$object->qty_order."','".$object->unit_order."','"
				.$object->qty_sell."','".$object->unit_sell."','".$object->qty_pr."','".$object->unit_pr."','".$object->unit_order_price."','"
				.$object->order_price."','".$object->con_order."','".$object->con_sell."','".$object->con_pr."','".$object->barcode."','".$object->supplied_by."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goInsertItem($object){
		$connection = new Postgresql($this->di['db']);

		if($object->qty_order == '') { $object->qty_order = '0'; }
		if($object->qty_sell == '') { $object->qty_sell = '0'; }		
		if($object->qty_pr == '') { $object->qty_pr = '0'; }	
		if($object->unit_order_price == '') { $object->unit_order_price = '0'; }	
		if($object->order_price == '') { $object->order_price = '0'; }	
		
		$sql = "INSERT INTO \"t_pr_detail\" (\"pr_detail_item\", \"request_no\", \"item_code\", \"description\", \"qty_order\", \"unit_order\", \"qty_sell\" "
				." , \"unit_sell\", \"qty_pr\", \"unit_pr\", \"unit_order_price\", \"order_price\", \"con_order\", \"con_sell\", \"con_pr\", \"barcode\", \"supplied_by\" ) "
				." VALUES ('".$object->pr_detail_item."','".$object->request_no."','".$object->item_code."','".pg_escape_string($object->description)."','".$object->qty_order."','"
				.$object->unit_order."','".$object->qty_sell."','".$object->unit_sell."','".$object->qty_pr."','".$object->unit_pr."','".$object->unit_order_price."','"
				.$object->order_price."','".$object->con_order."','".$object->con_sell."','".$object->con_pr."','".$object->barcode."','".$object->supplied_by."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_pr_detail\" SET ";
		$flag = false;
		if($object->item_code != '') { if($flag){ $sql .= ","; } $sql .= " \"item_code\" = '".$object->item_code."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".pg_escape_string($object->description)."' "; $flag = true; }
		if($object->qty_order != '') { if($flag){ $sql .= ","; } $sql .= " \"qty_order\" = '".$object->qty_order."' "; $flag = true; }
		if($object->unit_order != '') { if($flag){ $sql .= ","; } $sql .= " \"unit_order\" = '".$object->unit_order."' "; $flag = true; }
		if($object->qty_sell != '') { if($flag){ $sql .= ","; } $sql .= " \"qty_sell\" = '".$object->qty_sell."' "; $flag = true; }
		if($object->unit_sell != '') { if($flag){ $sql .= ","; } $sql .= " \"unit_sell\" = '".$object->unit_sell."' "; $flag = true; }
		if($object->qty_pr != '') { if($flag){ $sql .= ","; } $sql .= " \"qty_pr\" = '".$object->qty_pr."' "; $flag = true; }
		if($object->unit_pr != '') { if($flag){ $sql .= ","; } $sql .= " \"unit_pr\" = '".$object->unit_pr."' "; $flag = true; }
		if($object->unit_order_price != '') { if($flag){ $sql .= ","; } $sql .= " \"unit_order_price\" = '".$object->unit_order_price."' "; $flag = true; }
		if($object->order_price != '') { if($flag){ $sql .= ","; } $sql .= " \"order_price\" = '".$object->order_price."' "; $flag = true; }
		if($object->con_order != '') { if($flag){ $sql .= ","; } $sql .= " \"con_order\" = '".$object->con_order."' "; $flag = true; }
		if($object->con_sell != '') { if($flag){ $sql .= ","; } $sql .= " \"con_sell\" = '".$object->con_sell."' "; $flag = true; }
		if($object->con_pr != '') { if($flag){ $sql .= ","; } $sql .= " \"con_pr\" = '".$object->con_pr."' "; $flag = true; }
		if($object->barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"barcode\" = '".$object->barcode."' "; $flag = true; }
		if($object->supplied_by != '') { if($flag){ $sql .= ","; } $sql .= " \"supplied_by\" = '".$object->supplied_by."' "; $flag = true; }
		$sql .= " WHERE \"request_no\" = '".$object->request_no."' ";	
		$sql .= " AND \"pr_detail_item\" = '".$object->pr_detail_item."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
	
		$sql = " DELETE FROM \"t_pr_detail\" "
				." WHERE \"request_no\" = '".$object->request_no."' "
				." WHERE \"pr_detail_item\" = '".$object->pr_detail_item."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goDeletePR($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_pr_detail\" "
				." WHERE \"request_no\" = '".$object->request_no."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
