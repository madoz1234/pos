<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Adj extends Model
{

	public $adj_id;
	public $create_by;
	public $create_date;
	public $status;	
	public $adj_type;
	public $adj_detail;
	public $comment;
	public $flag_post;
	public $flag_delete;

	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"adj_id\", \"create_by\", \"create_date\", \"status\", \"adj_type\", \"adj_detail\", \"comment\", \"flag_post\", \"flag_delete\" "
				." FROM \"t_adj\" "
				." WHERE \"flag_delete\" = false ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['adj_id'] = $result[0];
			$lists[$count]['create_by'] = $result[1];
			$lists[$count]['create_date'] = $result[2];
			$lists[$count]['status'] = $result[3];
			$lists[$count]['adj_type'] = $result[4];
			$lists[$count]['adj_detail'] = $result[5];
			$lists[$count]['comment'] = $result[6];
			$lists[$count]['flag_post'] = $result[7];
			$lists[$count]['flag_delete'] = $result[8];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"adj_id\", \"create_by\", \"create_date\", \"status\", \"adj_type\", \"adj_detail\", \"comment\", \"flag_post\", \"flag_delete\" "
				." FROM \"t_adj\" "
				." WHERE \"adj_id\" = '".$object->adj_id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['adj_id'] = $result[0];
			$lists[$count]['create_by'] = $result[1];
			$lists[$count]['create_date'] = $result[2];
			$lists[$count]['status'] = $result[3];
			$lists[$count]['adj_type'] = $result[4];
			$lists[$count]['adj_detail'] = $result[5];
			$lists[$count]['comment'] = $result[6];
			$lists[$count]['flag_post'] = $result[7];
			$lists[$count]['flag_delete'] = $result[8];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"adj_id\", \"create_by\", \"create_date\", \"status\", \"adj_type\", \"adj_detail\", \"comment\", \"flag_post\", \"flag_delete\" "
				." FROM \"t_adj\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['adj_id'] = $result[0];
			$lists[$count]['create_by'] = $result[1];
			$lists[$count]['create_date'] = $result[2];
			$lists[$count]['status'] = $result[3];
			$lists[$count]['adj_type'] = $result[4];
			$lists[$count]['adj_detail'] = $result[5];
			$lists[$count]['comment'] = $result[6];
			$lists[$count]['flag_post'] = $result[7];
			$lists[$count]['flag_delete'] = $result[8];
			
			$count++;
		}
		
		return $lists;
	}

	public function getFreeSQL2($cond){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"adj_id\", \"create_by\", \"create_date\", \"status\", \"adj_type\", \"adj_detail\", \"comment\", \"flag_post\", \"flag_delete\" "
				." FROM \"t_adj\" ".$cond;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['adj_id'] = $result[0];
			$lists[$count]['create_by'] = $result[1];
			$lists[$count]['create_date'] = $result[2];
			$lists[$count]['status'] = $result[3];
			$lists[$count]['adj_type'] = $result[4];
			$lists[$count]['adj_detail'] = $result[5];
			$lists[$count]['comment'] = $result[6];
			$lists[$count]['flag_post'] = $result[7];
			$lists[$count]['flag_delete'] = $result[8];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"adj_id\") "
				." FROM \"t_adj\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->create_date == '') { $object->create_date = date("Y-m-d"); }
		if($object->flag_post == '') { $object->flag_post = 'f'; }
		if($object->flag_delete == '') { $object->flag_delete = 'f'; }
		
		$sql = "INSERT INTO \"t_adj\" (\"adj_id\", \"create_by\", \"create_date\", \"status\", \"adj_type\", \"adj_detail\", \"comment\", \"flag_post\", \"flag_delete\") "
				." VALUES ('".$object->adj_id."','".$object->create_by."','".$object->create_date."','".$object->status."','".$object->adj_type."','"
				.$object->adj_detail."','".$object->comment."','".$object->flag_post."','".$object->flag_delete."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_adj\" SET ";
		$flag = false;
		if($object->create_by != '') { if($flag){ $sql .= ","; } $sql .= " \"create_by\" = '".$object->create_by."' "; $flag = true; }
		if($object->create_date != '') { if($flag){ $sql .= ","; } $sql .= " \"create_date\" = '".$object->create_date."' "; $flag = true; }
		if($object->status != '') { if($flag){ $sql .= ","; } $sql .= " \"status\" = '".$object->status."' "; $flag = true; }
		if($object->adj_type != '') { if($flag){ $sql .= ","; } $sql .= " \"adj_type\" = '".$object->adj_type."' "; $flag = true; }
		if($object->adj_detail != '') { if($flag){ $sql .= ","; } $sql .= " \"adj_detail\" = '".$object->adj_detail."' "; $flag = true; }
		if($object->comment != '') { if($flag){ $sql .= ","; } $sql .= " \"comment\" = '".$object->comment."' "; $flag = true; }
		if($object->flag_post != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_post\" = '".$object->flag_post."' "; $flag = true; }
		if($object->flag_delete != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_delete\" = '".$object->flag_delete."' "; $flag = true; }
		$sql .= " WHERE \"adj_id\" = '".$object->adj_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_adj\" "
				." WHERE \"adj_id\" = '".$object->adj_id."' ";
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function getJoin_AdjDetail($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT a.\"adj_id\", \"create_by\", \"create_date\", \"status\", \"adj_type\", \"adj_detail\", \"comment\", \"flag_post\", \"flag_delete\", \"item_code\", \"description\", \"qty\", \"unit\", \"unit_price\", \"total_price\", \"barcode_code\" "
			." FROM \"t_adj\" a INNER JOIN \"t_adj_detail\" ad ON a.adj_id = ad.adj_id ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['adj_id'] = $result[0];
			$lists[$count]['create_by'] = $result[1];
			$lists[$count]['create_date'] = $result[2];
			$lists[$count]['status'] = $result[3];
			$lists[$count]['adj_type'] = $result[4];
			$lists[$count]['adj_detail'] = $result[5];
			$lists[$count]['comment'] = $result[6];
			$lists[$count]['flag_post'] = $result[7];
			$lists[$count]['flag_delete'] = $result[8];
			$lists[$count]['item_code'] = $result[9];
			$lists[$count]['description'] = $result[10];
			$lists[$count]['qty'] = $result[11];
			$lists[$count]['unit'] = $result[12];
			$lists[$count]['unit_price'] = $result[13];
			$lists[$count]['total_price'] = $result[14];
			$lists[$count]['barcode_code'] = $result[15];
			
			$count++;
		}
		
		return $lists;
	}
}
