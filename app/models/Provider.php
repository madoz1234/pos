<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Provider extends Model
{	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT provider_name, card_number "
				." FROM m_provider ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['provider_name'] = $result[0];
			$lists[$count]['card_number'] = $result[1];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT provider_name, card_number "
				." FROM m_provider ".$condition;		
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['provider_name'] = $result[0];
			$lists[$count]['card_number'] = $result[1];	
			
			$count++;
		}
		
		return $lists;
	}
}
