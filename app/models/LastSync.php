<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class LastSync extends Model
{

	public $id;
	public $tipe;
	public $sync_date;	
	public $sync_time;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"tipe\", \"sync_date\", \"sync_time\" "
				." FROM \"m_last_sync\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['tipe'] = $result[1];
			$lists[$count]['sync_date'] = $result[2];
			$lists[$count]['sync_time'] = $result[3];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"tipe\", \"sync_date\", \"sync_time\" "
				." FROM \"m_last_sync\" "
				." WHERE \"tipe\" = '".$object->tipe."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['tipe'] = $result[1];
			$lists[$count]['sync_date'] = $result[2];
			$lists[$count]['sync_time'] = $result[3];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"tipe\", \"sync_date\", \"sync_time\" "
				." FROM \"m_last_sync\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['tipe'] = $result[1];
			$lists[$count]['sync_date'] = $result[2];
			$lists[$count]['sync_time'] = $result[3];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"id\") "
				." FROM \"m_last_sync\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->sync_date == '') { $object->sync_date = date('Y-m-d'); }
		if($object->sync_time == '') { $object->sync_time = time(); }
		
		$sql = "INSERT INTO \"m_last_sync\" (\"id\", \"tipe\", \"sync_date\", \"sync_time\") "
				." VALUES ('".$object->id."','".$object->tipe."','".$object->sync_date."','".$object->sync_time."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_last_sync\" SET ";
		$flag = false;
		if($object->sync_date != '') { if($flag){ $sql .= ","; } $sql .= " \"sync_date\" = '".$object->sync_date."' "; $flag = true; }
		if($object->sync_time != '') { if($flag){ $sql .= ","; } $sql .= " \"sync_time\" = '".$object->sync_time."' "; $flag = true; }
		$sql .= " WHERE \"tipe\" = '".$object->tipe."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_last_sync\" "
				." WHERE \"tipe\" = '".$object->tipe."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
