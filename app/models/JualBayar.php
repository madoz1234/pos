<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class JualBayar extends Model
{

	public $id_bayar_item;
	public $id_jual;
	public $tipe;
	public $bank_id;	
	public $bank_name;
	public $total;
	public $no_kartu;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id_bayar_item\", \"id_jual\", \"tipe\", \"bank_id\", \"bank_name\", \"total\", \"no_kartu\" "
				." FROM \"t_jual_bayar\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_bayar_item'] = $result[0];
			$lists[$count]['id_jual'] = $result[1];
			$lists[$count]['tipe'] = $result[2];
			$lists[$count]['bank_id'] = $result[3];
			$lists[$count]['bank_name'] = $result[4];			
			$lists[$count]['total'] = $result[5];	
			$lists[$count]['no_kartu'] = $result[6];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id_bayar_item\", \"id_jual\", \"tipe\", \"bank_id\", \"bank_name\", \"total\", \"no_kartu\" "
				." FROM \"t_jual_bayar\" "
				." WHERE \"id_bayar_item\" = '".$object->id_bayar_item."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_bayar_item'] = $result[0];
			$lists[$count]['id_jual'] = $result[1];
			$lists[$count]['tipe'] = $result[2];
			$lists[$count]['bank_id'] = $result[3];
			$lists[$count]['bank_name'] = $result[4];			
			$lists[$count]['total'] = $result[5];	
			$lists[$count]['no_kartu'] = $result[6];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id_bayar_item\", \"id_jual\", \"tipe\", \"bank_id\", \"bank_name\", \"total\", \"no_kartu\" "
				." FROM \"t_jual_bayar\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_bayar_item'] = $result[0];
			$lists[$count]['id_jual'] = $result[1];
			$lists[$count]['tipe'] = $result[2];
			$lists[$count]['bank_id'] = $result[3];
			$lists[$count]['bank_name'] = $result[4];			
			$lists[$count]['total'] = $result[5];	
			$lists[$count]['no_kartu'] = $result[6];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"id_bayar_item\") "
				." FROM \"t_jual_bayar\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->total == '') { $object->total = '0'; }
		
		$sql = "INSERT INTO \"t_jual_bayar\" (\"id_jual\", \"tipe\", \"bank_id\", \"bank_name\", \"total\", \"no_kartu\") "
				." VALUES ('".$object->id_jual."','".$object->tipe."','".$object->bank_id."','".$object->bank_name."','".$object->total."','"
				.$object->no_kartu."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}

	public function goInsert2($object){
		$connection = new Postgresql($this->di['db']);

		if($object->total == '') { $object->total = '0'; }
		
		$sql = "INSERT INTO \"t_jual_bayar\" (\"id_jual\", \"tipe\", \"bank_id\", \"bank_name\", \"total\", \"no_kartu\") "
				." VALUES ('".$object->id_jual."','".$object->tipe."','".$object->bank_id."','".$object->bank_name."','".$object->total."','"
				.$object->no_kartu."'); ";
		
		return $sql;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_jual_bayar\" SET ";
		$flag = false;
		if($object->tipe != '') { if($flag){ $sql .= ","; } $sql .= " \"tipe\" = '".$object->tipe."' "; $flag = true; }
		if($object->bank_id != '') { if($flag){ $sql .= ","; } $sql .= " \"bank_id\" = '".$object->bank_id."' "; $flag = true; }
		if($object->bank_name != '') { if($flag){ $sql .= ","; } $sql .= " \"bank_name\" = '".$object->bank_name."' "; $flag = true; }
		if($object->total != '') { if($flag){ $sql .= ","; } $sql .= " \"total\" = '".$object->total."' "; $flag = true; }
		if($object->no_kartu != '') { if($flag){ $sql .= ","; } $sql .= " \"no_kartu\" = '".$object->no_kartu."' "; $flag = true; }	
		$sql .= " WHERE \"id_bayar_item\" = '".$object->id_bayar_item."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goUpdateJual($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_jual_bayar\" SET ";
		$flag = false;
		if($object->tipe != '') { if($flag){ $sql .= ","; } $sql .= " \"tipe\" = '".$object->tipe."' "; $flag = true; }
		if($object->bank_id != '') { if($flag){ $sql .= ","; } $sql .= " \"bank_id\" = '".$object->bank_id."' "; $flag = true; }
		if($object->bank_name != '') { if($flag){ $sql .= ","; } $sql .= " \"bank_name\" = '".$object->bank_name."' "; $flag = true; }
		if($object->total != '') { if($flag){ $sql .= ","; } $sql .= " \"total\" = '".$object->total."' "; $flag = true; }
		if($object->no_kartu != '') { if($flag){ $sql .= ","; } $sql .= " \"no_kartu\" = '".$object->no_kartu."' "; $flag = true; }	
		$sql .= " WHERE \"id_jual\" = '".$object->id_jual."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_jual_bayar\" "
				." WHERE \"id_bayar_item\" = '".$object->id_bayar_item."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goDeleteJual($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_jual_bayar\" "
				." WHERE \"id_jual\" = '".$object->id_jual."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function summarize($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT id_kasir, kassa_id, shift_id, tanggal, tipe, SUM(tjb.\"total\") as total_sum, bank_name "
			." FROM \"t_jual_bayar\" tjb INNER JOIN \"t_jual\" tj ON tjb.id_jual = tj.id_jual ".$condition;
			
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_kasir'] = $result[0];
			$lists[$count]['kassa_id'] = $result[1];
			$lists[$count]['shift_id'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];
			$lists[$count]['tipe'] = $result[4];
			$lists[$count]['total_sum'] = $result[5];
			$lists[$count]['bank_name'] = $result[6];
			$count++;
		}
		
		return $lists;
	}
}
