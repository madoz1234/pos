<?php
/**
 * ELS\Model\Validator\MultiUniqueness
 *
 * Allows to validate if model is unique across selected fields
 */
use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class MultiUniqueness extends \Phalcon\Mvc\Model\Validator
{

    /**
     * Executes the validator
     *
     * @param \Phalcon\Mvc\ModelInterface $record
     * @return boolean
     */
    public function validate($record)
    {
        $fields   = (array) $this->getOption('field');
        $excludes = (array) $this->getOption('exclude');

        if (array_intersect($fields, $excludes))
            throw new \InvalidArgumentException('Trying to exclude fields already used to uniqueness');

        $query = $record->query();
        $binds = [];

        // Add fields which must be unique
        foreach($fields as $field)
        {
            if (!empty($field))
            {
                $query->andWhere("$field = :$field:");
                $binds[$field] = $record->$field;
            }
        }

        // Add fields which must should be excluded from the check
        foreach($excludes as $field)
        {
            if (!empty($field) && !empty($record->$field))
            {
                $query->andWhere("$field <> :$field:");
                $binds[$field] = $record->$field;
            }
        }

        if ($query->bind($binds)->execute()->count())
        {
            $message = $this->getOption('message')? $this->getOption('message') : 'Value must be unique';

            foreach($fields as $field)
            {
                $this->appendMessage($message, $field, 'MultiUniqueness');
            }
            return false;
        }
        return true;
    }

}