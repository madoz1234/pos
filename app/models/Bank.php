<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Bank extends Model
{

	public $bank_id;
	public $bank_name;
	public $inactive;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"bank_id\", \"bank_name\", \"inactive\" "
				." FROM \"m_bank\" "
				." WHERE \"inactive\" != 1 ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['bank_id'] = $result[0];
			$lists[$count]['bank_name'] = $result[1];
			$lists[$count]['inactive'] = $result[2];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"bank_id\", \"bank_name\", \"inactive\" "
				." FROM \"m_bank\" "
				." WHERE \"bank_id\" = '".$object->bank_id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['bank_id'] = $result[0];
			$lists[$count]['bank_name'] = $result[1];
			$lists[$count]['inactive'] = $result[2];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"bank_id\", \"bank_name\", \"inactive\" "
				." FROM \"m_bank\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['bank_id'] = $result[0];
			$lists[$count]['bank_name'] = $result[1];
			$lists[$count]['inactive'] = $result[2];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"bank_id\") "
				." FROM \"m_bank\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->inactive == '') { $object->inactive = '0'; }
		
		$sql = "INSERT INTO \"m_bank\" (\"bank_id\",\"bank_name\", \"inactive\") "
				." VALUES ('".$object->bank_id."','".$object->bank_name."','".$object->inactive."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goMultiInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "DELETE FROM m_bank; INSERT INTO \"m_bank\" (\"bank_id\", \"bank_name\") "
			." VALUES ".$object;
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_bank\" SET ";
		$flag = false;
		if($object->bank_name != '') { if($flag){ $sql .= ","; } $sql .= " \"bank_name\" = '".$object->bank_name."' "; $flag = true; }
		if($object->inactive != NULL) { if($flag){ $sql .= ","; } $sql .= " \"inactive\" = '".$object->inactive."' "; $flag = true; }
		$sql .= " WHERE \"bank_id\" = '".$object->bank_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_bank\" "
				." WHERE \"bank_id\" = '".$object->bank_id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goMultiDelete($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " DELETE FROM \"m_bank\" "
				." WHERE \"bank_id\" IN (".$object.") ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
}
