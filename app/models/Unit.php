<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Unit extends Model
{

	public $unit;
	public $name;
	public $decimals;
	public $inactive;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"unit\", \"name\", \"decimals\", \"inactive\" "
				." FROM m_unit "
				." WHERE inactive != 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['unit'] = $result[0];
			$lists[$count]['name'] = $result[1];
			$lists[$count]['decimals'] = $result[2];
			$lists[$count]['inactive'] = $result[3];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"unit\", \"name\", \"decimals\", \"inactive\" "
				." FROM m_unit "
				." WHERE unit = '".$object->unit."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['unit'] = $result[0];
			$lists[$count]['name'] = $result[1];
			$lists[$count]['decimals'] = $result[2];
			$lists[$count]['inactive'] = $result[3];		
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"unit\", \"name\", \"decimals\", \"inactive\" "
				." FROM m_unit ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['unit'] = $result[0];
			$lists[$count]['name'] = $result[1];
			$lists[$count]['decimals'] = $result[2];
			$lists[$count]['inactive'] = $result[3];		
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"unit\") "
				." FROM \"m_unit\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->inactive == '') { $object->inactive = '0'; }
		$sql = "INSERT INTO \"m_unit\" (\"unit\", \"name\", \"decimals\", \"inactive\") "
				." VALUES ('".$object->unit."','".$object->name."','".$object->decimals."','".$object->inactive."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->inactive == '') { $object->inactive = '0'; }
		
		$sql = " UPDATE \"m_unit\" SET ";
		$flag = false;
		if($object->name != '') { if($flag){ $sql .= ","; } $sql .= " \"name\" = '".$object->name."' "; $flag = true; }
		if($object->decimals != '') { if($flag){ $sql .= ","; } $sql .= " \"decimals\" = '".$object->decimals."' "; $flag = true; }
		if($object->inactive != '') { if($flag){ $sql .= ","; } $sql .= " \"inactive\" = '".$object->inactive."' "; $flag = true; }
		$sql .= " WHERE \"unit\" = '".$object->unit."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_unit\" "
				." WHERE \"unit\" = '".$object->unit."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
