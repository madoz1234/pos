<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Diskon extends Model
{

	public $promosi_id;
	public $kode_produk;
	public $cost_price;
	public $harga_terdiskon;	
	public $diskon;
	public $flag;

	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"promosi_id\", \"kode_produk\", \"cost_price\", \"harga_terdiskon\", \"diskon\", \"flag\" "
				." FROM \"m_promosi_diskon\" "
				." WHERE \"flag_delete\" = false ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['promosi_id'] = $result[0];
			$lists[$count]['kode_produk'] = $result[1];
			$lists[$count]['cost_price'] = $result[2];
			$lists[$count]['harga_terdiskon'] = $result[3];
			$lists[$count]['diskon'] = $result[4];
			$lists[$count]['flag'] = $result[5];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"promosi_id\", \"kode_produk\", \"cost_price\", \"harga_terdiskon\", \"diskon\", \"flag\" "
				." WHERE \"promosi_id\" = '".$object->promosi_id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['promosi_id'] = $result[0];
			$lists[$count]['kode_produk'] = $result[1];
			$lists[$count]['cost_price'] = $result[2];
			$lists[$count]['harga_terdiskon'] = $result[3];
			$lists[$count]['diskon'] = $result[4];
			$lists[$count]['flag'] = $result[5];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"promosi_id\", \"kode_produk\", \"cost_price\", \"harga_terdiskon\", \"diskon\", \"flag\" "
				." FROM \"m_promosi_diskon\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['promosi_id'] = $result[0];
			$lists[$count]['kode_produk'] = $result[1];
			$lists[$count]['cost_price'] = $result[2];
			$lists[$count]['harga_terdiskon'] = $result[3];
			$lists[$count]['diskon'] = $result[4];
			$lists[$count]['flag'] = $result[5];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"promosi_id\") "
				." FROM \"m_promosi_diskon\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		if($object->cost_price == '') { $object->cost_price = date("Y-m-d"); }
		
		$sql = "INSERT INTO \"m_promosi_diskon\" (\"promosi_id\", \"kode_produk\", \"cost_price\", \"harga_terdiskon\", \"diskon\", \"flag\") "
				." VALUES ('".$object->promosi_id."','".$object->kode_produk."','".$object->cost_price."','".$object->harga_terdiskon."','".$object->diskon."','".$object->flag."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		$sql = " UPDATE \"m_promosi_diskon\" SET ";
		$flag = false;
		if($object->kode_produk != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_produk\" = '".$object->kode_produk."' "; $flag = true; }
		if($object->cost_price != '') { if($flag){ $sql .= ","; } $sql .= " \"cost_price\" = '".$object->cost_price."' "; $flag = true; }
		if($object->harga_terdiskon != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_terdiskon\" = '".$object->harga_terdiskon."' "; $flag = true; }
		if($object->diskon != '') { if($flag){ $sql .= ","; } $sql .= " \"diskon\" = '".$object->diskon."' "; $flag = true; }
		if($object->flag != '') { if($flag){ $sql .= ","; } $sql .= " \"flag\" = '".$object->flag."' "; $flag = true; }
		$sql .= " WHERE \"promosi_id\" = '".$object->promosi_id."' AND \"kode_produk\" = '".$object->kode_produk."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}

	public function goUpdate2($object){
		$connection = new Postgresql($this->di['db']);
		$sql = " UPDATE \"m_promosi_diskon\" SET ";
		$flag = false;
		if($object->kode_produk != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_produk\" = '".$object->kode_produk."' "; $flag = true; }
		if($object->cost_price != '') { if($flag){ $sql .= ","; } $sql .= " \"cost_price\" = '".$object->cost_price."' "; $flag = true; }
		if($object->harga_terdiskon != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_terdiskon\" = '".$object->harga_terdiskon."' "; $flag = true; }
		if($object->diskon != '') { if($flag){ $sql .= ","; } $sql .= " \"diskon\" = '".$object->diskon."' "; $flag = true; }
		if($object->flag != '') { if($flag){ $sql .= ","; } $sql .= " \"flag\" = '".$object->flag."' "; $flag = true; }
		$sql .= " WHERE \"promosi_id\" = '".$object->promosi_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_promosi_diskon\" "
				." WHERE \"promosi_id\" = '".$object->promosi_id."' ";
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function getJoin_AdjDetail($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT a.\"promosi_id\", \"kode_produk\", \"cost_price\", \"harga_terdiskon\", \"diskon\", \"flag\" "
			." FROM \"m_promosi_diskon\" a INNER JOIN \"m_promosi_diskon_detail\" ad ON a.promosi_id = ad.promosi_id ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['promosi_id'] = $result[0];
			$lists[$count]['kode_produk'] = $result[1];
			$lists[$count]['cost_price'] = $result[2];
			$lists[$count]['harga_terdiskon'] = $result[3];
			$lists[$count]['diskon'] = $result[4];
			$lists[$count]['flag'] = $result[5];
			
			$count++;
		}
		
		return $lists;
	}
}
