<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Cust extends Model
{

	public $debtor_no;
	public $name;
	public $debtor_ref;
	public $address;	
	public $tax_id;
	public $curr_code;
	public $discount;	
	public $pymt_discount;
	public $credit_limit;
	public $notes;
	public $inactive;
	public $nomor_member;
	
	// yg lama
	// public function getAll(){
	// 	$connection = new Postgresql($this->di['db']);
		
	// 	$sql = "SELECT \"debtor_no\", \"name\", \"debtor_ref\", \"address\", \"tax_id\", \"curr_code\", \"discount\", \"pymt_discount\" "
	// 			.", \"credit_limit\", \"notes\", \"inactive\", \"nomor_member\" "
	// 			." FROM \"m_cust\" "
	// 			." WHERE \"inactive\" != 1 ";
		
	// 	$results = $connection->query($sql);
		
	// 	$count = 0;
	// 	$lists = null;
	// 	$results->setFetchMode(Phalcon\Db::FETCH_NUM);
	// 	while ($result = $results->fetchArray()) {
	// 		$lists[$count]['debtor_no'] = $result[0];
	// 		$lists[$count]['name'] = $result[1];
	// 		$lists[$count]['debtor_ref'] = $result[2];
	// 		$lists[$count]['address'] = $result[3];
	// 		$lists[$count]['tax_id'] = $result[4];			
	// 		$lists[$count]['curr_code'] = $result[5];	
	// 		$lists[$count]['discount'] = $result[6];	
	// 		$lists[$count]['pymt_discount'] = $result[7];
	// 		$lists[$count]['credit_limit'] = $result[8];
	// 		$lists[$count]['notes'] = $result[9];
	// 		$lists[$count]['inactive'] = $result[10];
	// 		$lists[$count]['nomor_member'] = $result[11];
			
	// 		$count++;
	// 	}
		
	// 	return $lists;
	// }

	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"debtor_no\", \"name\", \"debtor_ref\", \"address\", \"tax_id\", \"curr_code\", \"discount\", \"pymt_discount\", \"credit_limit\", \"notes\", \"inactive\", \"nomor_member\" FROM \"m_cust\" ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['debtor_no'] = $result[0];
			$lists[$count]['name'] = $result[1];
			$lists[$count]['debtor_ref'] = $result[2];
			$lists[$count]['address'] = $result[3];
			$lists[$count]['tax_id'] = $result[4];			
			$lists[$count]['curr_code'] = $result[5];	
			$lists[$count]['discount'] = $result[6];	
			$lists[$count]['pymt_discount'] = $result[7];
			$lists[$count]['credit_limit'] = $result[8];
			$lists[$count]['notes'] = $result[9];
			$lists[$count]['inactive'] = $result[10];
			$lists[$count]['nomor_member'] = $result[11];
			
			$count++;
		}
		
		return $lists;
	}

	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"debtor_no\", \"name\", \"debtor_ref\", \"address\", \"tax_id\", \"curr_code\", \"discount\", \"pymt_discount\" "
				.", \"credit_limit\", \"notes\", \"inactive\", \"nomor_member\" "
				." FROM \"m_cust\" "
				." WHERE \"debtor_no\" = '".$object->debtor_no."' "
				." LIMIT 1 ";

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['debtor_no'] = $result[0];
			$lists[$count]['name'] = $result[1];
			$lists[$count]['debtor_ref'] = $result[2];
			$lists[$count]['address'] = $result[3];
			$lists[$count]['tax_id'] = $result[4];			
			$lists[$count]['curr_code'] = $result[5];	
			$lists[$count]['discount'] = $result[6];	
			$lists[$count]['pymt_discount'] = $result[7];
			$lists[$count]['credit_limit'] = $result[8];
			$lists[$count]['notes'] = $result[9];
			$lists[$count]['inactive'] = $result[10];
			$lists[$count]['nomor_member'] = $result[11];			
			
			$count++;
		}
		
		return $lists;
	}
	
	// yang lama
	// public function getFreeSQL($condition){
	// 	$connection = new Postgresql($this->di['db']);
		
	// 	$sql = "SELECT \"debtor_no\", \"name\", \"debtor_ref\", \"address\", \"tax_id\", \"curr_code\", \"discount\", \"pymt_discount\" "
	// 			.", \"credit_limit\", \"notes\", \"inactive\", \"nomor_member\" "
	// 			." FROM \"m_cust\" ".$condition;
		
	// 	$results = $connection->query($sql);
		
	// 	$count = 0;
	// 	$lists = null;
	// 	$results->setFetchMode(Phalcon\Db::FETCH_NUM);
	// 	while ($result = $results->fetchArray()) {
	// 		$lists[$count]['debtor_no'] = $result[0];
	// 		$lists[$count]['name'] = $result[1];
	// 		$lists[$count]['debtor_ref'] = $result[2];
	// 		$lists[$count]['address'] = $result[3];
	// 		$lists[$count]['tax_id'] = $result[4];			
	// 		$lists[$count]['curr_code'] = $result[5];	
	// 		$lists[$count]['discount'] = $result[6];	
	// 		$lists[$count]['pymt_discount'] = $result[7];
	// 		$lists[$count]['credit_limit'] = $result[8];
	// 		$lists[$count]['notes'] = $result[9];
	// 		$lists[$count]['inactive'] = $result[10];	
	// 		$lists[$count]['nomor_member'] = $result[11];
			
	// 		$count++;
	// 	}
		
	// 	return $lists;
	// }

	public function getFreeSQL($condition){

		// dd($condition);
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"debtor_no\", \"name\", \"debtor_ref\", \"address\", \"tax_id\", \"curr_code\", \"discount\", \"pymt_discount\", \"credit_limit\", \"notes\", \"inactive\", \"nomor_member\" FROM \"m_cust\" ".$condition;
		// dd($sql);

		$results = $connection->query($sql);

		// dd($results);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['debtor_no'] = $result[0];
			$lists[$count]['name'] = $result[1];
			$lists[$count]['debtor_ref'] = $result[3];
			$lists[$count]['address'] = $result[4];
			$lists[$count]['tax_id'] = $result[5];
			$lists[$count]['curr_code'] = $result[6];
			$lists[$count]['discount'] = $result[7];
			$lists[$count]['pymt_discount'] = $result[8];
			$lists[$count]['credit_limit'] = $result[9];
			$lists[$count]['notes'] = $result[10];
			$lists[$count]['inactive'] = $result[12];
			$lists[$count]['nomor_member'] = $result[13];
			
			$count++;
		}
		
		return $lists;
	}

	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"debtor_no\") "
				." FROM \"m_cust\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	// public function goInsert($object){
	// 	$connection = new Postgresql($this->di['db']);
		
	// 	if($object->discount == '') { $object->discount = '0'; }
	// 	if($object->pymt_discount == '') { $object->pymt_discount = '0'; }		
	// 	if($object->credit_limit == '') { $object->credit_limit = '0'; } 
	// 	if($object->inactive == '') { $object->inactive = '0'; } 
		
	// 	$sql = "INSERT INTO \"m_cust\" (\"name\", \"debtor_ref\", \"address\", \"tax_id\", \"curr_code\", \"discount\", \"pymt_discount\", \"credit_limit\" "
	// 			.", \"notes\", \"inactive\", \"nomor_member\") "
	// 			." VALUES ('".$object->name."','".$object->debtor_ref."','".$object->address."','".$object->tax_id
	// 			."','".$object->curr_code."','".$object->discount."','".$object->pymt_discount."','".$object->credit_limit
	// 			."','".$object->notes."','".$object->inactive."','".$object->nomor_member."') ";
		
	// 	$success = $connection->execute($sql);
	// 	$id = $connection->lastInsertId();
		
	// 	return $sql;
	// }

	public function goInsert($object){

		// dd($object);
		$connection = new Postgresql($this->di['db']);	
		
		$sql = "INSERT INTO \"m_cust\" (\"debtor_no\", \"name\", \"debtor_ref\", \"address\", \"tax_id\", \"curr_code\", \"discount\", \"pymt_discount\", \"credit_limit\", \"notes\", \"inactive\", \"nomor_member\") "
				." VALUES ('".$object->debtor_no."', '".$object->name."','".$object->debtor_ref."','".$object->address."','".$object->tax_id."','".$object->curr_code."','".$object->discount."','".$object->pymt_discount."','".$object->credit_limit."','".$object->notes."','".$object->inactive."','".$object->nomor_member."') ";
		// dd($sql);
		$success = $connection->execute($sql);		
		
		return $sql;
	}

	
	public function goMultiInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "INSERT INTO \"m_cust\" (\"debtor_no\", \"name\", \"debtor_ref\", \"address\", \"tax_id\", \"curr_code\", \"discount\", \"pymt_discount\", \"credit_limit\" "
				.", \"notes\", \"inactive\", \"nomor_member\") "
				." VALUES ".$object;
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_cust\" SET ";
		$flag = false;		
		if($object->name != '') { if($flag){ $sql .= ","; } $sql .= " \"name\" = '".$object->name."' "; $flag = true; }
		// if($object->debtor_ref != '') { if($flag){ $sql .= ","; } $sql .= " \"debtor_ref\" = '".$object->debtor_ref."' "; $flag = true; }
		if($object->address != '') { if($flag){ $sql .= ","; } $sql .= " \"address\" = '".$object->address."' "; $flag = true; }
		if($object->tax_id != '') { if($flag){ $sql .= ","; } $sql .= " \"tax_id\" = '".$object->tax_id."' "; $flag = true; }
		if($object->curr_code != '') { if($flag){ $sql .= ","; } $sql .= " \"curr_code\" = '".$object->curr_code."' "; $flag = true; }
		if($object->discount != '') { if($flag){ $sql .= ","; } $sql .= " \"discount\" = '".$object->discount."' "; $flag = true; }
		if($object->pymt_discount != '') { if($flag){ $sql .= ","; } $sql .= " \"pymt_discount\" = '".$object->pymt_discount."' "; $flag = true; }
		if($object->credit_limit != '') { if($flag){ $sql .= ","; } $sql .= " \"credit_limit\" = '".$object->credit_limit."' "; $flag = true; }
		if($object->notes != '') { if($flag){ $sql .= ","; } $sql .= " \"notes\" = '".$object->notes."' "; $flag = true; }
		if($object->inactive != '') { if($flag){ $sql .= ","; } $sql .= " \"inactive\" = '".$object->inactive."' "; $flag = true; }
		if($object->nomor_member != '') { if($flag){ $sql .= ","; } $sql .= " \"nomor_member\" = '".$object->nomor_member."' "; $flag = true; }
		$sql .= " WHERE \"debtor_ref\" = '".$object->debtor_ref."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}

	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_cust\" "
				." WHERE \"debtor_no\" = '".$object->debtor_no."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goMultiDelete($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " DELETE FROM \"m_cust\" "
				." WHERE \"debtor_no\" IN (".$object.") ";
		
		$success = $connection->execute($sql);	
		
		return $success;
	}
	
	public function getCustomerManagement(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT a.\"debtor_no\", a.\"name\", a.\"debtor_ref\", a.\"address\", a.\"tax_id\", a.\"curr_code\", a.\"discount\", a.\"pymt_discount\" "
				.", a.\"credit_limit\", a.\"notes\", a.\"inactive\", a.\"nomor_member\", b.\"total_order\", b.\"total_purchase\", b.\"last_visit\",
				CASE WHEN b.\"total_order\" > 0 THEN (b.\"total_purchase\" / b.\"total_order\") ELSE 0 END,
				CEIL( b.\"total_order\" / CASE WHEN ((abs(date_part('year',age(now(), b.\"first_visit\")))*12) + abs(date_part('month',age(now(), b.\"first_visit\")))) > 0
					THEN ((abs(date_part('year',age(now(), b.\"first_visit\")))*12) + abs(date_part('month',age(now(), b.\"first_visit\"))))
					ELSE 1
				END ),  
				CEIL( b.\"total_order\" / CASE WHEN abs(date_part('year',age(now(), b.\"first_visit\"))) > 0
					THEN abs(date_part('year',age(now(), b.\"first_visit\")))
					ELSE 1
				END ),
				c.\"point\" "
				." FROM \"m_cust\" a left join (  
					SELECT \"id_member\", count(\"id_jual\") as \"total_order\", sum(\"total\") as \"total_purchase\", 
					max(\"tanggal\") as \"last_visit\", min(\"tanggal\") as \"first_visit\"
					FROM \"t_jual\"
					WHERE \"id_member\" is not null 
					AND trim(\"id_member\") <> ''
					GROUP BY \"id_member\"
				) b ON a.\"nomor_member\" = b.\"id_member\" "
				." left join (
					SELECT \"id_member\", sum(\"point\") as \"point\"
					FROM \"t_point\"
					WHERE \"id_member\" is not null 
					AND trim(\"id_member\") <> ''
					GROUP BY \"id_member\"
				) c ON a.\"nomor_member\" = c.\"id_member\" "
				." WHERE \"inactive\" != 0 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['debtor_no'] = $result[0];
			$lists[$count]['name'] = $result[1];
			$lists[$count]['debtor_ref'] = $result[2];
			$lists[$count]['address'] = $result[3];
			$lists[$count]['tax_id'] = $result[4];			
			$lists[$count]['curr_code'] = $result[5];	
			$lists[$count]['discount'] = $result[6];	
			$lists[$count]['pymt_discount'] = $result[7];
			$lists[$count]['credit_limit'] = $result[8];
			$lists[$count]['notes'] = $result[9];
			$lists[$count]['inactive'] = $result[10];
			$lists[$count]['nomor_member'] = $result[11];
			$lists[$count]['total_order'] = $result[12]!=''?$result[12]:0;
			$lists[$count]['total_purchase'] = $result[13]!=''?$result[13]:0;
			$lists[$count]['last_visit'] = $result[14];
			$lists[$count]['average_purchase'] = $result[15]!=''?$result[15]:0;
			$lists[$count]['average_visit_mon'] = $result[16]!=''?$result[16]:0;
			$lists[$count]['average_visit_year'] = $result[17]!=''?$result[17]:0;
			$lists[$count]['point'] = $result[18]!=''?$result[18]:0;
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirstCustomerManagement($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT a.\"debtor_no\", a.\"name\", a.\"debtor_ref\", a.\"address\", a.\"tax_id\", a.\"curr_code\", a.\"discount\", a.\"pymt_discount\" "
				.", a.\"credit_limit\", a.\"notes\", a.\"inactive\", a.\"nomor_member\", b.\"total_order\", b.\"total_purchase\", b.\"last_visit\",
				CASE WHEN b.\"total_order\" > 0 THEN (b.\"total_purchase\" / b.\"total_order\") ELSE 0 END,
				CEIL( b.\"total_order\" / CASE WHEN ((abs(date_part('year',age(now(), b.\"first_visit\")))*12) + abs(date_part('month',age(now(), b.\"first_visit\")))) > 0
					THEN ((abs(date_part('year',age(now(), b.\"first_visit\")))*12) + abs(date_part('month',age(now(), b.\"first_visit\"))))
					ELSE 1
				END ),  
				CEIL( b.\"total_order\" / CASE WHEN abs(date_part('year',age(now(), b.\"first_visit\"))) > 0
					THEN abs(date_part('year',age(now(), b.\"first_visit\")))
					ELSE 1
				END ),
				c.\"point\" "
				." FROM \"m_cust\" a left join (
					SELECT \"id_member\", count(\"id_jual\") as \"total_order\", sum(\"total\") as \"total_purchase\", 
					max(\"tanggal\") as \"last_visit\", min(\"tanggal\") as \"first_visit\" 
					FROM \"t_jual\"
					WHERE \"id_member\" is not null 
					AND trim(\"id_member\") <> ''
					GROUP BY \"id_member\"
				) b ON a.\"nomor_member\" = b.\"id_member\" "
				." left join (
					SELECT \"id_member\", sum(\"point\") as \"point\"
					FROM \"t_point\"
					WHERE \"id_member\" is not null 
					AND trim(\"id_member\") <> ''
					GROUP BY \"id_member\"
				) c ON a.\"nomor_member\" = c.\"id_member\" "
				." WHERE \"inactive\" != 0 "
				." AND \"debtor_no\" = '".$object->debtor_no."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['debtor_no'] = $result[0];
			$lists[$count]['name'] = $result[1];
			$lists[$count]['debtor_ref'] = $result[2];
			$lists[$count]['address'] = $result[3];
			$lists[$count]['tax_id'] = $result[4];			
			$lists[$count]['curr_code'] = $result[5];	
			$lists[$count]['discount'] = $result[6];	
			$lists[$count]['pymt_discount'] = $result[7];
			$lists[$count]['credit_limit'] = $result[8];
			$lists[$count]['notes'] = $result[9];
			$lists[$count]['inactive'] = $result[10];
			$lists[$count]['nomor_member'] = $result[11];
			$lists[$count]['total_order'] = $result[12]!=''?$result[12]:0;
			$lists[$count]['total_purchase'] = $result[13]!=''?$result[13]:0;
			$lists[$count]['last_visit'] = $result[14];
			$lists[$count]['average_purchase'] = $result[15]!=''?$result[15]:0;			
			$lists[$count]['average_visit_mon'] = $result[16]!=''?$result[16]:0;
			$lists[$count]['average_visit_year'] = $result[17]!=''?$result[17]:0;
			$lists[$count]['point'] = $result[18]!=''?$result[18]:0;
			
			$count++;
		}
		
		return $lists;
	}
}
