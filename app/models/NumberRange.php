<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class NumberRange extends Model
{

	public $number_id;
	public $trans_type;
	public $period;
	public $prefix;
	public $from_;	
	public $to_;
	public $current_no;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"number_id\", \"trans_type\", \"prefix\", \"from_\", \"to_\", \"current_no\", \"period\" "
				." FROM \"m_number_range\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['number_id'] = $result[0];
			$lists[$count]['trans_type'] = $result[1];
			$lists[$count]['prefix'] = $result[2];
			$lists[$count]['from_'] = $result[3];
			$lists[$count]['to_'] = $result[4];			
			$lists[$count]['current_no'] = $result[5];			
			$lists[$count]['period'] = $result[6];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"number_id\", \"trans_type\", \"prefix\", \"from_\", \"to_\", \"current_no\", \"period\" "
				." FROM \"m_number_range\" "
				." WHERE \"trans_type\" = '".$object->trans_type."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['number_id'] = $result[0];
			$lists[$count]['trans_type'] = $result[1];
			$lists[$count]['prefix'] = $result[2];
			$lists[$count]['from_'] = $result[3];
			$lists[$count]['to_'] = $result[4];			
			$lists[$count]['current_no'] = $result[5];
			$lists[$count]['period'] = $result[6];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"number_id\", \"trans_type\", \"prefix\", \"from_\", \"to_\", \"current_no\", \"period\" "
				." FROM \"m_number_range\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['number_id'] = $result[0];
			$lists[$count]['trans_type'] = $result[1];
			$lists[$count]['prefix'] = $result[2];
			$lists[$count]['from_'] = $result[3];
			$lists[$count]['to_'] = $result[4];			
			$lists[$count]['current_no'] = $result[5];	
			$lists[$count]['period'] = $result[6];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"number_id\") "
				." FROM \"m_number_range\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->from_ == '') { $object->from_ = '0'; }
		if($object->to_ == '') { $object->to_ = '0'; }
		if($object->current_no == '') { $object->current_no = '0'; }
		
		$sql = "INSERT INTO \"m_number_range\" (\"trans_type\", \"prefix\", \"from_\", \"to_\", \"current_no\", \"period\") "
				." VALUES ('".$object->trans_type."','".$object->prefix."','".$object->from_."','".$object->to_."','".$object->current_no."','".$object->period."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_number_range\" SET ";
		$flag = false;		
		if($object->prefix != '') { if($flag){ $sql .= ","; } $sql .= " \"prefix\" = '".$object->prefix."' "; $flag = true; }
		if($object->from_ != '') { if($flag){ $sql .= ","; } $sql .= " \"from_\" = '".$object->from_."' "; $flag = true; }
		if($object->to_ != '') { if($flag){ $sql .= ","; } $sql .= " \"to_\" = '".$object->to_."' "; $flag = true; }
		if($object->current_no != '') { if($flag){ $sql .= ","; } $sql .= " \"current_no\" = '".$object->current_no."' "; $flag = true; }		
		if($object->period != '') { if($flag){ $sql .= ","; } $sql .= " \"period\" = '".$object->period."' "; $flag = true; }	
		$sql .= " WHERE \"trans_type\" = '".$object->trans_type."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_number_range\" "
				." WHERE \"number_id\" = '".$object->number_id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goAddNumber($object){
		$connection = new Postgresql($this->di['db']);

		$sql = " UPDATE \"m_number_range\" SET \"current_no\" = \"current_no\" + 1 "
				." WHERE \"trans_type\" = '".$object->trans_type."' "
				." AND \"period\" = '".$object->period."' ";

		$success = $connection->execute($sql);		
		
		return $success;
	}
}
