<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Retur extends Model
{

	public $retur_no;
	public $retur_type;
	public $reference;
	public $supplier_id;	
	public $create_date;
	public $delivery_date;
	public $delivery_address;
	public $comments;
	public $gr_no_immpos;
	public $retur_no_immbo;
	public $total_gr;
	public $total_retur;
	public $tax_included;
	public $flag_sync;
	public $gr_no_immbo;
	public $gr_retur_immpos;
	public $gr_retur_immbo;
	public $flag_delete;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT	\"retur_no\", \"retur_type\", \"reference\", \"supplier_id\", \"create_date\", \"delivery_date\", \"delivery_address\", \"comments\", \"gr_no_immpos\", "
					."\"retur_no_immbo\", \"total_gr\", \"total_retur\", \"tax_included\", \"flag_sync\", \"gr_no_immbo\", \"gr_retur_immpos\", \"gr_retur_immbo\", \"flag_delete\" "
					." FROM t_retur ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['retur_no'] = $result[0];
			$lists[$count]['retur_type'] = $result[1];
			$lists[$count]['reference'] = $result[2];
			$lists[$count]['supplier_id'] = $result[3];	
			$lists[$count]['create_date'] = $result[4];	
			$lists[$count]['delivery_date'] = $result[5];	
			$lists[$count]['delivery_address'] = $result[6];	
			$lists[$count]['comments'] = $result[7];	
			$lists[$count]['gr_no_immpos'] = $result[8];	
			$lists[$count]['retur_no_immbo'] = $result[9];	
			$lists[$count]['total_gr'] = $result[10];	
			$lists[$count]['total_retur'] = $result[11];	
			$lists[$count]['tax_included'] = $result[12];	
			$lists[$count]['flag_sync'] = $result[13];	
			$lists[$count]['gr_no_immbo'] = $result[14];
			$lists[$count]['gr_retur_immpos'] = $result[15];
			$lists[$count]['gr_retur_immbo'] = $result[16];
			$lists[$count]['flag_delete'] = $result[17];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT	\"retur_no\", \"retur_type\", \"reference\", \"supplier_id\", \"create_date\", \"delivery_date\", \"delivery_address\", \"comments\", \"gr_no_immpos\", "
					."\"retur_no_immbo\", \"total_gr\", \"total_retur\", \"tax_included\", \"flag_sync\", \"gr_no_immbo\", \"gr_retur_immpos\", \"gr_retur_immbo\", \"flag_delete\" "
					." FROM t_retur "
					." WHERE \"retur_no\" = '".$object->retur_no."' ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['retur_no'] = $result[0];
			$lists[$count]['retur_type'] = $result[1];
			$lists[$count]['reference'] = $result[2];
			$lists[$count]['supplier_id'] = $result[3];	
			$lists[$count]['create_date'] = $result[4];	
			$lists[$count]['delivery_date'] = $result[5];	
			$lists[$count]['delivery_address'] = $result[6];	
			$lists[$count]['comments'] = $result[7];	
			$lists[$count]['gr_no_immpos'] = $result[8];	
			$lists[$count]['retur_no_immbo'] = $result[9];	
			$lists[$count]['total_gr'] = $result[10];	
			$lists[$count]['total_retur'] = $result[11];	
			$lists[$count]['tax_included'] = $result[12];	
			$lists[$count]['flag_sync'] = $result[13];	
			$lists[$count]['gr_no_immbo'] = $result[14];
			$lists[$count]['gr_retur_immpos'] = $result[15];
			$lists[$count]['gr_retur_immbo'] = $result[16];
			$lists[$count]['flag_delete'] = $result[17];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT	\"retur_no\", \"retur_type\", \"reference\", \"supplier_id\", \"create_date\", \"delivery_date\", \"delivery_address\", \"comments\", \"gr_no_immpos\", "
					."\"retur_no_immbo\", \"total_gr\", \"total_retur\", \"tax_included\", \"flag_sync\", \"gr_no_immbo\", \"gr_retur_immpos\", \"gr_retur_immbo\", \"flag_delete\" "
					." FROM t_retur ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['retur_no'] = $result[0];
			$lists[$count]['retur_type'] = $result[1];
			$lists[$count]['reference'] = $result[2];
			$lists[$count]['supplier_id'] = $result[3];	
			$lists[$count]['create_date'] = $result[4];	
			$lists[$count]['delivery_date'] = $result[5];	
			$lists[$count]['delivery_address'] = $result[6];	
			$lists[$count]['comments'] = $result[7];	
			$lists[$count]['gr_no_immpos'] = $result[8];	
			$lists[$count]['retur_no_immbo'] = $result[9];	
			$lists[$count]['total_gr'] = $result[10];	
			$lists[$count]['total_retur'] = $result[11];	
			$lists[$count]['tax_included'] = $result[12];	
			$lists[$count]['flag_sync'] = $result[13];	
			$lists[$count]['gr_no_immbo'] = $result[14];
			$lists[$count]['gr_retur_immpos'] = $result[15];
			$lists[$count]['gr_retur_immbo'] = $result[16];
			$lists[$count]['flag_delete'] = $result[17];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"retur_no\") "
				." FROM \"t_retur\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}		

	public function getCountRR($cond4){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"retur_no\") "
				." FROM \"t_retur\" ".$cond4;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->supplier_id == '') { $object->supplier_id = '0'; }		
		if($object->total_gr == '') { $object->total_gr = '0'; }	
		if($object->total_retur == '') { $object->total_retur = '0'; }	
		if($object->tax_included == '') { $object->tax_included = '0'; }
		
		if($object->flag_sync == '') { $object->flag_sync = 'f'; }
		if($object->flag_delete == '') { $object->flag_delete = 'f'; }

		if($object->create_date == '') { $object->create_date = date("Y-m-d"); }		
		if($object->delivery_date == '') { $object->delivery_date = date("Y-m-d"); }		
		
		$sql = "INSERT INTO \"t_retur\" (\"retur_no\", \"retur_type\", \"reference\", \"supplier_id\", \"create_date\", \"delivery_date\", \"delivery_address\" "
				." , \"comments\", \"gr_no_immpos\", \"retur_no_immbo\", \"total_gr\", \"total_retur\", \"tax_included\", \"flag_sync\", \"gr_no_immbo\" "
				." , \"gr_retur_immpos\", \"gr_retur_immbo\", \"flag_delete\" ) "
				." VALUES ('".$object->retur_no."','".$object->retur_type."','".$object->reference."','".$object->supplier_id."','".$object->create_date."','"
				.$object->delivery_date."','".$object->delivery_address."','".$object->comments."','".$object->gr_no_immpos."','".$object->retur_no_immbo."','"
				.$object->total_gr."','".$object->total_retur."','".$object->tax_included."','".$object->flag_sync."','".$object->gr_no_immbo."','"
				.$object->gr_retur_immpos."','".$object->gr_retur_immbo."','".$object->flag_delete."')";
		$success = $connection->execute($sql);	

		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_retur\" SET ";
		$flag = false;
		if($object->retur_type != '') { if($flag){ $sql .= ","; } $sql .= " \"retur_type\" = '".$object->retur_type."' "; $flag = true; }
		if($object->reference != '') { if($flag){ $sql .= ","; } $sql .= " \"reference\" = '".$object->reference."' "; $flag = true; }
		if($object->supplier_id != '') { if($flag){ $sql .= ","; } $sql .= " \"supplier_id\" = '".$object->supplier_id."' "; $flag = true; }
		if($object->create_date != '') { if($flag){ $sql .= ","; } $sql .= " \"create_date\" = '".$object->create_date."' "; $flag = true; }
		if($object->delivery_date != '') { if($flag){ $sql .= ","; } $sql .= " \"delivery_date\" = '".$object->delivery_date."' "; $flag = true; }
		if($object->delivery_address != '') { if($flag){ $sql .= ","; } $sql .= " \"delivery_address\" = '".$object->delivery_address."' "; $flag = true; }
		if($object->comments != '') { if($flag){ $sql .= ","; } $sql .= " \"comments\" = '".$object->comments."' "; $flag = true; }		
		if($object->gr_no_immpos != '') { if($flag){ $sql .= ","; } $sql .= " \"gr_no_immpos\" = '".$object->gr_no_immpos."' "; $flag = true; }	
		if($object->retur_no_immbo != '') { if($flag){ $sql .= ","; } $sql .= " \"retur_no_immbo\" = '".$object->retur_no_immbo."' "; $flag = true; }	
		if($object->total_gr != '') { if($flag){ $sql .= ","; } $sql .= " \"total_gr\" = '".$object->total_gr."' "; $flag = true; }	
		if($object->total_retur != '') { if($flag){ $sql .= ","; } $sql .= " \"total_retur\" = '".$object->total_retur."' "; $flag = true; }	
		if($object->tax_included != '') { if($flag){ $sql .= ","; } $sql .= " \"tax_included\" = '".$object->tax_included."' "; $flag = true; }	
		if($object->flag_sync != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = '".$object->flag_sync."' "; $flag = true; }	
		if($object->gr_no_immbo != '') { if($flag){ $sql .= ","; } $sql .= " \"gr_no_immbo\" = '".$object->gr_no_immbo."' "; $flag = true; }	
		if($object->gr_retur_immpos != '') { if($flag){ $sql .= ","; } $sql .= " \"gr_retur_immpos\" = '".$object->gr_retur_immpos."' "; $flag = true; }	
		if($object->gr_retur_immbo != '') { if($flag){ $sql .= ","; } $sql .= " \"gr_retur_immbo\" = '".$object->gr_retur_immbo."' "; $flag = true; }	
		if($object->flag_delete != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_delete\" = '".$object->flag_delete."' "; $flag = true; }	
		$sql .= " WHERE \"retur_no\" = '".$object->retur_no."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goUpdateClear($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_retur\" SET ";
		$flag = false;
		if($object->retur_type == 'X') { if($flag){ $sql .= ","; } $sql .= " \"retur_type\" = '' "; $flag = true; }
		if($object->reference == 'X') { if($flag){ $sql .= ","; } $sql .= " \"reference\" = '' "; $flag = true; }
		if($object->supplier_id == 'X') { if($flag){ $sql .= ","; } $sql .= " \"supplier_id\" = '0' "; $flag = true; }
		if($object->delivery_address == 'X') { if($flag){ $sql .= ","; } $sql .= " \"delivery_address\" = '' "; $flag = true; }
		if($object->comments == 'X') { if($flag){ $sql .= ","; } $sql .= " \"comments\" = '' "; $flag = true; }		
		if($object->gr_no_immpos == 'X') { if($flag){ $sql .= ","; } $sql .= " \"gr_no_immpos\" = '' "; $flag = true; }	
		if($object->retur_no_immbo == 'X') { if($flag){ $sql .= ","; } $sql .= " \"retur_no_immbo\" = '' "; $flag = true; }	
		if($object->total_gr == 'X') { if($flag){ $sql .= ","; } $sql .= " \"total_gr\" = '0' "; $flag = true; }	
		if($object->total_retur == 'X') { if($flag){ $sql .= ","; } $sql .= " \"total_retur\" = '0' "; $flag = true; }	
		if($object->tax_included == 'X') { if($flag){ $sql .= ","; } $sql .= " \"tax_included\" = '0' "; $flag = true; }	
		if($object->flag_sync == 'X') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = 'f' "; $flag = true; }	
		if($object->gr_no_immbo == 'X') { if($flag){ $sql .= ","; } $sql .= " \"gr_no_immbo\" = '' "; $flag = true; }	
		if($object->gr_retur_immpos == 'X') { if($flag){ $sql .= ","; } $sql .= " \"gr_retur_immpos\" = '' "; $flag = true; }	
		if($object->gr_retur_immbo == 'X') { if($flag){ $sql .= ","; } $sql .= " \"gr_retur_immbo\" = '' "; $flag = true; }	
		if($object->flag_delete == 'X') { if($flag){ $sql .= ","; } $sql .= " \"flag_delete\" = 'f' "; $flag = true; }	
		$sql .= " WHERE \"retur_no\" = '".$object->retur_no."' ";
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_retur\" "
				." WHERE \"retur_no\" = '".$object->retur_no."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function getJoin_Detail_Product($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT	r.\"retur_no\", \"retur_type\", \"reference\", \"supplier_id\", \"create_date\", \"delivery_date\", \"delivery_address\", rd.\"comments\", \"gr_no_immpos\" "
				.", \"retur_no_immbo\", \"total_gr\", \"total_retur\", \"tax_included\", \"flag_sync\", \"gr_no_immbo\", \"gr_retur_immpos\", \"gr_retur_immbo\", \"flag_delete\" "
				.", rd.\"item_code\", rd.\"description\", \"qty_retur\", \"unit_retur\", \"retur_unit_price\", \"retur_price\", \"gr_detail_item\" "
				.", 
					CASE
						WHEN uom1_htype_1 IS true THEN uom1_nm
						WHEN uom2_htype_1 IS true THEN uom2_nm
						WHEN uom3_htype_1 IS true THEN uom3_nm
						WHEN uom4_htype_1 IS true THEN uom4_nm
						ELSE ''
					END 
						AS uom_s_type, 
					
					CASE
						WHEN uom1_htype_4 IS true THEN uom1_nm
						WHEN uom2_htype_4 IS true THEN uom2_nm
						WHEN uom3_htype_4 IS true THEN uom3_nm
						WHEN uom4_htype_4 IS true THEN uom4_nm
						ELSE ''
					END 
						AS uom_r_type, 
						
					CASE
						WHEN uom1_htype_1 IS true THEN uom1_internal_barcode
						WHEN uom2_htype_1 IS true THEN uom2_internal_barcode
						WHEN uom3_htype_1 IS true THEN uom3_internal_barcode
						WHEN uom4_htype_1 IS true THEN uom4_internal_barcode
						ELSE ''
					END 
						AS uom_s_barcode,
					
					CASE
						WHEN uom1_htype_4 IS true THEN uom1_internal_barcode
						WHEN uom2_htype_4 IS true THEN uom2_internal_barcode
						WHEN uom3_htype_4 IS true THEN uom3_internal_barcode
						WHEN uom4_htype_4 IS true THEN uom4_internal_barcode
						ELSE ''
					END 
						AS uom_r_barcode,
						
					CASE
						WHEN uom1_htype_1 IS true THEN uom1_conversion
						WHEN uom2_htype_1 IS true THEN uom2_conversion
						WHEN uom3_htype_1 IS true THEN uom3_conversion
						WHEN uom4_htype_1 IS true THEN uom4_conversion
						ELSE ''
					END 
						AS uom_s_conversion,
					
					CASE
						WHEN uom1_htype_4 IS true THEN uom1_conversion
						WHEN uom2_htype_4 IS true THEN uom2_conversion
						WHEN uom3_htype_4 IS true THEN uom3_conversion
						WHEN uom4_htype_4 IS true THEN uom4_conversion
						ELSE ''
					END 
						AS uom_r_conversion
					
				" 
				." FROM t_retur r INNER JOIN t_retur_detail rd ON r.retur_no = rd.retur_no 
				INNER JOIN m_product mp ON rd.item_code = mp.stock_id ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['retur_no'] = $result[0];
			$lists[$count]['retur_type'] = $result[1];
			$lists[$count]['reference'] = $result[2];
			$lists[$count]['supplier_id'] = $result[3];	
			$lists[$count]['create_date'] = $result[4];	
			$lists[$count]['delivery_date'] = $result[5];	
			$lists[$count]['delivery_address'] = $result[6];	
			$lists[$count]['comments'] = $result[7];	
			$lists[$count]['gr_no_immpos'] = $result[8];	
			$lists[$count]['retur_no_immbo'] = $result[9];	
			$lists[$count]['total_gr'] = $result[10];	
			$lists[$count]['total_retur'] = $result[11];	
			$lists[$count]['tax_included'] = $result[12];	
			$lists[$count]['flag_sync'] = $result[13];	
			$lists[$count]['gr_no_immbo'] = $result[14];
			$lists[$count]['gr_retur_immpos'] = $result[15];
			$lists[$count]['gr_retur_immbo'] = $result[16];
			$lists[$count]['flag_delete'] = $result[17];
			$lists[$count]['item_code'] = $result[18];
			$lists[$count]['description'] = $result[19];
			$lists[$count]['qty_retur'] = $result[20];
			$lists[$count]['unit_retur'] = $result[21];
			$lists[$count]['retur_unit_price'] = $result[22];
			$lists[$count]['retur_price'] = $result[23];
			$lists[$count]['gr_detail_item'] = $result[24];
			$lists[$count]['uom_s_type'] = $result[25];
			$lists[$count]['uom_r_type'] = $result[26];
			$lists[$count]['uom_s_barcode'] = $result[27];
			$lists[$count]['uom_r_barcode'] = $result[28];
			$lists[$count]['uom_s_conversion'] = $result[29];
			$lists[$count]['uom_r_conversion'] = $result[30];
			
			$count++;
		}
		
		return $lists;
	}
}
