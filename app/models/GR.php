<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class GR extends Model
{

	public $gr_no;
	public $create_by;
	public $create_date;	
	public $delivery_date;
	public $comments;
	public $pr_no_immpos;
	public $pr_no_immbo;
	public $po_no_immbo;
	public $retur_no_immpos;
	public $retur_no_immbo;
	public $retur_doc_immpos;
	public $retur_doc_immbo;
	public $total_pr;
	public $total_po;
	public $total_retur;
	public $gr_no_immbo;
	public $flag_sync;
	public $flag_delete;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"gr_no\", \"create_by\", \"create_date\", \"delivery_date\", \"comments\" "
				." , \"pr_no_immpos\", \"pr_no_immbo\", \"po_no_immbo\", \"retur_no_immpos\", \"retur_no_immbo\", \"total_pr\", \"total_po\" "	
				." , \"retur_doc_immpos\", \"retur_doc_immbo\", \"total_retur\", \"gr_no_immbo\", \"flag_sync\", \"flag_delete\" "
				." FROM \"t_gr\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['gr_no'] = $result[0];
			$lists[$count]['create_by'] = $result[1];
			$lists[$count]['create_date'] = $result[2];
			$lists[$count]['delivery_date'] = $result[3];			
			$lists[$count]['comments'] = $result[4];				
			$lists[$count]['pr_no_immpos'] = $result[5];	
			$lists[$count]['pr_no_immbo'] = $result[6];	
			$lists[$count]['po_no_immbo'] = $result[7];	
			$lists[$count]['retur_no_immpos'] = $result[8];	
			$lists[$count]['retur_no_immbo'] = $result[9];
			$lists[$count]['total_pr'] = $result[10];	
			$lists[$count]['total_po'] = $result[11];	
			$lists[$count]['retur_doc_immpos'] = $result[12];
			$lists[$count]['retur_doc_immbo'] = $result[13];
			$lists[$count]['total_retur'] = $result[14];			
			$lists[$count]['gr_no_immbo'] = $result[15];
			$lists[$count]['flag_sync'] = $result[16];
			$lists[$count]['flag_delete'] = $result[17];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"gr_no\", \"create_by\", \"create_date\", \"delivery_date\", \"comments\" "
				." , \"pr_no_immpos\", \"pr_no_immbo\", \"po_no_immbo\", \"retur_no_immpos\", \"retur_no_immbo\", \"total_pr\", \"total_po\" "	
				." , \"retur_doc_immpos\", \"retur_doc_immbo\", \"total_retur\", \"gr_no_immbo\", \"flag_sync\", \"flag_delete\" "				
				." FROM \"t_gr\" "
				." WHERE \"gr_no\" = '".$object->gr_no."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['gr_no'] = $result[0];
			$lists[$count]['create_by'] = $result[1];
			$lists[$count]['create_date'] = $result[2];
			$lists[$count]['delivery_date'] = $result[3];			
			$lists[$count]['comments'] = $result[4];				
			$lists[$count]['pr_no_immpos'] = $result[5];	
			$lists[$count]['pr_no_immbo'] = $result[6];	
			$lists[$count]['po_no_immbo'] = $result[7];	
			$lists[$count]['retur_no_immpos'] = $result[8];	
			$lists[$count]['retur_no_immbo'] = $result[9];
			$lists[$count]['total_pr'] = $result[10];	
			$lists[$count]['total_po'] = $result[11];			
			$lists[$count]['retur_doc_immpos'] = $result[12];
			$lists[$count]['retur_doc_immbo'] = $result[13];
			$lists[$count]['total_retur'] = $result[14];			
			$lists[$count]['gr_no_immbo'] = $result[15];
			$lists[$count]['flag_sync'] = $result[16];
			$lists[$count]['flag_delete'] = $result[17];			
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"gr_no\", \"create_by\", \"create_date\", \"delivery_date\", \"comments\" "
				." , \"pr_no_immpos\", \"pr_no_immbo\", \"po_no_immbo\", \"retur_no_immpos\", \"retur_no_immbo\", \"total_pr\", \"total_po\" "		
				." , \"retur_doc_immpos\", \"retur_doc_immbo\", \"total_retur\", \"gr_no_immbo\", \"flag_sync\", \"flag_delete\" "
				." FROM \"t_gr\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['gr_no'] = $result[0];
			$lists[$count]['create_by'] = $result[1];
			$lists[$count]['create_date'] = $result[2];
			$lists[$count]['delivery_date'] = $result[3];			
			$lists[$count]['comments'] = $result[4];				
			$lists[$count]['pr_no_immpos'] = $result[5];	
			$lists[$count]['pr_no_immbo'] = $result[6];	
			$lists[$count]['po_no_immbo'] = $result[7];	
			$lists[$count]['retur_no_immpos'] = $result[8];	
			$lists[$count]['retur_no_immbo'] = $result[9];
			$lists[$count]['total_pr'] = $result[10];	
			$lists[$count]['total_po'] = $result[11];			
			$lists[$count]['retur_doc_immpos'] = $result[12];
			$lists[$count]['retur_doc_immbo'] = $result[13];
			$lists[$count]['total_retur'] = $result[14];			
			$lists[$count]['gr_no_immbo'] = $result[15];
			$lists[$count]['flag_sync'] = $result[16];	
			$lists[$count]['flag_delete'] = $result[17];

			$count++;
		}
		
		return $lists;
	}

	public function getFreeSQL2($condition_gr2){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"gr_no\", \"create_by\", \"create_date\", \"delivery_date\", \"comments\" "
				." , \"pr_no_immpos\", \"pr_no_immbo\", \"po_no_immbo\", \"retur_no_immpos\", \"retur_no_immbo\", \"total_pr\", \"total_po\" "		
				." , \"retur_doc_immpos\", \"retur_doc_immbo\", \"total_retur\", \"gr_no_immbo\", \"flag_sync\", \"flag_delete\" "
				." FROM \"t_gr\" ".$condition_gr2;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['gr_no'] = $result[0];
			$lists[$count]['create_by'] = $result[1];
			$lists[$count]['create_date'] = $result[2];
			$lists[$count]['delivery_date'] = $result[3];			
			$lists[$count]['comments'] = $result[4];				
			$lists[$count]['pr_no_immpos'] = $result[5];	
			$lists[$count]['pr_no_immbo'] = $result[6];	
			$lists[$count]['po_no_immbo'] = $result[7];	
			$lists[$count]['retur_no_immpos'] = $result[8];	
			$lists[$count]['retur_no_immbo'] = $result[9];
			$lists[$count]['total_pr'] = $result[10];	
			$lists[$count]['total_po'] = $result[11];			
			$lists[$count]['retur_doc_immpos'] = $result[12];
			$lists[$count]['retur_doc_immbo'] = $result[13];
			$lists[$count]['total_retur'] = $result[14];			
			$lists[$count]['gr_no_immbo'] = $result[15];
			$lists[$count]['flag_sync'] = $result[16];	
			$lists[$count]['flag_delete'] = $result[17];

			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"gr_no\") "
				." FROM \"t_gr\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}

	public function getCountGR($cond3){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"gr_no\") "
				." FROM \"t_gr\" ".$cond3;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->total_pr == '') { $object->total_pr = '0'; }
		if($object->total_po == '') { $object->total_po = '0'; }
		if($object->total_retur == '') { $object->total_retur = '0'; }
		
		if($object->create_date == '') { $object->create_date = date("Y-m-d H:i:s"); }		
		if($object->delivery_date == '') { $object->delivery_date = date("Y-m-d"); }	

		if($object->flag_sync == '') { $object->flag_sync = 'f'; }			
		if($object->flag_delete == '') { $object->flag_delete = 'f'; }
		
		$sql = "INSERT INTO \"t_gr\" (\"gr_no\", \"create_by\", \"create_date\", \"delivery_date\", \"comments\" "
				." , \"pr_no_immpos\", \"pr_no_immbo\", \"po_no_immbo\", \"retur_no_immpos\", \"retur_no_immbo\", \"total_pr\", \"total_po\" "
				." , \"retur_doc_immpos\", \"retur_doc_immbo\", \"total_retur\", \"gr_no_immbo\", \"flag_sync\", \"flag_delete\" ) "
				." VALUES ('".$object->gr_no."','".$object->create_by."','".$object->create_date."','"
				.$object->delivery_date."','".$object->comments."','".$object->pr_no_immpos."','"
				.$object->pr_no_immbo."','".$object->po_no_immbo."','".$object->retur_no_immpos."','".$object->retur_no_immbo."','".$object->total_pr."','"
				.$object->total_po."','".$object->retur_doc_immpos."','".$object->retur_doc_immbo."','"
				.$object->total_retur."','".$object->gr_no_immbo."','".$object->flag_sync."','".$object->flag_delete."' ) ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_gr\" SET ";
		$flag = false;		
		if($object->create_by != '') { if($flag){ $sql .= ","; } $sql .= " \"create_by\" = '".$object->create_by."' "; $flag = true; }
		if($object->create_date != '') { if($flag){ $sql .= ","; } $sql .= " \"create_date\" = '".$object->create_date."' "; $flag = true; }
		if($object->delivery_date != '') { if($flag){ $sql .= ","; } $sql .= " \"delivery_date\" = '".$object->delivery_date."' "; $flag = true; }
		if($object->comments != '') { if($flag){ $sql .= ","; } $sql .= " \"comments\" = '".$object->comments."' "; $flag = true; }		
		if($object->pr_no_immpos != '') { if($flag){ $sql .= ","; } $sql .= " \"pr_no_immpos\" = '".$object->pr_no_immpos."' "; $flag = true; }
		if($object->pr_no_immbo != '') { if($flag){ $sql .= ","; } $sql .= " \"pr_no_immbo\" = '".$object->pr_no_immbo."' "; $flag = true; }
		if($object->po_no_immbo != '') { if($flag){ $sql .= ","; } $sql .= " \"po_no_immbo\" = '".$object->po_no_immbo."' "; $flag = true; }
		if($object->retur_no_immpos != '') { if($flag){ $sql .= ","; } $sql .= " \"retur_no_immpos\" = '".$object->retur_no_immpos."' "; $flag = true; }
		if($object->retur_no_immbo != '') { if($flag){ $sql .= ","; } $sql .= " \"retur_no_immbo\" = '".$object->retur_no_immbo."' "; $flag = true; }
		if($object->total_pr != '') { if($flag){ $sql .= ","; } $sql .= " \"total_pr\" = '".$object->total_pr."' "; $flag = true; }
		if($object->total_po != '') { if($flag){ $sql .= ","; } $sql .= " \"total_po\" = '".$object->total_po."' "; $flag = true; }		
		if($object->retur_doc_immpos != '') { if($flag){ $sql .= ","; } $sql .= " \"retur_doc_immpos\" = '".$object->retur_doc_immpos."' "; $flag = true; }	
		if($object->retur_doc_immbo != '') { if($flag){ $sql .= ","; } $sql .= " \"retur_doc_immbo\" = '".$object->retur_doc_immbo."' "; $flag = true; }	
		if($object->total_retur != '') { if($flag){ $sql .= ","; } $sql .= " \"total_retur\" = '".$object->total_retur."' "; $flag = true; }	
		if($object->gr_no_immbo != '') { if($flag){ $sql .= ","; } $sql .= " \"gr_no_immbo\" = '".$object->gr_no_immbo."' "; $flag = true; }
		if($object->flag_sync != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = '".$object->flag_sync."' "; $flag = true; }
		if($object->flag_delete != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_delete\" = '".$object->flag_delete."' "; $flag = true; }
		$sql .= " WHERE \"gr_no\" = '".$object->gr_no."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goUpdateClear($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_gr\" SET ";
		$flag = false;		
		if($object->create_by == 'X') { if($flag){ $sql .= ","; } $sql .= " \"create_by\" = '' "; $flag = true; }		
		if($object->comments == 'X') { if($flag){ $sql .= ","; } $sql .= " \"comments\" = '' "; $flag = true; }		
		if($object->pr_no_immpos == 'X') { if($flag){ $sql .= ","; } $sql .= " \"pr_no_immpos\" = '' "; $flag = true; }
		if($object->pr_no_immbo == 'X') { if($flag){ $sql .= ","; } $sql .= " \"pr_no_immbo\" = '' "; $flag = true; }
		if($object->po_no_immbo == 'X') { if($flag){ $sql .= ","; } $sql .= " \"po_no_immbo\" = '' "; $flag = true; }
		if($object->retur_no_immpos == 'X') { if($flag){ $sql .= ","; } $sql .= " \"retur_no_immpos\" = '' "; $flag = true; }
		if($object->retur_no_immbo == 'X') { if($flag){ $sql .= ","; } $sql .= " \"retur_no_immbo\" = '' "; $flag = true; }
		if($object->total_pr == 'X') { if($flag){ $sql .= ","; } $sql .= " \"total_pr\" = '0' "; $flag = true; }
		if($object->total_po == 'X') { if($flag){ $sql .= ","; } $sql .= " \"total_po\" = '0' "; $flag = true; }		
		if($object->retur_doc_immpos == 'X') { if($flag){ $sql .= ","; } $sql .= " \"retur_doc_immpos\" = '' "; $flag = true; }	
		if($object->retur_doc_immbo == 'X') { if($flag){ $sql .= ","; } $sql .= " \"retur_doc_immbo\" = '' "; $flag = true; }	
		if($object->total_retur == 'X') { if($flag){ $sql .= ","; } $sql .= " \"total_retur\" = '0' "; $flag = true; }	
		if($object->gr_no_immbo == 'X') { if($flag){ $sql .= ","; } $sql .= " \"gr_no_immbo\" = '0' "; $flag = true; }
		if($object->flag_sync == 'X') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = 'f' "; $flag = true; }
		if($object->flag_delete == 'X') { if($flag){ $sql .= ","; } $sql .= " \"flag_delete\" = 'f' "; $flag = true; }
		$sql .= " WHERE \"gr_no\" = '".$object->gr_no."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_gr\" "
				." WHERE \"gr_no\" = '".$object->gr_no."' ";
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function getJoin_PR_PO($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"gr_no\", \"create_by\", \"create_date\", \"delivery_date\", \"comments\", \"pr_no_immpos\", \"pr_no_immbo\", gr.\"po_no_immbo\", \"retur_no_immpos\" "
			.", \"retur_no_immbo\", \"total_pr\", \"total_po\", \"retur_doc_immpos\", \"retur_doc_immbo\", \"total_retur\", \"gr_no_immbo\", \"flag_sync\", \"flag_delete\" "		
			.", pod.\"item_code\", pod.\"description\", \"qty_order\", \"unit_order\", \"qty_sell\", \"unit_sell\", pod.\"qty_pr\", \"unit_pr\", \"unit_order_price\", \"order_price\" "
			.", \"con_order\", \"con_sell\", \"con_pr\", \"barcode\", prd.\"supplied_by\", \"qty_po\", \"unit_po\", \"po_unit_price\", \"po_price\", \"flag_gr_ok\", "	
			." 
				CASE
					WHEN uom1_htype_3 IS true THEN uom1_nm
					WHEN uom2_htype_3 IS true THEN uom2_nm
					WHEN uom3_htype_3 IS true THEN uom3_nm
					WHEN uom4_htype_3 IS true THEN uom4_nm
					ELSE ''
				END 
					AS uom_o_type,
					
				CASE
					WHEN uom1_htype_3 IS true THEN uom1_internal_barcode
					WHEN uom2_htype_3 IS true THEN uom2_internal_barcode
					WHEN uom3_htype_3 IS true THEN uom3_internal_barcode
					WHEN uom4_htype_3 IS true THEN uom4_internal_barcode
					ELSE ''
				END 
					AS uom_o_barcode,
					
				CASE
					WHEN uom1_htype_2 IS true THEN uom1_nm
					WHEN uom2_htype_2 IS true THEN uom2_nm
					WHEN uom3_htype_2 IS true THEN uom3_nm
					WHEN uom4_htype_2 IS true THEN uom4_nm
					ELSE ''
				END 
					AS uom_r_type,
					
				CASE
					WHEN uom1_htype_2 IS true THEN uom1_internal_barcode
					WHEN uom2_htype_2 IS true THEN uom2_internal_barcode
					WHEN uom3_htype_2 IS true THEN uom3_internal_barcode
					WHEN uom4_htype_2 IS true THEN uom4_internal_barcode
					ELSE ''
				END 
					AS uom_r_barcode, 
				
				CASE
					WHEN uom1_htype_3 IS true THEN uom1_conversion
					WHEN uom2_htype_3 IS true THEN uom2_conversion
					WHEN uom3_htype_3 IS true THEN uom3_conversion
					WHEN uom4_htype_3 IS true THEN uom4_conversion
					ELSE ''
				END 
					AS uom_o_conversion, 
				
				CASE
					WHEN uom1_htype_2 IS true THEN uom1_conversion
					WHEN uom2_htype_2 IS true THEN uom2_conversion
					WHEN uom3_htype_2 IS true THEN uom3_conversion
					WHEN uom4_htype_2 IS true THEN uom4_conversion
					ELSE ''
				END 
					AS uom_r_conversion
			"	
			
			." FROM \"t_gr\" gr INNER JOIN \"t_pr_detail\" prd ON gr.pr_no_immpos = prd.request_no "
			." INNER JOIN \"t_po_detail\" pod ON gr.po_no_immbo = pod.po_no_immbo::varchar "
			." INNER JOIN \"m_product\" mp ON prd.item_code = mp.stock_id ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['gr_no'] = $result[0];								
			$lists[$count]['create_by'] = $result[1];
			$lists[$count]['create_date'] = $result[2];
			$lists[$count]['delivery_date'] = $result[3];			
			$lists[$count]['comments'] = $result[4];				
			$lists[$count]['pr_no_immpos'] = $result[5];	
			$lists[$count]['pr_no_immbo'] = $result[6];	
			$lists[$count]['po_no_immbo'] = $result[7];	
			$lists[$count]['retur_no_immpos'] = $result[8];	
			$lists[$count]['retur_no_immbo'] = $result[9];
			$lists[$count]['total_pr'] = $result[10];	
			$lists[$count]['total_po'] = $result[11];			
			$lists[$count]['retur_doc_immpos'] = $result[12];
			$lists[$count]['retur_doc_immbo'] = $result[13];
			$lists[$count]['total_retur'] = $result[14];			
			$lists[$count]['gr_no_immbo'] = $result[15];
			$lists[$count]['flag_sync'] = $result[16];	
			$lists[$count]['flag_delete'] = $result[17];
			$lists[$count]['item_code'] = $result[18];
			$lists[$count]['description'] = $result[19];
			$lists[$count]['qty_order'] = $result[20];
			$lists[$count]['unit_order'] = $result[21];
			$lists[$count]['qty_sell'] = $result[22];
			$lists[$count]['unit_sell'] = $result[23];
			$lists[$count]['qty_pr'] = $result[24];
			$lists[$count]['unit_pr'] = $result[25];
			$lists[$count]['unit_order_price'] = $result[26];
			$lists[$count]['order_price'] = $result[27];
			$lists[$count]['con_order'] = $result[28];
			$lists[$count]['con_sell'] = $result[29];
			$lists[$count]['con_pr'] = $result[30];
			$lists[$count]['barcode'] = $result[31];
			$lists[$count]['supplied_by'] = $result[32];
			$lists[$count]['qty_po'] = $result[33];
			$lists[$count]['unit_po'] = $result[34];
			$lists[$count]['po_unit_price'] = $result[35];
			$lists[$count]['po_price'] = $result[36];
			$lists[$count]['flag_gr_ok'] = $result[37];
			$lists[$count]['uom_o_type'] = $result[38];
			$lists[$count]['uom_o_barcode'] = $result[39];
			$lists[$count]['uom_r_type'] = $result[40];
			$lists[$count]['uom_r_barcode'] = $result[41];
			$lists[$count]['uom_o_conversion'] = $result[42];
			$lists[$count]['uom_r_conversion'] = $result[43];

			$count++;
		}
		
		return $lists;
	}
	
	public function getFirsts($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT g.\"gr_no\", g.\"create_by\", g.\"create_date\", g.\"delivery_date\", g.\"comments\" "
				." , g.\"pr_no_immpos\", g.\"pr_no_immbo\", g.\"po_no_immbo\", g.\"retur_no_immpos\", g.\"retur_no_immbo\", g.\"total_pr\", g.\"total_po\" "	
				." , g.\"retur_doc_immpos\", g.\"retur_doc_immbo\", g.\"total_retur\", g.\"gr_no_immbo\", g.\"flag_sync\", g.\"flag_delete\", p.\"po_no_ref\", p.\"order_date\" "				
				." FROM \"t_gr\" g INNER JOIN \"t_po\" p ON g.pr_no_immpos = p.pr_no_immpos "
				." WHERE \"gr_no\" = '".$object->gr_no."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['gr_no'] = $result[0];
			$lists[$count]['create_by'] = $result[1];
			$lists[$count]['create_date'] = $result[2];
			$lists[$count]['delivery_date'] = $result[3];			
			$lists[$count]['comments'] = $result[4];				
			$lists[$count]['pr_no_immpos'] = $result[5];	
			$lists[$count]['pr_no_immbo'] = $result[6];	
			$lists[$count]['po_no_immbo'] = $result[7];	
			$lists[$count]['retur_no_immpos'] = $result[8];	
			$lists[$count]['retur_no_immbo'] = $result[9];
			$lists[$count]['total_pr'] = $result[10];	
			$lists[$count]['total_po'] = $result[11];			
			$lists[$count]['retur_doc_immpos'] = $result[12];
			$lists[$count]['retur_doc_immbo'] = $result[13];
			$lists[$count]['total_retur'] = $result[14];			
			$lists[$count]['gr_no_immbo'] = $result[15];
			$lists[$count]['flag_sync'] = $result[16];
			$lists[$count]['flag_delete'] = $result[17];			
			$lists[$count]['po_no_ref'] = $result[18];			
			$lists[$count]['order_date'] = $result[19];			
			
			$count++;
		}
		
		return $lists;
	}
}
