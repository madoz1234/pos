<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class PODetail extends Model
{

	public $po_detail_item;
	public $po_no_immbo;
	public $item_code;
	public $description;		
	public $qty_po;
	public $unit_po;	
	public $po_unit_price;
	public $po_price;
	public $flag_gr_ok;
	public $pr_detail_item;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"po_detail_item\", \"po_no_immbo\", \"item_code\", \"description\", \"qty_po\", \"unit_po\", \"po_unit_price\" "
				." , \"po_price\", \"flag_gr_ok\" , \"pr_detail_item\", \"qty_pr\" "
				." FROM \"t_po_detail\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['po_detail_item'] = $result[0];
			$lists[$count]['po_no_immbo'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty_po'] = $result[4];			
			$lists[$count]['unit_po'] = $result[5];	
			$lists[$count]['po_unit_price'] = $result[6];	
			$lists[$count]['po_price'] = $result[7];	
			$lists[$count]['flag_gr_ok'] = $result[8];	
			$lists[$count]['pr_detail_item'] = $result[9];				
			$lists[$count]['qty_pr'] = $result[10];				
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"po_detail_item\", \"po_no_immbo\", \"item_code\", \"description\", \"qty_po\", \"unit_po\", \"po_unit_price\" "
				." , \"po_price\", \"flag_gr_ok\" , \"pr_detail_item\", \"qty_pr\" "
				." FROM \"t_po_detail\" "
				." WHERE \"po_no_immbo\" = '".$object->po_no_immbo."' "
				." AND \"po_detail_item\" = '".$object->po_detail_item."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['po_detail_item'] = $result[0];
			$lists[$count]['po_no_immbo'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty_po'] = $result[4];			
			$lists[$count]['unit_po'] = $result[5];	
			$lists[$count]['po_unit_price'] = $result[6];	
			$lists[$count]['po_price'] = $result[7];	
			$lists[$count]['flag_gr_ok'] = $result[8];	
			$lists[$count]['pr_detail_item'] = $result[9];				
			$lists[$count]['qty_pr'] = $result[10];				
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"po_detail_item\", \"po_no_immbo\", \"item_code\", \"description\", \"qty_po\", \"unit_po\", \"po_unit_price\" "
				." , \"po_price\", \"flag_gr_ok\", \"pr_detail_item\", \"qty_pr\" "
				." FROM \"t_po_detail\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['po_detail_item'] = $result[0];
			$lists[$count]['po_no_immbo'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty_po'] = $result[4];			
			$lists[$count]['unit_po'] = $result[5];	
			$lists[$count]['po_unit_price'] = $result[6];	
			$lists[$count]['po_price'] = $result[7];	
			$lists[$count]['flag_gr_ok'] = $result[8];	
			$lists[$count]['pr_detail_item'] = $result[9];				
			$lists[$count]['qty_pr'] = $result[10];				
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"po_no_immbo\") "
				." FROM \"t_po_detail\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		//if($object->po_detail_item == '') { $object->po_detail_item = '0'; }
		if($object->po_no_immbo == '') { $object->po_no_immbo = '0'; }		
		if($object->qty_po == '') { $object->qty_po = '0'; }	
		if($object->po_unit_price == '') { $object->po_unit_price = '0'; }	
		if($object->po_price == '') { $object->po_price = '0'; }	
		if($object->pr_detail_item == '') { $object->pr_detail_item = '0'; }	
		if($object->qty_pr == '') { $object->qty_pr = '0'; }	
		
		$sql = "INSERT INTO \"t_po_detail\" (\"po_no_immbo\", \"item_code\", \"description\", \"qty_po\", \"unit_po\" "
				." , \"po_unit_price\", \"po_price\", \"flag_gr_ok\", \"pr_detail_item\", \"qty_pr\" ) "
				." VALUES ('".$object->po_no_immbo."','".$object->item_code."','".pg_escape_string($object->description)."','".$object->qty_po."','"
				.$object->unit_po."','".$object->po_unit_price."','".$object->po_price."','".$object->flag_gr_ok."','".$object->pr_detail_item."','".$object->qty_pr."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}

	public function insertSql($object){
		$connection = new Postgresql($this->di['db']);

		//if($object->po_detail_item == '') { $object->po_detail_item = '0'; }
		if($object->po_no_immbo == '') { $object->po_no_immbo = '0'; }		
		if($object->qty_po == '') { $object->qty_po = '0'; }	
		if($object->po_unit_price == '') { $object->po_unit_price = '0'; }	
		if($object->po_price == '') { $object->po_price = '0'; }	
		if($object->pr_detail_item == '') { $object->pr_detail_item = '0'; }	
		if($object->qty_pr == '') { $object->qty_pr = '0'; }	
		
		$sql = "INSERT INTO \"t_po_detail\" (\"po_no_immbo\", \"item_code\", \"description\", \"qty_po\", \"unit_po\" "
				." , \"po_unit_price\", \"po_price\", \"flag_gr_ok\", \"pr_detail_item\", \"qty_pr\" ) "
				." VALUES ('".$object->po_no_immbo."','".$object->item_code."','".pg_escape_string($object->description)."','".$object->qty_po."','"
				.$object->unit_po."','".$object->po_unit_price."','".$object->po_price."','".$object->flag_gr_ok."','".$object->pr_detail_item."','".$object->qty_pr."') ";

		return $sql;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_po_detail\" SET ";
		$flag = false;
		if($object->item_code != '') { if($flag){ $sql .= ","; } $sql .= " \"item_code\" = '".$object->item_code."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".pg_escape_string($object->description)."' "; $flag = true; }
		if($object->qty_po != '') { if($flag){ $sql .= ","; } $sql .= " \"qty_po\" = '".$object->qty_po."' "; $flag = true; }
		if($object->unit_po != '') { if($flag){ $sql .= ","; } $sql .= " \"unit_po\" = '".$object->unit_po."' "; $flag = true; }
		if($object->po_unit_price != '') { if($flag){ $sql .= ","; } $sql .= " \"po_unit_price\" = '".$object->po_unit_price."' "; $flag = true; }
		if($object->po_price != '') { if($flag){ $sql .= ","; } $sql .= " \"po_price\" = '".$object->po_price."' "; $flag = true; }
		if($object->flag_gr_ok != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_gr_ok\" = '".$object->flag_gr_ok."' "; $flag = true; }		
		if($object->pr_detail_item != '') { if($flag){ $sql .= ","; } $sql .= " \"pr_detail_item\" = '".$object->pr_detail_item."' "; $flag = true; }
		if($object->qty_pr != '') { if($flag){ $sql .= ","; } $sql .= " \"qty_pr\" = '".$object->qty_pr."' "; $flag = true; }
		$sql .= " WHERE \"po_no_immbo\" = '".$object->po_no_immbo."' ";	
		$sql .= " AND \"po_detail_item\" = '".$object->po_detail_item."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}

	public function updateSql($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_po_detail\" SET ";
		$flag = false;
		if($object->item_code != '') { if($flag){ $sql .= ","; } $sql .= " \"item_code\" = '".$object->item_code."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".pg_escape_string($object->description)."' "; $flag = true; }
		if($object->qty_po != '') { if($flag){ $sql .= ","; } $sql .= " \"qty_po\" = '".$object->qty_po."' "; $flag = true; }
		if($object->unit_po != '') { if($flag){ $sql .= ","; } $sql .= " \"unit_po\" = '".$object->unit_po."' "; $flag = true; }
		if($object->po_unit_price != '') { if($flag){ $sql .= ","; } $sql .= " \"po_unit_price\" = '".$object->po_unit_price."' "; $flag = true; }
		if($object->po_price != '') { if($flag){ $sql .= ","; } $sql .= " \"po_price\" = '".$object->po_price."' "; $flag = true; }
		if($object->flag_gr_ok != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_gr_ok\" = '".$object->flag_gr_ok."' "; $flag = true; }		
		if($object->pr_detail_item != '') { if($flag){ $sql .= ","; } $sql .= " \"pr_detail_item\" = '".$object->pr_detail_item."' "; $flag = true; }
		if($object->qty_pr != '') { if($flag){ $sql .= ","; } $sql .= " \"qty_pr\" = '".$object->qty_pr."' "; $flag = true; }
		$sql .= " WHERE \"po_no_immbo\" = '".$object->po_no_immbo."' ";	
		$sql .= " AND \"po_detail_item\" = '".$object->po_detail_item."' ";		
		
		return $sql;
	}
	
	public function goUpdate_PODirect($param){
		$connection = new Postgresql($this->di['db']);
		
		$sql .= " 
			UPDATE t_po_detail as pod
			SET pr_detail_item = prd.pr_detail_item
					
			FROM  
				t_pr_detail as prd,
				t_po as po
			
			WHERE  
				pod.po_no_immbo = po.po_no_immbo
				AND prd.request_no = CONCAT('DPR-', '".$param."')
				AND prd.item_code = pod.item_code
				AND pod.pr_detail_item = 0
		";
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goUpdatePO($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_po_detail\" SET ";
		$flag = false;
		if($object->item_code != '') { if($flag){ $sql .= ","; } $sql .= " \"item_code\" = '".$object->item_code."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".pg_escape_string($object->description)."' "; $flag = true; }
		if($object->qty_po != '') { if($flag){ $sql .= ","; } $sql .= " \"qty_po\" = '".$object->qty_po."' "; $flag = true; }
		if($object->unit_po != '') { if($flag){ $sql .= ","; } $sql .= " \"unit_po\" = '".$object->unit_po."' "; $flag = true; }
		if($object->po_unit_price != '') { if($flag){ $sql .= ","; } $sql .= " \"po_unit_price\" = '".$object->po_unit_price."' "; $flag = true; }
		if($object->po_price != '') { if($flag){ $sql .= ","; } $sql .= " \"po_price\" = '".$object->po_price."' "; $flag = true; }
		if($object->flag_gr_ok != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_gr_ok\" = '".$object->flag_gr_ok."' "; $flag = true; }		
		if($object->pr_detail_item != '') { if($flag){ $sql .= ","; } $sql .= " \"pr_detail_item\" = '".$object->pr_detail_item."' "; $flag = true; }
		if($object->qty_pr != '') { if($flag){ $sql .= ","; } $sql .= " \"qty_pr\" = '".$object->qty_pr."' "; $flag = true; }
		$sql .= " WHERE \"po_no_immbo\" = '".$object->po_no_immbo."' ";	
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_po_detail\" "
				." WHERE \"po_no_immbo\" = '".$object->po_no_immbo."' "
				." AND \"po_detail_item\" = '".$object->po_detail_item."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}

	public function executeQuery($sql){
		$connection = new Postgresql($this->di['db']);
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
