<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class PointTmuk extends Model
{

	public $id;
	public $tgl_berlaku;
	public $konversi;	
	public $faktor_konversi;
	public $faktor_reedem;
	public $status;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"tgl_berlaku\", \"konversi\", \"faktor_konversi\", \"faktor_reedem\", \"status\" "
				." FROM \"m_point_tmuk\" ";				
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['tgl_berlaku'] = $result[1];
			$lists[$count]['konversi'] = $result[2];
			$lists[$count]['faktor_konversi'] = $result[3];
			$lists[$count]['faktor_reedem'] = $result[4];
			$lists[$count]['status'] = $result[5];
			
			$count++;
		}
		
		return $lists;
	}

	public function checkAda($id){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"tgl_berlaku\", \"konversi\", \"faktor_konversi\", \"faktor_reedem\", \"status\" "
				." FROM \"m_point_tmuk\" WHERE id = ".$id." ";				
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = false;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count++;
		}

		if($count>0){
			$lists = true;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql =  "SELECT \"id\", \"point_get\", \"point_use\", \"start_date\", \"end_date\", \"create_by\", \"create_date\" "
				." FROM \"m_point\" "
				." WHERE \"id\" = '".$object->id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['point_get'] = $result[1];
			$lists[$count]['point_use'] = $result[2];
			$lists[$count]['start_date'] = $result[3];
			$lists[$count]['end_date'] = $result[4];
			$lists[$count]['create_by'] = $result[5];
			$lists[$count]['create_date'] = $result[6];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);

		$sql = "SELECT \"id\", \"tgl_berlaku\", \"konversi\", \"faktor_konversi\", \"faktor_reedem\", \"status\" "
				." FROM \"m_point_tmuk\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['tgl_berlaku'] = $result[1];
			$lists[$count]['konversi'] = $result[2];
			$lists[$count]['faktor_konversi'] = $result[3];
			$lists[$count]['faktor_reedem'] = $result[4];
			$lists[$count]['status'] = $result[5];
			
			$count++;
		}
		
		return $lists;
	}

	public function getPointAktif(){
		$connection = new Postgresql($this->di['db']);

		$sql = "SELECT \"id\", \"tgl_berlaku\", \"konversi\", \"faktor_konversi\", \"faktor_reedem\", \"status\" "
				." FROM \"m_point_tmuk\" WHERE status = 1 order by tgl_berlaku asc";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$sekarang = date('Y-m-d');
			if($result[1]<="'".$sekarang."'"){
				$lists[$count]['id'] = $result[0];
				$lists[$count]['tgl_berlaku'] = $result[1];
				$lists[$count]['konversi'] = $result[2];
				$lists[$count]['faktor_konversi'] = $result[3];
				$lists[$count]['faktor_reedem'] = $result[4];
				$lists[$count]['status'] = $result[5];
			}else{
				break;
			}
			
			// $count++;
		}
		
		return $lists;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		// if($object->point_get == '') { $object->point_get = '0'; }
		// if($object->point_use == '') { $object->point_use = '0'; }
		// if($object->start_date == '') { $object->start_date = date('Y-m-d'); }
		// if($object->end_date == '') { $object->end_date = date('Y-m-d'); }		
		// if($object->create_date == '') { $object->create_date = date('Y-m-d'); }
		
		$sql = "INSERT INTO \"m_point_tmuk\" (\"id\",\"tgl_berlaku\", \"konversi\", \"faktor_konversi\", \"faktor_reedem\", \"status\") "
				." VALUES ('".$object->id."','".$object->tgl_berlaku."','".$object->konversi."','".$object->faktor_konversi."','".$object->faktor_reedem."','".$object->status
				."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}	
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_point_tmuk\" SET ";
		$flag = false;
		if($object->tgl_berlaku != '') { if($flag){ $sql .= ","; } $sql .= " \"tgl_berlaku\" = '".$object->tgl_berlaku."' "; $flag = true; }
		if($object->konversi != '') { if($flag){ $sql .= ","; } $sql .= " \"konversi\" = '".$object->konversi."' "; $flag = true; }
		if($object->faktor_konversi != '') { if($flag){ $sql .= ","; } $sql .= " \"faktor_konversi\" = '".$object->faktor_konversi."' "; $flag = true; }
		if($object->faktor_reedem != '') { if($flag){ $sql .= ","; } $sql .= " \"faktor_reedem\" = '".$object->faktor_reedem."' "; $flag = true; }
		if($object->status != '') { if($flag){ $sql .= ","; } $sql .= " \"status\" = '".$object->status."' "; $flag = true; }
		$sql .= " WHERE \"id\" = '".$object->id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_point\" "
				." WHERE \"id\" = '".$object->id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
}
