<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class TundaDetail extends Model
{

	public $id_jual_item;
	public $id_jual;
	public $item_code;
	public $description;	
	public $harga;
	public $harga_member;
	public $qty;	
	public $satuan;	
	public $discount;	
	public $total;	
	public $promo;	
	public $barcode;
	public $disc_percent;
	public $disc_amount;
	public $tax_type;
	public $harga_hpp3;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id_jual_item\", \"id_jual\", \"item_code\", \"description\", \"harga\", \"harga_member\", \"qty\", \"satuan\", \"discount\", \"total\" "
				." , \"promo\", \"barcode\", \"disc_percent\", \"disc_amount\", \"tax_type\", \"harga_hpp3\"  "
				." FROM \"t_tunda_detail\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_jual_item'] = $result[0];
			$lists[$count]['id_jual'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['harga'] = $result[4];			
			$lists[$count]['harga_member'] = $result[5];	
			$lists[$count]['qty'] = $result[6];	
			$lists[$count]['satuan'] = $result[7];	
			$lists[$count]['discount'] = $result[8];	
			$lists[$count]['total'] = $result[9];	
			$lists[$count]['promo'] = $result[10];	
			$lists[$count]['barcode'] = $result[11];
			$lists[$count]['disc_percent'] = $result[12];			
			$lists[$count]['disc_amount'] = $result[13];
			$lists[$count]['tax_type'] = $result[14];
			$lists[$count]['harga_hpp3'] = $result[15];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id_jual_item\", \"id_jual\", \"item_code\", \"description\", \"harga\", \"harga_member\", \"qty\", \"satuan\", \"discount\", \"total\" "
				." , \"promo\", \"barcode\", \"disc_percent\", \"disc_amount\", \"tax_type\", \"harga_hpp3\"  "
				." FROM \"t_tunda_detail\" "
				." WHERE \"id_jual_item\" = '".$object->id_jual_item."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_jual_item'] = $result[0];
			$lists[$count]['id_jual'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['harga'] = $result[4];			
			$lists[$count]['harga_member'] = $result[5];	
			$lists[$count]['qty'] = $result[6];	
			$lists[$count]['satuan'] = $result[7];	
			$lists[$count]['discount'] = $result[8];	
			$lists[$count]['total'] = $result[9];	
			$lists[$count]['promo'] = $result[10];	
			$lists[$count]['barcode'] = $result[11];	
			$lists[$count]['disc_percent'] = $result[12];
			$lists[$count]['disc_amount'] = $result[13];
			$lists[$count]['tax_type'] = $result[14];
			$lists[$count]['harga_hpp3'] = $result[15];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id_jual_item\", \"id_jual\", \"item_code\", \"description\", \"harga\", \"harga_member\", \"qty\", \"satuan\", \"discount\", \"total\" "
				." , \"promo\", \"barcode\", \"disc_percent\", \"disc_amount\", \"tax_type\", \"harga_hpp3\"  "
				." FROM \"t_tunda_detail\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_jual_item'] = $result[0];
			$lists[$count]['id_jual'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['harga'] = $result[4];			
			$lists[$count]['harga_member'] = $result[5];	
			$lists[$count]['qty'] = $result[6];	
			$lists[$count]['satuan'] = $result[7];	
			$lists[$count]['discount'] = $result[8];	
			$lists[$count]['total'] = $result[9];	
			$lists[$count]['promo'] = $result[10];
			$lists[$count]['barcode'] = $result[11];
			$lists[$count]['disc_percent'] = $result[12];
			$lists[$count]['disc_amount'] = $result[13];
			$lists[$count]['tax_type'] = $result[14];
			$lists[$count]['harga_hpp3'] = $result[15];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"id_jual_item\") "
				." FROM \"t_tunda_detail\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->harga == '') { $object->harga = '0'; }
		if($object->harga_member == '') { $object->harga_member = '0'; }
		if($object->qty == '') { $object->qty = '0'; }
		if($object->discount == '') { $object->discount = '0'; }
		if($object->total == '') { $object->total = '0'; }
		if($object->tax_type == '') { $object->tax_type = '0'; }
		if($object->harga_hpp3 == '') { $object->harga_hpp3 = '0'; }
		
		$sql = "INSERT INTO \"t_tunda_detail\" (\"id_jual\", \"item_code\", \"description\", \"harga\", \"harga_member\", \"qty\", \"satuan\", \"discount\", \"total\" "
				.", \"promo\", \"barcode\", \"disc_percent\", \"disc_amount\", \"tax_type\", \"harga_hpp3\" ) "
				." VALUES ('".$object->id_jual."','".$object->item_code."','".pg_escape_string($object->description)."','".$object->harga."','".$object->harga_member."','"
				.$object->qty."','".$object->satuan."','".$object->discount."','".$object->total."','".$object->promo."','".$object->barcode."','"
				.$object->disc_percent."','".$object->disc_amount."','".$object->tax_type."','".$object->harga_hpp3."') ";
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_tunda_detail\" SET ";
		$flag = false;
		if($object->item_code != '') { if($flag){ $sql .= ","; } $sql .= " \"item_code\" = '".$object->item_code."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".pg_escape_string($object->description)."' "; $flag = true; }
		if($object->harga != '') { if($flag){ $sql .= ","; } $sql .= " \"harga\" = '".$object->harga."' "; $flag = true; }
		if($object->harga_member != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_member\" = '".$object->harga_member."' "; $flag = true; }
		if($object->qty != '') { if($flag){ $sql .= ","; } $sql .= " \"qty\" = '".$object->qty."' "; $flag = true; }
		if($object->satuan != '') { if($flag){ $sql .= ","; } $sql .= " \"satuan\" = '".$object->satuan."' "; $flag = true; }
		if($object->discount != '') { if($flag){ $sql .= ","; } $sql .= " \"discount\" = '".$object->discount."' "; $flag = true; }
		if($object->total != '') { if($flag){ $sql .= ","; } $sql .= " \"total\" = '".$object->total."' "; $flag = true; }
		if($object->promo != '') { if($flag){ $sql .= ","; } $sql .= " \"promo\" = '".$object->promo."' "; $flag = true; }		
		if($object->barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"barcode\" = '".$object->barcode."' "; $flag = true; }		
		if($object->disc_percent != '') { if($flag){ $sql .= ","; } $sql .= " \"disc_percent\" = '".$object->disc_percent."' "; $flag = true; }		
		if($object->disc_amount != '') { if($flag){ $sql .= ","; } $sql .= " \"disc_amount\" = '".$object->disc_amount."' "; $flag = true; }		
		if($object->tax_type != '') { if($flag){ $sql .= ","; } $sql .= " \"tax_type\" = '".$object->tax_type."' "; $flag = true; }		
		if($object->harga_hpp3 != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_hpp3\" = '".$object->harga_hpp3."' "; $flag = true; }		
		$sql .= " WHERE \"id_jual_item\" = '".$object->id_jual_item."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goUpdateJual($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_tunda_detail\" SET ";
		$flag = false;
		if($object->item_code != '') { if($flag){ $sql .= ","; } $sql .= " \"item_code\" = '".$object->item_code."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".pg_escape_string($object->description)."' "; $flag = true; }
		if($object->harga != '') { if($flag){ $sql .= ","; } $sql .= " \"harga\" = '".$object->harga."' "; $flag = true; }
		if($object->harga_member != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_member\" = '".$object->harga_member."' "; $flag = true; }
		if($object->qty != '') { if($flag){ $sql .= ","; } $sql .= " \"qty\" = '".$object->qty."' "; $flag = true; }
		if($object->satuan != '') { if($flag){ $sql .= ","; } $sql .= " \"satuan\" = '".$object->satuan."' "; $flag = true; }
		if($object->discount != '') { if($flag){ $sql .= ","; } $sql .= " \"discount\" = '".$object->discount."' "; $flag = true; }
		if($object->total != '') { if($flag){ $sql .= ","; } $sql .= " \"total\" = '".$object->total."' "; $flag = true; }
		if($object->promo != '') { if($flag){ $sql .= ","; } $sql .= " \"promo\" = '".$object->promo."' "; $flag = true; }
		if($object->barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"barcode\" = '".$object->barcode."' "; $flag = true; }
		if($object->disc_percent != '') { if($flag){ $sql .= ","; } $sql .= " \"disc_percent\" = '".$object->disc_percent."' "; $flag = true; }
		if($object->disc_amount != '') { if($flag){ $sql .= ","; } $sql .= " \"disc_amount\" = '".$object->disc_amount."' "; $flag = true; }
		if($object->tax_type != '') { if($flag){ $sql .= ","; } $sql .= " \"tax_type\" = '".$object->tax_type."' "; $flag = true; }		
		if($object->harga_hpp3 != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_hpp3\" = '".$object->harga_hpp3."' "; $flag = true; }		
		$sql .= " WHERE \"id_jual\" = '".$object->id_jual."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_tunda_detail\" "
				." WHERE \"id_jual_item\" = '".$object->id_jual_item."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goDeleteJual($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_tunda_detail\" "
				." WHERE \"id_jual\" = '".$object->id_jual."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function getJoin_Product($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT td.\"id_jual_item\", td.\"id_jual\", td.\"item_code\", td.\"description\", td.\"harga\", td.\"harga_member\", td.\"qty\", td.\"satuan\", td.\"discount\", td.\"total\" "
				." , td.\"promo\", td.\"barcode\", td.\"disc_percent\", td.\"disc_amount\", td.\"tax_type\", td.\"harga_hpp3\", mp.\"ppob\"  "
				." FROM \"t_tunda_detail\" td INNER JOIN \"m_product\" mp ON td.item_code = mp.stock_id ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_jual_item'] = $result[0];
			$lists[$count]['id_jual'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['harga'] = $result[4];			
			$lists[$count]['harga_member'] = $result[5];	
			$lists[$count]['qty'] = $result[6];	
			$lists[$count]['satuan'] = $result[7];	
			$lists[$count]['discount'] = $result[8];	
			$lists[$count]['total'] = $result[9];	
			$lists[$count]['promo'] = $result[10];
			$lists[$count]['barcode'] = $result[11];
			$lists[$count]['disc_percent'] = $result[12];
			$lists[$count]['disc_amount'] = $result[13];
			$lists[$count]['tax_type'] = $result[14];
			$lists[$count]['harga_hpp3'] = $result[15];
			$lists[$count]['ppob'] = $result[16];
			
			$count++;
		}
		
		return $lists;
	}
}
