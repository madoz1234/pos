<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Rack extends Model
{
	public $id;
	public $rack_type;
	public $tinggi;	
	public $panjang;
	public $lebar;
	public $shelving;
	public $hunger;
	public $tinggi_hunger;
	public $panjang_hunger;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"id\", \"rack_type\", \"tinggi\", \"panjang\", \"lebar\", \"shelving\", \"hunger\", \"tinggi_hunger\", 
				\"panjang_hunger\" "
				." FROM \"m_rack\" ";
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['rack_type'] = $result[1];
			$lists[$count]['tinggi'] = $result[2];
			$lists[$count]['panjang'] = $result[3];
			$lists[$count]['lebar'] = $result[4];
			$lists[$count]['shelving'] = $result[5];
			$lists[$count]['hunger'] = $result[6];
			$lists[$count]['tinggi_hunger'] = $result[7];
			$lists[$count]['panjang_hunger'] = $result[8];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"id\", \"rack_type\", \"tinggi\", \"panjang\", \"lebar\", \"shelving\", \"hunger\", \"tinggi_hunger\", 
				\"panjang_hunger\" "
				." FROM \"m_rack\" "
				." WHERE \"id\" = '".$object->id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['rack_type'] = $result[1];
			$lists[$count]['tinggi'] = $result[2];
			$lists[$count]['panjang'] = $result[3];
			$lists[$count]['lebar'] = $result[4];
			$lists[$count]['shelving'] = $result[5];
			$lists[$count]['hunger'] = $result[6];
			$lists[$count]['tinggi_hunger'] = $result[7];
			$lists[$count]['panjang_hunger'] = $result[8];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"id\", \"rack_type\", \"tinggi\", \"panjang\", \"lebar\", \"shelving\", \"hunger\", \"tinggi_hunger\", 
				\"panjang_hunger\" "
				." FROM \"m_rack\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['rack_type'] = $result[1];
			$lists[$count]['tinggi'] = $result[2];
			$lists[$count]['panjang'] = $result[3];
			$lists[$count]['lebar'] = $result[4];
			$lists[$count]['shelving'] = $result[5];
			$lists[$count]['hunger'] = $result[6];
			$lists[$count]['tinggi_hunger'] = $result[7];
			$lists[$count]['panjang_hunger'] = $result[8];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"id\") "
				." FROM \"m_rack\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->inactive == '') { $object->inactive = '0'; }
		
		$sql = "INSERT INTO \"m_rack\" (\"id\", \"rack_type\", \"tinggi\", \"panjang\", \"lebar\", \"shelving\", \"hunger\", 
				\"tinggi_hunger\", \"panjang_hunger\") "
				." VALUES ('".$object->id."','".$object->rack_type."','".$object->tinggi."','".$object->panjang.
				"','".$object->lebar."','".$object->shelving."','".$object->hunger."','".$object->tinggi_hunger."','".$object->panjang_hunger."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goMultiInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "INSERT INTO \"m_rack\" (\"id\", \"rack_type\", \"tinggi\", \"panjang\", \"lebar\", \"shelving\", \"hunger\", 
				\"tinggi_hunger\", \"panjang_hunger\") "
			." VALUES ".$object;
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_rack\" SET ";
		$flag = false;
		if($object->rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"rack_type\" = '".$object->rack_type."' "; $flag = true; }
		if($object->tinggi != '') { if($flag){ $sql .= ","; } $sql .= " \"tinggi\" = '".$object->tinggi."' "; $flag = true; }
		if($object->panjang != '') { if($flag){ $sql .= ","; } $sql .= " \"panjang\" = '".$object->panjang."' "; $flag = true; }
		if($object->lebar != '') { if($flag){ $sql .= ","; } $sql .= " \"lebar\" = '".$object->lebar."' "; $flag = true; }
		if($object->shelving != '') { if($flag){ $sql .= ","; } $sql .= " \"shelving\" = '".$object->shelving."' "; $flag = true; }
		if($object->hunger != '') { if($flag){ $sql .= ","; } $sql .= " \"hunger\" = '".$object->hunger."' "; $flag = true; }
		if($object->tinggi_hunger != '') { if($flag){ $sql .= ","; } $sql .= " \"tinggi_hunger\" = '".$object->tinggi_hunger."' "; $flag = true; }
		if($object->panjang_hunger != '') { if($flag){ $sql .= ","; } $sql .= " \"panjang_hunger\" = '".$object->panjang_hunger."' "; $flag = true; }
		$sql .= " WHERE \"id\" = '".$object->id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_rack\" "
				." WHERE \"id\" = '".$object->id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goMultiDelete($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " DELETE FROM \"m_rack\" ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
}
