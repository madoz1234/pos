<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class TMUK extends Model
{

	public $tmuk_id;
	public $tmuk;
	public $supplier_id;
	public $escrow_account;
	public $saldo;
	public $address1;
	public $address2;
	public $address3;
	public $telp;
	public $name;
	public $owner;
	public $tipe;	
	public $printer;	
	public $asn;	
	public $length_struk;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"tmuk_id\", \"tmuk\", \"supplier_id\", \"escrow_account\", \"saldo\", \"telp\", \"name\", \"owner\", \"address1\", \"address2\", \"address3\", \"tipe\", \"printer\", \"asn\", \"length_struk\", \"auto_approve\", \"kode_bank\", \"nama_bank\", \"nama_owner\", \"nama_lsi\" FROM \"m_tmuk\" ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['tmuk_id'] = $result[0];
			$lists[$count]['tmuk'] = $result[1];
			$lists[$count]['supplier_id'] = $result[2];
			$lists[$count]['escrow_account'] = $result[3];
			$lists[$count]['saldo'] = $result[4];
			$lists[$count]['telp'] = $result[5];
			$lists[$count]['name'] = $result[6];
			$lists[$count]['owner'] = $result[7];
			$lists[$count]['address1'] = $result[8];
			$lists[$count]['address2'] = $result[9];
			$lists[$count]['address3'] = $result[10];
			$lists[$count]['tipe'] = $result[11];
			$lists[$count]['printer'] = $result[12];
			$lists[$count]['asn'] = $result[13];
			$lists[$count]['length_struk'] = $result[14];
			$lists[$count]['auto_approve'] = $result[15];
			$lists[$count]['kode_bank'] = $result[16];
			$lists[$count]['nama_bank'] = $result[17];
			$lists[$count]['nama_owner'] = $result[18];
			$lists[$count]['nama_lsi'] = $result[19];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"tmuk_id\", \"tmuk\", \"supplier_id\", \"escrow_account\", \"saldo\", \"telp\", \"name\", \"owner\", \"address1\", \"address2\", \"address3\", \"tipe\", \"printer\", \"asn\", \"length_struk\", \"auto_approve\", \"kode_bank\", \"nama_bank\", \"nama_owner\", \"nama_lsi\" FROM \"m_tmuk\" WHERE \"tmuk_id\" = '".$object->tmuk_id."' LIMIT 1 " ;

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['tmuk_id'] = $result[0];
			$lists[$count]['tmuk'] = $result[1];
			$lists[$count]['supplier_id'] = $result[2];
			$lists[$count]['escrow_account'] = $result[3];
			$lists[$count]['saldo'] = $result[4];
			$lists[$count]['telp'] = $result[5];
			$lists[$count]['name'] = $result[6];
			$lists[$count]['owner'] = $result[7];
			$lists[$count]['address1'] = $result[8];
			$lists[$count]['address2'] = $result[9];
			$lists[$count]['address3'] = $result[10];
			$lists[$count]['tipe'] = $result[11];
			$lists[$count]['printer'] = $result[12];
			$lists[$count]['asn'] = $result[13];
			$lists[$count]['length_struk'] = $result[14];
			$lists[$count]['kode_bank'] = $result[15];
			$lists[$count]['nama_bank'] = $result[16];
			$lists[$count]['nama_owner'] = $result[17];
			$lists[$count]['nama_lsi'] = $result[18];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"tmuk_id\", \"tmuk\", \"supplier_id\", \"escrow_account\", \"saldo\", \"telp\", \"name\", \"owner\", \"address1\", \"address2\", \"address3\", \"tipe\", \"printer\", \"asn\", \"length_struk\", \"auto_approve\", \"kode_bank\", \"nama_bank\", \"nama_owner\", \"nama_lsi\" FROM \"m_tmuk\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['tmuk_id'] = $result[0];
			$lists[$count]['tmuk'] = $result[1];
			$lists[$count]['supplier_id'] = $result[2];
			$lists[$count]['escrow_account'] = $result[3];
			$lists[$count]['saldo'] = $result[4];
			$lists[$count]['telp'] = $result[5];
			$lists[$count]['name'] = $result[6];
			$lists[$count]['owner'] = $result[7];
			$lists[$count]['address1'] = $result[8];
			$lists[$count]['address2'] = $result[9];
			$lists[$count]['address3'] = $result[10];			
			$lists[$count]['tipe'] = $result[11];
			$lists[$count]['printer'] = $result[12];
			$lists[$count]['asn'] = $result[13];
			$lists[$count]['length_struk'] = $result[14];
			$lists[$count]['auto_approve'] = $result[15];
			$lists[$count]['nama_lsi'] = $result[16];
			$lists[$count]['length_struk'] = $result[17];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"tmuk_id\") "
				." FROM \"m_tmuk\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);	

		$sql = "INSERT INTO \"m_tmuk\" (\"tmuk\", \"supplier_id\", \"escrow_account\", \"saldo\", \"telp\", \"name\", \"owner\", \"address1\", \"address2\", \"address3\", \"tipe\", \"printer\", \"asn\", \"auto_approve\", \"kode_bank\", \"nama_bank\", \"nama_owner\", \"nama_lsi\", \"length_struk\") "
				." VALUES ('".$object->tmuk."','".$object->supplier_id."','".$object->escrow_account."','".$object->saldo."','".$object->telp."','".$object->name."','".$object->owner."','".$object->address1."','".$object->address2."','".$object->address3."','".$object->tipe."','".$object->printer."','".$object->asn."','".$object->auto_approve."','".$object->kode_bank."','".$object->nama_bank."','".$object->nama_owner."','".$object->nama_lsi."','".$object->length_struk."') ";
		
		$success = $connection->execute($sql);		
		
		return $sql;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		$sql = " UPDATE \"m_tmuk\" SET ";
		$flag = false;	
		if($object->tmuk != '') { if($flag){ $sql .= ","; } $sql .= " \"tmuk\" = '".$object->tmuk."' "; $flag = true; }
		if($object->supplier_id != '') { if($flag){ $sql .= ","; } $sql .= " \"supplier_id\" = '".$object->supplier_id."' "; $flag = true; }		
		if($object->escrow_account != '') { if($flag){ $sql .= ","; } $sql .= " \"escrow_account\" = '".$object->escrow_account."' "; $flag = true; }		
		if($object->saldo != '') { if($flag){ $sql .= ","; } $sql .= " \"saldo\" = '".$object->saldo."' "; $flag = true; }
		if($object->telp != '') { if($flag){ $sql .= ","; } $sql .= " \"telp\" = '".$object->telp."' "; $flag = true; }
		if($object->name != '') { if($flag){ $sql .= ","; } $sql .= " \"name\" = '".pg_escape_string($object->name)."' "; $flag = true; }
		if($object->owner != '') { if($flag){ $sql .= ","; } $sql .= " \"owner\" = '".$object->owner."' "; $flag = true; }
		if($object->address1 != '') { if($flag){ $sql .= ","; } $sql .= " \"address1\" = '".$object->address1."' "; $flag = true; }
		if($object->address2 != '') { if($flag){ $sql .= ","; } $sql .= " \"address2\" = '".$object->address2."' "; $flag = true; }
		if($object->address3 != '') { if($flag){ $sql .= ","; } $sql .= " \"address3\" = '".$object->address3."' "; $flag = true; }
		if($object->tipe != '') { if($flag){ $sql .= ","; } $sql .= " \"tipe\" = '".$object->tipe."' "; $flag = true; }
		if($object->printer != '') { if($flag){ $sql .= ","; } $sql .= " \"printer\" = '".$object->printer."' "; $flag = true; }
		if($object->asn != '') { if($flag){ $sql .= ","; } $sql .= " \"asn\" = '".$object->asn."' "; $flag = true; }
		if($object->length_struk != '') { if($flag){ $sql .= ","; } $sql .= " \"length_struk\" = '".$object->length_struk."' "; $flag = true; }
		if($object->auto_approve != '') { if($flag){ $sql .= ","; } $sql .= " \"auto_approve\" = '".$object->auto_approve."' "; $flag = true; }
		if($object->kode_bank != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_bank\" = '".$object->kode_bank."' "; $flag = true; }
		if($object->nama_bank != '') { if($flag){ $sql .= ","; } $sql .= " \"nama_bank\" = '".$object->nama_bank."' "; $flag = true; }
		if($object->nama_owner != '') { if($flag){ $sql .= ","; } $sql .= " \"nama_owner\" = '".$object->nama_owner."' "; $flag = true; }
		if($object->nama_lsi != '') { if($flag){ $sql .= ","; } $sql .= " \"nama_lsi\" = '".$object->nama_lsi."' "; $flag = true; }
		$sql .= " WHERE \"tmuk\" = '".$object->tmuk."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goUpdateSync($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_tmuk\" SET ";
		$flag = false;				
		if($object->supplier_id != '') { if($flag){ $sql .= ","; } $sql .= " \"supplier_id\" = '".$object->supplier_id."' "; $flag = true; }		
		if($object->escrow_account != '') { if($flag){ $sql .= ","; } $sql .= " \"escrow_account\" = '".$object->escrow_account."' "; $flag = true; }		
		if($object->saldo != '') { if($flag){ $sql .= ","; } $sql .= " \"saldo\" = '".$object->saldo."' "; $flag = true; }
		if($object->telp != '') { if($flag){ $sql .= ","; } $sql .= " \"telp\" = '".$object->telp."' "; $flag = true; }
		if($object->name != '') { if($flag){ $sql .= ","; } $sql .= " \"name\" = '".$object->name."' "; $flag = true; }
		if($object->owner != '') { if($flag){ $sql .= ","; } $sql .= " \"owner\" = '".$object->owner."' "; $flag = true; }
		if($object->address1 != '') { if($flag){ $sql .= ","; } $sql .= " \"address1\" = '".$object->address1."' "; $flag = true; }
		if($object->address2 != '') { if($flag){ $sql .= ","; } $sql .= " \"address2\" = '".$object->address2."' "; $flag = true; }
		if($object->address3 != '') { if($flag){ $sql .= ","; } $sql .= " \"address3\" = '".$object->address3."' "; $flag = true; }
		if($object->tipe != '') { if($flag){ $sql .= ","; } $sql .= " \"tipe\" = '".$object->tipe."' "; $flag = true; }
		if($object->asn != '') { if($flag){ $sql .= ","; } $sql .= " \"asn\" = '".$object->asn."' "; $flag = true; }
		if($object->nama_lsi != '') { if($flag){ $sql .= ","; } $sql .= " \"nama_lsi\" = '".$object->nama_lsi."' "; $flag = true; }
		if($object->length_struk != '') { if($flag){ $sql .= ","; } $sql .= " \"length_struk\" = '".$object->length_struk."' "; $flag = true; }
		$sql .= " WHERE \"tmuk\" = '".$object->tmuk."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_tmuk\" "
				." WHERE \"tmuk_id\" = '".$object->tmuk_id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function getJoin_Supplier(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"tmuk_id\", \"tmuk\", mt.\"supplier_id\", mt.\"escrow_account\", \"saldo\", \"telp\", \"name\", \"owner\", \"address1\", \"address2\", \"address3\", ms.\"supp_name\", mt.\"nama_lsi\", mt.\"length_struk\" "
				." FROM \"m_tmuk\" mt INNER JOIN \"m_supplier\" ms ON mt.supplier_id = ms.supplier_id::varchar ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['tmuk_id'] = $result[0];
			$lists[$count]['tmuk'] = $result[1];
			$lists[$count]['supplier_id'] = $result[2];
			$lists[$count]['escrow_account'] = $result[3];
			$lists[$count]['saldo'] = $result[4];
			$lists[$count]['telp'] = $result[5];
			$lists[$count]['name'] = $result[6];
			$lists[$count]['owner'] = $result[7];
			$lists[$count]['address1'] = $result[8];
			$lists[$count]['address2'] = $result[9];
			$lists[$count]['address3'] = $result[10];
			$lists[$count]['supp_name'] = $result[11];
			$lists[$count]['nama_lsi'] = $result[12];
			$lists[$count]['length_struk'] = $result[13];
			
			$count++;
		}
		
		return $lists;
	}

}
