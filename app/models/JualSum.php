<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class JualSum extends Model
{

	public $id_kasir;
	public $tanggal;	
	public $kassa_id;	
	public $shift_id;	
	public $subtotal;
	public $tax;
	public $discount;	
	public $total;	
	public $flag_sync;	
	public $kembalian;
	public $sales_no_immbo;
	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"id_kasir\", \"tanggal\", \"kassa_id\", \"shift_id\", \"subtotal\", \"tax\", \"discount\", \"total\", \"flag_sync\", \"kembalian\", \"sales_no_immbo\", \"kas_akhir\" "
			." FROM \"t_jual_sum\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['id_kasir'] = $result[1];
			$lists[$count]['tanggal'] = $result[2];
			$lists[$count]['kassa_id'] = $result[3];	
			$lists[$count]['shift_id'] = $result[4];	
			$lists[$count]['subtotal'] = $result[5];			
			$lists[$count]['tax'] = $result[6];	
			$lists[$count]['discount'] = $result[7];	
			$lists[$count]['total'] = $result[8];		
			$lists[$count]['flag_sync'] = json_encode($result[9]);	
			$lists[$count]['kembalian'] = $result[10];	
			$lists[$count]['kas_akhir'] = $result[10];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"id_kasir\", \"tanggal\", \"kassa_id\", \"shift_id\", \"subtotal\", \"tax\", \"discount\", \"total\", \"flag_sync\", \"kembalian\", \"sales_no_immbo\", \"kas_akhir\" "
			." FROM \"t_jual_sum\" "
				." WHERE \"id\" = '".$object->id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['id_kasir'] = $result[1];
			$lists[$count]['tanggal'] = $result[2];
			$lists[$count]['kassa_id'] = $result[3];	
			$lists[$count]['shift_id'] = $result[4];	
			$lists[$count]['subtotal'] = $result[5];			
			$lists[$count]['tax'] = $result[6];	
			$lists[$count]['discount'] = $result[7];	
			$lists[$count]['total'] = $result[8];		
			$lists[$count]['flag_sync'] = json_encode($result[9]);	
			$lists[$count]['kembalian'] = $result[10];	
			$lists[$count]['kas_akhir'] = $result[11];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"id_kasir\", \"tanggal\", \"kassa_id\", \"shift_id\", \"subtotal\", \"tax\", \"discount\", \"total\", \"flag_sync\", \"kembalian\", \"sales_no_immbo\", \"kas_akhir\" "
			." FROM \"t_jual_sum\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['id_kasir'] = $result[1];
			$lists[$count]['tanggal'] = $result[2];
			$lists[$count]['kassa_id'] = $result[3];	
			$lists[$count]['shift_id'] = $result[4];	
			$lists[$count]['subtotal'] = $result[5];			
			$lists[$count]['tax'] = $result[6];	
			$lists[$count]['discount'] = $result[7];	
			$lists[$count]['total'] = $result[8];		
			$lists[$count]['flag_sync'] = json_encode($result[9]);	
			$lists[$count]['kembalian'] = $result[10];	
			$lists[$count]['kas_akhir'] = $result[11];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"id\") "
				." FROM \"t_jual_sum\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {              
			$count = $result[0];
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->tanggal == '') { $object->tanggal = date("Y-m-d"); }
		if($object->subtotal == '') { $object->subtotal = '0'; }
		if($object->tax == '') { $object->tax = '0'; }
		if($object->discount == '') { $object->discount = '0'; }
		if($object->total == '') { $object->total = '0'; }
		if($object->flag_sync == '') { $object->flag_sync = 'f'; }
		
		$sql = "INSERT INTO \"t_jual_sum\" (\"id_kasir\", \"tanggal\", \"kassa_id\", \"shift_id\", \"subtotal\", \"tax\", \"discount\", \"total\", \"flag_sync\", \"kembalian\", \"kas_akhir\") "
				." VALUES ('".$object->id_kasir."','".$object->tanggal."','".$object->kassa_id."','".$object->shift_id."','".$object->subtotal."','"
				.$object->tax."','".$object->discount."','".$object->total."','".$object->flag_sync."','".$object->kembalian."','".$object->kas_akhir."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_jual_sum\" SET ";
		$flag = false;
		if($object->id_kasir != '') { if($flag){ $sql .= ","; } $sql .= " \"id_kasir\" = '".$object->id_kasir."' "; $flag = true; }
		if($object->tanggal != '') { if($flag){ $sql .= ","; } $sql .= " \"tanggal\" = '".$object->tanggal."' "; $flag = true; }
		if($object->subtotal != '') { if($flag){ $sql .= ","; } $sql .= " \"subtotal\" = '".$object->subtotal."' "; $flag = true; }
		if($object->tax != '') { if($flag){ $sql .= ","; } $sql .= " \"tax\" = '".$object->tax."' "; $flag = true; }
		if($object->discount != '') { if($flag){ $sql .= ","; } $sql .= " \"discount\" = '".$object->discount."' "; $flag = true; }
		if($object->total != '') { if($flag){ $sql .= ","; } $sql .= " \"total\" = '".$object->total."' "; $flag = true; }		
		if($object->flag_sync != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = '".$object->flag_sync."' "; $flag = true; }
		if($object->kembalian != '') { if($flag){ $sql .= ","; } $sql .= " \"kembalian\" = '".$object->kembalian."' "; $flag = true; }
		if($object->kassa_id != '') { if($flag){ $sql .= ","; } $sql .= " \"kassa_id\" = '".$object->kassa_id."' "; $flag = true; }
		if($object->shift_id != '') { if($flag){ $sql .= ","; } $sql .= " \"shift_id\" = '".$object->shift_id."' "; $flag = true; }
		$sql .= " WHERE \"id\" = ".$object->id." ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_jual_sum\" "
				." WHERE \"id\" = ".$object->id." ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goUpdateJual($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_jual_sum\" SET ";
		$flag = false;
		if($object->flag_sync != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = '".$object->flag_sync."' "; $flag = true; }
		if($object->sales_no_immbo != '') { if($flag){ $sql .= ","; } $sql .= " \"sales_no_immbo\" = '".$object->sales_no_immbo."' "; $flag = true; }
		$sql .= " WHERE \"id\" = '".$object->id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goUpdate_flagSync($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_jual_sum\" SET ";
		
		$flag = false;
		if($object->tanggal != '') { if($flag){ $sql .= ","; } $sql .= " \"tanggal\" = '".$object->tanggal."' "; $flag = true; }
		if($object->flag_sync != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = '".$object->flag_sync."' "; $flag = true; }
		$sql .= " WHERE \"tanggal\" = '".$object->tanggal."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function Summarize($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"tanggal\", sum(\"subtotal\") as sum_s, sum(\"tax\") as sum_tx, sum(\"discount\") as sum_d, sum(\"total\") as sum_t, sum(\"kembalian\") as sum_k, id_kasir, kassa_id, shift_id "
			." FROM \"t_jual_sum\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['tanggal'] = $result[0];
			$lists[$count]['sum_s'] = $result[1];
			$lists[$count]['sum_tx'] = $result[2];
			$lists[$count]['sum_d'] = $result[3];	
			$lists[$count]['sum_t'] = $result[4];	
			$lists[$count]['sum_k'] = $result[5];			
			$lists[$count]['id_kasir'] = $result[6];			
			$lists[$count]['kassa_id'] = $result[7];			
			$lists[$count]['shift_id'] = $result[8];			
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getJoin_StartEndShift($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT ses.tanggal, ses.user_id, ses.kassa_desc, ses.shift_id, ses.setoran, ses.cash_cashier, tjs.total "
			." FROM \"t_start_end_shift\" ses INNER JOIN \"t_jual_sum\" tjs ON ses.tanggal = tjs.tanggal AND "
			." ses.user_id = tjs.id_kasir AND ses.kassa_id = tjs.kassa_id::varchar AND ses.shift_id = tjs.shift_id::varchar ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['tanggal'] = $result[0];	
			$lists[$count]['user_id'] = $result[1];			
			$lists[$count]['kassa_desc'] = $result[2];			
			$lists[$count]['shift_id'] = $result[3];			
			$lists[$count]['setoran'] = $result[4];			
			$lists[$count]['cash_cashier'] = $result[5];			
			$lists[$count]['total'] = $result[6];			
			
			$count++;
		}
		
		return $lists;
	}
}
