<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Promo extends Model
{

	public $unit;
	public $name;
	public $decimals;
	public $inactive;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"kode_promosi\", \"nama_promosi\", \"from_\", \"to_\", \"loc_code\", \"jenis_promo\", \"amount\", \"flag\" "
				." FROM m_promosi ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['kode_promosi'] = $result[1];
			$lists[$count]['nama_promosi'] = $result[2];
			$lists[$count]['from_'] = $result[3];	
			$lists[$count]['to_'] = $result[4];	
			$lists[$count]['loc_code'] = $result[5];	
			$lists[$count]['jenis_promo'] = $result[6];	
			$lists[$count]['amount'] = $result[7];	
			$lists[$count]['flag'] = $result[8];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getAll_PrBrg(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"kode_promosi\", \"stock_id\", \"qty\", \"discount_max\", \"discount_amount\", \"flag\" "
				." FROM m_promosi_barang ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['kode_promosi'] = $result[1];
			$lists[$count]['stock_id'] = $result[2];
			$lists[$count]['qty'] = $result[3];	
			$lists[$count]['discount_max'] = $result[4];	
			$lists[$count]['discount_amount'] = $result[5];	
			$lists[$count]['flag'] = $result[6];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getAll_PrHdh(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"kode_promosi\", \"stock_id\", \"qty\" "
				." FROM m_promosi_hadiah ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['kode_promosi'] = $result[1];
			$lists[$count]['stock_id'] = $result[2];
			$lists[$count]['qty'] = $result[3];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getAll_PrLoc(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"kode_promosi\", \"location\" "
				." FROM m_promosi_location ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['kode_promosi'] = $result[1];
			$lists[$count]['location'] = $result[2];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getAll_PrPwp(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"kode_promosi\", \"stock_id\", \"price\" "
				." FROM m_promosi_pwp ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['kode_promosi'] = $result[1];
			$lists[$count]['stock_id'] = $result[2];
			$lists[$count]['price'] = $result[3];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object, $param){
		$connection = new Postgresql($this->di['db']);
		$count = 0;
		$lists = null;
		
		if($param == 'Barang'){	
			$sql = "SELECT \"id\", \"kode_promosi\", \"stock_id\", \"qty\", \"discount_max\", \"discount_amount\", \"flag\" "
				." FROM m_promosi_barang "
				." WHERE id = '".$object->id."' "
				." LIMIT 1 ";
		
			$results = $connection->query($sql);
			$results->setFetchMode(Phalcon\Db::FETCH_NUM);
			while ($result = $results->fetchArray()) {
				$lists[$count]['id'] = $result[0];
				$lists[$count]['kode_promosi'] = $result[1];
				$lists[$count]['stock_id'] = $result[2];
				$lists[$count]['qty'] = $result[3];	
				$lists[$count]['discount_max'] = $result[4];	
				$lists[$count]['discount_amount'] = $result[5];		
				$lists[$count]['flag'] = $result[6];		
				
				$count++;
			}
				
		}else if($param == 'Hadiah'){	
			$sql = "SELECT \"id\", \"kode_promosi\", \"stock_id\", \"qty\" "
				." FROM m_promosi_hadiah "
				." WHERE id = '".$object->id."' "
				." LIMIT 1 ";
		
			$results = $connection->query($sql);
			$results->setFetchMode(Phalcon\Db::FETCH_NUM);
			while ($result = $results->fetchArray()) {
				$lists[$count]['id'] = $result[0];
				$lists[$count]['kode_promosi'] = $result[1];
				$lists[$count]['stock_id'] = $result[2];
				$lists[$count]['qty'] = $result[3];			
				
				$count++;
			}
				
		}else if($param == 'Location'){
			$sql = "SELECT \"id\", \"kode_promosi\", \"location\" "
				." FROM m_promosi_location "
				." WHERE id = '".$object->id."' "
				." LIMIT 1 ";
		
			$results = $connection->query($sql);
			$results->setFetchMode(Phalcon\Db::FETCH_NUM);
			while ($result = $results->fetchArray()) {
				$lists[$count]['id'] = $result[0];
				$lists[$count]['kode_promosi'] = $result[1];
				$lists[$count]['location'] = $result[2];	
				
				$count++;
			}
				
		}else if($param == 'PWP'){
			$sql = "SELECT \"id\", \"kode_promosi\", \"stock_id\", \"price\" "
				." FROM m_promosi_pwp "
				." WHERE id = '".$object->id."' "
				." LIMIT 1 ";
		
			$results = $connection->query($sql);
			$results->setFetchMode(Phalcon\Db::FETCH_NUM);
			while ($result = $results->fetchArray()) {
				$lists[$count]['id'] = $result[0];
				$lists[$count]['kode_promosi'] = $result[1];
				$lists[$count]['stock_id'] = $result[2];
				$lists[$count]['price'] = $result[3];			
				
				$count++;
			}
				
		}else{
			$sql = "SELECT \"id\", \"kode_promosi\", \"nama_promosi\", \"from_\", \"to_\", \"loc_code\", \"jenis_promo\", \"amount\" "
				." FROM m_promosi "
				." WHERE id = '".$object->id."' "
				." LIMIT 1 ";
			
			$results = $connection->query($sql);
			$results->setFetchMode(Phalcon\Db::FETCH_NUM);
			while ($result = $results->fetchArray()) {
				$lists[$count]['id'] = $result[0];
				$lists[$count]['kode_promosi'] = $result[1];
				$lists[$count]['nama_promosi'] = $result[2];
				$lists[$count]['from_'] = $result[3];	
				$lists[$count]['to_'] = $result[4];	
				$lists[$count]['loc_code'] = $result[5];	
				$lists[$count]['jenis_promo'] = $result[6];	
				$lists[$count]['amount'] = $result[7];			
				
				$count++;
			}
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition, $param){
		$connection = new Postgresql($this->di['db']);		
		$sql = " SELECT \"id\", \"kode_promosi\", \"nama_promosi\", \"from_\", \"to_\", \"flag\" FROM \"m_promosi\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['kode_promosi'] = $result[1];
			$lists[$count]['nama_promosi'] = $result[2];
			$lists[$count]['from_'] = $result[3];
			$lists[$count]['to_'] = $result[4];
			$lists[$count]['flag'] = $result[5];
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"kode_promosi\", \"nama_promosi\", \"from_\", \"to_\", \"loc_code\", \"jenis_promo\", \"amount\", \"flag\" "
				." FROM m_promosi ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object, $param){
		$connection = new Postgresql($this->di['db']);
		$sql = "INSERT INTO \"m_promosi\" (\"id\", \"kode_promosi\", \"nama_promosi\", \"from_\", \"to_\", \"flag\") "
				." VALUES ('".$object->id."','".$object->kode_promosi."','".$object->nama_promosi."','".$object->from_."','".$object->to_."','".$object->flag."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goMultiInsert($object, $param){
		$connection = new Postgresql($this->di['db']);
				
		if($param == 'Barang'){		
			$sql = "INSERT INTO \"m_promosi_barang\" (\"id\", \"kode_promosi\", \"stock_id\", \"qty\", \"discount_max\", \"discount_amount\") "
				." VALUES ".$object;
				
		}else if($param == 'Hadiah'){		
			$sql = "INSERT INTO \"m_promosi_hadiah\" (\"id\", \"kode_promosi\", \"stock_id\", \"qty\") "
				." VALUES ".$object;
				
		}else if($param == 'Location'){
			$sql = "INSERT INTO \"m_promosi_location\" (\"id\", \"kode_promosi\", \"location\") "
				." VALUES ".$object;
				
		}else if($param == 'PWP'){
			$sql = "INSERT INTO \"m_promosi_pwp\" (\"id\", \"kode_promosi\", \"stock_id\", \"price\") "
				." VALUES ".$object;
				
		}else{
			$sql = "INSERT INTO \"m_promosi\" (\"id\", \"kode_promosi\", \"nama_promosi\", \"from_\", \"to_\", \"loc_code\", \"jenis_promo\", \"amount\") "
				." VALUES ".$object;
		}
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object, $param){
		$connection = new Postgresql($this->di['db']);
		$sql = " UPDATE \"m_promosi\" SET ";
		$flag = false;	
		if($object->kode_promosi != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_promosi\" = '".$object->kode_promosi."' "; $flag = true; }
		if($object->nama_promosi != '') { if($flag){ $sql .= ","; } $sql .= " \"nama_promosi\" = '".$object->nama_promosi."' "; $flag = true; }		
		if($object->from_ != '') { if($flag){ $sql .= ","; } $sql .= " \"from_\" = '".$object->from_."' "; $flag = true; }		
		if($object->to_ != '') { if($flag){ $sql .= ","; } $sql .= " \"to_\" = '".$object->to_."' "; $flag = true; }
		if($object->flag != '') { if($flag){ $sql .= ","; } $sql .= " \"flag\" = '".$object->flag."' "; $flag = true; }
		$sql .= " WHERE \"id\" = '".$object->id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object, $param){
		$connection = new Postgresql($this->di['db']);
		
		if($param == 'Barang'){
			$sql = " DELETE FROM \"m_promosi_barang\" "
				." WHERE \"id\" = '".$object->id."' ";
		}else if($param == 'Hadiah'){
			$sql = " DELETE FROM \"m_promosi_hadiah\" "
				." WHERE \"id\" = '".$object->id."' ";
		}else if($param == 'Location'){
			$sql = " DELETE FROM \"m_promosi_location\" "
				." WHERE \"id\" = '".$object->id."' ";
		}else if($param == 'PWP'){
			$sql = " DELETE FROM \"m_promosi_pwp\" "
				." WHERE \"id\" = '".$object->id."' ";
		}else{
			$sql = " DELETE FROM \"m_promosi\" "
				." WHERE \"id\" = '".$object->id."' ";
		}	
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goMultiDelete($object, $param){
		$connection = new Postgresql($this->di['db']);
		
		if($param == 'Barang'){
			$sql = " DELETE FROM \"m_promosi_barang\" "
				." ";
		}else if($param == 'Hadiah'){
			$sql = " DELETE FROM \"m_promosi_hadiah\" "
				." ";
		}else if($param == 'Location'){
			$sql = " DELETE FROM \"m_promosi_location\" "
				." ";
		}else if($param == 'PWP'){
			$sql = " DELETE FROM \"m_promosi_pwp\" "
				." ";
		}else{
			$sql = " DELETE FROM \"m_promosi\" "
				." ";
		}	
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function getJoin_PrBrg($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT p.\"id\", p.\"kode_promosi\", \"nama_promosi\", \"from_\", \"to_\", \"loc_code\", \"jenis_promo\", \"amount\", pb.\"stock_id\", \"qty\", \"discount_max\" "
		        ." , \"discount_amount\", \"uom1_internal_barcode\",
					CASE
						WHEN mpp.\"uom1_changed_price\" != 0 THEN mpp.\"uom1_changed_price\" 
						WHEN mpp.\"price\" != 0 THEN mpp.\"price\" 
						ELSE mpp.\"uom1_suggested_price\" 
					END as price
				"
				." FROM m_promosi p INNER JOIN m_promosi_barang pb ON p.kode_promosi = pb.kode_promosi "
				." INNER JOIN m_product mp ON mp.stock_id = pb.stock_id "
				." INNER JOIN m_product_price mpp ON mp.stock_id = mpp.stock_id ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['kode_promosi'] = $result[1];
			$lists[$count]['nama_promosi'] = $result[2];
			$lists[$count]['from_'] = $result[3];	
			$lists[$count]['to_'] = $result[4];	
			$lists[$count]['loc_code'] = $result[5];	
			$lists[$count]['jenis_promo'] = $result[6];	
			$lists[$count]['amount'] = $result[7];	
			$lists[$count]['stock_id'] = $result[8];
			$lists[$count]['qty'] = $result[9];	
			$lists[$count]['discount_max'] = $result[10];	
			$lists[$count]['discount_amount'] = $result[11];	
			$lists[$count]['uom1_internal_barcode'] = $result[12];	
			$lists[$count]['price'] = $result[13];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getJoin_PrHdh($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT p.\"id\", p.\"kode_promosi\", \"nama_promosi\", \"from_\", \"to_\", \"loc_code\", \"jenis_promo\", \"amount\", ph.\"stock_id\", \"qty\", \"uom1_internal_barcode\"
					
				"
				." FROM m_promosi p INNER JOIN m_promosi_hadiah ph ON p.kode_promosi = ph.kode_promosi "
				." INNER JOIN m_product mp ON mp.stock_id = ph.stock_id "
				." INNER JOIN m_product_price mpp ON mp.stock_id = mpp.stock_id ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['kode_promosi'] = $result[1];
			$lists[$count]['nama_promosi'] = $result[2];
			$lists[$count]['from_'] = $result[3];	
			$lists[$count]['to_'] = $result[4];	
			$lists[$count]['loc_code'] = $result[5];	
			$lists[$count]['jenis_promo'] = $result[6];	
			$lists[$count]['amount'] = $result[7];	
			$lists[$count]['stock_id'] = $result[8];
			$lists[$count]['qty'] = $result[9];	
			$lists[$count]['uom1_internal_barcode'] = $result[10];	
			
			$count++;
		}
		
		return $lists;
	}


	public function getJoinPromoPromoDetail($condition2){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT pp.\"kode_produk\" "
				." FROM \"m_promosi\" p INNER JOIN \"m_promosi_promo\" pp ON (p.id = pp.promosi_id) ".$condition2;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['kode_produk'] = $result[0];
			$count++;
		}
		
		return $lists;
	}
}
