<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Tax extends Model
{

	public $id;
	public $rate;
	public $name;
	public $from;
	public $to;
	public $inactive;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"id\", \"rate\", \"name\", \"from\", \"to\", \"inactive\" "
				." FROM \"m_tax\" ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['rate'] = $result[1];
			$lists[$count]['name'] = $result[2];
			$lists[$count]['from'] = $result[3];
			$lists[$count]['to'] = $result[4];
			$lists[$count]['inactive'] = $result[5];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"id\", \"rate\", \"name\", \"from\", \"to\", \"inactive\" "
				." FROM \"m_tax\" "
				." WHERE \"id\" = '".$object->id."' LIMIT 1 " ;

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['rate'] = $result[1];
			$lists[$count]['name'] = $result[2];
			$lists[$count]['from'] = $result[3];
			$lists[$count]['to'] = $result[4];
			$lists[$count]['inactive'] = $result[5];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"id\", \"rate\", \"name\", \"from\", \"to\", \"inactive\" "
				." FROM \"m_tax\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['rate'] = $result[1];
			$lists[$count]['name'] = $result[2];
			$lists[$count]['from'] = $result[3];
			$lists[$count]['to'] = $result[4];
			$lists[$count]['inactive'] = $result[5];			
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"id\") "
				." FROM \"m_tax\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);	
		
		if($object->from == '') { $object->from = '0001-01-01'; }
		if($object->to == '') { $object->to = '9999-12-31'; }
		
		$sql = "INSERT INTO \"m_tax\" (\"id\", \"rate\", \"name\", \"from\", \"to\", \"inactive\") "
				." VALUES ('".$object->id."','".$object->rate."','".$object->name."','".$object->from."','".$object->to."','".$object->inactive."') ";
		
		$success = $connection->execute($sql);		
		
		return $sql;
	}
	
	public function goMultiInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "INSERT INTO \"m_tax\" (\"id\", \"rate\", \"name\", \"from\", \"to\", \"inactive\") "
			." VALUES ".$object;
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_tax\" SET ";
		$flag = false;		
		if($object->rate != '') { if($flag){ $sql .= ","; } $sql .= " \"rate\" = '".$object->rate."' "; $flag = true; }
		if($object->name != '') { if($flag){ $sql .= ","; } $sql .= " \"name\" = '".$object->name."' "; $flag = true; }		
		if($object->from != '') { if($flag){ $sql .= ","; } $sql .= " \"from\" = '".$object->from."' "; $flag = true; }		
		if($object->to != '') { if($flag){ $sql .= ","; } $sql .= " \"to\" = '".$object->to."' "; $flag = true; }
		if($object->inactive != '') { if($flag){ $sql .= ","; } $sql .= " \"inactive\" = '".$object->inactive."' "; $flag = true; }
		$sql .= " WHERE \"id\" = '".$object->id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_tax\" "
				." WHERE \"id\" = '".$object->id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goMultiDelete($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " DELETE FROM \"m_tax\" "
				." WHERE \"id\" IN (".$object.") ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
