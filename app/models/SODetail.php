<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class SODetail extends Model
{

	public $so_detail_item;
	public $so_id;
	public $item_code;
	public $description;	
	public $qty_tersedia;
	public $qty_barcode;
	public $unit_item;	
	public $unit_price;	
	public $total_price;	
	public $flag_so_ok;	
	public $barcode_code;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"so_detail_item\", \"so_id\", \"item_code\", \"description\", \"qty_tersedia\", \"qty_barcode\", \"unit_item\", \"unit_price\" "
				." , \"total_price\", \"flag_so_ok\", \"barcode_code\" "
				." FROM \"t_so_detail\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['so_detail_item'] = $result[0];
			$lists[$count]['so_id'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty_tersedia'] = $result[4];			
			$lists[$count]['qty_barcode'] = $result[5];	
			$lists[$count]['unit_item'] = $result[6];	
			$lists[$count]['unit_price'] = $result[7];	
			$lists[$count]['total_price'] = $result[8];	
			$lists[$count]['flag_so_ok'] = $result[9];	
			$lists[$count]['barcode_code'] = $result[10];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"so_detail_item\", \"so_id\", \"item_code\", \"description\", \"qty_tersedia\", \"qty_barcode\", \"unit_item\", \"unit_price\" "
				." , \"total_price\", \"flag_so_ok\", \"barcode_code\" "
				." FROM \"t_so_detail\" "
				." WHERE \"so_id\" = '".$object->so_id."' "
				." AND \"so_detail_item\" = '".$object->so_detail_item."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['so_detail_item'] = $result[0];
			$lists[$count]['so_id'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty_tersedia'] = $result[4];			
			$lists[$count]['qty_barcode'] = $result[5];	
			$lists[$count]['unit_item'] = $result[6];	
			$lists[$count]['unit_price'] = $result[7];	
			$lists[$count]['total_price'] = $result[8];	
			$lists[$count]['flag_so_ok'] = $result[9];	
			$lists[$count]['barcode_code'] = $result[10];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"so_detail_item\", \"so_id\", \"item_code\", \"description\", \"qty_tersedia\", \"qty_barcode\", \"unit_item\", \"unit_price\" "
				." , \"total_price\", \"flag_so_ok\", \"barcode_code\" "
				." FROM \"t_so_detail\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['so_detail_item'] = $result[0];
			$lists[$count]['so_id'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty_tersedia'] = $result[4];			
			$lists[$count]['qty_barcode'] = $result[5];	
			$lists[$count]['unit_item'] = $result[6];	
			$lists[$count]['unit_price'] = $result[7];	
			$lists[$count]['total_price'] = $result[8];	
			$lists[$count]['flag_so_ok'] = $result[9];	
			$lists[$count]['barcode_code'] = $result[10];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"so_id\") "
				." FROM \"t_so_detail\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}

	public function getCount2($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT sum(\"qty_tersedia\") AS qty_tersedia, sum(\"qty_barcode\") AS qty_barcode "
				." FROM \"t_so_detail\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['qty_tersedia'] = $result[0];
			$lists[$count]['qty_barcode'] = $result[1];
			$count++;
		}
		
		return $lists;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->qty_tersedia == '') { $object->qty_tersedia = 0; }
		if($object->qty_barcode == '') { $object->qty_barcode = 0; }
		if($object->unit_price == '') { $object->unit_price = 0; }
		if($object->total_price == '') { $object->total_price = 0; }
		
		if($object->flag_so_ok == '') { $object->flag_so_ok = 'f'; }
		
		$sql = "INSERT INTO \"t_so_detail\" (\"so_id\", \"item_code\", \"description\", \"qty_tersedia\", \"qty_barcode\", \"unit_item\", \"unit_price\" "
				." , \"total_price\", \"flag_so_ok\", \"barcode_code\" ) "
				." VALUES ('".$object->so_id."','".$object->item_code."','".pg_escape_string($object->description)."','".$object->qty_tersedia."','".$object->qty_barcode."','"
				.$object->unit_item."','".$object->unit_price."','".$object->total_price."','".$object->flag_so_ok."','".$object->barcode_code."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}

	public function goInsert2($object){
		$connection = new Postgresql($this->di['db']);

		if($object->qty_tersedia == '') { $object->qty_tersedia = 0; }
		if($object->qty_barcode == '') { $object->qty_barcode = 0; }
		if($object->unit_price == '') { $object->unit_price = 0; }
		if($object->total_price == '') { $object->total_price = 0; }
		
		if($object->flag_so_ok == '') { $object->flag_so_ok = 'f'; }
		
		$sql = "INSERT INTO \"t_so_detail\" (\"so_id\", \"item_code\", \"description\", \"qty_tersedia\", \"qty_barcode\", \"unit_item\", \"unit_price\" "
				." , \"total_price\", \"flag_so_ok\", \"barcode_code\" ) "
				." VALUES ('".$object->so_id."','".$object->item_code."','".pg_escape_string($object->description)."','".$object->qty_tersedia."','".$object->qty_barcode."','"
				.$object->unit_item."','".$object->unit_price."','".$object->total_price."','".$object->flag_so_ok."','".$object->barcode_code."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goInsertItem($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->qty_tersedia == '') { $object->qty_tersedia = '0'; }
		if($object->qty_barcode == '') { $object->qty_barcode = '0'; }
		if($object->unit_price == '') { $object->unit_price = '0'; }
		if($object->total_price == '') { $object->total_price = '0'; }
		
		if($object->flag_so_ok == '') { $object->flag_so_ok = 'f'; }
		
		$sql = "INSERT INTO \"t_so_detail\" (\"so_detail_item\", \"so_id\", \"item_code\", \"description\", \"qty_tersedia\", \"qty_barcode\", \"unit_item\", \"unit_price\" "
				." , \"total_price\", \"flag_so_ok\", \"barcode_code\" ) "
				." VALUES ('".$object->so_detail_item."','".$object->so_id."','".$object->item_code."','".pg_escape_string($object->description)."','".$object->qty_tersedia."','".$object->qty_barcode."','"
				.$object->unit_item."','".$object->unit_price."','".$object->total_price."','".$object->flag_so_ok."','".$object->barcode_code."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goMultiInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "INSERT INTO \"t_so_detail\" (\"so_id\", \"item_code\", \"description\", \"qty_tersedia\", \"qty_barcode\", \"unit_item\", \"unit_price\" "
				." , \"total_price\", \"flag_so_ok\", \"barcode_code\") "
			." VALUES ".$object;
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goMultiInsertItem($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "INSERT INTO \"t_so_detail\" (\"so_detail_item\", \"so_id\", \"item_code\", \"description\", \"qty_tersedia\", \"qty_barcode\", \"unit_item\", \"unit_price\" "
				." , \"total_price\", \"flag_so_ok\", \"barcode_code\") "
			." VALUES ".$object;
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_so_detail\" SET ";
		$flag = false;
		if($object->item_code != '') { if($flag){ $sql .= ","; } $sql .= " \"item_code\" = '".$object->item_code."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".pg_escape_string($object->description)."' "; $flag = true; }
		if($object->qty_tersedia != '') { if($flag){ $sql .= ","; } $sql .= " \"qty_tersedia\" = '".$object->qty_tersedia."' "; $flag = true; }
		if($object->qty_barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"qty_barcode\" = '".$object->qty_barcode."' "; $flag = true; }
		if($object->unit_item != '') { if($flag){ $sql .= ","; } $sql .= " \"unit_item\" = '".$object->unit_item."' "; $flag = true; }
		if($object->unit_price != '') { if($flag){ $sql .= ","; } $sql .= " \"unit_price\" = '".$object->unit_price."' "; $flag = true; }
		if($object->total_price != '') { if($flag){ $sql .= ","; } $sql .= " \"total_price\" = '".$object->total_price."' "; $flag = true; }
		if($object->flag_so_ok != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_so_ok\" = '".$object->flag_so_ok."' "; $flag = true; }
		if($object->barcode_code != '') { if($flag){ $sql .= ","; } $sql .= " \"barcode_code\" = '".$object->barcode_code."' "; $flag = true; }		
		$sql .= " WHERE \"so_id\" = '".$object->so_id."' ";
		$sql .= " AND \"so_detail_item\" = '".$object->so_detail_item."' ";	
		
		$success = $connection->execute($sql);
		
		return $success;
	}

	public function goUpdate2($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_so_detail\" SET ";
		$flag = false;
		if($object->item_code != '') { if($flag){ $sql .= ","; } $sql .= " \"item_code\" = '".$object->item_code."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".pg_escape_string($object->description)."' "; $flag = true; }
		if($object->qty_barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"qty_barcode\" = '".$object->qty_barcode."' "; $flag = true; }
		if($object->unit_item != '') { if($flag){ $sql .= ","; } $sql .= " \"unit_item\" = '".$object->unit_item."' "; $flag = true; }
		if($object->barcode_code != '') { if($flag){ $sql .= ","; } $sql .= " \"barcode_code\" = '".$object->barcode_code."' "; $flag = true; }		
		$sql .= " WHERE \"so_id\" = '".$object->so_id."' ";
		$sql .= " AND \"so_detail_item\" = '".$object->so_detail_item."' ";	
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goFreeSql($sql){
		$connection = new Postgresql($this->di['db']);
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_so_detail\" ";
		$sql .= " WHERE \"so_id\" = '".$object->so_id."' ";		
		$sql .= " AND \"so_detail_item\" = '".$object->so_detail_item."' ";	
		
		$success = $connection->execute($sql);		
		
		return $success;
	}

	public function getSum($condition){
		$connection = new Postgresql($this->di['db']);
		//dedy, edit, map.
		$sql = " SELECT SUM(qty_tersedia) as tersedia, SUM(qty_barcode) as barcode "
				." FROM \"t_so_detail\" ".$condition;

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['qty_tersedia'] = $result[0];
			$lists[$count]['qty_barcode'] = $result[1];
			$count++;
		}
		
		return $lists;
	}
	
	public function goDeleteSO($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_so_detail\" ";
		$sql .= " WHERE \"so_id\" = '".$object->so_id."' ";	
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function getJoin($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"so_detail_item\", \"so_id\", tsd.\"item_code\", tsd.\"description\", \"qty_tersedia\", \"qty_barcode\", \"unit_item\", \"unit_price\" "
				." , \"total_price\", \"flag_so_ok\", \"barcode_code\", mc.\"l1_nm\", mc.\"bumun_nm\", mc.\"l1_cd\" "
				." FROM \"t_so_detail\" tsd INNER JOIN \"m_product\" mp ON tsd.item_code = mp.stock_id LEFT JOIN \"m_category\" mc ON mp.l1_cd = mc.l1_cd ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['so_detail_item'] = $result[0];
			$lists[$count]['so_id'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty_tersedia'] = $result[4];			
			$lists[$count]['qty_barcode'] = $result[5];	
			$lists[$count]['unit_item'] = $result[6];	
			$lists[$count]['unit_price'] = $result[7];	
			$lists[$count]['total_price'] = $result[8];	
			$lists[$count]['flag_so_ok'] = $result[9];	
			$lists[$count]['barcode_code'] = $result[10];	
			$lists[$count]['l1_nm'] = $result[11];	
			$lists[$count]['bumun_nm'] = $result[12];	
			$lists[$count]['l1_cd'] = $result[13];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getSalesSO(){
		$connection = new Postgresql($this->di['db']);
		$count = 0; $last_date = ''; $first_date = '';
		$sql = "SELECT so_date FROM t_so WHERE status = 'COMPLETE' AND so_type = 'MONTH' ORDER BY so_date DESC LIMIT 2";		
		$results = $connection->query($sql);
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			if($count == 0){
				$last_date = date('Y-m-d', strtotime($result[0].'-1 day'));
			}else{
				$first_date = date('Y-m-d', strtotime($result[0].'+1 day'));
			}
			
			$count++;
		}
		
		if($first_date == '')
			$first_date = date('Y-m', strtotime($last_date)).'-01';
		
		$sql2 = "SELECT SUM(qty*harga) FROM t_jual_detail_sum WHERE tanggal BETWEEN '".$first_date."' AND '".$last_date."'";		
		$results2 = $connection->query($sql2);
		$results2->setFetchMode(Phalcon\Db::FETCH_NUM);
		$result2 = $results2->fetchArray();
		
		return $result2[0];
	}

	public static function queryInsertHPP($object)
	{
		$sql = "INSERT INTO \"t_so_detail\" (\"so_id\", \"item_code\", \"barcode_code\" "
				.", \"description\", \"unit_item\", \"unit_price\" ) VALUES ";

		return $sql;
	}

	public static function insertSql($object)
	{
		if($object->unit_item == '') { $object->unit_item = '0'; }	
		if($object->unit_price == '') { $object->unit_price = '0'; }		

		$sql = "('".$object->so_id."','".$object->item_code."','".$object->barcode_code."','".pg_escape_string($object->description)."','".$object->unit_item."','".$object->unit_price."'), ";
		return $sql;
	}

	public static function InsertEndSql($object)
	{
		if($object->unit_item == '') { $object->unit_item = '0'; }	
		if($object->unit_price == '') { $object->unit_price = '0'; }	

		$sql = "('".$object->so_id."','".$object->item_code."','".$object->barcode_code."','".pg_escape_string($object->description)."','".$object->unit_item."','".$object->unit_price."') ";

		return $sql;
	}

	public static function endSql($object)
	{
		$sql = "on conflict do nothing;";
		return $sql;
	}

	public function executeQuery($sql){
		$connection = new Postgresql($this->di['db']);
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
