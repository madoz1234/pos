<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Divisi extends Model
{

	public $id;
	public $bumun_cd;
	public $bumun_nm;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"bumun_cd\", \"bumun_nm\" "
				." FROM \"m_divisi\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['bumun_cd'] = $result[1];
			$lists[$count]['bumun_nm'] = $result[2];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"bumun_cd\", \"bumun_nm\" "
				." FROM \"m_divisi\" "
				." WHERE \"bumun_cd\" = '".$object->bumun_cd."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['bumun_cd'] = $result[1];
			$lists[$count]['bumun_nm'] = $result[2];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"bumun_cd\", \"bumun_nm\" "
				." FROM \"m_divisi\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['bumun_cd'] = $result[1];
			$lists[$count]['bumun_nm'] = $result[2];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"bumun_cd\") "
				." FROM \"m_divisi\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->auto_update == '') { $object->auto_update = '0'; }
		if($object->inactive == '') { $object->inactive = '0'; }
		$sql = "INSERT INTO \"m_divisi\" (\"id\", \"bumun_cd\", \"bumun_nm\") "
				." VALUES ('".$object->id."','".$object->bumun_cd."','".$object->bumun_nm."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goMultiInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "INSERT INTO \"m_divisi\" (\"id\", \"bumun_cd\", \"bumun_nm\") "
			." VALUES ".$object;
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_divisi\" SET ";
		$flag = false;
		if($object->bumun_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"bumun_nm\" = '".$object->bumun_nm."' "; $flag = true; }
		$sql .= " WHERE \"bumun_cd\" = '".$object->bumun_cd."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_divisi\" "
				." WHERE \"bumun_cd\" = '".$object->bumun_cd."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goMultiDelete($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " DELETE FROM \"m_divisi\" "
				." WHERE \"bumun_cd\" IN (".$object.") ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
}
