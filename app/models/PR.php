<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class PR extends Model
{

	public $request_no;
	public $supplier_id;
	public $point_used;
	public $delivery_by;	
	public $created_by;
	public $comments;
	public $create_date;	
	public $delivery_date;
	public $reference;
	public $requisition_no;
	public $delivery_address;
	public $total;
	public $tax_included;
	public $tmuk;
	public $po_no_immbo;	
	public $flag_sync;	
	public $flag_delete;	
	public $status;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"request_no\", \"supplier_id\", \"point_used\", \"delivery_by\", \"created_by\", \"comments\", \"create_date\" "
				." , \"reference\", \"requisition_no\", \"delivery_address\", \"total\", \"tax_included\", \"tmuk\", \"po_no_immbo\" "
				." , \"flag_sync\", \"delivery_date\", \"flag_delete\", \"status\" "
				." FROM \"t_pr\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['request_no'] = $result[0];
			$lists[$count]['supplier_id'] = $result[1];
			$lists[$count]['point_used'] = $result[2];
			$lists[$count]['delivery_by'] = $result[3];
			$lists[$count]['created_by'] = $result[4];			
			$lists[$count]['comments'] = $result[5];	
			$lists[$count]['create_date'] = $result[6];	
			$lists[$count]['reference'] = $result[7];	
			$lists[$count]['requisition_no'] = $result[8];	
			$lists[$count]['delivery_address'] = $result[9];	
			$lists[$count]['total'] = $result[10];	
			$lists[$count]['tax_included'] = $result[11];	
			$lists[$count]['tmuk'] = $result[12];	
			$lists[$count]['po_no_immbo'] = $result[13];
			$lists[$count]['flag_sync'] = $result[14];			
			$lists[$count]['delivery_date'] = $result[15];	
			$lists[$count]['flag_delete'] = $result[16];
			$lists[$count]['status'] = $result[17];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"request_no\", \"supplier_id\", \"point_used\", \"delivery_by\", \"created_by\", \"comments\", \"create_date\" "
				." , \"reference\", \"requisition_no\", \"delivery_address\", \"total\", \"tax_included\", \"tmuk\", \"po_no_immbo\" "
				." , \"flag_sync\", \"delivery_date\", \"flag_delete\", \"status\" "
				." FROM \"t_pr\" "
				." WHERE \"request_no\" = '".$object->request_no."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['request_no'] = $result[0];
			$lists[$count]['supplier_id'] = $result[1];
			$lists[$count]['point_used'] = $result[2];
			$lists[$count]['delivery_by'] = $result[3];
			$lists[$count]['created_by'] = $result[4];			
			$lists[$count]['comments'] = $result[5];	
			$lists[$count]['create_date'] = $result[6];	
			$lists[$count]['reference'] = $result[7];	
			$lists[$count]['requisition_no'] = $result[8];	
			$lists[$count]['delivery_address'] = $result[9];	
			$lists[$count]['total'] = $result[10];	
			$lists[$count]['tax_included'] = $result[11];	
			$lists[$count]['tmuk'] = $result[12];	
			$lists[$count]['po_no_immbo'] = $result[13];
			$lists[$count]['flag_sync'] = $result[14];			
			$lists[$count]['delivery_date'] = $result[15];	
			$lists[$count]['flag_delete'] = $result[16];
			$lists[$count]['status'] = $result[17];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"request_no\", \"supplier_id\", \"point_used\", \"delivery_by\", \"created_by\", \"comments\", \"create_date\" "
				." , \"reference\", \"requisition_no\", \"delivery_address\", \"total\", \"tax_included\", \"tmuk\", \"po_no_immbo\" "
				." , \"flag_sync\", \"delivery_date\", \"flag_delete\", \"status\" "
				." FROM \"t_pr\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['request_no'] = $result[0];
			$lists[$count]['supplier_id'] = $result[1];
			$lists[$count]['point_used'] = $result[2];
			$lists[$count]['delivery_by'] = $result[3];
			$lists[$count]['created_by'] = $result[4];			
			$lists[$count]['comments'] = $result[5];	
			$lists[$count]['create_date'] = $result[6];	
			$lists[$count]['reference'] = $result[7];	
			$lists[$count]['requisition_no'] = $result[8];	
			$lists[$count]['delivery_address'] = $result[9];	
			$lists[$count]['total'] = $result[10];	
			$lists[$count]['tax_included'] = $result[11];	
			$lists[$count]['tmuk'] = $result[12];	
			$lists[$count]['po_no_immbo'] = $result[13];
			$lists[$count]['flag_sync'] = $result[14];			
			$lists[$count]['delivery_date'] = $result[15];	
			$lists[$count]['flag_delete'] = $result[16];	
			$lists[$count]['status'] = $result[17];			
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"request_no\") "
				." FROM \"t_pr\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}

	public function getCountPR($cond1){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"request_no\") "
				." FROM \"t_pr\" ".$cond1;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}

	public function getSumTotal($cond){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT SUM(\"total\")"
				." FROM \"t_pr\" ".$cond;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->supplier_id == '') { $object->supplier_id = '0'; }
		if($object->total == '') { $object->total = '0'; }
		if($object->tax_included == '') { $object->tax_included = '0'; }
		if($object->status == '') { $object->status = '0'; }
		
		if($object->create_date == '') { $object->create_date = date("Y-m-d"); }		
		if($object->delivery_date == '') { $object->delivery_date = date("Y-m-d"); }
		if($object->flag_sync == '') { $object->flag_sync = 'f'; }
		if($object->flag_delete == '') { $object->flag_delete = 'f'; }
		
		$sql = "INSERT INTO \"t_pr\" (\"request_no\", \"supplier_id\", \"point_used\", \"delivery_by\", \"created_by\", \"comments\", \"create_date\" "
				." , \"reference\", \"requisition_no\", \"delivery_address\", \"total\", \"tax_included\", \"tmuk\", \"po_no_immbo\", \"flag_sync\" "
				." , \"delivery_date\", \"flag_delete\", \"status\" ) "
				." VALUES ('".$object->request_no."','".$object->supplier_id."','".$object->point_used."','".$object->delivery_by."','".$object->created_by."','"
				.$object->comments."','".$object->create_date."','".$object->reference."','".$object->requisition_no."','".$object->delivery_address."','"
				.$object->total."','".$object->tax_included."','".$object->tmuk."','".$object->po_no_immbo."','".$object->flag_sync."','"
				.$object->delivery_date."','".$object->flag_delete."','".$object->status."' ) ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		// dd($object->request_no);
		
		$sql = " UPDATE \"t_pr\" SET ";
		$flag = false;
		if($object->supplier_id != '') { if($flag){ $sql .= ","; } $sql .= " \"supplier_id\" = '".$object->supplier_id."' "; $flag = true; }
		if($object->point_used != '') { if($flag){ $sql .= ","; } $sql .= " \"point_used\" = '".$object->point_used."' "; $flag = true; }
		if($object->delivery_by != '') { if($flag){ $sql .= ","; } $sql .= " \"delivery_by\" = '".$object->delivery_by."' "; $flag = true; }
		if($object->created_by != '') { if($flag){ $sql .= ","; } $sql .= " \"created_by\" = '".$object->created_by."' "; $flag = true; }
		if($object->comments != '') { if($flag){ $sql .= ","; } $sql .= " \"comments\" = '".$object->comments."' "; $flag = true; }
		if($object->create_date != '') { if($flag){ $sql .= ","; } $sql .= " \"create_date\" = '".$object->create_date."' "; $flag = true; }
		if($object->delivery_date != '') { if($flag){ $sql .= ","; } $sql .= " \"delivery_date\" = '".$object->delivery_date."' "; $flag = true; }
		if($object->reference != '') { if($flag){ $sql .= ","; } $sql .= " \"reference\" = '".$object->reference."' "; $flag = true; }
		if($object->requisition_no != '') { if($flag){ $sql .= ","; } $sql .= " \"requisition_no\" = '".$object->requisition_no."' "; $flag = true; }
		if($object->delivery_address != '') { if($flag){ $sql .= ","; } $sql .= " \"delivery_address\" = '".$object->delivery_address."' "; $flag = true; }
		if($object->total != '') { if($flag){ $sql .= ","; } $sql .= " \"total\" = '".$object->total."' "; $flag = true; }
		if($object->tax_included != '') { if($flag){ $sql .= ","; } $sql .= " \"tax_included\" = '".$object->tax_included."' "; $flag = true; }
		if($object->tmuk != '') { if($flag){ $sql .= ","; } $sql .= " \"tmuk\" = '".$object->tmuk."' "; $flag = true; }
		if($object->po_no_immbo != '') { if($flag){ $sql .= ","; } $sql .= " \"po_no_immbo\" = '".$object->po_no_immbo."' "; $flag = true; }
		if($object->flag_sync != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = '".$object->flag_sync."' "; $flag = true; }
		if($object->flag_delete != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_delete\" = '".$object->flag_delete."' "; $flag = true; }
		if($object->status != '') { if($flag){ $sql .= ","; } $sql .= " \"status\" = '".$object->status."' "; $flag = true; }
		$sql .= " WHERE \"request_no\" = '".$object->request_no."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_pr\" "
				." WHERE \"request_no\" = '".$object->request_no."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function getJoin($param){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT po.\"po_no_immbo\", po.\"pr_no_immbo\", po.\"order_date\", po.\"delivery_date\", pod.\"item_code\", pod.\"description\", pod.\"qty_po\", pod.\"po_unit_price\", pod.\"po_price\", po.\"total_po\",
		CASE
			WHEN uom1_htype_3 IS true THEN uom1_nm
			WHEN uom2_htype_3 IS true THEN uom2_nm
			WHEN uom3_htype_3 IS true THEN uom3_nm
			WHEN uom4_htype_3 IS true THEN uom4_nm
			ELSE ''
		END 
			AS uom_o_type,
			
		CASE
			WHEN uom1_htype_3 IS true THEN uom1_internal_barcode
			WHEN uom2_htype_3 IS true THEN uom2_internal_barcode
			WHEN uom3_htype_3 IS true THEN uom3_internal_barcode
			WHEN uom4_htype_3 IS true THEN uom4_internal_barcode
			ELSE ''
		END 
			AS uom_o_barcode,
			
		CASE
			WHEN uom1_htype_5 IS true THEN uom1_nm
			WHEN uom2_htype_5 IS true THEN uom2_nm
			WHEN uom3_htype_5 IS true THEN uom3_nm
			WHEN uom4_htype_5 IS true THEN uom4_nm
			ELSE ''
		END 
			AS uom_s_type,
			
		CASE
			WHEN uom1_htype_5 IS true THEN uom1_internal_barcode
			WHEN uom2_htype_5 IS true THEN uom2_internal_barcode
			WHEN uom3_htype_5 IS true THEN uom3_internal_barcode
			WHEN uom4_htype_5 IS true THEN uom4_internal_barcode
			ELSE ''
		END 
			AS uom_s_barcode, 
		
		CASE
			WHEN uom1_htype_3 IS true THEN uom1_conversion
			WHEN uom2_htype_3 IS true THEN uom2_conversion
			WHEN uom3_htype_3 IS true THEN uom3_conversion
			WHEN uom4_htype_3 IS true THEN uom4_conversion
			ELSE ''
		END 
			AS uom_o_conversion,
			
		CASE
			WHEN uom1_htype_5 IS true THEN uom1_internal_barcode
			WHEN uom2_htype_5 IS true THEN uom2_internal_barcode
			WHEN uom3_htype_5 IS true THEN uom3_internal_barcode
			WHEN uom4_htype_5 IS true THEN uom4_internal_barcode
			ELSE ''
		END 
			AS uom_s_conversion"
		
				." FROM t_po po INNER JOIN t_po_detail pod ON po.po_no_immbo = pod.po_no_immbo INNER JOIN m_product mp ON pod.item_code = mp.stock_id WHERE po.pr_no_immpos='".$param."'";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['po_no_immbo'] = $result[0];
			$lists[$count]['pr_no_immbo'] = $result[1];
			$lists[$count]['order_date'] = $result[2];
			$lists[$count]['delivery_date'] = $result[3];	
			$lists[$count]['item_code'] = $result[4];	
			$lists[$count]['description'] = $result[5];	
			$lists[$count]['qty_po'] = $result[6];	
			$lists[$count]['po_unit_price'] = $result[7];	
			$lists[$count]['po_price'] = $result[8];	
			$lists[$count]['total_po'] = $result[9];	
			$lists[$count]['uom_o_type'] = $result[10];	
			$lists[$count]['uom_o_barcode'] = $result[11];	
			$lists[$count]['uom_s_type'] = $result[12];	
			$lists[$count]['uom_s_barcode'] = $result[13];	
			$lists[$count]['uom_o_conversion'] = $result[14];	
			$lists[$count]['uom_s_conversion'] = $result[15];	
			
			$count++;
		}
		
		return $lists;
	}
}
