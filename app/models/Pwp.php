<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Pwp extends Model
{
	public $kode_promosi;
	public $stock_id;
	public $price;
	public $flag;

	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\",\"kode_promosi\", \"stock_id\", \"price\", \"flag\" "
				." FROM \"m_promosi_pwp\" "
				." WHERE \"flag_delete\" = false ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['kode_promosi'] = $result[1];
			$lists[$count]['stock_id'] = $result[2];
			$lists[$count]['price'] = $result[3];
			$lists[$count]['flag'] = $result[4];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\",\"kode_promosi\", \"stock_id\", \"price\", \"flag\" "
				." FROM \"m_promosi_pwp\" "
				." WHERE \"kode_promosi\" = '".$object->kode_promosi."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['kode_promosi'] = $result[1];
			$lists[$count]['stock_id'] = $result[2];
			$lists[$count]['price'] = $result[3];
			$lists[$count]['flag'] = $result[4];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\",\"kode_promosi\", \"stock_id\", \"price\", \"flag\" "
				." FROM \"m_promosi_pwp\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['kode_promosi'] = $result[1];
			$lists[$count]['stock_id'] = $result[2];
			$lists[$count]['price'] = $result[3];
			$lists[$count]['flag'] = $result[4];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"kode_promosi\") "
				." FROM \"m_promosi_pwp\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->price == '') { $object->price = date("Y-m-d"); }
		
		$sql = "INSERT INTO \"m_promosi_pwp\" (\"id\",\"kode_promosi\", \"stock_id\", \"price\", \"flag\") "
				." VALUES ('".$object->id."','".$object->kode_promosi."','".$object->stock_id."','".$object->price."','".$object->flag."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_promosi_pwp\" SET ";
		$flag = false;
		if($object->kode_promosi != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_promosi\" = '".$object->kode_promosi."' "; $flag = true; }
		if($object->stock_id != '') { if($flag){ $sql .= ","; } $sql .= " \"stock_id\" = '".$object->stock_id."' "; $flag = true; }
		if($object->price != '') { if($flag){ $sql .= ","; } $sql .= " \"price\" = '".$object->price."' "; $flag = true; }
		if($object->flag != '') { if($flag){ $sql .= ","; } $sql .= " \"flag\" = '".$object->flag."' "; $flag = true; }
		$sql .= " WHERE \"kode_promosi\" = '".$object->kode_promosi."' AND \"stock_id\" = '".$object->stock_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}

	public function goUpdate2($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_promosi_pwp\" SET ";
		$flag = false;
		if($object->kode_promosi != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_promosi\" = '".$object->kode_promosi."' "; $flag = true; }
		if($object->stock_id != '') { if($flag){ $sql .= ","; } $sql .= " \"stock_id\" = '".$object->stock_id."' "; $flag = true; }
		if($object->price != '') { if($flag){ $sql .= ","; } $sql .= " \"price\" = '".$object->price."' "; $flag = true; }
		if($object->flag != '') { if($flag){ $sql .= ","; } $sql .= " \"flag\" = '".$object->flag."' "; $flag = true; }
		$sql .= " WHERE \"kode_promosi\" = '".$object->kode_promosi."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_promosi_pwp\" "
				." WHERE \"kode_promosi\" = '".$object->kode_promosi."' ";
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function getJoinPwpDetail($condition2){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT pwpl.\"kode_produk\" "
				." FROM \"m_promosi_pwp\" pwp INNER JOIN \"m_promosi_pwp_list\" pwpl ON (pwp.id = pwpl.pwp_id) ".$condition2;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['kode_produk'] = $result[0];
			$count++;
		}
		
		return $lists;
	}
}
