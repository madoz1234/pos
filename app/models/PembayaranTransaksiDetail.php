<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class PembayaranTransaksiDetail extends Model
{

	public $id;
	public $payment_no;
	public $price;
	public $qty;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"payment_no\", \"stock_id\", \"price\", \"qty\" "
				." FROM t_inv_detail  "
				." ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['payment_no'] = $result[1];
			$lists[$count]['stock_id'] = $result[2];
			$lists[$count]['price'] = $result[3];	
			$lists[$count]['qty'] = $result[4];	
			
			$count++;
		}
		
		return $lists;
	}

	public function getSumTotal($cond){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT SUM(\"price\")"
				." FROM \"t_inv_detail\" ".$cond;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"payment_no\", \"stock_id\", \"price\", \"qty\" "
				." FROM t_inv_detail "
				." WHERE payment_no = '".$object->payment_no."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['payment_no'] = $result[1];
			$lists[$count]['stock_id'] = $result[2];
			$lists[$count]['price'] = $result[3];	
			$lists[$count]['qty'] = $result[4];		
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"payment_no\", \"stock_id\", \"price\", \"qty\" "
				." FROM t_inv_detail ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['payment_no'] = $result[1];
			$lists[$count]['stock_id'] = $result[2];
			$lists[$count]['price'] = $result[3];	
			$lists[$count]['qty'] = $result[4];			
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"payment_no\") "
				." FROM \"t_inv_detail\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->price == '') { $object->price = '0'; }
		if($object->qty == '') { $object->qty = '0'; }
		
		$sql = "INSERT INTO \"t_inv_detail\" (\"payment_no\", \"stock_id\", \"price\", \"qty\") "
				." VALUES ('".$object->payment_no."','".$object->stock_id."',".$object->price.",'".$object->qty."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_inv_detail\" SET ";
		$flag = false;
		if($object->stock_id != '') { if($flag){ $sql .= ","; } $sql .= " \"stock_id\" = '".$object->stock_id."' "; $flag = true; }
		if($object->price != '') { if($flag){ $sql .= ","; } $sql .= " \"price\" = '".$object->price."' "; $flag = true; }
		if($object->qty != '') { if($flag){ $sql .= ","; } $sql .= " \"qty\" = '".$object->qty."' "; $flag = true; }
		$sql .= " WHERE \"payment_no\" = '".$object->payment_no."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "DELETE FROM \"t_inv_detail\" "
				." WHERE \"payment_no\" = '".$object->payment_no."'";
				
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function getJoin_Product($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"payment_no\", id.\"stock_id\", \"price\", \"qty\", \"description\" "
				." FROM t_inv_detail id INNER JOIN m_product mp ON id.stock_id = mp.stock_id ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['payment_no'] = $result[1];
			$lists[$count]['stock_id'] = $result[2];
			$lists[$count]['price'] = $result[3];	
			$lists[$count]['qty'] = $result[4];			
			$lists[$count]['description'] = $result[5];			
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getJoin_Header($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT i.payment_no, id.stock_id, price, qty, description "
				." FROM t_inv_detail id INNER JOIN t_inv i ON id.payment_no = i.payment_no "
				." INNER JOIN m_product mp ON id.stock_id = mp.stock_id ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['payment_no'] = $result[0];
			$lists[$count]['stock_id'] = $result[1];
			$lists[$count]['price'] = $result[2];	
			$lists[$count]['qty'] = $result[3];					
			$lists[$count]['description'] = $result[4];					
			
			$count++;
		}
		
		return $lists;
	}
}