<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class ReturDetail extends Model
{

	public $retur_detail_item;
	public $retur_no;
	public $item_code;
	public $description;		
	public $qty_retur;
	public $unit_retur;	
	public $retur_unit_price;
	public $retur_price;
	public $gr_detail_item;
	public $flag_gr_ok;	
	public $comments;
	public $note;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"retur_detail_item\", \"retur_no\", \"item_code\", \"description\", \"qty_retur\", \"unit_retur\", \"retur_unit_price\" "
				." , \"retur_price\", \"gr_detail_item\" , \"flag_gr_ok\", \"comments\", \"note\" "
				." FROM \"t_retur_detail\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['retur_detail_item'] = $result[0];
			$lists[$count]['retur_no'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty_retur'] = $result[4];			
			$lists[$count]['unit_retur'] = $result[5];	
			$lists[$count]['retur_unit_price'] = $result[6];	
			$lists[$count]['retur_price'] = $result[7];	
			$lists[$count]['gr_detail_item'] = $result[8];	
			$lists[$count]['flag_gr_ok'] = $result[9];				
			$lists[$count]['comments'] = $result[10];	
			$lists[$count]['note'] = $result[11];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"retur_detail_item\", \"retur_no\", \"item_code\", \"description\", \"qty_retur\", \"unit_retur\", \"retur_unit_price\" "
				." , \"retur_price\", \"gr_detail_item\" , \"flag_gr_ok\", \"comments\", \"note\" "
				." FROM \"t_retur_detail\" "
				." WHERE \"retur_no\" = '".$object->retur_no."' "
				." AND \"retur_detail_item\" = '".$object->retur_detail_item."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['retur_detail_item'] = $result[0];
			$lists[$count]['retur_no'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty_retur'] = $result[4];			
			$lists[$count]['unit_retur'] = $result[5];	
			$lists[$count]['retur_unit_price'] = $result[6];	
			$lists[$count]['retur_price'] = $result[7];	
			$lists[$count]['gr_detail_item'] = $result[8];	
			$lists[$count]['flag_gr_ok'] = $result[9];	
			$lists[$count]['comments'] = $result[10];
			$lists[$count]['note'] = $result[11];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"retur_detail_item\", \"retur_no\", \"item_code\", \"description\", \"qty_retur\", \"unit_retur\", \"retur_unit_price\" "
				." , \"retur_price\", \"gr_detail_item\" , \"flag_gr_ok\", \"comments\", \"note\" "
				." FROM \"t_retur_detail\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['retur_detail_item'] = $result[0];
			$lists[$count]['retur_no'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty_retur'] = $result[4];			
			$lists[$count]['unit_retur'] = $result[5];	
			$lists[$count]['retur_unit_price'] = $result[6];	
			$lists[$count]['retur_price'] = $result[7];	
			$lists[$count]['gr_detail_item'] = $result[8];	
			$lists[$count]['flag_gr_ok'] = $result[9];
			$lists[$count]['comments'] = $result[10];
			$lists[$count]['note'] = $result[11];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"retur_no\") "
				." FROM \"t_retur_detail\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->qty_retur == '') { $object->qty_retur = '0'; }
		if($object->retur_unit_price == '') { $object->retur_unit_price = '0'; }		
		if($object->retur_price == '') { $object->retur_price = '0'; }	
	
		if($object->gr_detail_item == '') { $object->gr_detail_item = '0'; }
		if($object->flag_gr_ok == '') { $object->flag_gr_ok = 'f'; }
		
		$sql = "INSERT INTO \"t_retur_detail\" (\"retur_no\", \"item_code\", \"description\", \"qty_retur\", \"unit_retur\" "
				." , \"retur_unit_price\", \"retur_price\", \"gr_detail_item\", \"flag_gr_ok\", \"comments\", \"note\" ) "
				." VALUES ('".$object->retur_no."','".$object->item_code."','".pg_escape_string($object->description)."','".$object->qty_retur."','".$object->unit_retur."','"
				.$object->retur_unit_price."','".$object->retur_price."','".$object->gr_detail_item."','".$object->flag_gr_ok."','".$object->comments."','".$object->note."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_retur_detail\" SET ";
		$flag = false;
		if($object->item_code != '') { if($flag){ $sql .= ","; } $sql .= " \"item_code\" = '".$object->item_code."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".pg_escape_string($object->description)."' "; $flag = true; }
		if($object->qty_retur != '') { if($flag){ $sql .= ","; } $sql .= " \"qty_retur\" = '".$object->qty_retur."' "; $flag = true; }
		if($object->unit_retur != '') { if($flag){ $sql .= ","; } $sql .= " \"unit_retur\" = '".$object->unit_retur."' "; $flag = true; }
		if($object->retur_unit_price != '') { if($flag){ $sql .= ","; } $sql .= " \"retur_unit_price\" = '".$object->retur_unit_price."' "; $flag = true; }
		if($object->retur_price != '') { if($flag){ $sql .= ","; } $sql .= " \"retur_price\" = '".$object->retur_price."' "; $flag = true; }
		if($object->gr_detail_item != '') { if($flag){ $sql .= ","; } $sql .= " \"gr_detail_item\" = '".$object->gr_detail_item."' "; $flag = true; }		
		if($object->flag_gr_ok != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_gr_ok\" = '".$object->flag_gr_ok."' "; $flag = true; }
		if($object->comments != '') { if($flag){ $sql .= ","; } $sql .= " \"comments\" = '".$object->comments."' "; $flag = true; }
		if($object->note != '') { if($flag){ $sql .= ","; } $sql .= " \"note\" = '".$object->note."' "; $flag = true; }
		$sql .= " WHERE \"retur_no\" = '".$object->retur_no."' ";	
		$sql .= " AND \"retur_detail_item\" = '".$object->retur_detail_item."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goUpdateRetur($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_retur_detail\" SET ";
		$flag = false;
		if($object->item_code != '') { if($flag){ $sql .= ","; } $sql .= " \"item_code\" = '".$object->item_code."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".pg_escape_string($object->description)."' "; $flag = true; }
		if($object->qty_retur != '') { if($flag){ $sql .= ","; } $sql .= " \"qty_retur\" = '".$object->qty_retur."' "; $flag = true; }
		if($object->unit_retur != '') { if($flag){ $sql .= ","; } $sql .= " \"unit_retur\" = '".$object->unit_retur."' "; $flag = true; }
		if($object->retur_unit_price != '') { if($flag){ $sql .= ","; } $sql .= " \"retur_unit_price\" = '".$object->retur_unit_price."' "; $flag = true; }
		if($object->retur_price != '') { if($flag){ $sql .= ","; } $sql .= " \"retur_price\" = '".$object->retur_price."' "; $flag = true; }
		if($object->gr_detail_item != '') { if($flag){ $sql .= ","; } $sql .= " \"gr_detail_item\" = '".$object->gr_detail_item."' "; $flag = true; }		
		if($object->flag_gr_ok != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_gr_ok\" = '".$object->flag_gr_ok."' "; $flag = true; }
		if($object->comments != '') { if($flag){ $sql .= ","; } $sql .= " \"comments\" = '".$object->comments."' "; $flag = true; }
		if($object->note != '') { if($flag){ $sql .= ","; } $sql .= " \"note\" = '".$object->note."' "; $flag = true; }
		$sql .= " WHERE \"retur_no\" = '".$object->retur_no."' ";	
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_retur_detail\" "
				." WHERE \"retur_no\" = '".$object->retur_no."' "
				." AND \"retur_detail_item\" = '".$object->retur_detail_item."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function getJoin_POGR($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT trd.\"retur_detail_item\", trd.\"retur_no\", trd.\"item_code\", trd.\"description\", trd.\"qty_retur\", trd.\"unit_retur\", trd.\"retur_unit_price\" "
				." , trd.\"retur_price\", trd.\"gr_detail_item\", trd.\"flag_gr_ok\", trd.\"comments\", trd.\"note\", trd1.\"qty_retur\" as qty_po "
				." FROM \"t_retur_detail\" trd INNER JOIN \"t_retur\" tr ON trd.retur_no = tr.retur_no INNER JOIN \"t_gr\" tg ON tr.gr_no_immpos = tg.gr_no INNER JOIN \"t_retur\" tr1 ON tg.retur_no_immpos = tr1.retur_no INNER JOIN t_retur_detail trd1 ON trd1.retur_no = tr1.retur_no and trd1.item_code=trd.item_code ".$condition;
				
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['retur_detail_item'] = $result[0];
			$lists[$count]['retur_no'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty_retur'] = $result[4];			
			$lists[$count]['unit_retur'] = $result[5];	
			$lists[$count]['retur_unit_price'] = $result[6];	
			$lists[$count]['retur_price'] = $result[7];	
			$lists[$count]['gr_detail_item'] = $result[8];	
			$lists[$count]['flag_gr_ok'] = $result[9];
			$lists[$count]['comments'] = $result[10];
			$lists[$count]['note'] = $result[11];
			$lists[$count]['qty_po'] = $result[12];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getJoin_Product($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT trd.\"retur_detail_item\", trd.\"retur_no\", trd.\"item_code\", trd.\"description\", trd.\"qty_retur\", trd.\"unit_retur\", trd.\"retur_unit_price\" "
				." , trd.\"retur_price\", trd.\"gr_detail_item\", trd.\"flag_gr_ok\", trd.\"comments\", trd.\"note\",
				CASE
					WHEN uom1_nm = unit_retur THEN uom1_conversion
					WHEN uom2_nm = unit_retur THEN uom2_conversion
					WHEN uom3_nm = unit_retur THEN uom3_conversion
					WHEN uom4_nm = unit_retur THEN uom4_conversion
				END as konversi "
				." FROM \"t_retur_detail\" trd INNER JOIN \"m_product\" mp ON trd.item_code = mp.stock_id ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['retur_detail_item'] = $result[0];
			$lists[$count]['retur_no'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty_retur'] = $result[4];			
			$lists[$count]['unit_retur'] = $result[5];	
			$lists[$count]['retur_unit_price'] = $result[6];	
			$lists[$count]['retur_price'] = $result[7];	
			$lists[$count]['gr_detail_item'] = $result[8];	
			$lists[$count]['flag_gr_ok'] = $result[9];
			$lists[$count]['comments'] = $result[10];
			$lists[$count]['note'] = $result[11];
			$lists[$count]['konversi'] = $result[12];
			
			$count++;
		}
		
		return $lists;
	}
}
