<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Location extends Model
{

	public $loc_code;
	public $location_name;
	public $delivery_address;
	public $phone;	
	public $phone2;	
	public $fax;	
	public $email;	
	public $contact;	
	public $inactive;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"loc_code\", \"location_name\", \"delivery_address\", \"phone\", \"phone2\", \"fax\", \"email\", \"contact\", \"inactive\" "
				." FROM m_location "
				." WHERE inactive != 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['loc_code'] = $result[0];
			$lists[$count]['location_name'] = $result[1];
			$lists[$count]['delivery_address'] = $result[2];
			$lists[$count]['phone'] = $result[3];	
			$lists[$count]['phone2'] = $result[4];	
			$lists[$count]['fax'] = $result[5];	
			$lists[$count]['email'] = $result[6];	
			$lists[$count]['contact'] = $result[7];	
			$lists[$count]['inactive'] = $result[8];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"loc_code\", \"location_name\", \"delivery_address\", \"phone\", \"phone2\", \"fax\", \"email\", \"contact\", \"inactive\" "
				." FROM m_location "
				." WHERE loc_code = '".$object->loc_code."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['loc_code'] = $result[0];
			$lists[$count]['location_name'] = $result[1];
			$lists[$count]['delivery_address'] = $result[2];
			$lists[$count]['phone'] = $result[3];	
			$lists[$count]['phone2'] = $result[4];	
			$lists[$count]['fax'] = $result[5];	
			$lists[$count]['email'] = $result[6];	
			$lists[$count]['contact'] = $result[7];	
			$lists[$count]['inactive'] = $result[8];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"loc_code\", \"location_name\", \"delivery_address\", \"phone\", \"phone2\", \"fax\", \"email\", \"contact\", \"inactive\" "
				." FROM \"m_location\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['loc_code'] = $result[0];
			$lists[$count]['location_name'] = $result[1];
			$lists[$count]['delivery_address'] = $result[2];
			$lists[$count]['phone'] = $result[3];	
			$lists[$count]['phone2'] = $result[4];	
			$lists[$count]['fax'] = $result[5];	
			$lists[$count]['email'] = $result[6];	
			$lists[$count]['contact'] = $result[7];	
			$lists[$count]['inactive'] = $result[8];		
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"loc_code\") "
				." FROM \"m_location\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->inactive == '') { $object->inactive = '0'; }
		$sql = "INSERT INTO \"m_location\" (\"loc_code\", \"location_name\", \"delivery_address\", \"phone\", \"phone2\", \"fax\", \"email\", \"contact\", \"inactive\") "
				." VALUES ('".$object->loc_code."','".$object->location_name."','".$object->delivery_address."','".$object->phone."','".$object->phone2."','".$object->fax."','".$object->email."','".$object->contact."','".$object->inactive."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->inactive == '') { $object->inactive = '0'; }
		
		$sql = " UPDATE \"m_location\" SET ";
		$flag = false;
		if($object->location_name != '') { if($flag){ $sql .= ","; } $sql .= " \"location_name\" = '".$object->location_name."' "; $flag = true; }
		if($object->delivery_address != '') { if($flag){ $sql .= ","; } $sql .= " \"delivery_address\" = '".$object->delivery_address."' "; $flag = true; }
		if($object->phone != '') { if($flag){ $sql .= ","; } $sql .= " \"phone\" = '".$object->phone."' "; $flag = true; }
		if($object->phone2 != '') { if($flag){ $sql .= ","; } $sql .= " \"phone2\" = '".$object->phone2."' "; $flag = true; }
		if($object->fax != '') { if($flag){ $sql .= ","; } $sql .= " \"fax\" = '".$object->fax."' "; $flag = true; }
		if($object->email != '') { if($flag){ $sql .= ","; } $sql .= " \"email\" = '".$object->email."' "; $flag = true; }
		if($object->contact != '') { if($flag){ $sql .= ","; } $sql .= " \"contact\" = '".$object->contact."' "; $flag = true; }
		if($object->inactive != '') { if($flag){ $sql .= ","; } $sql .= " \"inactive\" = '".$object->inactive."' "; $flag = true; }
		$sql .= " WHERE \"loc_code\" = '".$object->loc_code."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_location\" "
				." WHERE \"loc_code\" = '".$object->loc_code."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
