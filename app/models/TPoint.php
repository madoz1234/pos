<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class TPoint extends Model
{

	public $id;
	public $id_member;
	public $id_ref;	
	public $point;	
	public $create_by;
	public $create_date;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"id_member\", \"id_ref\", \"point\", \"create_by\", \"create_date\" "
				." FROM \"t_point\" ";				
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['id_member'] = $result[1];
			$lists[$count]['id_ref'] = $result[2];
			$lists[$count]['point'] = $result[3];
			$lists[$count]['create_by'] = $result[4];
			$lists[$count]['create_date'] = $result[5];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql =  "SELECT \"id\", \"id_member\", \"id_ref\", \"point\", \"create_by\", \"create_date\" "
				." FROM \"t_point\" "
				." WHERE \"id\" = '".$object->id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['id_member'] = $result[1];
			$lists[$count]['id_ref'] = $result[2];
			$lists[$count]['point'] = $result[3];
			$lists[$count]['create_by'] = $result[4];
			$lists[$count]['create_date'] = $result[5];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql =  "SELECT \"id\", \"id_member\", \"id_ref\", \"point\", \"create_by\", \"create_date\" "
				." FROM \"t_point\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['id_member'] = $result[1];
			$lists[$count]['id_ref'] = $result[2];
			$lists[$count]['point'] = $result[3];
			$lists[$count]['create_by'] = $result[4];
			$lists[$count]['create_date'] = $result[5];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"id\") "
				." FROM \"t_point\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}

	public function countPoin($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id_member\", SUM(\"point\") as poin "
				." FROM \"t_point\" "
				."GROUP BY \"id_member\" ";
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_member'] = $result[0];
			$lists[$count]['point'] = $result[1];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->point == '') { $object->point = '0'; }
		if($object->create_date == '') { $object->create_date = date('Y-m-d'); }
		
		$sql = "INSERT INTO \"t_point\" (\"id_member\", \"id_ref\", \"point\", \"create_by\", \"create_date\") "
				." VALUES ('".$object->id_member."','".$object->id_ref."','".$object->point."','".$object->create_by
				."','".$object->create_date."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}	
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_point\" SET ";
		$flag = false;
		if($object->id_member != '') { if($flag){ $sql .= ","; } $sql .= " \"id_member\" = '".$object->id_member."' "; $flag = true; }
		if($object->id_ref != '') { if($flag){ $sql .= ","; } $sql .= " \"id_ref\" = '".$object->id_ref."' "; $flag = true; }
		if($object->point != '') { if($flag){ $sql .= ","; } $sql .= " \"point\" = '".$object->point."' "; $flag = true; }		
		$sql .= " WHERE \"id\" = '".$object->id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_point\" "
				." WHERE \"id\" = '".$object->id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
}
