<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Notifikasi extends Model
{

	public $id;
	public $data;
	public $read_at;
	public $created_by;	
	public $created_at;
		
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"data\", \"read_at\", \"created_by\", \"created_at\","
				." FROM \"notification\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['data'] = $result[1];
			$lists[$count]['read_at'] = $result[2];
			$lists[$count]['created_by'] = $result[3];
			$lists[$count]['created_at'] = $result[4];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"data\", \"read_at\", \"created_by\", \"created_at\","
				." FROM \"notification\" "
				." WHERE \"id\" = '".$object->id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['data'] = $result[1];
			$lists[$count]['read_at'] = $result[2];
			$lists[$count]['created_by'] = $result[3];
			$lists[$count]['created_at'] = $result[4];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition=""){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"data\", \"read_at\", \"created_by\", \"created_at\""
				." FROM \"notification\" ORDER BY created_at DESC";
						
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['data'] = $result[1];
			$lists[$count]['read_at'] = $result[2];
			$lists[$count]['created_by'] = $result[3];
			$lists[$count]['created_at'] = $result[4];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"id\") "
				." FROM \"notification\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "INSERT INTO \"notification\" (\"data\", \"read_at\", \"created_by\", \"created_at\") "
				." VALUES ('".$object->data."', NULL,'".$object->created_by."','".$object->created_at."') ";
		// print_r($sql);
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"notification\" SET ";
		$flag = false;
		if($object->id != '') { if($flag){ $sql .= ","; } $sql .= " \"id\" = '".$object->id."' "; $flag = true; }
		if($object->data != '') { if($flag){ $sql .= ","; } $sql .= " \"data\" = '".$object->data."' "; $flag = true; }
		if($object->read_at != '') { if($flag){ $sql .= ","; } $sql .= " \"read_at\" = '".$object->read_at."' "; $flag = true; }
		if($object->created_by != '') { if($flag){ $sql .= ","; } $sql .= " \"created_by\" = '".$object->created_by."' "; $flag = true; }
		if($object->created_at != '') { if($flag){ $sql .= ","; } $sql .= " \"created_at\" = '".$object->created_at."' "; $flag = true; }
		$sql .= " WHERE \"id\" = '".$object->id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"notification\" "
				." WHERE \"id\" = '".$object->id."' ";
		
		$success = $connection->execute($sql);
		
		return $success;
	}

	public function readAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "update notification set read_at = '".date('Y-m-d H:i:s')."' where read_at is NULL";
		// print_r($sql);
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
}
