<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Stock extends Model
{

	public $tgl_gr;
	public $no_gr;
	public $stock_id;
	public $qty;	
	public $sisa;
	public $total;

	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"tgl_gr\", \"no_gr\", \"stock_id\", \"qty\", \"sisa\", \"total\" "
				." FROM \"t_stock\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['tgl_gr'] = $result[0];
			$lists[$count]['no_gr'] = $result[1];
			$lists[$count]['stock_id'] = $result[2];
			$lists[$count]['qty'] = $result[3];
			$lists[$count]['sisa'] = $result[4];
			$lists[$count]['total'] = $result[5];
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"tgl_gr\", \"no_gr\", \"stock_id\", \"qty\", \"sisa\", \"total\" "
				." FROM \"t_stock\" "
				." WHERE \"tgl_gr\" = '".$object->tgl_gr."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['tgl_gr'] = $result[0];
			$lists[$count]['no_gr'] = $result[1];
			$lists[$count]['stock_id'] = $result[2];
			$lists[$count]['qty'] = $result[3];
			$lists[$count]['sisa'] = $result[4];
			$lists[$count]['total'] = $result[5];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"tgl_gr\", \"no_gr\", \"stock_id\", \"qty\", \"sisa\", \"total\" "
				." FROM \"t_stock\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['tgl_gr'] = $result[0];
			$lists[$count]['no_gr'] = $result[1];
			$lists[$count]['stock_id'] = $result[2];
			$lists[$count]['qty'] = $result[3];
			$lists[$count]['sisa'] = $result[4];
			$lists[$count]['total'] = $result[5];
			
			$count++;
		}
		
		return $lists;
	}

	public function getFreeSQL2($cond){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"tgl_gr\", \"no_gr\", \"stock_id\", \"qty\", \"sisa\", \"total\" "
				." FROM \"t_stock\" ".$cond;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['tgl_gr'] = $result[0];
			$lists[$count]['no_gr'] = $result[1];
			$lists[$count]['stock_id'] = $result[2];
			$lists[$count]['qty'] = $result[3];
			$lists[$count]['sisa'] = $result[4];
			$lists[$count]['total'] = $result[5];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"tgl_gr\") "
				." FROM \"t_stock\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "INSERT INTO \"t_stock\" (\"tgl_gr\", \"no_gr\", \"stock_id\", \"qty\", \"sisa\", \"total\") "
				." VALUES ('".$object->tgl_gr."','".$object->no_gr."','".$object->stock_id."','".$object->qty."','".$object->sisa."','".$object->total."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_stock\" SET ";
		$flag = false;
		if($object->no_gr != '') { if($flag){ $sql .= ","; } $sql .= " \"no_gr\" = '".$object->no_gr."' "; $flag = true; }
		if($object->stock_id != '') { if($flag){ $sql .= ","; } $sql .= " \"stock_id\" = '".$object->stock_id."' "; $flag = true; }
		if($object->qty != '') { if($flag){ $sql .= ","; } $sql .= " \"qty\" = '".$object->qty."' "; $flag = true; }
		if($object->sisa != '') { if($flag){ $sql .= ","; } $sql .= " \"sisa\" = '".$object->sisa."' "; $flag = true; }
		if($object->total != '') { if($flag){ $sql .= ","; } $sql .= " \"total\" = '".$object->total."' "; $flag = true; }
		$sql .= " WHERE \"stock_id\" = '".$object->stock_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}

	public function goUpdate2($object){
		$connection = new Postgresql($this->di['db']);
		if($object->sisa == 0) { $object->sisa = '0'; }
		$sql = " UPDATE \"t_stock\" SET ";
		$flag = false;
		if($object->sisa != '') { if($flag){ $sql .= ","; } $sql .= " \"sisa\" = '".$object->sisa."' "; $flag = true; }
		if($object->total != '') { if($flag){ $sql .= ","; } $sql .= " \"total\" = '".$object->total."' "; $flag = true; }
		$sql .= " WHERE \"no_gr\" = '".$object->no_gr."' AND \"stock_id\" = '".pg_escape_string($object->stock_id)."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}

	public function getSumSisa($cond){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT SUM(\"sisa\")"
				." FROM \"t_stock\" ".$cond;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}

	public function goUpdate3($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_stock\" SET ";
		$flag = false;
		if($object->total != '') { if($flag){ $sql .= ","; } $sql .= " \"total\" = '".$object->total."' "; $flag = true; }
		$sql .= " WHERE \"stock_id\" = '".pg_escape_string($object->stock_id)."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}

	public function goUpdate4($object){
		$connection = new Postgresql($this->di['db']);
		if($object->qty == 0) { $object->qty = '0'; }
		$sql = " UPDATE \"t_stock\" SET ";
		$flag = false;
		if($object->qty != '') { if($flag){ $sql .= ","; } $sql .= " \"qty\" = '".$object->qty."' "; $flag = true; }
		$sql .= " WHERE \"no_gr\" = '".$object->no_gr."' AND \"stock_id\" = '".pg_escape_string($object->stock_id)."' ";	
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_stock\" "
				." WHERE \"tgl_gr\" = '".$object->tgl_gr."' ";
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
}
