<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class StartEndDay extends Model
{

	public $id;
	public $tanggal;
	public $user_id;
	public $flag_start;	
	public $flag_end;
	public $jam;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"tanggal\", \"user_id\", \"flag_start\", \"flag_end\", \"jam\" "
				." FROM \"t_start_end_day\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['tanggal'] = $result[1];
			$lists[$count]['user_id'] = $result[2];
			$lists[$count]['flag_start'] = $result[3];
			$lists[$count]['flag_end'] = $result[4];
			$lists[$count]['jam'] = $result[5];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"tanggal\", \"user_id\", \"flag_start\", \"flag_end\", \"jam\" "
				." FROM \"t_start_end_day\" "
				." WHERE \"id\" = '".$object->id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['tanggal'] = $result[1];
			$lists[$count]['user_id'] = $result[2];
			$lists[$count]['flag_start'] = $result[3];
			$lists[$count]['flag_end'] = $result[4];
			$lists[$count]['jam'] = $result[5];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"tanggal\", \"user_id\", \"flag_start\", \"flag_end\", \"jam\" "
				." FROM \"t_start_end_day\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['tanggal'] = $result[1];
			$lists[$count]['user_id'] = $result[2];
			$lists[$count]['flag_start'] = $result[3];
			$lists[$count]['flag_end'] = $result[4];
			$lists[$count]['jam'] = $result[5];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"id\") "
				." FROM \"t_start_end_day\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->flag_start == '') { $object->flag_start = 'f'; }
		if($object->flag_end == '') { $object->flag_end = 'f'; }
		
		$sql = "INSERT INTO \"t_start_end_day\" (\"tanggal\", \"user_id\", \"flag_start\", \"flag_end\", \"jam\") "
				." VALUES ('".$object->tanggal."','".$object->user_id."','".$object->flag_start."','".$object->flag_end."','".$object->jam."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_start_end_day\" SET ";
		$flag = false;
		if($object->tanggal != '') { if($flag){ $sql .= ","; } $sql .= " \"tanggal\" = '".$object->tanggal."' "; $flag = true; }
		if($object->user_id != '') { if($flag){ $sql .= ","; } $sql .= " \"user_id\" = '".$object->user_id."' "; $flag = true; }
		if($object->flag_start != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_start\" = '".$object->flag_start."' "; $flag = true; }
		if($object->flag_end != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_end\" = '".$object->flag_end."' "; $flag = true; }
		if($object->jam != '') { if($flag){ $sql .= ","; } $sql .= " \"jam\" = '".$object->jam."' "; $flag = true; }
		$sql .= " WHERE \"id\" = '".$object->id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_start_end_day\" "
				." WHERE \"id\" = '".$object->id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
