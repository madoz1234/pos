<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Member extends Model
{

	public $branch_code;
	public $debtor_no;
	public $br_name;
	public $branch_ref;	
	public $br_address;
	public $contact_name;
	public $default_location;	
	public $tax_group_id;
	public $br_post_address;
	public $notes;
	public $inactive;
	public $nomor_member;
	public $credit_limit;
	public $telepon;
	public $email;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"branch_code\", \"debtor_no\", \"br_name\", \"branch_ref\", \"br_address\", \"contact_name\", \"default_location\" "
				.", \"tax_group_id\", \"br_post_address\", \"notes\", \"inactive\", \"nomor_member\", \"credit_limit\", \"telepon\", \"email\" "
				." FROM \"m_cust_member\" "
				." WHERE \"inactive\" != 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['branch_code'] = $result[0];
			$lists[$count]['debtor_no'] = $result[1];
			$lists[$count]['br_name'] = $result[2];
			$lists[$count]['branch_ref'] = $result[3];
			$lists[$count]['br_address'] = $result[4];			
			$lists[$count]['contact_name'] = $result[5];	
			$lists[$count]['default_location'] = $result[6];	
			$lists[$count]['tax_group_id'] = $result[7];
			$lists[$count]['br_post_address'] = $result[8];
			$lists[$count]['notes'] = $result[9];
			$lists[$count]['inactive'] = $result[10];
			$lists[$count]['nomor_member'] = $result[11];
			$lists[$count]['credit_limit'] = $result[12];
			$lists[$count]['telepon'] = $result[13];
			$lists[$count]['email'] = $result[14];
			
			$count++;
		}
		
		return $lists;
	}
	
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"branch_code\", \"debtor_no\", \"br_name\", \"branch_ref\", \"br_address\", \"contact_name\", \"default_location\" "
				.", \"tax_group_id\", \"br_post_address\", \"notes\", \"inactive\", \"nomor_member\", \"credit_limit\", \"telepon\", \"email\" "
				." FROM \"m_cust_member\" "
				." WHERE \"branch_code\" = '".$object->branch_code."' "
				." AND \"debtor_no\" = '".$object->debtor_no."' "
				." LIMIT 1 ";

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['branch_code'] = $result[0];
			$lists[$count]['debtor_no'] = $result[1];
			$lists[$count]['br_name'] = $result[2];
			$lists[$count]['branch_ref'] = $result[3];
			$lists[$count]['br_address'] = $result[4];			
			$lists[$count]['contact_name'] = $result[5];	
			$lists[$count]['default_location'] = $result[6];	
			$lists[$count]['tax_group_id'] = $result[7];
			$lists[$count]['br_post_address'] = $result[8];
			$lists[$count]['notes'] = $result[9];
			$lists[$count]['inactive'] = $result[10];
			$lists[$count]['nomor_member'] = $result[11];
			$lists[$count]['credit_limit'] = $result[12];
			$lists[$count]['telepon'] = $result[13];
			$lists[$count]['email'] = $result[14];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"branch_code\", \"debtor_no\", \"br_name\", \"branch_ref\", \"br_address\", \"contact_name\", \"default_location\" "
				.", \"tax_group_id\", \"br_post_address\", \"notes\", \"inactive\", \"nomor_member\", \"credit_limit\", \"telepon\", \"email\" "
				." FROM \"m_cust_member\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['branch_code'] = $result[0];
			$lists[$count]['debtor_no'] = $result[1];
			$lists[$count]['br_name'] = $result[2];
			$lists[$count]['branch_ref'] = $result[3];
			$lists[$count]['br_address'] = $result[4];			
			$lists[$count]['contact_name'] = $result[5];	
			$lists[$count]['default_location'] = $result[6];	
			$lists[$count]['tax_group_id'] = $result[7];
			$lists[$count]['br_post_address'] = $result[8];
			$lists[$count]['notes'] = $result[9];
			$lists[$count]['inactive'] = $result[10];
			$lists[$count]['nomor_member'] = $result[11];	
			$lists[$count]['credit_limit'] = $result[12];
			$lists[$count]['telepon'] = $result[13];
			$lists[$count]['email'] = $result[14];
			
			$count++;
		}
		
		return $lists;
	}

	public function getMemberKredit(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT m.\"nomor_member\", m.\"br_name\", SUM(bayar.\"amount\") "
				." FROM \"m_cust_member\" m JOIN  \"t_member_pay\" bayar "
				." ON m.nomor_member = bayar.id_member "
				." GROUP BY m.nomor_member,m.br_name ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['nomor_member'] = $result[0];	
			$lists[$count]['br_name'] = $result[1];
			$lists[$count]['hutang'] = $result[2];
			
			$count++;
		}
		
		return $lists;
	}

	public function getMemberPiutangAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT m.\"debtor_no\", m.\"nomor_member\", m.\"br_name\", bayar.\"total\", jual.\"tanggal\" "
				." FROM \"t_jual_bayar\" bayar JOIN  \"m_cust_member\" m "
				." ON m.nomor_member = bayar.no_kartu "
				." JOIN t_jual jual"
				." ON jual.id_jual = bayar.id_jual "
				." WHERE bayar.tipe = 'PIUTANG' ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['tmuk_kode'] = $result[0];	
			$lists[$count]['nomor_member'] = $result[1];
			$lists[$count]['nama_member'] = $result[2];
			$lists[$count]['total'] = $result[3];
			$lists[$count]['tanggal'] = $result[4];
			
			$count++;
		}
		
		return $lists;
	}


	public function getMemberPointAll(){
		$connection = new Postgresql($this->di['db']);

		$sql = "SELECT m.\"debtor_no\", m.\"nomor_member\", m.\"br_name\", p.\"point\", jual.\"tanggal\" "
				." FROM \"t_point\" p JOIN  \"m_cust_member\" m "
				." ON m.nomor_member = p.id_member "
				." JOIN t_jual jual"
				." ON jual.id_jual = p.id_ref "
				." WHERE p.point > 0 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['tmuk_kode'] = $result[0];	
			$lists[$count]['nomor_member'] = $result[1];
			$lists[$count]['nama_member'] = $result[2];
			$lists[$count]['point'] = $result[3];
			$lists[$count]['tanggal'] = $result[4];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"branch_code\") "
				." FROM \"m_cust_member\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}

	public function getNamaMember($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"br_name\" "
				." FROM \"m_cust_member\" ".$condition;
		
		$results = $connection->query($sql);

		$count = '';
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}

	public function getDebtorMember($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"debtor_no\" "
				." FROM \"m_cust_member\" ".$condition;
		
		$results = $connection->query($sql);

		$count = '';
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->inactive == '') { $object->inactive = '0'; }	
		if($object->tax_group_id == '') { $object->tax_group_id = '0'; }
		
		//cek apakah untuk cust tsb ada nomor member yang sama.
		$sql  = "SELECT * FROM m_cust_member WHERE debtor_no = '".$object->debtor_no."' and nomor_member = '".$object->nomor_member."'";
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count++;
		}
		
		if($count == 0)
		{
			//ambil max branch_code dari cust yang dipilih,.
			$sql  = "SELECT CASE WHEN max(branch_code) IS NULL THEN 1 ELSE MAX(branch_code)+1 END FROM m_cust_member WHERE debtor_no = '".$object->debtor_no."'";
			$results = $connection->query($sql);
			
			$count = 0;
			$lists = null;
			$results->setFetchMode(Phalcon\Db::FETCH_NUM);
			while ($result = $results->fetchArray()) {
				$branch_code = $result[0];
			}
			//$object->branch_code
			$sql = "INSERT INTO \"m_cust_member\" (\"branch_code\",\"debtor_no\", \"br_name\", \"branch_ref\", \"br_address\", \"contact_name\", \"default_location\", \"tax_group_id\" "
					.", \"br_post_address\", \"notes\", \"inactive\", \"nomor_member\", \"credit_limit\", \"telepon\", \"email\") "
					." VALUES ('".$branch_code."','".$object->debtor_no."','".$object->br_name."','".$object->branch_ref."','".$object->br_address
					."','".$object->contact_name."','".$object->default_location."','".$object->tax_group_id."','".$object->br_post_address
					."','".$object->notes."','".$object->inactive."','".$object->nomor_member."','".$object->credit_limit."', '".$object->telepon."', '".$object->email."') ";
			
			//echo $sql;
			
			$success = $connection->execute($sql);
			$id = $connection->lastInsertId();
			
			return $sql;
		}else{
			return false;
		}
	}
	
	public function goMultiInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "INSERT INTO \"m_cust_member\" (\"branch_code\", \"debtor_no\", \"br_name\", \"branch_ref\", \"br_address\", \"contact_name\", \"default_location\", \"tax_group_id\" "
				.", \"br_post_address\", \"notes\", \"inactive\", \"nomor_member\", \"telepon\", \"email\") "
				." VALUES ".$object;

		// dd($sql);
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_cust_member\" SET ";
		$flag = false;		
		if($object->br_name != '') { if($flag){ $sql .= ","; } $sql .= " \"br_name\" = '".$object->br_name."' "; $flag = true; }
		if($object->branch_ref != '') { if($flag){ $sql .= ","; } $sql .= " \"branch_ref\" = '".$object->branch_ref."' "; $flag = true; }
		if($object->br_address != '') { if($flag){ $sql .= ","; } $sql .= " \"br_address\" = '".$object->br_address."' "; $flag = true; }
		if($object->contact_name != '') { if($flag){ $sql .= ","; } $sql .= " \"contact_name\" = '".$object->contact_name."' "; $flag = true; }
		if($object->default_location != '') { if($flag){ $sql .= ","; } $sql .= " \"default_location\" = '".$object->default_location."' "; $flag = true; }
		if($object->tax_group_id != '') { if($flag){ $sql .= ","; } $sql .= " \"tax_group_id\" = '".$object->tax_group_id."' "; $flag = true; }
		if($object->br_post_address != '') { if($flag){ $sql .= ","; } $sql .= " \"br_post_address\" = '".$object->br_post_address."' "; $flag = true; }
		if($object->notes != '') { if($flag){ $sql .= ","; } $sql .= " \"notes\" = '".$object->notes."' "; $flag = true; }
		if($object->inactive != '') { if($flag){ $sql .= ","; } $sql .= " \"inactive\" = '".$object->inactive."' "; $flag = true; }
		if($object->nomor_member != '') { if($flag){ $sql .= ","; } $sql .= " \"nomor_member\" = '".$object->nomor_member."' "; $flag = true; }		
		if($object->credit_limit != '') { if($flag){ $sql .= ","; } $sql .= " \"credit_limit\" = '".$object->credit_limit."' "; $flag = true; }
		if($object->telepon != '') { if($flag){ $sql .= ","; } $sql .= " \"telepon\" = '".$object->telepon."' "; $flag = true; }
		if($object->email != '') { if($flag){ $sql .= ","; } $sql .= " \"email\" = '".$object->email."' "; $flag = true; }
		$sql .= " WHERE \"branch_code\" = '".$object->branch_code."' "	
				." AND \"debtor_no\" = '".$object->debtor_no."' ";			
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	// ==
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_cust_member\" "
				." WHERE \"branch_code\" = '".$object->branch_code."' "	
				." AND \"debtor_no\" = '".$object->debtor_no."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goDeleteDebtor($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_cust_member\" "
				." WHERE \"debtor_no\" = '".$object->debtor_no."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goMultiDelete($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " DELETE FROM \"m_cust_member\" "
				." WHERE \"branch_code\" IN (".$object.") ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function getMemberManagement(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT a.\"branch_code\", a.\"debtor_no\", a.\"br_name\", a.\"branch_ref\", a.\"br_address\", a.\"contact_name\", a.\"default_location\" "
				.", a.\"tax_group_id\", a.\"br_post_address\", a.\"notes\", a.\"inactive\", a.\"nomor_member\", b.\"total_order\", b.\"total_purchase\", 
				b.\"last_visit\",
				CASE WHEN b.\"total_order\" > 0 THEN (b.\"total_purchase\" / b.\"total_order\") ELSE 0 END,
				CEIL( b.\"total_order\" / CASE WHEN ((abs(date_part('year',age(now(), b.\"first_visit\")))*12) + abs(date_part('month',age(now(), b.\"first_visit\")))) > 0
					THEN ((abs(date_part('year',age(now(), b.\"first_visit\")))*12) + abs(date_part('month',age(now(), b.\"first_visit\"))))
					ELSE 1
				END ),  
				CEIL( b.\"total_order\" / CASE WHEN abs(date_part('year',age(now(), b.\"first_visit\"))) > 0
					THEN abs(date_part('year',age(now(), b.\"first_visit\")))
					ELSE 1
				END ),
				c.\"point\", a.\"credit_limit\" "
				." FROM \"m_cust_member\" a LEFT JOIN (  
					SELECT \"id_member\", count(\"id_jual\") as \"total_order\", sum(\"total\") as \"total_purchase\", 
					max(\"tanggal\") as \"last_visit\", min(\"tanggal\") as \"first_visit\"
					FROM \"t_jual\"
					WHERE \"id_member\" is not null 
					AND trim(\"id_member\") <> ''
					GROUP BY \"id_member\"
				) b ON a.\"nomor_member\" = b.\"id_member\" "
				." left join (
					SELECT \"id_member\", sum(\"point\") as \"point\"
					FROM \"t_point\"
					WHERE \"id_member\" is not null 
					AND trim(\"id_member\") <> ''
					GROUP BY \"id_member\"
				) c ON a.\"nomor_member\" = c.\"id_member\" "
				." WHERE \"inactive\" != 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['branch_code'] = $result[0];
			$lists[$count]['debtor_no'] = $result[1];
			$lists[$count]['br_name'] = $result[2];
			$lists[$count]['branch_ref'] = $result[3];
			$lists[$count]['br_address'] = $result[4];
			$lists[$count]['contact_name'] = $result[5];	
			$lists[$count]['default_location'] = $result[6];	
			$lists[$count]['tax_group_id'] = $result[7];
			$lists[$count]['br_post_address'] = $result[8];
			$lists[$count]['notes'] = $result[9];
			$lists[$count]['inactive'] = $result[10];
			$lists[$count]['nomor_member'] = $result[11];
			$lists[$count]['total_order'] = $result[12]!=''?$result[12]:0;
			$lists[$count]['total_purchase'] = $result[13]!=''?$result[13]:0;
			$lists[$count]['last_visit'] = $result[14];
			$lists[$count]['average_purchase'] = $result[15]!=''?$result[15]:0;
			$lists[$count]['average_visit_mon'] = $result[16]!=''?$result[16]:0;
			$lists[$count]['average_visit_year'] = $result[17]!=''?$result[17]:0;
			$lists[$count]['point'] = $result[18]!=''?$result[18]:0;
			$lists[$count]['credit_limit'] = $result[19]!=''?$result[19]:0;
			$lists[$count]['total_credit'] = '0';	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirstMemberManagement($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT a.\"branch_code\", a.\"debtor_no\", a.\"br_name\", a.\"branch_ref\", a.\"br_address\", a.\"telepon\", a.\"email\", a.\"contact_name\", a.\"default_location\" "
				.", a.\"tax_group_id\", a.\"br_post_address\", a.\"notes\", a.\"inactive\", a.\"nomor_member\", b.\"total_order\", b.\"total_purchase\", 
				b.\"last_visit\",
				CASE WHEN b.\"total_order\" > 0 THEN (b.\"total_purchase\" / b.\"total_order\") ELSE 0 END,
				CEIL( b.\"total_order\" / CASE WHEN ((abs(date_part('year',age(now(), b.\"first_visit\")))*12) + abs(date_part('month',age(now(), b.\"first_visit\")))) > 0
					THEN ((abs(date_part('year',age(now(), b.\"first_visit\")))*12) + abs(date_part('month',age(now(), b.\"first_visit\"))))
					ELSE 1
				END ),  
				CEIL( b.\"total_order\" / CASE WHEN abs(date_part('year',age(now(), b.\"first_visit\"))) > 0
					THEN abs(date_part('year',age(now(), b.\"first_visit\")))
					ELSE 1
				END ),
				c.\"point\", a.\"credit_limit\", (b.\"total_credit\"), d.\"amount\" "
				." FROM \"m_cust_member\" a LEFT JOIN (  
					SELECT a.\"id_member\", count(a.\"id_jual\") as \"total_order\", sum(a.\"total\") as \"total_purchase\", 
					max(a.\"tanggal\") as \"last_visit\", min(a.\"tanggal\") as \"first_visit\",
					sum(a.\"total_credit\") as total_credit
					FROM ( SELECT a.\"id_member\", a.\"id_jual\", a.\"total\", a.\"tanggal\", b.\"total\" as \"total_credit\" 
							FROM \"t_jual\" a JOIN \"t_jual_bayar\" b ON a.\"id_jual\" = b.\"id_jual\"
							WHERE b.\"tipe\" = 'PIUTANG' ) a					
					WHERE \"id_member\" is not null 
					AND trim(\"id_member\") <> ''
					GROUP BY \"id_member\"
				) b ON a.\"nomor_member\" = b.\"id_member\" "
				." left join (
					SELECT \"id_member\", sum(\"point\") as \"point\"
					FROM \"t_point\"
					WHERE \"id_member\" is not null 
					AND trim(\"id_member\") <> ''
					GROUP BY \"id_member\"
				) c ON a.\"nomor_member\" = c.\"id_member\" "
				." left join (
					SELECT \"id_member\", sum(\"amount\") as \"amount\"
					FROM \"t_member_pay\"
					WHERE \"branch_code\" = '".$object->branch_code."' AND \"debtor_no\" = '".$object->debtor_no."' 
					GROUP BY \"id_member\"
				) d ON a.\"nomor_member\" = d.\"id_member\" "
				." WHERE \"inactive\" != 1 "
				." AND \"branch_code\" = '".$object->branch_code."' AND \"debtor_no\" = '".$object->debtor_no."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['branch_code'] = $result[0];
			$lists[$count]['debtor_no'] = $result[1];
			$lists[$count]['br_name'] = $result[2];
			$lists[$count]['branch_ref'] = $result[3];
			$lists[$count]['br_address'] = $result[4];
			$lists[$count]['telepon'] = $result[5];			
			$lists[$count]['email'] = $result[6];	
			$lists[$count]['contact_name'] = $result[7];	
			$lists[$count]['default_location'] = $result[8];	
			$lists[$count]['tax_group_id'] = $result[9];
			$lists[$count]['br_post_address'] = $result[10];
			$lists[$count]['notes'] = $result[11];
			$lists[$count]['inactive'] = $result[12];
			$lists[$count]['nomor_member'] = $result[13];
			$lists[$count]['total_order'] = $result[14]!=''?$result[14]:0;
			$lists[$count]['total_purchase'] = $result[15]!=''?$result[15]:0;
			$lists[$count]['last_visit'] = $result[16];
			$lists[$count]['average_purchase'] = $result[17]!=''?$result[17]:0;
			$lists[$count]['average_visit_mon'] = $result[18]!=''?$result[18]:0;
			$lists[$count]['average_visit_year'] = $result[19]!=''?$result[19]:0;
			$lists[$count]['point'] = $result[20]!=''?$result[20]:0;
			$lists[$count]['credit_limit'] = $result[21]!=''?$result[21]:0;
			$lists[$count]['total_credit'] = $result[22]!=''?$result[22]:0;			
			$lists[$count]['payment'] = $result[23]!=''?$result[23]:0;	
			
			$count++;
		}
		
		return $lists;
	}
	
	
	public function reportAR(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "select nomor_member, br_name, br_address, credit_limit, tipe, sum(c.total), max(d.amount), sum(kembalian)
			from m_cust_member a
			inner join t_jual b on a.nomor_member=b.id_member
			inner join t_jual_bayar c on b.id_jual=c.id_jual
			left join t_member_pay d on a.nomor_member=d.id_member
			where nomor_member != '' and tipe != 'DONASI'
			group by nomor_member, br_name, br_address, credit_limit, tipe
			order by nomor_member, tipe desc";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['nomor_member'] = $result[0];
			$lists[$count]['br_name'] = $result[1];
			$lists[$count]['br_address'] = $result[2];			
			$lists[$count]['credit_limit'] = $result[3];	
			$lists[$count]['tipe'] = $result[4];	
			$lists[$count]['total_purchase'] = $result[5];
			$lists[$count]['piutang_payment'] = $result[6];
			$lists[$count]['kembalian'] = $result[7];
			
			$count++;
		}
		
		return $lists;
	}

	public function jeniskustomer(){
		$record = new JenisKustomer();
		$lists = $record::getAll();
		// dd($lists);
		
		$data = null;		
		if(isset($lists)){
			foreach($lists as $list){				
				$data->branch_ref = $list['jenis'];
			}
		}
		return $data;
	}

}
