<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Kassa extends Model
{

	public $kassa_id;
	public $description;
	public $inactive;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"kassa_id\", \"description\", \"inactive\" "
				." FROM \"m_kassa\" "
				." WHERE \"inactive\" != 1 ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['kassa_id'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$lists[$count]['inactive'] = $result[2];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"kassa_id\", \"description\", \"inactive\" "
				." FROM \"m_kassa\" "
				." WHERE \"kassa_id\" = '".$object->kassa_id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['kassa_id'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$lists[$count]['inactive'] = $result[2];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"kassa_id\", \"description\", \"inactive\" "
				." FROM \"m_kassa\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['kassa_id'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$lists[$count]['inactive'] = $result[2];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"kassa_id\") "
				." FROM \"m_kassa\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->inactive == '') { $object->inactive = '0'; }
		
		$sql = "INSERT INTO \"m_kassa\" (\"description\", \"inactive\") "
				." VALUES ('".$object->description."','".$object->inactive."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goMultiInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "INSERT INTO \"m_kassa\" (\"kassa_id\", \"description\") "
			." VALUES ".$object;
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_kassa\" SET ";
		$flag = false;
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".$object->description."' "; $flag = true; }
		if($object->inactive != '') { if($flag){ $sql .= ","; } $sql .= " \"inactive\" = '".$object->inactive."' "; $flag = true; }
		$sql .= " WHERE \"kassa_id\" = '".$object->kassa_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_kassa\" "
				." WHERE \"kassa_id\" = '".$object->kassa_id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goMultiDelete($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " DELETE FROM \"m_kassa\" "
				." WHERE \"kassa_id\" IN (".$object.") ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
}
