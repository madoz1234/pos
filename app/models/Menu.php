<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Menu extends Model
{

	public $menu_id;
	public $parent_id;
	public $sort_id;
	public $title;	
	public $view;
	public $icon_title;
	public $image_path;	
	public $depth;
	public $is_active;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"menu_id\", \"parent_id\", \"sort_id\", \"title\", \"view\", \"icon_title\", \"image_path\", \"depth\", \"is_active\" "
				." FROM \"m_menu\" "
				." WHERE \"is_active\" = 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['menu_id'] = $result[0];
			$lists[$count]['parent_id'] = $result[1];
			$lists[$count]['sort_id'] = $result[2];
			$lists[$count]['title'] = $result[3];
			$lists[$count]['view'] = $result[4];			
			$lists[$count]['icon_title'] = $result[5];	
			$lists[$count]['image_path'] = $result[6];	
			$lists[$count]['depth'] = $result[7];
			$lists[$count]['is_active'] = $result[8];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"menu_id\", \"parent_id\", \"sort_id\", \"title\", \"view\", \"icon_title\", \"image_path\", \"depth\", \"is_active\" "
				." FROM \"m_menu\" "
				." WHERE \"view\" = '".$object->view."' "				
				." LIMIT 1 ";

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['menu_id'] = $result[0];
			$lists[$count]['parent_id'] = $result[1];
			$lists[$count]['sort_id'] = $result[2];
			$lists[$count]['title'] = $result[3];
			$lists[$count]['view'] = $result[4];			
			$lists[$count]['icon_title'] = $result[5];	
			$lists[$count]['image_path'] = $result[6];	
			$lists[$count]['depth'] = $result[7];
			$lists[$count]['is_active'] = $result[8];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"menu_id\", \"parent_id\", \"sort_id\", \"title\", \"view\", \"icon_title\", \"image_path\", \"depth\", \"is_active\" "
				." FROM \"m_menu\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['menu_id'] = $result[0];
			$lists[$count]['parent_id'] = $result[1];
			$lists[$count]['sort_id'] = $result[2];
			$lists[$count]['title'] = $result[3];
			$lists[$count]['view'] = $result[4];			
			$lists[$count]['icon_title'] = $result[5];	
			$lists[$count]['image_path'] = $result[6];	
			$lists[$count]['depth'] = $result[7];
			$lists[$count]['is_active'] = $result[8];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"title\") "
				." FROM \"m_menu\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->is_active == '') { $object->is_active = '1'; }		

		$sql = "INSERT INTO \"m_menu\" (\"parent_id\", \"sort_id\", \"title\", \"view\", \"icon_title\", \"image_path\", \"depth\", \"is_active\") "
				." VALUES ('".$object->parent_id."','".$object->sort_id."','".$object->title."','".$object->view."','"
				.$object->icon_title."','".$object->image_path."','".$object->depth."','".$object->is_active."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_menu\" SET ";
		$flag = false;		
		if($object->parent_id != '') { if($flag){ $sql .= ","; } $sql .= " \"parent_id\" = '".$object->parent_id."' "; $flag = true; }
		if($object->sort_id != '') { if($flag){ $sql .= ","; } $sql .= " \"sort_id\" = '".$object->sort_id."' "; $flag = true; }
		if($object->title != '') { if($flag){ $sql .= ","; } $sql .= " \"title\" = '".$object->title."' "; $flag = true; }
		if($object->view != '') { if($flag){ $sql .= ","; } $sql .= " \"view\" = '".$object->view."' "; $flag = true; }
		if($object->icon_title != '') { if($flag){ $sql .= ","; } $sql .= " \"icon_title\" = '".$object->icon_title."' "; $flag = true; }
		if($object->image_path != '') { if($flag){ $sql .= ","; } $sql .= " \"image_path\" = '".$object->image_path."' "; $flag = true; }
		if($object->depth != '') { if($flag){ $sql .= ","; } $sql .= " \"depth\" = '".$object->depth."' "; $flag = true; }
		if($object->is_active != '') { if($flag){ $sql .= ","; } $sql .= " \"is_active\" = '".$object->is_active."' "; $flag = true; }
		$sql .= " WHERE \"menu_id\" = '".$object->menu_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_menu\" "
				." WHERE \"menu_id\" = '".$object->menu_id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
