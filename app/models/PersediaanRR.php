<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class PersediaanRR extends Model
{

	public $stock_id;
	public $qty;
	public $tanggal;	
	public $flag_sync;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"stock_id\", \"qty\", \"tanggal\", \"flag_sync\" "
				." FROM \"persediaan_rr\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['qty'] = $result[1];
			$lists[$count]['tanggal'] = $result[2];
			$lists[$count]['flag_sync'] = $result[3];	
			$count++;
		}
		
		return $lists;
	}

	public function getDataSyncPersediaan()
	{	

		$list = [];
		$persediaan_rr = new PersediaanRR();
		$condition = " WHERE flag_sync = false LIMIT 10";
		$lists_rr = $persediaan_rr::getFreeSQL($condition);

		$c_persediaan_rr = 0;
		if(count($lists_rr)>0){
			foreach($lists_rr as $list1){
				$list[$c_persediaan_rr]['stock_id']    = $list1['stock_id'];
				$list[$c_persediaan_rr]['qty'] = $list1['qty'];
				$list[$c_persediaan_rr]['tanggal']     = $list1['tanggal'];
				$c_persediaan_rr++;
			}
		}

		return $list;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"stock_id\", \"qty\", \"tanggal\", \"flag_sync\" "
				." FROM \"persediaan_rr\" "
				." WHERE \"stock_id\" = '".$object->stock_id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['qty'] = $result[1];
			$lists[$count]['tanggal'] = $result[2];
			$lists[$count]['flag_sync'] = $result[3];	
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"stock_id\", \"qty\", \"tanggal\", \"flag_sync\" "
				." FROM \"persediaan_rr\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['qty'] = $result[1];
			$lists[$count]['tanggal'] = $result[2];
			$lists[$count]['flag_sync'] = $result[3];	
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"stock_id\") "
				." FROM \"persediaan_rr\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}

	public static function queryInsertRR($object)
	{
		$sql = "INSERT INTO \"persediaan_rr\" (\"stock_id\", \"qty\" "
				.", \"tanggal\", \"flag_sync\" ) VALUES ";

		return $sql;
	}
	
	public static function insertSql($object)
	{
		if($object->qty == '') { $object->qty = '0'; }	
		if($object->flag_sync == '') { $object->flag_sync = '0'; }	

		$sql = "('".$object->stock_id."','".$object->qty."','"
				.$object->tanggal."','".$object->flag_sync."'),";

		return $sql;
	}

	public static function InsertEndSql($object)
	{
		if($object->qty == '') { $object->qty = '0'; }	
		if($object->flag_sync == '') { $object->flag_sync = '0'; }	

		$sql = "('".$object->stock_id."','".$object->qty."','"
				.$object->tanggal."','".$object->flag_sync."') ";

		return $sql;
	}

	public static function endSql($object)
	{
		$sql = "on conflict do nothing;";
		return $sql;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"persediaan_rr\" SET ";
		if($object->flag_sync != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = '".$object->flag_sync."' "; $flag = true; }	
		$sql .= " WHERE \"stock_id\" = '".$object->stock_id."' AND \"tanggal\" = '".$object->tanggal."' ";	
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"persediaan_rr\" ";
		$sql .= " WHERE \"qty\" = '".$object->qty."' ";		
		$sql .= " AND \"stock_id\" = '".$object->stock_id."' ";	
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function executeQuery($sql){
		$connection = new Postgresql($this->di['db']);
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
