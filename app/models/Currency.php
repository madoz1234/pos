<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Currency extends Model
{

	public $currency;
	public $curr_abrev;
	public $curr_symbol;
	public $country;	
	public $hundreds_name;
	public $auto_update;
	public $inactive;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"currency\", \"curr_abrev\", \"curr_symbol\", \"country\", \"hundreds_name\", \"auto_update\", \"inactive\" "
				." FROM \"m_currency\" "
				." WHERE \"inactive\" != 1 ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['currency'] = $result[0];
			$lists[$count]['curr_abrev'] = $result[1];
			$lists[$count]['curr_symbol'] = $result[2];
			$lists[$count]['country'] = $result[3];
			$lists[$count]['hundreds_name'] = $result[4];			
			$lists[$count]['auto_update'] = $result[5];	
			$lists[$count]['inactive'] = $result[6];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"currency\", \"curr_abrev\", \"curr_symbol\", \"country\", \"hundreds_name\", \"auto_update\", \"inactive\" "
				." FROM \"m_currency\" "
				." WHERE \"currency\" = '".$object->currency."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['currency'] = $result[0];
			$lists[$count]['curr_abrev'] = $result[1];
			$lists[$count]['curr_symbol'] = $result[2];
			$lists[$count]['country'] = $result[3];
			$lists[$count]['hundreds_name'] = $result[4];			
			$lists[$count]['auto_update'] = $result[5];	
			$lists[$count]['inactive'] = $result[6];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"currency\", \"curr_abrev\", \"curr_symbol\", \"country\", \"hundreds_name\", \"auto_update\", \"inactive\" "
				." FROM \"m_currency\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['currency'] = $result[0];
			$lists[$count]['curr_abrev'] = $result[1];
			$lists[$count]['curr_symbol'] = $result[2];
			$lists[$count]['country'] = $result[3];
			$lists[$count]['hundreds_name'] = $result[4];			
			$lists[$count]['auto_update'] = $result[5];	
			$lists[$count]['inactive'] = $result[6];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"currency\") "
				." FROM \"m_currency\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->auto_update == '') { $object->auto_update = '0'; }
		if($object->inactive == '') { $object->inactive = '0'; }
		$sql = "INSERT INTO \"m_currency\" (\"currency\", \"curr_abrev\", \"curr_symbol\", \"country\", \"hundreds_name\", \"auto_update\", \"inactive\") "
				." VALUES ('".$object->currency."','".$object->curr_abrev."','".$object->curr_symbol."','".$object->country."','".$object->hundreds_name."','"
				.$object->auto_update."','".$object->inactive."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_currency\" SET ";
		$flag = false;
		if($object->curr_abrev != '') { if($flag){ $sql .= ","; } $sql .= " \"curr_abrev\" = '".$object->curr_abrev."' "; $flag = true; }
		if($object->curr_symbol != '') { if($flag){ $sql .= ","; } $sql .= " \"curr_symbol\" = '".$object->curr_symbol."' "; $flag = true; }
		if($object->country != '') { if($flag){ $sql .= ","; } $sql .= " \"country\" = '".$object->country."' "; $flag = true; }
		if($object->hundreds_name != '') { if($flag){ $sql .= ","; } $sql .= " \"hundreds_name\" = '".$object->hundreds_name."' "; $flag = true; }
		if($object->auto_update != '') { if($flag){ $sql .= ","; } $sql .= " \"auto_update\" = '".$object->auto_update."' "; $flag = true; }
		if($object->inactive != '') { if($flag){ $sql .= ","; } $sql .= " \"inactive\" = '".$object->inactive."' "; $flag = true; }
		$sql .= " WHERE \"currency\" = '".$object->currency."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_currency\" "
				." WHERE \"currency\" = '".$object->currency."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
