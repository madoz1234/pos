<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Tunda extends Model
{

	public $id_jual;
	public $id_member;
	public $id_kasir;
	public $tanggal;	
	public $subtotal;
	public $tax;
	public $discount;	
	public $total;	
	public $note;
	public $flag_delete;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id_jual\", \"id_member\", \"id_kasir\", \"tanggal\", \"subtotal\", \"tax\", \"discount\", \"total\", \"note\", \"flag_delete\" "
				." FROM \"t_tunda\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_jual'] = $result[0];
			$lists[$count]['id_member'] = $result[1];
			$lists[$count]['id_kasir'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];
			$lists[$count]['subtotal'] = $result[4];			
			$lists[$count]['tax'] = $result[5];	
			$lists[$count]['discount'] = $result[6];	
			$lists[$count]['total'] = $result[7];	
			$lists[$count]['note'] = $result[8];	
			$lists[$count]['flag_delete'] = $result[9];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id_jual\", \"id_member\", \"id_kasir\", \"tanggal\", \"subtotal\", \"tax\", \"discount\", \"total\", \"note\", \"flag_delete\" "
				." FROM \"t_tunda\" "
				." WHERE \"id_jual\" = '".$object->id_jual."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_jual'] = $result[0];
			$lists[$count]['id_member'] = $result[1];
			$lists[$count]['id_kasir'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];
			$lists[$count]['subtotal'] = $result[4];			
			$lists[$count]['tax'] = $result[5];	
			$lists[$count]['discount'] = $result[6];	
			$lists[$count]['total'] = $result[7];	
			$lists[$count]['note'] = $result[8];	
			$lists[$count]['flag_delete'] = $result[9];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id_jual\", \"id_member\", \"id_kasir\", \"tanggal\", \"subtotal\", \"tax\", \"discount\", \"total\", \"note\", \"flag_delete\" "
				." FROM \"t_tunda\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_jual'] = $result[0];
			$lists[$count]['id_member'] = $result[1];
			$lists[$count]['id_kasir'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];
			$lists[$count]['subtotal'] = $result[4];			
			$lists[$count]['tax'] = $result[5];	
			$lists[$count]['discount'] = $result[6];	
			$lists[$count]['total'] = $result[7];	
			$lists[$count]['note'] = $result[8];	
			$lists[$count]['flag_delete'] = $result[9];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"id_jual\") "
				." FROM \"t_tunda\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->tanggal == '') { $object->tanggal = date("Y-m-d"); }
		if($object->subtotal == '') { $object->subtotal = '0'; }
		if($object->tax == '') { $object->tax = '0'; }
		if($object->discount == '') { $object->discount = '0'; }
		if($object->total == '') { $object->total = '0'; }
		if($object->flag_delete == '') { $object->flag_delete = '0'; }
		
		$sql = "INSERT INTO \"t_tunda\" (\"id_jual\", \"id_member\", \"id_kasir\", \"tanggal\", \"subtotal\", \"tax\", \"discount\", \"total\", \"note\", \"flag_delete\") "
				." VALUES ('".$object->id_jual."','".$object->id_member."','".$object->id_kasir."','".$object->tanggal."','".$object->subtotal."','"
				.$object->tax."','".$object->discount."','".$object->total."','".$object->note."','".$object->flag_delete."') ";
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_tunda\" SET ";
		$flag = false;
		if($object->id_member != '') { if($flag){ $sql .= ","; } $sql .= " \"id_member\" = '".$object->id_member."' "; $flag = true; }
		if($object->id_kasir != '') { if($flag){ $sql .= ","; } $sql .= " \"id_kasir\" = '".$object->id_kasir."' "; $flag = true; }
		if($object->tanggal != '') { if($flag){ $sql .= ","; } $sql .= " \"tanggal\" = '".$object->tanggal."' "; $flag = true; }
		if($object->subtotal != '') { if($flag){ $sql .= ","; } $sql .= " \"subtotal\" = '".$object->subtotal."' "; $flag = true; }
		if($object->tax != '') { if($flag){ $sql .= ","; } $sql .= " \"tax\" = '".$object->tax."' "; $flag = true; }
		if($object->discount != '') { if($flag){ $sql .= ","; } $sql .= " \"discount\" = '".$object->discount."' "; $flag = true; }
		if($object->total != '') { if($flag){ $sql .= ","; } $sql .= " \"total\" = '".$object->total."' "; $flag = true; }
		if($object->note != '') { if($flag){ $sql .= ","; } $sql .= " \"note\" = '".$object->note."' "; $flag = true; }
		if($object->flag_delete != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_delete\" = '".$object->flag_delete."' "; $flag = true; }
		$sql .= " WHERE \"id_jual\" = '".$object->id_jual."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_tunda\" "
				." WHERE \"id_jual\" = '".$object->id_jual."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
