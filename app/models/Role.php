<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Role extends Model
{

	public $role_id;
	public $role_name;
	public $description;
	public $is_active;
	public $pr_approve;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"role_id\", \"role_name\", \"description\", \"is_active\", \"pr_approve\" "
				." FROM \"m_role\" "
				." WHERE \"is_active\" = 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['role_id'] = $result[0];
			$lists[$count]['role_name'] = $result[1];
			$lists[$count]['description'] = $result[2];
			$lists[$count]['is_active'] = $result[3];
			$lists[$count]['pr_approve'] = $result[4];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"role_id\", \"role_name\", \"description\", \"is_active\", \"pr_approve\" "
				." FROM \"m_role\" "
				." WHERE \"role_id\" = '".$object->role_id."' "
				." LIMIT 1 ";

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['role_id'] = $result[0];
			$lists[$count]['role_name'] = $result[1];
			$lists[$count]['description'] = $result[2];
			$lists[$count]['is_active'] = $result[3];	
			$lists[$count]['pr_approve'] = $result[4];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"role_id\", \"role_name\", \"description\", \"is_active\", \"pr_approve\" "
				." FROM \"m_role\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['role_id'] = $result[0];
			$lists[$count]['role_name'] = $result[1];
			$lists[$count]['description'] = $result[2];
			$lists[$count]['is_active'] = $result[3];
			$lists[$count]['pr_approve'] = $result[4];			
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"role_name\") "
				." FROM \"m_role\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->is_active == '') { $object->is_active = '1'; }		
		if($object->pr_approve == '') { $object->pr_approve = 'f'; }
		
		$sql = "INSERT INTO \"m_role\" (\"role_name\", \"description\", \"is_active\", \"pr_approve\") "
				." VALUES ('".$object->role_name."','".$object->description."','".$object->is_active."','".$object->pr_approve."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $sql;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_role\" SET ";
		$flag = false;		
		if($object->role_name != '') { if($flag){ $sql .= ","; } $sql .= " \"role_name\" = '".$object->role_name."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".$object->description."' "; $flag = true; }
		if($object->is_active != '') { if($flag){ $sql .= ","; } $sql .= " \"is_active\" = '".$object->is_active."' "; $flag = true; }
		if($object->pr_approve != '') { if($flag){ $sql .= ","; } $sql .= " \"pr_approve\" = '".$object->pr_approve."' "; $flag = true; }
		$sql .= " WHERE \"role_id\" = '".$object->role_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_role\" "
				." WHERE \"role_id\" = '".$object->role_id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
