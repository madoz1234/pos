<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class JualBayarSum extends Model
{

	public $id_kasir;
	public $kassa_id;
	public $shift_id;
	public $tanggal;	
	public $tipe;
	public $total;
	public $flag_sync;
	public $id_jual_sum;
	public $bank_name;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"id_kasir\", \"kassa_id\", \"shift_id\", \"tanggal\", \"tipe\", \"total\", \"flag_sync\", \"id_jual_sum\", \"bank_name\" "
				." FROM \"t_jual_bayar_sum\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['id_kasir'] = $result[1];
			$lists[$count]['kassa_id'] = $result[2];
			$lists[$count]['shift_id'] = $result[3];
			$lists[$count]['tanggal'] = $result[4];			
			$lists[$count]['tipe'] = $result[5];	
			$lists[$count]['total'] = $result[6];
			$lists[$count]['flag_sync'] = json_encode($result[7]);
			$lists[$count]['id_jual_sum'] = $result[8];
			$lists[$count]['bank_name'] = $result[9];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"id_kasir\", \"kassa_id\", \"shift_id\", \"tanggal\", \"tipe\", \"total\", \"flag_sync\", \"id_jual_sum\", \"bank_name\" "
				." FROM \"t_jual_bayar_sum\" "
				." WHERE \"id\" = '".$object->id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['id_kasir'] = $result[1];
			$lists[$count]['kassa_id'] = $result[2];
			$lists[$count]['shift_id'] = $result[3];
			$lists[$count]['tanggal'] = $result[4];			
			$lists[$count]['tipe'] = $result[5];	
			$lists[$count]['total'] = $result[6];
			$lists[$count]['flag_sync'] = json_encode($result[7]);
			$lists[$count]['id_jual_sum'] = $result[8];
			$lists[$count]['bank_name'] = $result[9];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"id_kasir\", \"kassa_id\", \"shift_id\", \"tanggal\", \"tipe\", \"total\", \"flag_sync\", \"id_jual_sum\", \"bank_name\" "
				." FROM \"t_jual_bayar_sum\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['id_kasir'] = $result[1];
			$lists[$count]['kassa_id'] = $result[2];
			$lists[$count]['shift_id'] = $result[3];
			$lists[$count]['tanggal'] = $result[4];			
			$lists[$count]['tipe'] = $result[5];	
			$lists[$count]['total'] = $result[6];
			$lists[$count]['flag_sync'] = json_encode($result[7]);
			$lists[$count]['id_jual_sum'] = $result[8];
			$lists[$count]['bank_name'] = $result[9];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"id\") "
				." FROM \"t_jual_bayar_sum\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->total == '') { $object->total = '0'; }
		if($object->flag_sync == '') { $object->flag_sync = 'f'; }
		
		$sql = "INSERT INTO \"t_jual_bayar_sum\" (\"id_kasir\", \"kassa_id\", \"shift_id\", \"tanggal\", \"tipe\", \"total\", \"flag_sync\", \"id_jual_sum\", \"bank_name\") "
				." VALUES ('".$object->id_kasir."','".$object->kassa_id."','".$object->shift_id."','".$object->tanggal."','".$object->tipe."','"
				.$object->total."','".$object->flag_sync."',".$object->id_jual_sum.",'".$object->bank_name."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_jual_bayar\" SET ";
		$flag = false;
		if($object->tipe != '') { if($flag){ $sql .= ","; } $sql .= " \"tipe\" = '".$object->tipe."' "; $flag = true; }
		if($object->bank_id != '') { if($flag){ $sql .= ","; } $sql .= " \"bank_id\" = '".$object->bank_id."' "; $flag = true; }
		if($object->bank_name != '') { if($flag){ $sql .= ","; } $sql .= " \"bank_name\" = '".$object->bank_name."' "; $flag = true; }
		if($object->total != '') { if($flag){ $sql .= ","; } $sql .= " \"total\" = '".$object->total."' "; $flag = true; }
		if($object->no_kartu != '') { if($flag){ $sql .= ","; } $sql .= " \"no_kartu\" = '".$object->no_kartu."' "; $flag = true; }	
		$sql .= " WHERE \"id_bayar_item\" = '".$object->id_bayar_item."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goUpdateJual($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_jual_bayar_sum\" SET ";
		$flag = false;
		if($object->tipe != '') { if($flag){ $sql .= ","; } $sql .= " \"tipe\" = '".$object->tipe."' "; $flag = true; }				
		if($object->total != '') { if($flag){ $sql .= ","; } $sql .= " \"total\" = '".$object->total."' "; $flag = true; }
		if($object->flag_sync != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = '".$object->flag_sync."' "; $flag = true; }	
		$sql .= " WHERE \"id_jual_sum\" = '".$object->id_jual_sum."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_jual_bayar\" "
				." WHERE \"id_bayar_item\" = '".$object->id_bayar_item."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goDeleteJual($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_jual_bayar\" "
				." WHERE \"id_jual\" = '".$object->id_jual."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goUpdate_flagSync($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_jual_bayar_sum\" SET ";
		
		$flag = false;
		if($object->tanggal != '') { if($flag){ $sql .= ","; } $sql .= " \"tanggal\" = '".$object->tanggal."' "; $flag = true; }
		if($object->flag_sync != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = '".$object->flag_sync."' "; $flag = true; }
		$sql .= " WHERE \"tanggal\" = '".$object->tanggal."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
}
