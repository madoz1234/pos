<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class ProductMove extends Model
{

	public $trans_id;
	public $trans_no;
	public $stock_id;
	public $type;	
	public $loc_code;
	public $tran_date;
	public $person_id;		
	public $price;	
	public $reference;
	public $qty;
	public $discount_percent;
	public $standard_cost;
	public $visible;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"trans_id\", \"trans_no\", \"stock_id\", \"type\", \"loc_code\", \"tran_date\", \"person_id\" "
				." , \"price\", \"reference\", \"qty\", \"discount_percent\", \"standard_cost\", \"visible\" "
				." FROM \"m_product_move\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['trans_id'] = $result[0];
			$lists[$count]['trans_no'] = $result[1];
			$lists[$count]['stock_id'] = $result[2];
			$lists[$count]['type'] = $result[3];
			$lists[$count]['loc_code'] = $result[4];			
			$lists[$count]['tran_date'] = $result[5];	
			$lists[$count]['person_id'] = $result[6];	
			$lists[$count]['price'] = $result[7];
			$lists[$count]['reference'] = $result[8];
			$lists[$count]['qty'] = $result[9];
			$lists[$count]['discount_percent'] = $result[10];
			$lists[$count]['standard_cost'] = $result[11];
			$lists[$count]['visible'] = $result[12];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"trans_id\", \"trans_no\", \"stock_id\", \"type\", \"loc_code\", \"tran_date\", \"person_id\" "
				." , \"price\", \"reference\", \"qty\", \"discount_percent\", \"standard_cost\", \"visible\" "
				." FROM \"m_product_move\" "
				." WHERE \"currency\" = '".$object->trans_id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['trans_id'] = $result[0];
			$lists[$count]['trans_no'] = $result[1];
			$lists[$count]['stock_id'] = $result[2];
			$lists[$count]['type'] = $result[3];
			$lists[$count]['loc_code'] = $result[4];			
			$lists[$count]['tran_date'] = $result[5];	
			$lists[$count]['person_id'] = $result[6];	
			$lists[$count]['price'] = $result[7];
			$lists[$count]['reference'] = $result[8];
			$lists[$count]['qty'] = $result[9];
			$lists[$count]['discount_percent'] = $result[10];
			$lists[$count]['standard_cost'] = $result[11];
			$lists[$count]['visible'] = $result[12];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"trans_id\", \"trans_no\", \"stock_id\", \"type\", \"loc_code\", \"tran_date\", \"person_id\" "
				." , \"price\", \"reference\", \"qty\", \"discount_percent\", \"standard_cost\", \"visible\" "
				." FROM \"m_product_move\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['trans_id'] = $result[0];
			$lists[$count]['trans_no'] = $result[1];
			$lists[$count]['stock_id'] = $result[2];
			$lists[$count]['type'] = $result[3];
			$lists[$count]['loc_code'] = $result[4];			
			$lists[$count]['tran_date'] = $result[5];	
			$lists[$count]['person_id'] = $result[6];	
			$lists[$count]['price'] = $result[7];
			$lists[$count]['reference'] = $result[8];
			$lists[$count]['qty'] = $result[9];
			$lists[$count]['discount_percent'] = $result[10];
			$lists[$count]['standard_cost'] = $result[11];
			$lists[$count]['visible'] = $result[12];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"trans_id\") "
				." FROM \"m_product_move\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function getStock($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT SUM(\"qty\") "
				." FROM \"m_product_move\" WHERE \"stock_id\" = '".$object->stock_id."' ";
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function getAllStock(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"stock_id\", SUM(\"qty\") "
				." FROM \"m_product_move\" GROUP BY \"stock_id\" ";
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['stock_qty'] = $result[1];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->tran_date == '') { $object->tran_date = date("Y-m-d"); }
		if($object->price == '') { $object->price = '0'; }
		if($object->qty == '') { $object->qty = '0'; }
		if($object->discount_percent == '') { $object->discount_percent = '0'; }
		if($object->standard_cost == '') { $object->standard_cost = '0'; }
		if($object->person_id == '') { $object->person_id = '0'; }
		if($object->visible == '') { $object->visible = '0'; }
		
		$sql = "INSERT INTO \"m_product_move\" (\"trans_no\", \"stock_id\", \"type\", \"loc_code\", \"tran_date\", \"person_id\" "
				." , \"price\", \"reference\", \"qty\", \"discount_percent\", \"standard_cost\", \"visible\") "
				." VALUES ('".$object->trans_no."','".$object->stock_id."','".$object->type."','".$object->loc_code."','".$object->tran_date."','"
				.$object->person_id."','".$object->price."','".$object->reference."','".$object->qty."','".$object->discount_percent."','"
				.$object->standard_cost."','".$object->visible."') ";
		// dd($sql);
		$success = $connection->execute($sql);
		//$id = $connection->lastInsertId();
		
		return $success;
	}

	public function goInsert2($object){
		if($object->tran_date == '') { $object->tran_date = date("Y-m-d"); }
		if($object->price == '') { $object->price = '0'; }
		if($object->qty == '') { $object->qty = '0'; }
		if($object->discount_percent == '') { $object->discount_percent = '0'; }
		if($object->standard_cost == '') { $object->standard_cost = '0'; }
		if($object->person_id == '') { $object->person_id = '0'; }
		if($object->visible == '') { $object->visible = '0'; }
		
		$sql = "INSERT INTO \"m_product_move\" (\"trans_no\", \"stock_id\", \"type\", \"loc_code\", \"tran_date\", \"person_id\" "
				." , \"price\", \"reference\", \"qty\", \"discount_percent\", \"standard_cost\", \"visible\") "
				." VALUES ('".$object->trans_no."','".$object->stock_id."','".$object->type."','".$object->loc_code."','".$object->tran_date."','"
				.$object->person_id."','".$object->price."','".$object->reference."','".$object->qty."','".$object->discount_percent."','"
				.$object->standard_cost."','".$object->visible."'); ";
		return $sql;
	}
	
	public function goMultiInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "INSERT INTO \"m_product_move\" (\"trans_no\", \"stock_id\", \"type\", \"loc_code\", \"tran_date\", \"person_id\" "
				." , \"price\", \"reference\", \"qty\", \"discount_percent\", \"standard_cost\", \"visible\") "
				." VALUES ".$object;
		
		$success = $connection->execute($sql);
		//$id = $connection->lastInsertId();		
		
		return $success;		
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_product_move\" SET ";
		$flag = false;
		if($object->trans_no != '') { if($flag){ $sql .= ","; } $sql .= " \"trans_no\" = '".$object->trans_no."' "; $flag = true; }
		if($object->stock_id != '') { if($flag){ $sql .= ","; } $sql .= " \"stock_id\" = '".$object->stock_id."' "; $flag = true; }
		if($object->type != '') { if($flag){ $sql .= ","; } $sql .= " \"type\" = '".$object->type."' "; $flag = true; }
		if($object->loc_code != '') { if($flag){ $sql .= ","; } $sql .= " \"loc_code\" = '".$object->loc_code."' "; $flag = true; }
		if($object->tran_date != '') { if($flag){ $sql .= ","; } $sql .= " \"tran_date\" = '".$object->tran_date."' "; $flag = true; }
		if($object->person_id != '') { if($flag){ $sql .= ","; } $sql .= " \"person_id\" = '".$object->person_id."' "; $flag = true; }
		if($object->price != '') { if($flag){ $sql .= ","; } $sql .= " \"price\" = '".$object->price."' "; $flag = true; }
		if($object->reference != '') { if($flag){ $sql .= ","; } $sql .= " \"reference\" = '".$object->reference."' "; $flag = true; }
		if($object->qty != '') { if($flag){ $sql .= ","; } $sql .= " \"qty\" = '".$object->qty."' "; $flag = true; }
		if($object->discount_percent != '') { if($flag){ $sql .= ","; } $sql .= " \"discount_percent\" = '".$object->discount_percent."' "; $flag = true; }
		if($object->standard_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"standard_cost\" = '".$object->standard_cost."' "; $flag = true; }
		if($object->visible != '') { if($flag){ $sql .= ","; } $sql .= " \"visible\" = '".$object->visible."' "; $flag = true; }
		$sql .= " WHERE \"trans_id\" = '".$object->trans_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}

	public static function insertSql($object)
	{
		if($object->tran_date == '') { $object->tran_date = date("Y-m-d"); }
		if($object->price == '') { $object->price = '0'; }
		if($object->qty == '') { $object->qty = '0'; }
		if($object->discount_percent == '') { $object->discount_percent = '0'; }
		if($object->standard_cost == '') { $object->standard_cost = '0'; }
		if($object->person_id == '') { $object->person_id = '0'; }
		if($object->visible == '') { $object->visible = '0'; }
		
		$sql = "INSERT INTO \"m_product_move\" (\"trans_no\", \"stock_id\", \"type\", \"loc_code\", \"tran_date\", \"person_id\" "
				." , \"price\", \"reference\", \"qty\", \"discount_percent\", \"standard_cost\", \"visible\") "
				." VALUES ('".$object->trans_no."','".$object->stock_id."','".$object->type."','".$object->loc_code."','".$object->tran_date."','"
				.$object->person_id."','".$object->price."','".$object->reference."','".$object->qty."','".$object->discount_percent."','"
				.$object->standard_cost."','".$object->visible."'); ";
		return $sql;
	}

	public static function updateSql($object)
	{
		$sql = " UPDATE \"m_product_move\" SET ";
		$flag = false;
		if($object->trans_no != '') { if($flag){ $sql .= ","; } $sql .= " \"trans_no\" = '".$object->trans_no."' "; $flag = true; }
		if($object->stock_id != '') { if($flag){ $sql .= ","; } $sql .= " \"stock_id\" = '".$object->stock_id."' "; $flag = true; }
		if($object->type != '') { if($flag){ $sql .= ","; } $sql .= " \"type\" = '".$object->type."' "; $flag = true; }
		if($object->loc_code != '') { if($flag){ $sql .= ","; } $sql .= " \"loc_code\" = '".$object->loc_code."' "; $flag = true; }
		if($object->tran_date != '') { if($flag){ $sql .= ","; } $sql .= " \"tran_date\" = '".$object->tran_date."' "; $flag = true; }
		if($object->person_id != '') { if($flag){ $sql .= ","; } $sql .= " \"person_id\" = '".$object->person_id."' "; $flag = true; }
		if($object->price != '') { if($flag){ $sql .= ","; } $sql .= " \"price\" = '".$object->price."' "; $flag = true; }
		if($object->reference != '') { if($flag){ $sql .= ","; } $sql .= " \"reference\" = '".$object->reference."' "; $flag = true; }
		if($object->qty != '') { if($flag){ $sql .= ","; } $sql .= " \"qty\" = '".$object->qty."' "; $flag = true; }
		if($object->discount_percent != '') { if($flag){ $sql .= ","; } $sql .= " \"discount_percent\" = '".$object->discount_percent."' "; $flag = true; }
		if($object->standard_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"standard_cost\" = '".$object->standard_cost."' "; $flag = true; }
		if($object->visible != '') { if($flag){ $sql .= ","; } $sql .= " \"visible\" = '".$object->visible."' "; $flag = true; }
		$sql .= " WHERE \"trans_id\" = '".$object->trans_id."'; ";		

		return $sql;
	}
	
	public function goFreeSql($sql){
		$connection = new Postgresql($this->di['db']);
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goVoidRef($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_product_move\" SET ";		
		$sql .= " \"price\" = '0', ";
		$sql .= " \"qty\" = '0' ";
		$sql .= " WHERE \"reference\" = '".$object->reference."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_product_move\" "
				." WHERE \"trans_id\" = '".$object->trans_id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function getJoin_AD($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT mpm.\"trans_id\", mpm.\"trans_no\", mpm.\"stock_id\", mpm.\"type\", mpm.\"loc_code\", mpm.\"tran_date\", mpm.\"person_id\" "
				." , mpm.\"price\", mpm.\"reference\", mpm.\"qty\", mpm.\"discount_percent\", mpm.\"standard_cost\", mpm.\"visible\", ad.\"adj_detail\" "
				." FROM \"m_product_move\" mpm LEFT JOIN \"t_adj\" ad ON mpm.reference = ad.adj_id ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
				$lists[$count]['trans_id'] = $result[0];
				$lists[$count]['trans_no'] = $result[1];
				$lists[$count]['stock_id'] = $result[2];
				$lists[$count]['type'] = $result[3];
				$lists[$count]['loc_code'] = $result[4];			
				$lists[$count]['tran_date'] = $result[5];	
				$lists[$count]['person_id'] = $result[6];	
				$lists[$count]['price'] = $result[7];
				$lists[$count]['reference'] = $result[8];
				$lists[$count]['qty'] = $result[9];
				$lists[$count]['discount_percent'] = $result[10];
				$lists[$count]['standard_cost'] = $result[11];
				$lists[$count]['visible'] = $result[12];
				$lists[$count]['adj_detail'] = $result[13];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function Join_JualDetail($from, $to){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "		
			select a.stock_id, description, standard_cost, price, abs(a.qty), tran_date, barcode
			from m_product_move a
			inner join t_jual_detail b on a.reference = b.id_jual and a.stock_id = b.item_code
			where tran_date between '".$from."' and '".$to."' and type = 'CT'
			order by tran_date
		";
	
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['item_code'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$lists[$count]['uom1_harga'] = $result[2];
			$lists[$count]['harga'] = $result[3];
			$lists[$count]['qty'] = $result[4];
			$lists[$count]['tanggal'] = $result[5];
			$lists[$count]['barcode'] = $result[6];

			$count++;
		}
		
		return $lists;
	}

	public function Join_JualDetail2($from, $to){
		$connection = new Postgresql($this->di['db']);

		$sql = "		
			select a.stock_id, d.uom1_prod_nm, a.price, abs(a.qty), a.reference, b.barcode, a.tran_date
			from m_product_move a
			inner join t_jual_detail b on a.reference = b.id_jual AND a.stock_id = b.item_code
			inner join m_product d on a.stock_id = d.stock_id
			where a.tran_date between '".$from." 00:00:00' AND '".$to." 23:59:59' AND a.type = 'CT'
			order by a.tran_date
		";
	
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['item_code'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$lists[$count]['harga'] = $result[2];
			$lists[$count]['qty'] = $result[3];
			$lists[$count]['reference'] = $result[4];
			$lists[$count]['barcode'] = $result[5];
			$lists[$count]['tanggal'] = $result[6];

			$count++;
		}
		
		return $lists;
	}

	public function Join_JualDetail4($from, $to){
		$connection = new Postgresql($this->di['db']);

		$sql = "		
			select a.stock_id, b.description, a.price, abs(a.qty), a.reference, b.barcode, a.tran_date
			from m_product_move a
			inner join t_jual_detail b on a.reference = b.id_jual AND a.stock_id = b.item_code
			inner join m_product d on a.stock_id = d.stock_id
			where a.tran_date between '".$from." 00:00:00' AND '".$to." 23:59:59' AND a.type = 'CT' AND d.ppob ='1'
			order by a.tran_date
		";
	
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['item_code'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$lists[$count]['harga'] = $result[2];
			$lists[$count]['qty'] = $result[3];
			$lists[$count]['reference'] = $result[4];
			$lists[$count]['barcode'] = $result[5];
			$lists[$count]['tanggal'] = $result[6];

			$count++;
		}
		
		return $lists;
	}

	public function executeQuery($sql){
		$connection = new Postgresql($this->di['db']);
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
