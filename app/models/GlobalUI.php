<?php

use Phalcon\Mvc\Model;

class GlobalUI extends Model
{
	public function ui_open_panel($col){
		echo '<div class="col-lg-'.$col.' col-md-'.$col.'"><div class="panel panel-default">';
	}
	
	public function ui_open_panel_body(){
		echo '<div class="panel-body">';
	}
	
	public function ui_close_panel(){
		echo '</div></div>';
	}
	
	public function ui_close_panel_body(){
		echo '</div>';
	}
	
	/**********************************************************************************************************
	*	textField
	**********************************************************************************************************/
	
	public function ui_text($name, $caption, $mode, $value, $visible, $readonly, $mandatory){		
		if(strtoupper($mode) == 'DEFAULT'){
			if($visible){
				echo '<div class="col-lg-4 col-md-5">';
					echo '<label class="label_form-xs">';
						echo $caption;
					echo '</label>';
					echo '</div>';
				
				echo '<div class="col-lg-8 col-md-7">';
					if($readonly){	echo $this->tag->textField([ $name, "class"=>"form-control input-xs", "value"=>$value, "readonly"=>"true" ]);	}
					else{	echo $this->tag->textField([ $name, "class"=>"form-control input-xs", "value"=>$value ]);	}
				echo '</div>';
			}else{
				echo $this->tag->hiddenField([ $name, "class"=>"form-control input-xs", "value"=>$value ]);
			}
		}
	}
	
	/**********************************************************************************************************
	*	textArea
	**********************************************************************************************************/
	
	public function ui_text_area($name, $caption, $mode, $value, $visible, $readonly, $mandatory){	
		if(strtoupper($mode) == 'DEFAULT'){			
			if($visible){
				echo '<div class="col-lg-4 col-md-5">';
					echo '<label class="label_form-xs">';
						echo $caption;
					echo '</label>';			
				echo '</div>';
				
				echo '<div class="col-lg-8 col-md-7 form-group">';								
					if($readonly){ echo $this->tag->textArea([ $name, "class"=>"form-control input-xs", "value"=>$value, "readonly"=>"true" ]); }
					else{ echo $this->tag->textArea([ $name, "class"=>"form-control input-xs", "value"=>$value ]); }
				echo '</div>';
			}else{
				echo $this->tag->hiddenField([ $name, "class"=>"form-control input-xs", "value"=>$value ]);
			}
		}
	}
	
	/**********************************************************************************************************
	*	numericField
	**********************************************************************************************************/
	
	public function ui_numeric($name, $caption, $mode, $value, $visible, $readonly, $mandatory){
		if(strtoupper($mode) == 'DEFAULT'){
			if($visible){	
				echo '<div class="col-lg-4 col-md-5">';
					echo '<label class="label_form-xs">';
						echo $caption;
					echo '</label>';				
				echo '</div>';
			
				echo '<div class="col-lg-8 col-md-7">';				
					if($readonly){ echo $this->tag->numericField([ $name, "class"=>"form-control input-xs", "value"=>$value, "readonly"=>"true" ]); }
					else{ echo $this->tag->numericField([ $name, "class"=>"form-control input-xs", "value"=>$value ]); }
				echo '</div>';
			}else{
				echo $this->tag->hiddenField([ $name, "class"=>"form-control input-xs", "value"=>$value ]);
			}			
		}
	}
	
	public function ui_numeric_rate($name, $caption, $mode, $value, $visible, $readonly, $mandatory){
		if(strtoupper($mode) == 'DEFAULT'){
			if($visible){	
				echo '<div class="col-lg-4 col-md-5">';
					echo '<label class="label_form-xs">';
						echo $caption;
					echo '</label>';			
				echo '</div>';
			
				echo '<div class="col-lg-8 col-md-7">';				
					if($readonly){ echo $this->tag->numericField([ $name, "class"=>"form-control input-xs", "step"=>"0.01", "value"=>$value, "readonly"=>"true" ]); }
					else{ echo $this->tag->numericField([ $name, "class"=>"form-control input-xs", "step"=>"0.01", "value"=>$value ]); }
				echo '</div>';
			}else{
				echo $this->tag->hiddenField([ $name, "class"=>"form-control input-xs", "value"=>$value ]);
			}
		}
	}

	/**********************************************************************************************************
	*	dateField
	**********************************************************************************************************/
	
	public function ui_date($name, $caption, $mode, $value, $visible, $readonly, $mandatory){
		if(strtoupper($mode) == 'DEFAULT'){
			if($visible){
				echo '<div class="col-lg-4 col-md-5">';
					echo '<label class="label_form-xs">';
						echo $caption;
					echo '</label>';				
				echo '</div>';
				
				echo '<div class="col-lg-8 col-md-7">';				
					if($readonly){	echo $this->tag->textField([ $name, "class"=>"form-control input-xs", "value"=>$value, "readonly"=>"true" ]);	}
					else{	echo $this->tag->dateField([ $name, "class"=>"form-control input-xs", "value"=>$value ]);	}
				echo '</div>';
			}else{
				echo $this->tag->hiddenField([ $name, "class"=>"form-control input-xs", "value"=>$value ]);
			}			
		}
	}	
	
	/**********************************************************************************************************
	*	checkBox
	**********************************************************************************************************/
	
	public function ui_checkbox($name, $caption, $mode, $value, $visible, $readonly, $mandatory){	
		if(strtoupper($mode) == 'DEFAULT'){			
			if($visible){
				echo '<div class="col-lg-4 col-md-5">';
					echo '<label class="label_form-xs">';
						echo $caption;
					echo '</label>';				
				echo '</div>';
				
				echo '<div class="col-lg-8 col-md-7" style="text-align:left;">';								
				if($readonly){
					if($value){ echo $this->tag->checkField([ $name, "class"=>"form-check", "value"=>"Y", "checked"=>"checked", "disabled"=>"disabled", "style"=>"margin-top:5px" ]); }
					else{ echo $this->tag->checkField([ $name, "class"=>"form-check", "value"=>"Y", "disabled"=>"disabled", "style"=>"margin-top:5px" ]); }
				}else{
					if($value){ echo $this->tag->checkField([ $name, "class"=>"form-check", "value"=>"Y", "checked"=>"checked", "style"=>"margin-top:5px" ]); }
					else{ echo $this->tag->checkField([ $name, "class"=>"form-check", "value"=>"Y", "style"=>"margin-top:5px" ]); }
				}
				echo '</div>';
			}else{
				echo $this->tag->hiddenField([ $name, "class"=>"form-check", "value"=>$value ]);
			}			
		}else if(strtoupper($mode) == 'LIST'){				
			if($value){ echo $this->tag->checkField([ $name, "value"=>"Y", "checked"=>"checked", "disabled"=>"disabled", "style"=>"margin-top:1px" ]); }
			else{ echo $this->tag->checkField([ $name, "value"=>"Y", "disabled"=>"disabled", "style"=>"margin-top:1px" ]); }			
		}else if(strtoupper($mode) == 'EDITED'){				
			if($value){ echo $this->tag->checkField([ $name, "value"=>"Y", "checked"=>"checked", "style"=>"margin-top:1px" ]); }
			else{ echo $this->tag->checkField([ $name, "value"=>"Y", "style"=>"margin-top:1px" ]); }			
		}
	}
	
	/**********************************************************************************************************
	*	emailField
	**********************************************************************************************************/
	
	public function ui_email($name, $caption, $mode, $value, $visible, $readonly, $mandatory){
		if(strtoupper($mode) == 'DEFAULT'){
			if($visible){
				echo '<div class="col-lg-4 col-md-5">';
					echo '<label class="label_form-xs">';
						echo $caption;
					echo '</label>';				
				echo '</div>';
				
				echo '<div class="col-lg-8 col-md-7">';				
				if($readonly){
					echo $this->tag->emailField([ $name, "class"=>"form-control input-xs", "value"=>$value, "readonly"=>"true" ]);
				}else{
					echo $this->tag->emailField([ $name, "class"=>"form-control input-xs", "value"=>$value ]);
				}
				echo '</div>';
			}else{
				echo $this->tag->hiddenField([ $name, "class"=>"form-control input-xs", "value"=>$value ]);
			}			
		}
	}
	
	/**********************************************************************************************************
	*	select
	**********************************************************************************************************/
	
	public function ui_select($name, $caption, $mode, $value, $visible, $readonly, $mandatory){
		$list_function = new ListFunction();
		$data = null;
		
		//if(empty($data)){ $data[''] = 'Pilih...'; }
		
		if(strtoupper($caption) == 'RACK')	{ $data = $list_function::selectRack(); }
		if(strtoupper($caption) == 'CUSTOMER')	{ $data = $list_function::selectCust(); }
		if(strtoupper($caption) == 'TMUK')	{ $data = $list_function::selectTMUK(); }
		if($name == 'Debtor_No')  { $data = $list_function::selectTMUK(); }
		if($name == 'Branch_Ref')  { $data = $list_function::selectJenisKustomer(); }
		
		if(strtoupper($mode) == 'DEFAULT'){
			if($visible){
				echo '<div class="col-lg-4 col-md-5">';
					echo '<label class="label_form-xs">';
						echo $caption;
					echo '</label>';
				echo '</div>';
				
				echo '<div class="col-lg-8 col-md-7">';				
					if($readonly){ echo $this->tag->textField([ $name, "class"=>"form-control input-xs", "value"=>$value, "readonly"=>"true" ]); }
					else{ 
						$args=[ $name, $data, "class"=>"form-control input-xs", "value"=>[$value, $value] ];												
						if(empty($value)){
							$args['useEmpty']=true;
						}
						echo $this->tag->select($args); 
					}
				echo '</div>';
			}else{
				echo $this->tag->hiddenField([ $name, "class"=>"form-control input-xs", "value"=>$value ]);
			}
		}
		elseif(strtoupper($mode)=='DISABLED'){
			echo '<div class="col-lg-4 col-md-5">';
				echo '<label class="label_form-xs">';
					echo $caption;
				echo '</label>';
			echo '</div>';
			$args=[ $name, $data, "class"=>"form-control input-xs", "value"=>[$value, $value] ,"disabled"=>"disabled"];
			if(empty($value)){
				$args['useEmpty']=true;
			}
			echo '<div class="col-lg-8 col-md-7">';
				echo $this->tag->select($args);
			echo '</div>';
		}		
	}
	
	/**********************************************************************************************************
	*	fileField
	**********************************************************************************************************/
	
	public function ui_file($name, $caption, $mode, $value, $visible, $readonly, $mandatory){		
		if(strtoupper($mode) == 'DEFAULT'){
			if($visible){
				echo '<div class="col-lg-4 col-md-5">';
					echo '<label class="label_form-xs">';
						echo $caption;
					echo '</label>';
					echo '</div>';
				
				echo '<div class="col-lg-8 col-md-7">';
					if($readonly){	echo $this->tag->textField([ $name, "class"=>"form-control input-xs", "value"=>$value, "readonly"=>"true" ]);	}
					else{	echo $this->tag->fileField([ $name, "class"=>"input-file" ]);	}
				echo '</div>';
			}else{
				echo $this->tag->hiddenField([ $name, "class"=>"form-control input-xs", "value"=>$value ]);
			}
		}
	}	
	
}
