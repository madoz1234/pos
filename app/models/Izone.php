<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Izone extends Model
{

	public $activation_id;
	public $terminal_id;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"activation_id\", \"terminal_id\", \"pin\", \"first_name\" FROM \"m_izone\" ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['activation_id'] = $result[0];
			$lists[$count]['terminal_id'] = $result[1];
			$lists[$count]['pin'] = $result[2];
			$lists[$count]['first_name'] = $result[3];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(*) "
				." FROM \"m_izone\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);	
		
		$sql = "INSERT INTO \"m_izone\" (\"activation_id\", \"terminal_id\") "
				." VALUES ('".$object->activation_id."','".$object->terminal_id."') ";
		
		$success = $connection->execute($sql);		
		
		return $sql;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_izone\" SET ";
		$flag = false;		
		if($object->activation_id != '') { if($flag){ $sql .= ","; } $sql .= " \"activation_id\" = '".$object->activation_id."' "; $flag = true; }
		if($object->terminal_id != '') { if($flag){ $sql .= ","; } $sql .= " \"terminal_id\" = '".$object->terminal_id."' "; $flag = true; }
		if($object->pin != '') { if($flag){ $sql .= ","; } $sql .= " \"pin\" = '".$object->pin."' "; $flag = true; }
		if($object->first_name != '') { if($flag){ $sql .= ","; } $sql .= " \"first_name\" = '".$object->first_name."' "; $flag = true; }
		$sql .= " WHERE \"activation_id\" = '".$object->activation_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_izone\" "
				." ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
