<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class SO extends Model
{

	public $so_id;
	public $kode_verifikasi;
	public $user_id;
	public $so_date;	
	public $status;
	public $so_type;
	public $flag_sync;
		
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"so_id\", \"kode_verifikasi\", \"user_id\", \"so_date\", \"status\", \"so_type\", \"flag_sync\" "
				." FROM \"t_so\" "
				." WHERE \"status\" <> 'DELETE' ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['so_id'] = $result[0];
			$lists[$count]['kode_verifikasi'] = $result[1];
			$lists[$count]['user_id'] = $result[2];
			$lists[$count]['so_date'] = $result[3];
			$lists[$count]['status'] = $result[4];
			$lists[$count]['so_type'] = $result[5];
			$lists[$count]['flag_sync'] = $result[6];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"so_id\", \"kode_verifikasi\", \"user_id\", \"so_date\", \"status\", \"so_type\", \"flag_sync\" "
				." FROM \"t_so\" "
				." WHERE \"so_id\" = '".$object->so_id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['so_id'] = $result[0];
			$lists[$count]['kode_verifikasi'] = $result[1];
			$lists[$count]['user_id'] = $result[2];
			$lists[$count]['so_date'] = $result[3];
			$lists[$count]['status'] = $result[4];	
			$lists[$count]['so_type'] = $result[5];
			$lists[$count]['flag_sync'] = $result[6];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"so_id\", \"kode_verifikasi\", \"user_id\", \"so_date\", \"status\", \"so_type\", \"flag_sync\" "
				." FROM \"t_so\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['so_id'] = $result[0];
			$lists[$count]['kode_verifikasi'] = $result[1];
			$lists[$count]['user_id'] = $result[2];
			$lists[$count]['so_date'] = $result[3];
			$lists[$count]['status'] = $result[4];	
			$lists[$count]['so_type'] = $result[5];
			$lists[$count]['flag_sync'] = $result[6];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"so_id\") "
				." FROM \"t_so\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->so_date == '') { $object->so_date = date("Y-m-d"); }
		if($object->flag_sync == '') { $object->flag_sync = 'f'; }
		
		$sql = "INSERT INTO \"t_so\" (\"so_id\", \"kode_verifikasi\", \"user_id\", \"so_date\", \"status\", \"so_type\", \"flag_sync\") "
				." VALUES ('".$object->so_id."','".$object->kode_verifikasi."','".$object->user_id."','".$object->so_date."','".$object->status."','".$object->so_type
				."','".$object->flag_sync."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_so\" SET ";
		$flag = false;
		if($object->kode_verifikasi != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_verifikasi\" = '".$object->kode_verifikasi."' "; $flag = true; }
		if($object->user_id != '') { if($flag){ $sql .= ","; } $sql .= " \"user_id\" = '".$object->user_id."' "; $flag = true; }
		if($object->so_date != '') { if($flag){ $sql .= ","; } $sql .= " \"so_date\" = '".$object->so_date."' "; $flag = true; }
		if($object->status != '') { if($flag){ $sql .= ","; } $sql .= " \"status\" = '".$object->status."' "; $flag = true; }
		if($object->so_type != '') { if($flag){ $sql .= ","; } $sql .= " \"so_type\" = '".$object->so_type."' "; $flag = true; }
		if($object->flag_sync != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = '".$object->flag_sync."' "; $flag = true; }
		$sql .= " WHERE \"so_id\" = '".$object->so_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_so\" "
				." WHERE \"so_id\" = '".$object->so_id."' ";
		
		$success = $connection->execute($sql);
		
		return $success;
	}
}
