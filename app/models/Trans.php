<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Trans extends Model
{
	
	public function begin(){
		$connection = new Postgresql($this->di['db']);	
		$sql = " BEGIN ";
		
		$connection->query($sql);
	}
	
	public function commit(){
		$connection = new Postgresql($this->di['db']);	
		$sql = " COMMIT ";
		
		$connection->query($sql);
	}
	
	public function rollback(){
		$connection = new Postgresql($this->di['db']);	
		$sql = " ROLLBACK ";
		
		$connection->query($sql);
	}
}
