<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class EOD extends Model
{
	public $tmuk;
	public $tanggal;	
	public $initial;	
	public $cash_income;	
	public $net_sales;	
	public $donasi;	
	public $kredit;	
	public $debit;	
	public $voucher;	
	public $piutang;	
	public $total_actual;	
	public $variance;	
	public $rec_count;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"tmuk\", \"tanggal\", \"initial\", \"cash_income\", \"net_sales\", \"donasi\", \"kredit\", \"debit\", \"voucher\", \"total_actual\", \"variance\", \"rec_count\", \"piutang\" FROM \"t_eod\" ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['tmuk'] = $result[0];
			$lists[$count]['tanggal'] = $result[1];	
			$lists[$count]['initial'] = $result[2];	
			$lists[$count]['cash_income'] = $result[3];	
			$lists[$count]['net_sales'] = $result[4];	
			$lists[$count]['donasi'] = $result[5];	
			$lists[$count]['kredit'] = $result[6];	
			$lists[$count]['debit'] = $result[7];	
			$lists[$count]['voucher'] = $result[8];	
			$lists[$count]['total_actual'] = $result[9];
			$lists[$count]['variance'] = $result[10];	
			$lists[$count]['rec_count'] = $result[11];	
			$lists[$count]['piutang'] = $result[12];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"tmuk\", \"tanggal\", \"initial\", \"cash_income\", \"net_sales\", \"donasi\", \"kredit\", \"debit\", \"voucher\", \"total_actual\", \"variance\", \"rec_count\", \"piutang\" FROM \"t_eod\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['tmuk'] = $result[0];
			$lists[$count]['tanggal'] = $result[1];	
			$lists[$count]['initial'] = $result[2];	
			$lists[$count]['cash_income'] = $result[3];	
			$lists[$count]['net_sales'] = $result[4];	
			$lists[$count]['donasi'] = $result[5];	
			$lists[$count]['kredit'] = $result[6];	
			$lists[$count]['debit'] = $result[7];	
			$lists[$count]['voucher'] = $result[8];	
			$lists[$count]['total_actual'] = $result[9];
			$lists[$count]['variance'] = $result[10];	
			$lists[$count]['rec_count'] = $result[11];	
			$lists[$count]['piutang'] = $result[12];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(*) "
				." FROM \"t_eod\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);	
		
		$sql = "INSERT INTO \"t_eod\" (\"tmuk\", \"tanggal\", \"initial\", \"cash_income\", \"net_sales\", \"donasi\", \"kredit\", \"debit\", \"voucher\", \"total_actual\", \"variance\", \"rec_count\", \"piutang\") "
				." VALUES ('".$object->tmuk."','".$object->tanggal."',".$object->initial.",".$object->cash_income.",".$object->net_sales.",".$object->donasi.",".$object->kredit.",".$object->debit.",".$object->voucher.",".$object->total_actual.",".$object->variance.",".$object->rec_count.",".$object->piutang.") ";
		
		$success = $connection->execute($sql);		
		
		return $sql;
	}
}
