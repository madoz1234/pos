<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Supplier extends Model
{

	public $supplier_id;
	public $supp_name;
	public $supp_ref;
	public $address;	
	public $supp_address;
	public $gst_no;
	public $supp_account_no;
	public $website;
	public $bank_account;
	public $curr_code;
	public $payment_terms;
	public $tax_included;
	public $dimension_id;
	public $dimension2_id;
	public $tax_group_id;
	public $credit_limit;
	public $purchase_account;
	public $payable_account;
	public $payment_discount_account;
	public $notes;
	public $inactive;
	public $supp_code;
	public $supp_telp;
	public $pic;
	public $top_days;
	public $order_mon;
	public $order_tue;
	public $order_wed;
	public $order_thu;
	public $order_fri;
	public $order_sat;
	public $order_sun;
	public $lead_time;
	public $npwp;
	public $pkp;
	public $city;
	public $id_som;
	public $zip_code;
	public $kelurahan;
	public $provinsi;
	public $gps_latitude;
	public $gps_longitude;
	public $kecamatan;
	public $company_name;
	public $company_address;
	public $escrow_account;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"supplier_id\", \"supp_name\", \"supp_ref\", \"address\", \"supp_address\", \"gst_no\", \"contact\", \"supp_account_no\", \"website\", "
			." \"bank_account\", \"curr_code\", \"payment_terms\", \"tax_included\", \"dimension_id\", \"dimension2_id\", \"tax_group_id\", \"credit_limit\", "
			." \"purchase_account\", \"payable_account\", \"payment_discount_account\", \"notes\", \"inactive\", \"supp_code\", \"supp_telp\", \"pic\", \"top_days\", "
			." \"order_mon\", \"order_tue\", \"order_wed\", \"order_thu\", \"order_fri\", \"order_sat\", \"order_sun\", \"lead_time\", \"npwp\", \"pkp\", \"city\", "
			." \"id_som\", \"zip_code\", \"kelurahan\", \"provinsi\", \"gps_latitude\", \"gps_longitude\", \"kecamatan\", \"company_name\", \"company_address\", \"escrow_account\" "
			." FROM m_supplier ORDER BY supplier_id";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['supplier_id'] = $result[0];
			$lists[$count]['supp_name'] = $result[1];
			$lists[$count]['supp_ref'] = $result[2];
			$lists[$count]['address'] = $result[3];
			$lists[$count]['supp_address'] = $result[4];
			$lists[$count]['gst_no'] = $result[5];
			$lists[$count]['contact'] = $result[6];
			$lists[$count]['supp_account_no'] = $result[7];
			$lists[$count]['website'] = $result[8];
			$lists[$count]['bank_account'] = $result[9];
			$lists[$count]['curr_code'] = $result[10];
			$lists[$count]['payment_terms'] = $result[11];
			$lists[$count]['tax_included'] = $result[12];
			$lists[$count]['dimension_id'] = $result[13];
			$lists[$count]['dimension2_id'] = $result[14];
			$lists[$count]['tax_group_id'] = $result[15];
			$lists[$count]['credit_limit'] = $result[16];
			$lists[$count]['purchase_account'] = $result[17];
			$lists[$count]['payable_account'] = $result[18];
			$lists[$count]['payment_discount_account'] = $result[19];
			$lists[$count]['notes'] = $result[20];
			$lists[$count]['inacitve'] = $result[21];
			$lists[$count]['supp_code'] = $result[22];
			$lists[$count]['supp_telp'] = $result[23];
			$lists[$count]['pic'] = $result[24];
			$lists[$count]['top_days'] = $result[25];
			$lists[$count]['order_mon'] = $result[26];
			$lists[$count]['order_tue'] = $result[27];
			$lists[$count]['order_wed'] = $result[28];
			$lists[$count]['order_thu'] = $result[29];
			$lists[$count]['order_fri'] = $result[30];
			$lists[$count]['order_sat'] = $result[31];
			$lists[$count]['order_sun'] = $result[32];
			$lists[$count]['lead_time'] = $result[33];
			$lists[$count]['npwp'] = $result[34];
			$lists[$count]['pkp'] = $result[35];
			$lists[$count]['city'] = $result[36];
			$lists[$count]['id_som'] = $result[37];
			$lists[$count]['zip_code'] = $result[38];
			$lists[$count]['kelurahan'] = $result[39];
			$lists[$count]['provinsi'] = $result[40];
			$lists[$count]['gps_latitude'] = $result[41];
			$lists[$count]['gps_longitude'] = $result[42];
			$lists[$count]['kecamatan'] = $result[43];
			$lists[$count]['company_name'] = $result[44];
			$lists[$count]['company_address'] = $result[45];
			$lists[$count]['escrow_account'] = $result[46];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"supplier_id\", \"supp_name\", \"supp_ref\", \"address\", \"supp_address\", \"gst_no\", \"contact\", \"supp_account_no\", \"website\", "
			." \"bank_account\", \"curr_code\", \"payment_terms\", \"tax_included\", \"dimension_id\", \"dimension2_id\", \"tax_group_id\", \"credit_limit\", "
			." \"purchase_account\", \"payable_account\", \"payment_discount_account\", \"notes\", \"inactive\", \"supp_code\", \"supp_telp\", \"pic\", \"top_days\", "
			." \"order_mon\", \"order_tue\", \"order_wed\", \"order_thu\", \"order_fri\", \"order_sat\", \"order_sun\", \"lead_time\", \"npwp\", \"pkp\", \"city\", "
			." \"id_som\", \"zip_code\", \"kelurahan\", \"provinsi\", \"gps_latitude\", \"gps_longitude\", \"kecamatan\", \"company_name\", \"company_address\", \"escrow_account\" "
			." FROM m_supplier "
			." WHERE supplier_id = '".$object->supplier_id."' "
			." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['supplier_id'] = $result[0];
			$lists[$count]['supp_name'] = $result[1];
			$lists[$count]['supp_ref'] = $result[2];
			$lists[$count]['address'] = $result[3];
			$lists[$count]['supp_address'] = $result[4];
			$lists[$count]['gst_no'] = $result[5];
			$lists[$count]['contact'] = $result[6];
			$lists[$count]['supp_account_no'] = $result[7];
			$lists[$count]['website'] = $result[8];
			$lists[$count]['bank_account'] = $result[9];
			$lists[$count]['curr_code'] = $result[10];
			$lists[$count]['payment_terms'] = $result[11];
			$lists[$count]['tax_included'] = $result[12];
			$lists[$count]['dimension_id'] = $result[13];
			$lists[$count]['dimension2_id'] = $result[14];
			$lists[$count]['tax_group_id'] = $result[15];
			$lists[$count]['credit_limit'] = $result[16];
			$lists[$count]['purchase_account'] = $result[17];
			$lists[$count]['payable_account'] = $result[18];
			$lists[$count]['payment_discount_account'] = $result[19];
			$lists[$count]['notes'] = $result[20];
			$lists[$count]['inacitve'] = $result[21];
			$lists[$count]['supp_code'] = $result[22];
			$lists[$count]['supp_telp'] = $result[23];
			$lists[$count]['pic'] = $result[24];
			$lists[$count]['top_days'] = $result[25];
			$lists[$count]['order_mon'] = $result[26];
			$lists[$count]['order_tue'] = $result[27];
			$lists[$count]['order_wed'] = $result[28];
			$lists[$count]['order_thu'] = $result[29];
			$lists[$count]['order_fri'] = $result[30];
			$lists[$count]['order_sat'] = $result[31];
			$lists[$count]['order_sun'] = $result[32];
			$lists[$count]['lead_time'] = $result[33];
			$lists[$count]['npwp'] = $result[34];
			$lists[$count]['pkp'] = $result[35];
			$lists[$count]['city'] = $result[36];
			$lists[$count]['id_som'] = $result[37];
			$lists[$count]['zip_code'] = $result[38];
			$lists[$count]['kelurahan'] = $result[39];
			$lists[$count]['provinsi'] = $result[40];
			$lists[$count]['gps_latitude'] = $result[41];
			$lists[$count]['gps_longitude'] = $result[42];
			$lists[$count]['kecamatan'] = $result[43];
			$lists[$count]['company_name'] = $result[44];
			$lists[$count]['company_address'] = $result[45];
			$lists[$count]['escrow_account'] = $result[46];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"supplier_id\", \"supp_name\", \"supp_ref\", \"address\", \"supp_address\", \"gst_no\", \"contact\", \"supp_account_no\", \"website\", "
			." \"bank_account\", \"curr_code\", \"payment_terms\", \"tax_included\", \"dimension_id\", \"dimension2_id\", \"tax_group_id\", \"credit_limit\", "
			." \"purchase_account\", \"payable_account\", \"payment_discount_account\", \"notes\", \"inactive\", \"supp_code\", \"supp_telp\", \"pic\", \"top_days\", "
			." \"order_mon\", \"order_tue\", \"order_wed\", \"order_thu\", \"order_fri\", \"order_sat\", \"order_sun\", \"lead_time\", \"npwp\", \"pkp\", \"city\", "
			." \"id_som\", \"zip_code\", \"kelurahan\", \"provinsi\", \"gps_latitude\", \"gps_longitude\", \"kecamatan\", \"company_name\", \"company_address\", \"escrow_account\" "
			." FROM m_supplier ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['supplier_id'] = $result[0];
			$lists[$count]['supp_name'] = $result[1];
			$lists[$count]['supp_ref'] = $result[2];
			$lists[$count]['address'] = $result[3];
			$lists[$count]['supp_address'] = $result[4];
			$lists[$count]['gst_no'] = $result[5];
			$lists[$count]['contact'] = $result[6];
			$lists[$count]['supp_account_no'] = $result[7];
			$lists[$count]['website'] = $result[8];
			$lists[$count]['bank_account'] = $result[9];
			$lists[$count]['curr_code'] = $result[10];
			$lists[$count]['payment_terms'] = $result[11];
			$lists[$count]['tax_included'] = $result[12];
			$lists[$count]['dimension_id'] = $result[13];
			$lists[$count]['dimension2_id'] = $result[14];
			$lists[$count]['tax_group_id'] = $result[15];
			$lists[$count]['credit_limit'] = $result[16];
			$lists[$count]['purchase_account'] = $result[17];
			$lists[$count]['payable_account'] = $result[18];
			$lists[$count]['payment_discount_account'] = $result[19];
			$lists[$count]['notes'] = $result[20];
			$lists[$count]['inacitve'] = $result[21];
			$lists[$count]['supp_code'] = $result[22];
			$lists[$count]['supp_telp'] = $result[23];
			$lists[$count]['pic'] = $result[24];
			$lists[$count]['top_days'] = $result[25];
			$lists[$count]['order_mon'] = $result[26];
			$lists[$count]['order_tue'] = $result[27];
			$lists[$count]['order_wed'] = $result[28];
			$lists[$count]['order_thu'] = $result[29];
			$lists[$count]['order_fri'] = $result[30];
			$lists[$count]['order_sat'] = $result[31];
			$lists[$count]['order_sun'] = $result[32];
			$lists[$count]['lead_time'] = $result[33];
			$lists[$count]['npwp'] = $result[34];
			$lists[$count]['pkp'] = $result[35];
			$lists[$count]['city'] = $result[36];
			$lists[$count]['id_som'] = $result[37];
			$lists[$count]['zip_code'] = $result[38];
			$lists[$count]['kelurahan'] = $result[39];
			$lists[$count]['provinsi'] = $result[40];
			$lists[$count]['gps_latitude'] = $result[41];
			$lists[$count]['gps_longitude'] = $result[42];
			$lists[$count]['kecamatan'] = $result[43];
			$lists[$count]['company_name'] = $result[44];
			$lists[$count]['company_address'] = $result[45];
			$lists[$count]['escrow_account'] = $result[46];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"supplier_id\") "
				." FROM \"m_supplier\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		if($object->telepon != '') { if($flag){ $sql .= ","; } $sql .= " \"telepon\" = '".$object->telepon."' "; $flag = 0; }
		if($object->zip_code != '') { if($flag){ $sql .= ","; } $sql .= " \"zip_code\" = '".$object->zip_code."' "; $flag = 0; }
		if($object->kecamatan != '') { if($flag){ $sql .= ","; } $sql .= " \"kecamatan\" = '".$object->kecamatan."' "; $flag = 0; }
		if($object->company_name != '') { if($flag){ $sql .= ","; } $sql .= " \"company_name\" = '".$object->company_name."' "; $flag = 0; }
		if($object->tax_address != '') { if($flag){ $sql .= ","; } $sql .= " \"tax_address\" = '".$object->tax_address."' "; $flag = 0; }
		if($object->npwp != '') { if($flag){ $sql .= ","; } $sql .= " \"npwp\" = '".$object->npwp."' "; $flag = 0; }
		$sql = "INSERT INTO \"m_supplier\" (\"supplier_id\",\"supp_name\", \"supp_ref\", \"address\", \"supp_address\", \"gst_no\", \"contact\", \"supp_account_no\", \"website\", "
			." \"bank_account\", \"curr_code\", \"payment_terms\", \"tax_included\", \"dimension_id\", \"dimension2_id\", \"tax_group_id\", \"credit_limit\", "
			." \"purchase_account\", \"payable_account\", \"payment_discount_account\", \"notes\", \"inactive\", \"supp_code\", \"supp_telp\", \"pic\", \"top_days\", "
			." \"order_mon\", \"order_tue\", \"order_wed\", \"order_thu\", \"order_fri\", \"order_sat\", \"order_sun\", \"lead_time\", \"npwp\", \"pkp\", \"city\", "
			." \"id_som\", \"zip_code\", \"kelurahan\", \"provinsi\", \"gps_latitude\", \"gps_longitude\", \"kecamatan\", \"company_name\", \"company_address\", \"escrow_account\") "
			." VALUES ('".$object->supplier_id."','".pg_escape_string($object->supp_name)."','".$object->supp_ref."','".$object->address."','".$object->supp_address."','".$object->gst_no."','".$object->contact."','".$object->supp_account_no."','".$object->website."','".$object->bank_account."','".$object->curr_code."','".$object->payment_terms."','".$object->tax_included."','".$object->dimension_id."','".$object->dimension2_id."','".$object->tax_group_id."','".$object->credit_limit."','".$object->purchase_account."','".$object->payable_account."','".$object->payment_discount_account."','".$object->notes."','".$object->inactive."','".$object->supp_code."','".$object->supp_telp."','".$object->pic."','".$object->top_days."','".$object->order_mon."','".$object->order_tue."','".$object->order_wed."','".$object->order_thu."','".$object->order_fri."','".$object->order_sat."','".$object->order_sun."','".$object->lead_time."','".$object->npwp."','".$object->pkp."','".$object->city."','".$object->id_som."','".$object->zip_code."','".$object->kelurahan."','".$object->provinsi."','".$object->gps_latitude."','".$object->gps_longitude."','".$object->kecamatan."','".$object->company_name."','".$object->company_address."','".$object->escrow_account."') ";
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goMultiInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "DELETE FROM m_supplier; INSERT INTO \"m_supplier\" (\"supplier_id\", \"supp_name\", \"address\", \"kecamatan\", \"supp_telp\") "
			." VALUES ".$object;
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->inactive == '') { $object->inactive = '0'; }

		$sql = " UPDATE \"m_supplier\" SET ";
		$flag = false;
		if($object->store_code != '') { if($flag){ $sql .= ","; } $sql .= " \"store_code\" = '".$object->store_code."' "; $flag = true; }
		if($object->store_name != '') { if($flag){ $sql .= ","; } $sql .= " \"store_name\" = '".pg_escape_string($object->store_name)."' "; $flag = true; }
		if($object->address != '') { if($flag){ $sql .= ","; } $sql .= " \"address\" = '".$object->address."' "; $flag = true; }
		if($object->city != '') { if($flag){ $sql .= ","; } $sql .= " \"city\" = '".$object->city."' "; $flag = true; }
		if($object->kelurahan != '') { if($flag){ $sql .= ","; } $sql .= " \"kelurahan\" = '".$object->kelurahan."' "; $flag = true; }
		if($object->propinsi != '') { if($flag){ $sql .= ","; } $sql .= " \"propinsi\" = '".$object->propinsi."' "; $flag = true; }
		if($object->telepon != '') { if($flag){ $sql .= ","; } $sql .= " \"telepon\" = '".$object->telepon."' "; $flag = true; }
		if($object->zip_code != '') { if($flag){ $sql .= ","; } $sql .= " \"zip_code\" = '".$object->zip_code."' "; $flag = true; }
		if($object->kecamatan != '') { if($flag){ $sql .= ","; } $sql .= " \"kecamatan\" = '".$object->kecamatan."' "; $flag = true; }
		if($object->company_name != '') { if($flag){ $sql .= ","; } $sql .= " \"company_name\" = '".$object->company_name."' "; $flag = true; }
		if($object->tax_address != '') { if($flag){ $sql .= ","; } $sql .= " \"tax_address\" = '".$object->tax_address."' "; $flag = true; }
		if($object->npwp != '') { if($flag){ $sql .= ","; } $sql .= " \"npwp\" = '".$object->npwp."' "; $flag = true; }
		$sql .= " WHERE \"supplier_id\" = '".$object->supplier_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_supplier\" "
				." WHERE \"supplier_id\" = '".$object->supplier_id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goMultiDelete($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " DELETE FROM \"m_supplier\" "
				." WHERE \"supplier_id\" IN (".$object.") ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
}
