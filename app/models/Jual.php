<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Jual extends Model
{

	public $id_jual;
	public $id_member;
	public $id_kasir;
	public $kassa_id;
	public $shift_id;
	public $id_reference;
	public $tanggal;	
	public $subtotal;
	public $tax;
	public $discount;	
	public $total;	
	public $kembalian;
	public $note;	
	public $flag_sum;
	public $flag_delete;		
	public $flag_sync;		
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id_jual\", \"id_member\", \"id_kasir\", \"tanggal\", \"subtotal\", \"tax\", \"discount\", \"total\", \"note\" "
				." , \"flag_delete\", \"kassa_id\", \"shift_id\", \"flag_sum\", \"id_reference\", \"kembalian\", \"flag_sync\" "
				." FROM \"t_jual\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_jual'] = $result[0];
			$lists[$count]['id_member'] = $result[1];
			$lists[$count]['id_kasir'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];
			$lists[$count]['subtotal'] = $result[4];			
			$lists[$count]['tax'] = $result[5];	
			$lists[$count]['discount'] = $result[6];	
			$lists[$count]['total'] = $result[7];	
			$lists[$count]['note'] = $result[8];	
			$lists[$count]['flag_delete'] = $result[9];
			$lists[$count]['kassa_id'] = $result[10];
			$lists[$count]['shift_id'] = $result[11];
			$lists[$count]['flag_sum'] = $result[12];
			$lists[$count]['id_reference'] = $result[13];
			$lists[$count]['kembalian'] = $result[14];
			$lists[$count]['flag_sync'] = $result[15];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id_jual\", \"id_member\", \"id_kasir\", \"tanggal\", \"subtotal\", \"tax\", \"discount\", \"total\", \"note\" "
				." , \"flag_delete\", \"kassa_id\", \"shift_id\", \"flag_sum\", \"id_reference\", \"kembalian\", \"flag_sync\" "
				." FROM \"t_jual\" "
				." WHERE \"id_jual\" = '".$object->id_jual."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_jual'] = $result[0];
			$lists[$count]['id_member'] = $result[1];
			$lists[$count]['id_kasir'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];
			$lists[$count]['subtotal'] = $result[4];			
			$lists[$count]['tax'] = $result[5];	
			$lists[$count]['discount'] = $result[6];	
			$lists[$count]['total'] = $result[7];	
			$lists[$count]['note'] = $result[8];	
			$lists[$count]['flag_delete'] = $result[9];
			$lists[$count]['kassa_id'] = $result[10];
			$lists[$count]['shift_id'] = $result[11];
			$lists[$count]['flag_sum'] = $result[12];
			$lists[$count]['id_reference'] = $result[13];	
			$lists[$count]['kembalian'] = $result[14];
			$lists[$count]['flag_sync'] = $result[15];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id_jual\", \"id_member\", \"id_kasir\", \"tanggal\", \"subtotal\", \"tax\", \"discount\", \"total\", \"note\" "
				." , \"flag_delete\", \"kassa_id\", \"shift_id\", \"flag_sum\", \"id_reference\", \"kembalian\", \"flag_sync\", \"jam\" "
				." FROM \"t_jual\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_jual'] = $result[0];
			$lists[$count]['id_member'] = $result[1];
			$lists[$count]['id_kasir'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];
			$lists[$count]['subtotal'] = $result[4];			
			$lists[$count]['tax'] = $result[5];	
			$lists[$count]['discount'] = $result[6];	
			$lists[$count]['total'] = $result[7];	
			$lists[$count]['note'] = $result[8];	
			$lists[$count]['flag_delete'] = $result[9];
			$lists[$count]['kassa_id'] = $result[10];
			$lists[$count]['shift_id'] = $result[11];
			$lists[$count]['flag_sum'] = $result[12];
			$lists[$count]['id_reference'] = $result[13];	
			$lists[$count]['kembalian'] = $result[14];
			$lists[$count]['flag_sync'] = $result[15];
			$lists[$count]['jam'] = $result[16];
			
			$count++;
		}
		
		return $lists;
	}

	public function getSum($cond){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT sum(\"subtotal\") AS subtotal, jam "
				." FROM \"t_jual\" ".$cond." GROUP BY jam ORDER BY jam ASC LIMIT 50";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['subtotal'] = $result[0];
			$lists[$count]['jam'] = $result[1];
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"id_jual\") "
				." FROM \"t_jual\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {              
			$count = $result[0];
		}
		
		return $count;
	}

	public function getSumTotalWeek($condition){
		$connection = new Postgresql($this->di['db']);
		$tunai = 'TUNAI';
		$sql = "SELECT SUM(bayar.\"total\"),SUM(jual.\"kembalian\") "
				." FROM \"t_jual\" jual JOIN \"t_jual_bayar\" bayar
				ON  jual.id_jual = bayar.id_jual 
				where bayar.tipe = 'TUNAI' AND ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {              
			$count = $result[0]-$result[1];
		}
		
		return $count;
	}

	public function getSumTotalDay($condition){
		$connection = new Postgresql($this->di['db']);
		$tunai = 'TUNAI';
		// SELECT date(jual.tanggal), SUM(bayar.total),SUM(jual.kembalian)
		// FROM t_jual jual JOIN t_jual_bayar bayar
		// ON  jual.id_jual = bayar.id_jual 
		// where bayar.tipe = 'TUNAI' AND
		// jual.tanggal = '2018-05-11'
		// group by jual.tanggal
		$sql = "SELECT SUM(bayar.\"total\"),SUM(jual.\"kembalian\") "
				." FROM \"t_jual\" jual JOIN \"t_jual_bayar\" bayar
				ON  jual.id_jual = bayar.id_jual 
				where bayar.tipe = 'TUNAI' AND ".$condition." 
				group by DATE(jual.tanggal)
				";
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {              
			$count = $result[0]-$result[1];
		}
		
		return $count;
	}

	public function getSumTotalPenjualan(){
		$connection = new Postgresql($this->di['db']);
		$tunai = 'TUNAI';
		// SELECT date(jual.tanggal), SUM(bayar.total),SUM(jual.kembalian)
		// FROM t_jual jual JOIN t_jual_bayar bayar
		// ON  jual.id_jual = bayar.id_jual 
		// where bayar.tipe = 'TUNAI' AND
		// jual.tanggal = '2018-05-11'
		// group by jual.tanggal
		$sql = "SELECT SUM(bayar.\"total\"),SUM(jual.\"kembalian\") "
				." FROM \"t_jual\" jual JOIN \"t_jual_bayar\" bayar
				ON  jual.id_jual = bayar.id_jual 
				where bayar.tipe = 'TUNAI' 
				";
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {              
			$count = $result[0]-$result[1];
		}
		
		return $count;
	}

	public function getMaxDateJual(){
		$connection = new Postgresql($this->di['db']);
		$sql = "SELECT MAX(\"tanggal\") "
				." FROM \"t_jual\" ";
		
		$results = $connection->query($sql);

		$count = '';
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {              
			$count = $result[0];
		}
		
		return $count;
	}

	public function getMinDateJual(){
		$connection = new Postgresql($this->di['db']);
		$sql = "SELECT MIN(\"tanggal\") "
				." FROM \"t_jual\" ";
		
		$results = $connection->query($sql);

		$count = '';
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {              
			$count = $result[0];
		}
		
		return $count;
	}

	public function getSumTopupTunai($periode)
	{
		$data = new TMUK();
		$lists = $data->getAll();
		$pay = 0;
		
		if(count($lists) > 0){
			foreach($lists as $list){
				$tmuk = $list['tmuk'];

				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/topup/get-sum-tunai", [
					'tmuk' => $tmuk,
					'periode' => $periode,
				]);
				// transform json to object array php
				$obj = json_decode($result, true);
				if ($obj['status'] == 'success') {
					$pay = $obj['message'];
				}
			}						
		}
		return $pay;
	}

	public function getSumTopupTunaiAll()
	{
		$data = new TMUK();
		$lists = $data->getAll();
		$pay = 0;
		
		if(count($lists) > 0){
			foreach($lists as $list){
				$tmuk = $list['tmuk'];

				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/topup/get-sum-tunai-all", [
					'tmuk' => $tmuk,
				]);
				// transform json to object array php
				$obj = json_decode($result, true);
				if ($obj['status'] == 'success') {
					$pay = $obj['message'];
				}
			}						
		}
		return $pay;
	}

	public function checkSOD($date)
	{
		$sod = false;
		$connection = new Postgresql($this->di['db']);
		$sql = "SELECT tanggal "
				." FROM \"t_start_end_day\"  
				where tanggal = '".$date."' ";
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {              
			$count = $count + 1;
		}

		if($count==0){
			$sod = false;
		}else{
			$sod = true;
		}
		
		return $sod;
	}

	public function getSumTopupMember()
	{
		$data = new TMUK();
		$lists = $data->getAll();
		$pay = 0;
		
		if(count($lists) > 0){
			foreach($lists as $list){
				$tmuk = $list['tmuk'];

				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/topup/get-sum-member", [
					'tmuk' => $tmuk,
				]);
				// transform json to object array php
				$obj = json_decode($result, true);
				if ($obj['status'] == 'success') {
					$pay = $obj['message'];
				}
			}						
		}
		return $pay;
	}

	public function getTopupWeek($week)
	{
		$data = new TMUK();
		$lists = $data->getAll();
		$pay = 0;
		
		if(count($lists) > 0){
			foreach($lists as $list){
				$tmuk = $list['tmuk'];

				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/topup/get-pay-week", [
					'tmuk' => $tmuk,
					'week' => $week,
				]);
				// transform json to object array php
				$obj = json_decode($result, true);
				if ($obj['status'] == 'success') {
					$pay = $obj['message'];
				}
			}						
		}
		return $pay;
	}

	public function getTopupMember($member)
	{
		// var_dump($member);
		// die;
		$data = new TMUK();
		$lists = $data->getAll();
		$pay = 0;
		
		if(count($lists) > 0){
			foreach($lists as $list){
				$tmuk = $list['tmuk'];

				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/topup/get-pay-member", [
					'tmuk' => $tmuk,
					'member' => $member,
				]);
				// transform json to object array php
				$obj = json_decode($result, true);
				if ($obj['status'] == 'success') {
					$pay = $obj['message'];
				}
			}						
		}
		return $pay;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->tanggal == '') { $object->tanggal = date("Y-m-d"); }
		if($object->subtotal == '') { $object->subtotal = '0'; }
		if($object->tax == '') { $object->tax = '0'; }
		if($object->discount == '') { $object->discount = '0'; }
		if($object->total == '') { $object->total = '0'; }
		if($object->flag_delete == '') { $object->flag_delete = 'f'; }
		if($object->flag_sum == '') { $object->flag_sum = 'f'; }
		
		$sql = "INSERT INTO \"t_jual\" (\"id_jual\", \"id_member\", \"id_kasir\", \"tanggal\", \"subtotal\", \"tax\", \"discount\", \"total\", \"note\", \"flag_delete\" "
				." , \"kassa_id\", \"shift_id\", \"flag_sum\", \"id_reference\", \"kembalian\", \"jam\") "
				." VALUES ('".$object->id_jual."','".$object->id_member."','".$object->id_kasir."','".$object->tanggal."','".$object->subtotal."','"
				.$object->tax."','".$object->discount."','".$object->total."','".$object->note."','".$object->flag_delete."','"
				.$object->kassa_id."','".$object->shift_id."','".$object->flag_sum."','".$object->id_reference."','".$object->kembalian."','".date('H')."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}

	public function goInsert2($object){
		if($object->tanggal == '') { $object->tanggal = date("Y-m-d"); }
		if($object->subtotal == '') { $object->subtotal = '0'; }
		if($object->tax == '') { $object->tax = '0'; }
		if($object->discount == '') { $object->discount = '0'; }
		if($object->total == '') { $object->total = '0'; }
		if($object->flag_delete == '') { $object->flag_delete = 'f'; }
		if($object->flag_sum == '') { $object->flag_sum = 'f'; }
		
		$sql = "INSERT INTO \"t_jual\" (\"id_jual\", \"id_member\", \"id_kasir\", \"tanggal\", \"subtotal\", \"tax\", \"discount\", \"total\", \"note\", \"flag_delete\" "
				." , \"kassa_id\", \"shift_id\", \"flag_sum\", \"id_reference\", \"kembalian\", \"jam\") "
				." VALUES ('".$object->id_jual."','".$object->id_member."','".$object->id_kasir."','".$object->tanggal."','".$object->subtotal."','"
				.$object->tax."','".$object->discount."','".$object->total."','".$object->note."','".$object->flag_delete."','"
				.$object->kassa_id."','".$object->shift_id."','".$object->flag_sum."','".$object->id_reference."','".$object->kembalian."','".date('H')."'); ";
		
		return $sql;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		$sql = " UPDATE \"t_jual\" SET ";
		$flag = false;
		if($object->id_member != '') { if($flag){ $sql .= ","; } $sql .= " \"id_member\" = '".$object->id_member."' "; $flag = true; }
		if($object->id_kasir != '') { if($flag){ $sql .= ","; } $sql .= " \"id_kasir\" = '".$object->id_kasir."' "; $flag = true; }
		if($object->tanggal != '') { if($flag){ $sql .= ","; } $sql .= " \"tanggal\" = '".$object->tanggal."' "; $flag = true; }
		if($object->subtotal != '') { if($flag){ $sql .= ","; } $sql .= " \"subtotal\" = '".$object->subtotal."' "; $flag = true; }
		if($object->tax != '') { if($flag){ $sql .= ","; } $sql .= " \"tax\" = '".$object->tax."' "; $flag = true; }
		if($object->discount != '') { if($flag){ $sql .= ","; } $sql .= " \"discount\" = '".$object->discount."' "; $flag = true; }
		if($object->total != '') { if($flag){ $sql .= ","; } $sql .= " \"total\" = '".$object->total."' "; $flag = true; }
		if($object->note != '') { if($flag){ $sql .= ","; } $sql .= " \"note\" = '".$object->note."' "; $flag = true; }
		if($object->flag_delete != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_delete\" = '".$object->flag_delete."' "; $flag = true; }
		if($object->kassa_id != '') { if($flag){ $sql .= ","; } $sql .= " \"kassa_id\" = '".$object->kassa_id."' "; $flag = true; }
		if($object->shift_id != '') { if($flag){ $sql .= ","; } $sql .= " \"shift_id\" = '".$object->shift_id."' "; $flag = true; }
		if($object->flag_sum != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sum\" = '".$object->flag_sum."' "; $flag = true; }
		if($object->id_reference != '') { if($flag){ $sql .= ","; } $sql .= " \"id_reference\" = '".$object->id_reference."' "; $flag = true; }
		if($object->kembalian != '') { if($flag){ $sql .= ","; } $sql .= " \"kembalian\" = '".$object->kembalian."' "; $flag = true; }
		if($object->flag_sync != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = '".$object->flag_sync."' "; $flag = true; }
		$sql .= " WHERE \"id_jual\" = '".$object->id_jual."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;		
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_jual\" "
				." WHERE \"id_jual\" = '".$object->id_jual."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function getJoin_Customer($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id_jual\", \"br_name\", \"id_kasir\" , \"tanggal\", \"subtotal\", \"tax\", \"discount\", \"total\", \"note\", \"kembalian\",  shf.\"description\", kassa.\"description\" "
				." FROM \"t_jual\" tj LEFT JOIN \"m_cust_member\" mcm ON tj.id_member = mcm.nomor_member
				LEFT JOIN \"m_shift\" shf ON tj.shift_id = shf.shift_id
				LEFT JOIN \"m_kassa\" kassa ON tj.kassa_id = kassa.kassa_id ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_jual'] = $result[0];
			$lists[$count]['br_name'] = $result[1];
			$lists[$count]['id_kasir'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];
			$lists[$count]['subtotal'] = $result[4];			
			$lists[$count]['tax'] = $result[5];	
			$lists[$count]['discount'] = $result[6];	
			$lists[$count]['total'] = $result[7];	
			$lists[$count]['note'] = $result[8];	
			$lists[$count]['kembalian'] = $result[9];	
			$lists[$count]['shift'] = $result[10];	
			$lists[$count]['kassa'] = $result[11];	
			
			$count++;
		}
		
		return $lists;
	}

	public function getTotJualShift($condition){
		$connection = new Postgresql($this->di['db']);
		

		$sql = "SELECT j.\"id_kasir\", j.\"kassa_id\", j.\"shift_id\" , DATE(j.\"tanggal\"), SUM(j.\"subtotal\"),
		SUM(j.\"tax\"), SUM(j.\"total\"), SUM(j.\"kembalian\") "
				." FROM \"t_jual\" j ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_kasir'] = $result[0];
			$lists[$count]['kassa_id'] = $result[1];
			$lists[$count]['shift_id'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];
			$lists[$count]['subtotal'] = $result[4];			
			$lists[$count]['tax'] = $result[5];	
			$lists[$count]['total'] = $result[6];	
			$lists[$count]['kembalian'] = $result[7];	
			
			$count++;
		}
		
		return $lists;
	}

	public function getTotJualBayarShift($condition){
		$connection = new Postgresql($this->di['db']);
		

		$sql = "SELECT SUM(b.\"total\"), b.\"tipe\" "
				." FROM \"t_jual\" j join t_jual_bayar b on j.id_jual = b.id_jual ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['total'] = $result[0];
			$lists[$count]['tipe'] = $result[1];	
			
			$count++;
		}
		
		return $lists; 

	}

	public function getTotJualDay($condition){
		$connection = new Postgresql($this->di['db']);
		

		$sql = "SELECT DATE(j.\"tanggal\"), SUM(j.\"subtotal\"),
		SUM(j.\"tax\"), SUM(j.\"total\"), SUM(j.\"kembalian\") "
				." FROM \"t_jual\" j ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['tanggal'] = $result[0];
			$lists[$count]['subtotal'] = $result[1];			
			$lists[$count]['tax'] = $result[2];	
			$lists[$count]['total'] = $result[3];	
			$lists[$count]['kembalian'] = $result[4];	
			
			$count++;
		}
		
		return $lists;
	}

	public function getTotJualBayarDay($condition){
		$connection = new Postgresql($this->di['db']);
		

		$sql = "SELECT SUM(b.\"total\"), b.\"tipe\" "
				." FROM \"t_jual\" j join t_jual_bayar b on j.id_jual = b.id_jual ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['total'] = $result[0];
			$lists[$count]['tipe'] = $result[1];	
			
			$count++;
		}
		
		return $lists; 

	}
	
	public function getJoin($param){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT tj.\"id_jual\", mcm.\"br_name\", tj.\"id_kasir\", tj.\"tanggal\", tj.\"subtotal\", tj.\"tax\", tj.\"discount\", tjd.\"total\", tj.\"note\", tjd.\"item_code\", tjd.\"description\", tjd.\"harga\", tjd.\"harga_member\", tjd.\"qty\", tjd.\"satuan\", tjd.\"discount\", tjd.\"promo\" "
				
				." FROM \"t_jual\" tj INNER JOIN \"t_jual_detail\" tjd ON tj.id_jual = tjd.id_jual "
				." LEFT JOIN \"m_cust_member\" mcm ON tj.id_member = mcm.branch_code::varchar"
				." WHERE tj.\"id_jual\" = '".$param."' ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_jual'] = $result[0];
			$lists[$count]['br_name'] = $result[1];
			$lists[$count]['id_kasir'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];
			$lists[$count]['subtotal'] = $result[4];			
			$lists[$count]['tax'] = $result[5];	
			$lists[$count]['discount'] = $result[6];	
			$lists[$count]['total'] = $result[7];	
			$lists[$count]['note'] = $result[8];	
			$lists[$count]['item_code'] = $result[9];	
			$lists[$count]['description'] = $result[10];	
			$lists[$count]['harga'] = $result[11];	
			$lists[$count]['harga_member'] = $result[12];	
			$lists[$count]['qty'] = $result[13];	
			$lists[$count]['satuan'] = $result[14];	
			$lists[$count]['discount'] = $result[15];	
			$lists[$count]['promo'] = $result[16];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function summarize($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id_kasir\", \"tanggal\", \"kassa_id\", \"shift_id\", SUM(\"subtotal\") as subtotal_sum, SUM(\"tax\") as tax_sum, SUM(\"discount\") as discount_sum, SUM(\"total\") as total_sum, SUM(\"kembalian\") as kembalian_sum "
			." FROM \"t_jual\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_kasir'] = $result[0];
			$lists[$count]['tanggal'] = $result[1];
			$lists[$count]['kassa_id'] = $result[2];			
			$lists[$count]['shift_id'] = $result[3];	
			$lists[$count]['subtotal_sum'] = $result[4];	
			$lists[$count]['tax_sum'] = $result[5];	
			$lists[$count]['discount_sum'] = $result[6];	
			$lists[$count]['total_sum'] = $result[7];	
			$lists[$count]['kembalian_sum'] = $result[8];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function goUpdate_flagSum($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_jual\" SET ";
		$flag = false;
		if($object->id_kasir != '') { if($flag){ $sql .= ","; } $sql .= " \"id_kasir\" = '".$object->id_kasir."' "; $flag = true; }
		if($object->tanggal != '') { if($flag){ $sql .= ","; } $sql .= " \"tanggal\" = '".$object->tanggal."' "; $flag = true; }
		if($object->kassa_id != '') { if($flag){ $sql .= ","; } $sql .= " \"kassa_id\" = '".$object->kassa_id."' "; $flag = true; }
		if($object->shift_id != '') { if($flag){ $sql .= ","; } $sql .= " \"shift_id\" = '".$object->shift_id."' "; $flag = true; }
		if($object->flag_sum != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sum\" = '".$object->flag_sum."' "; $flag = true; }
		$sql .= " WHERE \"id_kasir\" = '".$object->id_kasir."' AND \"tanggal\" = '".$object->tanggal."' AND \"kassa_id\" = '".$object->kassa_id."' AND \"shift_id\" = '".$object->shift_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;		
	}
	
	public function getJoin_JualDetail($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "
			SELECT a.id_jual, a.item_code, a.barcode, a.description, a.tanggal, a.qty - b.qty as selisih, a.satuan, a.harga 
			FROM 

				(SELECT j.id_jual, jd.description, jd.item_code, jd.barcode, jd.qty, jd.satuan, jd.harga, tanggal 
				FROM t_jual j 
				JOIN t_jual_detail jd on j.id_jual = jd.id_jual ".$condition.") a

			RIGHT JOIN 

				(SELECT j.id_jual, jd.description, id_reference, qty 
				FROM t_jual j 
				JOIN t_jual_detail jd on j.id_jual = jd.id_jual 
				WHERE id_reference != '') b on a.id_jual = b.id_reference and a.description = b.description

		";		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_jual'] = $result[0];
			$lists[$count]['item_code'] = $result[1];
			$lists[$count]['barcode'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['tanggal'] = $result[4];			
			$lists[$count]['selisih'] = $result[5];	
			$lists[$count]['satuan'] = $result[6];	
			$lists[$count]['harga'] = $result[7];	
			
			$count++;
		}
		
		return $lists;
	}

	/* custom by kimochi */
	// param = tipe (array)
	public function getTotalPembayaranSync($tipe=[], $tgl)
	{
		$connection = new Postgresql($this->di['db']);

		$where = '';
		if ($tipe) {
			$tmp_tipe = [];
			foreach ($tipe as $val) {
				$tmp_tipe[] = "'$val'";
			}
			
			$where = ' AND jb.tipe IN (' . implode(',', $tmp_tipe).')';
		}
		
		// $sql = "
		// 	SELECT sum(j.total) as total
		// 	FROM t_jual j
		// 	JOIN t_jual_bayar jb ON jb.id_jual = j.id_jual
		// 	WHERE j.flag_sync = " . ($flag_sync ? 'true' : 'false') . " $where
		// ";
		if($tipe[0] == 'VOUCHER' || $tipe[0] == 'PIUTANG' || $tipe[0] == 'POINT'){
			$sql = "
				SELECT sum(jb.total) as total
				FROM t_jual j
				JOIN t_jual_bayar jb ON jb.id_jual = j.id_jual
				WHERE j.tanggal between '".$tgl.' 00:00:00'."' and '".$tgl.' 23:59:59'."' $where
			";	
		}else{
			$sql = "
				SELECT sum(j.total) as total
				FROM t_jual j
				JOIN t_jual_bayar jb ON jb.id_jual = j.id_jual
				WHERE j.tanggal between '".$tgl.' 00:00:00'."' and '".$tgl.' 23:59:59'."' $where
			";	
		}
		
		$results = $connection->query($sql);

		$lists = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists = $result[0];
		}
		return $lists;
	}

	public function getTotalPembayaranCetak($tipe=[], $from, $to)
	{
		$connection = new Postgresql($this->di['db']);

		$where = '';
		if ($tipe) {
			$tmp_tipe = [];
			foreach ($tipe as $val) {
				$tmp_tipe[] = "'$val'";
			}
			
			$where = ' AND jb.tipe IN (' . implode(',', $tmp_tipe).')';
		}
		
		// $sql = "
		// 	SELECT sum(j.total) as total
		// 	FROM t_jual j
		// 	JOIN t_jual_bayar jb ON jb.id_jual = j.id_jual
		// 	WHERE j.flag_sync = " . ($flag_sync ? 'true' : 'false') . " $where
		// ";
		if($tipe[0] == 'VOUCHER' || $tipe[0] == 'PIUTANG' || $tipe[0] == 'POINT'){
			$sql = "
				SELECT sum(jb.total) as total
				FROM t_jual j
				JOIN t_jual_bayar jb ON jb.id_jual = j.id_jual
				WHERE j.tanggal between '".$from.' 00:00:00'."' and '".$to.' 23:59:59'."' $where
			";	
		}else{
			$sql = "
				SELECT sum(j.total) as total
				FROM t_jual j
				JOIN t_jual_bayar jb ON jb.id_jual = j.id_jual
				WHERE j.tanggal between '".$from.' 00:00:00'."' and '".$to.' 23:59:59'."' $where
			";	
		}
		
		$results = $connection->query($sql);

		$lists = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists = $result[0];
		}
		return $lists;
	}

	public function Join_JualDetail($from, $to){
		$connection = new Postgresql($this->di['db']);

		$sql = "		
			select distinct(id_jual), tanggal
			from t_jual
			where tanggal between '".$from." 00:00:00' AND '".$to." 23:59:59'
			order by tanggal
		";
	
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_jual'] = $result[0];
			$lists[$count]['tanggal'] = date('d-m-Y', strtotime($result[1]));
			// $lists[$count]['harga'] = $result[2];
			// $lists[$count]['qty'] = $result[3];
			// $lists[$count]['reference'] = $result[4];
			// $lists[$count]['barcode'] = $result[5];
			// $lists[$count]['tanggal'] = $result[6];

			$count++;
		}
		
		return $lists;
	}

	// ambil hpp penjualan dari transaksi yg belum di sinkron
	public function getHppPenjualanSync($tgl)
	{
		$connection = new Postgresql($this->di['db']);

		$sql = "select sum(jd.qty*pp.hpp) as hpp from t_jual j join t_jual_detail jd on j.id_jual = jd.id_jual LEFT join trans_hpp pp on pp.stock_id = jd.item_code where pp.tanggal='".$tgl."' AND j.tanggal BETWEEN '".$tgl." 00:00:00' AND '".$tgl." 23:59:59'
		";	
		// 	select sum(qty*hpp) as hpp
		// 	from trans_hpp
		// 	where j.tanggal ='".$tgl."'
		// ";	
		
		$results = $connection->query($sql);

		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists = $result[0];
		}

		return $lists;
	}

	// update status syncron penjualan ke true
	public function updatePenjualanSync()
	{
		$connection = new Postgresql($this->di['db']);

		$connection->execute("update t_jual SET flag_sync = true where flag_sync = false;");

		return true;
	}

	// kebutuhan buat sync penjualan ke server HO
	// return array kumaha aink
	public function getDataSyncPenjualan()
	{	

		$list = [];

		// TMUK
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getAll();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
			}
		}

		$jual = new Jual();
		$condition = " WHERE flag_sync = false LIMIT 3";
		$lists_jual = $jual::getFreeSQL($condition);

		$c_jual = 0;
		if(count($lists_jual)>0){
			foreach($lists_jual as $list1){

				$list[$c_jual]['tmuk_kode']    = $tmuk->tmuk;
				$list[$c_jual]['jual_kode']    = $list1['id_jual'];
				$list[$c_jual]['member_nomor'] = $list1['id_member'];
				$list[$c_jual]['kasir_id']     = $list1['id_kasir'];
				$list[$c_jual]['kassa_id']     = $list1['kassa_id'];
				$list[$c_jual]['shift_id']     = $list1['shift_id'];
				$list[$c_jual]['reference_id'] = $list1['id_reference'];
				$list[$c_jual]['tanggal']      = $list1['tanggal'];
				$list[$c_jual]['subtotal']     = $list1['subtotal'];
				$list[$c_jual]['tax']          = $list1['tax'];
				$list[$c_jual]['discount']     = $list1['discount'];
				$list[$c_jual]['total']        = $list1['total'];
				$list[$c_jual]['kembalian']    = $list1['kembalian'];
				$list[$c_jual]['note']         = $list1['note'];
				$list[$c_jual]['flag_sum']     = $list1['flag_sum'];
				$list[$c_jual]['flag_delete']  = $list1['flag_delete'];
				$list[$c_jual]['jam']          = $list1['jam'];
				$list[$c_jual]['flag_sync']    = $list1['flag_sync'];

				// generate bayarnya
				$jualbayar = new JualBayar();
				$condition = " WHERE \"id_jual\" = '".$list1['id_jual']."' ";
				$lists_jual_bayar = $jualbayar::getFreeSQL($condition);

				$bayarnya = [];
				$c_bayar = 0;
				if(count($lists_jual_bayar)>0){
					foreach($lists_jual_bayar as $list2){
						$bayarnya[$c_bayar]['tmuk_kode']            = $tmuk->tmuk;
						$bayarnya[$c_bayar]['rekap_penjualan_kode'] = $list2['id_jual'];
						$bayarnya[$c_bayar]['tipe']                 = $list2['tipe'];
						$bayarnya[$c_bayar]['bank_id']              = $list2['bank_id'];
						$bayarnya[$c_bayar]['bank_name']            = $list2['bank_name'];
						$bayarnya[$c_bayar]['total']                = $list2['total'];
						$bayarnya[$c_bayar]['no_kartu']             = $list2['no_kartu'];

						$c_bayar++;
					}
				}


				// generate detailnya
				$jualdetail = new JualDetail();
				$condition = " WHERE \"id_jual\" = '".$list1['id_jual']."' ";
				$lists_jual_detail = $jualdetail::getFreeSQL($condition);

				$detailnya = [];
				$c_detail = 0;
				if(count($lists_jual_detail)>0){
					foreach($lists_jual_detail as $list3){
						$detailnya[$c_detail]['tmuk_kode']            = $tmuk->tmuk;
						$detailnya[$c_detail]['rekap_penjualan_kode'] = $list3['id_jual'];
						$detailnya[$c_detail]['item_code']            = $list3['item_code'];
						$detailnya[$c_detail]['description']          = $list3['description'];
						$detailnya[$c_detail]['harga']                = $list3['harga'];			
						$detailnya[$c_detail]['harga_member']         = $list3['harga_member'];	
						$detailnya[$c_detail]['qty']                  = $list3['qty'];	
						$detailnya[$c_detail]['satuan']               = $list3['satuan'];	
						$detailnya[$c_detail]['discount']             = $list3['discount'];	
						$detailnya[$c_detail]['total']                = $list3['total'];	
						$detailnya[$c_detail]['promo']                = $list3['promo'];
						$detailnya[$c_detail]['barcode']              = $list3['barcode'];			
						$detailnya[$c_detail]['disc_percent']         = $list3['disc_percent'];			
						$detailnya[$c_detail]['disc_amount']          = $list3['disc_amount'];
						$detailnya[$c_detail]['tax_type']             = $list3['tax_type'];
						$detailnya[$c_detail]['harga_hpp3']           = $list3['harga_hpp3'];

						$c_detail++;
					}
				}

				// hijikeun
				$list[$c_jual]['bayarnya'] = $bayarnya;
				$list[$c_jual]['detailnya'] = $detailnya;

				$c_jual++;
			}// end foreach t_jual
		}

		return $list;
	}

}
