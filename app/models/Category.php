<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Category extends Model
{

	public $id;
	public $bumun_cd;
	public $bumun_nm;
	public $l1_cd;
	public $l1_nm;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"bumun_cd\", \"bumun_nm\", \"l1_cd\", \"l1_nm\" "
				." FROM \"m_category\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['bumun_cd'] = $result[1];
			$lists[$count]['bumun_nm'] = $result[2];
			$lists[$count]['l1_cd'] = $result[3];
			$lists[$count]['l1_nm'] = $result[4];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"bumun_cd\", \"bumun_nm\", \"l1_cd\", \"l1_nm\" "
				." FROM \"m_category\" "
				." WHERE \"bumun_cd\" = '".$object->bumun_cd."' "
				." AND \"l1_cd\" = '".$object->l1_cd."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['bumun_cd'] = $result[1];
			$lists[$count]['bumun_nm'] = $result[2];
			$lists[$count]['l1_cd'] = $result[3];
			$lists[$count]['l1_nm'] = $result[4];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"bumun_cd\", \"bumun_nm\", \"l1_cd\", \"l1_nm\" "
				." FROM \"m_category\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['bumun_cd'] = $result[1];
			$lists[$count]['bumun_nm'] = $result[2];
			$lists[$count]['l1_cd'] = $result[3];
			$lists[$count]['l1_nm'] = $result[4];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"l1_cd\") "
				." FROM \"m_category\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->auto_update == '') { $object->auto_update = '0'; }
		if($object->inactive == '') { $object->inactive = '0'; }
		$sql = "INSERT INTO \"m_category\" (\"id\", \"bumun_cd\", \"bumun_nm\", \"l1_cd\", \"l1_nm\") "
				." VALUES ('".$object->id."','".$object->bumun_cd."','".$object->bumun_nm."','".$object->l1_cd."','".$object->l1_nm."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goMultiInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "INSERT INTO \"m_category\" (\"id\", \"bumun_cd\", \"bumun_nm\", \"l1_cd\", \"l1_nm\") "
			." VALUES ".$object;
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_category\" SET ";
		$flag = false;
		if($object->bumun_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"bumun_nm\" = '".$object->bumun_nm."' "; $flag = true; }
		if($object->l1_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"l1_nm\" = '".$object->l1_nm."' "; $flag = true; }
		$sql .= " WHERE \"bumun_cd\" = '".$object->bumun_cd."' ";		
		$sql .= " AND \"l1_cd\" = '".$object->l1_cd."' ";
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_category\" "
				." WHERE \"bumun_cd\" = '".$object->bumun_cd."' "
				." AND \"l1_cd\" = '".$object->l1_cd."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goMultiDelete($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " DELETE FROM \"m_category\" "
				." WHERE \"bumun_cd\" IN (".$object['bumun_cd'].") AND \"l1_cd\" IN (".$object['l1_cd'].") ";
		$success = $connection->execute($sql);
		// var_dump($sql); die();
		// var_dump('testtt'); die();
		$id = $connection->lastInsertId();
		
		return $success;
	}
}
