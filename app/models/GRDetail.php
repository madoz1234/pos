<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class PRDetail extends Model
{

	public $gr_detail_item;
	public $gr_no;
	public $item_code;
	public $description;		
	public $qty_pr;
	public $unit_pr;	
	public $qty_order;
	public $unit_order;
	public $qty_po;
	public $unit_po;
	public $pr_price;
	public $po_price;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"gr_detail_item\", \"gr_no\", \"item_code\", \"description\", \"qty_pr\", \"unit_pr\", \"qty_order\" "
				." , \"unit_order\", \"qty_po\", \"unit_po\", \"pr_price\", \"po_price\" "
				." FROM \"t_gr_detail\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['gr_detail_item'] = $result[0];
			$lists[$count]['gr_no'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty_pr'] = $result[4];			
			$lists[$count]['unit_pr'] = $result[5];	
			$lists[$count]['qty_order'] = $result[6];	
			$lists[$count]['unit_order'] = $result[7];	
			$lists[$count]['qty_po'] = $result[8];	
			$lists[$count]['unit_po'] = $result[9];	
			$lists[$count]['pr_price'] = $result[10];	
			$lists[$count]['po_price'] = $result[11];			
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"gr_detail_item\", \"gr_no\", \"item_code\", \"description\", \"qty_pr\", \"unit_pr\", \"qty_order\" "
				." , \"unit_order\", \"qty_po\", \"unit_po\", \"pr_price\", \"po_price\" "
				." FROM \"t_gr_detail\" "
				." WHERE \"gr_no\" = '".$object->gr_no."' "
				." AND \"gr_detail_item\" = '".$object->gr_detail_item."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['gr_detail_item'] = $result[0];
			$lists[$count]['gr_no'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty_pr'] = $result[4];			
			$lists[$count]['unit_pr'] = $result[5];	
			$lists[$count]['qty_order'] = $result[6];	
			$lists[$count]['unit_order'] = $result[7];	
			$lists[$count]['qty_po'] = $result[8];	
			$lists[$count]['unit_po'] = $result[9];	
			$lists[$count]['pr_price'] = $result[10];	
			$lists[$count]['po_price'] = $result[11];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"gr_detail_item\", \"gr_no\", \"item_code\", \"description\", \"qty_pr\", \"unit_pr\", \"qty_order\" "
				." , \"unit_order\", \"qty_po\", \"unit_po\", \"pr_price\", \"po_price\" "
				." FROM \"t_gr_detail\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['gr_detail_item'] = $result[0];
			$lists[$count]['gr_no'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['qty_pr'] = $result[4];			
			$lists[$count]['unit_pr'] = $result[5];	
			$lists[$count]['qty_order'] = $result[6];	
			$lists[$count]['unit_order'] = $result[7];	
			$lists[$count]['qty_po'] = $result[8];	
			$lists[$count]['unit_po'] = $result[9];	
			$lists[$count]['pr_price'] = $result[10];	
			$lists[$count]['po_price'] = $result[11];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"gr_no\") "
				." FROM \"t_gr_detail\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->qty_pr == '') { $object->qty_pr = '0'; }
		if($object->qty_order == '') { $object->qty_order = '0'; }				
		if($object->qty_po == '') { $object->qty_po = '0'; }				
		if($object->pr_price == '') { $object->pr_price = '0'; }				
		if($object->po_price == '') { $object->po_price = '0'; }				
		
		$sql = "INSERT INTO \"t_gr_detail\" (\"gr_no\", \"item_code\", \"description\", \"qty_pr\", \"unit_pr\", \"qty_order\" "
				." , \"unit_order\", \"qty_po\", \"unit_po\", \"pr_price\", \"po_price\" ) "
				." VALUES ('".$object->gr_no."','".$object->item_code."','".pg_escape_string($object->description)."','".$object->qty_pr."','".$object->unit_pr."','"
				.$object->qty_order."','".$object->unit_order."','".$object->qty_po."','".$object->unit_po."','".$object->pr_price."','"
				.$object->po_price."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_gr_detail\" SET ";
		$flag = false;
		if($object->item_code != '') { if($flag){ $sql .= ","; } $sql .= " \"item_code\" = '".$object->item_code."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".pg_escape_string($object->description)."' "; $flag = true; }
		if($object->qty_pr != '') { if($flag){ $sql .= ","; } $sql .= " \"qty_pr\" = '".$object->qty_pr."' "; $flag = true; }
		if($object->unit_pr != '') { if($flag){ $sql .= ","; } $sql .= " \"unit_pr\" = '".$object->unit_pr."' "; $flag = true; }
		if($object->qty_order != '') { if($flag){ $sql .= ","; } $sql .= " \"qty_order\" = '".$object->qty_order."' "; $flag = true; }
		if($object->unit_order != '') { if($flag){ $sql .= ","; } $sql .= " \"unit_order\" = '".$object->unit_order."' "; $flag = true; }
		if($object->qty_po != '') { if($flag){ $sql .= ","; } $sql .= " \"qty_po\" = '".$object->qty_po."' "; $flag = true; }
		if($object->unit_po != '') { if($flag){ $sql .= ","; } $sql .= " \"unit_po\" = '".$object->unit_po."' "; $flag = true; }
		if($object->pr_price != '') { if($flag){ $sql .= ","; } $sql .= " \"pr_price\" = '".$object->pr_price."' "; $flag = true; }
		if($object->po_price != '') { if($flag){ $sql .= ","; } $sql .= " \"po_price\" = '".$object->po_price."' "; $flag = true; }
		$sql .= " WHERE \"gr_no\" = '".$object->gr_no."' ";	
		$sql .= " AND \"gr_detail_item\" = '".$object->gr_detail_item."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_gr_detail\" "
				." WHERE \"gr_no\" = '".$object->gr_no."' "
				." WHERE \"gr_detail_item\" = '".$object->gr_detail_item."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
