<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class StartEndShift extends Model
{

	public $id;
	public $tanggal;
	public $user_id;
	public $kassa_id;
	public $kassa_desc;
	public $shift_id;
	public $shift_desc;
	public $setoran;
	public $flag_start;	
	public $flag_end;	
	public $cash_cashier;
	public $jam;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"tanggal\", \"user_id\", \"kassa_id\", \"kassa_desc\", \"shift_id\", \"shift_desc\", \"setoran\", \"flag_start\", \"flag_end\", \"cash_cashier\", \"jam\" "
				." FROM \"t_start_end_shift\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['tanggal'] = $result[1];
			$lists[$count]['user_id'] = $result[2];
			$lists[$count]['kassa_id'] = $result[3];
			$lists[$count]['kassa_desc'] = $result[4];			
			$lists[$count]['shift_id'] = $result[5];	
			$lists[$count]['shift_desc'] = $result[6];	
			$lists[$count]['setoran'] = $result[7];	
			$lists[$count]['flag_start'] = $result[8];	
			$lists[$count]['flag_end'] = $result[9];
			$lists[$count]['cash_cashier'] = $result[10];
			$lists[$count]['jam'] = $result[11];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"tanggal\", \"user_id\", \"kassa_id\", \"kassa_desc\", \"shift_id\", \"shift_desc\", \"setoran\", \"flag_start\", \"flag_end\", \"cash_cashier\", \"jam\"  "
				." FROM \"t_start_end_shift\" "
				." WHERE \"id\" = ".$object->id." "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['tanggal'] = $result[1];
			$lists[$count]['user_id'] = $result[2];
			$lists[$count]['kassa_id'] = $result[3];
			$lists[$count]['kassa_desc'] = $result[4];			
			$lists[$count]['shift_id'] = $result[5];	
			$lists[$count]['shift_desc'] = $result[6];	
			$lists[$count]['setoran'] = $result[7];	
			$lists[$count]['flag_start'] = $result[8];	
			$lists[$count]['flag_end'] = $result[9];	
			$lists[$count]['cash_cashier'] = $result[10];
			$lists[$count]['jam'] = $result[11];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"tanggal\", \"user_id\", \"kassa_id\", \"kassa_desc\", \"shift_id\", \"shift_desc\", \"setoran\", \"flag_start\", \"flag_end\", \"cash_cashier\", \"jam\", \"flag_sync\" "
				." FROM \"t_start_end_shift\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['tanggal'] = $result[1];
			$lists[$count]['user_id'] = $result[2];
			$lists[$count]['kassa_id'] = $result[3];
			$lists[$count]['kassa_desc'] = $result[4];			
			$lists[$count]['shift_id'] = $result[5];	
			$lists[$count]['shift_desc'] = $result[6];	
			$lists[$count]['setoran'] = $result[7];	
			$lists[$count]['flag_start'] = $result[8];	
			$lists[$count]['flag_end'] = $result[9];	
			$lists[$count]['cash_cashier'] = $result[10];
			$lists[$count]['jam'] = $result[11];
			$lists[$count]['flag_sync'] = $result[12];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"id\") "
				." FROM \"t_start_end_shift\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->tanggal == '') { $object->tanggal = date("Y-m-d"); }
		if($object->jam == '') { $object->jam = date("H:m:s"); }
		if($object->setoran == '') { $object->setoran = '0'; }
		if($object->cash_cashier == '') { $object->cash_cashier = '0'; }
		if($object->flag_start == '') { $object->flag_start = 'f'; }
		if($object->flag_end == '') { $object->flag_end = 'f'; }		
		
		$sql = "INSERT INTO \"t_start_end_shift\" (\"tanggal\", \"user_id\", \"kassa_id\", \"kassa_desc\", \"shift_id\", \"shift_desc\", \"setoran\", \"flag_start\", \"flag_end\", \"cash_cashier\", \"jam\") "
				." VALUES ('".$object->tanggal."','".$object->user_id."','".$object->kassa_id."','".$object->kassa_desc."','".$object->shift_id."','"
				.$object->shift_desc."','".$object->setoran."','".$object->flag_start."','".$object->flag_end."','".$object->cash_cashier."', '".$object->jam."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_start_end_shift\" SET ";
		$flag = false;
		if($object->tanggal != '') { if($flag){ $sql .= ","; } $sql .= " \"tanggal\" = '".$object->tanggal."' "; $flag = true; }
		if($object->user_id != '') { if($flag){ $sql .= ","; } $sql .= " \"user_id\" = '".$object->user_id."' "; $flag = true; }
		if($object->kassa_id != '') { if($flag){ $sql .= ","; } $sql .= " \"kassa_id\" = '".$object->kassa_id."' "; $flag = true; }
		if($object->kassa_desc != '') { if($flag){ $sql .= ","; } $sql .= " \"kassa_desc\" = '".$object->kassa_desc."' "; $flag = true; }
		if($object->shift_id != '') { if($flag){ $sql .= ","; } $sql .= " \"shift_id\" = '".$object->shift_id."' "; $flag = true; }
		if($object->shift_desc != '') { if($flag){ $sql .= ","; } $sql .= " \"shift_desc\" = '".$object->shift_desc."' "; $flag = true; }
		if($object->setoran != '') { if($flag){ $sql .= ","; } $sql .= " \"setoran\" = '".$object->setoran."' "; $flag = true; }
		if($object->flag_start != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_start\" = '".$object->flag_start."' "; $flag = true; }
		if($object->flag_end != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_end\" = '".$object->flag_end."' "; $flag = true; }		
		if($object->cash_cashier != '') { if($flag){ $sql .= ","; } $sql .= " \"cash_cashier\" = '".$object->cash_cashier."' "; $flag = true; }
		if($object->jam != '') { if($flag){ $sql .= ","; } $sql .= " \"jam\" = '".$object->jam."' "; $flag = true; }		
		if($object->flag_sync != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = '".$object->flag_sync."' "; $flag = true; }		
		$sql .= " WHERE \"id\" = '".$object->id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_start_end_shift\" "
				." WHERE \"id\" = '".$object->id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goUpdate_flagEndDay($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_start_end_shift\" SET ";
		
		$flag = false;
		if($object->flag_end_day != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_end_day\" = '".$object->flag_end_day."' "; $flag = true; }			
		$sql .= " WHERE \"tanggal\" = '".$object->tanggal."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
}
