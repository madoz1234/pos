<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class PwpList extends Model
{

	public $pwp_id;
	public $kode_produk;
	public $price;
	public $flag;

	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"pwp_id\", \"kode_produk\", \"flag\" "
				." FROM \"m_promosi_pwp_list\" "
				." WHERE \"flag_delete\" = false ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['pwp_id'] = $result[0];
			$lists[$count]['kode_produk'] = $result[1];
			$lists[$count]['flag'] = $result[2];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"pwp_id\", \"kode_produk\", \"flag\" "
				." FROM \"m_promosi_pwp_list\" "
				." WHERE \"pwp_id\" = '".$object->pwp_id."' AND \"kode_produk\" = '".$object->kode_produk."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['pwp_id'] = $result[0];
			$lists[$count]['kode_produk'] = $result[1];
			$lists[$count]['flag'] = $result[2];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"pwp_id\", \"kode_produk\", \"flag\" "
				." FROM \"m_promosi_pwp_list\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['pwp_id'] = $result[0];
			$lists[$count]['kode_produk'] = $result[1];
			$lists[$count]['flag'] = $result[2];
			
			$count++;
		}
		
		return $lists;
	}

	public function getFreeSQL2($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"flag\"), \"pwp_id\" "
				." FROM \"m_promosi_pwp_list\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['count'] = $result[0];
			$lists[$count]['pwp_id'] = $result[1];
			$count++;						
		}
		
		return $lists;
	}

	public function getPwpIdGroup($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"pwp_id\" "
				." FROM \"m_promosi_pwp_list\" ".$condition;
				
		$results = $connection->query($sql);
		
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists['pwp_id'][$result[0]] = $result[0];
		}

		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"pwp_id\") "
				." FROM \"m_promosi_pwp_list\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->price == '') { $object->price = date("Y-m-d"); }
		
		$sql = "INSERT INTO \"m_promosi_pwp_list\" (\"pwp_id\", \"kode_produk\", \"flag\") "
				." VALUES ('".$object->pwp_id."','".$object->kode_produk."','".$object->flag."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_promosi_pwp_list\" SET ";
		$flag = false;
		if($object->pwp_id != '') { if($flag){ $sql .= ","; } $sql .= " \"pwp_id\" = '".$object->pwp_id."' "; $flag = true; }
		if($object->kode_produk != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_produk\" = '".$object->kode_produk."' "; $flag = true; }
		if($object->flag != '') { if($flag){ $sql .= ","; } $sql .= " \"flag\" = '".$object->flag."' "; $flag = true; }
		$sql .= " WHERE \"pwp_id\" = '".$object->pwp_id."' AND \"kode_produk\" = '".$object->kode_produk."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_promosi_pwp_list\" "
				." WHERE \"pwp_id\" = '".$object->pwp_id."' ";
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	// public function getJoin_AdjDetail($condition){
	// 	$connection = new Postgresql($this->di['db']);
		
	// 	$sql = "SELECT a.\"pwp_id\", \"kode_produk\", \"price\", \"harga_diskon_x\", \"kode_produk_y\", \"harga_jual_y\", \"diskon_y\", \"harga_diskon_y\", \"flag_delete\", \"item_code\", \"description\", \"qty\", \"unit\", \"unit_price\", \"total_price\", \"barcode_code\" "
	// 		." FROM \"m_promosi_pwp_list\" a INNER JOIN \"m_promosi_pwp_list_detail\" ad ON a.pwp_id = ad.pwp_id ".$condition;
		
	// 	$results = $connection->query($sql);
		
	// 	$count = 0;
	// 	$lists = null;
	// 	$results->setFetchMode(Phalcon\Db::FETCH_NUM);
	// 	while ($result = $results->fetchArray()) {
	// 		$lists[$count]['pwp_id'] = $result[0];
	// 		$lists[$count]['kode_produk'] = $result[1];
	// 		$lists[$count]['price'] = $result[2];
	// 		$lists[$count]['harga_diskon_x'] = $result[3];
	// 		$lists[$count]['kode_produk_y'] = $result[4];
	// 		$lists[$count]['harga_jual_y'] = $result[5];
	// 		$lists[$count]['diskon_y'] = $result[6];
	// 		$lists[$count]['harga_diskon_y'] = $result[7];
	// 		$lists[$count]['flag_delete'] = $result[8];
	// 		$lists[$count]['item_code'] = $result[9];
	// 		$lists[$count]['description'] = $result[10];
	// 		$lists[$count]['qty'] = $result[11];
	// 		$lists[$count]['unit'] = $result[12];
	// 		$lists[$count]['unit_price'] = $result[13];
	// 		$lists[$count]['total_price'] = $result[14];
	// 		$lists[$count]['barcode_code'] = $result[15];
			
	// 		$count++;
	// 	}
		
	// 	return $lists;
	// }
}
