<?php

use Phalcon\Mvc\Model;

class ListFunction extends Model
{
	
	public function selectRack(){
		$rack = new Rack();
		$lists = $rack::getAll();
		
		$data = null;		
		if(isset($lists)){
			foreach($lists as $list){				
				$data[$list['id']] = $list['rack_type'];
			}
		}
		
		return $data;
	}
	
	public function selectCust(){
		$o_cust = new Cust();
		$lists = $o_cust::getAll();
		
		$data = null;		
		if(isset($lists)){
			foreach($lists as $list){				
				$data[$list['debtor_no']] = $list['name'];
			}
		}
		
		return $data;
	}
	
	public function selectTMUK(){
		$tmuk = new TMUK();
		$lists = $tmuk::getAll();
		
		$tmuk = null;		
		if(isset($lists)){
			foreach($lists as $list){				
				$tmuk = $list['tmuk'];
			}
		}
		
		$o_cust = new Cust();
		$lists = $o_cust::getAll();
		
		$data = null;		
		if(isset($lists)){
			foreach($lists as $list){
				if($list['nomor_member'] == $tmuk)
					$data[$list['debtor_no']] = $list['name'];
			}
		}
		
		
		
		return $data;
	}

	public function selectJenisKustomer(){
		$record = new JenisKustomer();
		$tes = new JenisKustomer();
		// $lists = $record::getAll();

		$lists = $record::getFreeSQL(" WHERE \"kode\" = '00003' ");
		// $result = $rcd::getFreeSQL(" WHERE \"debtor_ref\" = '".$val['tmuk']['nama']."' ");
		
		$data = null;
		if(isset($lists)){
			foreach($lists as $list){				
				$data[$list['jenis_kustomer_id']] = $list['jenis'];
				// $data = $list['jenis'];
			}
		}
		return $data;
	}
	
}

?>