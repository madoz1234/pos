<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class XY extends Model
{

	public $kode_produk_x;
	public $harga_jual_x;
	public $diskon_x;
	public $harga_diskon_x;	
	public $kode_produk_y;
	public $harga_jual_y;
	public $diskon_y;
	public $harga_diskon_y;
	public $promosi_id;
	public $flag;

	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"promosi_id\",\"kode_produk_x\", \"harga_jual_x\", \"diskon_x\", \"harga_diskon_x\", \"kode_produk_y\", \"harga_jual_y\", \"diskon_y\", \"harga_diskon_y\", \"flag\" "
				." FROM \"m_promo_x_get_y\" "
				." WHERE \"flag_delete\" = false ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['promosi_id'] = $result[0];
			$lists[$count]['kode_produk_x'] = $result[1];
			$lists[$count]['harga_jual_x'] = $result[2];
			$lists[$count]['diskon_x'] = $result[3];
			$lists[$count]['harga_diskon_x'] = $result[4];
			$lists[$count]['kode_produk_y'] = $result[5];
			$lists[$count]['harga_jual_y'] = $result[6];
			$lists[$count]['diskon_y'] = $result[7];
			$lists[$count]['harga_diskon_y'] = $result[8];
			$lists[$count]['flag'] = $result[9];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"promosi_id\",\"kode_produk_x\", \"harga_jual_x\", \"diskon_x\", \"harga_diskon_x\", \"kode_produk_y\", \"harga_jual_y\", \"diskon_y\", \"harga_diskon_y\", \"flag\" "
				." FROM \"m_promo_x_get_y\" "
				." WHERE \"kode_produk_x\" = '".$object->kode_produk_x."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['promosi_id'] = $result[0];
			$lists[$count]['kode_produk_x'] = $result[1];
			$lists[$count]['harga_jual_x'] = $result[2];
			$lists[$count]['diskon_x'] = $result[3];
			$lists[$count]['harga_diskon_x'] = $result[4];
			$lists[$count]['kode_produk_y'] = $result[5];
			$lists[$count]['harga_jual_y'] = $result[6];
			$lists[$count]['diskon_y'] = $result[7];
			$lists[$count]['harga_diskon_y'] = $result[8];
			$lists[$count]['flag'] = $result[9];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"promosi_id\",\"kode_produk_x\", \"harga_jual_x\", \"diskon_x\", \"harga_diskon_x\", \"kode_produk_y\", \"harga_jual_y\", \"diskon_y\", \"harga_diskon_y\", \"flag\" "
				." FROM \"m_promo_x_get_y\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['promosi_id'] = $result[0];
			$lists[$count]['kode_produk_x'] = $result[1];
			$lists[$count]['harga_jual_x'] = $result[2];
			$lists[$count]['diskon_x'] = $result[3];
			$lists[$count]['harga_diskon_x'] = $result[4];
			$lists[$count]['kode_produk_y'] = $result[5];
			$lists[$count]['harga_jual_y'] = $result[6];
			$lists[$count]['diskon_y'] = $result[7];
			$lists[$count]['harga_diskon_y'] = $result[8];
			$lists[$count]['flag'] = $result[9];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"kode_produk_x\") "
				." FROM \"m_promo_x_get_y\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->diskon_x == '') { $object->diskon_x = date("Y-m-d"); }
		if($object->harga_diskon_y == '') { $object->harga_diskon_y = 'f'; }
		
		$sql = "INSERT INTO \"m_promo_x_get_y\" (\"promosi_id\",\"kode_produk_x\", \"harga_jual_x\", \"diskon_x\", \"harga_diskon_x\", \"kode_produk_y\", \"harga_jual_y\", \"diskon_y\", \"harga_diskon_y\", \"flag\") "
				." VALUES ('".$object->promosi_id."','".$object->kode_produk_x."','".$object->harga_jual_x."','".$object->diskon_x."','".$object->harga_diskon_x."','".$object->kode_produk_y."','"
				.$object->harga_jual_y."','".$object->diskon_y."','".$object->harga_diskon_y."','".$object->flag."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_promo_x_get_y\" SET ";
		$flag = false;
		if($object->harga_jual_x != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_jual_x\" = '".$object->harga_jual_x."' "; $flag = true; }
		if($object->diskon_x != '') { if($flag){ $sql .= ","; } $sql .= " \"diskon_x\" = '".$object->diskon_x."' "; $flag = true; }
		if($object->harga_diskon_x != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_diskon_x\" = '".$object->harga_diskon_x."' "; $flag = true; }
		if($object->kode_produk_y != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_produk_y\" = '".$object->kode_produk_y."' "; $flag = true; }
		if($object->harga_jual_y != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_jual_y\" = '".$object->harga_jual_y."' "; $flag = true; }
		if($object->diskon_y != '') { if($flag){ $sql .= ","; } $sql .= " \"diskon_y\" = '".$object->diskon_y."' "; $flag = true; }
		if($object->harga_diskon_y != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_diskon_y\" = '".$object->harga_diskon_y."' "; $flag = true; }
		if($object->promosi_id != '') { if($flag){ $sql .= ","; } $sql .= " \"promosi_id\" = '".$object->promosi_id."' "; $flag = true; }
		if($object->flag != '') { if($flag){ $sql .= ","; } $sql .= " \"flag\" = '".$object->flag."' "; $flag = true; }
		$sql .= " WHERE \"promosi_id\" = '".$object->promosi_id."' AND \"kode_produk_x\" = '".$object->kode_produk_x."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}

	public function goUpdate2($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_promo_x_get_y\" SET ";
		$flag = false;
		if($object->harga_jual_x != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_jual_x\" = '".$object->harga_jual_x."' "; $flag = true; }
		if($object->diskon_x != '') { if($flag){ $sql .= ","; } $sql .= " \"diskon_x\" = '".$object->diskon_x."' "; $flag = true; }
		if($object->harga_diskon_x != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_diskon_x\" = '".$object->harga_diskon_x."' "; $flag = true; }
		if($object->kode_produk_y != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_produk_y\" = '".$object->kode_produk_y."' "; $flag = true; }
		if($object->harga_jual_y != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_jual_y\" = '".$object->harga_jual_y."' "; $flag = true; }
		if($object->diskon_y != '') { if($flag){ $sql .= ","; } $sql .= " \"diskon_y\" = '".$object->diskon_y."' "; $flag = true; }
		if($object->harga_diskon_y != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_diskon_y\" = '".$object->harga_diskon_y."' "; $flag = true; }
		if($object->promosi_id != '') { if($flag){ $sql .= ","; } $sql .= " \"promosi_id\" = '".$object->promosi_id."' "; $flag = true; }
		if($object->flag != '') { if($flag){ $sql .= ","; } $sql .= " \"flag\" = '".$object->flag."' "; $flag = true; }
		$sql .= " WHERE \"promosi_id\" = '".$object->promosi_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_promo_x_get_y\" "
				." WHERE \"promosi_id\" = '".$object->promosi_id."' ";
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function getJoin_AdjDetail($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT a.\"kode_produk_x\", \"harga_jual_x\", \"diskon_x\", \"harga_diskon_x\", \"kode_produk_y\", \"harga_jual_y\", \"diskon_y\", \"harga_diskon_y\", \"flag_delete\", \"item_code\", \"description\", \"qty\", \"unit\", \"unit_price\", \"total_price\", \"barcode_code\" "
			." FROM \"m_promo_x_get_y\" a INNER JOIN \"m_promo_x_get_y_detail\" ad ON a.kode_produk_x = ad.kode_produk_x ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['kode_produk_x'] = $result[0];
			$lists[$count]['harga_jual_x'] = $result[1];
			$lists[$count]['diskon_x'] = $result[2];
			$lists[$count]['harga_diskon_x'] = $result[3];
			$lists[$count]['kode_produk_y'] = $result[4];
			$lists[$count]['harga_jual_y'] = $result[5];
			$lists[$count]['diskon_y'] = $result[6];
			$lists[$count]['harga_diskon_y'] = $result[7];
			$lists[$count]['flag_delete'] = $result[8];
			$lists[$count]['item_code'] = $result[9];
			$lists[$count]['description'] = $result[10];
			$lists[$count]['qty'] = $result[11];
			$lists[$count]['unit'] = $result[12];
			$lists[$count]['unit_price'] = $result[13];
			$lists[$count]['total_price'] = $result[14];
			$lists[$count]['barcode_code'] = $result[15];
			
			$count++;
		}
		
		return $lists;
	}
}
