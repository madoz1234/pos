<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class JenisKustomer extends Model
{

	public $id;
	public $kode;
	public $jenis;
	public $jenis_kustomer_id;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"id\", \"kode\", \"jenis\", \"jenis_kustomer_id\" FROM \"m_jenis_kustomer\" ";
		
		$results = $connection->query($sql);
		// dd($results);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['kode'] = $result[1];
			$lists[$count]['jenis'] = $result[2];
			$lists[$count]['jenis_kustomer_id'] = $result[3];
			
			$count++;
		}
		
		return $lists;
	}

	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"id\", \"kode\", \"jenis\", \"jenis_kustomer_id\" FROM \"m_jenis_kustomer\" WHERE \"id\" = '".$object->id."' LIMIT 1 " ;

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['kode'] = $result[1];
			$lists[$count]['jenis'] = $result[2];
			$lists[$count]['jenis_kustomer_id'] = $result[3];
			
			$count++;
		}
		
		return $lists;
	}

	public function getFreeSQL($condition){
		// dd($condition);
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"id\", \"kode\", \"jenis\", \"jenis_kustomer_id\" FROM \"m_jenis_kustomer\" ".$condition;
		// dd($sql);
		$results = $connection->query($sql);
		// dd($results);
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['kode'] = $result[1];
			$lists[$count]['jenis'] = $result[2];
			$lists[$count]['jenis_kustomer_id'] = $result[3];
			
			$count++;
		}
		
		return $lists;
	}

	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"id\") "
				." FROM \"m_jenis_kustomer\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}

	// public function goInsert($object){
	// 	$connection = new Postgresql($this->di['db']);	
		
	// 	$sql = "INSERT INTO \"m_jenis_kustomer\" (\"kode\", \"jenis\", \"jenis_kustomer_id\")) "
	// 			." VALUES ('".$object->kode."','".$object->jenis."','".$object->jenis_kustomer_id."') ";
	// 	// dd($sql);
	// 	$success = $connection->execute($sql);
		
	// 	return $success;
	// }

	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);	
		
		$sql = "INSERT INTO \"m_jenis_kustomer\" (\"kode\", \"jenis\", \"jenis_kustomer_id\") "
				." VALUES ('".$object->kode."','".$object->jenis."','".$object->jenis_kustomer_id."') ";
		
		$success = $connection->execute($sql);		
		
		return $sql;
	}


	public function goUpdate($object){

		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_jenis_kustomer\" SET ";
		$flag = false;		
		if($object->jenis != '') { if($flag){ $sql .= ","; } $sql .= " \"jenis\" = '".$object->jenis."' "; $flag = true; }
		if($object->jenis_kustomer_id != '') { if($flag){ $sql .= ","; } $sql .= " \"jenis_kustomer_id\" = '".$object->jenis_kustomer_id."' "; $flag = true; }

		$sql .= " WHERE \"kode\" = '".$object->kode."' ";			

		$success = $connection->execute($sql);
		
		return $success;
	}


	public function goUpdateSync($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_jenis_kustomer\" SET ";
		$flag = false;				
		if($object->jenis != '') { if($flag){ $sql .= ","; } $sql .= " \"jenis\" = '".$object->jenis."' "; $flag = true; }
		if($object->jenis_kustomer_id != '') { if($flag){ $sql .= ","; } $sql .= " \"jenis_kustomer_id\" = '".$object->jenis_kustomer_id."' "; $flag = true; }
		$sql .= " WHERE \"kode\" = '".$object->kode."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
}