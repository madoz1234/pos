<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class JualDetail extends Model
{

	public $id_jual_item;
	public $id_jual;
	public $item_code;
	public $barcode;
	public $description;	
	public $harga;
	public $harga_member;	
	public $qty;	
	public $satuan;	
	public $discount;
	public $disc_percent;	
	public $disc_amount;	
	public $total;	
	public $promo;	
	public $tax_type;
	public $harga_hpp3;
	public $tanggal;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id_jual_item\", \"id_jual\", \"item_code\", \"description\", \"harga\", \"harga_member\", \"qty\", \"satuan\", \"discount\", \"total\" "
		." , \"promo\", \"barcode\", \"disc_percent\", \"disc_amount\", \"tax_type\", \"harga_hpp3\", \"tanggal\" "
		." FROM \"t_jual_detail\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_jual_item'] = $result[0];
			$lists[$count]['id_jual'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['harga'] = $result[4];			
			$lists[$count]['harga_member'] = $result[5];	
			$lists[$count]['qty'] = $result[6];	
			$lists[$count]['satuan'] = $result[7];	
			$lists[$count]['discount'] = $result[8];	
			$lists[$count]['total'] = $result[9];	
			$lists[$count]['promo'] = $result[10];
			$lists[$count]['barcode'] = $result[11];			
			$lists[$count]['disc_percent'] = $result[12];			
			$lists[$count]['disc_amount'] = $result[13];
			$lists[$count]['tax_type'] = $result[14];
			$lists[$count]['harga_hpp3'] = $result[15];
			$lists[$count]['tanggal'] = $result[16];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id_jual_item\", \"id_jual\", \"item_code\", \"description\", \"harga\", \"harga_member\", \"qty\", \"satuan\", \"discount\", \"total\" "
		." , \"promo\", \"barcode\", \"disc_percent\", \"disc_amount\", \"tax_type\", \"harga_hpp3\", \"tanggal\" "
		." FROM \"t_jual_detail\" "
		." WHERE \"id_jual_item\" = '".$object->id_jual_item."' "
		." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_jual_item'] = $result[0];
			$lists[$count]['id_jual'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['harga'] = $result[4];			
			$lists[$count]['harga_member'] = $result[5];	
			$lists[$count]['qty'] = $result[6];	
			$lists[$count]['satuan'] = $result[7];	
			$lists[$count]['discount'] = $result[8];	
			$lists[$count]['total'] = $result[9];	
			$lists[$count]['promo'] = $result[10];
			$lists[$count]['barcode'] = $result[11];			
			$lists[$count]['disc_percent'] = $result[12];
			$lists[$count]['disc_amount'] = $result[13];
			$lists[$count]['tax_type'] = $result[14];
			$lists[$count]['harga_hpp3'] = $result[15];
			$lists[$count]['tanggal'] = $result[16];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id_jual_item\", \"id_jual\", \"item_code\", \"description\", \"harga\", \"harga_member\", \"qty\", \"satuan\", \"discount\", \"total\" "
		." , \"promo\", \"barcode\", \"disc_percent\", \"disc_amount\", \"tax_type\", \"harga_hpp3\", \"tanggal\" "
		." FROM \"t_jual_detail\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_jual_item'] = $result[0];
			$lists[$count]['id_jual'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['harga'] = $result[4];			
			$lists[$count]['harga_member'] = $result[5];	
			$lists[$count]['qty'] = $result[6];	
			$lists[$count]['satuan'] = $result[7];	
			$lists[$count]['discount'] = $result[8];	
			$lists[$count]['total'] = $result[9];	
			$lists[$count]['promo'] = $result[10];
			$lists[$count]['barcode'] = $result[11];
			$lists[$count]['disc_percent'] = $result[12];
			$lists[$count]['disc_amount'] = $result[13];
			$lists[$count]['tax_type'] = $result[14];
			$lists[$count]['harga_hpp3'] = $result[15];
			$lists[$count]['tanggal'] = $result[16];
			
			$count++;
		}
		
		return $lists;
	}

	public function getFreeSQL2($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"harga_hpp3\", \"harga\", \"discount\", \"total\" "
		." FROM \"t_jual_detail\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['harga_hpp3'] = $result[0];
			$lists[$count]['harga'] = $result[1];
			$lists[$count]['discount'] = $result[2];
			$lists[$count]['total'] = $result[3];
			$count++;
		}
		
		return $lists;
	}

	public function getTotalDiskon($tgl)
	{
		$connection = new Postgresql($this->di['db']);

		$sql = "
			SELECT sum(jd.discount) as total
			FROM t_jual j
			JOIN t_jual_detail jd ON jd.id_jual = j.id_jual
			WHERE j.tanggal between '".$tgl.' 00:00:00'."' and '".$tgl.' 23:59:59'."' ";	
		
		$results = $connection->query($sql);

		$lists = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists = $result[0];
		}
		return $lists;
	}

	public function getTotalDiskonCetak($from, $to)
	{
		$connection = new Postgresql($this->di['db']);

		$sql = "
			SELECT sum(jd.discount) as total
			FROM t_jual j
			JOIN t_jual_detail jd ON jd.id_jual = j.id_jual
			WHERE j.tanggal between '".$from.' 00:00:00'."' and '".$to.' 23:59:59'."' ";	
		
		$results = $connection->query($sql);

		$lists = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists = $result[0];
		}
		return $lists;
	}

	public function getTotalMember($tgl)
	{
		$connection = new Postgresql($this->di['db']);

		$sql = "
			SELECT sum(jd.harga_member) as total
			FROM t_jual j
			JOIN t_jual_detail jd ON jd.id_jual = j.id_jual
			WHERE j.tanggal between '".$tgl.' 00:00:00'."' and '".$tgl.' 23:59:59'."' ";	
		
		$results = $connection->query($sql);

		$lists = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists = $result[0];
		}
		return $lists;
	}

	public function getTotalMemberCetak($from, $to)
	{
		$connection = new Postgresql($this->di['db']);

		$sql = "
			SELECT sum(jd.harga_member) as total
			FROM t_jual j
			JOIN t_jual_detail jd ON jd.id_jual = j.id_jual
			WHERE j.tanggal between '".$from.' 00:00:00'."' and '".$to.' 23:59:59'."' ";	
		
		$results = $connection->query($sql);

		$lists = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists = $result[0];
		}
		return $lists;
	}

	public function getTotal($tgl)
	{
		$connection = new Postgresql($this->di['db']);

		$sql = "
			SELECT sum(jd.harga * jd.qty) as total
			FROM t_jual j
			JOIN t_jual_detail jd ON jd.id_jual = j.id_jual
			WHERE j.tanggal between '".$tgl.' 00:00:00'."' and '".$tgl.' 23:59:59'."' ";	
		
		$results = $connection->query($sql);

		$lists = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists = $result[0];
		}
		return $lists;
	}

	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"id_jual_item\") "
		." FROM \"t_jual_detail\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->harga == '') { $object->harga = '0'; }
		if($object->harga_member == '') { $object->harga_member = '0'; }		
		if($object->qty == '') { $object->qty = '0'; }
		if($object->discount == '') { $object->discount = '0'; }
		if($object->total == '') { $object->total = '0'; }
		if($object->disc_percent == '') { $object->disc_percent = '0'; }
		if($object->disc_amount == '') { $object->disc_amount = '0'; }
		if($object->tax_type == '') { $object->tax_type = '1'; }
		if($object->harga_hpp3 == '') { $object->harga_hpp3 = '0'; }
		
		$sql = "INSERT INTO \"t_jual_detail\" (\"id_jual\", \"item_code\", \"description\", \"harga\", \"harga_member\", \"qty\", \"satuan\", \"discount\", \"total\" "
		." , \"promo\", \"barcode\", \"disc_percent\", \"disc_amount\", \"tax_type\", \"harga_hpp3\", \"tanggal\") "
		." VALUES ('".$object->id_jual."','".$object->item_code."','".pg_escape_string($object->description)."','".$object->harga."','".$object->harga_member."','"
		.$object->qty."','".$object->satuan."','".$object->discount."','".$object->total."','".$object->promo."','".$object->barcode."','"
		.$object->disc_percent."','".$object->disc_amount."','".$object->tax_type."','".$object->harga_hpp3."','".$object->tanggal."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}

	public function goInsert2($object){
		if($object->harga == '') { $object->harga = '0'; }
		if($object->harga_member == '') { $object->harga_member = '0'; }		
		if($object->qty == '') { $object->qty = '0'; }
		if($object->discount == '') { $object->discount = '0'; }
		if($object->total == '') { $object->total = '0'; }
		if($object->disc_percent == '') { $object->disc_percent = '0'; }
		if($object->disc_amount == '') { $object->disc_amount = '0'; }
		if($object->tax_type == '') { $object->tax_type = '1'; }
		if($object->harga_hpp3 == '') { $object->harga_hpp3 = '0'; }
		
		$sql = "INSERT INTO \"t_jual_detail\" (\"id_jual\", \"item_code\", \"description\", \"harga\", \"harga_member\", \"qty\", \"satuan\", \"discount\", \"total\" "
		." , \"promo\", \"barcode\", \"disc_percent\", \"disc_amount\", \"tax_type\", \"harga_hpp3\", \"tanggal\") "
		." VALUES ('".$object->id_jual."','".$object->item_code."','".pg_escape_string($object->description)."','".$object->harga."','".$object->harga_member."','"
		.$object->qty."','".$object->satuan."','".$object->discount."','".$object->total."','".$object->promo."','".$object->barcode."','"
		.$object->disc_percent."','".$object->disc_amount."','".$object->tax_type."','".$object->harga_hpp3."','".$object->tanggal."'); ";
		
		return $sql;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_jual_detail\" SET ";
		$flag = false;
		if($object->item_code != '') { if($flag){ $sql .= ","; } $sql .= " \"item_code\" = '".$object->item_code."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".pg_escape_string($object->description)."' "; $flag = true; }
		if($object->harga != '') { if($flag){ $sql .= ","; } $sql .= " \"harga\" = '".$object->harga."' "; $flag = true; }
		if($object->harga_member != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_member\" = '".$object->harga_member."' "; $flag = true; }
		if($object->qty != '') { if($flag){ $sql .= ","; } $sql .= " \"qty\" = '".$object->qty."' "; $flag = true; }
		if($object->satuan != '') { if($flag){ $sql .= ","; } $sql .= " \"satuan\" = '".$object->satuan."' "; $flag = true; }
		if($object->discount != '') { if($flag){ $sql .= ","; } $sql .= " \"discount\" = '".$object->discount."' "; $flag = true; }
		if($object->total != '') { if($flag){ $sql .= ","; } $sql .= " \"total\" = '".$object->total."' "; $flag = true; }
		if($object->promo != '') { if($flag){ $sql .= ","; } $sql .= " \"promo\" = '".$object->promo."' "; $flag = true; }			
		if($object->barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"barcode\" = '".$object->barcode."' "; $flag = true; }		
		if($object->disc_percent != '') { if($flag){ $sql .= ","; } $sql .= " \"disc_percent\" = '".$object->disc_percent."' "; $flag = true; }		
		if($object->disc_amount != '') { if($flag){ $sql .= ","; } $sql .= " \"disc_amount\" = '".$object->disc_amount."' "; $flag = true; }		
		if($object->tax_type != '') { if($flag){ $sql .= ","; } $sql .= " \"tax_type\" = '".$object->tax_type."' "; $flag = true; }		
		if($object->harga_hpp3 != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_hpp3\" = '".$object->harga_hpp3."' "; $flag = true; }		
		if($object->tanggal != '') { if($flag){ $sql .= ","; } $sql .= " \"tanggal\" = '".$object->tanggal."' "; $flag = true; }		
		$sql .= " WHERE \"id_jual_item\" = '".$object->id_jual_item."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goUpdateJual($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_jual_detail\" SET ";
		$flag = false;
		if($object->item_code != '') { if($flag){ $sql .= ","; } $sql .= " \"item_code\" = '".$object->item_code."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".pg_escape_string($object->description)."' "; $flag = true; }
		if($object->harga != '') { if($flag){ $sql .= ","; } $sql .= " \"harga\" = '".$object->harga."' "; $flag = true; }
		if($object->harga_member != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_member\" = '".$object->harga_member."' "; $flag = true; }
		if($object->qty != '') { if($flag){ $sql .= ","; } $sql .= " \"qty\" = '".$object->qty."' "; $flag = true; }
		if($object->satuan != '') { if($flag){ $sql .= ","; } $sql .= " \"satuan\" = '".$object->satuan."' "; $flag = true; }
		if($object->discount != '') { if($flag){ $sql .= ","; } $sql .= " \"discount\" = '".$object->discount."' "; $flag = true; }
		if($object->total != '') { if($flag){ $sql .= ","; } $sql .= " \"total\" = '".$object->total."' "; $flag = true; }
		if($object->promo != '') { if($flag){ $sql .= ","; } $sql .= " \"promo\" = '".$object->promo."' "; $flag = true; }
		if($object->barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"barcode\" = '".$object->barcode."' "; $flag = true; }
		if($object->disc_percent != '') { if($flag){ $sql .= ","; } $sql .= " \"disc_percent\" = '".$object->disc_percent."' "; $flag = true; }
		if($object->disc_amount != '') { if($flag){ $sql .= ","; } $sql .= " \"disc_amount\" = '".$object->disc_amount."' "; $flag = true; }
		if($object->tax_type != '') { if($flag){ $sql .= ","; } $sql .= " \"tax_type\" = '".$object->tax_type."' "; $flag = true; }		
		if($object->harga_hpp3 != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_hpp3\" = '".$object->harga_hpp3."' "; $flag = true; }		
		if($object->tanggal != '') { if($flag){ $sql .= ","; } $sql .= " \"tanggal\" = '".$object->tanggal."' "; $flag = true; }		
		$sql .= " WHERE \"id_jual\" = '".$object->id_jual."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);

		$sql = " DELETE FROM \"t_jual_detail\" "
		." WHERE \"id_jual_item\" = '".$object->id_jual_item."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goDeleteJual($object){
		$connection = new Postgresql($this->di['db']);

		$sql = " DELETE FROM \"t_jual_detail\" "
		." WHERE \"id_jual\" = '".$object->id_jual."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function summarize($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"item_code\", \"description\", \"harga\", \"harga_member\", SUM(\"qty\") as qty_sum, SUM(tjd.\"discount\") as discount_sum, SUM(tjd.\"total\") as total_sum, tj.\"tanggal\", id_kasir, kassa_id, shift_id, harga_hpp3 "
		." FROM \"t_jual_detail\" tjd INNER JOIN \"t_jual\" tj ON tjd.id_jual = tj.id_jual ".$condition;
		$results = $connection->query($sql);
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['item_code'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$lists[$count]['harga'] = $result[2];
			$lists[$count]['harga_member'] = $result[3];
			$lists[$count]['qty_sum'] = $result[4];			
			$lists[$count]['discount_sum'] = $result[5];	
			$lists[$count]['total_sum'] = $result[6];	
			$lists[$count]['tanggal'] = $result[7];	
			$lists[$count]['id_kasir'] = $result[8];	
			$lists[$count]['kassa_id'] = $result[9];	
			$lists[$count]['shift_id'] = $result[10];	
			$lists[$count]['harga_hpp3'] = $result[11];		
			$count++;
		}
		
		return $lists;
	}
	
	public function get_dashboard1($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT SUM(total) as total, DATE(tanggal) as tanggal "
		." FROM \"t_jual\" ".$condition
		." GROUP BY DATE(tanggal) ORDER BY DATE(tanggal) ";
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['total'] = $result[0];
			$lists[$count]['waktu'] = date('d-m-Y', strtotime($result[1]));
			$count++;
		}
		
		return $lists;
	}
	
	public function get_dashboard1_alt($date){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT SUM(total), jam FROM t_jual WHERE DATE(tanggal) = '".$date."' GROUP BY jam ";

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['total'] = $result[0];
			$lists[$count]['waktu'] = $result[1].'.00';
			$count++;
		}
		
		return $lists;
	}
	
	public function get_dashboard2($condition){
		$connection = new Postgresql($this->di['db']);
		
		$month_ago = date('m', strtotime('-1 month'));
		$month_now = date('m');
		
		$sql = " SELECT SUM(total) as total, EXTRACT(MONTH FROM tanggal) as month "
		." FROM \"t_jual_sum\" WHERE EXTRACT(MONTH FROM tanggal) = ".$month_ago." GROUP BY month UNION "

		." SELECT SUM(total) as total, EXTRACT(MONTH FROM tanggal) as month "
		." FROM \"t_jual_sum\" WHERE EXTRACT(MONTH FROM tanggal) = ".$month_now." GROUP BY month ORDER BY month ";

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['total'] = $result[0];
			$lists[$count]['month'] = $result[1];
			$count++;
		}
		
		return $lists;
	}
	
	public function get_dash3dash4($condit){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT sum(jd.total) as total, COUNT(DISTINCT(j.id_jual)) as struk
			FROM t_jual j
			JOIN t_jual_detail jd ON jd.id_jual = j.id_jual ".$condit;

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['total_sales'] = $result[0];
			$lists[$count]['struk'] = $result[1];
			$count++;
		}
		
		return $lists;
	}
	
	public function get_dashboard5($condi){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT SUM(qty), mp.description "
		." FROM \"t_jual\" tj INNER JOIN \"t_jual_detail\" tjd ON tj.id_jual = tjd.id_jual "
		." INNER JOIN \"m_product\" mp ON tjd.item_code = mp.stock_id ".$condi
		." GROUP BY mp.stock_id "
		." ORDER BY SUM(qty) DESC LIMIT 5 ";
			// dd($sql);

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['jumlah'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$count++;
		}
		
		return $lists;
	}
	
	public function get_dashboard6($condi){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT COUNT(*) as jumlah, l1_nm "
		." FROM \"t_jual\" tj INNER JOIN \"t_jual_detail\" tjd ON tj.id_jual = tjd.id_jual "
		." INNER JOIN \"m_product\" mp ON tjd.item_code = mp.stock_id "
		." INNER JOIN \"m_category\" mc ON mp.l1_cd = mc.l1_cd ".$condi
		." GROUP BY l1_nm "
		." ORDER BY jumlah DESC LIMIT 3 ";

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['jumlah'] = $result[0];
			$lists[$count]['l1_nm'] = $result[1];
			$count++;
		}
		
		return $lists;
	}
	
	public function get_dashboard7($condi){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT MAX(harga*qty) as jumlah, mp.description "
		." FROM \"t_jual\" tj INNER JOIN \"t_jual_detail\" tjd ON tj.id_jual = tjd.id_jual "
		." INNER JOIN \"m_product\" mp ON tjd.item_code = mp.stock_id ".$condi
		." GROUP BY mp.description "
		." ORDER BY jumlah DESC LIMIT 5 ";

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['jumlah'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$count++;
		}
		
		return $lists;
	}
	
	public function get_dashboard8($condition){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT count(*) FROM t_pr ".$condition." AND flag_sync = false AND flag_delete = false AND status = 0 " ;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];
		}
		
		return $count;
	}
	
	public function get_dashboard8PR($condition){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT request_no, total FROM t_pr ".$condition." AND flag_sync = false AND flag_delete = false AND status = 0 " ;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['request_no'] = $result[0];
			$lists[$count]['total'] = $result[1];
			$count++;
		}
		
		return $lists;
	}
	
	public function get_dashboard9($condition){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT count(*) FROM t_po ".$condition." AND gr_no_immpos = '' " ;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];
		}
		
		return $count;
	}
	
	public function get_dashboard9PO($condition){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT po_no_ref, order_date FROM t_po ".$condition." AND gr_no_immpos = '' ORDER BY order_date " ;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['po_no_ref'] = $result[0];
			$lists[$count]['order_date'] = $result[1];
			$count++;
		}
		
		return $lists;
	}
	
	public function get_dashboard10(){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT so_id, so_date FROM t_so WHERE status = 'COMPLETE' AND so_type = 'MONTH' ORDER BY so_date DESC LIMIT 3 " ;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['so_id'] = $result[0];
			$lists[$count]['so_date'] = date('d-m-Y', strtotime($result[1]));
			$count++;
		}
		
		return $lists;
	}

	public function get_product_move(){
		$connection = new Postgresql($this->di['db']);
		$sql = "SELECT * FROM m_product_move WHERE type = '' ORDER BY tran_date ASC ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$sql2 = "SELECT * FROM m_product_move WHERE type != '' ORDER BY tran_date ASC ";
			$results2 = $connection->query($sql2);
			$results2->setFetchMode(Phalcon\Db::FETCH_NUM);

			$status = false;
			while ($result2 = $results2->fetchArray()) {
				if($result[2] == $result2[2] && $status==false){
					$status = true;
				}
			}

			if(!$status){
				$lists[$count]['stock_id'] = $result[2];
				$lists[$count]['qty'] = $result[9];
				$count++;
			}
		}
		
		return $lists;
	}

	public function get_product_name($id){
		$id = strval($id);
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT * FROM m_product WHERE stock_id = '".$id."' ";
		
		// dd($sql);

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = '';
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists = $result[3];
		}
		
		return $lists;
	}
	
	public function get_dashboardSO($condition){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT sum(qty_tersedia), sum(qty_barcode), (sum(qty_tersedia) + sum(qty_barcode)) FROM t_so_detail ".$condition ;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['qty_tersedia'] = $result[0];
			$lists[$count]['qty_barcode'] = $result[1];
			$lists[$count]['selisih'] = $result[2];
			$count++;
		}
		
		return $lists;
	}
	
	public function get_dashboard11($condit){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT sum(qty) as stok, b.description FROM m_product_move a FULL JOIN m_product b ON a.stock_id=b.stock_id
		WHERE a.stock_id IN (SELECT stock_id FROM m_product WHERE stock_id NOT IN 
		( SELECT item_code FROM t_jual_detail a INNER JOIN t_jual b ON a.id_jual=b.id_jual ".$condit." )) GROUP BY b.description ORDER BY stok DESC LIMIT 5 ";
		
		//echo $sql;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_qty'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$count++;
		}
		
		return $lists;
	}
	
	public function getJoin_Product($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT tjd.\"id_jual_item\", tjd.\"id_jual\", tjd.\"item_code\", tjd.\"description\", tjd.\"harga\", tjd.\"harga_member\", tjd.\"qty\", tjd.\"satuan\", tjd.\"discount\", tjd.\"total\" "
		." , tjd.\"promo\", tjd.\"barcode\", tjd.\"disc_percent\", tjd.\"disc_amount\", tjd.\"tax_type\", tjd.\"harga_hpp3\", mp.\"ppob\", tjd.\"tanggal\" "
		." FROM \"t_jual_detail\" tjd INNER JOIN \"m_product\" mp ON tjd.item_code = mp.stock_id ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id_jual_item'] = $result[0];
			$lists[$count]['id_jual'] = $result[1];
			$lists[$count]['item_code'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['harga'] = $result[4];			
			$lists[$count]['harga_member'] = $result[5];	
			$lists[$count]['qty'] = $result[6];	
			$lists[$count]['satuan'] = $result[7];	
			$lists[$count]['discount'] = $result[8];	
			$lists[$count]['total'] = $result[9];	
			$lists[$count]['promo'] = $result[10];
			$lists[$count]['barcode'] = $result[11];
			$lists[$count]['disc_percent'] = $result[12];
			$lists[$count]['disc_amount'] = $result[13];
			$lists[$count]['tax_type'] = $result[14];
			$lists[$count]['harga_hpp3'] = $result[15];
			$lists[$count]['ppob'] = $result[16];
			$lists[$count]['tanggal'] = $result[17];
			
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_Product2($cond){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT tjd.\"description\", tjd.\"qty\", tjd.\"total\", mp.\"jenis\", tjd.\"harga_member\", tjd.\"harga\", tjd.\"discount\", tjd.\"item_code\" "
		." FROM \"t_jual_detail\" tjd INNER JOIN \"m_product\" mp ON tjd.item_code = mp.stock_id ".$cond;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['description'] = $result[0];
			$lists[$count]['qty'] = $result[1];
			$lists[$count]['total'] = $result[2];
			$lists[$count]['jenis'] = $result[3];
			$lists[$count]['harga_member'] = $result[4];
			$lists[$count]['harga'] = $result[5];
			$lists[$count]['discount'] = $result[6];
			$lists[$count]['item_code'] = $result[7];
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_Product3($condi){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT tjd.\"item_code\", tjd.\"description\", tjd.\"qty\", mpp.\"uom1_rounding_price\" "
		." FROM \"t_jual_detail\" tjd INNER JOIN \"m_product\" mp ON tjd.item_code = mp.stock_id "
		." INNER JOIN m_product_price mpp ON tjd.item_code = mpp.stock_id ".$condi;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['item_code'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$lists[$count]['qty'] = $result[2];
			$lists[$count]['rounding'] = $result[3];
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_Product100($val){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT tjd.\"item_code\", tjd.\"barcode\", tjd.\"description\", tjd.\"harga\", tjd.\"qty\", tjd.\"discount\", tjd.\"total\", tjd.\"harga_hpp3\", mpp.\"uom1_rounding_price\" "
		." FROM \"t_jual_detail\" tjd INNER JOIN \"m_product\" mp ON tjd.item_code = mp.stock_id "
		." INNER JOIN m_product_price mpp ON tjd.item_code = mpp.stock_id  AND id_jual ='".$val."'";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['item_code'] = $result[0];
			$lists[$count]['barcode'] = $result[1];
			$lists[$count]['description'] = $result[2];
			$lists[$count]['harga'] = $result[3];
			$lists[$count]['qty'] = $result[4];
			$lists[$count]['discount'] = $result[5];
			$lists[$count]['total'] = $result[6];
			$lists[$count]['harga_hpp3'] = $result[7];
			$count++;
		}
		
		return $lists;
	}
}
