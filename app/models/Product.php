<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Product extends Model
{
	public $stock_id;
	public $category_id;
	public $tax_type_id;
	public $description;
	public $long_description;
	public $units;
	public $actual_cost;
	public $last_cost;
	public $material_cost;
	public $labour_cost;
	public $overhead_cost;
	public $inactive;
	public $no_sale;
	public $editable;
	public $jenis_item;
	public $bumun_cd;
	public $l1_cd;
	public $l2_cd;
	public $l3_cd;
	public $l4_cd;
	public $margin;
	public $moving_type;
	public $ass_type_1;
	public $ass_type_2;
	public $ass_type_3;
	public $expiry_product;
	public $weight_product;
	public $batch_product;
	public $active_status;
	public $kode_1;
	public $kode_2;
	public $kode_3;
	public $discount;
	public $supplied_by;
	public $uom1_nm;
	public $uom1_htype_1;
	public $uom1_htype_2;
	public $uom1_htype_3;
	public $uom1_htype_4;
	public $uom1_htype_5;
	public $uom1_rack_type;
	public $uom1_shelfing_rack_type;
	public $uom1_conversion;
	public $uom1_internal_barcode;
	public $uom1_width;
	public $uom1_length;
	public $uom1_height;
	public $uom1_weight;
	public $uom1_margin;
	public $uom1_box_type;
	public $uom1_harga;
	public $uom2_nm;
	public $uom2_htype_1;
	public $uom2_htype_2;
	public $uom2_htype_3;
	public $uom2_htype_4;
	public $uom2_htype_5;
	public $uom2_rack_type;
	public $uom2_shelfing_rack_type;
	public $uom2_conversion;
	public $uom2_internal_barcode;
	public $uom2_width;
	public $uom2_length;
	public $uom2_height;
	public $uom2_weight;
	public $uom2_margin;
	public $uom2_box_type;
	public $uom2_harga;
	public $uom3_nm;
	public $uom3_htype_1;
	public $uom3_htype_2;
	public $uom3_htype_3;
	public $uom3_htype_4;
	public $uom3_htype_5;
	public $uom3_rack_type;
	public $uom3_shelfing_rack_type;
	public $uom3_conversion;
	public $uom3_internal_barcode;
	public $uom3_width;
	public $uom3_length;
	public $uom3_height;
	public $uom3_weight;
	public $uom3_margin;
	public $uom3_box_type;
	public $uom3_harga;
	public $uom4_nm;
	public $uom4_htype_1;
	public $uom4_htype_2;
	public $uom4_htype_3;
	public $uom4_htype_4;
	public $uom4_htype_5;
	public $uom4_rack_type;
	public $uom4_shelfing_rack_type;
	public $uom4_conversion;
	public $uom4_internal_barcode;
	public $uom4_width;
	public $uom4_length;
	public $uom4_height;
	public $uom4_weight;
	public $uom4_margin;
	public $uom4_box_type;
	public $uom4_harga;
	public $uom1_prod_nm;
	public $uom2_prod_nm;
	public $uom3_prod_nm;
	public $uom4_prod_nm;
	public $jenis;
	public $status_flag;

	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"stock_id\", \"category_id\", \"tax_type_id\", \"description\", \"long_description\", \"units\", \"actual_cost\" "
				.", \"last_cost\", \"material_cost\", \"labour_cost\", \"overhead_cost\", \"inactive\", \"no_sale\", \"editable\" "
				.", \"jenis_item\", \"bumun_cd\", \"l1_cd\", \"l2_cd\", \"l3_cd\", \"l4_cd\", \"margin\" "
				.", \"moving_type\", \"ass_type_1\", \"ass_type_2\", \"ass_type_3\", \"expiry_product\", \"weight_product\", \"batch_product\" "
				.", \"active_status\", \"kode_1\", \"kode_2\", \"kode_3\", \"discount\", \"supplied_by\", \"uom1_nm\" "
				.", \"uom1_htype_1\", \"uom1_htype_2\", \"uom1_htype_3\", \"uom1_htype_4\", \"uom1_htype_5\", \"uom1_rack_type\", \"uom1_shelfing_rack_type\" "
				.", \"uom1_internal_barcode\", \"uom1_width\", \"uom1_length\", \"uom1_height\", \"uom1_weight\", \"uom1_margin\", \"uom1_box_type\" "
				.", \"uom1_harga\", \"uom2_nm\", \"uom2_htype_1\", \"uom2_htype_2\", \"uom2_htype_3\", \"uom2_htype_4\", \"uom2_htype_5\" "
				.", \"uom2_rack_type\", \"uom2_shelfing_rack_type\", \"uom2_conversion\", \"uom2_internal_barcode\", \"uom2_width\", \"uom2_length\", \"uom2_height\" "
				.", \"uom2_weight\", \"uom2_margin\", \"uom2_box_type\", \"uom2_harga\", \"uom3_nm\", \"uom3_htype_1\", \"uom3_htype_2\" "
				.", \"uom3_htype_3\", \"uom3_htype_4\", \"uom3_htype_5\", \"uom3_rack_type\", \"uom3_shelfing_rack_type\", \"uom3_conversion\", \"uom3_internal_barcode\" "
				.", \"uom3_width\", \"uom3_length\", \"uom3_height\", \"uom3_weight\", \"uom3_margin\", \"uom3_box_type\", \"uom3_harga\" "
				.", \"uom4_nm\", \"uom4_htype_1\", \"uom4_htype_2\", \"uom4_htype_3\", \"uom4_htype_4\", \"uom4_htype_5\", \"uom4_rack_type\" "
				.", \"uom4_shelfing_rack_type\", \"uom4_conversion\", \"uom4_internal_barcode\", \"uom4_width\", \"uom4_length\", \"uom4_height\", \"uom4_weight\" "
				.", \"uom4_margin\", \"uom4_box_type\", \"uom4_harga\", \"uom1_prod_nm\", \"uom2_prod_nm\", \"uom3_prod_nm\", \"uom4_prod_nm\", \"uom1_conversion\", \"ppob\", \"mb_flag\", \"status\", \"tipe\", \"jenis\", \"status_flag\" "
				." FROM \"m_product\" "
				." WHERE \"inactive\" != 1 ";			
	
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			if($result[111]!= 2){
				$lists[$count]['stock_id'] = $result[0];
				$lists[$count]['category_id'] = $result[1];
				$lists[$count]['tax_type_id'] = $result[2];
				$lists[$count]['description'] = $result[3];
				$lists[$count]['long_description'] = $result[4];
				$lists[$count]['units'] = $result[5];
				$lists[$count]['actual_cost'] = $result[6];
				$lists[$count]['last_cost'] = $result[7];
				$lists[$count]['material_cost'] = $result[8];
				$lists[$count]['labour_cost'] = $result[9];
				$lists[$count]['overhead_cost'] = $result[10];
				$lists[$count]['inactive'] = $result[11];
				$lists[$count]['no_sale'] = $result[12];
				$lists[$count]['editable'] = $result[13];
				$lists[$count]['jenis_item'] = $result[14];
				$lists[$count]['bumun_cd'] = $result[15];
				$lists[$count]['l1_cd'] = $result[16];
				$lists[$count]['l2_cd'] = $result[17];
				$lists[$count]['l3_cd'] = $result[18];
				$lists[$count]['l4_cd'] = $result[19];
				$lists[$count]['margin'] = $result[20];
				$lists[$count]['moving_type'] = $result[21];
				$lists[$count]['ass_type_1'] = $result[22];
				$lists[$count]['ass_type_2'] = $result[23];
				$lists[$count]['ass_type_3'] = $result[24];
				$lists[$count]['expiry_product'] = $result[25];
				$lists[$count]['weight_product'] = $result[26];
				$lists[$count]['batch_product'] = $result[27];
				$lists[$count]['active_status'] = $result[28];
				$lists[$count]['kode_1'] = $result[29];
				$lists[$count]['kode_2'] = $result[30];
				$lists[$count]['kode_3'] = $result[31];
				$lists[$count]['discount'] = $result[32];
				$lists[$count]['supplied_by'] = $result[33];
				$lists[$count]['uom1_nm'] = $result[34];
				$lists[$count]['uom1_htype_1'] = $result[35];
				$lists[$count]['uom1_htype_2'] = $result[36];
				$lists[$count]['uom1_htype_3'] = $result[37];
				$lists[$count]['uom1_htype_4'] = $result[38];
				$lists[$count]['uom1_htype_5'] = $result[39];
				$lists[$count]['uom1_rack_type'] = $result[40];
				$lists[$count]['uom1_shelfing_rack_type'] = $result[41];
				$lists[$count]['uom1_internal_barcode'] = $result[42];
				$lists[$count]['uom1_width'] = $result[43];
				$lists[$count]['uom1_length'] = $result[44];
				$lists[$count]['uom1_height'] = $result[45];
				$lists[$count]['uom1_weight'] = $result[46];
				$lists[$count]['uom1_margin'] = $result[47];
				$lists[$count]['uom1_box_type'] = $result[48];
				$lists[$count]['uom1_harga'] = $result[49];
				$lists[$count]['uom2_nm'] = $result[50];
				$lists[$count]['uom2_htype_1'] = $result[51];
				$lists[$count]['uom2_htype_2'] = $result[52];
				$lists[$count]['uom2_htype_3'] = $result[53];
				$lists[$count]['uom2_htype_4'] = $result[54];
				$lists[$count]['uom2_htype_5'] = $result[55];
				$lists[$count]['uom2_rack_type'] = $result[56];
				$lists[$count]['uom2_shelfing_rack_type'] = $result[57];
				$lists[$count]['uom2_conversion'] = $result[58];
				$lists[$count]['uom2_internal_barcode'] = $result[59];
				$lists[$count]['uom2_width'] = $result[60];
				$lists[$count]['uom2_length'] = $result[61];
				$lists[$count]['uom2_height'] = $result[62];
				$lists[$count]['uom2_weight'] = $result[63];
				$lists[$count]['uom2_margin'] = $result[64];
				$lists[$count]['uom2_box_type'] = $result[65];
				$lists[$count]['uom2_harga'] = $result[66];
				$lists[$count]['uom3_nm'] = $result[67];
				$lists[$count]['uom3_htype_1'] = $result[68];
				$lists[$count]['uom3_htype_2'] = $result[69];
				$lists[$count]['uom3_htype_3'] = $result[70];
				$lists[$count]['uom3_htype_4'] = $result[71];
				$lists[$count]['uom3_htype_5'] = $result[72];
				$lists[$count]['uom3_rack_type'] = $result[73];
				$lists[$count]['uom3_shelfing_rack_type'] = $result[74];
				$lists[$count]['uom3_conversion'] = $result[75];
				$lists[$count]['uom3_internal_barcode'] = $result[76];
				$lists[$count]['uom3_width'] = $result[77];
				$lists[$count]['uom3_length'] = $result[78];
				$lists[$count]['uom3_height'] = $result[79];
				$lists[$count]['uom3_weight'] = $result[80];
				$lists[$count]['uom3_margin'] = $result[81];
				$lists[$count]['uom3_box_type'] = $result[82];
				$lists[$count]['uom3_harga'] = $result[83];
				$lists[$count]['uom4_nm'] = $result[84];
				$lists[$count]['uom4_htype_1'] = $result[85];
				$lists[$count]['uom4_htype_2'] = $result[86];
				$lists[$count]['uom4_htype_3'] = $result[87];
				$lists[$count]['uom4_htype_4'] = $result[88];
				$lists[$count]['uom4_htype_5'] = $result[89];
				$lists[$count]['uom4_rack_type'] = $result[90];
				$lists[$count]['uom4_shelfing_rack_type'] = $result[91];
				$lists[$count]['uom4_conversion'] = $result[92];
				$lists[$count]['uom4_internal_barcode'] = $result[93];
				$lists[$count]['uom4_width'] = $result[94];
				$lists[$count]['uom4_length'] = $result[95];
				$lists[$count]['uom4_height'] = $result[96];
				$lists[$count]['uom4_weight'] = $result[97];
				$lists[$count]['uom4_margin'] = $result[98];
				$lists[$count]['uom4_box_type'] = $result[99];
				$lists[$count]['uom4_harga'] = $result[100];
				$lists[$count]['uom1_prod_nm'] = $result[101];
				$lists[$count]['uom2_prod_nm'] = $result[102];
				$lists[$count]['uom3_prod_nm'] = $result[103];
				$lists[$count]['uom4_prod_nm'] = $result[104];
				$lists[$count]['uom1_conversion'] = $result[105];
				$lists[$count]['ppob'] = $result[106];
				$lists[$count]['mb_flag'] = $result[107];
				$lists[$count]['status'] = $result[108];
				$lists[$count]['tipe'] = $result[109];
				$lists[$count]['jenis'] = $result[110];
				$lists[$count]['status_flag'] = $result[111];
			}
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"stock_id\", \"category_id\", \"tax_type_id\", \"description\", \"long_description\", \"units\", \"actual_cost\" "
				.", \"last_cost\", \"material_cost\", \"labour_cost\", \"overhead_cost\", \"inactive\", \"no_sale\", \"editable\" "
				.", \"jenis_item\", \"bumun_cd\", \"l1_cd\", \"l2_cd\", \"l3_cd\", \"l4_cd\", \"margin\" "
				.", \"moving_type\", \"ass_type_1\", \"ass_type_2\", \"ass_type_3\", \"expiry_product\", \"weight_product\", \"batch_product\" "
				.", \"active_status\", \"kode_1\", \"kode_2\", \"kode_3\", \"discount\", \"supplied_by\", \"uom1_nm\" "
				.", \"uom1_htype_1\", \"uom1_htype_2\", \"uom1_htype_3\", \"uom1_htype_4\", \"uom1_htype_5\", \"uom1_rack_type\", \"uom1_shelfing_rack_type\" "
				.", \"uom1_internal_barcode\", \"uom1_width\", \"uom1_length\", \"uom1_height\", \"uom1_weight\", \"uom1_margin\", \"uom1_box_type\" "
				.", \"uom1_harga\", \"uom2_nm\", \"uom2_htype_1\", \"uom2_htype_2\", \"uom2_htype_3\", \"uom2_htype_4\", \"uom2_htype_5\" "
				.", \"uom2_rack_type\", \"uom2_shelfing_rack_type\", \"uom2_conversion\", \"uom2_internal_barcode\", \"uom2_width\", \"uom2_length\", \"uom2_height\" "
				.", \"uom2_weight\", \"uom2_margin\", \"uom2_box_type\", \"uom2_harga\", \"uom3_nm\", \"uom3_htype_1\", \"uom3_htype_2\" "
				.", \"uom3_htype_3\", \"uom3_htype_4\", \"uom3_htype_5\", \"uom3_rack_type\", \"uom3_shelfing_rack_type\", \"uom3_conversion\", \"uom3_internal_barcode\" "
				.", \"uom3_width\", \"uom3_length\", \"uom3_height\", \"uom3_weight\", \"uom3_margin\", \"uom3_box_type\", \"uom3_harga\" "
				.", \"uom4_nm\", \"uom4_htype_1\", \"uom4_htype_2\", \"uom4_htype_3\", \"uom4_htype_4\", \"uom4_htype_5\", \"uom4_rack_type\" "
				.", \"uom4_shelfing_rack_type\", \"uom4_conversion\", \"uom4_internal_barcode\", \"uom4_width\", \"uom4_length\", \"uom4_height\", \"uom4_weight\" "
				.", \"uom4_margin\", \"uom4_box_type\", \"uom4_harga\", \"uom1_prod_nm\", \"uom2_prod_nm\", \"uom3_prod_nm\", \"uom4_prod_nm\", \"uom1_conversion\", \"status_flag\" "
				." FROM \"m_product\" "
				." WHERE \"stock_id\" = '".$object->stock_id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			if($result[106]!= 2){
				$lists[$count]['stock_id'] = $result[0];
				$lists[$count]['category_id'] = $result[1];
				$lists[$count]['tax_type_id'] = $result[2];
				$lists[$count]['description'] = $result[3];
				$lists[$count]['long_description'] = $result[4];
				$lists[$count]['units'] = $result[5];
				$lists[$count]['actual_cost'] = $result[6];
				$lists[$count]['last_cost'] = $result[7];
				$lists[$count]['material_cost'] = $result[8];
				$lists[$count]['labour_cost'] = $result[9];
				$lists[$count]['overhead_cost'] = $result[10];
				$lists[$count]['inactive'] = $result[11];
				$lists[$count]['no_sale'] = $result[12];
				$lists[$count]['editable'] = $result[13];
				$lists[$count]['jenis_item'] = $result[14];
				$lists[$count]['bumun_cd'] = $result[15];
				$lists[$count]['l1_cd'] = $result[16];
				$lists[$count]['l2_cd'] = $result[17];
				$lists[$count]['l3_cd'] = $result[18];
				$lists[$count]['l4_cd'] = $result[19];
				$lists[$count]['margin'] = $result[20];
				$lists[$count]['moving_type'] = $result[21];
				$lists[$count]['ass_type_1'] = $result[22];
				$lists[$count]['ass_type_2'] = $result[23];
				$lists[$count]['ass_type_3'] = $result[24];
				$lists[$count]['expiry_product'] = $result[25];
				$lists[$count]['weight_product'] = $result[26];
				$lists[$count]['batch_product'] = $result[27];
				$lists[$count]['active_status'] = $result[28];
				$lists[$count]['kode_1'] = $result[29];
				$lists[$count]['kode_2'] = $result[30];
				$lists[$count]['kode_3'] = $result[31];
				$lists[$count]['discount'] = $result[32];
				$lists[$count]['supplied_by'] = $result[33];
				$lists[$count]['uom1_nm'] = $result[34];
				$lists[$count]['uom1_htype_1'] = $result[35];
				$lists[$count]['uom1_htype_2'] = $result[36];
				$lists[$count]['uom1_htype_3'] = $result[37];
				$lists[$count]['uom1_htype_4'] = $result[38];
				$lists[$count]['uom1_htype_5'] = $result[39];
				$lists[$count]['uom1_rack_type'] = $result[40];
				$lists[$count]['uom1_shelfing_rack_type'] = $result[41];
				$lists[$count]['uom1_internal_barcode'] = $result[42];
				$lists[$count]['uom1_width'] = $result[43];
				$lists[$count]['uom1_length'] = $result[44];
				$lists[$count]['uom1_height'] = $result[45];
				$lists[$count]['uom1_weight'] = $result[46];
				$lists[$count]['uom1_margin'] = $result[47];
				$lists[$count]['uom1_box_type'] = $result[48];
				$lists[$count]['uom1_harga'] = $result[49];
				$lists[$count]['uom2_nm'] = $result[50];
				$lists[$count]['uom2_htype_1'] = $result[51];
				$lists[$count]['uom2_htype_2'] = $result[52];
				$lists[$count]['uom2_htype_3'] = $result[53];
				$lists[$count]['uom2_htype_4'] = $result[54];
				$lists[$count]['uom2_htype_5'] = $result[55];
				$lists[$count]['uom2_rack_type'] = $result[56];
				$lists[$count]['uom2_shelfing_rack_type'] = $result[57];
				$lists[$count]['uom2_conversion'] = $result[58];
				$lists[$count]['uom2_internal_barcode'] = $result[59];
				$lists[$count]['uom2_width'] = $result[60];
				$lists[$count]['uom2_length'] = $result[61];
				$lists[$count]['uom2_height'] = $result[62];
				$lists[$count]['uom2_weight'] = $result[63];
				$lists[$count]['uom2_margin'] = $result[64];
				$lists[$count]['uom2_box_type'] = $result[65];
				$lists[$count]['uom2_harga'] = $result[66];
				$lists[$count]['uom3_nm'] = $result[67];
				$lists[$count]['uom3_htype_1'] = $result[68];
				$lists[$count]['uom3_htype_2'] = $result[69];
				$lists[$count]['uom3_htype_3'] = $result[70];
				$lists[$count]['uom3_htype_4'] = $result[71];
				$lists[$count]['uom3_htype_5'] = $result[72];
				$lists[$count]['uom3_rack_type'] = $result[73];
				$lists[$count]['uom3_shelfing_rack_type'] = $result[74];
				$lists[$count]['uom3_conversion'] = $result[75];
				$lists[$count]['uom3_internal_barcode'] = $result[76];
				$lists[$count]['uom3_width'] = $result[77];
				$lists[$count]['uom3_length'] = $result[78];
				$lists[$count]['uom3_height'] = $result[79];
				$lists[$count]['uom3_weight'] = $result[80];
				$lists[$count]['uom3_margin'] = $result[81];
				$lists[$count]['uom3_box_type'] = $result[82];
				$lists[$count]['uom3_harga'] = $result[83];
				$lists[$count]['uom4_nm'] = $result[84];
				$lists[$count]['uom4_htype_1'] = $result[85];
				$lists[$count]['uom4_htype_2'] = $result[86];
				$lists[$count]['uom4_htype_3'] = $result[87];
				$lists[$count]['uom4_htype_4'] = $result[88];
				$lists[$count]['uom4_htype_5'] = $result[89];
				$lists[$count]['uom4_rack_type'] = $result[90];
				$lists[$count]['uom4_shelfing_rack_type'] = $result[91];
				$lists[$count]['uom4_conversion'] = $result[92];
				$lists[$count]['uom4_internal_barcode'] = $result[93];
				$lists[$count]['uom4_width'] = $result[94];
				$lists[$count]['uom4_length'] = $result[95];
				$lists[$count]['uom4_height'] = $result[96];
				$lists[$count]['uom4_weight'] = $result[97];
				$lists[$count]['uom4_margin'] = $result[98];
				$lists[$count]['uom4_box_type'] = $result[99];
				$lists[$count]['uom4_harga'] = $result[100];
				$lists[$count]['uom1_prod_nm'] = $result[101];
				$lists[$count]['uom2_prod_nm'] = $result[102];
				$lists[$count]['uom3_prod_nm'] = $result[103];
				$lists[$count]['uom4_prod_nm'] = $result[104];	
				$lists[$count]['uom1_conversion'] = $result[105];
				$lists[$count]['status_flag'] = $result[105];
			}
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"stock_id\", \"category_id\", \"tax_type_id\", \"description\", \"long_description\", \"units\", \"actual_cost\" "
				.", \"last_cost\", \"material_cost\", \"labour_cost\", \"overhead_cost\", \"inactive\", \"no_sale\", \"editable\" "
				.", \"jenis_item\", \"bumun_cd\", \"l1_cd\", \"l2_cd\", \"l3_cd\", \"l4_cd\", \"margin\" "
				.", \"moving_type\", \"ass_type_1\", \"ass_type_2\", \"ass_type_3\", \"expiry_product\", \"weight_product\", \"batch_product\" "
				.", \"active_status\", \"kode_1\", \"kode_2\", \"kode_3\", \"discount\", \"supplied_by\", \"uom1_nm\" "
				.", \"uom1_htype_1\", \"uom1_htype_2\", \"uom1_htype_3\", \"uom1_htype_4\", \"uom1_htype_5\", \"uom1_rack_type\", \"uom1_shelfing_rack_type\" "
				.", \"uom1_internal_barcode\", \"uom1_width\", \"uom1_length\", \"uom1_height\", \"uom1_weight\", \"uom1_margin\", \"uom1_box_type\" "
				.", \"uom1_harga\", \"uom2_nm\", \"uom2_htype_1\", \"uom2_htype_2\", \"uom2_htype_3\", \"uom2_htype_4\", \"uom2_htype_5\" "
				.", \"uom2_rack_type\", \"uom2_shelfing_rack_type\", \"uom2_conversion\", \"uom2_internal_barcode\", \"uom2_width\", \"uom2_length\", \"uom2_height\" "
				.", \"uom2_weight\", \"uom2_margin\", \"uom2_box_type\", \"uom2_harga\", \"uom3_nm\", \"uom3_htype_1\", \"uom3_htype_2\" "
				.", \"uom3_htype_3\", \"uom3_htype_4\", \"uom3_htype_5\", \"uom3_rack_type\", \"uom3_shelfing_rack_type\", \"uom3_conversion\", \"uom3_internal_barcode\" "
				.", \"uom3_width\", \"uom3_length\", \"uom3_height\", \"uom3_weight\", \"uom3_margin\", \"uom3_box_type\", \"uom3_harga\" "
				.", \"uom4_nm\", \"uom4_htype_1\", \"uom4_htype_2\", \"uom4_htype_3\", \"uom4_htype_4\", \"uom4_htype_5\", \"uom4_rack_type\" "
				.", \"uom4_shelfing_rack_type\", \"uom4_conversion\", \"uom4_internal_barcode\", \"uom4_width\", \"uom4_length\", \"uom4_height\", \"uom4_weight\" "
				.", \"uom4_margin\", \"uom4_box_type\", \"uom4_harga\", \"uom1_prod_nm\", \"uom2_prod_nm\", \"uom3_prod_nm\", \"uom4_prod_nm\", \"uom1_conversion\", \"status\", \"jenis\", \"status_flag\" "
				." FROM \"m_product\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['category_id'] = $result[1];
			$lists[$count]['tax_type_id'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['long_description'] = $result[4];
			$lists[$count]['units'] = $result[5];
			$lists[$count]['actual_cost'] = $result[6];
			$lists[$count]['last_cost'] = $result[7];
			$lists[$count]['material_cost'] = $result[8];
			$lists[$count]['labour_cost'] = $result[9];
			$lists[$count]['overhead_cost'] = $result[10];
			$lists[$count]['inactive'] = $result[11];
			$lists[$count]['no_sale'] = $result[12];
			$lists[$count]['editable'] = $result[13];
			$lists[$count]['jenis_item'] = $result[14];
			$lists[$count]['bumun_cd'] = $result[15];
			$lists[$count]['l1_cd'] = $result[16];
			$lists[$count]['l2_cd'] = $result[17];
			$lists[$count]['l3_cd'] = $result[18];
			$lists[$count]['l4_cd'] = $result[19];
			$lists[$count]['margin'] = $result[20];
			$lists[$count]['moving_type'] = $result[21];
			$lists[$count]['ass_type_1'] = $result[22];
			$lists[$count]['ass_type_2'] = $result[23];
			$lists[$count]['ass_type_3'] = $result[24];
			$lists[$count]['expiry_product'] = $result[25];
			$lists[$count]['weight_product'] = $result[26];
			$lists[$count]['batch_product'] = $result[27];
			$lists[$count]['active_status'] = $result[28];
			$lists[$count]['kode_1'] = $result[29];
			$lists[$count]['kode_2'] = $result[30];
			$lists[$count]['kode_3'] = $result[31];
			$lists[$count]['discount'] = $result[32];
			$lists[$count]['supplied_by'] = $result[33];
			$lists[$count]['uom1_nm'] = $result[34];
			$lists[$count]['uom1_htype_1'] = $result[35];
			$lists[$count]['uom1_htype_2'] = $result[36];
			$lists[$count]['uom1_htype_3'] = $result[37];
			$lists[$count]['uom1_htype_4'] = $result[38];
			$lists[$count]['uom1_htype_5'] = $result[39];
			$lists[$count]['uom1_rack_type'] = $result[40];
			$lists[$count]['uom1_shelfing_rack_type'] = $result[41];
			$lists[$count]['uom1_internal_barcode'] = $result[42];
			$lists[$count]['uom1_width'] = $result[43];
			$lists[$count]['uom1_length'] = $result[44];
			$lists[$count]['uom1_height'] = $result[45];
			$lists[$count]['uom1_weight'] = $result[46];
			$lists[$count]['uom1_margin'] = $result[47];
			$lists[$count]['uom1_box_type'] = $result[48];
			$lists[$count]['uom1_harga'] = $result[49];
			$lists[$count]['uom2_nm'] = $result[50];
			$lists[$count]['uom2_htype_1'] = $result[51];
			$lists[$count]['uom2_htype_2'] = $result[52];
			$lists[$count]['uom2_htype_3'] = $result[53];
			$lists[$count]['uom2_htype_4'] = $result[54];
			$lists[$count]['uom2_htype_5'] = $result[55];
			$lists[$count]['uom2_rack_type'] = $result[56];
			$lists[$count]['uom2_shelfing_rack_type'] = $result[57];
			$lists[$count]['uom2_conversion'] = $result[58];
			$lists[$count]['uom2_internal_barcode'] = $result[59];
			$lists[$count]['uom2_width'] = $result[60];
			$lists[$count]['uom2_length'] = $result[61];
			$lists[$count]['uom2_height'] = $result[62];
			$lists[$count]['uom2_weight'] = $result[63];
			$lists[$count]['uom2_margin'] = $result[64];
			$lists[$count]['uom2_box_type'] = $result[65];
			$lists[$count]['uom2_harga'] = $result[66];
			$lists[$count]['uom3_nm'] = $result[67];
			$lists[$count]['uom3_htype_1'] = $result[68];
			$lists[$count]['uom3_htype_2'] = $result[69];
			$lists[$count]['uom3_htype_3'] = $result[70];
			$lists[$count]['uom3_htype_4'] = $result[71];
			$lists[$count]['uom3_htype_5'] = $result[72];
			$lists[$count]['uom3_rack_type'] = $result[73];
			$lists[$count]['uom3_shelfing_rack_type'] = $result[74];
			$lists[$count]['uom3_conversion'] = $result[75];
			$lists[$count]['uom3_internal_barcode'] = $result[76];
			$lists[$count]['uom3_width'] = $result[77];
			$lists[$count]['uom3_length'] = $result[78];
			$lists[$count]['uom3_height'] = $result[79];
			$lists[$count]['uom3_weight'] = $result[80];
			$lists[$count]['uom3_margin'] = $result[81];
			$lists[$count]['uom3_box_type'] = $result[82];
			$lists[$count]['uom3_harga'] = $result[83];
			$lists[$count]['uom4_nm'] = $result[84];
			$lists[$count]['uom4_htype_1'] = $result[85];
			$lists[$count]['uom4_htype_2'] = $result[86];
			$lists[$count]['uom4_htype_3'] = $result[87];
			$lists[$count]['uom4_htype_4'] = $result[88];
			$lists[$count]['uom4_htype_5'] = $result[89];
			$lists[$count]['uom4_rack_type'] = $result[90];
			$lists[$count]['uom4_shelfing_rack_type'] = $result[91];
			$lists[$count]['uom4_conversion'] = $result[92];
			$lists[$count]['uom4_internal_barcode'] = $result[93];
			$lists[$count]['uom4_width'] = $result[94];
			$lists[$count]['uom4_length'] = $result[95];
			$lists[$count]['uom4_height'] = $result[96];
			$lists[$count]['uom4_weight'] = $result[97];
			$lists[$count]['uom4_margin'] = $result[98];
			$lists[$count]['uom4_box_type'] = $result[99];
			$lists[$count]['uom4_harga'] = $result[100];
			$lists[$count]['uom1_prod_nm'] = $result[101];
			$lists[$count]['uom2_prod_nm'] = $result[102];
			$lists[$count]['uom3_prod_nm'] = $result[103];
			$lists[$count]['uom4_prod_nm'] = $result[104];	
			$lists[$count]['uom1_conversion'] = $result[105];
			$lists[$count]['status'] = $result[106];
			$lists[$count]['jenis'] = $result[107];
			$lists[$count]['status_flag'] = $result[108];
			$count++;
		}
		return $lists;
	}

	public function getFreeSQL2($query){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT mp.\"stock_id\", mp.\"category_id\", mp.\"tax_type_id\", mp.\"description\", mp.\"long_description\", mp.\"units\", mp.\"actual_cost\" "
				.", mp.\"last_cost\", mp.\"material_cost\", mp.\"labour_cost\", mp.\"overhead_cost\", mp.\"inactive\", mp.\"no_sale\", mp.\"editable\" "
				.", mp.\"jenis_item\", mp.\"bumun_cd\", mp.\"l1_cd\", mp.\"l2_cd\", mp.\"l3_cd\", mp.\"l4_cd\", mp.\"margin\" "
				.", mp.\"moving_type\", mp.\"ass_type_1\", mp.\"ass_type_2\", mp.\"ass_type_3\", mp.\"expiry_product\", mp.\"weight_product\", mp.\"batch_product\" "
				.", mp.\"active_status\", mp.\"kode_1\", mp.\"kode_2\", mp.\"kode_3\", mp.\"discount\", mp.\"supplied_by\", mp.\"uom1_nm\" "
				.", mp.\"uom1_htype_1\", mp.\"uom1_htype_2\", mp.\"uom1_htype_3\", mp.\"uom1_htype_4\", mp.\"uom1_htype_5\", mp.\"uom1_rack_type\", mp.\"uom1_shelfing_rack_type\" "
				.", mp.\"uom1_internal_barcode\", mp.\"uom1_width\", mp.\"uom1_length\", mp.\"uom1_height\", mp.\"uom1_weight\", mp.\"uom1_margin\", mp.\"uom1_box_type\" "
				.", mp.\"uom1_harga\", mp.\"uom2_nm\", mp.\"uom2_htype_1\", mp.\"uom2_htype_2\", mp.\"uom2_htype_3\", mp.\"uom2_htype_4\", mp.\"uom2_htype_5\" "
				.", mp.\"uom2_rack_type\", mp.\"uom2_shelfing_rack_type\", mp.\"uom2_conversion\", mp.\"uom2_internal_barcode\", mp.\"uom2_width\", mp.\"uom2_length\", mp.\"uom2_height\" "
				.", mp.\"uom2_weight\", mp.\"uom2_margin\", mp.\"uom2_box_type\", mp.\"uom2_harga\", mp.\"uom3_nm\", mp.\"uom3_htype_1\", mp.\"uom3_htype_2\" "
				.", mp.\"uom3_htype_3\", mp.\"uom3_htype_4\", mp.\"uom3_htype_5\", mp.\"uom3_rack_type\", mp.\"uom3_shelfing_rack_type\", mp.\"uom3_conversion\", mp.\"uom3_internal_barcode\" "
				.", mp.\"uom3_width\", mp.\"uom3_length\", mp.\"uom3_height\", mp.\"uom3_weight\", mp.\"uom3_margin\", mp.\"uom3_box_type\", mp.\"uom3_harga\" "
				.", mp.\"uom4_nm\", mp.\"uom4_htype_1\", mp.\"uom4_htype_2\", mp.\"uom4_htype_3\", mp.\"uom4_htype_4\", mp.\"uom4_htype_5\", mp.\"uom4_rack_type\" "
				.", mp.\"uom4_shelfing_rack_type\", mp.\"uom4_conversion\", mp.\"uom4_internal_barcode\", mp.\"uom4_width\", mp.\"uom4_length\", mp.\"uom4_height\", mp.\"uom4_weight\" "
				.", mp.\"uom4_margin\", mp.\"uom4_box_type\", mp.\"uom4_harga\", mp.\"uom1_prod_nm\", mp.\"uom2_prod_nm\", mp.\"uom3_prod_nm\", mp.\"uom4_prod_nm\", mp.\"uom1_conversion\", mp.\"jenis\", mp.\"status_flag\" "
				." FROM \"m_product\" mp INNER JOIN m_product_move mpm ON mp.stock_id = mpm.stock_id
				WHERE (mp.\"uom1_internal_barcode\" LIKE '%".$query."%' OR mp.\"uom2_internal_barcode\" LIKE '%".$query."%' OR mp.\"description\" LIKE '%".$query."%') AND mp.mb_flag = 'B' AND mpm.type !='SO' AND mp.status_flag !='2' GROUP BY mp.stock_id";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
				$lists[$count]['stock_id'] = $result[0];
				$lists[$count]['category_id'] = $result[1];
				$lists[$count]['tax_type_id'] = $result[2];
				$lists[$count]['description'] = $result[3];
				$lists[$count]['long_description'] = $result[4];
				$lists[$count]['units'] = $result[5];
				$lists[$count]['actual_cost'] = $result[6];
				$lists[$count]['last_cost'] = $result[7];
				$lists[$count]['material_cost'] = $result[8];
				$lists[$count]['labour_cost'] = $result[9];
				$lists[$count]['overhead_cost'] = $result[10];
				$lists[$count]['inactive'] = $result[11];
				$lists[$count]['no_sale'] = $result[12];
				$lists[$count]['editable'] = $result[13];
				$lists[$count]['jenis_item'] = $result[14];
				$lists[$count]['bumun_cd'] = $result[15];
				$lists[$count]['l1_cd'] = $result[16];
				$lists[$count]['l2_cd'] = $result[17];
				$lists[$count]['l3_cd'] = $result[18];
				$lists[$count]['l4_cd'] = $result[19];
				$lists[$count]['margin'] = $result[20];
				$lists[$count]['moving_type'] = $result[21];
				$lists[$count]['ass_type_1'] = $result[22];
				$lists[$count]['ass_type_2'] = $result[23];
				$lists[$count]['ass_type_3'] = $result[24];
				$lists[$count]['expiry_product'] = $result[25];
				$lists[$count]['weight_product'] = $result[26];
				$lists[$count]['batch_product'] = $result[27];
				$lists[$count]['active_status'] = $result[28];
				$lists[$count]['kode_1'] = $result[29];
				$lists[$count]['kode_2'] = $result[30];
				$lists[$count]['kode_3'] = $result[31];
				$lists[$count]['discount'] = $result[32];
				$lists[$count]['supplied_by'] = $result[33];
				$lists[$count]['uom1_nm'] = $result[34];
				$lists[$count]['uom1_htype_1'] = $result[35];
				$lists[$count]['uom1_htype_2'] = $result[36];
				$lists[$count]['uom1_htype_3'] = $result[37];
				$lists[$count]['uom1_htype_4'] = $result[38];
				$lists[$count]['uom1_htype_5'] = $result[39];
				$lists[$count]['uom1_rack_type'] = $result[40];
				$lists[$count]['uom1_shelfing_rack_type'] = $result[41];
				$lists[$count]['uom1_internal_barcode'] = $result[42];
				$lists[$count]['uom1_width'] = $result[43];
				$lists[$count]['uom1_length'] = $result[44];
				$lists[$count]['uom1_height'] = $result[45];
				$lists[$count]['uom1_weight'] = $result[46];
				$lists[$count]['uom1_margin'] = $result[47];
				$lists[$count]['uom1_box_type'] = $result[48];
				$lists[$count]['uom1_harga'] = $result[49];
				$lists[$count]['uom2_nm'] = $result[50];
				$lists[$count]['uom2_htype_1'] = $result[51];
				$lists[$count]['uom2_htype_2'] = $result[52];
				$lists[$count]['uom2_htype_3'] = $result[53];
				$lists[$count]['uom2_htype_4'] = $result[54];
				$lists[$count]['uom2_htype_5'] = $result[55];
				$lists[$count]['uom2_rack_type'] = $result[56];
				$lists[$count]['uom2_shelfing_rack_type'] = $result[57];
				$lists[$count]['uom2_conversion'] = $result[58];
				$lists[$count]['uom2_internal_barcode'] = $result[59];
				$lists[$count]['uom2_width'] = $result[60];
				$lists[$count]['uom2_length'] = $result[61];
				$lists[$count]['uom2_height'] = $result[62];
				$lists[$count]['uom2_weight'] = $result[63];
				$lists[$count]['uom2_margin'] = $result[64];
				$lists[$count]['uom2_box_type'] = $result[65];
				$lists[$count]['uom2_harga'] = $result[66];
				$lists[$count]['uom3_nm'] = $result[67];
				$lists[$count]['uom3_htype_1'] = $result[68];
				$lists[$count]['uom3_htype_2'] = $result[69];
				$lists[$count]['uom3_htype_3'] = $result[70];
				$lists[$count]['uom3_htype_4'] = $result[71];
				$lists[$count]['uom3_htype_5'] = $result[72];
				$lists[$count]['uom3_rack_type'] = $result[73];
				$lists[$count]['uom3_shelfing_rack_type'] = $result[74];
				$lists[$count]['uom3_conversion'] = $result[75];
				$lists[$count]['uom3_internal_barcode'] = $result[76];
				$lists[$count]['uom3_width'] = $result[77];
				$lists[$count]['uom3_length'] = $result[78];
				$lists[$count]['uom3_height'] = $result[79];
				$lists[$count]['uom3_weight'] = $result[80];
				$lists[$count]['uom3_margin'] = $result[81];
				$lists[$count]['uom3_box_type'] = $result[82];
				$lists[$count]['uom3_harga'] = $result[83];
				$lists[$count]['uom4_nm'] = $result[84];
				$lists[$count]['uom4_htype_1'] = $result[85];
				$lists[$count]['uom4_htype_2'] = $result[86];
				$lists[$count]['uom4_htype_3'] = $result[87];
				$lists[$count]['uom4_htype_4'] = $result[88];
				$lists[$count]['uom4_htype_5'] = $result[89];
				$lists[$count]['uom4_rack_type'] = $result[90];
				$lists[$count]['uom4_shelfing_rack_type'] = $result[91];
				$lists[$count]['uom4_conversion'] = $result[92];
				$lists[$count]['uom4_internal_barcode'] = $result[93];
				$lists[$count]['uom4_width'] = $result[94];
				$lists[$count]['uom4_length'] = $result[95];
				$lists[$count]['uom4_height'] = $result[96];
				$lists[$count]['uom4_weight'] = $result[97];
				$lists[$count]['uom4_margin'] = $result[98];
				$lists[$count]['uom4_box_type'] = $result[99];
				$lists[$count]['uom4_harga'] = $result[100];
				$lists[$count]['uom1_prod_nm'] = $result[101];
				$lists[$count]['uom2_prod_nm'] = $result[102];
				$lists[$count]['uom3_prod_nm'] = $result[103];
				$lists[$count]['uom4_prod_nm'] = $result[104];	
				$lists[$count]['uom1_conversion'] = $result[105];
				$lists[$count]['jenis'] = $result[106];
				$lists[$count]['status_flag'] = $result[107];
			$count++;
		}
		
		return $lists;
	}
	
	public function getProductPR($object){
		$barcode = $object['barcode'];
		$count_index = $object['count_index'];
		
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT a.\"stock_id\", a.\"uom2_prod_nm\", CASE WHEN \"uom1_htype_3\" = 't' THEN \"uom1_nm\" WHEN \"uom2_htype_3\" = 't' THEN \"uom2_nm\" "
			." WHEN \"uom3_htype_3\" = 't' THEN \"uom3_nm\" WHEN \"uom4_htype_3\" = 't' THEN \"uom4_nm\" ELSE '' END "
			.", CASE WHEN \"uom1_htype_1\" = 't' THEN \"uom1_nm\" WHEN \"uom2_htype_1\" = 't' THEN \"uom2_nm\" "
			." WHEN \"uom3_htype_1\" = 't' THEN \"uom3_nm\" WHEN \"uom4_htype_1\" = 't' THEN \"uom4_nm\" ELSE '' END "
			.", CASE WHEN \"uom1_internal_barcode\" = '".$barcode."' THEN \"uom1_nm\" WHEN \"uom2_internal_barcode\" = '".$barcode."' THEN \"uom2_nm\" "
			." WHEN \"uom3_internal_barcode\" = '".$barcode."' THEN \"uom3_nm\" WHEN \"uom4_internal_barcode\" = '".$barcode."' THEN \"uom4_nm\" ELSE '' END "
			.", CASE WHEN \"uom1_htype_3\" = 't' THEN \"uom1_harga\" WHEN \"uom2_htype_3\" = 't' THEN \"uom2_harga\" "
			." WHEN \"uom3_htype_3\" = 't' THEN \"uom3_harga\" WHEN \"uom4_htype_3\" = 't' THEN \"uom4_harga\" ELSE 0 END "
			
			.", CASE WHEN \"uom1_htype_3\" = 't' THEN \"uom1_conversion\" WHEN \"uom2_htype_3\" = 't' THEN \"uom2_conversion\" "
			." WHEN \"uom3_htype_3\" = 't' THEN \"uom3_conversion\" WHEN \"uom4_htype_3\" = 't' THEN \"uom4_conversion\" ELSE '' END "
			.", CASE WHEN \"uom1_htype_1\" = 't' THEN \"uom1_conversion\" WHEN \"uom2_htype_1\" = 't' THEN \"uom2_conversion\" "
			." WHEN \"uom3_htype_1\" = 't' THEN \"uom3_conversion\" WHEN \"uom4_htype_1\" = 't' THEN \"uom4_conversion\" ELSE '' END "
			.", CASE WHEN \"uom1_internal_barcode\" = '".$barcode."' THEN \"uom1_conversion\" WHEN \"uom2_internal_barcode\" = '".$barcode."' THEN \"uom2_conversion\" "
			." WHEN \"uom3_internal_barcode\" = '".$barcode."' THEN \"uom3_conversion\" WHEN \"uom4_internal_barcode\" = '".$barcode."' THEN \"uom4_conversion\" ELSE '' END "
			
			.", a.supplied_by, a.status, a.tipe, a.jenis, a.status_flag"
			
			." FROM \"m_product\" a "
			." WHERE ( \"uom1_internal_barcode\" ='".$barcode."'  OR \"uom2_internal_barcode\" ='".$barcode."' OR \"uom3_internal_barcode\" ='".$barcode."' "
			." OR \"uom4_internal_barcode\" ='".$barcode."' ) "
			." LIMIT 1 ";
		
		$results = $connection->query($sql);
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			if($result[13]!= 2){
				$lists[$count]['stock_id'] = $result[0];
				$lists[$count]['uom2_prod_nm'] = $result[1];
				$lists[$count]['uom_order'] = $result[2];
				$lists[$count]['uom_sell'] = $result[3];
				$lists[$count]['uom_pr'] = $result[4];
				$lists[$count]['harga_order'] = $result[5];
				$lists[$count]['convert_order'] = $result[6];
				$lists[$count]['convert_sell'] = $result[7];
				$lists[$count]['convert_pr'] = $result[8];
				$lists[$count]['count_index'] = $count_index;
				$lists[$count]['barcode'] = $barcode;
				$lists[$count]['supplied_by'] = $result[9];
				$lists[$count]['status'] = $result[10];
				$lists[$count]['tipe'] = $result[11];
				$lists[$count]['jenis'] = $result[12];
				$lists[$count]['status_flag'] = $result[13];
			}
			$count++;
		}
	
		return $lists;
	}

	public function getProductPR2($object){
		$barcode = $object['barcode'];
		$count_index = $object['count_index'];
		
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT a.\"stock_id\", a.\"description\" , a.\"uom2_harga\", a.\"uom1_conversion\", a.\"uom2_conversion\", a.\"uom1_nm\", a.\"uom2_nm\", a.\"supplied_by\", a.\"status\", a.\"tipe\", a.\"supplied_by\", a.\"jenis\", a.\"status_flag\" "
			." FROM \"m_product\" a "
			." WHERE ( \"uom1_internal_barcode\" ='".$barcode."'  OR \"uom2_internal_barcode\" ='".$barcode."' ) AND \"status_flag\" !='2' "
			." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
				$lists[$count]['stock_id'] = $result[0];
				$lists[$count]['description'] = $result[1]; // nama barang
				$lists[$count]['uom_order'] = $result[6]; // satuan beli
				$lists[$count]['uom_sell'] = $result[5]; // satuan jual
				$lists[$count]['uom_pr'] = $result[5];// Pcs
				$lists[$count]['harga_order'] = $result[2];
				$lists[$count]['convert_order'] = $result[4];
				$lists[$count]['convert_sell'] = $result[3];
				$lists[$count]['convert_pr'] = $result[3];
				$lists[$count]['count_index'] = $count_index;
				$lists[$count]['barcode'] = $barcode;
				$lists[$count]['status'] = $result[8];
				$lists[$count]['tipe'] = $result[9];
				$lists[$count]['jenis'] = $result[10];
				$lists[$count]['status_flag'] = $result[12];
			$count++;
		}
	
		return $lists;
	}
	
	public function getProductAD($object){
		$r_product = $object['r_product'];
		$count_index = $object['count_index'];
		
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT a.\"stock_id\" "
			.", CASE WHEN \"uom1_htype_5\" = 't' THEN \"uom1_prod_nm\" WHEN \"uom2_htype_5\" = 't' THEN \"uom2_prod_nm\" "
			." WHEN \"uom3_htype_5\" = 't' THEN \"uom3_prod_nm\" WHEN \"uom4_htype_5\" = 't' THEN \"uom4_prod_nm\" ELSE '' END "					
			.", CASE WHEN \"uom1_htype_5\" = 't' THEN \"uom1_nm\" WHEN \"uom2_htype_5\" = 't' THEN \"uom2_nm\" "
			." WHEN \"uom3_htype_5\" = 't' THEN \"uom3_nm\" WHEN \"uom4_htype_5\" = 't' THEN \"uom4_nm\" ELSE '' END "
			.", CASE WHEN \"uom1_htype_5\" = 't' THEN \"uom1_cost_price\" WHEN \"uom2_htype_5\" = 't' THEN \"uom2_harga\" "
			." WHEN \"uom3_htype_5\" = 't' THEN \"uom3_harga\" WHEN \"uom4_htype_5\" = 't' THEN \"uom4_harga\" ELSE '0' END "		
			.", CASE WHEN \"uom1_htype_5\" = 't' THEN \"uom1_internal_barcode\" WHEN \"uom2_htype_5\" = 't' THEN \"uom2_internal_barcode\" "
			." WHEN \"uom3_htype_5\" = 't' THEN \"uom3_internal_barcode\" WHEN \"uom4_htype_5\" = 't' THEN \"uom4_internal_barcode\" ELSE '' END "	
			." ,a.\"status_flag\" "	
			." FROM \"m_product\" a INNER JOIN m_product_price b ON a.stock_id = b.stock_id "
			// ." WHERE \"inactive\" != 1  "
			." AND ( \"uom1_internal_barcode\" = '".$r_product."' OR \"uom2_internal_barcode\" = '".$r_product."' OR \"uom3_internal_barcode\" = '".$r_product."' OR \"uom4_internal_barcode\" = '".$r_product."' ) "
			." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			if($result[5]!= 2){
				$lists[$count]['stock_id'] = $result[0];
				$lists[$count]['uom1_prod_nm'] = $result[1];
				$lists[$count]['unit'] = $result[2];
				$lists[$count]['unit_price'] = $result[3];
				$lists[$count]['barcode'] = $result[4];
				$lists[$count]['count_index'] = $count_index;
			}
			$count++;
		}
	
		return $lists;
	}

	public function getProductAdjustment($object){
		$r_product = $object['r_product'];
		$count_index = $object['count_index'];
		
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT a.\"stock_id\",  a.\"uom1_prod_nm\",  b.\"average_cost\", a.\"uom1_internal_barcode\", a.\"uom1_nm\" "				
			.", CASE WHEN \"uom1_htype_5\" = 't' THEN \"uom1_nm\" WHEN \"uom2_htype_5\" = 't' THEN \"uom2_nm\" "
			." WHEN \"uom3_htype_5\" = 't' THEN \"uom3_nm\" WHEN \"uom4_htype_5\" = 't' THEN \"uom4_nm\" ELSE '' END "		
			.", CASE WHEN \"uom1_htype_5\" = 't' THEN \"uom1_internal_barcode\" WHEN \"uom2_htype_5\" = 't' THEN \"uom2_internal_barcode\" "
			." WHEN \"uom3_htype_5\" = 't' THEN \"uom3_internal_barcode\" WHEN \"uom4_htype_5\" = 't' THEN \"uom4_internal_barcode\" ELSE '' END "	
			." ,a.\"status_flag\" "	
			." FROM \"m_product\" a INNER JOIN m_product_price b ON a.stock_id = b.stock_id "
			// ." WHERE \"inactive\" != 1  "
			." AND ( \"uom1_internal_barcode\" = '".$r_product."' OR \"uom2_internal_barcode\" = '".$r_product."' OR \"uom3_internal_barcode\" = '".$r_product."' OR \"uom4_internal_barcode\" = '".$r_product."' ) AND a.\"stock_id\" != '2' "
			." LIMIT 1 ";
		
		$results = $connection->query($sql);
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
				$lists[$count]['stock_id'] = $result[0];
				$lists[$count]['uom1_prod_nm'] = $result[1];
				$lists[$count]['unit'] = $result[4];
				$lists[$count]['unit_price'] = $result[2];
				$lists[$count]['barcode'] = $result[3];
				$lists[$count]['count_index'] = $count_index;
			$count++;
		}
	
		return $lists;
	}
	
	public function getProductSale($object){
		$barcode = $object['barcode'];
		$count_index = $object['count_index'];
		
		$connection = new Postgresql($this->di['db']);

		$sql = " SELECT a.\"stock_id\", a.\"uom1_prod_nm\", a.\"uom1_internal_barcode\", a.\"uom1_nm\", b.\"uom1_changed_price\", b.\"uom1_rounding_price\", b.\"average_cost\", b.\"uom1_member_price\", a.\"ppob\" "
			." FROM \"m_product\" a JOIN \"m_product_price\" b ON a.\"stock_id\" = b.\"stock_id\" "
			." WHERE a.\"inactive\" != 1 AND a.\"status_flag\" !='2' "
			." AND ( \"uom1_internal_barcode\" ='".$barcode."'  OR \"uom2_internal_barcode\" ='".$barcode."' ) "
			." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$diskon = 0;
		$amount = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$disc = new Diskon();
			$condition = " WHERE \"kode_produk\" = '".$result[0]."' AND \"flag\" = 'true' ";
			$dis = $disc::getFreeSQL($condition);
			if(count($dis)>0){
				$diskon = $dis['0']['harga_terdiskon'];
				$amount = $dis['0']['diskon'];
				$harga = $dis['0']['cost_price'];
			}else{
				$diskon =0;
				$amount=0;
				if($result[4] > 0){
					$harga = $result[4];
				}else{
					$harga = $result[5];
				}
			}

			$disco = new Promo();
			$condition = " WHERE \"id\" = '".$dis['0']['promosi_id']."' AND \"flag\" = 'true' ";
			$discount = $disco::getFreeSQL($condition);
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['uom1_prod_nm'] = $result[1];
			$lists[$count]['harga'] = $harga;
			$lists[$count]['count_index'] = $count_index;
			$lists[$count]['barcode'] = $result[2];
			$lists[$count]['diskon'] = $diskon;
			$lists[$count]['diskon_amount'] = $amount;
			$lists[$count]['from'] = $discount['0']['from_'];
			$lists[$count]['to'] = $discount['0']['to_'];
			$lists[$count]['satuan'] = $result[3];
			$lists[$count]['tax_type'] = $result[9];
			$lists[$count]['harga_member'] = $result[10];
			$lists[$count]['harga_hpp3'] = $result[6]; //HPP+3%
			$lists[$count]['qty'] = $result[12];
			$lists[$count]['ppob'] = $result[13];
			$lists[$count]['jenis_promo'] = $result[14];
			$lists[$count]['changed'] = $result[4];
			$lists[$count]['rounding'] = $result[5];
			$lists[$count]['average_cost'] = $result[6];
			$count++;
		}
	
		return $lists;
	}

	public function getProductSale2($object){
		$barcode = $object['barcode'];
		$count_index = $object['count_index'];
		
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT a.\"stock_id\", a.\"uom1_prod_nm\", a.\"uom1_internal_barcode\", a.\"uom1_nm\", b.\"uom1_changed_price\", b.\"uom1_rounding_price\", b.\"average_cost\", b.\"uom1_member_price\", a.\"ppob\", a.\"status_flag\" "
			." FROM \"m_product\" a JOIN \"m_product_price\" b ON a.\"stock_id\" = b.\"stock_id\" "
			." WHERE a.\"inactive\" != 1 AND a.\"status_flag\" != '2' "
			." AND ( \"uom1_internal_barcode\" ='".$barcode."'  OR \"uom2_internal_barcode\" ='".$barcode."' ) "
			." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$diskon = 0;
		$amount = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$disc = new Diskon();
			$condition = " WHERE \"kode_produk\" = '".$result[0]."' AND \"flag\" = 'true' ";
			$dis = $disc::getFreeSQL($condition);
			if($dis){
				$diskon = $dis[0]['harga_terdiskon'];
				$amount = $dis[0]['diskon'];
				$harga = $dis[0]['cost_price'];
			}else{
				$diskon=0;
				$amount=0;
				if($result[4] > 0){
					$harga = $result[4];
				}else{
					$harga = $result[5];
				}
			}

			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['uom1_prod_nm'] = $result[1];
			$lists[$count]['harga'] = $harga;
			$lists[$count]['count_index'] = $count_index;
			$lists[$count]['barcode'] = $result[2];
			$lists[$count]['diskon'] = $diskon;
			$lists[$count]['diskon_amount'] = $amount;
			// $lists[$count]['from'] = $result[6];
			// $lists[$count]['to'] = $result[7];
			$lists[$count]['satuan'] = $result[3];
			$lists[$count]['harga_member'] = $result[7];
			$lists[$count]['harga_hpp3'] = $result[6];
			$lists[$count]['ppob'] = $result[8];
			$lists[$count]['changed'] = $result[4];
			$lists[$count]['rounding'] = $result[5];
			$lists[$count]['average_cost'] = $result[6];
			$xy = new XY();
			$condition = " WHERE \"kode_produk_x\" = '".$result[0]."' AND \"flag\" = 'true' ";
			$bonus_xy = $xy::getFreeSQL($condition);
			
			if(count($bonus_xy)>0){
				$kode = $bonus_xy['0']['kode_produk_y'];
					$prod = new Product();
					$condition = " WHERE \"stock_id\" = '".$bonus_xy['0']['kode_produk_y']."' ";
					$produk = $prod::getFreeSQL($condition);

					$price = new ProductPrice();
					$condition = " WHERE \"stock_id\" = '".$bonus_xy['0']['kode_produk_y']."' ";
					$cek_price = $price::getFreeSQL($condition);
				$nama = $produk['0']['uom1_prod_nm'];
				$harga2 = 0;
				$barcode2 = $produk['0']['uom1_internal_barcode'];
				$satuan2 = $produk['0']['uom1_nm'];
				$hpp = $cek_price['0']['average_cost'];
			}else{
				$kode = 0;
				$nama = 0;
				$harga2 = 0;
				$barcode2 = 0;
				$satuan2 = 0;
				$hpp = 0;
			}

			$lists[$count]['kode_produk_y'] = $kode;
			$lists[$count]['nama'] = $nama;
			$lists[$count]['harga2'] = $harga2;
			$lists[$count]['barcode2'] = $barcode2;
			$lists[$count]['satuan2'] = $satuan2;
			$lists[$count]['harga_hpp'] = $hpp;
			$count++;
		}
	
		return $lists;
	}
	
	public function getProductSale3($object){
		$stock_id = $object['stock_id'];
		
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT a.\"stock_id\", a.\"uom1_prod_nm\", a.\"uom1_internal_barcode\", a.\"uom1_nm\", b.\"uom1_changed_price\", b.\"uom1_rounding_price\", b.\"average_cost\", b.\"uom1_member_price\", a.\"ppob\" "
			." FROM \"m_product\" a JOIN \"m_product_price\" b ON a.\"stock_id\" = b.\"stock_id\" "
			." WHERE a.\"stock_id\" = '".$stock_id."' "
			." LIMIT 1 ";
		
		
		$count = 0;
		$lists = null;
		$diskon = 0;
		$amount = 0;
		$results = $connection->query($sql);
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$disc = new Diskon();
			$condition = " WHERE \"kode_produk\" = '".$result[0]."' AND \"flag\" = 'true' ";
			$dis = $disc::getFreeSQL($condition);
			if(count($dis)>0){
				$diskon = $dis[0]['harga_terdiskon'];
				$amount = $dis[0]['diskon'];
				$harga = $result[5];
			}else{
				$diskon = 0;
				$amount = 0;
				if($result[4] > 0){
					$harga = $result[4];
				}else{
					$harga = $result[5];
				}
			}

			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['uom1_prod_nm'] = $result[1];
			$lists[$count]['harga'] = $harga;
			$lists[$count]['count_index'] = $count_index;
			$lists[$count]['barcode'] = $result[2];
			$lists[$count]['diskon'] = $diskon;
			$lists[$count]['diskon_amount'] = $amount;
			// $lists[$count]['from'] = $result[6];
			// $lists[$count]['to'] = $result[7];
			$lists[$count]['satuan'] = $result[3];
			$lists[$count]['harga_member'] = $result[7];
			$lists[$count]['harga_hpp3'] = $result[6]; //HPP+3%
			$lists[$count]['ppob'] = $result[8];
			// $lists[$count]['jenis_promo'] = $result[14];
			$lists[$count]['changed'] = $result[4];
			$lists[$count]['rounding'] = $result[5];
			$lists[$count]['average_cost'] = $result[6];

			// $xy = new XY();
			// $condition = " WHERE \"kode_produk_x\" = '".$result[0]."' AND \"flag\" = 'true' ";
			// $bonus_xy = $xy::getFreeSQL($condition);
			
			// if(count($bonus_xy)>0){
			// 	$kode = $bonus_xy['0']['kode_produk_y'];
			// 		$prod = new Product();
			// 		$condition = " WHERE \"stock_id\" = '".$bonus_xy['0']['kode_produk_y']."' ";
			// 		$produk = $prod::getFreeSQL($condition);

			// 		$price = new ProductPrice();
			// 		$condition = " WHERE \"stock_id\" = '".$bonus_xy['0']['kode_produk_y']."' ";
			// 		$cek_price = $price::getFreeSQL($condition);
			// 	$nama = $produk['0']['uom1_prod_nm'];
			// 	$harga2 = 0;
			// 	$barcode2 = $produk['0']['uom1_internal_barcode'];
			// 	$satuan2 = $produk['0']['uom1_nm'];
			// 	$hpp = $cek_price['0']['average_cost'];
			// }else{
			// 	$kode = 0;
			// 	$nama = 0;
			// 	$harga2 = 0;
			// 	$barcode2 = 0;
			// 	$satuan2 = 0;
			// 	$hpp = 0;
			// }

			// $lists[$count]['kode_produk_y'] = $kode;
			// $lists[$count]['nama'] = $nama;
			// $lists[$count]['harga2'] = $harga2;
			// $lists[$count]['barcode2'] = $barcode2;
			// $lists[$count]['satuan2'] = $satuan2;
			// $lists[$count]['harga_hpp'] = $hpp;

			$count++;
		}
	
		return $lists;
	}

	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"stock_id\") "
				." FROM \"m_product\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		// dd($object);
		$connection = new Postgresql($this->di['db']);

		if($object->inactive == '') { $object->inactive = '0'; }		
		if($object->category_id == '') { $object->category_id = '0'; }	
		if($object->tax_type_id == '') { $object->tax_type_id = '0'; }	
		if($object->no_sale == '') { $object->no_sale = '0'; }	
		if($object->editable == '') { $object->editable = '0'; }	
		if($object->jenis_item == '') { $object->jenis_item = '0'; }			
		
		if($object->uom1_internal_barcode == '') { $object->uom1_internal_barcode = 'f'; }
		if($object->uom2_internal_barcode == '') { $object->uom2_internal_barcode = 'f'; }
		if($object->ass_type_3 == '') { $object->ass_type_3 = 'f'; }
		if($object->expiry_product == '') { $object->expiry_product = 'f'; }
		if($object->weight_product == '') { $object->weight_product = 'f'; }
		if($object->batch_product == '') { $object->batch_product = 'f'; }
		if($object->active_status == '') { $object->active_status = 'f'; }
		
		if($object->margin == '') { $object->margin = '0'; }
		if($object->discount == '') { $object->discount = '0'; }
		if($object->actual_cost == '') { $object->actual_cost = '0'; }
		if($object->last_cost == '') { $object->last_cost = '0'; }
		if($object->material_cost == '') { $object->material_cost = '0'; }
		if($object->overhead_cost == '') { $object->overhead_cost = '0'; }
		if($object->labour_cost == '') { $object->labour_cost = '0'; }
		if($object->uom1_width == '') { $object->uom1_width = '0'; }
		if($object->uom2_width == '') { $object->uom2_width = '0'; }
		if($object->uom1_length == '') { $object->uom1_length = '0'; }
		if($object->uom2_length == '') { $object->uom2_length = '0'; }
		if($object->uom1_height == '') { $object->uom1_height = '0'; }
		if($object->uom2_height == '') { $object->uom2_height = '0'; }
		if($object->uom1_weight == '') { $object->uom1_weight = '0'; }
		if($object->uom2_weight == '') { $object->uom2_weight = '0'; }
		if($object->uom1_harga == '') { $object->uom1_harga = '0'; }
		if($object->uom2_harga == '') { $object->uom2_harga = '0'; }
		if($object->tipe == '') { $object->tipe = ''; }
		if($object->ppob == '') { $object->ppob = '0'; }
		if($object->uom1_prod_nm == '') { $object->uom1_prod_nm = ''; }
		if($object->uom2_prod_nm == '') { $object->uom2_prod_nm = ''; }
		if($object->jenis == '') { $object->jenis = ''; }
		if($object->status_flag == '') { $object->status_flag = 'true'; }

		$sql = "INSERT INTO \"m_product\" (\"stock_id\", \"description\", \"long_description\" "
				.", \"actual_cost\", \"last_cost\", \"material_cost\", \"labour_cost\", \"overhead_cost\" "
				.", \"inactive\", \"bumun_cd\", \"l1_cd\", \"l2_cd\", \"l3_cd\", \"l4_cd\" "
				.", \"uom1_internal_barcode\", \"uom2_internal_barcode\" "
				.", \"margin\", \"uom1_nm\", \"uom2_nm\" "
				.",	\"uom1_conversion\", \"uom2_conversion\", \"uom1_width\", \"uom2_width\" " 
				.", \"uom1_length\", \"uom2_length\", \"uom1_height\", \"uom2_height\" " 
				.", \"uom1_weight\", \"uom2_weight\", \"uom1_harga\", \"uom2_harga\", \"ppob\", \"status\", \"tipe\", \"jenis\" ) "
				." VALUES ('".$object->stock_id."','".pg_escape_string($object->description)."','".pg_escape_string($object->long_description)."','"
				.$object->actual_cost."','".$object->last_cost."','".$object->material_cost."','".$object->labour_cost."','".$object->overhead_cost."','"
				.$object->inactive."','".$object->bumun_cd."','".$object->l1_cd."','".$object->l2_cd."','".$object->l3_cd."','".$object->l4_cd."','"
				.$object->uom1_internal_barcode."','".$object->uom2_internal_barcode."','"
				.$object->margin."','".$object->uom1_nm."','".$object->uom2_nm."','"
				.$object->uom1_conversion."','".$object->uom2_conversion."','".$object->uom1_width."','".$object->uom2_width."','"
				.$object->uom1_length."','".$object->uom2_length."','".$object->uom1_height."','".$object->uom2_height."','"
				.$object->uom1_weight."','".$object->uom2_weight."','".$object->uom1_harga."','".$object->uom2_harga."','".$object->ppob."','".$object->status."','".$object->tipe."','".$object->uom1_prod_nm."','".$object->uom2_prod_nm."','".$object->jenis."','".$object->status_flag."') ";
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goMultiInsertSync($field, $value){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "INSERT INTO \"m_product\" ( ".$field." ) "		
				." VALUES ".$value;
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_product\" SET ";
		$flag = false;		
		if($object->category_id != '') { if($flag){ $sql .= ","; } $sql .= " \"category_id\" = '".$object->category_id."' "; $flag = true; }
		if($object->tax_type_id != '') { if($flag){ $sql .= ","; } $sql .= " \"tax_type_id\" = '".$object->tax_type_id."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".pg_escape_string($object->description)."' "; $flag = true; }
		if($object->long_description != '') { if($flag){ $sql .= ","; } $sql .= " \"long_description\" = '".pg_escape_string($object->long_description)."' "; $flag = true; }
		if($object->units != '') { if($flag){ $sql .= ","; } $sql .= " \"units\" = '".$object->units."' "; $flag = true; }
		if($object->actual_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"actual_cost\" = '".$object->actual_cost."' "; $flag = true; }
		if($object->last_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"last_cost\" = '".$object->last_cost."' "; $flag = true; }
		if($object->material_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"material_cost\" = '".$object->material_cost."' "; $flag = true; }
		if($object->labour_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"labour_cost\" = '".$object->labour_cost."' "; $flag = true; }
		if($object->overhead_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"overhead_cost\" = '".$object->overhead_cost."' "; $flag = true; }
		if($object->inactive != '') { if($flag){ $sql .= ","; } $sql .= " \"inactive\" = '".$object->inactive."' "; $flag = true; }
		if($object->no_sale != '') { if($flag){ $sql .= ","; } $sql .= " \"no_sale\" = '".$object->no_sale."' "; $flag = true; }
		if($object->editable != '') { if($flag){ $sql .= ","; } $sql .= " \"editable\" = '".$object->editable."' "; $flag = true; }
		if($object->jenis_item != '') { if($flag){ $sql .= ","; } $sql .= " \"jenis_item\" = '".$object->jenis_item."' "; $flag = true; }
		if($object->bumun_cd != '') { if($flag){ $sql .= ","; } $sql .= " \"bumun_cd\" = '".$object->bumun_cd."' "; $flag = true; }
		if($object->l1_cd != '') { if($flag){ $sql .= ","; } $sql .= " \"l1_cd\" = '".$object->l1_cd."' "; $flag = true; }
		if($object->l2_cd != '') { if($flag){ $sql .= ","; } $sql .= " \"l2_cd\" = '".$object->l2_cd."' "; $flag = true; }
		if($object->l3_cd != '') { if($flag){ $sql .= ","; } $sql .= " \"l3_cd\" = '".$object->l3_cd."' "; $flag = true; }
		if($object->l4_cd != '') { if($flag){ $sql .= ","; } $sql .= " \"l4_cd\" = '".$object->l4_cd."' "; $flag = true; }
		if($object->margin != '') { if($flag){ $sql .= ","; } $sql .= " \"margin\" = '".$object->margin."' "; $flag = true; }
		if($object->moving_type != '') { if($flag){ $sql .= ","; } $sql .= " \"moving_type\" = '".$object->moving_type."' "; $flag = true; }
		if($object->ass_type_1 != '') { if($flag){ $sql .= ","; } $sql .= " \"ass_type_1\" = '".$object->ass_type_1."' "; $flag = true; }
		if($object->ass_type_2 != '') { if($flag){ $sql .= ","; } $sql .= " \"ass_type_2\" = '".$object->ass_type_2."' "; $flag = true; }
		if($object->ass_type_3 != '') { if($flag){ $sql .= ","; } $sql .= " \"ass_type_3\" = '".$object->ass_type_3."' "; $flag = true; }
		if($object->expiry_product != '') { if($flag){ $sql .= ","; } $sql .= " \"expiry_product\" = '".$object->expiry_product."' "; $flag = true; }
		if($object->weight_product != '') { if($flag){ $sql .= ","; } $sql .= " \"weight_product\" = '".$object->weight_product."' "; $flag = true; }
		if($object->batch_product != '') { if($flag){ $sql .= ","; } $sql .= " \"batch_product\" = '".$object->batch_product."' "; $flag = true; }
		if($object->active_status != '') { if($flag){ $sql .= ","; } $sql .= " \"active_status\" = '".$object->active_status."' "; $flag = true; }
		if($object->kode_1 != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_1\" = '".$object->kode_1."' "; $flag = true; }
		if($object->kode_2 != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_2\" = '".$object->kode_2."' "; $flag = true; }
		if($object->kode_3 != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_3\" = '".$object->kode_3."' "; $flag = true; }
		if($object->discount != '') { if($flag){ $sql .= ","; } $sql .= " \"discount\" = '".$object->discount."' "; $flag = true; }
		if($object->supplied_by != '') { if($flag){ $sql .= ","; } $sql .= " \"supplied_by\" = '".$object->supplied_by."' "; $flag = true; }
		if($object->uom1_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_nm\" = '".$object->uom1_nm."' "; $flag = true; }
		if($object->uom1_htype_1 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_htype_1\" = '".$object->uom1_htype_1."' "; $flag = true; }
		if($object->uom1_htype_2 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_htype_2\" = '".$object->uom1_htype_2."' "; $flag = true; }
		if($object->uom1_htype_3 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_htype_3\" = '".$object->uom1_htype_3."' "; $flag = true; }
		if($object->uom1_htype_4 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_htype_4\" = '".$object->uom1_htype_4."' "; $flag = true; }
		if($object->uom1_htype_5 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_htype_5\" = '".$object->uom1_htype_5."' "; $flag = true; }
		if($object->uom1_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_rack_type\" = '".$object->uom1_rack_type."' "; $flag = true; }
		if($object->uom1_shelfing_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_shelfing_rack_type\" = '".$object->uom1_shelfing_rack_type."' "; $flag = true; }
		if($object->uom1_internal_barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_internal_barcode\" = '".$object->uom1_internal_barcode."' "; $flag = true; }
		if($object->uom1_width != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_width\" = '".$object->uom1_width."' "; $flag = true; }
		if($object->uom1_length != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_length\" = '".$object->uom1_length."' "; $flag = true; }
		if($object->uom1_height != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_height\" = '".$object->uom1_height."' "; $flag = true; }
		if($object->uom1_weight != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_weight\" = '".$object->uom1_weight."' "; $flag = true; }
		if($object->uom1_margin != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_margin\" = '".$object->uom1_margin."' "; $flag = true; }
		if($object->uom1_box_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_box_type\" = '".$object->uom1_box_type."' "; $flag = true; }
		if($object->uom1_harga != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_harga\" = '".$object->uom1_harga."' "; $flag = true; }
		if($object->uom2_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_nm\" = '".$object->uom2_nm."' "; $flag = true; }
		if($object->uom2_htype_1 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_htype_1\" = '".$object->uom2_htype_1."' "; $flag = true; }
		if($object->uom2_htype_2 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_htype_2\" = '".$object->uom2_htype_2."' "; $flag = true; }
		if($object->uom2_htype_3 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_htype_3\" = '".$object->uom2_htype_3."' "; $flag = true; }
		if($object->uom2_htype_4 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_htype_4\" = '".$object->uom2_htype_4."' "; $flag = true; }
		if($object->uom2_htype_5 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_htype_5\" = '".$object->uom2_htype_5."' "; $flag = true; }
		if($object->uom2_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_rack_type\" = '".$object->uom2_rack_type."' "; $flag = true; }
		if($object->uom2_shelfing_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_shelfing_rack_type\" = '".$object->uom2_shelfing_rack_type."' "; $flag = true; }
		if($object->uom2_conversion != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_conversion\" = '".$object->uom2_conversion."' "; $flag = true; }
		if($object->uom2_internal_barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_internal_barcode\" = '".$object->uom2_internal_barcode."' "; $flag = true; }
		if($object->uom2_width != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_width\" = '".$object->uom2_width."' "; $flag = true; }
		if($object->uom2_length != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_length\" = '".$object->uom2_length."' "; $flag = true; }
		if($object->uom2_height != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_height\" = '".$object->uom2_height."' "; $flag = true; }
		if($object->uom2_weight != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_weight\" = '".$object->uom2_weight."' "; $flag = true; }
		if($object->uom2_margin != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_margin\" = '".$object->uom2_margin."' "; $flag = true; }
		if($object->uom2_box_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_box_type\" = '".$object->uom2_box_type."' "; $flag = true; }
		if($object->uom2_harga != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_harga\" = '".$object->uom2_harga."' "; $flag = true; }
		if($object->uom3_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_nm\" = '".$object->uom3_nm."' "; $flag = true; }
		if($object->uom3_htype_1 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_htype_1\" = '".$object->uom3_htype_1."' "; $flag = true; }
		if($object->uom3_htype_2 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_htype_2\" = '".$object->uom3_htype_2."' "; $flag = true; }
		if($object->uom3_htype_3 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_htype_3\" = '".$object->uom3_htype_3."' "; $flag = true; }
		if($object->uom3_htype_4 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_htype_4\" = '".$object->uom3_htype_4."' "; $flag = true; }
		if($object->uom3_htype_5 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_htype_5\" = '".$object->uom3_htype_5."' "; $flag = true; }
		if($object->uom3_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_rack_type\" = '".$object->uom3_rack_type."' "; $flag = true; }
		if($object->uom3_shelfing_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_shelfing_rack_type\" = '".$object->uom3_shelfing_rack_type."' "; $flag = true; }
		if($object->uom3_conversion != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_conversion\" = '".$object->uom3_conversion."' "; $flag = true; }
		if($object->uom3_internal_barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_internal_barcode\" = '".$object->uom3_internal_barcode."' "; $flag = true; }
		if($object->uom3_width != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_width\" = '".$object->uom3_width."' "; $flag = true; }
		if($object->uom3_length != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_length\" = '".$object->uom3_length."' "; $flag = true; }
		if($object->uom3_height != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_height\" = '".$object->uom3_height."' "; $flag = true; }
		if($object->uom3_weight != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_weight\" = '".$object->uom3_weight."' "; $flag = true; }
		if($object->uom3_margin != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_margin\" = '".$object->uom3_margin."' "; $flag = true; }
		if($object->uom3_box_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_box_type\" = '".$object->uom3_box_type."' "; $flag = true; }
		if($object->uom3_harga != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_harga\" = '".$object->uom3_harga."' "; $flag = true; }
		if($object->uom4_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_nm\" = '".$object->uom4_nm."' "; $flag = true; }
		if($object->uom4_htype_1 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_htype_1\" = '".$object->uom4_htype_1."' "; $flag = true; }
		if($object->uom4_htype_2 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_htype_2\" = '".$object->uom4_htype_2."' "; $flag = true; }
		if($object->uom4_htype_3 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_htype_3\" = '".$object->uom4_htype_3."' "; $flag = true; }
		if($object->uom4_htype_4 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_htype_4\" = '".$object->uom4_htype_4."' "; $flag = true; }
		if($object->uom4_htype_5 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_htype_5\" = '".$object->uom4_htype_5."' "; $flag = true; }
		if($object->uom4_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_rack_type\" = '".$object->uom4_rack_type."' "; $flag = true; }
		if($object->uom4_shelfing_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_shelfing_rack_type\" = '".$object->uom4_shelfing_rack_type."' "; $flag = true; }
		if($object->uom4_conversion != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_conversion\" = '".$object->uom4_conversion."' "; $flag = true; }
		if($object->uom4_internal_barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_internal_barcode\" = '".$object->uom4_internal_barcode."' "; $flag = true; }
		if($object->uom4_width != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_width\" = '".$object->uom4_width."' "; $flag = true; }
		if($object->uom4_length != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_length\" = '".$object->uom4_length."' "; $flag = true; }
		if($object->uom4_height != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_height\" = '".$object->uom4_height."' "; $flag = true; }
		if($object->uom4_weight != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_weight\" = '".$object->uom4_weight."' "; $flag = true; }
		if($object->uom4_margin != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_margin\" = '".$object->uom4_margin."' "; $flag = true; }
		if($object->uom4_box_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_box_type\" = '".$object->uom4_box_type."' "; $flag = true; }
		if($object->uom4_harga != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_harga\" = '".$object->uom4_harga."' "; $flag = true; }
		if($object->uom1_prod_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_prod_nm\" = '".$object->uom1_prod_nm."' "; $flag = true; }
		if($object->uom2_prod_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_prod_nm\" = '".$object->uom2_prod_nm."' "; $flag = true; }
		if($object->uom3_prod_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_prod_nm\" = '".$object->uom3_prod_nm."' "; $flag = true; }
		if($object->uom4_prod_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_prod_nm\" = '".$object->uom4_prod_nm."' "; $flag = true; }
		if($object->jenis != '') { if($flag){ $sql .= ","; } $sql .= " \"jenis\" = '".$object->jenis."' "; $flag = true; }
		if($object->status_flag != '') { if($flag){ $sql .= ","; } $sql .= " \"status_flag\" = '".$object->status_flag."' "; $flag = true; }
		$sql .= " WHERE \"stock_id\" = '".$object->stock_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}

	public function goUpdate2($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_product\" SET ";
		$flag = false;		
		if($object->category_id != '') { if($flag){ $sql .= ","; } $sql .= " \"category_id\" = '".$object->category_id."' "; $flag = true; }
		if($object->tax_type_id != '') { if($flag){ $sql .= ","; } $sql .= " \"tax_type_id\" = '".$object->tax_type_id."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".pg_escape_string($object->description)."' "; $flag = true; }
		if($object->long_description != '') { if($flag){ $sql .= ","; } $sql .= " \"long_description\" = '".pg_escape_string($object->long_description)."' "; $flag = true; }
		if($object->units != '') { if($flag){ $sql .= ","; } $sql .= " \"units\" = '".$object->units."' "; $flag = true; }
		if($object->actual_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"actual_cost\" = '".$object->actual_cost."' "; $flag = true; }
		if($object->last_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"last_cost\" = '".$object->last_cost."' "; $flag = true; }
		if($object->material_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"material_cost\" = '".$object->material_cost."' "; $flag = true; }
		if($object->labour_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"labour_cost\" = '".$object->labour_cost."' "; $flag = true; }
		if($object->overhead_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"overhead_cost\" = '".$object->overhead_cost."' "; $flag = true; }
		if($object->inactive != '') { if($flag){ $sql .= ","; } $sql .= " \"inactive\" = '".$object->inactive."' "; $flag = true; }
		if($object->no_sale != '') { if($flag){ $sql .= ","; } $sql .= " \"no_sale\" = '".$object->no_sale."' "; $flag = true; }
		if($object->editable != '') { if($flag){ $sql .= ","; } $sql .= " \"editable\" = '".$object->editable."' "; $flag = true; }
		if($object->jenis_item != '') { if($flag){ $sql .= ","; } $sql .= " \"jenis_item\" = '".$object->jenis_item."' "; $flag = true; }
		if($object->bumun_cd != '') { if($flag){ $sql .= ","; } $sql .= " \"bumun_cd\" = '".$object->bumun_cd."' "; $flag = true; }
		if($object->l1_cd != '') { if($flag){ $sql .= ","; } $sql .= " \"l1_cd\" = '".$object->l1_cd."' "; $flag = true; }
		if($object->l2_cd != '') { if($flag){ $sql .= ","; } $sql .= " \"l2_cd\" = '".$object->l2_cd."' "; $flag = true; }
		if($object->l3_cd != '') { if($flag){ $sql .= ","; } $sql .= " \"l3_cd\" = '".$object->l3_cd."' "; $flag = true; }
		if($object->l4_cd != '') { if($flag){ $sql .= ","; } $sql .= " \"l4_cd\" = '".$object->l4_cd."' "; $flag = true; }
		if($object->margin != '') { if($flag){ $sql .= ","; } $sql .= " \"margin\" = '".$object->margin."' "; $flag = true; }
		if($object->moving_type != '') { if($flag){ $sql .= ","; } $sql .= " \"moving_type\" = '".$object->moving_type."' "; $flag = true; }
		if($object->ass_type_1 != '') { if($flag){ $sql .= ","; } $sql .= " \"ass_type_1\" = '".$object->ass_type_1."' "; $flag = true; }
		if($object->ass_type_2 != '') { if($flag){ $sql .= ","; } $sql .= " \"ass_type_2\" = '".$object->ass_type_2."' "; $flag = true; }
		if($object->ass_type_3 != '') { if($flag){ $sql .= ","; } $sql .= " \"ass_type_3\" = '".$object->ass_type_3."' "; $flag = true; }
		if($object->expiry_product != '') { if($flag){ $sql .= ","; } $sql .= " \"expiry_product\" = '".$object->expiry_product."' "; $flag = true; }
		if($object->weight_product != '') { if($flag){ $sql .= ","; } $sql .= " \"weight_product\" = '".$object->weight_product."' "; $flag = true; }
		if($object->batch_product != '') { if($flag){ $sql .= ","; } $sql .= " \"batch_product\" = '".$object->batch_product."' "; $flag = true; }
		if($object->active_status != '') { if($flag){ $sql .= ","; } $sql .= " \"active_status\" = '".$object->active_status."' "; $flag = true; }
		if($object->kode_1 != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_1\" = '".$object->kode_1."' "; $flag = true; }
		if($object->kode_2 != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_2\" = '".$object->kode_2."' "; $flag = true; }
		if($object->kode_3 != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_3\" = '".$object->kode_3."' "; $flag = true; }
		if($object->discount != '') { if($flag){ $sql .= ","; } $sql .= " \"discount\" = '".$object->discount."' "; $flag = true; }
		if($object->supplied_by != '') { if($flag){ $sql .= ","; } $sql .= " \"supplied_by\" = '".$object->supplied_by."' "; $flag = true; }
		if($object->uom1_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_nm\" = '".$object->uom1_nm."' "; $flag = true; }
		if($object->uom1_htype_1 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_htype_1\" = '".$object->uom1_htype_1."' "; $flag = true; }
		if($object->uom1_htype_2 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_htype_2\" = '".$object->uom1_htype_2."' "; $flag = true; }
		if($object->uom1_htype_3 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_htype_3\" = '".$object->uom1_htype_3."' "; $flag = true; }
		if($object->uom1_htype_4 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_htype_4\" = '".$object->uom1_htype_4."' "; $flag = true; }
		if($object->uom1_htype_5 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_htype_5\" = '".$object->uom1_htype_5."' "; $flag = true; }
		if($object->uom1_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_rack_type\" = '".$object->uom1_rack_type."' "; $flag = true; }
		if($object->uom1_shelfing_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_shelfing_rack_type\" = '".$object->uom1_shelfing_rack_type."' "; $flag = true; }
		if($object->uom1_internal_barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_internal_barcode\" = '".$object->uom1_internal_barcode."' "; $flag = true; }
		if($object->uom1_width != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_width\" = '".$object->uom1_width."' "; $flag = true; }
		if($object->uom1_length != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_length\" = '".$object->uom1_length."' "; $flag = true; }
		if($object->uom1_height != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_height\" = '".$object->uom1_height."' "; $flag = true; }
		if($object->uom1_weight != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_weight\" = '".$object->uom1_weight."' "; $flag = true; }
		if($object->uom1_margin != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_margin\" = '".$object->uom1_margin."' "; $flag = true; }
		if($object->uom1_box_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_box_type\" = '".$object->uom1_box_type."' "; $flag = true; }
		if($object->uom1_harga != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_harga\" = '".$object->uom1_harga."' "; $flag = true; }
		if($object->uom2_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_nm\" = '".$object->uom2_nm."' "; $flag = true; }
		if($object->uom2_htype_1 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_htype_1\" = '".$object->uom2_htype_1."' "; $flag = true; }
		if($object->uom2_htype_2 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_htype_2\" = '".$object->uom2_htype_2."' "; $flag = true; }
		if($object->uom2_htype_3 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_htype_3\" = '".$object->uom2_htype_3."' "; $flag = true; }
		if($object->uom2_htype_4 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_htype_4\" = '".$object->uom2_htype_4."' "; $flag = true; }
		if($object->uom2_htype_5 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_htype_5\" = '".$object->uom2_htype_5."' "; $flag = true; }
		if($object->uom2_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_rack_type\" = '".$object->uom2_rack_type."' "; $flag = true; }
		if($object->uom2_shelfing_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_shelfing_rack_type\" = '".$object->uom2_shelfing_rack_type."' "; $flag = true; }
		if($object->uom2_conversion != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_conversion\" = '".$object->uom2_conversion."' "; $flag = true; }
		if($object->uom2_internal_barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_internal_barcode\" = '".$object->uom2_internal_barcode."' "; $flag = true; }
		if($object->uom2_width != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_width\" = '".$object->uom2_width."' "; $flag = true; }
		if($object->uom2_length != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_length\" = '".$object->uom2_length."' "; $flag = true; }
		if($object->uom2_height != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_height\" = '".$object->uom2_height."' "; $flag = true; }
		if($object->uom2_weight != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_weight\" = '".$object->uom2_weight."' "; $flag = true; }
		if($object->uom2_margin != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_margin\" = '".$object->uom2_margin."' "; $flag = true; }
		if($object->uom2_box_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_box_type\" = '".$object->uom2_box_type."' "; $flag = true; }
		if($object->uom2_harga != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_harga\" = '".$object->uom2_harga."' "; $flag = true; }
		if($object->uom3_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_nm\" = '".$object->uom3_nm."' "; $flag = true; }
		if($object->uom3_htype_1 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_htype_1\" = '".$object->uom3_htype_1."' "; $flag = true; }
		if($object->uom3_htype_2 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_htype_2\" = '".$object->uom3_htype_2."' "; $flag = true; }
		if($object->uom3_htype_3 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_htype_3\" = '".$object->uom3_htype_3."' "; $flag = true; }
		if($object->uom3_htype_4 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_htype_4\" = '".$object->uom3_htype_4."' "; $flag = true; }
		if($object->uom3_htype_5 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_htype_5\" = '".$object->uom3_htype_5."' "; $flag = true; }
		if($object->uom3_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_rack_type\" = '".$object->uom3_rack_type."' "; $flag = true; }
		if($object->uom3_shelfing_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_shelfing_rack_type\" = '".$object->uom3_shelfing_rack_type."' "; $flag = true; }
		if($object->uom3_conversion != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_conversion\" = '".$object->uom3_conversion."' "; $flag = true; }
		if($object->uom3_internal_barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_internal_barcode\" = '".$object->uom3_internal_barcode."' "; $flag = true; }
		if($object->uom3_width != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_width\" = '".$object->uom3_width."' "; $flag = true; }
		if($object->uom3_length != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_length\" = '".$object->uom3_length."' "; $flag = true; }
		if($object->uom3_height != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_height\" = '".$object->uom3_height."' "; $flag = true; }
		if($object->uom3_weight != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_weight\" = '".$object->uom3_weight."' "; $flag = true; }
		if($object->uom3_margin != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_margin\" = '".$object->uom3_margin."' "; $flag = true; }
		if($object->uom3_box_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_box_type\" = '".$object->uom3_box_type."' "; $flag = true; }
		if($object->uom3_harga != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_harga\" = '".$object->uom3_harga."' "; $flag = true; }
		if($object->uom4_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_nm\" = '".$object->uom4_nm."' "; $flag = true; }
		if($object->uom4_htype_1 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_htype_1\" = '".$object->uom4_htype_1."' "; $flag = true; }
		if($object->uom4_htype_2 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_htype_2\" = '".$object->uom4_htype_2."' "; $flag = true; }
		if($object->uom4_htype_3 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_htype_3\" = '".$object->uom4_htype_3."' "; $flag = true; }
		if($object->uom4_htype_4 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_htype_4\" = '".$object->uom4_htype_4."' "; $flag = true; }
		if($object->uom4_htype_5 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_htype_5\" = '".$object->uom4_htype_5."' "; $flag = true; }
		if($object->uom4_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_rack_type\" = '".$object->uom4_rack_type."' "; $flag = true; }
		if($object->uom4_shelfing_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_shelfing_rack_type\" = '".$object->uom4_shelfing_rack_type."' "; $flag = true; }
		if($object->uom4_conversion != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_conversion\" = '".$object->uom4_conversion."' "; $flag = true; }
		if($object->uom4_internal_barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_internal_barcode\" = '".$object->uom4_internal_barcode."' "; $flag = true; }
		if($object->uom4_width != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_width\" = '".$object->uom4_width."' "; $flag = true; }
		if($object->uom4_length != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_length\" = '".$object->uom4_length."' "; $flag = true; }
		if($object->uom4_height != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_height\" = '".$object->uom4_height."' "; $flag = true; }
		if($object->uom4_weight != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_weight\" = '".$object->uom4_weight."' "; $flag = true; }
		if($object->uom4_margin != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_margin\" = '".$object->uom4_margin."' "; $flag = true; }
		if($object->uom4_box_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_box_type\" = '".$object->uom4_box_type."' "; $flag = true; }
		if($object->uom4_harga != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_harga\" = '".$object->uom4_harga."' "; $flag = true; }
		if($object->uom1_prod_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_prod_nm\" = '".$object->uom1_prod_nm."' "; $flag = true; }
		if($object->uom2_prod_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_prod_nm\" = '".$object->uom2_prod_nm."' "; $flag = true; }
		if($object->uom3_prod_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_prod_nm\" = '".$object->uom3_prod_nm."' "; $flag = true; }
		if($object->uom4_prod_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_prod_nm\" = '".$object->uom4_prod_nm."' "; $flag = true; }
		if($object->status != '') { if($flag){ $sql .= ","; } $sql .= " \"status\" = '".$object->status."' "; $flag = true; }
		if($object->tipe != '') { if($flag){ $sql .= ","; } $sql .= " \"tipe\" = '".$object->tipe."' "; $flag = true; }
		if($object->ppob != '') { if($flag){ $sql .= ","; } $sql .= " \"ppob\" = '".$object->ppob."' "; $flag = true; }
		if($object->jenis != '') { if($flag){ $sql .= ","; } $sql .= " \"jenis\" = '".$object->jenis."' "; $flag = true; }
		if($object->status_flag != '') { if($flag){ $sql .= ","; } $sql .= " \"status_flag\" = '".$object->status_flag."' "; $flag = true; }
		$sql .= " WHERE \"stock_id\" = '".$object->stock_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}

	public static function insertSql($object)
	{
		if($object->inactive == '') { $object->inactive = '0'; }		
		if($object->category_id == '') { $object->category_id = '0'; }	
		if($object->tax_type_id == '') { $object->tax_type_id = '0'; }	
		if($object->no_sale == '') { $object->no_sale = '0'; }	
		if($object->editable == '') { $object->editable = '0'; }	
		if($object->jenis_item == '') { $object->jenis_item = '0'; }			
		
		if($object->uom1_internal_barcode == '') { $object->uom1_internal_barcode = 'f'; }
		if($object->uom2_internal_barcode == '') { $object->uom2_internal_barcode = 'f'; }
		if($object->ass_type_3 == '') { $object->ass_type_3 = 'f'; }
		if($object->expiry_product == '') { $object->expiry_product = 'f'; }
		if($object->weight_product == '') { $object->weight_product = 'f'; }
		if($object->batch_product == '') { $object->batch_product = 'f'; }
		if($object->active_status == '') { $object->active_status = 'f'; }
		
		if($object->margin == '') { $object->margin = '0'; }
		if($object->discount == '') { $object->discount = '0'; }
		if($object->actual_cost == '') { $object->actual_cost = '0'; }
		if($object->last_cost == '') { $object->last_cost = '0'; }
		if($object->material_cost == '') { $object->material_cost = '0'; }
		if($object->overhead_cost == '') { $object->overhead_cost = '0'; }
		if($object->labour_cost == '') { $object->labour_cost = '0'; }
		if($object->uom1_width == '') { $object->uom1_width = '0'; }
		if($object->uom2_width == '') { $object->uom2_width = '0'; }
		if($object->uom1_length == '') { $object->uom1_length = '0'; }
		if($object->uom2_length == '') { $object->uom2_length = '0'; }
		if($object->uom1_height == '') { $object->uom1_height = '0'; }
		if($object->uom2_height == '') { $object->uom2_height = '0'; }
		if($object->uom1_weight == '') { $object->uom1_weight = '0'; }
		if($object->uom2_weight == '') { $object->uom2_weight = '0'; }
		if($object->uom1_harga == '') { $object->uom1_harga = '0'; }
		if($object->uom2_harga == '') { $object->uom2_harga = '0'; }
		if($object->tipe == '') { $object->tipe = ''; }
		if($object->ppob == '') { $object->ppob = '0'; }
		if($object->uom1_prod_nm == '') { $object->uom1_prod_nm = ''; }
		if($object->uom2_prod_nm == '') { $object->uom2_prod_nm = ''; }
		if($object->jenis == '') { $object->jenis = ''; }
		if($object->status_flag == '') { $object->status_flag = '1'; }

		$sql = "INSERT INTO \"m_product\" (\"stock_id\", \"description\", \"long_description\" "
				.", \"actual_cost\", \"last_cost\", \"material_cost\", \"labour_cost\", \"overhead_cost\" "
				.", \"inactive\", \"bumun_cd\", \"l1_cd\", \"l2_cd\", \"l3_cd\", \"l4_cd\" "
				.", \"uom1_internal_barcode\", \"uom2_internal_barcode\" "
				.", \"margin\", \"uom1_nm\", \"uom2_nm\" "
				.",	\"uom1_conversion\", \"uom2_conversion\", \"uom1_width\", \"uom2_width\" " 
				.", \"uom1_length\", \"uom2_length\", \"uom1_height\", \"uom2_height\" " 
				.", \"uom1_weight\", \"uom2_weight\", \"uom1_harga\", \"uom2_harga\", \"ppob\", \"status\", \"tipe\", \"uom1_prod_nm\", \"uom2_prod_nm\", \"jenis\", \"status_flag\" ) "
				." VALUES ('".$object->stock_id."','".pg_escape_string($object->description)."','".pg_escape_string($object->description)."','"
				.$object->actual_cost."','".$object->last_cost."','".$object->material_cost."','".$object->labour_cost."','".$object->overhead_cost."','"
				.$object->inactive."','".$object->bumun_cd."','".$object->l1_cd."','".$object->l2_cd."','".$object->l3_cd."','".$object->l4_cd."','"
				.$object->uom1_internal_barcode."','".$object->uom2_internal_barcode."','"
				.$object->margin."','".$object->uom1_nm."','".$object->uom2_nm."','"
				.$object->uom1_conversion."','".$object->uom2_conversion."','".$object->uom1_width."','".$object->uom2_width."','"
				.$object->uom1_length."','".$object->uom2_length."','".$object->uom1_height."','".$object->uom2_height."','"
				.$object->uom1_weight."','".$object->uom2_weight."','".$object->uom1_harga."','".$object->uom2_harga."','".$object->ppob."','".$object->status."','".$object->tipe."','".pg_escape_string($object->uom1_prod_nm)."','".pg_escape_string($object->uom2_prod_nm)."','".$object->jenis."','".$object->status_flag."'); ";

		return $sql;
	}

	public static function updateSql($object)
	{
		$sql = " UPDATE \"m_product\" SET ";
		$flag = false;		
		if($object->status_flag == '') { $object->status_flag = '1'; }
		if($object->status_flag == 0) { $object->status_flag = '1'; }
		if($object->ppob == 0) { $object->ppob = '0'; }
		if($object->ppob == '') { $object->ppob = '0'; }
		if($object->category_id != '') { if($flag){ $sql .= ","; } $sql .= " \"category_id\" = '".$object->category_id."' "; $flag = true; }
		if($object->tax_type_id != '') { if($flag){ $sql .= ","; } $sql .= " \"tax_type_id\" = '".$object->tax_type_id."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".pg_escape_string($object->description)."' "; $flag = true; }
		if($object->long_description != '') { if($flag){ $sql .= ","; } $sql .= " \"long_description\" = '".pg_escape_string($object->long_description)."' "; $flag = true; }
		if($object->units != '') { if($flag){ $sql .= ","; } $sql .= " \"units\" = '".$object->units."' "; $flag = true; }
		if($object->actual_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"actual_cost\" = '".$object->actual_cost."' "; $flag = true; }
		if($object->last_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"last_cost\" = '".$object->last_cost."' "; $flag = true; }
		if($object->material_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"material_cost\" = '".$object->material_cost."' "; $flag = true; }
		if($object->labour_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"labour_cost\" = '".$object->labour_cost."' "; $flag = true; }
		if($object->overhead_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"overhead_cost\" = '".$object->overhead_cost."' "; $flag = true; }
		if($object->inactive != '') { if($flag){ $sql .= ","; } $sql .= " \"inactive\" = '".$object->inactive."' "; $flag = true; }
		if($object->no_sale != '') { if($flag){ $sql .= ","; } $sql .= " \"no_sale\" = '".$object->no_sale."' "; $flag = true; }
		if($object->editable != '') { if($flag){ $sql .= ","; } $sql .= " \"editable\" = '".$object->editable."' "; $flag = true; }
		if($object->jenis_item != '') { if($flag){ $sql .= ","; } $sql .= " \"jenis_item\" = '".$object->jenis_item."' "; $flag = true; }
		if($object->bumun_cd != '') { if($flag){ $sql .= ","; } $sql .= " \"bumun_cd\" = '".$object->bumun_cd."' "; $flag = true; }
		if($object->l1_cd != '') { if($flag){ $sql .= ","; } $sql .= " \"l1_cd\" = '".$object->l1_cd."' "; $flag = true; }
		if($object->l2_cd != '') { if($flag){ $sql .= ","; } $sql .= " \"l2_cd\" = '".$object->l2_cd."' "; $flag = true; }
		if($object->l3_cd != '') { if($flag){ $sql .= ","; } $sql .= " \"l3_cd\" = '".$object->l3_cd."' "; $flag = true; }
		if($object->l4_cd != '') { if($flag){ $sql .= ","; } $sql .= " \"l4_cd\" = '".$object->l4_cd."' "; $flag = true; }
		if($object->margin != '') { if($flag){ $sql .= ","; } $sql .= " \"margin\" = '".$object->margin."' "; $flag = true; }
		if($object->moving_type != '') { if($flag){ $sql .= ","; } $sql .= " \"moving_type\" = '".$object->moving_type."' "; $flag = true; }
		if($object->ass_type_1 != '') { if($flag){ $sql .= ","; } $sql .= " \"ass_type_1\" = '".$object->ass_type_1."' "; $flag = true; }
		if($object->ass_type_2 != '') { if($flag){ $sql .= ","; } $sql .= " \"ass_type_2\" = '".$object->ass_type_2."' "; $flag = true; }
		if($object->ass_type_3 != '') { if($flag){ $sql .= ","; } $sql .= " \"ass_type_3\" = '".$object->ass_type_3."' "; $flag = true; }
		if($object->expiry_product != '') { if($flag){ $sql .= ","; } $sql .= " \"expiry_product\" = '".$object->expiry_product."' "; $flag = true; }
		if($object->weight_product != '') { if($flag){ $sql .= ","; } $sql .= " \"weight_product\" = '".$object->weight_product."' "; $flag = true; }
		if($object->batch_product != '') { if($flag){ $sql .= ","; } $sql .= " \"batch_product\" = '".$object->batch_product."' "; $flag = true; }
		if($object->active_status != '') { if($flag){ $sql .= ","; } $sql .= " \"active_status\" = '".$object->active_status."' "; $flag = true; }
		if($object->kode_1 != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_1\" = '".$object->kode_1."' "; $flag = true; }
		if($object->kode_2 != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_2\" = '".$object->kode_2."' "; $flag = true; }
		if($object->kode_3 != '') { if($flag){ $sql .= ","; } $sql .= " \"kode_3\" = '".$object->kode_3."' "; $flag = true; }
		if($object->discount != '') { if($flag){ $sql .= ","; } $sql .= " \"discount\" = '".$object->discount."' "; $flag = true; }
		if($object->supplied_by != '') { if($flag){ $sql .= ","; } $sql .= " \"supplied_by\" = '".$object->supplied_by."' "; $flag = true; }
		if($object->uom1_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_nm\" = '".$object->uom1_nm."' "; $flag = true; }
		if($object->uom1_htype_1 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_htype_1\" = '".$object->uom1_htype_1."' "; $flag = true; }
		if($object->uom1_htype_2 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_htype_2\" = '".$object->uom1_htype_2."' "; $flag = true; }
		if($object->uom1_htype_3 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_htype_3\" = '".$object->uom1_htype_3."' "; $flag = true; }
		if($object->uom1_htype_4 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_htype_4\" = '".$object->uom1_htype_4."' "; $flag = true; }
		if($object->uom1_htype_5 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_htype_5\" = '".$object->uom1_htype_5."' "; $flag = true; }
		if($object->uom1_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_rack_type\" = '".$object->uom1_rack_type."' "; $flag = true; }
		if($object->uom1_shelfing_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_shelfing_rack_type\" = '".$object->uom1_shelfing_rack_type."' "; $flag = true; }
		if($object->uom1_internal_barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_internal_barcode\" = '".$object->uom1_internal_barcode."' "; $flag = true; }
		if($object->uom1_width != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_width\" = '".$object->uom1_width."' "; $flag = true; }
		if($object->uom1_length != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_length\" = '".$object->uom1_length."' "; $flag = true; }
		if($object->uom1_height != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_height\" = '".$object->uom1_height."' "; $flag = true; }
		if($object->uom1_weight != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_weight\" = '".$object->uom1_weight."' "; $flag = true; }
		if($object->uom1_margin != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_margin\" = '".$object->uom1_margin."' "; $flag = true; }
		if($object->uom1_box_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_box_type\" = '".$object->uom1_box_type."' "; $flag = true; }
		if($object->uom1_harga != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_harga\" = '".$object->uom1_harga."' "; $flag = true; }
		if($object->uom2_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_nm\" = '".$object->uom2_nm."' "; $flag = true; }
		if($object->uom2_htype_1 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_htype_1\" = '".$object->uom2_htype_1."' "; $flag = true; }
		if($object->uom2_htype_2 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_htype_2\" = '".$object->uom2_htype_2."' "; $flag = true; }
		if($object->uom2_htype_3 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_htype_3\" = '".$object->uom2_htype_3."' "; $flag = true; }
		if($object->uom2_htype_4 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_htype_4\" = '".$object->uom2_htype_4."' "; $flag = true; }
		if($object->uom2_htype_5 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_htype_5\" = '".$object->uom2_htype_5."' "; $flag = true; }
		if($object->uom2_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_rack_type\" = '".$object->uom2_rack_type."' "; $flag = true; }
		if($object->uom2_shelfing_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_shelfing_rack_type\" = '".$object->uom2_shelfing_rack_type."' "; $flag = true; }
		if($object->uom2_conversion != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_conversion\" = '".$object->uom2_conversion."' "; $flag = true; }
		if($object->uom2_internal_barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_internal_barcode\" = '".$object->uom2_internal_barcode."' "; $flag = true; }
		if($object->uom2_width != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_width\" = '".$object->uom2_width."' "; $flag = true; }
		if($object->uom2_length != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_length\" = '".$object->uom2_length."' "; $flag = true; }
		if($object->uom2_height != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_height\" = '".$object->uom2_height."' "; $flag = true; }
		if($object->uom2_weight != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_weight\" = '".$object->uom2_weight."' "; $flag = true; }
		if($object->uom2_margin != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_margin\" = '".$object->uom2_margin."' "; $flag = true; }
		if($object->uom2_box_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_box_type\" = '".$object->uom2_box_type."' "; $flag = true; }
		if($object->uom2_harga != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_harga\" = '".$object->uom2_harga."' "; $flag = true; }
		if($object->uom3_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_nm\" = '".$object->uom3_nm."' "; $flag = true; }
		if($object->uom3_htype_1 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_htype_1\" = '".$object->uom3_htype_1."' "; $flag = true; }
		if($object->uom3_htype_2 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_htype_2\" = '".$object->uom3_htype_2."' "; $flag = true; }
		if($object->uom3_htype_3 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_htype_3\" = '".$object->uom3_htype_3."' "; $flag = true; }
		if($object->uom3_htype_4 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_htype_4\" = '".$object->uom3_htype_4."' "; $flag = true; }
		if($object->uom3_htype_5 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_htype_5\" = '".$object->uom3_htype_5."' "; $flag = true; }
		if($object->uom3_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_rack_type\" = '".$object->uom3_rack_type."' "; $flag = true; }
		if($object->uom3_shelfing_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_shelfing_rack_type\" = '".$object->uom3_shelfing_rack_type."' "; $flag = true; }
		if($object->uom3_conversion != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_conversion\" = '".$object->uom3_conversion."' "; $flag = true; }
		if($object->uom3_internal_barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_internal_barcode\" = '".$object->uom3_internal_barcode."' "; $flag = true; }
		if($object->uom3_width != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_width\" = '".$object->uom3_width."' "; $flag = true; }
		if($object->uom3_length != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_length\" = '".$object->uom3_length."' "; $flag = true; }
		if($object->uom3_height != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_height\" = '".$object->uom3_height."' "; $flag = true; }
		if($object->uom3_weight != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_weight\" = '".$object->uom3_weight."' "; $flag = true; }
		if($object->uom3_margin != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_margin\" = '".$object->uom3_margin."' "; $flag = true; }
		if($object->uom3_box_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_box_type\" = '".$object->uom3_box_type."' "; $flag = true; }
		if($object->uom3_harga != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_harga\" = '".$object->uom3_harga."' "; $flag = true; }
		if($object->uom4_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_nm\" = '".$object->uom4_nm."' "; $flag = true; }
		if($object->uom4_htype_1 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_htype_1\" = '".$object->uom4_htype_1."' "; $flag = true; }
		if($object->uom4_htype_2 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_htype_2\" = '".$object->uom4_htype_2."' "; $flag = true; }
		if($object->uom4_htype_3 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_htype_3\" = '".$object->uom4_htype_3."' "; $flag = true; }
		if($object->uom4_htype_4 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_htype_4\" = '".$object->uom4_htype_4."' "; $flag = true; }
		if($object->uom4_htype_5 != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_htype_5\" = '".$object->uom4_htype_5."' "; $flag = true; }
		if($object->uom4_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_rack_type\" = '".$object->uom4_rack_type."' "; $flag = true; }
		if($object->uom4_shelfing_rack_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_shelfing_rack_type\" = '".$object->uom4_shelfing_rack_type."' "; $flag = true; }
		if($object->uom4_conversion != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_conversion\" = '".$object->uom4_conversion."' "; $flag = true; }
		if($object->uom4_internal_barcode != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_internal_barcode\" = '".$object->uom4_internal_barcode."' "; $flag = true; }
		if($object->uom4_width != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_width\" = '".$object->uom4_width."' "; $flag = true; }
		if($object->uom4_length != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_length\" = '".$object->uom4_length."' "; $flag = true; }
		if($object->uom4_height != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_height\" = '".$object->uom4_height."' "; $flag = true; }
		if($object->uom4_weight != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_weight\" = '".$object->uom4_weight."' "; $flag = true; }
		if($object->uom4_margin != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_margin\" = '".$object->uom4_margin."' "; $flag = true; }
		if($object->uom4_box_type != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_box_type\" = '".$object->uom4_box_type."' "; $flag = true; }
		if($object->uom4_harga != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_harga\" = '".$object->uom4_harga."' "; $flag = true; }
		if($object->uom1_prod_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_prod_nm\" = '".pg_escape_string($object->uom1_prod_nm)."' "; $flag = true; }
		if($object->uom2_prod_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_prod_nm\" = '".pg_escape_string($object->uom2_prod_nm)."' "; $flag = true; }
		if($object->uom3_prod_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_prod_nm\" = '".$object->uom3_prod_nm."' "; $flag = true; }
		if($object->uom4_prod_nm != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_prod_nm\" = '".$object->uom4_prod_nm."' "; $flag = true; }
		if($object->status != '') { if($flag){ $sql .= ","; } $sql .= " \"status\" = '".$object->status."' "; $flag = true; }
		if($object->tipe != '') { if($flag){ $sql .= ","; } $sql .= " \"tipe\" = '".$object->tipe."' "; $flag = true; }
		if($object->ppob != '') { if($flag){ $sql .= ","; } $sql .= " \"ppob\" = '".$object->ppob."' "; $flag = true; }
		if($object->jenis != '') { if($flag){ $sql .= ","; } $sql .= " \"jenis\" = '".$object->jenis."' "; $flag = true; }
		if($object->status_flag != '') { if($flag){ $sql .= ","; } $sql .= " \"status_flag\" = '".$object->status_flag."' "; $flag = true; }
		# code...
		$sql .= " WHERE \"stock_id\" = '".$object->stock_id."'; ";
		return $sql;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_product\" "
				." WHERE \"stock_id\" = '".$object->stock_id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goMultiDelete(){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_product\" "
				." ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}

	public function getJoin_Data($condition){
		$connection = new Postgresql($this->di['db']);

		$sql = " SELECT mp.\"stock_id\", mp.\"status_flag\" "
				." FROM \"m_product\" mp INNER JOIN \"m_product_move\" mpm ON (mp.stock_id = mpm.stock_id) 
				INNER JOIN m_product_price mpp ON (mp.stock_id = mpp.stock_id) ".$condition;

		$results = $connection->query($sql);
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_Data2($cond){
		$connection = new Postgresql($this->di['db']);

		$sql = " SELECT mp.\"uom1_prod_nm\", mpp.\"uom1_rounding_price\", mpp.\"uom1_changed_price\", mp.\"status_flag\"  "
				." FROM \"m_product\" mp INNER JOIN m_product_price mpp ON (mp.stock_id = mpp.stock_id) ".$cond;

		$results = $connection->query($sql);
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['uom1_prod_nm'] = $result[0];
			$lists[$count]['uom1_rounding_price'] = $result[1];
			$lists[$count]['uom1_changed_price'] = $result[2];
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_ProductMove($cond1){
		$connection = new Postgresql($this->di['db']);
		//dedy, edit, map.
		$sql = " SELECT mp.\"stock_id\", mp.\"uom1_prod_nm\", SUM(mpm.qty) as nilai_persediaan, mp.uom1_harga, mp.uom1_internal_barcode, "
				." 	CASE
						WHEN uom1_htype_5 IS true THEN uom1_internal_barcode
						WHEN uom2_htype_5 IS true THEN uom2_internal_barcode
						WHEN uom3_htype_5 IS true THEN uom3_internal_barcode
						WHEN uom4_htype_5 IS true THEN uom4_internal_barcode
						ELSE ''
					END 
						AS uom_ib, 
					mc.l1_nm, mc.bumun_nm, mp.\"tipe\", 
					CASE
						WHEN uom1_htype_5 IS true THEN uom1_cost_price
						WHEN uom2_htype_5 IS true THEN uom2_cost_price
						WHEN uom3_htype_5 IS true THEN uom3_cost_price
						WHEN uom4_htype_5 IS true THEN uom4_cost_price
						WHEN uom1_htype_1 IS true THEN uom1_cost_price
						WHEN uom2_htype_1 IS true THEN uom2_cost_price
						WHEN uom3_htype_1 IS true THEN uom3_cost_price
						WHEN uom4_htype_1 IS true THEN uom4_cost_price
						ELSE 0
					END 
						AS map, mp.\"status_flag\"	"
				." FROM \"m_product\" mp INNER JOIN \"m_product_move\" mpm ON mp.stock_id = mpm.stock_id 
				INNER JOIN m_product_price mpp ON mp.stock_id = mpp.stock_id 
				INNER JOIN m_category mc ON mp.l1_cd = mc.l1_cd ".$cond1."
				GROUP BY mp.stock_id, mc.l1_nm, mc.bumun_nm, mp.uom1_harga, mpp.uom1_cost_price, mpp.uom2_cost_price, mpp.uom3_cost_price, mpp.uom4_cost_price ";

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$lists[$count]['nilai_persediaan'] = $result[2];
			$lists[$count]['uom1_harga'] = $result[3];
			$lists[$count]['uom_ib'] = $result[4];
			$lists[$count]['l1_nm'] = $result[6];
			$lists[$count]['bumun_nm'] = $result[7];			
			$lists[$count]['supplied_by'] = $result[8];	
			if($result[9] > 0){
				$lists[$count]['map'] = $result[9];	
			}else{
				$lists[$count]['map'] = $result[3];	
			}		
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_ProductMove3($cond1,$cond2){
		$connection = new Postgresql($this->di['db']);
		//dedy, edit, map.
		$sql = " SELECT mp.\"stock_id\", mp.\"uom1_prod_nm\", SUM(mpm.qty) as nilai_persediaan, mp.uom1_harga, mp.uom1_internal_barcode, "
				." 	CASE
						WHEN uom1_htype_5 IS true THEN uom1_internal_barcode
						WHEN uom2_htype_5 IS true THEN uom2_internal_barcode
						WHEN uom3_htype_5 IS true THEN uom3_internal_barcode
						WHEN uom4_htype_5 IS true THEN uom4_internal_barcode
						ELSE ''
					END 
						AS uom_ib, 
					mc.l1_nm, mc.bumun_nm, mp.\"tipe\", 
					CASE
						WHEN uom1_htype_5 IS true THEN uom1_cost_price
						WHEN uom2_htype_5 IS true THEN uom2_cost_price
						WHEN uom3_htype_5 IS true THEN uom3_cost_price
						WHEN uom4_htype_5 IS true THEN uom4_cost_price
						WHEN uom1_htype_1 IS true THEN uom1_cost_price
						WHEN uom2_htype_1 IS true THEN uom2_cost_price
						WHEN uom3_htype_1 IS true THEN uom3_cost_price
						WHEN uom4_htype_1 IS true THEN uom4_cost_price
						ELSE 0
					END 
						AS map, mp.\"tipe\", mp.\"status_flag\" "
				." FROM \"m_product\" mp INNER JOIN \"m_product_move\" mpm ON mp.stock_id = mpm.stock_id 
				INNER JOIN m_product_price mpp ON mp.stock_id = mpp.stock_id 
				INNER JOIN m_category mc ON mp.l1_cd = mc.l1_cd ".$cond1."
				GROUP BY mp.stock_id, mc.l1_nm, mc.bumun_nm, mp.uom1_harga, mpp.uom1_cost_price, mpp.uom2_cost_price, mpp.uom3_cost_price, mpp.uom4_cost_price ".$cond2;

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			if($result[2] <= 0){
				$lists[$count]['stock_id'] = $result[0];
				$lists[$count]['description'] = $result[1];
				$lists[$count]['nilai_persediaan'] = $result[2];
				$lists[$count]['uom1_harga'] = $result[3];
				$lists[$count]['uom_ib'] = $result[4];
				$lists[$count]['l1_nm'] = $result[6];
				$lists[$count]['bumun_nm'] = $result[7];			
				$lists[$count]['supplied_by'] = $result[8];	
				if($result[9] > 0){
					$lists[$count]['map'] = $result[9];	
				}else{
					$lists[$count]['map'] = $result[3];
				}
				$lists[$count]['tipe'] = $result[10];	
			}
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_ProductMove2(){
		$connection = new Postgresql($this->di['db']);
		//dedy, edit, map.
		$sql = " SELECT mp.\"stock_id\", mp.\"uom1_prod_nm\", mp.\"status\", mp.\"uom1_internal_barcode\", mp.\"supplied_by\", mc.\"l1_nm\", mc.\"bumun_nm\", mpp.\"uom1_cost_price\", SUM(mpm.qty) as nilai_persediaan, mpp.\"average_cost\", mp.\"tipe\", mp.\"uom2_conversion\", mp.\"status_flag\" "
				." FROM \"m_product\" mp INNER JOIN \"m_product_move\" mpm ON mp.stock_id = mpm.stock_id 
				FULL JOIN m_product_price mpp ON mp.stock_id = mpp.stock_id
				LEFT JOIN m_category mc ON mp.l1_cd = mc.l1_cd
				GROUP BY mp.stock_id, mc.l1_nm, mc.bumun_nm, mp.uom1_harga, mpp.uom1_cost_price, mpp.uom2_cost_price, mpp.uom3_cost_price, mpp.uom4_cost_price, mpp.average_cost, mp.tipe ";
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			if($result[12] != 2){
				$lists[$count]['stock_id'] = $result[0];
				$lists[$count]['uom1_prod_nm'] = $result[1];
				$lists[$count]['status'] = $result[2];		
				$lists[$count]['uom_ib'] = $result[3];
				$lists[$count]['supplied_by'] = 'Lotte';	
				$lists[$count]['l1_nm'] = $result[5];
				$lists[$count]['bumun_nm'] = $result[6];			
				$lists[$count]['map'] = $result[9];	
				$lists[$count]['nilai_persediaan'] = $result[8];
				$lists[$count]['tipe'] = $result[10];
			}
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_ProductMove4($condition){
		$connection = new Postgresql($this->di['db']);
		//dedy, edit, map.
		$sql = " SELECT SUM(qty) as nilai_persediaan, mp.\"status_flag\"  "
				." FROM \"m_product_move\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['nilai_persediaan'] = $result[0];
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_ProductMove5($cond1){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT mp.\"stock_id\", mp.\"uom1_prod_nm\", SUM(mpm.qty) as nilai_persediaan, mp.uom1_harga, mp.uom1_internal_barcode, "
				." 	CASE
						WHEN uom1_htype_5 IS true THEN uom1_internal_barcode
						WHEN uom2_htype_5 IS true THEN uom2_internal_barcode
						WHEN uom3_htype_5 IS true THEN uom3_internal_barcode
						WHEN uom4_htype_5 IS true THEN uom4_internal_barcode
						ELSE ''
					END 
						AS uom_ib, 
					mc.l1_nm, mc.bumun_nm, mp.\"tipe\", 
					CASE
						WHEN uom1_htype_5 IS true THEN uom1_cost_price
						WHEN uom2_htype_5 IS true THEN uom2_cost_price
						WHEN uom3_htype_5 IS true THEN uom3_cost_price
						WHEN uom4_htype_5 IS true THEN uom4_cost_price
						WHEN uom1_htype_1 IS true THEN uom1_cost_price
						WHEN uom2_htype_1 IS true THEN uom2_cost_price
						WHEN uom3_htype_1 IS true THEN uom3_cost_price
						WHEN uom4_htype_1 IS true THEN uom4_cost_price
						ELSE 0
					END 
						AS map, mp.\"tipe\", mp.\"status_flag\"	"
				." FROM \"m_product\" mp INNER JOIN \"m_product_move\" mpm ON mp.stock_id = mpm.stock_id 
				INNER JOIN m_product_price mpp ON mp.stock_id = mpp.stock_id 
				INNER JOIN m_category mc ON mp.l1_cd = mc.l1_cd ".$cond1."
				GROUP BY mp.stock_id, mc.l1_nm, mc.bumun_nm, mp.uom1_harga, mpp.uom1_cost_price, mpp.uom2_cost_price, mpp.uom3_cost_price, mpp.uom4_cost_price ";

		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			if($result[2] > 0){
				$lists[$count]['stock_id'] = $result[0];
				$lists[$count]['description'] = $result[1];
				$lists[$count]['nilai_persediaan'] = $result[2];
				$lists[$count]['uom1_harga'] = $result[3];
				$lists[$count]['uom_ib'] = $result[4];
				$lists[$count]['l1_nm'] = $result[6];
				$lists[$count]['bumun_nm'] = $result[7];			
				$lists[$count]['supplied_by'] = $result[8];	
				if($result[9] > 0){
					$lists[$count]['map'] = $result[9];	
				}else{
					$lists[$count]['map'] = $result[3];	
				}
				$lists[$count]['tipe'] = $result[10];
			}
			$count++;
		}
		return $lists;
	}

	public function getJoin_ProductMove6($cond){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT tbl.\"stock_id\", sum(tbl.qty) AS qty FROM (SELECT \"stock_id\", 0 qty FROM \"m_product\" "
					." UNION SELECT b.\"stock_id\", sum(b.qty) AS qty FROM \"m_product\" a RIGHT JOIN \"m_product_move\" b ON a.\"stock_id\" = b.\"stock_id\" WHERE b.\"type\" ='CT' AND b.\"tran_date\" ='".$cond."' GROUP BY b.\"stock_id\") tbl GROUP BY tbl.\"stock_id\" ";
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['qty'] = $result[1];
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_ProductMove7($cond){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT tbl.\"stock_id\", sum(tbl.qty) AS qty FROM (SELECT \"stock_id\", 0 qty FROM \"m_product\" "
					." UNION SELECT b.\"stock_id\", sum(b.qty) AS qty FROM \"m_product\" a RIGHT JOIN \"m_product_move\" b ON a.\"stock_id\" = b.\"stock_id\" WHERE b.\"type\" ='GR' AND b.\"tran_date\" ='".$cond."'
					GROUP BY b.\"stock_id\") tbl GROUP BY tbl.\"stock_id\" ";
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['qty'] = $result[1];
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_ProductMove8($cond){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT tbl.\"stock_id\", sum(tbl.qty) AS qty FROM (SELECT \"stock_id\", 0 qty FROM \"m_product\" "
					." UNION SELECT b.\"stock_id\", sum(b.qty) AS qty FROM \"m_product\" a RIGHT JOIN \"m_product_move\" b ON a.\"stock_id\" = b.\"stock_id\" WHERE b.\"type\" ='SO' AND b.\"tran_date\" ='".$cond."'
					GROUP BY b.\"stock_id\") tbl GROUP BY tbl.\"stock_id\" ";
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['qty'] = $result[1];
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_ProductMove9($cond){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT tbl.\"stock_id\", sum(tbl.qty) AS qty FROM (SELECT \"stock_id\", 0 qty FROM \"m_product\" "
					." UNION SELECT b.\"stock_id\", sum(b.qty) AS qty FROM \"m_product\" a RIGHT JOIN \"m_product_move\" b ON a.\"stock_id\" = b.\"stock_id\" WHERE b.\"type\" ='RR' AND b.\"tran_date\" ='".$cond."'
					GROUP BY b.\"stock_id\") tbl GROUP BY tbl.\"stock_id\" ";
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['qty'] = $result[1];
			$count++;
		}
		
		return $lists;
	}

	public function getCariHPP($cond){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT mp.\"stock_id\", SUM(mpm.qty) AS qty, mpp.\"average_cost\" "
						." FROM m_product mp LEFT JOIN m_product_move mpm ON mp.\"stock_id\" = mpm.\"stock_id\" LEFT JOIN m_product_price mpp ON mp.\"stock_id\" = mpp.\"stock_id\" GROUP BY mp.\"stock_id\", mpp.\"stock_id\" ORDER BY mp.\"stock_id\" ";
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['qty'] = $result[1];
			$lists[$count]['hpp'] = $result[2];
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_ProductMove10($cond){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT tbl.\"stock_id\", sum(tbl.qty) AS qty FROM (SELECT \"stock_id\", 0 qty FROM \"m_product\" "
					." UNION SELECT b.\"stock_id\", sum(b.qty) AS qty FROM \"m_product\" a RIGHT JOIN \"m_product_move\" b ON a.\"stock_id\" = b.\"stock_id\" WHERE b.\"type\" ='AD' AND b.\"tran_date\" ='".$cond."'
					GROUP BY b.\"stock_id\") tbl GROUP BY tbl.\"stock_id\" ";
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['qty'] = $result[1];
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_ProductMove11($cond){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT tbl.\"stock_id\", sum(tbl.qty) AS qty FROM (SELECT \"stock_id\", 0 qty FROM \"m_product\" "
					." UNION SELECT b.\"stock_id\", sum(b.qty) AS qty FROM \"m_product\" a RIGHT JOIN \"m_product_move\" b ON a.\"stock_id\" = b.\"stock_id\" WHERE b.\"type\" ='PYR' AND b.\"tran_date\" ='".$cond."'
					GROUP BY b.\"stock_id\") tbl GROUP BY tbl.\"stock_id\" ";
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['qty'] = $result[1];
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_Opening($condition){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT mp.\"stock_id\", mp.\"uom1_prod_nm\", SUM(mpm.qty) as nilai_persediaan, mpm.\"type\", mp.\"status_flag\" "
				." FROM \"m_product\" mp INNER JOIN \"m_product_move\" mpm ON mp.stock_id = mpm.stock_id
				GROUP BY mp.stock_id, mpm.type ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			if($result[3] == ''){
				$lists[$count]['stock_id'] = $result[0];
				$lists[$count]['uom1_prod_nm'] = $result[1];
				$lists[$count]['nilai_persediaan'] = $result[2];
				$lists[$count]['tipe'] = $result[3];
			}
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_ProductPrice($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT mp.\"stock_id\" "
				.", \"uom1_nm\", \"uom1_htype_1\", \"uom1_htype_2\", \"uom1_htype_3\", \"uom1_htype_4\", \"uom1_htype_5\", \"uom1_internal_barcode\" "
				.", \"uom2_nm\", \"uom2_htype_1\", \"uom2_htype_2\", \"uom2_htype_3\", \"uom2_htype_4\", \"uom2_htype_5\", \"uom2_internal_barcode\" "
				.", \"uom3_nm\", \"uom3_htype_1\", \"uom3_htype_2\", \"uom3_htype_3\", \"uom3_htype_4\", \"uom3_htype_5\", \"uom3_internal_barcode\" "
				.", \"uom4_nm\", \"uom4_htype_1\", \"uom4_htype_2\", \"uom4_htype_3\", \"uom4_htype_4\", \"uom4_htype_5\", \"uom4_internal_barcode\" "
				.", \"uom1_prod_nm\", \"uom2_prod_nm\", \"uom3_prod_nm\", \"uom4_prod_nm\" "
				
				.", \"uom1_suggested_price\", \"uom1_rounding_price\", \"uom1_changed_price\", \"uom1_member_price\", \"uom1_margin_amount\", \"uom1_cost_price\" "
				.", \"uom2_suggested_price\", \"uom2_rounding_price\", \"uom2_changed_price\", \"uom2_member_price\", \"uom2_margin_amount\", \"uom2_cost_price\" "
				.", \"uom3_suggested_price\", \"uom3_rounding_price\", \"uom3_changed_price\", \"uom3_member_price\", \"uom3_margin_amount\", \"uom3_cost_price\" "
				.", \"uom4_suggested_price\", \"uom4_rounding_price\", \"uom4_changed_price\", \"uom4_member_price\", \"uom4_margin_amount\", \"uom4_cost_price\" "
				.", mpp.\"margin\", \"average_cost\",

					CASE
						WHEN uom1_changed_price != 0 THEN uom1_changed_price
						WHEN uom2_changed_price != 0 THEN uom2_changed_price
						WHEN uom3_changed_price != 0 THEN uom3_changed_price
						WHEN uom4_changed_price != 0 THEN uom4_changed_price
						ELSE 0
					END 
						AS uom_changed_price,
						
					CASE
						WHEN uom1_rounding_price != 0 THEN uom1_rounding_price
						WHEN uom2_rounding_price != 0 THEN uom2_rounding_price
						WHEN uom3_rounding_price != 0 THEN uom3_rounding_price
						WHEN uom4_rounding_price != 0 THEN uom4_rounding_price
						ELSE 0
					END 
						AS uom_rounding_price,
					
					CASE
						WHEN uom1_suggested_price != 0 THEN uom1_suggested_price
						WHEN uom2_suggested_price != 0 THEN uom2_suggested_price
						WHEN uom3_suggested_price != 0 THEN uom3_suggested_price
						WHEN uom4_suggested_price != 0 THEN uom4_suggested_price
						ELSE 0
					END 
						AS uom_suggested_price,
					
					CASE
						WHEN uom1_member_price != 0 THEN uom1_member_price
						WHEN uom2_member_price != 0 THEN uom2_member_price
						WHEN uom3_member_price != 0 THEN uom3_member_price
						WHEN uom4_member_price != 0 THEN uom4_member_price
						ELSE 0
					END 
						AS uom_member_price, mp.supplied_by, mp.uom1_harga, mp.description, mpp.average_cost, uom1_conversion, uom2_conversion, uom3_conversion, uom4_conversion, uom2_harga, mp.tipe, mp.uom1_prod_nm, mp.uom2_prod_nm, mp.status_flag

				"
				." FROM \"m_product\" mp INNER JOIN \"m_product_price\" mpp ON mp.stock_id = mpp.stock_id ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			if($result[72] != 004 AND $result[72] != 003){
				$lists[$count]['stock_id'] = $result[0];
				$lists[$count]['uom1_nm'] = $result[1];
				$lists[$count]['uom1_htype_1'] = $result[2];
				$lists[$count]['uom1_htype_2'] = $result[3];
				$lists[$count]['uom1_htype_3'] = $result[4];
				$lists[$count]['uom1_htype_4'] = $result[5];
				$lists[$count]['uom1_htype_5'] = $result[6];
				$lists[$count]['uom1_internal_barcode'] = $result[7];
				$lists[$count]['uom2_nm'] = $result[8];
				$lists[$count]['uom2_htype_1'] = $result[9];
				$lists[$count]['uom2_htype_2'] = $result[10];
				$lists[$count]['uom2_htype_3'] = $result[11];
				$lists[$count]['uom2_htype_4'] = $result[12];
				$lists[$count]['uom2_htype_5'] = $result[13];
				$lists[$count]['uom2_internal_barcode'] = $result[14];
				$lists[$count]['uom3_nm'] = $result[15];
				$lists[$count]['uom3_htype_1'] = $result[16];
				$lists[$count]['uom3_htype_2'] = $result[17];
				$lists[$count]['uom3_htype_3'] = $result[18];
				$lists[$count]['uom3_htype_4'] = $result[19];
				$lists[$count]['uom3_htype_5'] = $result[20];
				$lists[$count]['uom3_internal_barcode'] = $result[21];
				$lists[$count]['uom4_nm'] = $result[22];
				$lists[$count]['uom4_htype_1'] = $result[23];
				$lists[$count]['uom4_htype_2'] = $result[24];
				$lists[$count]['uom4_htype_3'] = $result[25];
				$lists[$count]['uom4_htype_4'] = $result[26];
				$lists[$count]['uom4_htype_5'] = $result[27];
				$lists[$count]['uom4_internal_barcode'] = $result[28];
				$lists[$count]['uom1_prod_nm'] = $result[29];
				$lists[$count]['uom2_prod_nm'] = $result[30];
				$lists[$count]['uom3_prod_nm'] = $result[31];
				$lists[$count]['uom4_prod_nm'] = $result[32];	
				$lists[$count]['uom1_suggested_price'] = $result[33];
				$lists[$count]['uom1_rounding_price'] = $result[34];
				$lists[$count]['uom1_changed_price'] = $result[35];
				$lists[$count]['uom1_member_price'] = $result[36];
				$lists[$count]['uom1_margin_amount'] = $result[37];
				$lists[$count]['uom1_cost_price'] = $result[38];
				$lists[$count]['uom2_suggested_price'] = $result[39];
				$lists[$count]['uom2_rounding_price'] = $result[40];
				$lists[$count]['uom2_changed_price'] = $result[41];
				$lists[$count]['uom2_member_price'] = $result[42];
				$lists[$count]['uom2_margin_amount'] = $result[43];
				$lists[$count]['uom2_cost_price'] = $result[44];
				$lists[$count]['uom3_suggested_price'] = $result[45];
				$lists[$count]['uom3_rounding_price'] = $result[46];
				$lists[$count]['uom3_changed_price'] = $result[47];
				$lists[$count]['uom3_member_price'] = $result[48];
				$lists[$count]['uom3_margin_amount'] = $result[49];
				$lists[$count]['uom3_cost_price'] = $result[50];
				$lists[$count]['uom4_suggested_price'] = $result[51];
				$lists[$count]['uom4_rounding_price'] = $result[52];
				$lists[$count]['uom4_changed_price'] = $result[53];
				$lists[$count]['uom4_member_price'] = $result[54];
				$lists[$count]['uom4_margin_amount'] = $result[55];
				$lists[$count]['uom4_cost_price'] = $result[56];
				$lists[$count]['margin'] = $result[57];
				$lists[$count]['average_cost'] = $result[58];
				$lists[$count]['uom_changed_price'] = $result[59];
				$lists[$count]['uom_rounding_price'] = $result[60];
				$lists[$count]['uom_suggested_price'] = $result[61];
				$lists[$count]['uom_member_price'] = $result[62];
				$lists[$count]['supplied_by'] = $result[63];
				$lists[$count]['uom1_harga'] = $result[64];
				$lists[$count]['description'] = $result[65];
				$lists[$count]['price'] = $result[66];
				$lists[$count]['uom1_conversion'] = $result[67];
				$lists[$count]['uom2_conversion'] = $result[68];
				$lists[$count]['uom3_conversion'] = $result[69];
				$lists[$count]['uom4_conversion'] = $result[70];
				$lists[$count]['harga'] = $result[71];
				$lists[$count]['tipe'] = $result[72];
				$lists[$count]['uom1_prod_nm'] = $result[73];
				$lists[$count]['uom2_prod_nm'] = $result[74];
				$lists[$count]['status_flag'] = $result[75];
			}
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_JualDetail($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " (SELECT \"item_code\", tjd.\"description\", MAX(\"harga_hpp3\"), AVG(\"harga\"), SUM(\"qty\"), tj.\"tanggal\", tjd.\"barcode\", AVG(tjd.\"discount\"), mp.\"status_flag\" "
				." FROM \"m_product\" mp INNER JOIN \"t_jual_detail\" tjd ON mp.stock_id = tjd.item_code INNER JOIN \"t_jual\" tj ON tjd.id_jual = tj.id_jual AND id_member = '' "
				." ".$condition;

		$sql .= " ) UNION (SELECT \"item_code\", tjd.\"description\", MAX(\"harga_hpp3\"), AVG(\"harga_member\"), SUM(\"qty\"), tj.\"tanggal\", tjd.\"barcode\", AVG(tjd.\"discount\"), mp.\"status_flag\" "
				." FROM \"m_product\" mp INNER JOIN \"t_jual_detail\" tjd ON mp.stock_id = tjd.item_code INNER JOIN \"t_jual\" tj ON tjd.id_jual = tj.id_jual AND id_member != '' "
				." INNER JOIN \"m_cust_member\" mcm ON tj.id_member = mcm.nomor_member "
				." ".$condition;
				
		$sql .= " ) UNION (SELECT \"item_code\", tjd.\"description\", MAX(\"harga_hpp3\"), AVG(\"harga_hpp3\"), SUM(\"qty\"), tj.\"tanggal\", tjd.\"barcode\", AVG(tjd.\"discount\"), mp.\"status_flag\" "
				." FROM \"m_product\" mp INNER JOIN \"t_jual_detail\" tjd ON mp.stock_id = tjd.item_code INNER JOIN \"t_jual\" tj ON tjd.id_jual = tj.id_jual AND id_member != '' "
				." INNER JOIN \"m_cust\" mc ON tj.id_member = mc.nomor_member "
				." ".$condition." ) ORDER BY tanggal ";
	
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['item_code'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$lists[$count]['uom1_harga'] = $result[2];
			$lists[$count]['harga'] = $result[3];
			$lists[$count]['qty'] = $result[4];
			$lists[$count]['tanggal'] = $result[5];
			$lists[$count]['barcode'] = $result[6];
			$lists[$count]['discount'] = $result[7];
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_JualDetail2($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " (SELECT \"item_code\", tjd.\"description\", mpp.\"average_cost\", AVG(\"harga\"), SUM(\"qty\"), tj.\"tanggal\", tjd.\"barcode\", AVG(tjd.\"discount\"), mp.\"status_flag\" "
				." FROM \"m_product\" mp INNER JOIN \"m_product_price\" mpp ON mp.stock_id = mpp.stock_id INNER JOIN \"t_jual_detail\" tjd ON mp.stock_id = tjd.item_code INNER JOIN \"t_jual\" tj ON tjd.id_jual = tj.id_jual AND id_member = '' "
				." ".$condition;

		$sql .= " ) UNION (SELECT \"item_code\", tjd.\"description\", mpp.\"average_cost\", AVG(\"harga_member\"), SUM(\"qty\"), tj.\"tanggal\", tjd.\"barcode\", AVG(tjd.\"discount\"), mp.\"status_flag\" "
				." FROM \"m_product\" mp INNER JOIN \"m_product_price\" mpp ON mp.stock_id = mpp.stock_id INNER JOIN \"t_jual_detail\" tjd ON mp.stock_id = tjd.item_code INNER JOIN \"t_jual\" tj ON tjd.id_jual = tj.id_jual AND id_member != '' "
				." INNER JOIN \"m_cust_member\" mcm ON tj.id_member = mcm.nomor_member "
				." ".$condition;
				
		$sql .= " ) UNION (SELECT \"item_code\", tjd.\"description\", mpp.\"average_cost\", AVG(\"harga_hpp3\"), SUM(\"qty\"), tj.\"tanggal\", tjd.\"barcode\", AVG(tjd.\"discount\"), mp.\"status_flag\" "
				." FROM \"m_product\" mp INNER JOIN \"m_product_price\" mpp ON mp.stock_id = mpp.stock_id INNER JOIN \"t_jual_detail\" tjd ON mp.stock_id = tjd.item_code INNER JOIN \"t_jual\" tj ON tjd.id_jual = tj.id_jual AND id_member != '' "
				." INNER JOIN \"m_cust\" mc ON tj.id_member = mc.nomor_member "
				." ".$condition." ) ORDER BY tanggal ";
	
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['item_code'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$lists[$count]['uom1_harga'] = $result[2];
			$lists[$count]['harga'] = $result[3];
			$lists[$count]['qty'] = $result[4];
			$lists[$count]['tanggal'] = $result[5];
			$lists[$count]['barcode'] = $result[6];
			$lists[$count]['discount'] = $result[7];
			$count++;
		}
		
		return $lists;
	}

	public function getJoin_JualDetail3($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " (SELECT \"item_code\", tjd.\"description\", mpp.\"average_cost\", AVG(\"harga\"), SUM(\"qty\"), tj.\"tanggal\", tjd.\"barcode\", AVG(tjd.\"discount\"), mp.\"status_flag\" "
				." FROM \"m_product\" mp INNER JOIN \"m_product_price\" mpp ON mp.stock_id = mpp.stock_id INNER JOIN \"t_jual_detail\" tjd ON mp.stock_id = tjd.item_code INNER JOIN \"t_jual\" tj ON tjd.id_jual = tj.id_jual AND id_member = '' "
				." ".$condition;

		$sql .= " ) UNION (SELECT \"item_code\", tjd.\"description\", mpp.\"average_cost\", AVG(\"harga_member\"), SUM(\"qty\"), tj.\"tanggal\", tjd.\"barcode\", AVG(tjd.\"discount\"), mp.\"status_flag\" "
				." FROM \"m_product\" mp INNER JOIN \"m_product_price\" mpp ON mp.stock_id = mpp.stock_id INNER JOIN \"t_jual_detail\" tjd ON mp.stock_id = tjd.item_code INNER JOIN \"t_jual\" tj ON tjd.id_jual = tj.id_jual AND id_member != '' "
				." INNER JOIN \"m_cust_member\" mcm ON tj.id_member = mcm.nomor_member "
				." ".$condition;
				
		$sql .= " ) UNION (SELECT \"item_code\", tjd.\"description\", mpp.\"average_cost\", AVG(\"harga_hpp3\"), SUM(\"qty\"), tj.\"tanggal\", tjd.\"barcode\", AVG(tjd.\"discount\"), mp.\"status_flag\" "
				." FROM \"m_product\" mp INNER JOIN \"m_product_price\" mpp ON mp.stock_id = mpp.stock_id INNER JOIN \"t_jual_detail\" tjd ON mp.stock_id = tjd.item_code INNER JOIN \"t_jual\" tj ON tjd.id_jual = tj.id_jual AND id_member != '' "
				." INNER JOIN \"m_cust\" mc ON tj.id_member = mc.nomor_member "
				." ".$condition." ) ORDER BY tanggal ";
	
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['item_code'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$lists[$count]['uom1_harga'] = $result[2];
			$lists[$count]['harga'] = $result[3];
			$lists[$count]['qty'] = $result[4];
			$lists[$count]['tanggal'] = $result[5];
			$lists[$count]['barcode'] = $result[6];
			$lists[$count]['discount'] = $result[7];
			$count++;
		}
		
		return $lists;
	}
	
	

	public function getFreeSQLs($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT DISTINCT \"stock_id\", \"category_id\", \"tax_type_id\", \"description\", \"long_description\", \"units\", \"actual_cost\" "
				.", \"last_cost\", \"material_cost\", \"labour_cost\", \"overhead_cost\", \"inactive\", \"no_sale\", \"editable\" "
				.", \"jenis_item\", \"bumun_cd\", \"l1_cd\", \"l2_cd\", \"l3_cd\", \"l4_cd\", \"margin\" "
				.", \"moving_type\", \"ass_type_1\", \"ass_type_2\", \"ass_type_3\", \"expiry_product\", \"weight_product\", \"batch_product\" "
				.", \"active_status\", \"kode_1\", \"kode_2\", \"kode_3\", \"discount\", \"supplied_by\", \"uom1_nm\" "
				.", \"uom1_htype_1\", \"uom1_htype_2\", \"uom1_htype_3\", \"uom1_htype_4\", \"uom1_htype_5\", \"uom1_rack_type\", \"uom1_shelfing_rack_type\" "
				.", \"uom1_internal_barcode\", \"uom1_width\", \"uom1_length\", \"uom1_height\", \"uom1_weight\", \"uom1_margin\", \"uom1_box_type\" "
				.", \"uom1_harga\", \"uom2_nm\", \"uom2_htype_1\", \"uom2_htype_2\", \"uom2_htype_3\", \"uom2_htype_4\", \"uom2_htype_5\" "
				.", \"uom2_rack_type\", \"uom2_shelfing_rack_type\", \"uom2_conversion\", \"uom2_internal_barcode\", \"uom2_width\", \"uom2_length\", \"uom2_height\" "
				.", \"uom2_weight\", \"uom2_margin\", \"uom2_box_type\", \"uom2_harga\", \"uom3_nm\", \"uom3_htype_1\", \"uom3_htype_2\" "
				.", \"uom3_htype_3\", \"uom3_htype_4\", \"uom3_htype_5\", \"uom3_rack_type\", \"uom3_shelfing_rack_type\", \"uom3_conversion\", \"uom3_internal_barcode\" "
				.", \"uom3_width\", \"uom3_length\", \"uom3_height\", \"uom3_weight\", \"uom3_margin\", \"uom3_box_type\", \"uom3_harga\" "
				.", \"uom4_nm\", \"uom4_htype_1\", \"uom4_htype_2\", \"uom4_htype_3\", \"uom4_htype_4\", \"uom4_htype_5\", \"uom4_rack_type\" "
				.", \"uom4_shelfing_rack_type\", \"uom4_conversion\", \"uom4_internal_barcode\", \"uom4_width\", \"uom4_length\", \"uom4_height\", \"uom4_weight\" "
				.", \"uom4_margin\", \"uom4_box_type\", \"uom4_harga\", \"uom1_prod_nm\", \"uom2_prod_nm\", \"uom3_prod_nm\", \"uom4_prod_nm\", \"uom1_conversion\" "
				." FROM \"m_product\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['category_id'] = $result[1];
			$lists[$count]['tax_type_id'] = $result[2];
			$lists[$count]['description'] = $result[3];
			$lists[$count]['long_description'] = $result[4];
			$lists[$count]['units'] = $result[5];
			$lists[$count]['actual_cost'] = $result[6];
			$lists[$count]['last_cost'] = $result[7];
			$lists[$count]['material_cost'] = $result[8];
			$lists[$count]['labour_cost'] = $result[9];
			$lists[$count]['overhead_cost'] = $result[10];
			$lists[$count]['inactive'] = $result[11];
			$lists[$count]['no_sale'] = $result[12];
			$lists[$count]['editable'] = $result[13];
			$lists[$count]['jenis_item'] = $result[14];
			$lists[$count]['bumun_cd'] = $result[15];
			$lists[$count]['l1_cd'] = $result[16];
			$lists[$count]['l2_cd'] = $result[17];
			$lists[$count]['l3_cd'] = $result[18];
			$lists[$count]['l4_cd'] = $result[19];
			$lists[$count]['margin'] = $result[20];
			$lists[$count]['moving_type'] = $result[21];
			$lists[$count]['ass_type_1'] = $result[22];
			$lists[$count]['ass_type_2'] = $result[23];
			$lists[$count]['ass_type_3'] = $result[24];
			$lists[$count]['expiry_product'] = $result[25];
			$lists[$count]['weight_product'] = $result[26];
			$lists[$count]['batch_product'] = $result[27];
			$lists[$count]['active_status'] = $result[28];
			$lists[$count]['kode_1'] = $result[29];
			$lists[$count]['kode_2'] = $result[30];
			$lists[$count]['kode_3'] = $result[31];
			$lists[$count]['discount'] = $result[32];
			$lists[$count]['supplied_by'] = $result[33];
			$lists[$count]['uom1_nm'] = $result[34];
			$lists[$count]['uom1_htype_1'] = $result[35];
			$lists[$count]['uom1_htype_2'] = $result[36];
			$lists[$count]['uom1_htype_3'] = $result[37];
			$lists[$count]['uom1_htype_4'] = $result[38];
			$lists[$count]['uom1_htype_5'] = $result[39];
			$lists[$count]['uom1_rack_type'] = $result[40];
			$lists[$count]['uom1_shelfing_rack_type'] = $result[41];
			$lists[$count]['uom1_internal_barcode'] = $result[42];
			$lists[$count]['uom1_width'] = $result[43];
			$lists[$count]['uom1_length'] = $result[44];
			$lists[$count]['uom1_height'] = $result[45];
			$lists[$count]['uom1_weight'] = $result[46];
			$lists[$count]['uom1_margin'] = $result[47];
			$lists[$count]['uom1_box_type'] = $result[48];
			$lists[$count]['uom1_harga'] = $result[49];
			$lists[$count]['uom2_nm'] = $result[50];
			$lists[$count]['uom2_htype_1'] = $result[51];
			$lists[$count]['uom2_htype_2'] = $result[52];
			$lists[$count]['uom2_htype_3'] = $result[53];
			$lists[$count]['uom2_htype_4'] = $result[54];
			$lists[$count]['uom2_htype_5'] = $result[55];
			$lists[$count]['uom2_rack_type'] = $result[56];
			$lists[$count]['uom2_shelfing_rack_type'] = $result[57];
			$lists[$count]['uom2_conversion'] = $result[58];
			$lists[$count]['uom2_internal_barcode'] = $result[59];
			$lists[$count]['uom2_width'] = $result[60];
			$lists[$count]['uom2_length'] = $result[61];
			$lists[$count]['uom2_height'] = $result[62];
			$lists[$count]['uom2_weight'] = $result[63];
			$lists[$count]['uom2_margin'] = $result[64];
			$lists[$count]['uom2_box_type'] = $result[65];
			$lists[$count]['uom2_harga'] = $result[66];
			$lists[$count]['uom3_nm'] = $result[67];
			$lists[$count]['uom3_htype_1'] = $result[68];
			$lists[$count]['uom3_htype_2'] = $result[69];
			$lists[$count]['uom3_htype_3'] = $result[70];
			$lists[$count]['uom3_htype_4'] = $result[71];
			$lists[$count]['uom3_htype_5'] = $result[72];
			$lists[$count]['uom3_rack_type'] = $result[73];
			$lists[$count]['uom3_shelfing_rack_type'] = $result[74];
			$lists[$count]['uom3_conversion'] = $result[75];
			$lists[$count]['uom3_internal_barcode'] = $result[76];
			$lists[$count]['uom3_width'] = $result[77];
			$lists[$count]['uom3_length'] = $result[78];
			$lists[$count]['uom3_height'] = $result[79];
			$lists[$count]['uom3_weight'] = $result[80];
			$lists[$count]['uom3_margin'] = $result[81];
			$lists[$count]['uom3_box_type'] = $result[82];
			$lists[$count]['uom3_harga'] = $result[83];
			$lists[$count]['uom4_nm'] = $result[84];
			$lists[$count]['uom4_htype_1'] = $result[85];
			$lists[$count]['uom4_htype_2'] = $result[86];
			$lists[$count]['uom4_htype_3'] = $result[87];
			$lists[$count]['uom4_htype_4'] = $result[88];
			$lists[$count]['uom4_htype_5'] = $result[89];
			$lists[$count]['uom4_rack_type'] = $result[90];
			$lists[$count]['uom4_shelfing_rack_type'] = $result[91];
			$lists[$count]['uom4_conversion'] = $result[92];
			$lists[$count]['uom4_internal_barcode'] = $result[93];
			$lists[$count]['uom4_width'] = $result[94];
			$lists[$count]['uom4_length'] = $result[95];
			$lists[$count]['uom4_height'] = $result[96];
			$lists[$count]['uom4_weight'] = $result[97];
			$lists[$count]['uom4_margin'] = $result[98];
			$lists[$count]['uom4_box_type'] = $result[99];
			$lists[$count]['uom4_harga'] = $result[100];
			$lists[$count]['uom1_prod_nm'] = $result[101];
			$lists[$count]['uom2_prod_nm'] = $result[102];
			$lists[$count]['uom3_prod_nm'] = $result[103];
			$lists[$count]['uom4_prod_nm'] = $result[104];	
			$lists[$count]['uom1_conversion'] = $result[105];
			
			$count++;
		}
		
		return $lists;
	}

	public function getFreeSQLs2($query){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT stock_id, uom1_internal_barcode, uom1_prod_nm, status_flag "
				." FROM m_product 
				WHERE (uom1_internal_barcode LIKE '%".$query."%' OR uom1_prod_nm LIKE '%".$query."%') AND status_flag != '2' GROUP BY stock_id";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['uom1_internal_barcode'] = $result[1];
			$lists[$count]['uom1_prod_nm'] = $result[2];
			$count++;
		}
		
		return $lists;
	}
	
	public function POdirect_info($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "  SELECT 
			CASE
				WHEN uom1_htype_1 is true THEN uom1_nm
				WHEN uom2_htype_1 is true THEN uom2_nm
				WHEN uom3_htype_1 is true THEN uom3_nm
				WHEN uom4_htype_1 is true THEN uom4_nm
			END as uom_selling,
			
			CASE
				WHEN uom1_htype_3 is true THEN uom1_nm
				WHEN uom2_htype_3 is true THEN uom2_nm
				WHEN uom3_htype_3 is true THEN uom3_nm
				WHEN uom4_htype_3 is true THEN uom4_nm
			END as uom_order,
			
			CASE
				WHEN uom1_htype_1 is true THEN uom1_conversion
				WHEN uom2_htype_1 is true THEN uom2_conversion
				WHEN uom3_htype_1 is true THEN uom3_conversion
				WHEN uom4_htype_1 is true THEN uom4_conversion
			END as sell_conversion,
			
			CASE
				WHEN uom1_htype_3 is true THEN uom1_conversion
				WHEN uom2_htype_3 is true THEN uom2_conversion
				WHEN uom3_htype_3 is true THEN uom3_conversion
				WHEN uom4_htype_3 is true THEN uom4_conversion
			END as order_conversion,
			
			CASE
				WHEN uom1_htype_3 is true THEN uom1_internal_barcode
				WHEN uom2_htype_3 is true THEN uom2_internal_barcode
				WHEN uom3_htype_3 is true THEN uom3_internal_barcode
				WHEN uom4_htype_3 is true THEN uom4_internal_barcode
			END as order_conversion
		
		FROM m_product ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()){
			$lists[$count]['uom_selling'] = $result[0];
			$lists[$count]['uom_order'] = $result[1];
			$lists[$count]['sell_conversion'] = $result[2];
			$lists[$count]['order_conversion'] = $result[3];
			$lists[$count]['barcode'] = $result[4];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function get_non_map($cond){
		$connection = new Postgresql($this->di['db']);
		$sql = " SELECT SUM(mpm.qty) as qty_stock, 
				CASE 
					WHEN uom1_cost_price != 0 THEN uom1_cost_price
					ELSE uom1_harga
				END as map
				"
				." FROM m_product_move mpm INNER JOIN m_product_price mpp ON mpm.stock_id = mpp.stock_id 
				INNER JOIN m_product mp ON mpp.stock_id = mp.stock_id ".$cond."
				GROUP BY map ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['qty_stock'] = $result[0];
			$lists[$count]['uom1_cost_price'] = $result[1];
			$count++;
		}
		
		return $lists;
	}

	public function executeQuery($sql){
		$connection = new Postgresql($this->di['db']);
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
