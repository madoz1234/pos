<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class JualDetailSum extends Model
{

	public $id_jual_item;
	public $id_jual;
	public $item_code;
	public $description;	
	public $harga;
	public $harga_member;
	public $qty;	
	public $satuan;	
	public $discount;	
	public $total;	
	public $promo;	
	public $flag_sync;	
	public $id_jual_sum;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"item_code\", \"description\", \"tanggal\", \"id_kasir\", \"kassa_id\", \"shift_id\", \"harga\", \"harga_member\", \"qty\", \"discount\", \"total\", \"flag_sync\", \"id_jual_sum\", \"harga_hpp3\" "
				." FROM \"t_jual_detail_sum\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['item_code'] = $result[1];
			$lists[$count]['description'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];
			$lists[$count]['id_kasir'] = $result[4];
			$lists[$count]['kassa_id'] = $result[5];
			$lists[$count]['shift_id'] = $result[6];
			$lists[$count]['harga'] = $result[7];			
			$lists[$count]['harga_member'] = $result[8];	
			$lists[$count]['qty'] = $result[9];	
			$lists[$count]['discount'] = $result[10];	
			$lists[$count]['total'] = $result[11];	
			$lists[$count]['flag_sync'] = json_encode($result[12]);	
			$lists[$count]['id_jual_sum'] = $result[13];	
			$lists[$count]['harga_hpp3'] = $result[14];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"item_code\", \"description\", \"tanggal\", \"id_kasir\", \"kassa_id\", \"shift_id\", \"harga\", \"harga_member\", \"qty\", \"discount\", \"total\", \"flag_sync\", \"id_jual_sum\", \"harga_hpp3\" "
				." FROM \"t_jual_detail_sum\" "
				." WHERE \"id\" = '".$object->id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['item_code'] = $result[1];
			$lists[$count]['description'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];
			$lists[$count]['id_kasir'] = $result[4];
			$lists[$count]['kassa_id'] = $result[5];
			$lists[$count]['shift_id'] = $result[6];
			$lists[$count]['harga'] = $result[7];			
			$lists[$count]['harga_member'] = $result[8];	
			$lists[$count]['qty'] = $result[9];	
			$lists[$count]['discount'] = $result[10];	
			$lists[$count]['total'] = $result[11];	
			$lists[$count]['flag_sync'] = json_encode($result[12]);	
			$lists[$count]['id_jual_sum'] = $result[13];	
			$lists[$count]['harga_hpp3'] = $result[14];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"item_code\", \"description\", \"tanggal\", \"id_kasir\", \"kassa_id\", \"shift_id\", \"harga\", \"harga_member\", \"qty\", \"discount\", \"total\", \"flag_sync\", \"id_jual_sum\", \"harga_hpp3\" "
				." FROM \"t_jual_detail_sum\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['item_code'] = $result[1];
			$lists[$count]['description'] = $result[2];
			$lists[$count]['tanggal'] = $result[3];
			$lists[$count]['id_kasir'] = $result[4];
			$lists[$count]['kassa_id'] = $result[5];
			$lists[$count]['shift_id'] = $result[6];
			$lists[$count]['harga'] = $result[7];			
			$lists[$count]['harga_member'] = $result[8];	
			$lists[$count]['qty'] = $result[9];	
			$lists[$count]['discount'] = $result[10];	
			$lists[$count]['total'] = $result[11];	
			$lists[$count]['flag_sync'] = json_encode($result[12]);	
			$lists[$count]['id_jual_sum'] = $result[13];	
			$lists[$count]['harga_hpp3'] = $result[14];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"id\") "
				." FROM \"t_jual_detail_sum\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);

		if($object->harga == '') { $object->harga = '0'; }
		if($object->harga_member == '') { $object->harga_member = '0'; }
		if($object->harga_hpp3 == '') { $object->harga_hpp3 = '0'; }
		if($object->qty == '') { $object->qty = '0'; }
		if($object->discount == '') { $object->discount = '0'; }
		if($object->total == '') { $object->total = '0'; }
		if($object->flag_sync == '') { $object->flag_sync = 'f'; }
		
		$sql = "INSERT INTO \"t_jual_detail_sum\" (\"item_code\", \"description\", \"tanggal\", \"id_kasir\", \"kassa_id\", \"shift_id\", \"harga\", \"harga_member\", \"qty\", \"discount\", \"total\", \"flag_sync\", \"id_jual_sum\", \"harga_hpp3\") "
				." VALUES ('".$object->item_code."','".pg_escape_string($object->description)."','".$object->tanggal."','".$object->id_kasir."','".$object->kassa_id."','".$object->shift_id."','".$object->harga."','".$object->harga_member."','".$object->qty."','".$object->discount."','".$object->total."','".$object->flag_sync."',".$object->id_jual_sum.",'".$object->harga_hpp3."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_jual_detail\" SET ";
		$flag = false;
		if($object->item_code != '') { if($flag){ $sql .= ","; } $sql .= " \"item_code\" = '".$object->item_code."' "; $flag = true; }
		if($object->description != '') { if($flag){ $sql .= ","; } $sql .= " \"description\" = '".pg_escape_string($object->description)."' "; $flag = true; }
		if($object->harga != '') { if($flag){ $sql .= ","; } $sql .= " \"harga\" = '".$object->harga."' "; $flag = true; }
		if($object->harga_member != '') { if($flag){ $sql .= ","; } $sql .= " \"harga_member\" = '".$object->harga_member."' "; $flag = true; }
		if($object->qty != '') { if($flag){ $sql .= ","; } $sql .= " \"qty\" = '".$object->qty."' "; $flag = true; }
		if($object->satuan != '') { if($flag){ $sql .= ","; } $sql .= " \"satuan\" = '".$object->satuan."' "; $flag = true; }
		if($object->discount != '') { if($flag){ $sql .= ","; } $sql .= " \"discount\" = '".$object->discount."' "; $flag = true; }
		if($object->total != '') { if($flag){ $sql .= ","; } $sql .= " \"total\" = '".$object->total."' "; $flag = true; }
		if($object->promo != '') { if($flag){ $sql .= ","; } $sql .= " \"promo\" = '".$object->promo."' "; $flag = true; }		
		$sql .= " WHERE \"id_jual_item\" = '".$object->id_jual_item."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goUpdateJual($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_jual_detail_sum\" SET ";
		$flag = false;
		if($object->total != '') { if($flag){ $sql .= ","; } $sql .= " \"total\" = '".$object->total."' "; $flag = true; }
		if($object->flag_sync != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = '".$object->flag_sync."' "; $flag = true; }
		$sql .= " WHERE \"id_jual_sum\" = '".$object->id_jual_sum."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goUpdate_flagSync($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"t_jual_detail_sum\" SET ";
		
		$flag = false;
		if($object->tanggal != '') { if($flag){ $sql .= ","; } $sql .= " \"tanggal\" = '".$object->tanggal."' "; $flag = true; }
		if($object->flag_sync != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = '".$object->flag_sync."' "; $flag = true; }
		$sql .= " WHERE \"tanggal\" = '".$object->tanggal."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_jual_detail\" "
				." WHERE \"id_jual_item\" = '".$object->id_jual_item."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goDeleteJual($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"t_jual_detail\" "
				." WHERE \"id_jual\" = '".$object->id_jual."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function summarize($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"item_code\", \"description\", SUM(\"harga\") as harga_sum, SUM(\"harga_member\") as harga_member_sum, SUM(\"qty\") as qty_sum, SUM(\"discount\") as discount_sum, SUM(\"total\") as total_sum "
			." FROM \"t_jual_detail_sum\" ".$condition;
			
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['item_code'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$lists[$count]['harga_sum'] = $result[2];
			$lists[$count]['harga_member_sum'] = $result[3];
			$lists[$count]['qty_sum'] = $result[4];			
			$lists[$count]['discount_sum'] = $result[5];	
			$lists[$count]['total_sum'] = $result[6];	
			$count++;
		}
		
		return $lists;
	}
	
	public function req_scheduledOrder($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT a.item_code, a.description, SUM(\"harga\") as harga_sum, SUM(\"harga_member\") as harga_member_sum, SUM(\"qty\") as qty_sum, SUM(a.discount) as discount_sum, SUM(\"total\") as total_sum "
			." FROM \"t_jual_detail_sum\" a INNER JOIN m_product b ON a.item_code = b.stock_id ".$condition;
			
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['item_code'] = $result[0];
			$lists[$count]['description'] = $result[1];
			$lists[$count]['harga_sum'] = $result[2];
			$lists[$count]['harga_member_sum'] = $result[3];
			$lists[$count]['qty_sum'] = $result[4];			
			$lists[$count]['discount_sum'] = $result[5];	
			$lists[$count]['total_sum'] = $result[6];	
			$count++;
		}
		
		return $lists;
	}
	
	public function getDMS($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT SUM(qty), tanggal "
			." FROM \"t_jual_detail_sum\" ".$condition
			." GROUP BY tanggal ORDER BY tanggal ";
			
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['qty'] = $result[0];
			$lists[$count]['tanggal'] = $result[1];
			$count++;
		}
		
		return $lists;
	}
}
