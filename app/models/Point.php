<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class Point extends Model
{

	public $id;
	public $point_get;
	public $point_use;	
	public $start_date;
	public $end_date;
	public $create_by;
	public $create_date;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"id\", \"point_get\", \"point_use\", \"start_date\", \"end_date\", \"create_by\", \"create_date\" "
				." FROM \"m_point\" ";				
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['point_get'] = $result[1];
			$lists[$count]['point_use'] = $result[2];
			$lists[$count]['start_date'] = $result[3];
			$lists[$count]['end_date'] = $result[4];
			$lists[$count]['create_by'] = $result[5];
			$lists[$count]['create_date'] = $result[6];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql =  "SELECT \"id\", \"point_get\", \"point_use\", \"start_date\", \"end_date\", \"create_by\", \"create_date\" "
				." FROM \"m_point\" "
				." WHERE \"id\" = '".$object->id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['point_get'] = $result[1];
			$lists[$count]['point_use'] = $result[2];
			$lists[$count]['start_date'] = $result[3];
			$lists[$count]['end_date'] = $result[4];
			$lists[$count]['create_by'] = $result[5];
			$lists[$count]['create_date'] = $result[6];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql =  "SELECT \"id\", \"point_get\", \"point_use\", \"start_date\", \"end_date\", \"create_by\", \"create_date\" "
				." FROM \"m_point\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['id'] = $result[0];
			$lists[$count]['point_get'] = $result[1];
			$lists[$count]['point_use'] = $result[2];
			$lists[$count]['start_date'] = $result[3];
			$lists[$count]['end_date'] = $result[4];
			$lists[$count]['create_by'] = $result[5];
			$lists[$count]['create_date'] = $result[6];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"id\") "
				." FROM \"m_point\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		if($object->point_get == '') { $object->point_get = '0'; }
		if($object->point_use == '') { $object->point_use = '0'; }
		if($object->start_date == '') { $object->start_date = date('Y-m-d'); }
		if($object->end_date == '') { $object->end_date = date('Y-m-d'); }		
		if($object->create_date == '') { $object->create_date = date('Y-m-d'); }
		
		$sql = "INSERT INTO \"m_point\" (\"point_get\", \"point_use\", \"start_date\", \"end_date\", \"create_by\", \"create_date\") "
				." VALUES ('".$object->point_get."','".$object->point_use."','".$object->start_date."','".$object->end_date."','".$object->create_by
				."','".$object->create_date."') ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}	
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"m_point\" SET ";
		$flag = false;
		if($object->point_get != '') { if($flag){ $sql .= ","; } $sql .= " \"point_get\" = '".$object->point_get."' "; $flag = true; }
		if($object->point_use != '') { if($flag){ $sql .= ","; } $sql .= " \"point_use\" = '".$object->point_use."' "; $flag = true; }
		if($object->start_date != '') { if($flag){ $sql .= ","; } $sql .= " \"start_date\" = '".$object->start_date."' "; $flag = true; }
		if($object->end_date != '') { if($flag){ $sql .= ","; } $sql .= " \"end_date\" = '".$object->end_date."' "; $flag = true; }
		$sql .= " WHERE \"id\" = '".$object->id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_point\" "
				." WHERE \"id\" = '".$object->id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
}
