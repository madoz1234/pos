<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class NilaiPersediaan extends Model
{

	public $stock_id;
	public $qty;
	public $tanggal;	
	public $flag_sync;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"stock_id\", \"qty\", \"map\", \"nilai\", \"tanggal\", \"flag_sync\" "
				." FROM \"nilai_persediaan\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['qty'] = $result[1];
			$lists[$count]['map'] = $result[2];
			$lists[$count]['nilai'] = $result[3];
			$lists[$count]['tanggal'] = $result[4];
			$lists[$count]['flag_sync'] = $result[5];	
			$count++;
		}
		
		return $lists;
	}

	public function getDataSyncPersediaan()
	{	

		$list = [];
		$nilai_persediaan = new NilaiPersediaan();
		$condition = " WHERE flag_sync = false LIMIT 10";
		$lists_ad = $nilai_persediaan::getFreeSQL($condition);

		$c_nilai_persediaan = 0;
		if(count($lists_ad)>0){
			foreach($lists_ad as $list1){
				$list[$c_nilai_persediaan]['stock_id']    = $list1['stock_id'];
				$list[$c_nilai_persediaan]['qty'] = $list1['qty'];
				$list[$c_nilai_persediaan]['map']     = $list1['map'];
				$list[$c_nilai_persediaan]['nilai']     = $list1['nilai'];
				$list[$c_nilai_persediaan]['tanggal']     = $list1['tanggal'];
				$c_nilai_persediaan++;
			}
		}

		return $list;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"stock_id\", \"qty\", \"map\", \"nilai\", \"tanggal\", \"flag_sync\" "
				." FROM \"nilai_persediaan\" "
				." WHERE \"stock_id\" = '".$object->stock_id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['qty'] = $result[1];
			$lists[$count]['map'] = $result[2];
			$lists[$count]['nilai'] = $result[3];
			$lists[$count]['tanggal'] = $result[4];
			$lists[$count]['flag_sync'] = $result[5];	
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"stock_id\", \"qty\", \"map\", \"nilai\", \"tanggal\", \"flag_sync\" "
				." FROM \"nilai_persediaan\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['qty'] = $result[1];
			$lists[$count]['map'] = $result[2];
			$lists[$count]['nilai'] = $result[3];
			$lists[$count]['tanggal'] = $result[4];
			$lists[$count]['flag_sync'] = $result[5];	
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"stock_id\") "
				." FROM \"nilai_persediaan\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}

	public static function queryNilaiPersediaan($object)
	{
		$sql = "INSERT INTO \"nilai_persediaan\" (\"stock_id\", \"qty\", \"map\", \"nilai\" "
				.", \"tanggal\", \"flag_sync\" ) VALUES ";

		return $sql;
	}
	
	public static function insertSql($object)
	{
		if($object->qty == '') { $object->qty = '0'; }	
		if($object->map == '') { $object->map = '0'; }	
		if($object->nilai == '') { $object->nilai = '0'; }	
		if($object->flag_sync == '') { $object->flag_sync = '0'; }	

		$sql = "('".$object->stock_id."','".$object->qty."','".$object->map."','".$object->nilai."','"
				.$object->tanggal."','".$object->flag_sync."'),";

		return $sql;
	}

	public static function InsertEndSql($object)
	{
		if($object->qty == '') { $object->qty = '0'; }	
		if($object->map == '') { $object->map = '0'; }	
		if($object->nilai == '') { $object->nilai = '0'; }	
		if($object->flag_sync == '') { $object->flag_sync = '0'; }		

		$sql = "('".$object->stock_id."','".$object->qty."','".$object->map."','".$object->nilai."','"
				.$object->tanggal."','".$object->flag_sync."') ";

		return $sql;
	}

	public static function endSql($object)
	{
		$sql = "on conflict do nothing;";
		return $sql;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " UPDATE \"nilai_persediaan\" SET ";
		if($object->flag_sync != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = '".$object->flag_sync."' "; $flag = true; }	
		$sql .= " WHERE \"stock_id\" = '".$object->stock_id."' AND \"tanggal\" = '".$object->tanggal."' ";	
		$success = $connection->execute($sql);
		
		return $success;
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"nilai_persediaan\" ";
		$sql .= " WHERE \"qty\" = '".$object->qty."' ";		
		$sql .= " AND \"stock_id\" = '".$object->stock_id."' ";	
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function executeQuery($sql){
		$connection = new Postgresql($this->di['db']);
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
