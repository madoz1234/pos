<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class EOS extends Model
{
	public $tmuk;
	public $shift;	
	public $kassa;	
	public $tanggal;	
	public $initial;	
	public $cash_income;	
	public $net_sales;	
	public $donasi;	
	public $kredit;	
	public $debit;	
	public $voucher;	
	public $piutang;	
	public $total_actual;	
	public $variance;	
	public $rec_count;	
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"tmuk\", \"shift\", \"kassa\", \"tanggal\", \"initial\", \"cash_income\", \"net_sales\", \"donasi\", \"kredit\", \"debit\", \"voucher\", \"total_actual\", \"variance\", \"rec_count\", \"piutang\" FROM \"t_eos\" ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['tmuk'] = $result[0];
			$lists[$count]['shift'] = $result[1];	
			$lists[$count]['kassa'] = $result[2];	
			$lists[$count]['tanggal'] = $result[3];	
			$lists[$count]['initial'] = $result[4];	
			$lists[$count]['cash_income'] = $result[5];	
			$lists[$count]['net_sales'] = $result[6];	
			$lists[$count]['donasi'] = $result[7];	
			$lists[$count]['kredit'] = $result[8];	
			$lists[$count]['debit'] = $result[9];	
			$lists[$count]['voucher'] = $result[10];	
			$lists[$count]['total_actual'] = $result[11];
			$lists[$count]['variance'] = $result[12];	
			$lists[$count]['rec_count'] = $result[13];	
			$lists[$count]['piutang'] = $result[14];	
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = " SELECT \"tmuk\", \"shift\", \"kassa\", \"tanggal\", \"initial\", \"cash_income\", \"net_sales\", \"donasi\", \"kredit\", \"debit\", \"voucher\", \"total_actual\", \"variance\", \"rec_count\", \"piutang\", \"id\" FROM \"t_eos\"  ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['tmuk'] = $result[0];
			$lists[$count]['shift'] = $result[1];	
			$lists[$count]['kassa'] = $result[2];	
			$lists[$count]['tanggal'] = $result[3];	
			$lists[$count]['initial'] = $result[4];	
			$lists[$count]['cash_income'] = $result[5];	
			$lists[$count]['net_sales'] = $result[6];	
			$lists[$count]['donasi'] = $result[7];	
			$lists[$count]['kredit'] = $result[8];	
			$lists[$count]['debit'] = $result[9];	
			$lists[$count]['voucher'] = $result[10];	
			$lists[$count]['total_actual'] = $result[11];
			$lists[$count]['variance'] = $result[12];	
			$lists[$count]['rec_count'] = $result[13];
			$lists[$count]['piutang'] = $result[14];
			$lists[$count]['id'] = $result[15];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(*) "
				." FROM \"t_eos\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);	
		
		$sql = "INSERT INTO \"t_eos\" (\"tmuk\", \"shift\", \"kassa\", \"tanggal\", \"initial\", \"cash_income\", \"net_sales\", \"donasi\", \"kredit\", \"debit\", \"voucher\", \"total_actual\", \"variance\", \"rec_count\", \"piutang\") "
				." VALUES ('".$object->tmuk."','".$object->shift."','".$object->kassa."','".$object->tanggal."',".$object->initial.",".$object->cash_income.",".$object->net_sales.",".$object->donasi.",".$object->kredit.",".$object->debit.",".$object->voucher.",".$object->total_actual.",".$object->variance.",".$object->rec_count.",".$object->piutang.") ";
		
		$success = $connection->execute($sql);		
		
		return $sql;
	}

	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		$sql = " UPDATE \"t_eos\" SET ";
		$flag = false;
		if($object->flag_sync != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_sync\" = '".$object->flag_sync."' "; $flag = true; }
		$sql .= " WHERE \"id\" = '".$object->id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;		
	}
}
