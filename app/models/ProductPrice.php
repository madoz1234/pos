<?php

use Phalcon\Mvc\Model;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class ProductPrice extends Model
{

	public $stock_id;
	public $sales_type_id;
	public $curr_abrev;
	public $price;	
	public $margin;
	public $average_cost;
	public $uom1_suggested_price;
	public $uom1_rounding_price;	
	public $uom1_changed_price;	
	public $uom1_member_price;	
	public $uom1_margin_amount;	
	public $uom2_suggested_price;	
	public $uom2_rounding_price;	
	public $uom2_changed_price;	
	public $uom2_member_price;	
	public $uom2_margin_amount;	
	public $uom3_suggested_price;	
	public $uom3_rounding_price;	
	public $uom3_changed_price;	
	public $uom3_member_price;	
	public $uom3_margin_amount;	
	public $uom4_suggested_price;	
	public $uom4_rounding_price;	
	public $uom4_changed_price;	
	public $uom4_member_price;	
	public $uom4_margin_amount;	
	public $uom1_cost_price;	
	public $uom2_cost_price;	
	public $uom3_cost_price;	
	public $uom4_cost_price;
	public $tanggal_change;
	
	public function getAll(){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"stock_id\", \"sales_type_id\", \"curr_abrev\", \"price\", \"margin\", \"average_cost\" "
				." , \"uom1_suggested_price\", \"uom1_rounding_price\", \"uom1_changed_price\", \"uom1_member_price\", \"uom1_margin_amount\", \"uom1_cost_price\" "
				." , \"uom2_suggested_price\", \"uom2_rounding_price\", \"uom2_changed_price\", \"uom2_member_price\", \"uom2_margin_amount\", \"uom2_cost_price\" "
				." , \"uom3_suggested_price\", \"uom3_rounding_price\", \"uom3_changed_price\", \"uom3_member_price\", \"uom3_margin_amount\", \"uom3_cost_price\" "
				." , \"uom4_suggested_price\", \"uom4_rounding_price\", \"uom4_changed_price\", \"uom4_member_price\", \"uom4_margin_amount\", \"uom4_cost_price\", \"tanggal_change\" "				
				." FROM \"m_product_price\" ";		
		
		$results = $connection->query($sql);

		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['sales_type_id'] = $result[1];
			$lists[$count]['curr_abrev'] = $result[2];
			$lists[$count]['price'] = $result[3];
			$lists[$count]['margin'] = $result[4];			
			$lists[$count]['average_cost'] = $result[5];	
			$lists[$count]['uom1_suggested_price'] = $result[6];	
			$lists[$count]['uom1_rounding_price'] = $result[7];
			$lists[$count]['uom1_changed_price'] = $result[8];
			$lists[$count]['uom1_member_price'] = $result[9];
			$lists[$count]['uom1_margin_amount'] = $result[10];
			$lists[$count]['uom1_cost_price'] = $result[11];
			$lists[$count]['uom2_suggested_price'] = $result[12];	
			$lists[$count]['uom2_rounding_price'] = $result[13];
			$lists[$count]['uom2_changed_price'] = $result[14];
			$lists[$count]['uom2_member_price'] = $result[15];
			$lists[$count]['uom2_margin_amount'] = $result[16];
			$lists[$count]['uom2_cost_price'] = $result[17];
			$lists[$count]['uom3_suggested_price'] = $result[18];	
			$lists[$count]['uom3_rounding_price'] = $result[19];
			$lists[$count]['uom3_changed_price'] = $result[20];
			$lists[$count]['uom3_member_price'] = $result[21];
			$lists[$count]['uom3_margin_amount'] = $result[22];
			$lists[$count]['uom3_cost_price'] = $result[23];
			$lists[$count]['uom4_suggested_price'] = $result[24];	
			$lists[$count]['uom4_rounding_price'] = $result[25];
			$lists[$count]['uom4_changed_price'] = $result[26];
			$lists[$count]['uom4_member_price'] = $result[27];
			$lists[$count]['uom4_margin_amount'] = $result[28];
			$lists[$count]['uom4_cost_price'] = $result[29];
			$lists[$count]['tanggal_change'] = $result[30];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFirst($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"stock_id\", \"sales_type_id\", \"curr_abrev\", \"price\", \"margin\", \"average_cost\" "
				." , \"uom1_suggested_price\", \"uom1_rounding_price\", \"uom1_changed_price\", \"uom1_member_price\", \"uom1_margin_amount\", \"uom1_cost_price\" "
				." , \"uom2_suggested_price\", \"uom2_rounding_price\", \"uom2_changed_price\", \"uom2_member_price\", \"uom2_margin_amount\", \"uom2_cost_price\" "
				." , \"uom3_suggested_price\", \"uom3_rounding_price\", \"uom3_changed_price\", \"uom3_member_price\", \"uom3_margin_amount\", \"uom3_cost_price\" "
				." , \"uom4_suggested_price\", \"uom4_rounding_price\", \"uom4_changed_price\", \"uom4_member_price\", \"uom4_margin_amount\", \"uom4_cost_price\", \"tanggal_change\" "
				." FROM \"m_product_price\" "
				." WHERE \"stock_id\" = '".$object->stock_id."' "
				." LIMIT 1 ";
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['sales_type_id'] = $result[1];
			$lists[$count]['curr_abrev'] = $result[2];
			$lists[$count]['price'] = $result[3];
			$lists[$count]['margin'] = $result[4];			
			$lists[$count]['average_cost'] = $result[5];	
			$lists[$count]['uom1_suggested_price'] = $result[6];	
			$lists[$count]['uom1_rounding_price'] = $result[7];
			$lists[$count]['uom1_changed_price'] = $result[8];
			$lists[$count]['uom1_member_price'] = $result[9];
			$lists[$count]['uom1_margin_amount'] = $result[10];
			$lists[$count]['uom1_cost_price'] = $result[11];
			$lists[$count]['uom2_suggested_price'] = $result[12];	
			$lists[$count]['uom2_rounding_price'] = $result[13];
			$lists[$count]['uom2_changed_price'] = $result[14];
			$lists[$count]['uom2_member_price'] = $result[15];
			$lists[$count]['uom2_margin_amount'] = $result[16];
			$lists[$count]['uom2_cost_price'] = $result[17];
			$lists[$count]['uom3_suggested_price'] = $result[18];	
			$lists[$count]['uom3_rounding_price'] = $result[19];
			$lists[$count]['uom3_changed_price'] = $result[20];
			$lists[$count]['uom3_member_price'] = $result[21];
			$lists[$count]['uom3_margin_amount'] = $result[22];
			$lists[$count]['uom3_cost_price'] = $result[23];
			$lists[$count]['uom4_suggested_price'] = $result[24];	
			$lists[$count]['uom4_rounding_price'] = $result[25];
			$lists[$count]['uom4_changed_price'] = $result[26];
			$lists[$count]['uom4_member_price'] = $result[27];
			$lists[$count]['uom4_margin_amount'] = $result[28];
			$lists[$count]['uom4_cost_price'] = $result[29];
			$lists[$count]['tanggal_change'] = $result[30];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"stock_id\", \"sales_type_id\", \"curr_abrev\", \"price\", \"margin\", \"average_cost\" "
				." , \"uom1_suggested_price\", \"uom1_rounding_price\", \"uom1_changed_price\", \"uom1_member_price\", \"uom1_margin_amount\", \"uom1_cost_price\" "
				." , \"uom2_suggested_price\", \"uom2_rounding_price\", \"uom2_changed_price\", \"uom2_member_price\", \"uom2_margin_amount\", \"uom2_cost_price\" "
				." , \"uom3_suggested_price\", \"uom3_rounding_price\", \"uom3_changed_price\", \"uom3_member_price\", \"uom3_margin_amount\", \"uom3_cost_price\" "
				." , \"uom4_suggested_price\", \"uom4_rounding_price\", \"uom4_changed_price\", \"uom4_member_price\", \"uom4_margin_amount\", \"uom4_cost_price\", \"tanggal_change\" "
				." FROM \"m_product_price\" ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['sales_type_id'] = $result[1];
			$lists[$count]['curr_abrev'] = $result[2];
			$lists[$count]['price'] = $result[3];
			$lists[$count]['margin'] = $result[4];			
			$lists[$count]['average_cost'] = $result[5];	
			$lists[$count]['uom1_suggested_price'] = $result[6];	
			$lists[$count]['uom1_rounding_price'] = $result[7];
			$lists[$count]['uom1_changed_price'] = $result[8];
			$lists[$count]['uom1_member_price'] = $result[9];
			$lists[$count]['uom1_margin_amount'] = $result[10];
			$lists[$count]['uom1_cost_price'] = $result[11];
			$lists[$count]['uom2_suggested_price'] = $result[12];	
			$lists[$count]['uom2_rounding_price'] = $result[13];
			$lists[$count]['uom2_changed_price'] = $result[14];
			$lists[$count]['uom2_member_price'] = $result[15];
			$lists[$count]['uom2_margin_amount'] = $result[16];
			$lists[$count]['uom2_cost_price'] = $result[17];
			$lists[$count]['uom3_suggested_price'] = $result[18];	
			$lists[$count]['uom3_rounding_price'] = $result[19];
			$lists[$count]['uom3_changed_price'] = $result[20];
			$lists[$count]['uom3_member_price'] = $result[21];
			$lists[$count]['uom3_margin_amount'] = $result[22];
			$lists[$count]['uom3_cost_price'] = $result[23];
			$lists[$count]['uom4_suggested_price'] = $result[24];	
			$lists[$count]['uom4_rounding_price'] = $result[25];
			$lists[$count]['uom4_changed_price'] = $result[26];
			$lists[$count]['uom4_member_price'] = $result[27];
			$lists[$count]['uom4_margin_amount'] = $result[28];
			$lists[$count]['uom4_cost_price'] = $result[29];
			$lists[$count]['tanggal_change'] = $result[30];
			
			$count++;
		}
		
		return $lists;
	}
	
	public function getFreeSQL2($cond){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT \"stock_id\", \"sales_type_id\", \"curr_abrev\", \"price\", \"margin\", \"average_cost\" "
				." , \"uom1_suggested_price\", \"uom1_rounding_price\", \"uom1_changed_price\", \"uom1_margin_amount\", \"uom1_cost_price\" "
				." , \"uom2_suggested_price\", \"uom2_rounding_price\", \"uom2_changed_price\", \"uom2_margin_amount\", \"uom2_cost_price\" "
				." , \"uom3_suggested_price\", \"uom3_rounding_price\", \"uom3_changed_price\", \"uom3_margin_amount\", \"uom3_cost_price\" "
				." , \"uom4_suggested_price\", \"uom4_rounding_price\", \"uom4_changed_price\", \"uom4_margin_amount\", \"uom4_cost_price\", \"tanggal_change\" "
				." FROM \"m_product_price\" ".$cond;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['sales_type_id'] = $result[1];
			$lists[$count]['curr_abrev'] = $result[2];
			$lists[$count]['price'] = $result[3];
			$lists[$count]['margin'] = $result[4];			
			$lists[$count]['average_cost'] = $result[5];	
			$lists[$count]['uom1_suggested_price'] = $result[6];	
			$lists[$count]['uom1_rounding_price'] = $result[7];
			$lists[$count]['uom1_changed_price'] = $result[8];
			$lists[$count]['uom1_member_price'] = $result[9];
			$lists[$count]['uom1_margin_amount'] = $result[10];
			$lists[$count]['uom1_cost_price'] = $result[11];
			$lists[$count]['uom2_suggested_price'] = $result[12];	
			$lists[$count]['uom2_rounding_price'] = $result[13];
			$lists[$count]['uom2_changed_price'] = $result[14];
			$lists[$count]['uom2_member_price'] = $result[15];
			$lists[$count]['uom2_margin_amount'] = $result[16];
			$lists[$count]['uom2_cost_price'] = $result[17];
			$lists[$count]['uom3_suggested_price'] = $result[18];	
			$lists[$count]['uom3_rounding_price'] = $result[19];
			$lists[$count]['uom3_changed_price'] = $result[20];
			$lists[$count]['uom3_member_price'] = $result[21];
			$lists[$count]['uom3_margin_amount'] = $result[22];
			$lists[$count]['uom3_cost_price'] = $result[23];
			$lists[$count]['uom4_suggested_price'] = $result[24];	
			$lists[$count]['uom4_rounding_price'] = $result[25];
			$lists[$count]['uom4_changed_price'] = $result[26];
			$lists[$count]['uom4_member_price'] = $result[27];
			$lists[$count]['uom4_margin_amount'] = $result[28];
			$lists[$count]['uom4_cost_price'] = $result[29];
			$lists[$count]['tanggal_change'] = $result[30];
			
			$count++;
		}
		
		return $lists;
	}

	public function getCount($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(\"stock_id\") "
				." FROM \"m_product_price\" ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}
	
	public function getCountLabel($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(a.stock_id) "
				." FROM m_product_price a INNER JOIN m_product b on a.stock_id = b.stock_id ".$condition;
		
		$results = $connection->query($sql);

		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$count = $result[0];						
		}
		
		return $count;
	}

	public function getCountLabel2($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT COUNT(a.stock_id), a.\"stock_id\", b.\"uom1_prod_nm\", a.\"uom1_changed_price\", a.\"uom1_rounding_price\" "
				." FROM m_product_price a INNER JOIN m_product b on a.stock_id = b.stock_id ".$condition;
		
		$results = $connection->query($sql);
		$lists = null;
		$count = 0;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[1];
			$lists[$count]['uom1_prod_nm'] = $result[2];
			if($result[3] > 0){
				$lists[$count]['harga'] = $result[3];
			}else{
				$lists[$count]['harga'] = $result[4];
			}

			$count++;
		}
		
		return $lists;
	}
	
	public function goInsert($object){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "INSERT INTO \"m_product_price\" (\"stock_id\", \"sales_type_id\", \"curr_abrev\", \"price\", \"margin\", \"average_cost\" "
				." , \"uom1_suggested_price\", \"uom1_rounding_price\", \"uom1_changed_price\", \"uom1_member_price\", \"uom1_margin_amount\", \"uom1_cost_price\" "
				." , \"uom2_suggested_price\", \"uom2_rounding_price\", \"uom2_changed_price\", \"uom2_member_price\", \"uom2_margin_amount\", \"uom2_cost_price\" "
				." , \"uom3_suggested_price\", \"uom3_rounding_price\", \"uom3_changed_price\", \"uom3_member_price\", \"uom3_margin_amount\", \"uom3_cost_price\" "
				." , \"uom4_suggested_price\", \"uom4_rounding_price\", \"uom4_changed_price\", \"uom4_member_price\", \"uom4_margin_amount\", \"uom4_cost_price\" "
				." ) "		
				." VALUES ('".$object->stock_id."','".$object->sales_type_id."','".$object->curr_abrev."','".$object->price."','".$object->margin."','"
				.$object->average_cost."','".$object->uom1_suggested_price."','".$object->uom1_rounding_price."','".$object->uom1_changed_price."','".$object->uom1_member_price."','".$object->uom1_margin_amount."','"
				.$object->uom1_cost_price."','".$object->uom2_suggested_price."','".$object->uom2_rounding_price."','".$object->uom2_changed_price."','".$object->uom2_member_price."','".$object->uom2_margin_amount."','"
				.$object->uom2_cost_price."','".$object->uom3_suggested_price."','".$object->uom3_rounding_price."','".$object->uom3_changed_price."','".$object->uom3_member_price."','".$object->uom3_margin_amount."','"
				.$object->uom3_cost_price."','".$object->uom4_suggested_price."','".$object->uom4_rounding_price."','".$object->uom4_changed_price."','".$object->uom4_member_price."','".$object->uom4_margin_amount."','"
				.$object->uom4_cost_price."') ";
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goMultiInsertSync($field, $value){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "INSERT INTO \"m_product_price\" ( ".$field." ) "		
				." VALUES ".$value." ";
		
		$success = $connection->execute($sql);
		$id = $connection->lastInsertId();
		
		return $success;
	}
	
	public function goUpdate($object){
		$connection = new Postgresql($this->di['db']);
		$sql = " UPDATE \"m_product_price\" SET ";
		$flag = false;
		if($object->sales_type_id != '') { if($flag){ $sql .= ","; } $sql .= " \"sales_type_id\" = '".$object->sales_type_id."' "; $flag = true; }
		if($object->curr_abrev != '') { if($flag){ $sql .= ","; } $sql .= " \"curr_abrev\" = '".$object->curr_abrev."' "; $flag = true; }
		if($object->price != '') { if($flag){ $sql .= ","; } $sql .= " \"price\" = '".$object->price."' "; $flag = true; }
		if($object->margin != '') { if($flag){ $sql .= ","; } $sql .= " \"margin\" = '".$object->margin."' "; $flag = true; }
		if($object->average_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"average_cost\" = '".$object->average_cost."' "; $flag = true; }
		if($object->uom1_suggested_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_suggested_price\" = '".$object->uom1_suggested_price."' "; $flag = true; }
		if($object->uom1_rounding_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_rounding_price\" = '".$object->uom1_rounding_price."' "; $flag = true; }
		if($object->uom1_changed_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_changed_price\" = '".$object->uom1_changed_price."' "; $flag = true; }
		if($object->uom1_member_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_member_price\" = '".$object->uom1_member_price."' "; $flag = true; }
		if($object->uom1_margin_amount != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_margin_amount\" = '".$object->uom1_margin_amount."' "; $flag = true; }
		if($object->uom1_cost_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_cost_price\" = '".$object->uom1_cost_price."' "; $flag = true; }	
		if($object->uom2_suggested_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_suggested_price\" = '".$object->uom2_suggested_price."' "; $flag = true; }
		if($object->uom2_rounding_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_rounding_price\" = '".$object->uom2_rounding_price."' "; $flag = true; }
		if($object->uom2_changed_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_changed_price\" = '".$object->uom2_changed_price."' "; $flag = true; }
		if($object->uom2_member_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_member_price\" = '".$object->uom2_member_price."' "; $flag = true; }
		if($object->uom2_margin_amount != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_margin_amount\" = '".$object->uom2_margin_amount."' "; $flag = true; }
		if($object->uom2_cost_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_cost_price\" = '".$object->uom2_cost_price."' "; $flag = true; }
		if($object->uom3_suggested_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_suggested_price\" = '".$object->uom3_suggested_price."' "; $flag = true; }
		if($object->uom3_rounding_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_rounding_price\" = '".$object->uom3_rounding_price."' "; $flag = true; }
		if($object->uom3_changed_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_changed_price\" = '".$object->uom3_changed_price."' "; $flag = true; }
		if($object->uom3_member_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_member_price\" = '".$object->uom3_member_price."' "; $flag = true; }
		if($object->uom3_margin_amount != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_margin_amount\" = '".$object->uom3_margin_amount."' "; $flag = true; }
		if($object->uom3_cost_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_cost_price\" = '".$object->uom3_cost_price."' "; $flag = true; }
		if($object->uom4_suggested_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_suggested_price\" = '".$object->uom4_suggested_price."' "; $flag = true; }
		if($object->uom4_rounding_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_rounding_price\" = '".$object->uom4_rounding_price."' "; $flag = true; }
		if($object->uom4_changed_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_changed_price\" = '".$object->uom4_changed_price."' "; $flag = true; }
		if($object->uom4_member_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_member_price\" = '".$object->uom4_member_price."' "; $flag = true; }
		if($object->uom4_margin_amount != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_margin_amount\" = '".$object->uom4_margin_amount."' "; $flag = true; }
		if($object->uom4_cost_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_cost_price\" = '".$object->uom4_cost_price."' "; $flag = true; }
		if($object->flag_price != '') { if($flag){ $sql .= ","; } $sql .= " \"flag_price\" = '".$object->flag_price."' "; $flag = true; }
		if($object->tanggal_change != '') { if($flag){ $sql .= ","; } $sql .= " \"tanggal_change\" = '".$object->tanggal_change."' "; $flag = true; }
		$sql .= " WHERE \"stock_id\" = '".$object->stock_id."' ";		
		
		$success = $connection->execute($sql);
		
		return $success;
	}

	public static function insertSql($object)
	{
		$sql = "INSERT INTO \"m_product_price\" (\"stock_id\", \"sales_type_id\", \"curr_abrev\", \"price\", \"margin\", \"average_cost\" "
				." , \"uom1_suggested_price\", \"uom1_rounding_price\", \"uom1_changed_price\", \"uom1_member_price\", \"uom1_margin_amount\", \"uom1_cost_price\" "
				." , \"uom2_suggested_price\", \"uom2_rounding_price\", \"uom2_changed_price\", \"uom2_member_price\", \"uom2_margin_amount\", \"uom2_cost_price\" "
				." , \"uom3_suggested_price\", \"uom3_rounding_price\", \"uom3_changed_price\", \"uom3_member_price\", \"uom3_margin_amount\", \"uom3_cost_price\" "
				." , \"uom4_suggested_price\", \"uom4_rounding_price\", \"uom4_changed_price\", \"uom4_member_price\", \"uom4_margin_amount\", \"uom4_cost_price\" "
				." ) "		
				." VALUES ('".$object->stock_id."','".$object->sales_type_id."','".$object->curr_abrev."','".$object->price."','".$object->margin."','"
				.$object->average_cost."','".$object->uom1_suggested_price."','".$object->uom1_rounding_price."','".$object->uom1_changed_price."','".$object->uom1_member_price."','".$object->uom1_margin_amount."','"
				.$object->uom1_cost_price."','".$object->uom2_suggested_price."','".$object->uom2_rounding_price."','".$object->uom2_changed_price."','".$object->uom2_member_price."','".$object->uom2_margin_amount."','"
				.$object->uom2_cost_price."','".$object->uom3_suggested_price."','".$object->uom3_rounding_price."','".$object->uom3_changed_price."','".$object->uom3_member_price."','".$object->uom3_margin_amount."','"
				.$object->uom3_cost_price."','".$object->uom4_suggested_price."','".$object->uom4_rounding_price."','".$object->uom4_changed_price."','".$object->uom4_member_price."','".$object->uom4_margin_amount."','"
				.$object->uom4_cost_price."'); ";
		return $sql;
	}

	public static function updateSql($object)
	{
		$sql = " UPDATE \"m_product_price\" SET ";
		$flag = false;
		if($object->sales_type_id != '') { if($flag){ $sql .= ","; } $sql .= " \"sales_type_id\" = '".$object->sales_type_id."' "; $flag = true; }
		if($object->curr_abrev != '') { if($flag){ $sql .= ","; } $sql .= " \"curr_abrev\" = '".$object->curr_abrev."' "; $flag = true; }
		if($object->price != '') { if($flag){ $sql .= ","; } $sql .= " \"price\" = '".$object->price."' "; $flag = true; }
		if($object->margin != '') { if($flag){ $sql .= ","; } $sql .= " \"margin\" = '".$object->margin."' "; $flag = true; }
		if($object->average_cost != '') { if($flag){ $sql .= ","; } $sql .= " \"average_cost\" = '".$object->average_cost."' "; $flag = true; }
		if($object->uom1_suggested_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_suggested_price\" = '".$object->uom1_suggested_price."' "; $flag = true; }
		if($object->uom1_rounding_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_rounding_price\" = '".$object->uom1_rounding_price."' "; $flag = true; }
		if($object->uom1_changed_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_changed_price\" = '".$object->uom1_changed_price."' "; $flag = true; }
		if($object->uom1_member_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_member_price\" = '".$object->uom1_member_price."' "; $flag = true; }
		if($object->uom1_margin_amount != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_margin_amount\" = '".$object->uom1_margin_amount."' "; $flag = true; }
		if($object->uom1_cost_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom1_cost_price\" = '".$object->uom1_cost_price."' "; $flag = true; }	
		if($object->uom2_suggested_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_suggested_price\" = '".$object->uom2_suggested_price."' "; $flag = true; }
		if($object->uom2_rounding_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_rounding_price\" = '".$object->uom2_rounding_price."' "; $flag = true; }
		if($object->uom2_changed_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_changed_price\" = '".$object->uom2_changed_price."' "; $flag = true; }
		if($object->uom2_member_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_member_price\" = '".$object->uom2_member_price."' "; $flag = true; }
		if($object->uom2_margin_amount != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_margin_amount\" = '".$object->uom2_margin_amount."' "; $flag = true; }
		if($object->uom2_cost_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom2_cost_price\" = '".$object->uom2_cost_price."' "; $flag = true; }
		if($object->uom3_suggested_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_suggested_price\" = '".$object->uom3_suggested_price."' "; $flag = true; }
		if($object->uom3_rounding_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_rounding_price\" = '".$object->uom3_rounding_price."' "; $flag = true; }
		if($object->uom3_changed_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_changed_price\" = '".$object->uom3_changed_price."' "; $flag = true; }
		if($object->uom3_member_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_member_price\" = '".$object->uom3_member_price."' "; $flag = true; }
		if($object->uom3_margin_amount != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_margin_amount\" = '".$object->uom3_margin_amount."' "; $flag = true; }
		if($object->uom3_cost_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom3_cost_price\" = '".$object->uom3_cost_price."' "; $flag = true; }
		if($object->uom4_suggested_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_suggested_price\" = '".$object->uom4_suggested_price."' "; $flag = true; }
		if($object->uom4_rounding_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_rounding_price\" = '".$object->uom4_rounding_price."' "; $flag = true; }
		if($object->uom4_changed_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_changed_price\" = '".$object->uom4_changed_price."' "; $flag = true; }
		if($object->uom4_member_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_member_price\" = '".$object->uom4_member_price."' "; $flag = true; }
		if($object->uom4_margin_amount != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_margin_amount\" = '".$object->uom4_margin_amount."' "; $flag = true; }
		if($object->uom4_cost_price != '') { if($flag){ $sql .= ","; } $sql .= " \"uom4_cost_price\" = '".$object->uom4_cost_price."' "; $flag = true; }
		$sql .= " WHERE \"stock_id\" = '".$object->stock_id."'; ";

		return $sql;		
	}
	
	public function goDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_product_price\" "
				." WHERE \"stock_id\" = '".$object->stock_id."' ";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function goMultiDelete($object){
		$connection = new Postgresql($this->di['db']);
				
		$sql = " DELETE FROM \"m_product_price\" "
				." WHERE \"stock_id\" IN (".$object.")";
		
		$success = $connection->execute($sql);		
		
		return $success;
	}
	
	public function getJoin_Product($condition){
		$connection = new Postgresql($this->di['db']);
		
		$sql = "SELECT mpp.\"stock_id\", \"sales_type_id\", \"curr_abrev\", \"price\", mpp.\"margin\", \"average_cost\" "
				." , 
				
					CASE
						WHEN uom1_changed_price != 0 THEN uom1_changed_price
						WHEN uom2_changed_price != 0 THEN uom2_changed_price
						WHEN uom3_changed_price != 0 THEN uom3_changed_price
						WHEN uom4_changed_price != 0 THEN uom4_changed_price
						ELSE 0
					END 
						AS uom_changed_price,
						
					CASE
						WHEN uom1_rounding_price != 0 THEN uom1_rounding_price
						WHEN uom2_rounding_price != 0 THEN uom2_rounding_price
						WHEN uom3_rounding_price != 0 THEN uom3_rounding_price
						WHEN uom4_rounding_price != 0 THEN uom4_rounding_price
						ELSE 0
					END 
						AS uom_rounding_price,
					
					CASE
						WHEN uom1_suggested_price != 0 THEN uom1_suggested_price
						WHEN uom2_suggested_price != 0 THEN uom2_suggested_price
						WHEN uom3_suggested_price != 0 THEN uom3_suggested_price
						WHEN uom4_suggested_price != 0 THEN uom4_suggested_price
						ELSE 0
					END 
						AS uom_suggested_price,
						
					CASE
						WHEN uom1_htype_1 IS true THEN uom1_internal_barcode
						WHEN uom2_htype_1 IS true THEN uom2_internal_barcode
						WHEN uom3_htype_1 IS true THEN uom3_internal_barcode
						WHEN uom4_htype_1 IS true THEN uom4_internal_barcode
						ELSE ''
					END 
						AS uom_barcode,
				
						\"uom1_margin_amount\", \"uom1_cost_price\" "
				." , 	\"uom2_margin_amount\", \"uom2_cost_price\" "
				." , 	\"uom3_margin_amount\", \"uom3_cost_price\" "
				." , 	\"uom4_margin_amount\", \"uom4_cost_price\" "
				." FROM \"m_product_price\" mpp INNER JOIN \"m_product\" mp ON mpp.stock_id = mp.stock_id ".$condition;
		
		$results = $connection->query($sql);
		
		$count = 0;
		$lists = null;
		$results->setFetchMode(Phalcon\Db::FETCH_NUM);
		while ($result = $results->fetchArray()) {
			$lists[$count]['stock_id'] = $result[0];
			$lists[$count]['sales_type_id'] = $result[1];
			$lists[$count]['curr_abrev'] = $result[2];
			$lists[$count]['price'] = $result[3];
			$lists[$count]['margin'] = $result[4];			
			$lists[$count]['average_cost'] = $result[5];	
			$lists[$count]['uom_changed_price'] = $result[6];	
			$lists[$count]['uom_rounding_price'] = $result[7];
			$lists[$count]['uom_suggested_price'] = $result[8];
			$lists[$count]['uom_barcode'] = $result[9];
			$lists[$count]['uom1_margin_amount'] = $result[10];
			$lists[$count]['uom1_cost_price'] = $result[11];
			$lists[$count]['uom2_margin_amount'] = $result[12];
			$lists[$count]['uom2_cost_price'] = $result[13];
			$lists[$count]['uom3_margin_amount'] = $result[14];
			$lists[$count]['uom3_cost_price'] = $result[15];
			$lists[$count]['uom4_margin_amount'] = $result[16];
			$lists[$count]['uom4_cost_price'] = $result[17];
			
			$count++;
		}
		
		return $lists;
	}

	public function executeQuery($sql){
		$connection = new Postgresql($this->di['db']);
		$success = $connection->execute($sql);		
		
		return $success;
	}
}
