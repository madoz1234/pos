<?php

use Phalcon\Mvc\Controller;

class PembayaranTransaksiController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'PembayaranTransaksi' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$condition = " WHERE flag = false ";
		$dataPT = new PembayaranTransaksi();
		$this->view->lists = $dataPT::getJoin_Supplier($condition);
		
		$condition = " WHERE flag = true AND lower(status) = 'disetujui' ";
		$this->view->lists_h = $dataPT::getJoin_Supplier($condition);
	}
	
	public function ajaxOptAction(){
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		foreach($lists_tmuk as $list_tmuk){
			$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
			$tmuk->tmuk = $list_tmuk['tmuk'];
			$tmuk->nama = $list_tmuk['name'];
			$tmuk->supplier_id = $list_tmuk['supplier_id'];
			$tmuk->saldo = $list_tmuk['saldo'];
		}
		$product = new Product();
		$condition = " WHERE tipe = '".$_GET['cond']."' ORDER BY description ";
		$listsProduct = $product::getFreeSQL($condition);
		$data_product = null;
		$i = 0;
		$j = 0;

		if($_GET['cond'] == 001 || $_GET['cond'] == 003){
			$tmuk = new TMUK();
			$lists_tmuk = $tmuk::getALL();
			foreach($lists_tmuk as $list_tmuk){
				$supp[$j]['id'] = 0;
				$supp[$j]['name'] = $list_tmuk['nama_lsi'];
				$j++;
			}
		}

		if($_GET['cond'] == 002){
			$dataSupplier = new Supplier();
			$lists = $dataSupplier::getAll();
			if(count($lists) > 0){
				foreach($lists as $list){
					$supp[$j]['id'] = $list['supplier_id'];
					$supp[$j]['name'] = $list['supp_name'];
					$j++;
				}
			}
		}

		if($_GET['cond'] == 004){
			$supp[0]['id'] = 100;
			$supp[0]['name'] = $tmuk->nama;
		}

		foreach($listsProduct as $list){
			$data_product[$i]['val'] = $list['stock_id'];	
			$data_product[$i]['display'] = $list['description'];
			$data_product[$i]['barcode'] = $list['uom1_internal_barcode'];
			$i++;
		}

		$variable = array( 'variable1' => $data_product, 
	                       'variable2' => $supp );
		
		return json_encode($variable);
	}
	
	public function addAction()
	{
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		foreach($lists_tmuk as $list_tmuk){
			$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
			$tmuk->tmuk = $list_tmuk['tmuk'];
			$tmuk->nama = $list_tmuk['name'];
			$tmuk->supplier_id = $list_tmuk['supplier_id'];
			$tmuk->saldo = $list_tmuk['saldo'];
		}
		
		$saldo = (new TMUKController())->getSaldo();	

		$curl = new CurlClass();
		$result = $curl->get($this->di['api']['link']."/api/tmuk/get-mengendap");
		$saldo_mengendap = json_decode($result, true);

		$cek = new CurlClass();
		$data = $cek->post($this->di['api']['link']."/api/tmuk/get-scn", [
			'tmuk' => $tmuk->tmuk
		]);
		$scn = json_decode($data, true);

		$cek_locked = new CurlClass();
		$data_locked = $cek_locked->post($this->di['api']['link']."/api/tmuk/get-locked", [
			'tmuk' => $tmuk->tmuk
		]);
		$locked = json_decode($data_locked, true);

		$available = ($saldo + $scn['escrow']) - $locked['locked'] - $saldo_mengendap['message'];
		$this->view->saldo = $available;
		$this->AuthorityAction();
	}
	
	public function editAction(){
		$this->AuthorityAction();
		
		$dataPT = new PembayaranTransaksi();
		$dataPTD = new PembayaranTransaksiDetail();
		$dataProduct = new Product();
		$dataSupplier = new Supplier();

		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		foreach($lists_tmuk as $list_tmuk){
			$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
			$tmuk->tmuk = $list_tmuk['tmuk'];
			$tmuk->supplier_id = $list_tmuk['supplier_id'];
			$tmuk->saldo = $list_tmuk['saldo'];
		}
		
		$saldo = (new TMUKController())->getSaldo();	

		$curl = new CurlClass();
		$result = $curl->get($this->di['api']['link']."/api/tmuk/get-mengendap");
		$saldo_mengendap = json_decode($result, true);

		$cek = new CurlClass();
		$data = $cek->post($this->di['api']['link']."/api/tmuk/get-scn", [
			'tmuk' => $tmuk->tmuk
		]);
		$scn = json_decode($data, true);

		$cek_locked = new CurlClass();
		$data_locked = $cek_locked->post($this->di['api']['link']."/api/tmuk/get-locked", [
			'tmuk' => $tmuk->tmuk
		]);
		$locked = json_decode($data_locked, true);

		$available = ($saldo + $scn['escrow']) - $locked['locked'] - $saldo_mengendap['message'];
		
		$conditionPTD = " WHERE id.payment_no = '".$_REQUEST['payment_no']."' ";
		$conditionProduct = " WHERE LOWER(supplied_by) = 'non lotte' OR UPPER(mb_flag) = 'D' ORDER BY description ";
		
		$dataPT->payment_no = $_REQUEST['payment_no'];
		$this->view->saldo = $available;
		$this->view->listsPT = $dataPT::getFirst($dataPT);
		$this->view->listsPTD = $dataPTD::getJoin_Header($conditionPTD);
		$this->view->listsProduct = $dataProduct::getFreeSQL($conditionProduct);
		$this->view->listsSupplier = $dataSupplier::getAll();
	}
	
	public function updateAction(){
		$dataPT = new PembayaranTransaksi();
		$dataPTD = new PembayaranTransaksiDetail();
		
		$inv_datas = json_decode($_POST['inv_data']);
		$json_datas = json_decode($_POST['json_data']);	
		
		$count = 0;
		
		foreach($inv_datas as $list_inv){			
			$dataPT->payment_no = $list_inv->payment_no;
			$dataPT->supp_id = $list_inv->supplier_code;
			$dataPT->tanggal = $list_inv->tanggal;
			$dataPT->status = $list_inv->status;			
			$dataPT->memo = $list_inv->memo;			
			$dataPT->bayar = $list_inv->bayar;			
			$success = $dataPT::goUpdate($dataPT);
			
			$dataPTD->payment_no = $list_inv->payment_no;
			$dataPTD::goDelete($dataPTD);
			
			foreach($json_datas as $list_json){				
				$dataPTD->stock_id = $list_json->item_code;
				$dataPTD->price = $list_json->biaya;
				$dataPTD->qty = $list_json->quantity;				
				$success = $dataPTD::goInsert($dataPTD);
				
				$count++;
			}
			
			if($success){
				$this->flashSession->success("Berhasil;Invoice telah diupdate.;success");
				$data['message'] = '<b>'.$list_inv->payment_no.'</b> Berhasil diubah.';

				$notif = new Notifikasi();
				$notif->data = $data['message'];
				$notif->read_at = null;
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by = $this->session->get("user")['user_name'];

				$sukses = $notif::goInsert($notif);
				$this->response->redirect(uri('PembayaranTransaksi'));
			}else{
				$this->flashSession->success("Gagal;Invoice gagal diupdate.;error");
				$this->response->redirect(uri('PembayaranTransaksi'));
			}
		}
	}
	
	public function insertAction(){
		$dataPT = new PembayaranTransaksi();
		$dataPTD = new PembayaranTransaksiDetail();
		
		$inv_datas = json_decode($_POST['inv_data']);
		$json_datas = json_decode($_POST['json_data']);	
		foreach($inv_datas as $list_inv){
			$number_range = new NumberRange();
			$number_range->trans_type = 'PYR';
			$number_range->period = date('Ym');
			$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
			$lists = $number_range::getFreeSQL($condition);
			
			if(isset($lists)){
				foreach($lists as $list){
					$number_range->trans_type = $list['trans_type'];
					$number_range->period = $list['period'];
					$number_range->prefix = $list['prefix'];
					$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
				}
			}else{
				$number_range->trans_type = 'PYR';
				$number_range->period = date('Ym');
				$number_range->prefix = 'PYR';
				$number_range->from_ = '1';
				$number_range->to_ = '9999';
				$number_range->current_no = '1';
				$success = $number_range::goInsert($number_range);			
				$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
			}
			
			$tmuk = new TMUK();
			$lists_tmuk = $tmuk::getAll();
			
			if(isset($lists_tmuk)){
				foreach($lists_tmuk as $list_tmuk){
					$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
					$tmuk->tmuk = $list_tmuk['tmuk'];
					$tmuk->supplier_id = $list_tmuk['supplier_id'];
				}
			}
			
			$dataPT->inv_no = $list_inv->invoice_no;
			$dataPT->supp_id = $list_inv->supplier_code;
			$dataPT->tanggal = $list_inv->tanggal;
			$dataPT->status = $list_inv->status;
			$dataPT->payment_no = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no; 		
			$dataPT->memo = $list_inv->memo;
			$dataPT->tipe = $list_inv->tipe;
			$dataPT->bayar = $list_inv->bayar;
			$dataPT->flag_ho = 0;
			$dataPT::goInsert($dataPT);
			
			$count = 0;
			foreach($json_datas as $list_json){	
				$dataPTD->payment_no = $dataPT->payment_no;
				$dataPTD->stock_id = $list_json->item_code;
				$dataPTD->price = $list_json->biaya * $list_json->quantity;
				$dataPTD->qty = $list_json->quantity;				
				$success = $dataPTD::goInsert($dataPTD);
				
				$count++;
			}
			
			if($success){
				$success_num = $number_range::goAddNumber($number_range);
				$this->flashSession->success("Berhasil;Invoice berhasil dibuat.;success");
				$data['message'] = '<b>'.$list_inv->invoice_no.'</b> Berhasil dibuat.';

				$notif = new Notifikasi();
				$notif->data = $data['message'];
				$notif->read_at = null;
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by = $this->session->get("user")['user_name'];

				$sukses = $notif::goInsert($notif);
				$this->response->redirect(uri('PembayaranTransaksi'));
			}else{
				$this->flashSession->success("Gagal;Invoice gagal dibuat, silahkan buat kembali.;error");
				$this->response->redirect(uri('PembayaranTransaksi'));
			}
		}
	}
	
	public function send_invoiceAction(){	
		$data = null;
		$dataTMUK = TMUK::getAll();
		
		$condition = " WHERE payment_no = '".$_REQUEST['payment_no']."' ";
		$conditionPTD = " WHERE id.payment_no = '".$_REQUEST['payment_no']."' ";
		$inv_datas = PembayaranTransaksi::getFreeSQL($condition);
		$json_datas = PembayaranTransaksiDetail::getJoin_Header($conditionPTD);

		$cond = " WHERE payment_no = '".$_REQUEST['payment_no']."' ";
		$cek = PembayaranTransaksiDetail::getSumTotal($cond);
		
		$payment_no = '';
		
		foreach($inv_datas as $list_inv){			
			$data['sup'] = $list_inv['supp_id'];
			
			foreach($dataTMUK as $tmuklist){
				$data['tmuk'] = $tmuklist['tmuk'];
			}
			
			$payment_no = $list_inv['payment_no'];
			
			$data['tgl'] = $list_inv['tanggal'];
			$data['inv'] = $list_inv['payment_no'];
			$data['memo'] = $list_inv['memo'];
			$data['total'] = $cek;
			$data['vendor'] = $list_inv['supp_id'];
			$data['tipe'] = $list_inv['tipe'];
			$data['bayar'] = $list_inv['bayar'];
			
			$count = 0;
			foreach($json_datas as $list_json){
				$cond = " WHERE mpm.stock_id = '".$list_json['stock_id']."' ";
				$lists = Product::get_non_map($cond);
			
				if($lists){
					foreach($lists as $list){
						// sesuai meeting tgl 6 juli 2017, pak bagus jika qty stok minus, maka jadi 0
						if($list['qty_stock'] < 0) { $list['qty_stock'] =  0; }
						$map = (($list_json['qty'] * $list_json['price']) + ($list['qty_stock'] * $list['uom1_cost_price'])) / ($list['qty_stock'] + $list_json['qty']);
					}
				}else{
					$map = $list_json['price'];
				}
					
				$xxx[$count]['no'] = $count + 1;
				$xxx[$count]['id'] = $list_json['stock_id'];
				$xxx[$count]['qty'] = $list_json['qty'];
				$xxx[$count]['price'] = $list_json['price'];
				$xxx[$count]['map'] = $map;
				$xxx['total'] += ($list_json['qty'] * $list_json['price']);
				$prods[] = $xxx[$count];
				$count++;
			}
		}

		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
			}
		}

		// get saldo
		$saldo = (new TMUKController())->getSaldo();

		$curl = new CurlClass();
		$result = $curl->get($this->di['api']['link']."/api/tmuk/get-mengendap");
		$saldo_mengendap = json_decode($result, true);

		$cek = new CurlClass();
		$data_scn = $cek->post($this->di['api']['link']."/api/tmuk/get-scn", [
			'tmuk' => $tmuk->tmuk
		]);
		$scn = json_decode($data_scn, true);

		$cek_locked = new CurlClass();
		$data_locked = $cek_locked->post($this->di['api']['link']."/api/tmuk/get-locked", [
			'tmuk' => $tmuk->tmuk
		]);
		$locked = json_decode($data_locked, true);

		$available = ($saldo + $scn['escrow']) - $locked['locked'] - $saldo_mengendap['message'];
		if($available > 0){
			$curl = new CurlClass();
			$result = $curl->post($this->di['api']['link']."/api/pyr/send-pyr", [
				'pyr' => $data,
				'detail' => $prods
			]);
			$obj = json_decode($result, true);
			
			if($obj['status'] == 'success'){
				$dataPT = new PembayaranTransaksi();
				$dataPT->flag = 'true';
				$dataPT->payment_no = $_REQUEST['payment_no'];
				$dataPT::goUpdate($dataPT);
				$this->flashSession->success("Berhasil;Invoice berhasil di Sync ke IMMBO.;success");
				$data['message'] = 'Sync <b>'.$_REQUEST['payment_no'].'</b> Berhasil.';

				$notif = new Notifikasi();
				$notif->data = $data['message'];
				$notif->read_at = null;
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by = $this->session->get("user")['user_name'];

				$sukses = $notif::goInsert($notif);
				$this->response->redirect(uri('PembayaranTransaksi'));	
			}else{
				$this->flashSession->success("Gagal;Invoice gagal di Sync ke IMMBO.;error");
				$this->response->redirect(uri('PembayaranTransaksi'));
			}
		}else{
			$this->flashSession->success('Gagal;Jumlah saldo anda yang tersedia : <b>Rp '.number_format($available,0,",",".").'</b>.;error');
			$this->response->redirect(uri('PembayaranTransaksi'));	
		}
	}
	
	public function deleteAction(){
		$this->AuthorityAction();
		
		$dataPT = new PembayaranTransaksi();
		$dataPT->payment_no = $_REQUEST['payment_no'];
		$success = $dataPT::goDelete($dataPT);
		
		$this->response->redirect(uri('PembayaranTransaksi'));
	}
	
	public function printAction(){
		
	}
}
