<?php

use Phalcon\Mvc\Controller;

class LoginController extends Controller
{	
	
	public function indexAction()
	{
		
	}

	public function loginAction()
	{			
		// get data post
		$user = new User();

		$user->user_name = pg_escape_string($this->request->getPost("Username"));		
		$user->user_password = pg_escape_string(base64_encode($this->request->getPost("Password")));		
		
		//$user->user_name = $this->request->getPost("Username");		
		//$user->user_password = base64_encode($this->request->getPost("Password"));				
		$lists = $user::getFirst($user);
		
		$count = 0;
		foreach($lists as $list){		
			$user->user_id = $list['user_id'];
			$user->user_name = $list['user_name'];
			$user->role_id = $list['role_id'];
			$user->real_name = $list['real_name'];
			$user->last_login = $list['last_login'];
			$user->count_login = $list['count_login'];
			$user->date_created = $list['date_created'];	
			$user->user_password = $list['user_password'];
			$user->is_active = $list['is_active'];
			$user->images = $list['images'];			

			$count = $count + 1;
		}		
		
		if($count > 0){
			$this->session->set("is_login", "X");
			
			$this->session->set('user', array(
				'user_id' => $user->user_id,
				'user_name' => $user->user_name,
				'role_id' => $user->role_id,
				'real_name' => $user->real_name,
				'last_login' => $user->last_login,
				'count_login' => $user->count_login,
				'date_created' => $user->date_created,
				'user_password' => $user->user_password,
				'is_active' => $user->is_active,
				'images' => $user->images
			));
			$this->response->redirect(uri('Dashboard'));
		}else{			
			$this->view->disable();
			$this->response->redirect(uri('login'));
		}
	}

}
