<?php

use Phalcon\Mvc\Controller;

require_once ('../app/library/src/Autoloader.php');

PhpXmlRpc\Autoloader::register();
use PhpXmlRpc\Value;
use PhpXmlRpc\Request;
use PhpXmlRpc\Client;

class KustomerController extends Controller
{
	public function getKustomer()
	{
		$data = new Cust();

		// dd($data);
		$lists = $data->getAll();
		// dd($lists);
		$jk = 0;
		
		if(count($lists) > 0){
			foreach($lists as $list){
				$id = $list['id'];
				$kode = $list['kode'];

				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/kustomer/get-kustomer", [
					'id' => $id,
					'kode' => $kode,
				]);
				//dd($result);
				// transform json to object array php
				$obj = json_decode($result, true);
				if ($obj['status'] == 'success') {
					$jk = $obj['message'];

					// dd($jk);
					// loop
					foreach ($jk as $val) {
						$rcd    = new Cust();
						$result = $rcd::getFreeSQL(" WHERE \"debtor_ref\" = '".$val['tmuk']['nama']."' ");

						// dd($result);
						if (count($result) > 0) { // update
							$update = new Cust();
							$update->debtor_no = $val['id'];
							$update->name = $val['membercard']['nama'];
							$update->debtor_ref = $val['tmuk']['nama'];
							$update->address = $val['alamat'];
							$update->tax_id = $val['pajak_id'];
							$update->curr_code = $val['kode'];
							$update->discount = $val['persen_diskon'];
							$update->pymt_discount = $val['status_kredit'];
							$update->credit_limit = $val['limit_kredit'];
							$update->notes = $val['kode'];
							$update->inactive = $val['status_kredit'];
							$update->nomor_member = $val['membercard']['nomor'];
							$success = $update::goUpdate($update);
							
						} else { //insert
							$insert = new Cust();
							$insert->debtor_no = $val['id'];
							$insert->name = $val['membercard']['nama'];
							$insert->debtor_ref = $val['tmuk']['nama'];
							$insert->address = $val['alamat'];
							$insert->tax_id = $val['pajak_id'];
							$insert->curr_code = $val['kode'];
							$insert->discount = $val['persen_diskon'];
							$insert->pymt_discount = $val['status_kredit'];
							$insert->credit_limit = $val['limit_kredit'];
							$insert->notes = $val['kode'];
							$insert->inactive = $val['status_kredit'];
							$insert->nomor_member = $val['membercard']['nomor'];
							$success = $insert::goInsert($insert);
						}
					}
				}
			}						
		}
		return $jk;
	}

}