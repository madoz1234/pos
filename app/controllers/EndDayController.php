<?php

use Phalcon\Mvc\Controller;
include __DIR__ . "/../library/print_format.php";

class EndDayController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'EndDay' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$last_sync = new LastSync();
		$this->view->lists = $last_sync::getAll();
	}		
	
	public function cetak_EODAction(){
		$device = "COM2";
		// $device = "SOMETHING.txt";
		exec("mode $device BAUD=38400 PARITY=n DATA=8 STOP=1 xon=off octs=off rts=on to=on");
		$comport = fopen($device, "w");
		
		$total = 0; $count = 0; $dpp = 0; $ppn = 0; $total_barang = 0; $net_sales = 0; $total_act = 0; $setoran = 0; $cash = 0; 
		$tunai = 0; $donasi = 0; $debet = 0; $credit = 0; $voucher = 0; $piutang = 0; $cash_income = 0; $kembalian = 0; $rec_count = 0;
		
		$cond_SED = " WHERE flag_end = false ORDER BY id DESC LIMIT 1 ";
		$listsSED = StartEndDay::getFreeSQL($cond_SED);
		
		if($listsSED){
			foreach($listsSED as $list_SED){
				$tanggal = $list_SED['tanggal'];
			}
		}
		
		if($tanggal == '') { return false; }
		$cond_SES = " WHERE tanggal = '".$tanggal."' AND flag_end_day = false ";
		$listsSES = StartEndShift::getFreeSQL($cond_SES);
		
		$cond_Jual = " WHERE DATE(tanggal) = '".$tanggal."' ";
		$cond = " WHERE DATE(tanggal) = '".$tanggal."' AND flag_sync = false ";
		
		$listsTMUK = TMUK::getAll();
		$listsJual = Jual::getCount($cond_Jual);
		// $listsJualSum = JualSum::getFreeSQL($cond);
		// $listsJualBayarSum = JualBayarSum::getFreeSQL($cond);
		$condition1 = "WHERE DATE(j.tanggal) = '".$tanggal."' group by date(j.tanggal) ";

		$condition2 = "WHERE DATE(j.tanggal) = '".$tanggal."' group by b.tipe ";
		
		$listsJualSum = Jual::getTotJualDay($condition1);
		$listsJualBayarSum = Jual::getTotJualBayarDay($condition2);
		
		if($listsTMUK){ 
			foreach($listsTMUK as $listtm){
				$tmuk_name = $listtm['name'];
				$tmuk_code = $listtm['tmuk'];
				$tmuk_telp = $listtm['telp']; 
				$owner = $listtm['owner'];
				$length = $listtm['length_struk'];
			}
		}
		
		if($listsSES){
			foreach($listsSES as $listses){				
				$setoran += $listses['setoran'];
				$cash += $listses['cash_cashier'];
			}
		}
		// dd($listsJual);
		
		if($listsJual){
			$rec_count = $listsJual;
		}
		
		if($listsJualSum){
			foreach($listsJualSum as $listjs){
				$ppn += $listjs['tax'];
				$cash_income += $listjs['total'] + $ppn;
				$net_sales += $listjs['total'];
				$kembalian += $listjs['kembalian'];
			}
		}
		
		if($listsJualBayarSum){		
			foreach($listsJualBayarSum as $listjbs){
				if($listjbs['tipe'] == 'TUNAI'){ $tunai += $listjbs['total']; } 
				if($listjbs['tipe'] == 'DONASI'){ $donasi += $listjbs['total']; } 
				if($listjbs['tipe'] == 'KREDIT'){ $credit += $listjbs['total']; } 
				if($listjbs['tipe'] == 'DEBIT'){ $debet += $listjbs['total']; } 
				if($listjbs['tipe'] == 'VOUCHER'){ $voucher += $listjbs['total']; } 
				if($listjbs['tipe'] == 'PIUTANG'){ $piutang += $listjbs['total']; } 
				
				$total_act += $listjbs['total'];
			}
		}
		
		$length_3 = round(0.062 * $length);
		$length_4 = round(0.083 * $length);		
		$length_5 = round(0.104 * $length);
		$length_8 = round(0.166 * $length);
		$length_9 = round(0.187 * $length);		
		$length_10 = round(0.208 * $length);
		$length_12 = round(0.250 * $length);		
		$length_13 = round(0.270 * $length);		
		$length_14 = round(0.291 * $length);		
		$length_16 = round(0.333 * $length);		
		$length_18 = round(0.375 * $length);		
		$length_19 = round(0.395 * $length);
		$length_20 = round(0.416 * $length);
		$length_24 = round(0.500 * $length);
		$length_25 = round(0.520 * $length);
		$length_30 = round(0.625 * $length);
		$length_32 = round(0.666 * $length);
		$length_48 = round(1 * $length);
		
		/*echo 'setoran#'.$setoran.'<br>';
		echo 'cash#'.$cash.'<br>';
		echo 'net sales#'.$net_sales.'<br>';
		echo 'donasi#'.$donasi.'<br>';
		echo 'kredit#'.$credit.'<br>';
		echo 'debit#'.$debet.'<br>';
		echo 'voucher#'.$voucher.'<br>';
		echo 'piutang#'.$piutang.'<br>';
		echo 'total actual#'.($tunai - $kembalian + $credit + $debet + $voucher).'<br>';
		echo 'variance#'.($cash - $net_sales).'<br>';
		echo 'rec count#'.$rec_count.'<br>';
		
		return false;*/
		
		$dataEOD = new EOD();
		$dataEOD->tmuk = $tmuk_code;
		$dataEOD->tanggal = $tanggal;
		$dataEOD->initial = $setoran;
		$dataEOD->cash_income = $cash;
		$dataEOD->net_sales = $net_sales;
		$dataEOD->donasi = $donasi;
		$dataEOD->kredit = $credit;
		$dataEOD->debit = $debet;
		$dataEOD->voucher = $voucher;
		$dataEOD->piutang = $piutang;
		$dataEOD->total_actual = $net_sales;
		$dataEOD->variance = $cash - $net_sales;
		$dataEOD->rec_count = $rec_count;
		$success = $dataEOD->goInsert($dataEOD);

		foreach($listsSED as $list_s){					
			$start_day = new StartEndDay();
			$start_day->tanggal = $list_s['tanggal'];
			$start_day->id = $list_s['id'];
			$start_day->flag_end = true;
			$success = $start_day::goUpdate($start_day);
		}	
		
		$str = print_center('END OF DAY'); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
				
		$str = print_left('Store Code  : '.$tmuk_code, $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_left('Store Name  : '.$tmuk_name, $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_left('Date        : '.date('d-m-Y', strtotime($tanggal)), $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);

		$str = print_left('SALES', $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '-'; }		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_right('Initial', $length_32).print_right(number_format($setoran,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('Cash Income', $length_32).print_right(number_format($cash,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('Net Sales', $length_32).print_right(number_format($net_sales,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('Net Return', $length_32).print_right(number_format('0',0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('PPN', $length_32).print_right(number_format('0',0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '-'; }		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_right('Total Sales', $length_32).print_right(number_format($net_sales,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '='; }		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_left('ACTUAL', $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '-'; }		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_right('Cash', $length_32).print_right(number_format($net_sales,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('Cash Income', $length_32).print_right(number_format($cash,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('Donasi', $length_32).print_right(number_format($donasi,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('Kredit', $length_32).print_right(number_format($credit,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('Debit', $length_32).print_right(number_format($debet,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('Voucher', $length_32).print_right(number_format($voucher,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('A/R Anggota', $length_32).print_right(number_format($piutang,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '-'; }		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_right('Total Actual', $length_32).print_right(number_format(($net_sales + $credit + $debet + $voucher),0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);	
		$str = print_right('Variance', $length_32).print_right(number_format(($cash - $net_sales),0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);	  	
		$str = print_right('Variance Paid', $length_32).print_right(number_format('0',0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);	
		$str = print_right('Receipt Count', $length_32).print_right(number_format($rec_count,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);	
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '='; }		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_center('Mengetahui Store Leader'); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_center('(     '.$owner.'     )'); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '-'; }		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_center('Print Date : '.date('d-m-Y H:i:s')); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = "\x00\x1Bi\x00"; fputs($comport, $str);
		fclose($comport);
		
		$dataSES = new StartEndShift();
		$dataJS = new JualSum();
		$dataJDS = new JualDetailSum();
		$dataJBS = new JualBayarSum();
		
		$dataSES->tanggal = $tanggal;
		$dataSES->flag_end_day = true;
		$success = $dataSES::goUpdate_flagEndDay($dataSES);
		
		$dataJS->tanggal = $tanggal;
		$dataJS->flag_sync = true;
		$success = $dataJS::goUpdate_flagSync($dataJS);
		
		$dataJDS->tanggal = $tanggal;
		$dataJDS->flag_sync = true;
		$success = $dataJDS::goUpdate_flagSync($dataJDS);
		
		$dataJBS->tanggal = $tanggal;
		$dataJBS->flag_sync = true;
		$success = $dataJDS::goUpdate_flagSync($dataJBS);
	}
}
