<?php

use Phalcon\Mvc\Controller;

class NotifikasiController extends Controller
{
	public function indexAction()
	{
		$notif = new Notifikasi();
		$lists = $notif::getFreeSQL();

		// update reaad at
		$notif->readAll();


		$this->view->data = $lists;
	}
	
	public function updateAction()
	{
		$user = new User();
		$user->user_id = $this->request->getPost("User_ID");
		$user->user_name = $this->request->getPost("User_Name");
		$user->role_id = $this->request->getPost("Role_ID");
		$user->real_name = $this->request->getPost("Real_Name");

		if ($this->request->hasFiles() == true) {
            // Print the real file names and their sizes
        	$file = $this->request->getUploadedFiles()[0];
            $file->moveTo('upload/' . $file->getName());
        	$user->images = 'upload/' . $file->getName();
		// echo (new \Phalcon\Debug\Dump())->variable($file, "file"); exit;
       	}

		$user->user_password = $this->request->getPost("User_Password");
		$user->user_password = base64_encode($user->user_password);
		$success = $user::goUpdate($user);
		
		if($success){			
			$this->response->redirect(uri('User'));
		}else{						
			$this->response->redirect(uri('User/Edit/User_ID='.$user->user_id));
		}
	}

	public function editAction()
	{
		$this->AuthorityAction();
		
		$role = new Role();
		$lists_role = $role::getAll();
		
		$data_role = null;		
		foreach($lists_role as $list_role){
			$data_role[$list_role['role_id']] = $list_role['role_name'];
		}
		
		$this->view->role = $data_role;
		
		$user = new User();
		$user->user_id = $_REQUEST['User_ID'];
		$condition = " WHERE \"user_id\" ='".$user->user_id."' LIMIT 1 ";
		$lists = $user::getFreeSQL($condition);
		
		$this->view->data = new User();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->user_id = $list['user_id'];
				$this->view->data->user_name = $list['user_name'];
				$this->view->data->role_id = $list['role_id'];
				$this->view->data->real_name = $list['real_name'];
				$this->view->data->last_login = $list['last_login'];
				$this->view->data->count_login = $list['count_login'];
				$this->view->data->date_created = $list['date_created'];
				$this->view->data->user_password = $list['user_password'];
				$this->view->data->is_active = $list['is_active'];
				$this->view->data->images = $list['images'];
			}
		}
	}
	// public function indexAction()
	// {
		
	// }
	// 
	// $member->setMemberimage(base64_encode(file_get_contents($this->request->getUploadedFiles()[0]->getTempName())));
	//
	
	// if($this->request->isPost()) {

 //    $picture = $this->request->getUploadedFiles()[0];

 //    // If uploaded
 //    if (!$picture->getError()) {$picture->moveTo('path/to/save/' . $picture->getName());}

	// }

}
