<?php

use Phalcon\Mvc\Controller;

class LabelHargaController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'LabelHarga' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
	}
	
	public function printAction()
	{
		$this->AuthorityAction();
	}
	
	public function ajaxSearchProductAction(){
		$product = new Product();
		$query = strtoupper($_GET['query']);
		$cond1 = " WHERE type != 'SO' ";
		$cond2 = " WHERE inactive = 0";
		$cond3 = " WHERE standard_cost !=0 ";
		// $condition = " WHERE (a.\"uom1_internal_barcode\" LIKE '%".$query."%' OR a.\"uom2_internal_barcode\" LIKE '%".$query."%' OR a.\"uom3_internal_barcode\" LIKE '%".$query."%' OR a.\"uom4_internal_barcode\" LIKE '%".$query."%' OR a.\"description\" LIKE '%".$query."%') AND a.mb_flag = 'B' AND a.ppob = 0 AND a.inactive = 0 ORDER BY a.description";
		$lists_product = $product::getFreeSQL2($query);
		
		$data_product = null;	
		
		for($i = 0; $i < count($lists_product); $i++){
			if($lists_product[$i]['uom1_internal_barcode'] != ''){
				$data_product[$i+1]['val'] = $lists_product[$i]['uom1_internal_barcode'];
				$data_product[$i+1]['display'] = $lists_product[$i]['uom1_internal_barcode'].' - '.$lists_product[$i]['uom1_prod_nm'];	
			}else if($lists_product[$i]['uom2_internal_barcode'] != ''){
				$data_product[$i+1]['val'] = $lists_product[$i]['uom2_internal_barcode'];
				$data_product[$i+1]['display'] = $lists_product[$i]['uom2_internal_barcode'].' - '.$lists_product[$i]['uom2_prod_nm'];	
			}else if($lists_product[$i]['uom3_internal_barcode'] != ''){
				$data_product[$i+1]['val'] = $lists_product[$i]['uom3_internal_barcode'];
				$data_product[$i+1]['display'] = $lists_product[$i]['uom3_internal_barcode'].' - '.$lists_product[$i]['uom3_prod_nm'];	
			}else{
				$data_product[$i+1]['val'] = $lists_product[$i]['uom4_internal_barcode'];
				$data_product[$i+1]['display'] = $lists_product[$i]['uom4_internal_barcode'].' - '.$lists_product[$i]['uom4_prod_nm'];	
			}
				
		}
		
		return json_encode($data_product);
	}
	
	public function ajaxChangedAction(){
		
		$data = new Product();		
		$condition = " WHERE flag_price = true AND mb_flag != 'D' AND ppob = 0 AND inactive = 0 AND status_flag !='2' AND tanggal_change > ( NOW() - INTERVAL '5 DAYS' ) ORDER by description ";
		$lists_product = $data::getJoin_ProductPrice($condition);
		
		$data_product = null;
		
		for($i = 0; $i < count($lists_product); $i++){
			if($lists_product[$i]['uom1_internal_barcode'] != ''){
				$data_product[$i+1]['val'] = $lists_product[$i]['uom1_internal_barcode'];
				$data_product[$i+1]['display'] = $lists_product[$i]['uom1_internal_barcode'].' - '.$lists_product[$i]['uom1_prod_nm'];	
			}else if($lists_product[$i]['uom2_internal_barcode'] != ''){
				$data_product[$i+1]['val'] = $lists_product[$i]['uom2_internal_barcode'];
				$data_product[$i+1]['display'] = $lists_product[$i]['uom2_internal_barcode'].' - '.$lists_product[$i]['uom2_prod_nm'];	
			}else if($lists_product[$i]['uom3_internal_barcode'] != ''){
				$data_product[$i+1]['val'] = $lists_product[$i]['uom3_internal_barcode'];
				$data_product[$i+1]['display'] = $lists_product[$i]['uom3_internal_barcode'].' - '.$lists_product[$i]['uom3_prod_nm'];	
			}else{
				$data_product[$i+1]['val'] = $lists_product[$i]['uom4_internal_barcode'];
				$data_product[$i+1]['display'] = $lists_product[$i]['uom4_internal_barcode'].' - '.$lists_product[$i]['uom4_prod_nm'];	
			}
				
		}
		
		return json_encode($data_product);
	}

}
