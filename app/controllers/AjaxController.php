<?php

use Phalcon\Mvc\Controller;

class AjaxController extends Controller
{	
	
	public function indexAction()
	{			
		//
	}

	public function checkonlineAction()
	{
		return json_encode([
			'status' => is_connected() === true ? 'online' : 'offline'
		]);
	}

	public function sendonlineAction(){
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getAll();
		$kode = "";
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$kode = $list_tmuk['tmuk'];
			}
		}
		$status = false;

		$curl = new CurlClass();
		$result = $curl->post($this->di['api']['link']."/api/ping/cek-ping", [
			'tmuk' => $kode
		]);


				// transform json to object array php
		$obj = json_decode($result, true);

		// var_dump($result);
		// die;

		if ($obj['status'] == 'success') {
			$status = true;
		}
		return json_encode([
			'status' => $status
		]);
	}

	public function syncpenjualanAction()
	{
		$jual = new Jual();
		$data = $jual->getDataSyncPenjualan();
		$response = null;

        // $this->db->begin();

		try {
			if (count($data) > 0) {
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/penjualan/send-jual", [
					'data' => json_encode($data),
				]);

				// dd($result);

				$response = json_decode($result, true);

				if (!is_null($response) && $response['status'] == 'success') {
					// update flag tiap penjualan
					foreach ($data as $row) {
						$jual = new Jual();
						$jual->id_jual   = $row['jual_kode'];
						$jual->flag_sync = true;

						$jual::goUpdate($jual);
					}
					
				} else {
					return json_encode([
						'status' => 'error',
						'message' => $result,
					]);
				}
			}
		} catch (Exception $e) {
            // $this->db->rollback();

			return json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}

        // $this->db->commit();
		return json_encode([
			'status' => true,
			'response' => $response,
		]);
	}


	public function syncpersediaanjualAction()
	{
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getAll();
		$kode = "";
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$kode = $list_tmuk['tmuk'];
			}
		}

		$persediaan_jual = new PersediaanJual();
		$data = $persediaan_jual->getDataSyncPersediaan();
		$response = null;

        // $this->db->begin();

		try {
			if (count($data) > 0) {
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/produk/send-nilai-penjualan", [
					'tmuk' => $kode,
					'data' => json_encode($data)
				]);

				$response = json_decode($result, true);
				if (!is_null($response) && $response['status'] == 'success') {
					// update flag tiap penjualan
					foreach ($data as $row) {
						$persediaan_jual = new PersediaanJual();
						$persediaan_jual->stock_id   = $row['stock_id'];
						$persediaan_jual->tanggal   = $row['tanggal'];
						$persediaan_jual->flag_sync = true;

						$persediaan_jual::goUpdate($persediaan_jual);
					}
					
				} else {
					return json_encode([
						'status' => 'error',
						'message' => $result,
					]);
				}
			}
		} catch (Exception $e) {
            // $this->db->rollback();

			return json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}

        // $this->db->commit();
		return json_encode([
			'status' => true,
			'response' => $response,
		]);
	}

	public function syncpersediaanGRAction()
	{
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getAll();
		$kode = "";
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$kode = $list_tmuk['tmuk'];
			}
		}

		$persediaan_gr = new PersediaanGR();
		$data = $persediaan_gr->getDataSyncPersediaan();
		$response = null;

        // $this->db->begin();

		try {
			if (count($data) > 0) {
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/produk/send-nilai-gr", [
					'tmuk' => $kode,
					'data' => json_encode($data)
				]);

				$response = json_decode($result, true);
				if (!is_null($response) && $response['status'] == 'success') {
					// update flag tiap pengran
					foreach ($data as $row) {
						$persediaan_gr = new PersediaanGR();
						$persediaan_gr->stock_id   = $row['stock_id'];
						$persediaan_gr->tanggal   = $row['tanggal'];
						$persediaan_gr->flag_sync = true;

						$persediaan_gr::goUpdate($persediaan_gr);
					}
					
				} else {
					return json_encode([
						'status' => 'error',
						'message' => $result,
					]);
				}
			}
		} catch (Exception $e) {
            // $this->db->rollback();

			return json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}

        // $this->db->commit();
		return json_encode([
			'status' => true,
			'response' => $response,
		]);
	}

	public function syncpersediaanADAction()
	{
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getAll();
		$kode = "";
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$kode = $list_tmuk['tmuk'];
			}
		}

		$persediaan_ad = new PersediaanAD();
		$data = $persediaan_ad->getDataSyncPersediaan();
		$response = null;

        // $this->db->begin();

		try {
			if (count($data) > 0) {
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/produk/send-nilai-ad", [
					'tmuk' => $kode,
					'data' => json_encode($data)
				]);

				$response = json_decode($result, true);
				if (!is_null($response) && $response['status'] == 'success') {
					// update flag tiap penadan
					foreach ($data as $row) {
						$persediaan_ad = new PersediaanAD();
						$persediaan_ad->stock_id   = $row['stock_id'];
						$persediaan_ad->tanggal   = $row['tanggal'];
						$persediaan_ad->flag_sync = true;

						$persediaan_ad::goUpdate($persediaan_ad);
					}
					
				} else {
					return json_encode([
						'status' => 'error',
						'message' => $result,
					]);
				}
			}
		} catch (Exception $e) {
            // $this->db->rollback();

			return json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}

        // $this->db->commit();
		return json_encode([
			'status' => true,
			'response' => $response,
		]);
	}

	public function syncpersediaanPYRAction()
	{
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getAll();
		$kode = "";
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$kode = $list_tmuk['tmuk'];
			}
		}

		$persediaan_pyr = new PersediaanPYR();
		$data = $persediaan_pyr->getDataSyncPersediaan();
		$response = null;

        // $this->db->begin();

		try {
			if (count($data) > 0) {
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/produk/send-nilai-pyr", [
					'tmuk' => $kode,
					'data' => json_encode($data)
				]);

				$response = json_decode($result, true);
				if (!is_null($response) && $response['status'] == 'success') {
					// update flag tiap penpyran
					foreach ($data as $row) {
						$persediaan_pyr = new PersediaanPYR();
						$persediaan_pyr->stock_id   = $row['stock_id'];
						$persediaan_pyr->tanggal   = $row['tanggal'];
						$persediaan_pyr->flag_sync = true;

						$persediaan_pyr::goUpdate($persediaan_pyr);
					}
					
				} else {
					return json_encode([
						'status' => 'error',
						'message' => $result,
					]);
				}
			}
		} catch (Exception $e) {
            // $this->db->rollback();

			return json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}

        // $this->db->commit();
		return json_encode([
			'status' => true,
			'response' => $response,
		]);
	}

	public function syncpersediaanRRAction()
	{
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getAll();
		$kode = "";
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$kode = $list_tmuk['tmuk'];
			}
		}

		$persediaan_rr = new PersediaanRR();
		$data = $persediaan_rr->getDataSyncPersediaan();
		$response = null;

        // $this->db->begin();

		try {
			if (count($data) > 0) {
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/produk/send-nilai-rr", [
					'tmuk' => $kode,
					'data' => json_encode($data)
				]);

				$response = json_decode($result, true);
				if (!is_null($response) && $response['status'] == 'success') {
					// update flag tiap penrran
					foreach ($data as $row) {
						$persediaan_rr = new PersediaanRR();
						$persediaan_rr->stock_id   = $row['stock_id'];
						$persediaan_rr->tanggal   = $row['tanggal'];
						$persediaan_rr->flag_sync = true;

						$persediaan_rr::goUpdate($persediaan_rr);
					}
					
				} else {
					return json_encode([
						'status' => 'error',
						'message' => $result,
					]);
				}
			}
		} catch (Exception $e) {
            // $this->db->rollback();

			return json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}

        // $this->db->commit();
		return json_encode([
			'status' => true,
			'response' => $response,
		]);
	}

	public function syncpersediaanSOAction()
	{
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getAll();
		$kode = "";
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$kode = $list_tmuk['tmuk'];
			}
		}

		$persediaan_so = new PersediaanSO();
		$data = $persediaan_so->getDataSyncPersediaan();
		$response = null;

        // $this->db->begin();

		try {
			if (count($data) > 0) {
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/produk/send-nilai-so", [
					'tmuk' => $kode,
					'data' => json_encode($data)
				]);

				$response = json_decode($result, true);
				if (!is_null($response) && $response['status'] == 'success') {
					// update flag tiap pensoan
					foreach ($data as $row) {
						$persediaan_so = new PersediaanSO();
						$persediaan_so->stock_id   = $row['stock_id'];
						$persediaan_so->tanggal   = $row['tanggal'];
						$persediaan_so->flag_sync = true;

						$persediaan_so::goUpdate($persediaan_so);
					}
					
				} else {
					return json_encode([
						'status' => 'error',
						'message' => $result,
					]);
				}
			}
		} catch (Exception $e) {
            // $this->db->rollback();

			return json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}

        // $this->db->commit();
		return json_encode([
			'status' => true,
			'response' => $response,
		]);
	}

	public function syncNilaiPersediaanAction()
	{
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getAll();
		$kode = "";
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$kode = $list_tmuk['tmuk'];
			}
		}

		$persediaan_so = new NilaiPersediaan();
		$data = $persediaan_so->getDataSyncPersediaan();
		$response = null;

        // $this->db->begin();
		try {
			if (count($data) > 0) {
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/produk/send-nilai-persediaan", [
					'tmuk' => $kode,
					'data' => json_encode($data)
				]);

				$response = json_decode($result, true);
				if (!is_null($response) && $response['status'] == 'success') {
					// update flag tiap pensoan
					foreach ($data as $row) {
						$nilai_persediaan = new NilaiPersediaan();
						$nilai_persediaan->stock_id   = $row['stock_id'];
						$nilai_persediaan->tanggal   = $row['tanggal'];
						$nilai_persediaan->flag_sync = true;

						$nilai_persediaan::goUpdate($nilai_persediaan);
					}
					
				} else {
					return json_encode([
						'status' => 'error',
						'message' => $result,
					]);
				}
			}
		} catch (Exception $e) {
            // $this->db->rollback();

			return json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}

        // $this->db->commit();
		return json_encode([
			'status' => true,
			'response' => $response,
		]);
	}
	
}
