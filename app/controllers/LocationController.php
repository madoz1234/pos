<?php

use Phalcon\Mvc\Controller;

class LocationController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'Location' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$location = new Location();
		$this->view->lists = $location::getAll();
	}
	
	public function ajaxLocationAction()
	{
		$location = new Location();
		$condition = " WHERE \"loc_code\" = '".$_GET['id']."' LIMIT 1 ";
		$data = $location::getFreeSQL($condition);

		return json_encode($data);
	}
	
	public function addAction()
	{
		$this->AuthorityAction();
	}
	
	public function editAction()
	{		
		$this->AuthorityAction();
	
		$location = new Location();
		$location->loc_code = $_REQUEST['Loc_code'];
		$lists = $location::getFirst($location);
		
		$this->view->data = new Location();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->loc_code = $list['loc_code'];
				$this->view->data->location_name = $list['location_name'];
				$this->view->data->delivery_address = $list['delivery_address'];
				$this->view->data->phone = $list['phone'];
				$this->view->data->phone2 = $list['phone2'];
				$this->view->data->fax = $list['fax'];
				$this->view->data->email = $list['email'];
				$this->view->data->contact = $list['contact'];
			}
		}
	}
	
	public function viewAction()
	{		
		$this->AuthorityAction();
		
		$location = new Location();
		$location->loc_code = $_REQUEST['Loc_code'];
		$lists = $location::getFirst($location);
		
		$this->view->data = new Location();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->loc_code = $list['loc_code'];
				$this->view->data->location_name = $list['location_name'];
				$this->view->data->delivery_address = $list['delivery_address'];
				$this->view->data->phone = $list['phone'];
				$this->view->data->phone2 = $list['phone2'];
				$this->view->data->fax = $list['fax'];
				$this->view->data->email = $list['email'];
				$this->view->data->contact = $list['contact'];
			}
		}
	}
	
	public function deleteAction($param)
	{	
		$this->AuthorityAction();
		
		$data = new Location();
		$data->loc_code = $_REQUEST["Loc_code"];
		$success = $data::goDelete($data);
	
		$this->response->redirect(uri('Location'));
	}
	
	public function insertAction()
	{		
		$data = new Location();

		$data->loc_code 		= $this->request->getPost("loc_code");
        $data->location_name 	= $this->request->getPost("location_name");
        $data->delivery_address = $this->request->getPost("delivery_address");
        $data->phone 			= $this->request->getPost("phone");
        $data->phone2 			= $this->request->getPost("phone2");
        $data->fax 				= $this->request->getPost("fax");
        $data->email 			= $this->request->getPost("email");
        $data->contact 			= $this->request->getPost("contact");
        $data->inactive 		= $this->request->getPost("inactive");
		
		$lists = $data::goInsert($data);

		if($lists){
			$this->response->redirect(uri('Location'));
		}else{
			$this->response->redirect('Location/Add');
		}
	}
	
	public function updateAction()
	{		
		$data = new Location();

		$data->loc_code 		= $this->request->getPost("loc_code");
        $data->location_name 	= $this->request->getPost("location_name");
        $data->delivery_address = $this->request->getPost("delivery_address");
        $data->phone 			= $this->request->getPost("phone");
        $data->phone2 			= $this->request->getPost("phone2");
        $data->fax 				= $this->request->getPost("fax");
        $data->email 			= $this->request->getPost("email");
        $data->contact 			= $this->request->getPost("contact");
        $data->inactive 		= $this->request->getPost("inactive");
		
		$lists = $data::goUpdate($data);
		
		if($lists){
			$this->response->redirect(uri('Location'));
		}else{
			$this->response->redirect(uri('Location/Edit?Loc_code='.$data->loc_code));
		}
	}
}
