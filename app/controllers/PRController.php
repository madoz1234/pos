<?php

use Phalcon\Mvc\Controller;

class PRController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'PR' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$pr = new PR();
		$condition = " WHERE \"po_no_immbo\" IS NULL OR \"po_no_immbo\" = '' AND \"flag_sync\" = false AND \"flag_delete\" = false ORDER BY create_date DESC ";
		$this->view->lists = $pr::getFreeSQL($condition);
		$this->view->listsHistory = $pr::getAll();
	}
	
	public function ajaxSearchProductAction(){
		$product = new Product();
		$query = strtoupper($_GET['query']);
		
		$condition = " WHERE (\"description\" LIKE '%".$query."%' OR \"uom1_internal_barcode\" LIKE '%".$query."%' OR \"uom2_internal_barcode\" LIKE '%".$query."%' OR \"uom3_internal_barcode\" LIKE '%".$query."%' OR \"uom4_internal_barcode\" LIKE '%".$query."%' OR \"uom1_prod_nm\" LIKE '%".$query."%') AND mb_flag = 'B' AND inactive = 0 AND \"status_flag\" != '2' ORDER BY description ";
		$lists_product = $product::getFreeSQL($condition);
		
		$data_product = null;	
		
		for($i = 0, $j=0; $i < count($lists_product); $i++){
			if($lists_product[$i]['uom1_internal_barcode'] != ''){
				$data_product[$j]['val'] = $lists_product[$i]['uom1_internal_barcode'];
				$data_product[$j]['display'] = $lists_product[$i]['uom1_internal_barcode'].' - '.$lists_product[$i]['description'];
				$j++;
				if($lists_product[$i]['uom1_internal_barcode'] == $query){
					$data_product = null;
					$data_product[0]['val'] = $lists_product[$i]['uom1_internal_barcode'];
					$data_product[0]['display'] = $lists_product[$i]['uom1_internal_barcode'].' - '.$lists_product[$i]['description'];				
					break;
				}
			}else
			
			if($lists_product[$i]['uom2_internal_barcode'] != ''){
				$data_product[$j]['val'] = $lists_product[$i]['uom2_internal_barcode'];
				$data_product[$j]['display'] = $lists_product[$i]['uom2_internal_barcode'].' - '.$lists_product[$i]['description'];
				$j++;
				if($lists_product[$i]['uom2_internal_barcode'] == $query){
					$data_product = null;
					$data_product[0]['val'] = $lists_product[$i]['uom2_internal_barcode'];
					$data_product[0]['display'] = $lists_product[$i]['uom2_internal_barcode'].' - '.$lists_product[$i]['description'];				
					break;
				}
			}else
			
			if($lists_product[$i]['uom3_internal_barcode'] != ''){
				$data_product[$j]['val'] = $lists_product[$i]['uom3_internal_barcode'];
				$data_product[$j]['display'] = $lists_product[$i]['uom3_internal_barcode'].' - '.$lists_product[$i]['description'];
				$j++;
				if($lists_product[$i]['uom3_internal_barcode'] == $query){
					$data_product = null;
					$data_product[0]['val'] = $lists_product[$i]['uom3_internal_barcode'];
					$data_product[0]['display'] = $lists_product[$i]['uom3_internal_barcode'].' - '.$lists_product[$i]['description'];				
					break;
				}
			}else
			
			if($lists_product[$i]['uom4_internal_barcode'] != ''){
				$data_product[$j]['val'] = $lists_product[$i]['uom4_internal_barcode'];
				$data_product[$j]['display'] = $lists_product[$i]['uom4_internal_barcode'].' - '.$lists_product[$i]['description'];
				$j++;
				if($lists_product[$i]['uom4_internal_barcode'] == $query){
					$data_product = null;
					$data_product[0]['val'] = $lists_product[$i]['uom4_internal_barcode'];
					$data_product[0]['display'] = $lists_product[$i]['uom4_internal_barcode'].' - '.$lists_product[$i]['description'];				
					break;
				}
			}
		}

		return json_encode($data_product);
	}
	
	public function addAction()
	{
		$this->AuthorityAction();
		
		$delivery_by = null;
		$delivery_by['Ambil Sendiri'] = 'Ambil Sendiri';
		$delivery_by['LSI'] = 'LSI';		
		$this->view->delivery_by = $delivery_by;

		$tmuk = new TMUK();
		$lists = $tmuk::getAll();
		
		if(isset($lists)){
			foreach($lists as $list){
				$tmuk->auto = $list['auto_approve'];
			}
		}
		$status = null;
		if($tmuk->auto <= 1){
			$status['0'] = 'Tidak Disetujui';
			$status['1'] = 'Disetujui';
		}else{
			$status['1'] = 'Disetujui';
		}

		$this->view->status = $status;
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		foreach($lists_tmuk as $list_tmuk){
			$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
			$tmuk->tmuk = $list_tmuk['tmuk'];
			$tmuk->supplier_id = $list_tmuk['supplier_id'];
			$tmuk->saldo = $list_tmuk['saldo'];
		}
		// echo 'x';
		
		$saldo = (new TMUKController())->getSaldo();	

		$curl = new CurlClass();
		$result = $curl->get($this->di['api']['link']."/api/tmuk/get-mengendap");
		$saldo_mengendap = json_decode($result, true);

		$cek = new CurlClass();
		$data = $cek->post($this->di['api']['link']."/api/tmuk/get-scn", [
			'tmuk' => $tmuk->tmuk
		]);
		$scn = json_decode($data, true);

		$cek_locked = new CurlClass();
		$data_locked = $cek_locked->post($this->di['api']['link']."/api/tmuk/get-locked", [
			'tmuk' => $tmuk->tmuk
		]);
		$locked = json_decode($data_locked, true);
		// dd()

		$available = ($saldo + $scn['escrow']) - $locked['locked'] - $saldo_mengendap['message'];
		$this->view->saldo = $available;
		//$this->view->saldo = $tmuk->saldo;
		
		$role = new Role();
		$role->role_id = $this->session->get("user")['role_id'];
		$lists_role = $role::getFirst($role);
		
		foreach($lists_role as $list_role){
			$role->role_id = $list_role['role_id'];
			$role->role_name = $list_role['role_name'];
			$role->description = $list_role['description'];
			$role->is_active = $list_role['is_active'];
			$role->pr_approve = $list_role['pr_approve'];
		}
		
		$this->view->role = $role;
		
		// default date + 2
		$tanggal = date('Y-m-d');
		$date = DateTime::createFromFormat('Y-m-d',$tanggal);
		$date->modify('+2 day');
		$tanggal = $date->format('Y-m-d');
		
		$this->view->default_date = $tanggal;
	}
	
	public function editAction()
	{
		$this->AuthorityAction();
		
		$delivery_by = null;
		$delivery_by['Ambil Sendiri'] = 'Ambil Sendiri';
		$delivery_by['LSI'] = 'LSI';		
		$this->view->delivery_by = $delivery_by;

		$tmuk = new TMUK();
		$lists = $tmuk::getAll();
		
		if(isset($lists)){
			foreach($lists as $list){
				$tmuk->auto = $list['auto_approve'];
			}
		}
		
		$status = null;
		if($tmuk->auto <= 1){
			$status['0'] = 'Tidak Disetujui';
			$status['1'] = 'Disetujui';
		}else{
			$status['1'] = 'Disetujui';
		}

		$this->view->status = $status;
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		foreach($lists_tmuk as $list_tmuk){
			$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
			$tmuk->tmuk = $list_tmuk['tmuk'];
			$tmuk->supplier_id = $list_tmuk['supplier_id'];
			$tmuk->saldo = $list_tmuk['saldo'];
		}	
		
		//$this->view->tmuk = $tmuk;
		
		$saldo = (new TMUKController())->getSaldo();	

		$curl = new CurlClass();
		$result = $curl->get($this->di['api']['link']."/api/tmuk/get-mengendap");
		$saldo_mengendap = json_decode($result, true);

		$cek = new CurlClass();
		$data = $cek->post($this->di['api']['link']."/api/tmuk/get-scn", [
			'tmuk' => $tmuk->tmuk
		]);
		$scn = json_decode($data, true);

		$cek_locked = new CurlClass();
		$data_locked = $cek_locked->post($this->di['api']['link']."/api/tmuk/get-locked", [
			'tmuk' => $tmuk->tmuk
		]);
		$locked = json_decode($data_locked, true);

		$available = ($saldo + $scn['escrow']) - $locked['locked'] - $saldo_mengendap['message'];
		$this->view->saldo = $available;
		
		$pr = new PR();
		$pr->request_no = $_REQUEST['Request_No'];
		$lists_pr = $pr::getFirst($pr);
		
		foreach($lists_pr as $list_pr){
			$pr->request_no = $list_pr['request_no'];
			$pr->point_used = $list_pr['point_used'];
			$pr->reference = $list_pr['reference'];
			$pr->delivery_by = $list_pr['delivery_by'];
			$pr->created_by = $list_pr['created_by'];
			$pr->delivery_date = $list_pr['delivery_date'];
			$pr->comments = $list_pr['comments'];
			$pr->total = $list_pr['total'];
			$pr->supplier_id = $list_pr['supplier_id'];
			$pr->tmuk = $list_pr['tmuk'];
			$pr->status = $list_pr['status'];
		}
		
		$this->view->pr = $pr;
		
		$pr_detail = new PRDetail();
		$pr_detail->request_no = $_REQUEST['Request_No'];
		$condition = " WHERE \"request_no\" = '".$pr_detail->request_no."' ";
		$lists_pr_detail = $pr_detail::getFreeSQL($condition);
		
		$this->view->pr_details = $lists_pr_detail;
		
		$role = new Role();
		$role->role_id = $this->session->get("user")['role_id'];
		$lists_role = $role::getFirst($role);
		
		foreach($lists_role as $list_role){
			$role->role_id = $list_role['role_id'];
			$role->role_name = $list_role['role_name'];
			$role->description = $list_role['description'];
			$role->is_active = $list_role['is_active'];
			$role->pr_approve = $list_role['pr_approve'];
		}	
		$this->view->role = $role;
	}
	
	public function viewAction()
	{
		$this->AuthorityAction();
	}
	
	public function deleteAction()
	{		
		$this->AuthorityAction();
		$pr = new PR();
		$pr->request_no = $_REQUEST['Request_No'];
		$pr->flag_delete = true;
		$success = $pr::goUpdate($pr);
		if($success){
			$data['message'] = '<b>'.$pr->request_no.'</b> Berhasil Dihapus.';
			$notif = new Notifikasi();
			$notif->data = $data['message'];
			$notif->read_at = null;
			$notif->created_at = date('Y-m-d H:i:s');
			$notif->created_by = $this->session->get("user")['user_name'];

			$sukses = $notif::goInsert($notif);
		}
		
		$this->response->redirect(uri('PR'));
	}
	
	public function ajaxProductAction(){
		$product = new Product();
		$object['barcode'] = $_REQUEST['barcode'];
		$object['count_index'] = $_REQUEST['count_index'];
		$data = $product::getProductPR2($object);
		
		return json_encode($data);
	}
	
	public function insertAction()
	{
		$number_range = new NumberRange();
		$number_range->trans_type = 'PR';
		$number_range->period = date('Ym');
		$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
		$lists = $number_range::getFreeSQL($condition);
		
		if(isset($lists)){
			foreach($lists as $list){
				$number_range->trans_type = $list['trans_type'];
				$number_range->period = $list['period'];
				$number_range->prefix = $list['prefix'];
				$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
			}
		}else{
			$number_range->trans_type = 'PR';
			$number_range->period = date('Ym');
			$number_range->prefix = 'PR';
			$number_range->from_ = '1';
			$number_range->to_ = '9999';
			$number_range->current_no = '1';
			$success = $number_range::goInsert($number_range);			
			$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
		}
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
			}
		}
		
		$pr = new PR();
		$pr->request_no = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;
		$pr->point_used = $this->request->getPost("point_used");
		$pr->delivery_by = $this->request->getPost("delivery_by");
		$pr->created_by = $this->request->getPost("created_by");
		$pr->delivery_date = $this->request->getPost("delivery_date");
		$pr->comments = $this->request->getPost("comments");
		$pr->total = $this->request->getPost("total");
		$pr->total = str_replace('.', '', $pr->total);
		$pr->total = str_replace(',', '.', $pr->total);
		$pr->supplier_id = $tmuk->supplier_id;
		$pr->tmuk = $tmuk->tmuk;
		$pr->status = $this->request->getPost("status");
		$success = $pr::goInsert($pr);
		
		if($success){			
			$success_num = $number_range::goAddNumber($number_range);
			
			$json_datas = json_decode($_POST['json_data']);	
			foreach($json_datas as $json_data){
				$pr_detail = new PRDetail();
				$pr_detail->request_no = $pr->request_no;
				$pr_detail->item_code = $json_data[0];
				$pr_detail->barcode = $json_data[1];
				$pr_detail->description = $json_data[2];
				$pr_detail->qty_order = $json_data[3];
				$pr_detail->unit_order = $json_data[4];
				$pr_detail->qty_sell = $json_data[5];
				$pr_detail->unit_sell = $json_data[6];
				$pr_detail->qty_pr = $json_data[7];
				$pr_detail->unit_pr = $json_data[8];				
				$pr_detail->order_price = $json_data[10];				
				$pr_detail->con_order = $json_data[11];
				$pr_detail->con_sell = $json_data[12];
				$pr_detail->con_pr = $json_data[9];
				$pr_detail->unit_order_price = $json_data[14];				
				$pr_detail->supplied_by = str_replace('%20',' ',$json_data[15]);
				$success = $pr_detail::goInsert($pr_detail);
			}
			
			$data['type'] = 'S';
			$data['message'] = 'PR berhasil Dibuat.<br/>Nomor PR : <b>'.$pr_detail->request_no.'</b>';
			$notif = new Notifikasi();
			$notif->data = $data['message'];
			$notif->read_at = null;
			$notif->created_at = date('Y-m-d H:i:s');
			$notif->created_by = $this->session->get("user")['user_name'];

			$sukses = $notif::goInsert($notif);
			return json_encode($data);
		}else{						
			$data['type'] = 'E';
			$data['message'] = 'Gagal membuat PR!';
			return json_encode($data);
		}
	}
	
	public function updateAction()
	{		
		$pr = new PR();
		$pr->request_no = $this->request->getPost("request_no");
		$pr->point_used = $this->request->getPost("point_used");		
		$pr->delivery_by = $this->request->getPost("delivery_by");
		$pr->created_by = $this->request->getPost("created_by");
		$pr->delivery_date = $this->request->getPost("delivery_date");
		$pr->comments = $this->request->getPost("comments");
		$pr->total = $this->request->getPost("total");		
		$pr->total = str_replace('.', '', $pr->total);
		$pr->total = str_replace(',', '.', $pr->total);
		$pr->status = $this->request->getPost("status");	
		$success = $pr::goUpdate($pr);
		
		if($success){	
			$json_datas = json_decode($_POST['json_data']);	
			$pr_detail_delete = new PRDetail();
			$pr_detail_delete->request_no = $this->request->getPost("request_no");
			$success = $pr_detail_delete::goDeletePR($pr_detail_delete);
			foreach($json_datas as $json_data){
				$pr_detail = new PRDetail();
				$condition = " WHERE \"request_no\" = '".$this->request->getPost("request_no")."' AND \"item_code\" = '".$json_data[0]."' ";
				$cek = $pr_detail::getFreeSQL($condition);
				if($cek){
					$pr_detail->request_no = $this->request->getPost("request_no");
					$pr_detail->item_code = $json_data[0];
					$pr_detail->barcode = $json_data[1];
					$pr_detail->description = $json_data[2];
					$pr_detail->qty_order = $json_data[3];
					$pr_detail->unit_order = $json_data[4];
					$pr_detail->qty_sell = $json_data[5];
					$pr_detail->unit_sell = $json_data[6];
					$pr_detail->qty_pr = $json_data[7];
					$pr_detail->unit_pr = $json_data[8];				
					$pr_detail->order_price = $json_data[10];				
					$pr_detail->con_order = $json_data[11];
					$pr_detail->con_sell = $json_data[12];
					$pr_detail->con_pr = $json_data[9];
					$pr_detail->unit_order_price = $json_data[14];
					$pr_detail->pr_detail_item = $json_data[15];				
					$pr_detail->supplied_by = str_replace('%20',' ',$json_data[16]);
					$success = $pr_detail::goUpdate($pr_detail);
				}else{
					$pr_detail->request_no = $this->request->getPost("request_no");
					$pr_detail->item_code = $json_data[0];
					$pr_detail->barcode = $json_data[1];
					$pr_detail->description = $json_data[2];
					$pr_detail->qty_order = $json_data[3];
					$pr_detail->unit_order = $json_data[4];
					$pr_detail->qty_sell = $json_data[5];
					$pr_detail->unit_sell = $json_data[6];
					$pr_detail->qty_pr = $json_data[7];
					$pr_detail->unit_pr = $json_data[8];				
					$pr_detail->order_price = $json_data[10];				
					$pr_detail->con_order = $json_data[11];
					$pr_detail->con_sell = $json_data[12];
					$pr_detail->con_pr = $json_data[9];
					$pr_detail->unit_order_price = $json_data[14];
					$pr_detail->pr_detail_item = $json_data[15];				
					$pr_detail->supplied_by = str_replace('%20',' ',$json_data[16]);
					$success = $pr_detail::goInsert($pr_detail);
				}		
			}
			if($success){
				$data['type'] = 'S';
				$data['message'] = '<b>'.$pr->request_no.'</b> Berhasil Update.';
				$notif = new Notifikasi();
				$notif->data = $data['message'];
				$notif->read_at = null;
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by = $this->session->get("user")['user_name'];

				$sukses = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = '<b>'.$pr->request_no.'</b> Gagal Di Update.';
				$notif = new Notifikasi();
				$notif->data = $data['message'];
				$notif->read_at = null;
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by = $this->session->get("user")['user_name'];

				$sukses = $notif::goInsert($notif);
				return json_encode($data);
			}
			
		}else{						
			$data['type'] = 'E';
			$data['message'] = '<b>'.$pr->request_no.'</b> Gagal diubah.';
			$notif = new Notifikasi();
			$notif->data = $data['message'];
			$notif->read_at = null;
			$notif->created_at = date('Y-m-d H:i:s');
			$notif->created_by = $this->session->get("user")['user_name'];

			$sukses = $notif::goInsert($notif);
			return json_encode($data);
		}
	}
	
	public function ajaxPRAction()
	{
		$pr_detail = new PRDetail();
		$condition = " WHERE \"request_no\" = '".$_GET['request']."' ";
		$data = $pr_detail::getFreeSQL($condition);

		return json_encode($data);
	}
	
	public function syncAction()
	{
		// cek koneksi
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = '<b>Mohon Periksa Kembali Koneksi Anda !!!</b>';

			return json_encode($data);
		}else{
			$tmuk = new TMUK();
			$lists_tmuk = $tmuk::getALL();
			
			if(isset($lists_tmuk)){
				foreach($lists_tmuk as $list_tmuk){
					$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
					$tmuk->tmuk = $list_tmuk['tmuk'];
					$tmuk->supplier_id = $list_tmuk['supplier_id'];
				}
			}

			// get saldo
			$saldo = (new TMUKController())->getSaldo();
			// $saldo = 100000000; // ke hapus

			// init PR
			$pr = new PR();
			$pr->request_no = $_REQUEST['Request_No'];
			$lists_h = $pr::getFirst($pr);
			
			$pr_detail = new PRDetail();
			$pr_detail->request_no = $_REQUEST['Request_No'];
			$condition = " WHERE \"request_no\" ='".$pr_detail->request_no."' " ;
			$lists_d = $pr_detail::getFreeSQL($condition);		
			
			if(isset($lists_h)){
				foreach($lists_h as $list_h){
					$pr->request_no = $list_h['request_no'];
					$pr->point_used = $list_h['point_used'];
					$pr->reference = $list_h['reference'];
					$pr->delivery_by = $list_h['delivery_by'];
					$pr->created_by = $list_h['created_by'];
					$pr->create_date = $list_h['create_date'];
					$pr->delivery_date = $list_h['delivery_date'];
					$pr->comments = $list_h['comments'];
					$pr->total = $list_h['total'];
					$pr->supplier_id = $list_h['supplier_id'];
					$pr->tmuk = $list_h['tmuk'];
				}
			}
			
			$curl = new CurlClass();
			$result = $curl->get($this->di['api']['link']."/api/tmuk/get-mengendap");
			$saldo_mengendap = json_decode($result, true);

			$cek = new CurlClass();
			$data = $cek->post($this->di['api']['link']."/api/tmuk/get-scn", [
				'tmuk' => $tmuk->tmuk
			]);
			$scn = json_decode($data, true);

			$cek_locked = new CurlClass();
			$data_locked = $cek_locked->post($this->di['api']['link']."/api/tmuk/get-locked", [
				'tmuk' => $tmuk->tmuk
			]);
			$locked = json_decode($data_locked, true);

			$available = ($saldo + $scn['escrow']) - $locked['locked'] - $saldo_mengendap['message'];

			$cek = $available - $pr->total;
			// cek
			if($cek >= 0){
				$data = null;
				$data['sup'] = $pr->supplier_id;
				$data['tmuk'] = $pr->tmuk;
				$data['req'] = $pr->delivery_date;
				$data['pr'] = $pr->request_no;
				$data['total'] = $pr->total;
				$data['date'] = $pr->create_date;
				$data['comments'] = $pr->comments;
				
				// $count = 0;
				$dummy = array();
				$prods = [];
				if(isset($lists_d)){
					foreach($lists_d as $list_d){
						$xx['id']    			= $list_d['item_code'];
						$xx['qty']   			= $list_d['qty_order'];
						$xx['price'] 			= $list_d['unit_order_price'];
						$xx['unit_order']   	= $list_d['unit_order'];
						$xx['qty_sell']     	= $list_d['qty_sell'];
						$xx['unit_sell']    	= $list_d['unit_sell'];
						$xx['qty_pr']    		= $list_d['qty_pr'];
						$xx['unit_pr']    		= $list_d['unit_pr'];
						$xx['order_price']    	= $list_d['order_price'];
						$xx['con_pr']    		= $list_d['con_pr'];

						$prods[] = $xx;
					}
				}

				$data['lists'] = $prods;
			// init post
			$curl = new CurlClass();
			$result = $curl->post($this->di['api']['link']."/api/pr/send", $data);

			$obj = json_decode($result, true);
			
				if($obj['status'] == 'success'){
					$pr_update = new PR();
					$pr_update->request_no = $_REQUEST['Request_No'];
					$pr_update->requisition_no = $obj['message'];
					$pr_update->flag_sync = 't';
					$success = $pr_update::goUpdate($pr_update);
					
					$hasil['type'] = 'S';
					$hasil['message'] = 'Berhasil Sync <b>'.$pr_update->request_no.'</b>';
					$notif = new Notifikasi();
					$notif->data = $hasil['message'];
					$notif->read_at = null;
					$notif->created_at = date('Y-m-d H:i:s');
					$notif->created_by = $this->session->get("user")['user_name'];

					$sukses = $notif::goInsert($notif);
					return json_encode($hasil);
				}else{
					$hasil['type'] = 'E';
					$hasil['message'] = $obj['message'];
					return json_encode($hasil);				
				}
			}else{
				$hasil['type'] = 'E';
				$hasil['message'] = 'Jumlah saldo anda yang tersedia : <b>Rp '.number_format($available,0,",",".").'</b><br/> PR tidak bisa di Sync.';
				return json_encode($hasil);
			}	
		}

	}
	
	public function printAction()
	{
		$this->AuthorityAction();
		
		$pr = new PR();
		$pr->request_no = $_REQUEST['Request_No'];
		$lists_pr = $pr::getFirst($pr);
		
		if(isset($lists_pr)){
			foreach($lists_pr as $list_pr){
				$pr->request_no = $list_pr['request_no'];
				$pr->point_used = $list_pr['point_used'];
				$pr->reference = $list_pr['reference'];
				$pr->delivery_by = $list_pr['delivery_by'];
				$pr->created_by = $list_pr['created_by'];
				$pr->delivery_date = $list_pr['delivery_date'];
				$pr->create_date = $list_pr['create_date'];
				$pr->comments = $list_pr['comments'];
				$pr->total = $list_pr['total'];
				$pr->supplier_id = $list_pr['supplier_id'];
				$pr->tmuk = $list_pr['tmuk'];
				$pr->status = $list_pr['status'];
			}
		}
		
		$this->view->pr = $pr;
		
		$pr_detail = new PRDetail();
		$pr_detail->request_no = $_REQUEST['Request_No'];
		$condition = " WHERE \"request_no\" = '".$pr_detail->request_no."' ";
		$lists_pr_detail = $pr_detail::getFreeSQL($condition);
		
		$this->view->pr_details = $lists_pr_detail;
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
				$tmuk->saldo = $list_tmuk['saldo'];
				$tmuk->telp = $list_tmuk['telp'];
				$tmuk->name = $list_tmuk['name'];
				$tmuk->owner = $list_tmuk['owner'];
				$tmuk->address1 = $list_tmuk['address1'];			
				$tmuk->lsi = $list_tmuk['nama_lsi'];				
			}
		}
		
		$this->view->tmuk = $tmuk;
	}
}
