<?php

use Phalcon\Mvc\Controller;

require_once ('../app/library/src/Autoloader.php');

PhpXmlRpc\Autoloader::register();
use PhpXmlRpc\Value;
use PhpXmlRpc\Request;
use PhpXmlRpc\Client;

class JenisKustomerController extends Controller
{
	public function getJenisKustomer()
	{
		$data = new JenisKustomer();

		// dd($data);
		$lists = $data->getAll();
		// dd($lists);
		$jk = 0;
		
		if(count($lists) > 0){
			foreach($lists as $list){
				$id = $list['id'];
				$kode = $list['kode'];

				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/jenis-kustomer/get-kustomer", [
					'id' => $id,
					'kode' => $kode,
				]);
				// dd($result);
				// transform json to object array php
				$obj = json_decode($result, true);
				if ($obj['status'] == 'success') {
					$jk = $obj['message'];

					// loop
					foreach ($jk as $val) {
						$rcd    = new JenisKustomer();
						$result = $rcd::getFreeSQL(" WHERE \"kode\" = '".$val['kode']."' ");

						// dd($rcd);
						// dd($result);
						if (count($result) > 0) { // update
							$update = new JenisKustomer();
							// $update->id = $id;
							$update->jenis = $val['jenis'];
							$update->kode = $val['kode'];
							$update->jenis_kustomer_id = $val['id'];
							$success = $update::goUpdate($update);
							
						} else { //insert
							$insert = new JenisKustomer();
							// $insert->id = $id;
							$insert->jenis = $val['jenis'];
							$insert->kode = $val['kode'];
							$insert->jenis_kustomer_id = $val['id'];
							$success = $insert::goInsert($insert);
						}
					}
				}
			}						
		}
		return $jk;
	}



}