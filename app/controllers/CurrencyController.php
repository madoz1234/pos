<?php

use Phalcon\Mvc\Controller;

class CurrencyController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'Currency' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$currency = new Currency();
		$this->view->lists = $currency::getAll();
	}
	
	public function ajaxCurrencyAction()
	{
		$currency = new Currency();
		$condition = " WHERE \"currency\" = '".$_GET['id']."' LIMIT 1 ";
		$data = $currency::getFreeSQL($condition);

		return json_encode($data);
	}
	
	public function addAction()
	{
		$this->AuthorityAction();
	}
	
	public function editAction()
	{
		$this->AuthorityAction();
		
		$currency = new Currency();
		$currency->currency = $_REQUEST['Currency'];
		$lists = $currency::getFirst($currency);
		
		$this->view->data = new Currency();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->currency = $list['currency'];
				$this->view->data->curr_abrev = $list['curr_abrev'];
				$this->view->data->curr_symbol = $list['curr_symbol'];
				$this->view->data->country = $list['country'];
				$this->view->data->hundreds_name = $list['hundreds_name'];
			}
		}
	}
	
	public function viewAction()
	{
		$this->AuthorityAction();
		
		$currency = new Currency();
		$currency->currency = $_REQUEST['Currency'];
		$lists = $currency::getFirst($currency);
		
		$this->view->data = new Currency();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->currency = $list['currency'];
				$this->view->data->curr_abrev = $list['curr_abrev'];
				$this->view->data->curr_symbol = $list['curr_symbol'];
				$this->view->data->country = $list['country'];
				$this->view->data->hundreds_name = $list['hundreds_name'];
			}
		}
	}
	
	public function deleteAction()
	{
		$this->AuthorityAction();
		
		$currency = new Currency();
		$currency->currency = $_REQUEST["Currency"];
		$success = $currency::goDelete($currency);
		
		$this->response->redirect(uri('Currency'));
		
	}
	
	public function insertAction()
	{
		$currency = new Currency();
		$currency->currency = $this->request->getPost("Currency");
		$currency->curr_abrev = $this->request->getPost("Curr_Abrev");
		$currency->curr_symbol = $this->request->getPost("Curr_Symbol");
		$currency->country = $this->request->getPost("Country");
		$currency->hundreds_name = $this->request->getPost("Hundreds_Name");
		$success = $currency::goInsert($currency);
		
		if($success){			
			$this->response->redirect(uri('Currency'));
		}else{						
			$this->response->redirect(uri('Currency/Add'));
		}
	}
	
	public function updateAction()
	{
		$currency = new Currency();
		$currency->currency = $this->request->getPost("Currency");
		$currency->curr_abrev = $this->request->getPost("Curr_Abrev");
		$currency->curr_symbol = $this->request->getPost("Curr_Symbol");
		$currency->country = $this->request->getPost("Country");
		$currency->hundreds_name = $this->request->getPost("Hundreds_Name");
		$success = $currency::goUpdate($currency);
		
		if($success){			
			$this->response->redirect(uri('Currency'));
		}else{						
			$this->response->redirect(uri('Currency/Edit?Currency='.$currency->currency));
		}
	}
}
