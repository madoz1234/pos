<?php

use Phalcon\Mvc\Controller;

class KassaController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'Kassa' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$kassa = new Kassa();
		$this->view->lists = $kassa::getAll();
	}
	
	public function ajaxKassaAction()
	{
		$kassa = new Kassa();
		$condition = " WHERE \"kassa_id\" = '".$_GET['id']."' LIMIT 1 ";
		$data = $kassa::getFreeSQL($condition);

		return json_encode($data);
	}
	
	public function addAction()
	{
		$this->AuthorityAction();
	}
	
	public function editAction()
	{
		$this->AuthorityAction();
		
		$kassa = new Kassa();
		$kassa->kassa_id = $_REQUEST['Kassa_ID'];
		$lists = $kassa::getFirst($kassa);
		
		$this->view->data = new Shift();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->kassa_id = $list['kassa_id'];
				$this->view->data->description = $list['description'];				
				$this->view->data->inactive = $list['inactive'];
			}
		}
	}
	
	public function viewAction()
	{
		$kassa = new Kassa();
		$kassa->kassa_id = $_REQUEST['Kassa_ID'];
		$lists = $kassa::getFirst($kassa);
		
		$this->view->data = new Shift();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->kassa_id = $list['kassa_id'];
				$this->view->data->description = $list['description'];				
				$this->view->data->inactive = $list['inactive'];
			}
		}
	}
	
	public function deleteAction()
	{
		$this->AuthorityAction();
		
		$kassa = new Kassa();
		$kassa->kassa_id = $_REQUEST["Kassa_ID"];
		$cek = new StartEndShift();
		$condition = " WHERE \"kassa_id\" = '".$kassa->kassa_id."' ";
		$condition .= " AND \"flag_start\" = true AND \"flag_end\" = false ";
		$lists_seShift_pos = $cek::getFreeSQL($condition);

		if(count($lists_seShift_pos) > 0){
			$data['type'] = 'E';
			$data['message'] = '<i><b>POS</b></i> sedang digunakan.';
			return json_encode($data);	
		}else{
			$success = $kassa::goDelete($kassa);
		}
		if($success){
			$data['type'] = 'S';
			$data['message'] = '<i><b>Berhasil Hapus Pos</b></i>';			
			return json_encode($data);
		}else{
			$data['type'] = 'E';
			$data['message'] = '<i><b>Gagal Hapus Pos</b></i>';
			return json_encode($data);			
		}
		$this->response->redirect('Kassa');
	}
	
	public function insertAction()
	{
		$kassa = new Kassa();		
		$kassa->description = $this->request->getPost("Description");

		$condition = " WHERE \"description\" = '".$this->request->getPost("Description")."' ";
		$cari = $kassa::getFreeSQL($condition);
		if(count($cari) == 0){
			$success = $kassa::goInsert($kassa);
		}
		if($success){
			$data['type'] = 'S';
			$data['message'] = '<i><b>Berhasil Tambah Pos</b></i>';			
			return json_encode($data);
		}else{
			$data['type'] = 'E';
			$data['message'] = '<i><b>Nama Pos Tidak Boleh Sama</b></i>';
			return json_encode($data);			
		}
	}
	
	public function updateAction()
	{
		$kassa = new Kassa();
		$kassa->kassa_id = $this->request->getPost("Kassa_ID");		
		$kassa->description = $this->request->getPost("Description");
		
		$cond = " WHERE \"kassa_id\" = '".$this->request->getPost("Kassa_ID")."' ";
		$cond .= " AND \"description\" = '".$this->request->getPost("Description")."' ";
		$cari = $kassa::getFreeSQL($cond);

		$cek = new StartEndShift();
		$condition = " WHERE \"kassa_id\" = '".$kassa->kassa_id."' ";
		$condition .= " AND \"flag_start\" = true AND \"flag_end\" = false ";
		$lists_seShift_pos = $cek::getFreeSQL($condition);
		if(isset($lists_seShift_pos)){
			$data['type'] = 'E';
			$data['message'] = '<i><b> POS </b></i> sedang digunakan.';
			return json_encode($data);	
		}else{
			if(count($cari) == 0){
				$condition = " WHERE \"kassa_id\" != '".$this->request->getPost("Kassa_ID")."' ";
				$condition .= " AND \"description\" = '".$this->request->getPost("Description")."' ";
				$cari2 = $kassa::getFreeSQL($condition);
				if($cari2 == 0){
					$success = $kassa::goUpdate($kassa);	
				}
			}else{
				$success = $kassa::goUpdate($kassa);
			}

			if($success){
				$data['type'] = 'S';
				$data['message'] = '<i><b>Berhasil Ubah Pos</b></i>';			
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = '<i><b>Nama Pos Tidak Boleh Sama</b></i>';
				return json_encode($data);			
			}
		}
	}
}
