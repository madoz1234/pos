<?php

use Phalcon\Mvc\Controller;
include __DIR__ . "/../library/print_format.php";

class StartDayController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'StartDay' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$last_sync = new LastSync();
		$this->view->lists = $last_sync::getAll();
	}
	
	public function cetak_SODAction(){	
		$device = "COM2";
		exec("mode $device BAUD=38400 PARITY=n DATA=8 STOP=1 xon=off octs=off rts=on to=on");
		$comport = fopen($device, "w");
		
		$cond = " WHERE tanggal = '".date('Y-m-d')."' AND flag_end = false ";
		$listsTMUK = TMUK::getAll();
		$listsSED = StartEndDay::getFreeSQL($cond);
		$listsLS = LastSync::getAll();
		
		if(isset($listsTMUK)){ 
			foreach($listsTMUK as $listtm){
				$tmuk_name = $listtm['name'];
				$tmuk_code = $listtm['tmuk'];
				$tmuk_telp = $listtm['telp']; 
				$owner = $listtm['owner'];
				$length = $listtm['length_struk'];
			}
		}
		
		$tanggal = '';
		if(isset($listsSED)){
			foreach($listsSED as $listSED){
				$tanggal = $listSED['tanggal'];			
			}
		}
		
		$length_3 = round(0.062 * $length);
		$length_4 = round(0.083 * $length);		
		$length_5 = round(0.104 * $length);
		$length_8 = round(0.166 * $length);
		$length_9 = round(0.187 * $length);		
		$length_10 = round(0.208 * $length);
		$length_12 = round(0.250 * $length);		
		$length_13 = round(0.270 * $length);		
		$length_14 = round(0.291 * $length);		
		$length_16 = round(0.333 * $length);		
		$length_18 = round(0.375 * $length);		
		$length_19 = round(0.395 * $length);
		$length_20 = round(0.416 * $length);
		$length_24 = round(0.500 * $length);
		$length_25 = round(0.520 * $length);
		$length_30 = round(0.625 * $length);
		$length_32 = round(0.666 * $length);
		$length_48 = round(1 * $length);
		
		$str = print_center('START OF DAY'); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
				
		$str = print_left('Store Code  : '.$tmuk_code, $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_left('Store Name  : '.$tmuk_name, $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_left('Date        : '.date('d-m-Y', strtotime($tanggal)), $length_48); $str = substr($str, 0, $length); fputs($comport, $str);

		$str = print_left('Last Sync', $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		
		if(isset($listsLS)){
			foreach($listsLS as $list_ls){
				$str = print_right($list_ls['tipe'], $length_20).print_right($list_ls['sync_date'], $length_14).print_right($list_ls['sync_time'], $length_14); $str = substr($str, 0, $length); fputs($comport, $str);
			}
		}
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '='; }		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_center('Mengetahui Store Leader'); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_center('(     '.$owner.'     )'); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '-'; }		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_center('Print Date : '.date('d-m-Y H:i:s')); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = "\x00\x1Bi\x00"; fputs($comport, $str);
		
		fclose($comport);
	}
}
