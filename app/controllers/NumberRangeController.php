<?php

use Phalcon\Mvc\Controller;

class NumberRangeController extends Controller
{	
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'NumberRange' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$number_range = new NumberRange();
		$this->view->lists = $number_range::getAll();
	}
	
	public function ajaxDataAction()
	{
		$number_range = new NumberRange();
		$condition = " WHERE \"number_id\" = '".$_GET['id']."' LIMIT 1 ";
		$data = $number_range::getFreeSQL($condition);

		return json_encode($data);
	}
	
	public function addAction()
	{
		$this->AuthorityAction();
	}
	
	public function editAction()
	{
		$this->AuthorityAction();
		
		$number_range = new NumberRange();
		$number_range->number_id = $_REQUEST['Number_ID'];
		$condition = " WHERE \"number_id\" = '".$number_range->number_id."' ";
		$lists = $number_range::getFreeSQL($condition);
		
		$this->view->data = new NumberRange();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->number_id = $list['number_id'];
				$this->view->data->trans_type = $list['trans_type'];				
				$this->view->data->period = $list['period'];
				$this->view->data->prefix = $list['prefix'];
				$this->view->data->from_ = $list['from_'];
				$this->view->data->to_ = $list['to_'];
				$this->view->data->current_no = $list['current_no'];				
			}
		}
	}
	
	public function viewAction()
	{
		$number_range = new NumberRange();
		$number_range->number_id = $_REQUEST['Number_ID'];
		$condition = " WHERE \"number_id\" = '".$number_range->number_id."' ";
		$lists = $number_range::getFreeSQL($condition);
		
		$this->view->data = new NumberRange();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->number_id = $list['number_id'];
				$this->view->data->trans_type = $list['trans_type'];				
				$this->view->data->period = $list['period'];
				$this->view->data->prefix = $list['prefix'];
				$this->view->data->from_ = $list['from_'];
				$this->view->data->to_ = $list['to_'];
				$this->view->data->current_no = $list['current_no'];				
			}
		}
	}
	
	public function deleteAction()
	{
		$this->AuthorityAction();
		
		$number_range = new NumberRange();
		$number_range->number_id = $_REQUEST["Number_ID"];
		$success = $number_range::goDelete($number_range);
		
		$this->response->redirect(uri('NumberRange'));
	}
	
	public function insertAction()
	{
		$number_range = new NumberRange();		
		$number_range->trans_type = $this->request->getPost("Trans_Type");
		$number_range->period = $this->request->getPost("Period");
		$number_range->prefix = $this->request->getPost("Prefix");
		$number_range->from_ = $this->request->getPost("From_");
		$number_range->to_ = $this->request->getPost("To_");
		$number_range->current_no = $this->request->getPost("Current_No");
		$success = $number_range::goInsert($number_range);
		
		if($success){			
			$this->response->redirect(uri('NumberRange'));
		}else{						
			$this->response->redirect(uri('NumberRange/Add'));
		}
	}
	
	public function updateAction()
	{
		$number_range = new NumberRange();
		$number_range->number_id = $this->request->getPost("Number_ID");		
		$number_range->trans_type = $this->request->getPost("Trans_Type");
		$number_range->period = $this->request->getPost("Period");
		$number_range->prefix = $this->request->getPost("Prefix");
		$number_range->from_ = $this->request->getPost("From_");
		$number_range->to_ = $this->request->getPost("To_");
		$number_range->current_no = $this->request->getPost("Current_No");
		$success = $number_range::goUpdate($number_range);
		
		if($success){			
			$this->response->redirect(uri('NumberRange'));
		}else{						
			$this->response->redirect(uri('NumberRange/Edit?Shift_ID='.$number_range->number_id));
		}
	}
}
