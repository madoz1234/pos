<?php
set_time_limit(0);
use Phalcon\Mvc\Controller;

class TopUpController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'TopUp' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{		
		$this->AuthorityAction();
		
		$delivery_by = null;
		$delivery_by['Ambil Sendiri'] = 'Ambil Sendiri';
		$delivery_by['LSI'] = 'LSI';		
		$this->view->delivery_by = $delivery_by;
		
		$status = null;
		$status['0'] = 'Tidak Disetujui';
		$status['1'] = 'Disetujui';
		$this->view->status = $status;
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		foreach($lists_tmuk as $list_tmuk){
			$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
			$tmuk->tmuk = $list_tmuk['tmuk'];
			$tmuk->supplier_id = $list_tmuk['supplier_id'];
			$tmuk->saldo = $list_tmuk['saldo'];
		}
		
		$saldo = (new TMUKController())->getSaldoEscrow();		
		$this->view->saldo = $saldo;

		$rekening = (new TMUKController())->getRekening();		
		$this->view->rekening = $rekening;
		//$this->view->saldo = $tmuk->saldo;
		
		$role = new Role();
		$role->role_id = $this->session->get("user")['role_id'];
		$lists_role = $role::getFirst($role);
		
		foreach($lists_role as $list_role){
			$role->role_id = $list_role['role_id'];
			$role->role_name = $list_role['role_name'];
			$role->description = $list_role['description'];
			$role->is_active = $list_role['is_active'];
			$role->pr_approve = $list_role['pr_approve'];
		}
		
		$this->view->role = $role;
		// default date + 2
		$tanggal = date('Y-m-d');
		$date = DateTime::createFromFormat('Y-m-d',$tanggal);
		$date->modify('+2 day');
		$tanggal = $date->format('Y-m-d');
		
		$this->view->default_date = $tanggal;
	}

	public function getSaldoAction()
	{
		$data = new TMUK();
		$lists = $data::getAll();
		$saldo = 0;
		
		if(count($lists) > 0){
			foreach($lists as $list){
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/tmuk/get-saldo", [
					'tmuk' => $tmuk
				]);

				// transform json to object array php
				$obj = json_decode($result, true);

				if ($obj['status'] == true) {
					$saldo = $data['message'];
				}
			}						
		}
		
		return $saldo;
	}


	public function insertAction(){
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getAll();
		$kode = "";
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$kode = $list_tmuk['tmuk'];
			}
		}		
		$tanggal = $this->request->getPost("tanggal");
		$jenis = $this->request->getPost("jenis");
		$jumlah = $this->request->getPost("jumlah");
		// $lampiran = $this->request->getPost("lampiran");
		// $lampiran = new \CURLFile( realpath($_FILES['lampiran']['tmp_name']), $_FILES['lampiran']['type'], $_FILES['lampiran']['name'] );
		if ($this->request->hasFiles('lampiran') == true) {
            foreach ($this->request->getUploadedFiles('lampiran') as $file) {
            	$lampiran = $file;
                $lampiranPath = $file->getTempName();
                $lampiranName = $file->getName();
            }
       }else{
			$lampiran=null;
		}
		$tmuk_kode = $kode;
		$status = 0;
		// var_dump($lampiran); die;

		
		$curl = new CurlClass();
		$lamp = $curl->file($lampiranPath, $lampiranName);
		$result = $curl->exec($this->di['api']['link']."/api/transaksi/topup/send", [
			'tanggal' => $tanggal,
			'jenis' => $jenis,
			'jumlah' => $jumlah,
			'tmuk' => $tmuk_kode,
			'status' => $status,
		], [
			'lampiran' => [ $lampiran ]
		]);

		$success = json_decode($result, true);
		
		return $result;

		if($success['status'] == 'success'){			
			$resul['type'] = 'S';
			$resul['message'] = 'Success Top Up!';
			return json_encode($resul);
		}else{						
			$resul['type'] = 'E';
			$resul['message'] = 'Failed Top Up';
			return json_encode($resul);
		}
	}
	
}
