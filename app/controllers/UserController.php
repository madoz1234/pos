<?php

use Phalcon\Mvc\Controller;

class UserController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'User' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$user = new User();
		$this->view->lists = $user::getAll();
	}
	
	public function addAction()
	{
		$this->AuthorityAction();
		
		$role = new Role();
		$lists_role = $role::getAll();
		
		$data_role = null;		
		foreach($lists_role as $list_role){
			$data_role[$list_role['role_id']] = $list_role['role_name'];
		}
		
		$this->view->role = $data_role;
	}
	
	public function editAction()
	{
		$this->AuthorityAction();
		
		$role = new Role();
		$lists_role = $role::getAll();
		
		$data_role = null;		
		foreach($lists_role as $list_role){
			$data_role[$list_role['role_id']] = $list_role['role_name'];
		}
		
		$this->view->role = $data_role;
		
		$user = new User();
		$user->user_id = $_REQUEST['User_ID'];
		$condition = " WHERE \"user_id\" ='".$user->user_id."' LIMIT 1 ";
		$lists = $user::getFreeSQL($condition);
		
		$this->view->data = new User();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->user_id = $list['user_id'];
				$this->view->data->user_name = $list['user_name'];
				$this->view->data->role_id = $list['role_id'];
				$this->view->data->real_name = $list['real_name'];
				$this->view->data->last_login = $list['last_login'];
				$this->view->data->count_login = $list['count_login'];
				$this->view->data->date_created = $list['date_created'];
				$this->view->data->user_password = $list['user_password'];
				$this->view->data->is_active = $list['is_active'];
				$this->view->data->images = $list['images'];
			}
		}
	}
	
	public function viewAction()
	{
		$this->AuthorityAction();
		
		$user = new User();
		$user->user_id = $_REQUEST['User_ID'];
		$condition = " WHERE \"user_id\" ='".$user->user_id."' LIMIT 1 ";
		$lists = $user::getFreeSQL($condition);
		
		$this->view->data = new User();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->user_id = $list['user_id'];
				$this->view->data->user_name = $list['user_name'];
				$this->view->data->role_id = $list['role_id'];
				$this->view->data->real_name = $list['real_name'];
				$this->view->data->last_login = $list['last_login'];
				$this->view->data->count_login = $list['count_login'];
				$this->view->data->date_created = $list['date_created'];
				$this->view->data->user_password = $list['user_password'];
				$this->view->data->is_active = $list['is_active'];
				$this->view->data->images = $list['images'];
			}
		}
	}
	
	public function deleteAction()
	{
		$this->AuthorityAction();
		$role_id = $this->session->get('user')['role_id'];
		$user = new User();
		$user->user_id = $_REQUEST["User_ID"];
		if($role_id == 1){
			$success = $user::goDelete($user);
			if($success){
				$data['type'] = 'S';
				$data['message'] = '<i><b>Berhasil Hapus User</b></i>';			
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = '<i><b>Gagal Hapus User</b></i>';
				return json_encode($data);			
			}
		}else{
			$data['type'] = 'E';
			$data['message'] = '<i><b>Gagal !! Anda Tidak Mempunyai Akses</b></i>';
			return json_encode($data);
		}		
	}
	
	public function insertAction()
	{
		//echo (new \Phalcon\Debug\Dump())->variable($this->request->getUploadedFiles()[0]->getName(), "file"); exit;
		$user = new User();
		$user->user_name = $this->request->getPost("User_Name");
		$user->role_id = $this->request->getPost("Role_ID");
		$user->real_name = $this->request->getPost("Real_Name");
		$user->user_password = $this->request->getPost("User_Password");

		if ($this->request->hasFiles() == true) {
            // Print the real file names and their sizes
        	$file = $this->request->getUploadedFiles()[0];
            $file->moveTo('upload/' . $file->getName());
		// echo (new \Phalcon\Debug\Dump())->variable($file, "file"); exit;
       	}
        $user->images = 'upload/' . $file->getName();
        $condition = " WHERE \"user_name\" = '".$this->request->getPost("User_Name")."'  AND \"role_id\" = '".$this->request->getPost("Role_ID")."'";
		$cari = $user::getFreeSQL($condition);
		$role_id = $this->session->get('user')['role_id'];
		if($role_id == 1){
			if(count($cari) == 0){
				$success = $user::goInsert($user);
			}
			if($success){
				$data['type'] = 'S';
				$data['message'] = '<i><b>Berhasil Tambah User</b></i>';			
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = '<i><b>Username Tidak Boleh Sama</b></i>';
				return json_encode($data);			
			}
		}else{
			$data['type'] = 'E';
			$data['message'] = '<i><b>Gagal !! Anda Tidak Mempunyai Akses</b></i>';
			return json_encode($data);
		}
	}
	
	public function updateAction()
	{
		$user = new User();
		$user->user_id = $this->request->getPost("User_ID");
		$user->user_name = $this->request->getPost("User_Name");
		$user->role_id = $this->request->getPost("Role_ID");
		$user->real_name = $this->request->getPost("Real_Name");
		if ($this->request->hasFiles() == true) {
            // Print the real file names and their sizes
        	$file = $this->request->getUploadedFiles()[0];
            $file->moveTo('upload/' . $file->getName());
		// echo (new \Phalcon\Debug\Dump())->variable($file, "file"); exit;
       	}
        $user->images = 'upload/' . $file->getName();

		$user->user_password = $this->request->getPost("User_Password");
		$user->user_password = base64_encode($user->user_password);

		$cond = " WHERE \"user_id\" = '".$this->request->getPost("User_ID")."' ";
		$cond .= " AND \"role_id\" = '".$this->request->getPost("Role_ID")."' ";
		$cond .= " AND \"user_name\" = '".$this->request->getPost("User_Name")."' ";
		$cari = $user::getFreeSQL($cond);

		$user_id = $this->session->get('user')['user_id'];
		if($user->user_id == $user_id OR $user_id == 1){
			if(count($cari) == 0){
				$condition = " WHERE \"user_id\" != '".$user_id."' ";
				$condition .= " AND \"role_id\" = '".$this->request->getPost("Role_ID")."' ";
				$condition .= " AND \"user_name\" = '".$this->request->getPost("User_Name")."' ";
				$cari2 = $user::getFreeSQL($condition);
				if(count($cari2) == 0){
					$success = $user::goUpdate($user);	
				}
			}else{
				$success = $user::goUpdate($user);
			}
			if($success){
				$data['type'] = 'S';
				$data['message'] = '<i><b>Berhasil Ubah User</b></i>';			
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = '<i><b>Username Tidak Boleh Sama</b></i>';
				return json_encode($data);			
			}
		}else{
			$data['type'] = 'E';
			$data['message'] = '<i><b>Gagal !! Anda Tidak Mempunyai Akses</b></i>';
			return json_encode($data);
		}
	}
	
	public function ajaxUserAction()
	{
		$user = new User();
		$condition = " WHERE \"user_id\" = '".$_GET['id']."' LIMIT 1 ";
		$data = $user::getFreeSQL($condition);

		return json_encode($data);
	}
	
	public function voidUserAction()
	{
		$user = new User();
		$condition = " WHERE \"user_name\" = '".$_GET['user']."' AND \"user_password\" = '".base64_encode($_GET['pass'])."' ";
		$data = $user::getJoin_role($condition);
		return json_encode($data);
	}
	
}
