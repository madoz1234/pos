<?php

use Phalcon\Mvc\Controller;
use \Phalcon\Db\Adapter\Pdo\Postgresql;

class LogoutController extends Controller
{	
	
	public function indexAction(){
		$this->session->remove("is_login");
		$this->response->redirect(uri('login'));
	}

}
