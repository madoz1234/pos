<?php
set_time_limit(0);
use Phalcon\Mvc\Controller;
use GuzzleHttp\Client;

class TopUpController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'TopUp' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{		
		// $this->AuthorityAction();
		
		$delivery_by = null;
		$delivery_by['Ambil Sendiri'] = 'Ambil Sendiri';
		$delivery_by['LSI'] = 'LSI';		
		$this->view->delivery_by = $delivery_by;
		
		$status = null;
		$status['0'] = 'Tidak Disetujui';
		$status['1'] = 'Disetujui';
		$this->view->status = $status;
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		foreach($lists_tmuk as $list_tmuk){
			$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
			$tmuk->tmuk = $list_tmuk['tmuk'];
			$tmuk->supplier_id = $list_tmuk['supplier_id'];
			$tmuk->saldo = $list_tmuk['saldo'];
		}
		
		$saldo = (new TMUKController())->getSaldoEscrow();		
		$this->view->saldo = $saldo;

		// $rekening = (new TMUKController())->getRekening();
		$rekening = '';
		$rekening_nama = '';
		$rekening_pemilik = '';
		$data = new TMUK();
		$listst = $data->getAll();
		foreach ($listst as $value) {
			$rekening = $value['escrow_account'];
			$rekening_nama = $value['nama_bank'];
			$rekening_pemilik = $value['nama_owner'];
		}		
		$this->view->rekening = $rekening;
		$this->view->rekening_nama = $rekening_nama;
		$this->view->rekening_pemilik = $rekening_pemilik;
		//$this->view->saldo = $tmuk->saldo;
		
		$role = new Role();
		$role->role_id = $this->session->get("user")['role_id'];
		$lists_role = $role::getFirst($role);
		
		foreach($lists_role as $list_role){
			$role->role_id = $list_role['role_id'];
			$role->role_name = $list_role['role_name'];
			$role->description = $list_role['description'];
			$role->is_active = $list_role['is_active'];
			$role->pr_approve = $list_role['pr_approve'];
		}
		
		$this->view->role = $role;
		// default date + 2
		$tanggal = date('Y-m-d');
		$date = DateTime::createFromFormat('Y-m-d',$tanggal);
		$date->modify('+2 day');
		$tanggal = $date->format('Y-m-d');
		
		$this->view->default_date = $tanggal;
	}

	public function getSaldoAction()
	{
		$data = new TMUK();
		$lists = $data::getAll();
		$saldo = 0;
		
		if(count($lists) > 0){
			foreach($lists as $list){
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/tmuk/get-saldo", [
					'tmuk' => $tmuk
				]);

				// transform json to object array php
				$obj = json_decode($result, true);

				if ($obj['status'] == true) {
					$saldo = $data['message'];
				}
			}						
		}
		
		return $saldo;
	}

	

	public function insertAction(){
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getAll();
		$kode = "";
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$kode = $list_tmuk['tmuk'];
			}
		}

		$tanggal = $this->request->getPost("tanggal") ?: null ;
		$totaljual = $this->request->getPost("totaljual");
		// var_dump($this->request->getPost("totaljual"));
		// die;

		$periode = $this->request->getPost("periode") ?: null ;
		$jenis = $this->request->getPost("jenis");
		$jumlah = $this->request->getPost("jumlah") ?: null;
		$tmuk_kode = $kode;
		
		$week1 = $this->request->getPost("week1") ?: null;
		$jumlah1 = $this->request->getPost("jumlah1") ?: null;
		$week2 = $this->request->getPost("week2") ?: null;
		$jumlah2 = $this->request->getPost("jumlah2") ?: null;
		$week3 = $this->request->getPost("week3") ?: null;
		$jumlah3 = $this->request->getPost("jumlah3") ?: null;
		$week4 = $this->request->getPost("week4") ?: null;
		$jumlah4 = $this->request->getPost("jumlah4") ?: null;
		$week5 = $this->request->getPost("week5") ?: null;
		$jumlah5 = $this->request->getPost("jumlah5") ?: null;
		
		$member = $this->request->getPost("member") ?: null;
		
		$status = 0;

		// var_dump($periode);
		// die;

		if(is_null($tanggal) or is_null($jumlah)){
			$resul['type'] = 'E';
			$resul['message'] = 'Failed Top Up';
			return json_encode($resul);
		}

		$data = [
			[
				'name'     => 'tanggal',
				'contents' => $tanggal,
			],
			[
				'name'     => 'totaljual',
				'contents' => $totaljual,
			],
			[
				'name'     => 'periode',
				'contents' => $periode,
			],
			[
				'name'     => 'tmuk',
				'contents' => $tmuk_kode,
			],
			[
				'name'     => 'jenis',
				'contents' => $jenis,
			],
			[
				'name'     => 'jumlah',
				'contents' => $jumlah,
			],
			[
				'name'     => 'status',
				'contents' => $status,
			],
			[
				'name'     => 'lampiran',
				'contents' => null,
			]
		];
		if ($this->request->hasFiles('lampiran') == true) {
			foreach ($this->request->getUploadedFiles('lampiran') as $file) {
				$lampiran = $file;
				$filepath = curl_file_create( realpath($lampiran->getTempName()), $lampiran->getRealType(), $lampiran->getName() );
				$data = [
					[
						'name'     => 'tanggal',
						'contents' => $tanggal,
					],
					[
						'name'     => 'totaljual',
						'contents' => $totaljual,
					],
					[
						'name'     => 'periode',
						'contents' => $periode,
					],
					[
						'name'     => 'tmuk',
						'contents' => $tmuk_kode,
					],
					[
						'name'     => 'jenis',
						'contents' => $jenis,
					],
					[
						'name'     => 'jumlah',
						'contents' => $jumlah,
					],
					[
						'name'     => 'status',
						'contents' => $status,
					],
					[
						'name'     => 'lampiran',
						'contents' => fopen($lampiran->getTempName(), 'r'),
						'filename' => $lampiran->getName()
					]
				];
			}
		}
		if($jenis==1){
			$week = 
			[
				[    'name'     => 'week1',
				'contents' => $week1,
			],
			[
				'name'     => 'jumlah1',
				'contents' => $jumlah1,
			],
			[
				'name'     => 'week2',
				'contents' => $week2,
			],
			[
				'name'     => 'jumlah2',
				'contents' => $jumlah2,
			],
			[
				'name'     => 'week3',
				'contents' => $week3,
			],
			[
				'name'     => 'jumlah3',
				'contents' => $jumlah3,
			],
			[
				'name'     => 'week4',
				'contents' => $week4,
			],
			[
				'name'     => 'jumlah4',
				'contents' => $jumlah4,
			],
			[
				'name'     => 'week5',
				'contents' => $week5,
			],
			[
				'name'     => 'jumlah5',
				'contents' => $jumlah5,
			]
		];
		$merge = array_merge($data,$week);
	}

	// if($jenis==2){
	// 	$member = 
	// 		[
	// 			[   'name'     => 'member',
	// 				'contents' => $member,
	// 			]
	// 		];
	// 	$merge = array_merge($data,$member);
	// }

	// var_dump($merge);
	// die;
	$client = new Client();
	if($jenis==1){
		$result = $client->request('POST', $this->di['api']['link']."/api/transaksi/topup/send", [
			'multipart' => $merge
		]);
	}else{
		// if($jenis!=2){
		$result = $client->request('POST', $this->di['api']['link']."/api/transaksi/topup/send", [
			'multipart' => $data
		]);
		// }
	}
	

	$success = json_decode($result->getBody()->getContents(), true);

	// var_dump($member);
	// die;
	try {
		$id = 0;

		foreach ($success['message'] as $key =>$value) {
			if($key=='id'){
				$id = $value;
			}
		}

		$output = [];
		$output[] = [
			'name' => 'id',
			'contents' => $id
		];

		foreach($member as $key => $value){
			$output[] = [
				'name' => 'member['.$key.']',
				'contents' => $value,
			];
		}

		if($jenis==2){
			$result2 = $client->request('POST', $this->di['api']['link']."/api/transaksi/topup/send-detail", [
				'multipart' => $output
			]);
			$success2 = json_decode($result2->getBody()->getContents(), true);
			if($success2['status'] == 'error'){									
				$resul2['type'] = 'E';
				$resul2['message'] = 'Failed Top Up';
				return json_encode($resul2);
			}
		}	
	} catch (Exception $e) {
		
	}

	if($success['status'] == 'success'){			
		$resul['type'] = 'S';
		$resul['message'] = 'Success Top Up!';
		return json_encode($resul);
	}else{						
		$resul['type'] = 'E';
		$resul['message'] = 'Failed Top Up';
		return json_encode($resul);
	}
}

}
