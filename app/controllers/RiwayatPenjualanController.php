<?php

use Phalcon\Mvc\Controller;

class RiwayatPenjualanController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'RiwayatPenjualan' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
	}
	
	public function printAction(){
		$this->AuthorityAction();
	}	
	
	public function ajaxStrukAction(){
		$data = array();
		
		$data = Jual::getJoin($_GET['id_jual']);
		return json_encode($data);
	}
	
	public function deleteAction(){
		$this->AuthorityAction();
		
		$dataJual = new Jual;
		$dataJual->id_jual = $_GET['id_jual'];
		
		$success = $dataJual::goDelete($dataJual);
		if($success){
			$this->flashSession->success("Invoice berhasil dihapus.");
			$this->response->redirect(uri('RiwayatPenjualan'));
		}else{
			$this->flashSession->success("Invoice gagal dihapus.");
			$this->response->redirect(uri('RiwayatPenjualan'));
		}
	}

}
