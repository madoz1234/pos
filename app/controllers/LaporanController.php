<?php

include __DIR__ . "/../library/print_format.php";
require __DIR__.'/../library/spipu/html2pdf/html2pdf.class.php';

use Phalcon\Mvc\Controller;
use Spipu\Html2Pdf\Html2Pdf;

class LaporanController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'Laporan' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
	}
	
	public function showAction(){
		if($_POST['export'] == "Excel"){
			if($_POST['report'] == 'Pembelian Barang (PO)'){ $this->response->redirect(uri("Laporan/PO?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }
			if($_POST['report'] == 'Penerimaan Barang (GR)'){ $this->response->redirect(uri("Laporan/GR?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }	
			if($_POST['report'] == 'Penyesuaian'){ $this->response->redirect(uri("Laporan/Adjusment?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }	
			// if($_POST['report'] == 'Arus Kas'){ $this->response->redirect("Laporan/excelArusKas?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir'].""); }			
			// if($_POST['report'] == 'Rangkuman Penjualan'){ $this->response->redirect("Laporan/excelSummary?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir'].""); }
			if($_POST['report'] == 'Retur Barang'){ $this->response->redirect(uri("Laporan/ReturBarang?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }
			if($_POST['report'] == 'Stock'){ $this->response->redirect(uri("Laporan/Stock?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }			
			if($_POST['report'] == 'Stock Kosong'){ $this->response->redirect(uri("Laporan/Stock_0?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }		
			if($_POST['report'] == 'Penjualan'){ $this->response->redirect(uri("Laporan/Jual?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }			
			if($_POST['report'] == 'Rangkuman Penjualan'){ $this->response->redirect(uri("Laporan/excelKasir?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }
			// if($_POST['report'] == 'Retur Penjualan'){ $this->response->redirect("Laporan/excelReturSales?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir'].""); }
			if($_POST['report'] == 'PPOB'){ $this->response->redirect(uri("Laporan/PPOB?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }
			if($_POST['report'] == 'A/R Control'){ $this->response->redirect(uri("Laporan/AR?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }
		}
		
		if($_POST['export'] == "PDF"){
			if($_POST['report'] == 'Pembelian Barang (PO)'){ $this->response->redirect(uri("Laporan/printPO?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }	
			if($_POST['report'] == 'Penerimaan Barang (GR)'){ $this->response->redirect(uri("Laporan/printGR?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }	
			if($_POST['report'] == 'Penyesuaian'){ $this->response->redirect(uri("Laporan/printAdjusment?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir'].""));	}	
			// if($_POST['report'] == 'Arus Kas'){ $this->response->redirect("Laporan/printArusKas?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir'].""); }
			// if($_POST['report'] == 'Rangkuman Penjualan'){ $this->response->redirect("Laporan/printSummary?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir'].""); }
			if($_POST['report'] == 'Retur Barang'){ $this->response->redirect(uri("Laporan/printRetur?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }
			if($_POST['report'] == 'Stock'){ $this->response->redirect(uri("Laporan/printStock?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }
			if($_POST['report'] == 'Stock Kosong'){ $this->response->redirect(uri("Laporan/printStock0?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }
			if($_POST['report'] == 'Penjualan'){ $this->response->redirect(uri("Laporan/printSales?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }
			if($_POST['report'] == 'Rangkuman Penjualan'){ $this->response->redirect(uri("Laporan/printKasir?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }
			// if($_POST['report'] == 'Retur Penjualan'){ $this->response->redirect("Laporan/printReturSales?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir'].""); }
			if($_POST['report'] == 'PPOB'){ $this->response->redirect(uri("Laporan/printPPOB?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }
			if($_POST['report'] == 'A/R Control'){ $this->response->redirect(uri("Laporan/printAR?from=".$_POST['tgl_mulai']."&to=".$_POST['tgl_akhir']."")); }
		}
	}

	public function AdjusmentAction(){
		$this->AuthorityAction();
		function tgl_indonesia($date){
			$Hari = array ("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu",);
			$Bulan = array ("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
			$tahun     = substr($date, 0, 4);
			$bulan     = substr($date, 5, 2);
			$tgl    = substr($date, 8, 2);
			$waktu    = substr($date,11, 5);
			$hari    = date("w", strtotime($date));
			$result = $Hari[$hari].", ".$tgl." ".$Bulan[(int)$bulan-1]." ".$tahun." ".$waktu." ";
			return $result;
		}

		$from = $_GET['from'];
		$to = $_GET['to'];

		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->name = $list_tmuk['name'];			
			}
		}

		$adj = new Adj();
		$condition = " WHERE create_date >= '".$_GET['from']."' AND create_date <= '".$_GET['to']."' ";
		$lists = $adj::getJoin_AdjDetail($condition);

		if(count($lists) < 1){
			$this->flashSession->success("Gagal;Tidak ada data.;error");
			return $this->response->redirect(uri('Laporan'));
		}


		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->setTitle("Adj(".date('d-m-Y', strtotime($_GET['from']))." - ".date('d-m-Y', strtotime($_GET['to'])).")");

		foreach(range('A1','H4') as $head)
		{
			$objPHPExcel->getActiveSheet()->getStyle($head)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
		}

		$objPHPExcel->getActiveSheet()->getStyle('A1:H4')->getFont()->setBold(true);

		foreach (range('A', $objPHPExcel->getActiveSheet()->getHighestDataColumn()) as $col) {
			$objPHPExcel->getActiveSheet()
			->getColumnDimension($col)
			->setAutoSize(true);
		} 


		foreach(range('A4','H4') as $set)
		{
			$objPHPExcel->getActiveSheet()->getStyle($set)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
			$objPHPExcel->getActiveSheet()->getColumnDimension($set)->setAutoSize(true);
		}

		$tgl = tgl_indonesia($from)." -  ".tgl_indonesia($to);

		$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells('A1:H1')
		->mergeCells('A2:H2')
		->setCellValue('A1', 'LAPORAN PENYESUAIAN TMUK '.$tmuk->name)
		->setCellValue('A2', "$tgl")
		->setCellValue('A4', 'Tanggal')
		->setCellValue('B4', 'Kode Barang')
		->setCellValue('C4', 'Barcode')
		->setCellValue('D4', 'Nama Barang')
		->setCellValue('E4', 'Jumlah')
		->setCellValue('F4', 'Satuan')
		->setCellValue('G4', 'Tipe')
		->setCellValue('H4', 'Detail');
		$i=5;
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		foreach ($lists as $key => $value) {
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, date('d-m-Y', strtotime($value['create_date'])))
			->setCellValue('B'.$i, $value['item_code'])
			->setCellValue('C'.$i, $value['barcode_code'])
			->setCellValue('D'.$i, $value['description'])
			->setCellValue('E'.$i, $value['qty'])
			->setCellValue('F'.$i, $value['unit'])
			->setCellValue('G'.$i, $value['adj_type'])
			->setCellValue('H'.$i, $value['adj_detail']);
			$i++;
		}
		$objPHPExcel->getActiveSheet()->getStyle('A4:H'.($i-1))->applyFromArray($styleArray);
		unset($styleArray);

		$fname = "Penyesuaian_Report(".date('d-m-Y', strtotime($_GET['from']))." - ".date('d-m-Y', strtotime($_GET['to'])).")" . ".xlsx";
		$temp_file = tempnam(sys_get_temp_dir(), 'phpexcel');

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($temp_file);

		$response = new \Phalcon\Http\Response();

		$response->setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		$response->setHeader('Content-Disposition', 'attachment;filename="' . $fname . '"');
		$response->setHeader('Cache-Control', 'max-age=0');

		$response->setHeader('Cache-Control', 'max-age=1');

		$response->setContent(file_get_contents($temp_file));

		unlink($temp_file);

		return $response;
	}

	public function POAction(){
		$this->AuthorityAction();
		function tgl_indonesia($date){
			$Hari = array ("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu",);
			$Bulan = array ("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
			$tahun     = substr($date, 0, 4);
			$bulan     = substr($date, 5, 2);
			$tgl    = substr($date, 8, 2);
			$waktu    = substr($date,11, 5);
			$hari    = date("w", strtotime($date));
			$result = $Hari[$hari].", ".$tgl." ".$Bulan[(int)$bulan-1]." ".$tahun." ".$waktu." ";
			return $result;
		}

		$from = $_GET['from'];
		$to = $_GET['to'];

		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->name = $list_tmuk['name'];			
			}
		}

		$data = new PO();
		$condition = " WHERE order_date >= '".$_GET['from']."' AND order_date <= '".$_GET['to']."' ";
		$lists = $data::getJoin($condition);

		if(count($lists) < 1){
			$this->flashSession->success("Gagal;Tidak ada data.;error");
			return $this->response->redirect(uri('Laporan'));
		}

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->setTitle("PO(".date('d-m-Y', strtotime($_GET['from']))." - ".date('d-m-Y', strtotime($_GET['to'])).")");
		foreach(range('A1','N2') as $judul)
		{
			$objPHPExcel->getActiveSheet()->getColumnDimension($judul)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getStyle($judul)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
		}

		foreach(range('A4','N5') as $head)
		{
			$objPHPExcel->getActiveSheet()->getColumnDimension($head)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getStyle($head)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
		}
		$objPHPExcel->getActiveSheet()->getStyle('A1:N5')->getFont()->setBold(true);

		foreach (range('A', $objPHPExcel->getActiveSheet()->getHighestDataColumn()) as $col) {
			$objPHPExcel->getActiveSheet()
			->getColumnDimension($col)
			->setAutoSize(true);
		}

		$tgl = tgl_indonesia($from)." -  ".tgl_indonesia($to);

		$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells('A1:N1')
		->mergeCells('A2:N2')
		->mergeCells('A4:A5')
		->mergeCells('B4:B5')
		->mergeCells('C4:C5')
		->mergeCells('D4:D5')
		->mergeCells('E4:G4')
		->mergeCells('H4:J4')
		->mergeCells('K4:L5')
		->mergeCells('M4:M5')
		->mergeCells('N4:N5');


		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A1', 'LAPORAN PURCHASE ORDER (PO) TMUK '.$tmuk->name)
		->setCellValue('A2', "$tgl")
		->setCellValue('A4', 'Nomor PO')
		->setCellValue('B4', 'Tanggal Pesan')
		->setCellValue('C4', 'Kode Barang')
		->setCellValue('D4', 'Nama Barang')
		->setCellValue('E4', 'Info Beli')
		->setCellValue('E5', 'Jumlah')
		->setCellValue('F5', 'Satuan')
		->setCellValue('G5', 'Barcode')
		->setCellValue('H4', 'Info Jual')
		->setCellValue('H5', 'Jumlah')
		->setCellValue('I5', 'Satuan')
		->setCellValue('J5', 'Barcode')
		->setCellValue('K4', 'Jumlah PO')
		->setCellValue('M4', 'Harga Satuan')
		->setCellValue('N4', 'Harga Total');
		$i=6;
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		foreach ($lists as $key => $value) {
			$total += $value['po_price'];
			if($value['po_unit_price'] == false){
				$po_price = 0;
			}else{
				$po_price = $value['po_unit_price'];
			}
			$objPHPExcel->getActiveSheet()->getStyle('M'.$i)->getNumberFormat()
				->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
			$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->getNumberFormat()
				->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, $value['po_no_ref'])
			->setCellValue('B'.$i, date('d-m-Y', strtotime($value['order_date'])))
			->setCellValue('C'.$i, $value['item_code'])
			->setCellValue('D'.$i, $value['description'])
			->setCellValue('E'.$i, $value['uom_o_conversion'])
			->setCellValue('F'.$i, $value['uom_o_type'])
			->setCellValue('G'.$i, $value['uom_o_barcode'])
			->setCellValue('H'.$i, $value['uom_s_conversion'])
			->setCellValue('I'.$i, $value['uom_s_type'])
			->setCellValue('J'.$i, $value['uom_s_barcode'])
			->setCellValue('K'.$i, $value['qty_po'])
			->setCellValue('L'.$i, $value['unit_po'])
			->setCellValue('M'.$i, $po_price)
			->setCellValue('N'.$i, $value['po_price']);
			$i++;
		}

		$a = "A".$i; // or any value
		$b = "L".($i); // or any value
		$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells("$a:$b");

		$objPHPExcel->getActiveSheet()->getStyle('M'.($i))->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('N'.($i))->getNumberFormat()
			->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('M'.($i), 'Total');

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('N'.($i), $total);

		$objPHPExcel->getActiveSheet()->getStyle('A4:N'.($i))->applyFromArray($styleArray);
		unset($styleArray);
		
		$fname = "PO_Report(".date('d-m-Y', strtotime($_GET['from']))." - ".date('d-m-Y', strtotime($_GET['to'])).")" . ".xlsx";
		$temp_file = tempnam(sys_get_temp_dir(), 'phpexcel');

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($temp_file);

		$response = new \Phalcon\Http\Response();

		$response->setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		$response->setHeader('Content-Disposition', 'attachment;filename="' . $fname . '"');
		$response->setHeader('Cache-Control', 'max-age=0');

		$response->setHeader('Cache-Control', 'max-age=1');

		$response->setContent(file_get_contents($temp_file));

		unlink($temp_file);

		return $response;
	}

	public function ReturBarangAction(){
		$this->AuthorityAction();
		function tgl_indonesia($date){
			$Hari = array ("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu",);
			$Bulan = array ("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
			$tahun     = substr($date, 0, 4);
			$bulan     = substr($date, 5, 2);
			$tgl    = substr($date, 8, 2);
			$waktu    = substr($date,11, 5);
			$hari    = date("w", strtotime($date));
			$result = $Hari[$hari].", ".$tgl." ".$Bulan[(int)$bulan-1]." ".$tahun." ".$waktu." ";
			return $result;
		}

		$from = $_GET['from'];
		$to = $_GET['to'];

		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->name = $list_tmuk['name'];			
			}
		}

		$data = new Retur();
		$condition = " WHERE create_date >= '".$_GET['from']."' AND create_date <= '".$_GET['to']."' ";
		$lists = $data::getJoin_Detail_Product($condition);

		if(count($lists) < 1){
			$this->flashSession->success("Gagal;Tidak ada data.;error");
			return $this->response->redirect(uri('Laporan'));
		}

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->setTitle("Retur(".date('d-m-Y', strtotime($_GET['from']))." - ".date('d-m-Y', strtotime($_GET['to'])).")");
		foreach(range('A1','M5') as $head)
		{
			$objPHPExcel->getActiveSheet()->getColumnDimension($head)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getStyle($head)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
		}
		$objPHPExcel->getActiveSheet()->getStyle('A1:M5')->getFont()->setBold(true);

		foreach (range('A', $objPHPExcel->getActiveSheet()->getHighestDataColumn()) as $col) {
			$objPHPExcel->getActiveSheet()
			->getColumnDimension($col)
			->setAutoSize(true);
		} 

		$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells('A1:M1')
		->mergeCells('A2:M2')
		->mergeCells('A4:A5')
		->mergeCells('B4:B5')
		->mergeCells('C4:C5')
		->mergeCells('D4:F4')
		->mergeCells('G4:I4')
		->mergeCells('J4:K5')
		->mergeCells('L4:L5')
		->mergeCells('M4:M5');

		$tgl = tgl_indonesia($from)." -  ".tgl_indonesia($to);

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A1', 'LAPORAN RETUR BARANG (RR) TMUK '.$tmuk->name)
		->setCellValue('A2', "$tgl")
		->setCellValue('A4', 'Nomor Retur')
		->setCellValue('B4', 'Kode Barang')
		->setCellValue('C4', 'Nama Barang')
		->setCellValue('D4', 'Info Retur')
		->setCellValue('D5', 'Jumlah')
		->setCellValue('E5', 'Satuan')
		->setCellValue('F5', 'Barcode')
		->setCellValue('G4', 'Info Jual')
		->setCellValue('G5', 'Jumlah')
		->setCellValue('H5', 'Satuan')
		->setCellValue('I5', 'Barcode')
		->setCellValue('J4', 'Jumlah Retur')
		->setCellValue('L4', 'Harga Satuan')
		->setCellValue('M4', 'Harga Total');
		$i=6;
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		foreach ($lists as $key => $value) {
			$total += $value['qty_retur'] * $value['retur_unit_price'];
			$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->getNumberFormat()
				->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
			$objPHPExcel->getActiveSheet()->getStyle('M'.$i)->getNumberFormat()
				->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, $value['retur_no'])
			->setCellValue('B'.$i, $value['item_code'])
			->setCellValue('C'.$i, $value['description'])
			->setCellValue('D'.$i, $value['uom_r_conversion'])
			->setCellValue('E'.$i, $value['uom_r_type'])
			->setCellValue('F'.$i, $value['uom_r_barcode'])
			->setCellValue('G'.$i, $value['uom_s_conversion'])
			->setCellValue('H'.$i, $value['uom_s_type'])
			->setCellValue('I'.$i, $value['uom_s_barcode'])
			->setCellValue('J'.$i, $value['qty_retur'])
			->setCellValue('K'.$i, $value['unit_retur'])
			->setCellValue('L'.$i, $value['retur_unit_price'])
			->setCellValue('M'.$i, ($value['qty_retur'] * $value['retur_unit_price']));
			$i++;
		}

		$a = "A".$i; // or any value
		$b = "K".($i); // or any value
		$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells("$a:$b");

		$objPHPExcel->getActiveSheet()->getStyle('L'.($i))->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('M'.($i))->getNumberFormat()
			->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('L'.($i), 'Total');

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('M'.($i), $total);

		$objPHPExcel->getActiveSheet()->getStyle('A4:M'.($i))->applyFromArray($styleArray);
		unset($styleArray);
		
		$fname = "ReturBarang_Report(".date('d-m-Y', strtotime($_GET['from']))." - ".date('d-m-Y', strtotime($_GET['to'])).")" . ".xlsx";
		$temp_file = tempnam(sys_get_temp_dir(), 'phpexcel');

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($temp_file);

		$response = new \Phalcon\Http\Response();

		$response->setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		$response->setHeader('Content-Disposition', 'attachment;filename="' . $fname . '"');
		$response->setHeader('Cache-Control', 'max-age=0');

		$response->setHeader('Cache-Control', 'max-age=1');

		$response->setContent(file_get_contents($temp_file));

		unlink($temp_file);

		return $response;
	}

	public function GRAction(){
		$this->AuthorityAction();
		function tgl_indonesia($date){
			$Hari = array ("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu",);
			$Bulan = array ("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
			$tahun     = substr($date, 0, 4);
			$bulan     = substr($date, 5, 2);
			$tgl    = substr($date, 8, 2);
			$waktu    = substr($date,11, 5);
			$hari    = date("w", strtotime($date));
			$result = $Hari[$hari].", ".$tgl." ".$Bulan[(int)$bulan-1]." ".$tahun." ".$waktu." ";
			return $result;
		}

		$from = $_GET['from'];
		$to = $_GET['to'];

		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->name = $list_tmuk['name'];			
			}
		}

		$data = new GR();
		$condition = " WHERE create_date >= '".$_GET['from']."' AND create_date <= '".$_GET['to']."' ";
		$lists = $data::getJoin_PR_PO($condition);

		if(count($lists) < 1){
			$this->flashSession->success("Gagal;Tidak ada data.;error");
			return $this->response->redirect(uri('Laporan'));
		}


		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->setTitle("GR(".date('d-m-Y', strtotime($_GET['from']))." - ".date('d-m-Y', strtotime($_GET['to'])).")");
		foreach(range('A1','O5') as $head)
		{
			$objPHPExcel->getActiveSheet()->getColumnDimension($head)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getStyle($head)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
		}
		$objPHPExcel->getActiveSheet()->getStyle('A1:O5')->getFont()->setBold(true);

		foreach (range('A', $objPHPExcel->getActiveSheet()->getHighestDataColumn()) as $col) {
			$objPHPExcel->getActiveSheet()
			->getColumnDimension($col)
			->setAutoSize(true);
		}

		$tgl = tgl_indonesia($from)." -  ".tgl_indonesia($to);

		$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells('A1:O1')
		->mergeCells('A2:O2')
		->mergeCells('A4:A5')
		->mergeCells('B4:B5')
		->mergeCells('C4:C5')
		->mergeCells('D4:D5')
		->mergeCells('E4:G4')
		->mergeCells('H4:J4')
		->mergeCells('K4:L5')
		->mergeCells('M4:N5')
		->mergeCells('O4:O5');


		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A1', 'LAPORAN PENERIMAAN BARANG (GR) TMUK '.$tmuk->name)
		->setCellValue('A2', "$tgl")
		->setCellValue('A4', 'Nomor PO')
		->setCellValue('B4', 'Tanggal Buat')
		->setCellValue('C4', 'Kode Barang')
		->setCellValue('D4', 'Nama Barang')
		->setCellValue('E4', 'Info Beli')
		->setCellValue('E5', 'Jumlah')
		->setCellValue('F5', 'Satuan')
		->setCellValue('G5', 'Barcode')
		->setCellValue('H4', 'Info Receive')
		->setCellValue('H5', 'Jumlah')
		->setCellValue('I5', 'Satuan')
		->setCellValue('J5', 'Barcode')
		->setCellValue('K4', 'Jumlah PR')
		->setCellValue('M4', 'Jumlah PO')
		->setCellValue('O4', 'Status');
		$i=6;
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		foreach ($lists as $key => $value) {
			// $objPHPExcel->getActiveSheet()->getStyle('M'.$i)->getNumberFormat()
			// 	->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
			// $objPHPExcel->getActiveSheet()->getStyle('N'.$i)->getNumberFormat()
			// 	->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, $value['gr_no'])
			->setCellValue('B'.$i, date('d-m-Y', strtotime($value['create_date'])))
			->setCellValue('C'.$i, $value['item_code'])
			->setCellValue('D'.$i, $value['description'])
			->setCellValue('E'.$i, $value['uom_o_conversion'])
			->setCellValue('F'.$i, $value['uom_o_type'])
			->setCellValue('G'.$i, $value['uom_o_barcode'])
			->setCellValue('H'.$i, $value['uom_r_conversion'])
			->setCellValue('I'.$i, $value['uom_r_type'])
			->setCellValue('J'.$i, $value['uom_r_barcode'])
			->setCellValue('K'.$i, $value['qty_pr'])
			->setCellValue('L'.$i, $value['unit_pr'])
			->setCellValue('M'.$i, $value['qty_order'])
			->setCellValue('N'.$i, $value['unit_order'])
			->setCellValue('O'.$i, round(($value['qty_po'] / ($value['qty_order']) * 100), 2)." %");
			$i++;
		}

		$objPHPExcel->getActiveSheet()->getStyle('A4:O'.($i-1))->applyFromArray($styleArray);
		unset($styleArray);
		
		$fname = "GR_Report(".date('d-m-Y', strtotime($_GET['from']))." - ".date('d-m-Y', strtotime($_GET['to'])).")" . ".xlsx";
		$temp_file = tempnam(sys_get_temp_dir(), 'phpexcel');

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($temp_file);

		$response = new \Phalcon\Http\Response();

		$response->setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		$response->setHeader('Content-Disposition', 'attachment;filename="' . $fname . '"');
		$response->setHeader('Cache-Control', 'max-age=0');

		$response->setHeader('Cache-Control', 'max-age=1');

		$response->setContent(file_get_contents($temp_file));

		unlink($temp_file);

		return $response;
	}

	public function ARAction(){
		$this->AuthorityAction();
		function tgl_indonesia($date){
			$Hari = array ("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu",);
			$Bulan = array ("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
			$tahun     = substr($date, 0, 4);
			$bulan     = substr($date, 5, 2);
			$tgl    = substr($date, 8, 2);
			$waktu    = substr($date,11, 5);
			$hari    = date("w", strtotime($date));
			$result = $Hari[$hari].", ".$tgl." ".$Bulan[(int)$bulan-1]." ".$tahun." ".$waktu." ";
			return $result;
		}

		$from = $_GET['from'];
		$to = $_GET['to'];

		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->name = $list_tmuk['name'];			
			}
		}

		$lists = Member::reportAR();

		if(count($lists) <= 0){
			$this->flashSession->success("Gagal;Tidak ada data.;error");
			return $this->response->redirect(uri('Laporan'));
		}


		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->setTitle("AR(".date('d-m-Y', strtotime($_GET['from']))." - ".date('d-m-Y', strtotime($_GET['to'])).")");

		foreach(range('A1','F2') as $head)
		{
			$objPHPExcel->getActiveSheet()->getStyle($head)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
		}


		foreach(range('A4','G4') as $set)
		{
			$objPHPExcel->getActiveSheet()->getStyle($set)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
			$objPHPExcel->getActiveSheet()->getColumnDimension($set)->setAutoSize(true);
		}

		$tgl = tgl_indonesia($from)." -  ".tgl_indonesia($to);

		$i=5; $j = 4;
		$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('A2:G2')->getFont()->setBold( true );
		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('A1:G1')
			->mergeCells('A2:G2')
			->setCellValue('A1', 'LAPORAN A/R Control TMUK '.$tmuk->name)
			->setCellValue('A2', "$tgl");
		$id_member = "";
		foreach ($lists as $value) {
			if($value['tipe'] == 'PIUTANG'){	
				if($value['total_purchase'] > 0){
					$piutang = $value['total_purchase'];
				}else{
					$piutang = 0;
				}	
			}
			if($value['tipe'] == 'TUNAI'){
				if($value['total_purchase'] != 0){
					$grand_total += $value['total_purchase'] - $value['kembalian'];
				}else{
					$grand_total = 0;
				}
			}else{
				if($value['total_purchase'] > 0){
					$grand_total += $value['total_purchase'];
				}else{
					$grand_total = 0;
				}
			}
			if($value['piutang_payment'] > 0){
				$grand_piutang = $value['piutang_payment'];
			}else{
				$grand_piutang = 0;
			}
			$total = $value['total_purchase'];
			if($id_member != $value['nomor_member']){
				$from = "A".$j; // or any value
				$to = "G".$j; // or any value
				$objPHPExcel->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold( true );
				$objPHPExcel->getActiveSheet()->getStyle('F'.($j+3))->getFont()->setBold( true );
				$objPHPExcel->getActiveSheet()->getStyle('F'.($j+4))->getFont()->setBold( true );
				$objPHPExcel->getActiveSheet()->getStyle('F'.($j+5))->getFont()->setBold( true );
				$objPHPExcel->getActiveSheet()->getStyle('F'.($j+3))->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
				);
				$objPHPExcel->getActiveSheet()->getStyle('F'.($j+4))->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
				);
				$objPHPExcel->getActiveSheet()->getStyle('F'.($j+5))->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
				);
				$cond = " WHERE id_member = '".$value['nomor_member']."'";
				$total_order = Jual::getCount($cond);

				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$j, 'ID Member')
					->setCellValue('B'.$j, 'Nama Member')
					->setCellValue('C'.$j, 'Alamat')
					->setCellValue('D'.$j, 'Limit Kredit')
					->setCellValue('E'.$j, 'Jumlah Transaksi')
					->setCellValue('F'.$j, 'Jenis Pembelian')
					->setCellValue('G'.$j, 'Total Nilai Pembelian');
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$i, $value['nomor_member'])
					->setCellValue('B'.$i, $value['br_name'])
					->setCellValue('C'.$i, $value['br_address'])
					->setCellValue('D'.$i, $value['credit_limit'])
					->setCellValue('E'.$i, $total_order)
					->setCellValue('F'.$i, 'TUNAI')
					->setCellValue('G'.$i, ($value['total_purchase'] -$value['kembalian']));
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('F'.($j+3), 'Total Pembayaran')
					->setCellValue('F'.($j+4), 'Piutang Dibayar')
					->setCellValue('F'.($j+5), 'Total Piutang');

				if($value['tipe'] == 'TUNAI'){
					$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('G'.($i+1), 0)
						->setCellValue('G'.($i+2), $value['total_purchase'])
						->setCellValue('G'.($i+3), 0)
						->setCellValue('G'.($i+4), 0);
				}
				if($value['tipe'] == 'PIUTANG'){
					$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('F'.($i+1), 'PIUTANG');
				}
				$kembali1 = $value['kembalian'];
				$a = $value['total_purchase'];
				$i+=7;
				$j+=7;
			}else{
				if($value['tipe'] == 'PIUTANG'){
					$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('F'.($i-6), 'PIUTANG');
					$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('G'.($i-6), $value['total_purchase'])
						->setCellValue('G'.($i-4), $grand_piutang)
						->setCellValue('G'.($i-3), $value['total_purchase']);
				}
				$b = $value['total_purchase'];
				$kembali2 = $value['kembalian'];
			}
			if($value['tipe'] == 'PIUTANG'){
				$waduk = $a + $b;
				$kembalian = $kembali1 + $kembali2;
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('F'.($i-6), 'PIUTANG');
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('G'.($i-5), ($waduk - $kembalian));
			}else{
				$waduk = $a;
				$kembalian = $kembali1;
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('G'.($i-5), ($waduk - $kembalian));
			}
			$id_member = $value['nomor_member'];
		}

		$fname = "AR_Report(".date('d-m-Y', strtotime($_GET['from']))." - ".date('d-m-Y', strtotime($_GET['to'])).")" . ".xlsx";
		$temp_file = tempnam(sys_get_temp_dir(), 'phpexcel');

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($temp_file);

		$response = new \Phalcon\Http\Response();

		$response->setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		$response->setHeader('Content-Disposition', 'attachment;filename="' . $fname . '"');
		$response->setHeader('Cache-Control', 'max-age=0');

		$response->setHeader('Cache-Control', 'max-age=1');

		$response->setContent(file_get_contents($temp_file));

		unlink($temp_file);

		return $response;
	}

	public function StockAction(){
		$this->AuthorityAction();
		function tgl_indonesia($date){
			$Hari = array ("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu",);
			$Bulan = array ("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
			$tahun     = substr($date, 0, 4);
			$bulan     = substr($date, 5, 2);
			$tgl    = substr($date, 8, 2);
			$waktu    = substr($date,11, 5);
			$hari    = date("w", strtotime($date));
			$result = $Hari[$hari].", ".$tgl." ".$Bulan[(int)$bulan-1]." ".$tahun." ".$waktu." ";
			return $result;
		}

		$from = $_GET['from'];
		$to = $_GET['to'];

		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->name = $list_tmuk['name'];			
			}
		}

		$data = new Product();
		$cond1 = " WHERE tran_date <= '".$_GET['to']."' AND status_flag !='2' ";
		$lists = $data::getJoin_ProductMove5($cond1);

		if(count($lists) < 1){
			$this->flashSession->success("Gagal;Tidak ada data.;error");
			return $this->response->redirect(uri('Laporan'));
		}


		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->setTitle("Stock(".date('d-m-Y', strtotime($_GET['from']))." - ".date('d-m-Y', strtotime($_GET['to'])).")");

		foreach(range('A1','F2') as $head)
		{
			$objPHPExcel->getActiveSheet()->getStyle($head)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
		}


		foreach(range('A4','I4') as $set)
		{
			$objPHPExcel->getActiveSheet()->getStyle($set)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
			$objPHPExcel->getActiveSheet()->getColumnDimension($set)->setAutoSize(true);
		}

		$objPHPExcel->getActiveSheet()->getStyle('A1:I4')->getFont()->setBold(true);

		foreach (range('A', $objPHPExcel->getActiveSheet()->getHighestDataColumn()) as $col) {
			$objPHPExcel->getActiveSheet()
			->getColumnDimension($col)
			->setAutoSize(true);
		}

		$tgl = tgl_indonesia($from)." -  ".tgl_indonesia($to);

		$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells('A1:I1')
		->mergeCells('A2:I2')
		->setCellValue('A1', 'LAPORAN STOCK TMUK '.$tmuk->name)
		->setCellValue('A2', "$tgl")
		->setCellValue('A4', 'Departemen')
		->setCellValue('B4', 'Kategori')
		->setCellValue('C4', 'Lotte / Non Lotte')
		->setCellValue('D4', 'Kode Barang')
		->setCellValue('E4', 'Barcode')
		->setCellValue('F4', 'Nama Barang')
		->setCellValue('G4', 'MAP')
		->setCellValue('H4', 'Kuantitas')
		->setCellValue('I4', 'Nilai');

		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		$i=5;
		foreach ($lists as $key => $value) {
			if($value['tipe'] == 001){
				$tipe = 'Trade Lotte';
			}
			if($value['tipe'] == 002){
				$tipe = 'Trade Non Lotte';
			}
			if($value['tipe'] == 003){
				$tipe = 'Non Trade Lotte';
			}
			if($value['tipe'] == 004){
				$tipe = 'Non Trade Non Lotte';
			}
			$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()
				->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
			$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getNumberFormat()
				->setFormatCode('_(""* #,##0_);_(""* \(#,##0\);_(""* "-"??_);_(@_)');
			$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->getNumberFormat()
				->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');

			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, $value['bumun_nm'])
			->setCellValue('B'.$i, htmlspecialchars_decode($value['l1_nm']))
			->setCellValue('C'.$i, $tipe)
			->setCellValue('D'.$i, $value['stock_id'])
			->setCellValue('E'.$i, $value['uom_ib'])
			->setCellValue('F'.$i, htmlspecialchars_decode($value['description']))
			->setCellValue('G'.$i, $value['uom1_harga'])
			->setCellValue('H'.$i, $value['nilai_persediaan'])
			->setCellValue('I'.$i, ($value['uom1_harga'] * $value['nilai_persediaan']));
			$i++;
		}

		$objPHPExcel->getActiveSheet()->getStyle('A4:I'.($i-1))->applyFromArray($styleArray);
		unset($styleArray);
		$fname = "Stock_Report(".date('d-m-Y', strtotime($_GET['from']))." - ".date('d-m-Y', strtotime($_GET['to'])).")" . ".xlsx";
		$temp_file = tempnam(sys_get_temp_dir(), 'phpexcel');

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($temp_file);

		$response = new \Phalcon\Http\Response();

		$response->setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		$response->setHeader('Content-Disposition', 'attachment;filename="' . $fname . '"');
		$response->setHeader('Cache-Control', 'max-age=0');

		$response->setHeader('Cache-Control', 'max-age=1');

		$response->setContent(file_get_contents($temp_file));

		unlink($temp_file);

		return $response;
	}

	public function JualAction(){
		$this->AuthorityAction();
		function tgl_indonesia($date){
			$Hari = array ("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu",);
			$Bulan = array ("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
			$tahun     = substr($date, 0, 4);
			$bulan     = substr($date, 5, 2);
			$tgl    = substr($date, 8, 2);
			$waktu    = substr($date,11, 5);
			$hari    = date("w", strtotime($date));
			$result = $Hari[$hari].", ".$tgl." ".$Bulan[(int)$bulan-1]." ".$tahun." ".$waktu." ";
			return $result;
		}

		$from = $_GET['from'];
		$to = $_GET['to'];

		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->name = $list_tmuk['name'];			
			}
		}
		$from = $_GET['from'];
		$to = $_GET['to'];

		$data = new ProductMove();
		$lists = $data::Join_JualDetail2($from, $to);
		$t_jual = new Jual();
		$jual_detail = new JualDetail();
		$join = $t_jual::Join_JualDetail($from, $to);

		$piutang_member     = $t_jual->getTotalPembayaranCetak(['PIUTANG'], $from, $to);
		$point_member       = $t_jual->getTotalPembayaranCetak(['POINT'], $from, $to);
		$potongan_voucher	= $t_jual->getTotalPembayaranCetak(['VOUCHER'], $from, $to);
		$potongan_diskon 	= $jual_detail->getTotalDiskonCetak($from, $to);
		$potongan_member 	= $jual_detail->getTotalMemberCetak($from, $to);

		$potongan_penjualan = $potongan_voucher + $potongan_diskon + $potongan_member;
		if(count($lists) < 1){
			$this->flashSession->success("Gagal;Tidak ada data.;error");
			return $this->response->redirect(uri('Laporan'));
		}


		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->setTitle("Sales(".date('d-m-Y', strtotime($_GET['from']))." - ".date('d-m-Y', strtotime($_GET['to'])).")");
		foreach(range('A1','N5') as $head)
		{
			$objPHPExcel->getActiveSheet()->getColumnDimension($head)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getStyle($head)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
		}
		$objPHPExcel->getActiveSheet()->getStyle('A1:N4')->getFont()->setBold(true);

		foreach (range('A', $objPHPExcel->getActiveSheet()->getHighestDataColumn()) as $col) {
			$objPHPExcel->getActiveSheet()
			->getColumnDimension($col)
			->setAutoSize(true);
		} 

		$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells('A1:N1')
		->mergeCells('A2:N2');
		$tgl = tgl_indonesia($from)." -  ".tgl_indonesia($to);

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A1', 'LAPORAN PENJUALAN TMUK '.$tmuk->name)
		->setCellValue('A2', "$tgl")
		->setCellValue('A4', 'Tanggal')
		->setCellValue('B4', 'Kode Barang')
		->setCellValue('C4', 'Barcode')
		->setCellValue('D4', 'Nama Barang')
		->setCellValue('E4', 'Harga Jual')
		->setCellValue('F4', 'Diskon')
		->setCellValue('G4', 'HPP')
		->setCellValue('H4', 'Qty')
		->setCellValue('I4', 'Margin (Rp)')
		->setCellValue('J4', 'Margin (%)')
		->setCellValue('K4', 'Sales Before PPN')
		->setCellValue('L4', 'PPN')
		->setCellValue('M4', 'Sales After PPN')
		->setCellValue('N4', 'Laba / Rugi');
		$i=5;
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		
		foreach ($join as $key => $val) {
			$jd = new JualDetail();
			$cek = $jd::getJoin_Product100($val['id_jual']);
			foreach ($cek as $key => $data) {
				if(count($data) > 0){
					// $jual_detail = new JualDetail();
					// $condition = " WHERE \"item_code\" = '".$data['0']['item_code']."' AND \"id_jual\" = '".$val['id_jual']."' LIMIT 1";
					// $aa = $jual_detail::getFreeSQL2($condition);

					$mpp = new ProductPrice();
					$condition = " WHERE \"stock_id\" = '".$data['item_code']."' ";
					$price = $mpp::getFreeSQL($condition);

					if($data){
						$aharga_hpp = $data['harga_hpp3'];
						$aharga_jual = $data['harga'];
						$adiscount = $data['discount'];
						$aharga_total = $data['total'];
					}else{
						$aharga_hpp = $price['0']['average_cost'];
						if($price['0']['uom1_changed_price'] > 0){
							$aharga_jual = $price['0']['uom1_changed_price'];
						}else{
							$aharga_jual = $price['0']['uom1_rounding_price'];
						}
						$adiscount = 0;
						$aharga_total = 0;
					}

					if($adiscount > 0){
						$adiskon = ($adiscount / $data['qty']);
						$aharga = ($aharga_total / $data['qty']);
					}else{
						$adiskon = 0;
						$aharga = $aharga_jual;
					}

					$ahpp = $aharga_hpp;
					$aLR = ($aharga_total) - ($ahpp * $data['qty']);

					$aslsbt = ($aharga_total / 1.1);
					$aslsat = ($aharga_total);
					$atax = ($aslsat - $aslsbt);	
					$amarginhp = ($aharga - $ahpp);
					$amarginpersen = ($amarginhp / $ahpp * 100);
					$total_hpp += ($ahpp * $data['qty']);

					$atotal += $aLR;
					$atotalSBT += $aslsbt;
					$atotalSAT += $aslsat;
					$atotalTax += $atax;
					$atotalMarginh += $amarginhp;
					$atotalMarginp += $amarginpersen;

					$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()
						->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()
						->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
					$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()
						->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
					$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->getNumberFormat()
						->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
					$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->getNumberFormat()
						->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
					$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->getNumberFormat()
						->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
					$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->getNumberFormat()
						->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
					$objPHPExcel->getActiveSheet()->getStyle('M'.$i)->getNumberFormat()
						->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
					$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->getNumberFormat()
						->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
					$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$i, date('d-m-Y', strtotime($val['tanggal'])))
					->setCellValue('B'.$i, $data['item_code'])
					->setCellValue('C'.$i, $data['barcode'])
					->setCellValue('D'.$i, $data['description'])
					->setCellValue('E'.$i, $aharga_jual)
					->setCellValue('F'.$i, $adiskon)
					->setCellValue('G'.$i, $ahpp)
					->setCellValue('H'.$i, $data['qty'])
					->setCellValue('I'.$i, $amarginhp)
					->setCellValue('J'.$i, $amarginpersen)
					->setCellValue('K'.$i, $aslsbt)
					->setCellValue('L'.$i, $atax)
					->setCellValue('M'.$i, $aslsat)
					->setCellValue('N'.$i, $aLR);
					$i++;
				}
			}
		}
		$a = "A".$i; // or any value
		$b = "L".($i+4); // or any value
		$total = $atotalSAT + $potongan_penjualan + $piutang_member + $point_member; // set total penjualan
		$marginRp = $total - $total_hpp; // set margin Rp/ Profit
		$marginPersen = ($marginRp / $total) * 100; // Set Margin %
		$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells("$a:$b");

		$objPHPExcel->getActiveSheet()->getStyle('M'.($i))->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('M'.($i+1))->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('M'.($i+2))->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('M'.($i+3))->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('M'.($i+4))->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('N'.($i))->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('N'.($i+1))->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('N'.($i+2))->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('N'.($i+3))->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('N'.($i+4))->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getStyle('N'.($i))->getNumberFormat()
			->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
		$objPHPExcel->getActiveSheet()->getStyle('N'.($i+1))->getNumberFormat()
			->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
		$objPHPExcel->getActiveSheet()->getStyle('N'.($i+2))->getNumberFormat()
			->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
		$objPHPExcel->getActiveSheet()->getStyle('N'.($i+3))->getNumberFormat()
			->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
		$objPHPExcel->getActiveSheet()->getStyle('N'.($i+4))->getNumberFormat()
			->setFormatCode('_(""* #,##0_);_(""* \(#,##0\);_(""* "-"??_);_(@_)');
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('M'.($i), 'Total Penjualan')
		->setCellValue('M'.($i+1), 'Total Penjualan Tunai')
		->setCellValue('M'.($i+2), 'Total HPP Penjualan')
		->setCellValue('M'.($i+3), 'Profit')
		->setCellValue('M'.($i+4), 'Margin %');

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('N'.($i), $total)
		->setCellValue('N'.($i+1), $atotalSAT)
		->setCellValue('N'.($i+2), $total_hpp)
		->setCellValue('N'.($i+3), $marginRp)
		->setCellValue('N'.($i+4), round($marginPersen));

		$objPHPExcel->getActiveSheet()->getStyle('A4:N'.($i+4))->applyFromArray($styleArray);
		unset($styleArray);
		
		$fname = "Sales_Report(".date('d-m-Y', strtotime($_GET['from']))." - ".date('d-m-Y', strtotime($_GET['to'])).")" . ".xlsx";
		$temp_file = tempnam(sys_get_temp_dir(), 'phpexcel');

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($temp_file);

		$response = new \Phalcon\Http\Response();

		$response->setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		$response->setHeader('Content-Disposition', 'attachment;filename="' . $fname . '"');
		$response->setHeader('Cache-Control', 'max-age=0');

		$response->setHeader('Cache-Control', 'max-age=1');

		$response->setContent(file_get_contents($temp_file));

		unlink($temp_file);

		return $response;
	}

	public function PPOBAction(){
		$this->AuthorityAction();
		function tgl_indonesia($date){
			$Hari = array ("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu",);
			$Bulan = array ("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
			$tahun     = substr($date, 0, 4);
			$bulan     = substr($date, 5, 2);
			$tgl    = substr($date, 8, 2);
			$waktu    = substr($date,11, 5);
			$hari    = date("w", strtotime($date));
			$result = $Hari[$hari].", ".$tgl." ".$Bulan[(int)$bulan-1]." ".$tahun." ".$waktu." ";
			return $result;
		}

		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->name = $list_tmuk['name'];			
			}
		}
		$from = $_GET['from'];
		$to = $_GET['to'];

		$data = new ProductMove();
		$lists = $data::Join_JualDetail4($from, $to);
		$lists_tmuk = TMUK::getAll();
		if(count($lists) < 1){
			$this->flashSession->success("Gagal;Tidak ada data.;error");
			return $this->response->redirect(uri('Laporan'));
		}

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->setTitle("PPOB(".date('d-m-Y', strtotime($_GET['from']))." - ".date('d-m-Y', strtotime($_GET['to'])).")");
		foreach(range('A1','N5') as $head)
		{
			$objPHPExcel->getActiveSheet()->getColumnDimension($head)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getStyle($head)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
		}
		$objPHPExcel->getActiveSheet()->getStyle('A1:N4')->getFont()->setBold(true);

		foreach (range('A', $objPHPExcel->getActiveSheet()->getHighestDataColumn()) as $col) {
			$objPHPExcel->getActiveSheet()
			->getColumnDimension($col)
			->setAutoSize(true);
		} 

		$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells('A1:N1')
		->mergeCells('A2:N2');
		$tgl = tgl_indonesia($from)." -  ".tgl_indonesia($to);

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A1', 'LAPORAN PENJUALAN PPOB TMUK '.$tmuk->name)
		->setCellValue('A2', "$tgl")
		->setCellValue('A4', 'Tanggal')
		->setCellValue('B4', 'Kode Barang')
		->setCellValue('C4', 'Barcode')
		->setCellValue('D4', 'Nama Barang')
		->setCellValue('E4', 'Harga Jual')
		->setCellValue('F4', 'Diskon')
		->setCellValue('G4', 'HPP')
		->setCellValue('H4', 'Qty')
		->setCellValue('I4', 'Margin (Rp)')
		->setCellValue('J4', 'Margin (%)')
		->setCellValue('K4', 'Sales Before PPN')
		->setCellValue('L4', 'PPN')
		->setCellValue('M4', 'Sales After PPN')
		->setCellValue('N4', 'Laba / Rugi');
		$i=5;
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		foreach ($lists as $key => $value) {
			$jual_detail = new JualDetail();
			$condition = " WHERE \"item_code\" = '".$value['item_code']."' AND \"id_jual\" = '".$value['reference']."' LIMIT 1";
			$aa = $jual_detail::getFreeSQL2($condition);

			$mpp = new ProductPrice();
			$condition = " WHERE \"stock_id\" = '".$value['item_code']."' ";
			$price = $mpp::getFreeSQL($condition);

			if($aa){
				$harga_hpp = $aa['0']['harga_hpp3'];
				$harga_jual = $aa['0']['harga'];
				$discount = $aa['0']['discount'];
				$harga_total = $aa['0']['total'];
			}else{
				$harga_hpp = $price['0']['average_cost'];
				if($price['0']['uom1_changed_price'] > 0){
					$harga_jual = $price['0']['uom1_changed_price'];
				}else{
					$harga_jual = $price['0']['uom1_rounding_price'];
				}
				$discount = 0;
				$harga_total =0;
			}

			if($discount > 0){
				$diskon = ($discount);
				$harga = ($harga_total / $value['qty']);
			}else{
				$diskon = 0;
				$harga = $harga_jual;
			}

			$hpp = $harga_hpp;
			$LR = ($harga_total) - ($hpp * $value['qty']);

			$slsbt = ($harga_total / 1.1);
			$slsat = ($harga_total);
			$tax = ($slsat - $slsbt);	
			$marginhp = ($harga - $hpp);
			$marginpersen = ($marginhp / $hpp * 100);

			$total += $LR;
			$totalSBT += $slsbt;
			$totalSAT += $slsat;
			$totalTax += $tax;
			$totalMarginh += $marginhp;
			$totalMarginp += $marginpersen;

			$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()
				->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
			$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()
				->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
			$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()
				->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
			$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->getNumberFormat()
				->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
			$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->getNumberFormat()
				->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
			$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->getNumberFormat()
				->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
			$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->getNumberFormat()
				->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
			$objPHPExcel->getActiveSheet()->getStyle('M'.$i)->getNumberFormat()
				->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
			$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->getNumberFormat()
				->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, date('d-m-Y', strtotime($value['tanggal'])))
			->setCellValue('B'.$i, $value['item_code'])
			->setCellValue('C'.$i, $value['barcode'])
			->setCellValue('D'.$i, $value['description'])
			->setCellValue('E'.$i, $harga_jual)
			->setCellValue('F'.$i, $diskon)
			->setCellValue('G'.$i, $hpp)
			->setCellValue('H'.$i, $value['qty'])
			->setCellValue('I'.$i, $marginhp)
			->setCellValue('J'.$i, $marginpersen)
			->setCellValue('K'.$i, $slsbt)
			->setCellValue('L'.$i, $tax)
			->setCellValue('M'.$i, $slsat)
			->setCellValue('N'.$i, $LR);
			$i++;
		}
		$a = "A".$i; // or any value
		$b = "G".$i; // or any value
		$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells("$a:$b");

		$objPHPExcel->getActiveSheet()->getStyle('H'.($i))->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('I'.($i))->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('J'.($i))->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('K'.($i))->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('L'.($i))->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('M'.($i))->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('N'.($i))->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getStyle('I'.($i))->getNumberFormat()
			->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
		$objPHPExcel->getActiveSheet()->getStyle('J'.($i))->getNumberFormat()
			->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
		$objPHPExcel->getActiveSheet()->getStyle('K'.($i))->getNumberFormat()
			->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
		$objPHPExcel->getActiveSheet()->getStyle('L'.($i))->getNumberFormat()
			->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
		$objPHPExcel->getActiveSheet()->getStyle('M'.($i))->getNumberFormat()
			->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
		$objPHPExcel->getActiveSheet()->getStyle('N'.($i))->getNumberFormat()
			->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('H'.($i), 'Total');

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('I'.($i), $totalMarginh)
		->setCellValue('J'.($i), $totalMarginp)
		->setCellValue('K'.($i), $totalSBT)
		->setCellValue('L'.($i), $totalTax)
		->setCellValue('M'.($i), $totalSAT)
		->setCellValue('N'.($i), $total);

		$objPHPExcel->getActiveSheet()->getStyle('A4:N'.($i))->applyFromArray($styleArray);
		unset($styleArray);
		
		$fname = "PPOB_Report(".date('d-m-Y', strtotime($_GET['from']))." - ".date('d-m-Y', strtotime($_GET['to'])).")" . ".xlsx";
		$temp_file = tempnam(sys_get_temp_dir(), 'phpexcel');

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($temp_file);

		$response = new \Phalcon\Http\Response();

		$response->setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		$response->setHeader('Content-Disposition', 'attachment;filename="' . $fname . '"');
		$response->setHeader('Cache-Control', 'max-age=0');

		$response->setHeader('Cache-Control', 'max-age=1');

		$response->setContent(file_get_contents($temp_file));

		unlink($temp_file);

		return $response;
	}

	public function Stock_0Action(){
		$this->AuthorityAction();
		function tgl_indonesia($date){
			$Hari = array ("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu",);
			$Bulan = array ("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
			$tahun     = substr($date, 0, 4);
			$bulan     = substr($date, 5, 2);
			$tgl    = substr($date, 8, 2);
			$waktu    = substr($date,11, 5);
			$hari    = date("w", strtotime($date));
			$result = $Hari[$hari].", ".$tgl." ".$Bulan[(int)$bulan-1]." ".$tahun." ".$waktu." ";
			return $result;
		}

		$from = $_GET['from'];
		$to = $_GET['to'];

		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->name = $list_tmuk['name'];			
			}
		}

		$data = new Product();
		$cond1 = " WHERE tran_date <= '".$_GET['to']."' AND status_flag !='2' ";
		$lists = $data::getJoin_ProductMove3($cond1);

		if(count($lists) < 1){
			$this->flashSession->success("Gagal;Tidak ada data.;error");
			return $this->response->redirect(uri('Laporan'));
		}

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->setTitle("Stock(".date('d-m-Y', strtotime($_GET['from']))." - ".date('d-m-Y', strtotime($_GET['to'])).")");

		foreach(range('A1','F2') as $head)
		{
			$objPHPExcel->getActiveSheet()->getStyle($head)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
		}


		foreach(range('A4','I4') as $set)
		{
			$objPHPExcel->getActiveSheet()->getStyle($set)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
			$objPHPExcel->getActiveSheet()->getColumnDimension($set)->setAutoSize(true);
		}

		$objPHPExcel->getActiveSheet()->getStyle('A1:I4')->getFont()->setBold(true);

		foreach (range('A', $objPHPExcel->getActiveSheet()->getHighestDataColumn()) as $col) {
			$objPHPExcel->getActiveSheet()
			->getColumnDimension($col)
			->setAutoSize(true);
		}

		$tgl = tgl_indonesia($from)." -  ".tgl_indonesia($to);

		$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells('A1:I1')
		->mergeCells('A2:I2')
		->setCellValue('A1', 'LAPORAN STOCK KOSONG TMUK '.$tmuk->name)
		->setCellValue('A2', "$tgl")
		->setCellValue('A4', 'Departemen')
		->setCellValue('B4', 'Kategori')
		->setCellValue('C4', 'Lotte / Non Lotte')
		->setCellValue('D4', 'Kode Barang')
		->setCellValue('E4', 'Barcode')
		->setCellValue('F4', 'Nama Barang')
		->setCellValue('G4', 'MAP')
		->setCellValue('H4', 'Kuantitas')
		->setCellValue('I4', 'Nilai');

		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		$i=5;
		foreach ($lists as $key => $value) {
			if($value['tipe'] == 001){
				$tipe = 'Trade Lotte';
			}
			if($value['tipe'] == 002){
				$tipe = 'Trade Non Lotte';
			}
			if($value['tipe'] == 003){
				$tipe = 'Non Trade Lotte';
			}
			if($value['tipe'] == 004){
				$tipe = 'Non Trade Non Lotte';
			}
			$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()
				->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');
			$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getNumberFormat()
				->setFormatCode('_(""* #,##0_);_(""* \(#,##0\);_(""* "-"??_);_(@_)');
			$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->getNumberFormat()
				->setFormatCode('_("Rp."* #,##0_);_("Rp."* \(#,##0\);_("Rp."* "-"??_);_(@_)');

			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, $value['bumun_nm'])
			->setCellValue('B'.$i, htmlspecialchars_decode($value['l1_nm']))
			->setCellValue('C'.$i, $tipe)
			->setCellValue('D'.$i, $value['stock_id'])
			->setCellValue('E'.$i, $value['uom_ib'])
			->setCellValue('F'.$i, htmlspecialchars_decode($value['description']))
			->setCellValue('G'.$i, $value['uom1_harga'])
			->setCellValue('H'.$i, $value['nilai_persediaan'])
			->setCellValue('I'.$i, ($value['uom1_harga'] * $value['nilai_persediaan']));
			$i++;
		}

		$objPHPExcel->getActiveSheet()->getStyle('A4:I'.($i-1))->applyFromArray($styleArray);
		unset($styleArray);
		$fname = "Stock_0_Report(".date('d-m-Y', strtotime($_GET['from']))." - ".date('d-m-Y', strtotime($_GET['to'])).")" . ".xlsx";
		$temp_file = tempnam(sys_get_temp_dir(), 'phpexcel');

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($temp_file);

		$response = new \Phalcon\Http\Response();

		$response->setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		$response->setHeader('Content-Disposition', 'attachment;filename="' . $fname . '"');
		$response->setHeader('Cache-Control', 'max-age=0');

		$response->setHeader('Cache-Control', 'max-age=1');

		$response->setContent(file_get_contents($temp_file));

		unlink($temp_file);

		return $response;
	}
	
	public function printPOAction(){ $this->AuthorityAction(); }
	public function printGRAction(){ $this->AuthorityAction(); }
	public function printReturAction(){ $this->AuthorityAction(); }
	public function printAdjusmentAction(){ $this->AuthorityAction(); }
	// public function printArusKasAction(){ $this->AuthorityAction();	}
	// public function printSummaryAction(){ $this->AuthorityAction(); }
	public function printStockAction(){ $this->AuthorityAction();
	}
	public function printStock0Action(){ $this->AuthorityAction(); }
	public function printSalesAction(){ $this->AuthorityAction(); }
	public function printKasirAction(){ $this->AuthorityAction(); }
	public function printReturSalesAction(){ $this->AuthorityAction(); }
	public function printPPOBAction(){ $this->AuthorityAction(); }
	public function printARAction(){ $this->AuthorityAction(); }
	
	public function excelPOAction(){ $this->AuthorityAction(); }
	public function excelGRAction(){ $this->AuthorityAction(); }
	public function excelReturAction(){ $this->AuthorityAction(); }
	public function excelAdjusmentAction(){ $this->AuthorityAction(); }
	public function excelArusKasAction(){ $this->AuthorityAction();	}
	// public function excelSummaryAction(){ $this->AuthorityAction(); }
	public function excelStockAction(){ $this->AuthorityAction(); }
	public function excelStock0Action(){ $this->AuthorityAction(); }
	public function excelSalesAction(){ $this->AuthorityAction(); }
	public function excelKasirAction(){ $this->AuthorityAction(); }
	public function excelReturSalesAction(){ $this->AuthorityAction(); }
	public function excelPPOBAction(){ $this->AuthorityAction(); }
	public function excelARAction(){ $this->AuthorityAction(); }
	
	public function reprintAction(){
		$cond = " WHERE tanggal = '".$_POST['tanggal']."' ";
		if($_POST['option'] === 'EOS'){
			$lists = EOS::getFreeSQL($cond);
			if(count($lists) <= 0){
				$this->flashSession->success("Gagal;Tidak ada data.;error");
				return $this->response->redirect(uri('Laporan'));
			}
			
			$listsTMUK = TMUK::getAll();
			if($listsTMUK){
				foreach($listsTMUK as $listtm){
					$tmuk_name = $listtm['name'];
					$tmuk_code = $listtm['tmuk'];
					$tmuk_address1 = $listtm['address1']; 
					$tmuk_address2 = $listtm['address2']; 
					$tmuk_address3 = $listtm['address3']; 
					$tmuk_telp = $listtm['telp']; 
					$owner = $listtm['owner']; 
					$length = $listtm['length_struk'];
				}
			}
			
			$length_3 = round(0.062 * $length);
			$length_4 = round(0.083 * $length);		
			$length_5 = round(0.104 * $length);
			$length_8 = round(0.166 * $length);
			$length_9 = round(0.187 * $length);		
			$length_10 = round(0.208 * $length);
			$length_12 = round(0.250 * $length);		
			$length_13 = round(0.270 * $length);		
			$length_14 = round(0.291 * $length);		
			$length_16 = round(0.333 * $length);		
			$length_18 = round(0.375 * $length);		
			$length_19 = round(0.395 * $length);
			$length_20 = round(0.416 * $length);
			$length_22 = round(0.458 * $length);
			$length_24 = round(0.500 * $length);
			$length_25 = round(0.520 * $length);
			$length_30 = round(0.625 * $length);
			$length_32 = round(0.666 * $length);
			$length_48 = round(1 * $length);
			
			foreach($lists as $list){
				$device = "COM2";
				// $device = "test.txt";
				exec("mode $device BAUD=38400 PARITY=n DATA=8 STOP=1 xon=off octs=off rts=on to=on");
				$comport = fopen($device, "w");
				
				$str = print_center('END SHIFT'); $str = substr($str, 0, $length); fputs($comport, $str);
			
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
				$str = substr($str, 0, $length); fputs($comport, $str);
						
				$str = print_left('Shift       : '.$list['shift'], $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_left('Kasir       : '.$list['kassa'], $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_left('Store Code  : '.$tmuk_code, $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_left('Store Name  : '.$tmuk_name, $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_left('Date        : '.date('d-m-Y', strtotime($list['tanggal'])), $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
				$str = substr($str, 0, $length); fputs($comport, $str);

				$str = print_left('SALES', $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= '-'; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = print_right('Initial', $length_32).print_right(number_format($list['initial'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_right('Cash Income', $length_32).print_right(number_format($list['cash_income'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_right('Net Sales', $length_32).print_right(number_format($list['net_sales'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_right('Net Return', $length_32).print_right(number_format('0',0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_right('PPN', $length_32).print_right(number_format('0',0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= '-'; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = print_right('Total Sales', $length_32).print_right(number_format($list['net_sales'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= '='; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = print_left('ACTUAL', $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= '-'; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = print_right('Cash', $length_32).print_right(number_format($list['net_sales'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);					// net sales
				$str = print_right('Cash Income', $length_32).print_right(number_format($list['cash_income'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_right('Donasi', $length_32).print_right(number_format($list['donasi'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_right('Kredit', $length_32).print_right(number_format($list['kredit'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_right('Debit', $length_32).print_right(number_format($list['debit'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_right('Voucher', $length_32).print_right(number_format($list['voucher'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= '-'; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = print_right('Total Actual', $length_32).print_right(number_format((($list['total_actual']) + $list['kredit'] + $list['debit'] + $list['voucher']),0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);	
				$str = print_right('Variance', $length_32).print_right(number_format(($list['variance']),0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);	  	
				$str = print_right('Variance Paid', $length_32).print_right(number_format('0',0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);	
				$str = print_right('Receipt Count', $length_32).print_right(number_format($list['rec_count'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);	
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= '='; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = print_center('Mengetahui Store Leader'); $str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = print_center('(     '.$owner.'     )'); $str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= '-'; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = print_center('Print Date : '.date('d-m-Y H:i:s')); $str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = "\x00\x1Bi\x00"; fputs($comport, $str);
				
				fclose($comport);	
			}
				
		}else if($_POST['option'] === 'EOD'){			
			$lists = EOD::getFreeSQL($cond);			
			
			if(empty($lists[0]['tanggal'])){
				$this->flashSession->success("Gagal;Tidak ada data.;error");
				return $this->response->redirect(uri('Laporan'));
			}
			
			$listsTMUK = TMUK::getAll();
			
			if($listsTMUK){
				foreach($listsTMUK as $listtm){
					$tmuk_name = $listtm['name'];
					$tmuk_code = $listtm['tmuk'];
					$tmuk_address1 = $listtm['address1']; 
					$tmuk_address2 = $listtm['address2']; 
					$tmuk_address3 = $listtm['address3']; 
					$tmuk_telp = $listtm['telp']; 
					$owner = $listtm['owner']; 
					$length = $listtm['length_struk'];
				}
			}
			
			$length_3 = round(0.062 * $length);
			$length_4 = round(0.083 * $length);		
			$length_5 = round(0.104 * $length);
			$length_8 = round(0.166 * $length);
			$length_9 = round(0.187 * $length);		
			$length_10 = round(0.208 * $length);
			$length_12 = round(0.250 * $length);		
			$length_13 = round(0.270 * $length);		
			$length_14 = round(0.291 * $length);		
			$length_16 = round(0.333 * $length);		
			$length_18 = round(0.375 * $length);		
			$length_19 = round(0.395 * $length);
			$length_20 = round(0.416 * $length);
			$length_24 = round(0.500 * $length);
			$length_25 = round(0.520 * $length);
			$length_30 = round(0.625 * $length);
			$length_32 = round(0.666 * $length);
			$length_48 = round(1 * $length);
			
			foreach($lists as $list){				
				$device = "COM2";
				// $device = "SOMETHING.txt";
				exec("mode $device BAUD=38400 PARITY=n DATA=8 STOP=1 xon=off octs=off rts=on to=on");
				$comport = fopen($device, "w");
				
				$str = print_center('END OF DAY'); $str = substr($str, 0, $length); fputs($comport, $str);
			
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
				$str = substr($str, 0, $length); fputs($comport, $str);
	
				$str = print_left('Store Code  : '.$tmuk_code, $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_left('Store Name  : '.$tmuk_name, $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_left('Date        : '.date('d-m-Y', strtotime($list['tanggal'])), $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
				$str = substr($str, 0, $length); fputs($comport, $str);

				$str = print_left('SALES', $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= '-'; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = print_right('Initial', $length_32).print_right(number_format($list['initial'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_right('Cash Income', $length_32).print_right(number_format($list['cash_income'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_right('Net Sales', $length_32).print_right(number_format($list['net_sales'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_right('Net Return', $length_32).print_right(number_format('0',0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_right('PPN', $length_32).print_right(number_format('0',0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= '-'; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = print_right('Total Sales', $length_32).print_right(number_format($list['net_sales'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= '='; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = print_left('ACTUAL', $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= '-'; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = print_right('Cash', $length_32).print_right(number_format($list['net_sales'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);					// net sales
				$str = print_right('Cash Income', $length_32).print_right(number_format($list['cash_income'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_right('Donasi', $length_32).print_right(number_format($list['donasi'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_right('Kredit', $length_32).print_right(number_format($list['kredit'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_right('Debit', $length_32).print_right(number_format($list['debit'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				$str = print_right('Voucher', $length_32).print_right(number_format($list['voucher'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= '-'; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = print_right('Total Actual', $length_32).print_right(number_format((($list['total_actual']) + $list['kredit'] + $list['debit'] + $list['voucher']),0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);	
				$str = print_right('Variance', $length_32).print_right(number_format(($list['variance']),0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);	  	
				$str = print_right('Variance Paid', $length_32).print_right(number_format('0',0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);	
				$str = print_right('Receipt Count', $length_32).print_right(number_format($list['rec_count'],0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);	
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= '='; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = print_center('Mengetahui Store Leader'); $str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = print_center('(     '.$owner.'     )'); $str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<$length;$i++){ $str .= '-'; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = print_center('Print Date : '.date('d-m-Y H:i:s')); $str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = ''; 
				for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
				$str = substr($str, 0, $length); fputs($comport, $str);
				
				$str = "\x00\x1Bi\x00"; fputs($comport, $str);
				
				fclose($comport);		
			}
		}
	}
}
