<?php

use Phalcon\Mvc\Controller;

class ReturBarangController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'ReturBarang' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$gr = new GR();
		$condition_gr = " WHERE ( \"retur_doc_immpos\" IS NULL OR retur_doc_immpos = '' ) AND \"flag_sync\" = true AND create_date > (current_timestamp - interval '24 hours') ";
		$lists_gr = $gr::getFreeSQL($condition_gr);
		
		$range_po_no = '';
		$count = 0;
		if(isset($lists_gr)){
			foreach($lists_gr as $list_gr){
				if(isset($list_gr['po_no_immbo']) && $list_gr['po_no_immbo'] != ''){
					if($count > 0) { $range_po_no .= ','; }
					$range_po_no .= "'".$list_gr['po_no_immbo']."'";
					$count++;
				}
			}
		}
		
		$range_retur_no = '';
		$count = 0;
		if(isset($lists_gr)){
			foreach($lists_gr as $list_gr){
				if(isset($list_gr['retur_no_immpos']) && $list_gr['retur_no_immpos'] != ''){
					if($count > 0) { $range_retur_no .= ','; }
					$range_retur_no .= "'".$list_gr['retur_no_immpos']."'";
					$count++;
				}
			}
		}
		
		$list_po_detail = null;
		if($range_po_no != ''){
			$po_detail = new PODetail();
			$condition = " WHERE \"po_no_immbo\" IN (".$range_po_no.") AND \"flag_gr_ok\" = false ";
			$list_po_detail = $po_detail::getFreeSQL($condition);
		}
		
		$list_retur_detail = null;
		if($range_retur_no != ''){
			$retur_detail = new ReturDetail();
			$condition = " WHERE \"retur_no\" IN (".$range_retur_no.") AND \"flag_gr_ok\" = false  ";
			$list_retur_detail = $retur_detail::getFreeSQL($condition);
		}
		
		$list_data = null;
		$count = 0;		
		foreach($lists_gr as $list_gr){
			$flag = false;
			if($list_gr['po_no_immbo'] != ''){
				if(isset($list_po_detail)){				
					foreach($list_po_detail as $line_po){
						if($list_gr['po_no_immbo'] == $line_po['po_no_immbo']) { $flag = true; }
					}
				}
			}
			
			if($list_gr['retur_no_immpos'] != ''){
				if(isset($list_retur_detail)){				
					foreach($list_retur_detail as $line_retur){
						if($list_gr['retur_no_immpos'] == $line_retur['retur_no']) { $flag = true; }
					}
				}
			}
			
			if($flag){
				$list_data[$count] = $list_gr;
				$count++;
			}
		}
		$this->view->lists = $list_data	;	
		
		$retur = new Retur();
		$condition_retur = " WHERE \"flag_sync\" = false  AND \"flag_delete\" = false";
		$this->view->lists_sync = $retur::getFreeSQL($condition_retur);
		$this->view->listsHistory = $retur::getAll();
		
		$jual = new Jual();
		$condition = " WHERE \"flag_delete\" = false AND \"flag_sum\" = false ";
		$this->view->lists_jual = $jual::getFreeSql($condition);
	}
	
	public function addAction()
	{		
		$this->AuthorityAction();
		
		$gr = new GR();
		$gr->gr_no = $_REQUEST['GR_NO'];
		$lists_gr = $gr::getFirst($gr);
		
		$this->view->data_gr = new GR();
		if(count($lists_gr) > 0){
			foreach($lists_gr as $list_gr){
				$this->view->data_gr->gr_no = $list_gr['gr_no'];
				$this->view->data_gr->create_by = $list_gr['create_by'];
				$this->view->data_gr->create_date = $list_gr['create_date'];
				$this->view->data_gr->delivery_date = $list_gr['delivery_date'];
				$this->view->data_gr->comments = $list_gr['comments'];
				$this->view->data_gr->pr_no_immpos = $list_gr['pr_no_immpos'];
				$this->view->data_gr->pr_no_immbo = $list_gr['pr_no_immbo'];
				$this->view->data_gr->po_no_immbo = $list_gr['po_no_immbo'];
				$this->view->data_gr->retur_no_immpos = $list_gr['retur_no_immpos'];
				$this->view->data_gr->retur_no_immbo = $list_gr['retur_no_immbo'];
				$this->view->data_gr->total_pr = $list_gr['total_pr'];
				$this->view->data_gr->total_po = $list_gr['total_po'];
				$this->view->data_gr->retur_doc_immpos = $list_gr['retur_doc_immpos'];
				$this->view->data_gr->retur_doc_immbo = $list_gr['retur_doc_immbo'];
				$this->view->data_gr->total_retur = $list_gr['total_retur'];
			}			
		}
		
		$po = new PO();
		$po->po_no_immbo = $_REQUEST['PO_NO_immbo'];
		if($po->po_no_immbo != ''){
			$lists_po = $po::getFirst($po);
		}
		
		$this->view->data_po = new PO();
		if(count($lists_po) > 0){
			foreach($lists_po as $list_po){
				$this->view->data_po->po_no_immbo = $list_po['po_no_immbo'];
				$this->view->data_po->po_no_ref = $list_po['po_no_ref'];
				$this->view->data_po->reference = $list_po['reference'];
				$this->view->data_po->supplier_id = $list_po['supplier_id'];
				$this->view->data_po->order_date = $list_po['order_date'];
				$this->view->data_po->delivery_date = $list_po['delivery_date'];
				$this->view->data_po->delivery_address = $list_po['delivery_address'];
				$this->view->data_po->comments = $list_po['comments'];
				$this->view->data_po->pr_no_immpos = $list_po['pr_no_immpos'];
				$this->view->data_po->pr_no_immbo = $list_po['pr_no_immbo'];
				$this->view->data_po->gr_no_immpos = $list_po['gr_no_immpos'];
				$this->view->data_po->total_pr = $list_po['total_pr'];
				$this->view->data_po->total_po = $list_po['total_po'];
				$this->view->data_po->flag_sync = $list_po['flag_sync'];
			}
		}
		
		$po_detail = new PODetail();
		$this->view->lists_po_detail = null;
		$po_detail->po_no_immbo = $_REQUEST['PO_NO_immbo'];
		if($po_detail->po_no_immbo != ''){
			$condition = " WHERE \"po_no_immbo\" ='".$po_detail->po_no_immbo."' AND \"flag_gr_ok\" = false ";
			$lists_po_detail = $po_detail::getFreeSQL($condition);

			$this->view->lists_po_detail = $lists_po_detail;
		}
		
		$retur = new Retur();
		$retur->retur_no = $_REQUEST['Retur_No'];
		if($retur->retur_no != ''){
			$lists_retur = $retur::getFirst($retur);
		}
		
		$this->view->data_retur = new Retur();
		if(count($lists_retur) > 0){
			foreach($lists_retur as $list_retur){
				$this->view->data_retur->retur_no = $list_retur['retur_no'];
				$this->view->data_retur->retur_type = $list_retur['retur_type'];
				$this->view->data_retur->reference = $list_retur['reference'];
				$this->view->data_retur->supplier_id = $list_retur['supplier_id'];
				$this->view->data_retur->create_date = $list_retur['create_date'];
				$this->view->data_retur->delivery_date = $list_retur['delivery_date'];
				$this->view->data_retur->delivery_address = $list_retur['delivery_address'];
				$this->view->data_retur->comments = $list_retur['comments'];
				$this->view->data_retur->gr_no_immpos = $list_retur['gr_no_immpos'];
				$this->view->data_retur->retur_no_immbo = $list_retur['retur_no_immbo'];
				$this->view->data_retur->total_gr = $list_retur['total_gr'];
				$this->view->data_retur->total_retur = $list_retur['total_retur'];
				$this->view->data_retur->tax_included = $list_retur['tax_included'];
				$this->view->data_retur->flag_sync = $list_retur['flag_sync'];
				$this->view->data_retur->gr_no_immbo = $list_retur['gr_no_immbo'];
				$this->view->data_retur->gr_retur_immpos = $list_retur['gr_retur_immpos'];
				$this->view->data_retur->gr_retur_immbo = $list_retur['gr_retur_immbo'];
			}
		}	
		
		$retur_detail = new ReturDetail();
		$this->view->lists_retur_detail = null;
		$retur_detail->retur_no = $_REQUEST['Retur_No'];
		if($retur_detail->retur_no != ''){
			$condition = " WHERE \"retur_no\" ='".$retur_detail->retur_no."' AND \"flag_gr_ok\" = false ";
			$lists_retur_detail = $retur_detail::getFreeSQL($condition);

			$this->view->lists_retur_detail = $lists_retur_detail;
		}		
	}
	
	public function editAction()
	{	
		$this->AuthorityAction();
	
		$retur = new Retur();
		$retur-> retur_no = $_GET['Retur_No'];
		$lists_retur = $retur::getFirst($retur);
		
		print_r($lists_retur);
		
		$this->view->data_retur = new Retur();
		if(count($lists_retur) > 0){
			foreach($lists_retur as $list_retur){
				$this->view->data_retur->retur_no = $list_retur['retur_no'];
				$this->view->data_retur->retur_type = $list_retur['retur_type'];
				$this->view->data_retur->reference = $list_retur['reference'];
				$this->view->data_retur->supplier_id = $list_retur['supplier_id'];
				$this->view->data_retur->create_date = $list_retur['create_date'];
				$this->view->data_retur->delivery_date = $list_retur['delivery_date'];
				$this->view->data_retur->delivery_address = $list_retur['delivery_address'];
				$this->view->data_retur->comments = $list_retur['comments'];
				$this->view->data_retur->gr_no_immpos = $list_retur['gr_no_immpos'];
				$this->view->data_retur->retur_no_immbo = $list_retur['retur_no_immbo'];
				$this->view->data_retur->total_gr = $list_retur['total_gr'];
				$this->view->data_retur->total_retur = $list_retur['total_retur'];
				$this->view->data_retur->tax_included = $list_retur['tax_included'];
				$this->view->data_retur->flag_sync = $list_retur['flag_sync'];
				$this->view->data_retur->gr_no_immbo = $list_retur['gr_no_immbo'];
				$this->view->data_retur->gr_retur_immpos = $list_retur['gr_retur_immpos'];
				$this->view->data_retur->gr_retur_immbo = $list_retur['gr_retur_immbo'];
			}
		}
		
		$retur_detail = new ReturDetail();
		$this->view->lists_retur_detail = null;
		$retur_detail->retur_no = $_GET['Retur_No'];
		
		echo $retur_detail->retur_no;
		
		$retur_detail = new ReturDetail();
		$this->view->lists_retur_detail = null;
		$retur_detail->retur_no = $_REQUEST['Retur_No'];
		if($retur_detail->retur_no != ''){
			$condition = " WHERE \"retur_no\" ='".$retur_detail->retur_no."' AND \"flag_gr_ok\" = false ";
			$lists_retur_detail = $retur_detail::getFreeSQL($condition);

			$this->view->lists_retur_detail = $lists_retur_detail;
		}
	}
	
	public function ajaxPOAction()
	{
		$data = array();	
		$condition = " WHERE po.po_no_immbo = '".$_GET['order']."' ";
		$data = PO::getJoin($condition);

		$count = 0;
		for($i=0; $i<count($data); $i++){
			$data[$i]['qty_pr'];
			$data[$i]['unit_po'];
		}
		
		return json_encode($data);
	}
	
	public function ajaxPRAction()
	{
		$pr_detail = new PRDetail();
		$condition = " WHERE \"request_no\" = '".$_GET['request']."' ";
		$data = $pr_detail::getFreeSQL($condition);
		
		return json_encode($data);
	}
	
	public function ajaxGRAction(){
		$gr = new GR();
		$gr->gr_no = $_GET['gr'];
		$lists_gr = $gr::getFirst($gr);
		
		if(count($lists_gr) > 0){
			foreach($lists_gr as $list_gr){
				$gr->gr_no = $list_gr['gr_no'];
				$gr->create_by = $list_gr['create_by'];
				$gr->create_date = $list_gr['create_date'];
				$gr->delivery_date = $list_gr['delivery_date'];
				$gr->comments = $list_gr['comments'];
				$gr->pr_no_immpos = $list_gr['pr_no_immpos'];
				$gr->pr_no_immbo = $list_gr['pr_no_immbo'];
				$gr->po_no_immbo = $list_gr['po_no_immbo'];
				$gr->retur_no_immpos = $list_gr['retur_no_immpos'];
				$gr->retur_no_immbo = $list_gr['retur_no_immbo'];
				$gr->total_pr = $list_gr['total_pr'];
				$gr->total_po = $list_gr['total_po'];
				$gr->retur_doc_immpos = $list_gr['retur_doc_immpos'];
				$gr->retur_doc_immbo = $list_gr['retur_doc_immbo'];
				$gr->total_retur = $list_gr['total_retur'];
			}			
		}
		
		$data = null;
		
		// GR PO/PR
		if($gr->po_no_immbo != '' && $gr->po_no_immbo != null){
			// $po_detail = new PODetail();
			// $condition = " WHERE \"po_no_immbo\" = '".$gr->po_no_immbo."' ";
			// $data = $po_detail::getFreeSQL($condition);

			$data = array();	
			$condition = " WHERE po.po_no_immbo = '".$gr->po_no_immbo."' ";
			$data = PO::getJoin($condition);
		}
		
		if($gr->pr_no_immpos != '' && $gr->pr_no_immpos != null){
			$pr_detail = new PRDetail();
			$condition = " WHERE \"request_no\" = '".$gr->pr_no_immpos."' ";
			$data_pr = $pr_detail::getFreeSQL($condition);
		}
		
		for($i=0; $i<count($data); $i++){
			$data[$i]['qty_pr'];
			$data[$i]['unit_pr'];
			$data[$i]['unit_po'];
			$data[$i]['qty_retur'] = '0';
			$data[$i]['unit_retur'] = '';
		}
		
		// GR Retur
		if($gr->retur_no_immpos != '' && $gr->retur_no_immpos != null){
			$retur_detail = new ReturDetail();
			$condition = " WHERE \"retur_no\" = '".$gr->retur_no_immpos."' ";
			$data = $retur_detail::getFreeSQL($condition);
			
			for($i=0; $i<count($data); $i++){
				$data[$i]['qty_pr'] = '0';
				$data[$i]['unit_pr'] = '';
				$data[$i]['qty_po'] = '0';
				$data[$i]['unit_po'] = '';
				$data[$i]['po_unit_price'] = $data[$i]['retur_unit_price'];
				$data[$i]['po_price'] = '0';
			}
		}				
		
		return json_encode($data);
	}
	
	public function ajaxReturAction(){
		$retur_detail = new ReturDetail();
		$condition = " WHERE \"retur_no\" = '".$_GET['retur']."' ";
		$data = $retur_detail::getFreeSQL($condition);
		
		return json_encode($data);
	}
	
	public function ajaxJualAction()
	{
		$jual_detail = new JualDetail();
		$condition = " WHERE \"id_jual\" = '".$_GET['hold']."' ";
		$data = $jual_detail::getFreeSQL($condition);

		return json_encode($data);
	}
	
	public function insertAction(){
		$number_range = new NumberRange();
		$number_range->trans_type = 'RR';
		$number_range->period = date('Ym');
		$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
		$lists = $number_range::getFreeSQL($condition);
		
		if(isset($lists)){
			foreach($lists as $list){
				$number_range->trans_type = $list['trans_type'];
				$number_range->period = $list['period'];
				$number_range->prefix = $list['prefix'];
				$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
			}
		}else{
			$number_range->trans_type = 'RR';
			$number_range->period = date('Ym');
			$number_range->prefix = 'RR';
			$number_range->from_ = '1';
			$number_range->to_ = '9999';
			$number_range->current_no = '1';
			$success = $number_range::goInsert($number_range);			
			$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
		}
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
			}
		}
		
		$retur = new Retur();
		$retur->retur_no = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;	
		$retur->retur_type = $this->request->getPost("retur_type");
		$retur->reference = $this->request->getPost("reference");		
		$retur->supplier_id = $this->request->getPost("supplier_id");	
		$retur->delivery_date = $this->request->getPost("delivery_date");
		$retur->comments = $this->request->getPost("comments");
		$retur->gr_no_immpos = $this->request->getPost("gr_no");
		$retur->total_gr = $this->request->getPost("total_gr");
		$success = $retur::goInsert($retur);
		
		if($success){
			$success_num = $number_range::goAddNumber($number_range);
			
			$gr = new GR();
			$gr->retur_doc_immpos = $retur->retur_no;
			$gr->gr_no = $retur->gr_no_immpos;
			$success = $gr::goUpdate($gr);			
			
			$json_datas = json_decode($_POST['json_data']);			
			foreach($json_datas as $json_data){
				$produk = new Product();
				$condition = " WHERE \"stock_id\" = '".$json_data[1]."' ";
				$data_product = $produk::getFreeSQL($condition);

				$retur_detail = new ReturDetail();
				$retur_detail->retur_no = $retur->retur_no;
				$retur_detail->item_code = $json_data[1];
				$retur_detail->description = str_ireplace("'", "", $json_data[2]);
				foreach ($data_product as $key => $value) {
					$retur_detail->qty_retur = $json_data[5] / $value['uom2_conversion'];
				}
				$retur_detail->unit_retur = $json_data[6];
				$retur_detail->retur_unit_price = $json_data[7];
				$retur_detail->retur_price = $json_data[8];
				$retur_detail->gr_detail_item = $json_data[0];
				$retur_detail->note = str_replace('%20',' ',$json_data[9]);				
				$retur_detail->comments = str_replace('%20',' ',$json_data[10]);		
				$success = $retur_detail::goInsert($retur_detail);
			}				
			
			$data['type'] = 'S';
			$data['message'] = 'Retur berhasil Dibuat.<br/>Nomor Retur : <b>'.$retur->retur_no.'</b>';
			$notif = new Notifikasi();
			$notif->data = $data['message'];
			$notif->read_at = null;
			$notif->created_at = date('Y-m-d H:i:s');
			$notif->created_by = $this->session->get("user")['user_name'];

			$sukses = $notif::goInsert($notif);
			return json_encode($data);
		}else{						
			$data['type'] = 'E';
			$data['message'] = 'Gagal membuat Retur!';
			return json_encode($data);
		}
	}
	
	public function updateAction()
	{			
		$retur = new Retur();			
		$retur->retur_no = $this->request->getPost("retur_no");	
		$retur->retur_type = $this->request->getPost("retur_type");
		$retur->reference = $this->request->getPost("reference");		
		$retur->supplier_id = $this->request->getPost("supplier_id");	
		$retur->delivery_date = $this->request->getPost("delivery_date");
		$retur->comments = $this->request->getPost("comments");
		$retur->gr_no_immpos = $this->request->getPost("gr_no");
		$retur->total_gr = $this->request->getPost("total_gr");
		$success = $retur::goUpdate($retur);
		
		if($success){					
			$json_datas = json_decode($_POST['json_data']);	
			
			foreach($json_datas as $json_data){
				$retur_detail = new ReturDetail();
				$retur_detail->retur_no = $retur->retur_no;	
				$retur_detail->retur_detail_item = $json_data[0];
				$retur_detail->qty_retur = $json_data[5];
				$retur_detail->note = str_replace('%20',' ',$json_data[9]);
				$retur_detail->comments = str_replace('%20',' ',$json_data[10]);	
				$success = $retur_detail::goUpdate($retur_detail);
			}
			
			$data['type'] = 'S';
			$data['message'] = '<b>'.$retur->retur_no.'</b> Berhasil diubah!';
			return json_encode($data);
		}else{						
			$data['type'] = 'E';
			$data['message'] = '<b>'.$retur->retur_no.'</b> Gagal diubah!';
			return json_encode($data);
		}
	}
	
	public function deleteAction()
	{			
		$this->AuthorityAction();
		
		$retur = new Retur();
		$retur-> retur_no = $_GET['Retur_No'];
		$lists_retur = $retur::getFirst($retur);
		
		$this->view->data_retur = new Retur();
		if(count($lists_retur) > 0){
			foreach($lists_retur as $list_retur){
				$retur->retur_no = $list_retur['retur_no'];
				$retur->retur_type = $list_retur['retur_type'];
				$retur->reference = $list_retur['reference'];
				$retur->supplier_id = $list_retur['supplier_id'];
				$retur->create_date = $list_retur['create_date'];
				$retur->delivery_date = $list_retur['delivery_date'];
				$retur->delivery_address = $list_retur['delivery_address'];
				$retur->comments = $list_retur['comments'];
				$retur->gr_no_immpos = $list_retur['gr_no_immpos'];
				$retur->retur_no_immbo = $list_retur['retur_no_immbo'];
				$retur->total_gr = $list_retur['total_gr'];
				$retur->total_retur = $list_retur['total_retur'];
				$retur->tax_included = $list_retur['tax_included'];
				$retur->flag_sync = $list_retur['flag_sync'];
				$retur->gr_no_immbo = $list_retur['gr_no_immbo'];
				$retur->gr_retur_immpos = $list_retur['gr_retur_immpos'];
				$retur->gr_retur_immbo = $list_retur['gr_retur_immbo'];
			}
		}
		
		$retur_update = new Retur();			
		$retur_update->retur_no = $retur->retur_no;	
		$retur_update->flag_delete = 't';
		$success = $retur_update::goUpdate($retur_update);
		
		if($success){
			if($retur->gr_no_immpos != ''){
				$gr = new GR();
				$gr->gr_no = $retur->gr_no_immpos;
				$gr->retur_doc_immpos = 'X';			
				$success = $gr::goUpdateClear($gr);			
				
				$json_datas = json_decode($_POST['json_data']);			
				foreach($json_datas as $json_data){
					$retur_detail = new ReturDetail();
					$retur_detail->retur_no = $retur->retur_no;
					$retur_detail->flag_gr_ok = 'f';				
					$success = $retur_detail::goUpdateRetur($retur_detail);
				}
			}
		}
		
		$this->response->redirect(uri('ReturBarang'));		
	}
	
	public function syncAction()
	{	
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = '<b>Mohon Periksa Kembali Koneksi Anda !!!</b>';

			return json_encode($data);
		}else{
			
			$tmuk = new TMUK();
			$lists_tmuk = $tmuk::getALL();
			
			if(isset($lists_tmuk)){
				foreach($lists_tmuk as $list_tmuk){
					$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
					$tmuk->tmuk = $list_tmuk['tmuk'];
					$tmuk->supplier_id = $list_tmuk['supplier_id'];
				}
			}
			
			$retur = new Retur();
			$retur->retur_no = $_REQUEST['Retur_No'];
			$lists_h = $retur::getFirst($retur);
			
			if(isset($lists_h)){
				foreach($lists_h as $list_h){				
					$retur->retur_no = $list_h['retur_no'];
					$retur->retur_type = $list_h['retur_type'];
					$retur->reference = $list_h['reference'];
					$retur->supplier_id = $list_h['supplier_id'];
					$retur->create_date = $list_h['create_date'];
					$retur->delivery_date = $list_h['delivery_date'];
					$retur->delivery_address = $list_h['delivery_address'];
					$retur->comments = $list_h['comments'];
					$retur->gr_no_immpos = $list_h['gr_no_immpos'];
					$retur->retur_no_immbo = $list_h['retur_no_immbo'];
					$retur->total_gr = $list_h['total_gr'];
					$retur->total_retur = $list_h['total_retur'];
					$retur->tax_included = $list_h['tax_included'];
					$retur->flag_sync = $list_h['flag_sync'];
				}
			}
			
			$retur_detail = new ReturDetail();
			$retur_detail->retur_no = $_REQUEST['Retur_No'];
			$condition = " WHERE \"retur_no\" ='".$retur_detail->retur_no."' " ;
			$lists_d = $retur_detail::getJoin_Product($condition);						
			
			$gr = new GR();
			$gr->gr_no = $retur->gr_no_immpos;
			$lists_gr = $gr::getFirst($gr);
			
			if(isset($lists_gr)){
				foreach($lists_gr as $list_gr){
					$gr->gr_no = $list_gr['gr_no'];
					$gr->create_by = $list_gr['create_by'];
					$gr->create_date = $list_gr['create_date'];
					$gr->delivery_date = $list_gr['delivery_date'];
					$gr->comments = $list_gr['comments'];
					$gr->pr_no_immpos = $list_gr['pr_no_immpos'];
					$gr->pr_no_immbo = $list_gr['pr_no_immbo'];
					$gr->po_no_immbo = $list_gr['po_no_immbo'];
					$gr->retur_no_immpos = $list_gr['retur_no_immpos'];
					$gr->retur_no_immbo = $list_gr['retur_no_immbo'];
					$gr->total_pr = $list_gr['total_pr'];
					$gr->total_po = $list_gr['total_po'];
					$gr->retur_doc_immpos = $list_gr['retur_doc_immpos'];
					$gr->retur_doc_immbo = $list_gr['retur_doc_immbo'];
					$gr->total_retur = $list_gr['total_retur'];
				}			
			}
			
			// fill sync param
			if(isset($gr) && $gr->po_no_immbo != ''){
				$data = null;
				$data['tmuk'] = $tmuk->tmuk;
				$data['nomorpo'] = $gr->po_no_immbo;
				$data['no_retur'] = $retur->retur_no;
				$prods = [];
				if(isset($lists_d)){
					foreach($lists_d as $list_detail){
						if($list_detail['note'] !== 'Barang tidak sesuai (ditukar)'){
							$xxx['id'] = $list_detail['item_code'];
							$xxx['qty'] = $list_detail['qty_retur'];
							$xxx['note'] = $list_detail['note'];
							$xxx['reason'] = $v['comments'];
							$xxx['retur_unit_price'] = $list_detail['retur_unit_price'];
							$xxx['retur_price'] = $list_detail['retur_price'];
							$prods[] = $xxx;
						}
					}
				}
				$data['detail'] = $prods;
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/retur/send-retur", [
					'list' => $data
				]);
				$obj = json_decode($result, true);
				if($obj['status'] == 'success'){
					$retur_update = new Retur();
					$retur_update->retur_no = $_REQUEST['Retur_No'];
					$retur_update->retur_no_immbo = $retur->retur_no;
					$retur_update->flag_sync = 't';
					$success = $retur_update::goUpdate($retur_update);
					
					$gr_update = new GR();
					$gr_update->gr_no = $retur->gr_no_immpos;
					$gr_update->retur_doc_immbo = $retur->retur_no;			
					$success = $gr_update::goUpdate($gr_update);
					
					$number_move = new NumberRange();
					$number_move->trans_type = 'MV';
					$number_move->period = date('Ym');
					$condition = " WHERE \"trans_type\" = '".$number_move->trans_type."' AND \"period\" = '".$number_move->period."' ";
					$lists = $number_move::getFreeSQL($condition);
					
					if(isset($lists)){
						foreach($lists as $list){
							$number_move->trans_type = $list['trans_type'];
							$number_move->period = $list['period'];
							$number_move->prefix = $list['prefix'];
							$number_move->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
						}
					}else{
						$number_move->trans_type = 'MV';
						$number_move->period = date('Ym');
						$number_move->prefix = 'MV';
						$number_move->from_ = '1';
						$number_move->to_ = '9999';
						$number_move->current_no = '1';
						$success = $number_move::goInsert($number_move);			
						$number_move->current_no = str_pad($number_move->current_no, 4, "0", STR_PAD_LEFT);
					}
					
					// if(isset($lists_d)){
					// 	foreach($lists_d as $list_d){					
					// 		$product_move = new ProductMove();
					// 		$product_move->trans_no = $number_move->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_move->current_no;	
					// 		$product_move->stock_id = $list_d['item_code'];
					// 		$product_move->type = 'RR';
					// 		$product_move->price = $list_d['retur_price'];
					// 		$product_move->reference = $retur->retur_no;
					// 		$product_move->qty = ($list_d['qty_retur'] * $list_d['konversi'] * -1);
					// 		$success = $product_move::goInsert($product_move);
					// 	}
					// }
					
					// $success_num = $number_move::goAddNumber($number_move);
					$data['type'] = 'S';
					$data['message'] = 'Sukses Sync <b>'.$retur->retur_no.'</b>';
					$notif = new Notifikasi();
					$notif->data = $data['message'];
					$notif->read_at = null;
					$notif->created_at = date('Y-m-d H:i:s');
					$notif->created_by = $this->session->get("user")['user_name'];

					$sukses = $notif::goInsert($notif);
					return json_encode($data);
				}else{
					$data['type'] = 'E';
					$data['message'] = 'Gagal Sync <b>'.$retur->retur_no.'</b>';
					$notif = new Notifikasi();
					$notif->data = $data['message'];
					$notif->read_at = null;
					$notif->created_at = date('Y-m-d H:i:s');
					$notif->created_by = $this->session->get("user")['user_name'];

					$sukses = $notif::goInsert($notif);
					return json_encode($data);
				}
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Gagal Sync';
				return json_encode($data);
			}
		}
	}
	
	public function printAction()
	{
		$this->AuthorityAction();
		
		$retur = new Retur();
		$retur-> retur_no = $_GET['Retur_No'];
		$lists_retur = $retur::getFirst($retur);
		
		if(count($lists_retur) > 0){
			foreach($lists_retur as $list_retur){
				$retur->retur_no = $list_retur['retur_no'];
				$retur->retur_type = $list_retur['retur_type'];
				$retur->reference = $list_retur['reference'];
				$retur->supplier_id = $list_retur['supplier_id'];
				$retur->create_date = $list_retur['create_date'];
				$retur->delivery_date = $list_retur['delivery_date'];
				$retur->delivery_address = $list_retur['delivery_address'];
				$retur->comments = $list_retur['comments'];
				$retur->gr_no_immpos = $list_retur['gr_no_immpos'];
				$retur->retur_no_immbo = $list_retur['retur_no_immbo'];
				$retur->total_gr = $list_retur['total_gr'];
				$retur->total_retur = $list_retur['total_retur'];
				$retur->tax_included = $list_retur['tax_included'];
				$retur->flag_sync = $list_retur['flag_sync'];
				$retur->gr_no_immbo = $list_retur['gr_no_immbo'];
				$retur->gr_retur_immpos = $list_retur['gr_retur_immpos'];
				$retur->gr_retur_immbo = $list_retur['gr_retur_immbo'];
			}
		}
		
		$this->view->retur = $retur;
		
		$retur_detail = new ReturDetail();		
		$this->view->retur_details = null;
		$retur_detail->retur_no = $retur-> retur_no;
		if($retur_detail->retur_no != ''){
			$condition = " WHERE \"retur_no\" ='".$retur_detail->retur_no."' ";
			$lists_retur_detail = $retur_detail::getFreeSQL($condition);

			$this->view->retur_details = $lists_retur_detail;
		}
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
				$tmuk->saldo = $list_tmuk['saldo'];
				$tmuk->telp = $list_tmuk['telp'];
				$tmuk->name = $list_tmuk['name'];
				$tmuk->owner = $list_tmuk['owner'];
				$tmuk->address1 = $list_tmuk['address1'];
				$tmuk->address2 = $list_tmuk['address2'];
				$tmuk->address3 = $list_tmuk['address3'];				
				$tmuk->lsi = $list_tmuk['nama_lsi'];				
			}
		}
		
		$this->view->tmuk = $tmuk;
		
		// $supplier = new Supplier();
		// $condition = " WHERE \"supplier_id\" = '".$tmuk->supplier_id."' "; 
		// $lists_suppplier = $supplier::getFreeSQL($condition);
		
		// if(isset($lists_suppplier)){
		// 	foreach($lists_suppplier as $list_suppplier){
		// 		$supplier->supplier_id = $list_suppplier['supplier_id'];
		// 		$supplier->supp_name = $list_suppplier['supp_name'];	
		// 		$supplier->address = $list_suppplier['address'];
		// 		$supplier->kecamatan = $list_suppplier['kecamatan'];				
		// 		$supplier->supp_telp = $list_suppplier['supp_telp'];
		// 	}
		// }
		
		// $this->view->supplier = $supplier;
	}
	
	public function addManualAction()
	{
		$this->AuthorityAction();
	}
	
	public function ajaxProductAction(){
		$product = new Product();
		$object['barcode'] = $_REQUEST['barcode'];
		$object['count_index'] = $_REQUEST['count_index'];
		$data = $product::getProductPR($object);
		
		return json_encode($data);
	}
	
	public function insertManualAction()
	{
		$number_range = new NumberRange();
		$number_range->trans_type = 'RR';
		$number_range->period = date('Ym');
		$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
		$lists = $number_range::getFreeSQL($condition);
		
		if(isset($lists)){
			foreach($lists as $list){
				$number_range->trans_type = $list['trans_type'];
				$number_range->period = $list['period'];
				$number_range->prefix = $list['prefix'];
				$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
			}
		}else{
			$number_range->trans_type = 'RR';
			$number_range->period = date('Ym');
			$number_range->prefix = 'RR';
			$number_range->from_ = '1';
			$number_range->to_ = '9999';
			$number_range->current_no = '1';
			$success = $number_range::goInsert($number_range);			
			$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
		}
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
			}
		}
		
		$retur = new Retur();
		$retur->retur_no = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;		
		$retur->retur_type = $this->request->getPost("retur_type");
		$retur->delivery_date = $this->request->getPost("delivery_date");
		$retur->comments = $this->request->getPost("comments");
		$success = $retur::goInsert($retur);
		
		if($success){			
			$success_num = $number_range::goAddNumber($number_range);
			
			$json_datas = json_decode($_POST['json_data']);			
			foreach($json_datas as $json_data){
				$retur_detail = new ReturDetail();
				$retur_detail->retur_no = $retur->retur_no;
				$retur_detail->item_code = $json_data[0];
				$retur_detail->description = $json_data[1];
				$retur_detail->qty_retur = $json_data[2];
				$retur_detail->unit_retur = $json_data[3];
				$retur_detail->retur_unit_price = $json_data[6];				
				$retur_detail->retur_price = $retur_detail->retur_unit_price * $retur_detail->qty_retur;
				$retur_detail->note = str_replace('%20',' ',$json_data[4]);				
				$retur_detail->comments = str_replace('%20',' ',$json_data[5]);			
				$success = $retur_detail::goInsert($retur_detail);
			}
			
			$data['type'] = 'S';
			$data['message'] = '<b>'.$retur->retur_no.'</b> Berhasil dibuat.';
			$notif = new Notifikasi();
			$notif->data = $data['message'];
			$notif->read_at = null;
			$notif->created_at = date('Y-m-d H:i:s');
			$notif->created_by = $this->session->get("user")['user_name'];

			$sukses = $notif::goInsert($notif);
			return json_encode($data);
		}else{						
			$data['type'] = 'E';
			$data['message'] = 'Gagal membuat retur manual!';
			return json_encode($data);
		}
	}
	
	public function ajaxSearchProductAction(){
		$product = new Product();
		$query = strtoupper($_GET['query']);
		
		$condition = " WHERE \"uom1_internal_barcode\" LIKE '%".$query."%' OR \"uom2_internal_barcode\" LIKE '%".$query."%' OR \"uom3_internal_barcode\" LIKE '%".$query."%' OR \"uom4_internal_barcode\" LIKE '%".$query."%' OR \"uom1_prod_nm\" LIKE '%".$query."%' AND mb_flag = 'B' AND ppob = 0 AND (LOWER(supplied_by) = 'lotte') ORDER BY description";
		$lists_product = $product::getFreeSQL($condition);
		
		$data_product = null;			
		
		for($i = 0, $j = 0; $i < count($lists_product); $i++){
			if($lists_product[$i]['uom1_htype_3']){
				$data_product[$j]['val'] = $lists_product[$i]['uom1_internal_barcode'];
				$data_product[$j]['display'] = $lists_product[$i]['uom1_internal_barcode'].' - '.$lists_product[$i]['uom1_prod_nm'];	
				$j++;
			}else if($lists_product[$i]['uom2_htype_3']){
				$data_product[$j]['val'] = $lists_product[$i]['uom2_internal_barcode'];
				$data_product[$j]['display'] = $lists_product[$i]['uom2_internal_barcode'].' - '.$lists_product[$i]['uom2_prod_nm'];		
				$j++;				
			}else if($lists_product[$i]['uom3_htype_3']){
				$data_product[$j]['val'] = $lists_product[$i]['uom3_internal_barcode'];
				$data_product[$j]['display'] = $lists_product[$i]['uom3_internal_barcode'].' - '.$lists_product[$i]['uom3_prod_nm'];	
				$j++;
			}else if($lists_product[$i]['uom4_htype_3']){
				$data_product[$j]['val'] = $lists_product[$i]['uom4_internal_barcode'];
				$data_product[$j]['display'] = $lists_product[$i]['uom4_internal_barcode'].' - '.$lists_product[$i]['uom4_prod_nm'];		
				$j++;
			}	
		}
		
		return json_encode($data_product);
	}
}
