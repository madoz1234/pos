
<?php

use Phalcon\Mvc\Controller;
use PHPExcel;

class DashboardController extends Controller
{
	
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'Dashboard' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		$dataOpening = new Product();
		
		$condition = " WHERE type = '' AND status_flag != '2' ";
		$this->view->opening = $dataOpening::getJoin_Opening($condition);	
	}

	public function sendChatAction()
	{
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getAll();
		$kode = "";
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$nama = 'TMUK '.$list_tmuk['name'];
			}
		}

		$input = $this->request->getPost("inputchat") ?: null ;
		if($input!=null){
			$curl = new CurlClass();
			$result = $curl->post($this->di['api']['link']."/api/chat/send-chat", [
				'pesan' => $input,
				'nama' => $nama,
			]);
			$obj = json_decode($result, true);
			if($obj['status'] == 'success'){			
				$resul['type'] = 'S';
				$resul['message'] = 'Pesan Terkirim!';
				return json_encode($resul);
			}else{						
				$resul['type'] = 'E';
				$resul['message'] = 'Pesan Gagal Dikirim!';
				return json_encode($resul);
			}
		}else{
			$resul['type'] = 'E';
			$resul['message'] = 'Pesan Gagal Dikirim!';
			return json_encode($resul);
		}	
	}
	
	public function getDataAction(){
		$condition = " WHERE DATE(tanggal) BETWEEN '".$_REQUEST['from']."' AND '".$_REQUEST['to']."' ";		
		$condi = " WHERE DATE(tj.tanggal) BETWEEN '".$_REQUEST['from']."' AND '".$_REQUEST['to']."' ";		
		$condit = " WHERE DATE(b.tanggal) BETWEEN '".$_REQUEST['from']."' AND '".$_REQUEST['to']."' ";		
		$conditi = " WHERE DATE(j.tanggal) BETWEEN '".$_REQUEST['from']."' AND '".$_REQUEST['to']."' ";		
		$cond = " WHERE DATE(tanggal) = '".date('Y-m-d')."'";		
		$cond_PR = " WHERE create_date BETWEEN '".$_REQUEST['from']."' AND '".$_REQUEST['to']."' ";		
		$cond_PO = " WHERE order_date BETWEEN '".$_REQUEST['from']."' AND '".$_REQUEST['to']."' ";		
		
		$lists_dash1 = JualDetail::get_dashboard1($condition);
		$lists_dash2 = JualDetail::get_dashboard2($condition);
		$lists_dash3_4 = JualDetail::get_dash3dash4($conditi);
		$lists_dash5 = JualDetail::get_dashboard5($condi);
		$lists_dash6 = JualDetail::get_dashboard6($condi);
		$lists_dash7 = JualDetail::get_dashboard7($condi);
		$lists_dash8 = JualDetail::get_dashboard8($cond_PR);
		$lists_dash8PR = JualDetail::get_dashboard8PR($cond_PR);
		$lists_dash9 = JualDetail::get_dashboard9($cond_PO);
		$lists_dash9PO = JualDetail::get_dashboard9PO($cond_PO);
		$lists_dash10 = JualDetail::get_dashboard10();
		$lists_dash11 = JualDetail::get_dashboard11($condit);
		$lists_dash15 = Jual::getSum($cond);	
			
		$val = [
			"dash1" => $lists_dash1,
			"dash2" => $lists_dash2,
			"dash3_4" => $lists_dash3_4,
			"dash5" => $lists_dash5,
			"dash6" => $lists_dash6,
			"dash7" => $lists_dash7,
			"dash8" => $lists_dash8,
			"dash8PR" => $lists_dash8PR,
			"dash9" => $lists_dash9,
			"dash9PO" => $lists_dash9PO,
			"dash10" => $lists_dash10,
			"dash11" => $lists_dash11,
			"dash15" => $lists_dash15
		];
		
		return json_encode($val);
	}
	
	public function ajaxProsesAction(){
		$cond_PR = " WHERE create_date BETWEEN '".$_REQUEST['from']."' AND '".$_REQUEST['to']."' ";		
		$cond_PO = " WHERE order_date BETWEEN '".$_REQUEST['from']."' AND '".$_REQUEST['to']."' ";		
		
		$lists_dash8PR = JualDetail::get_dashboard8PR($cond_PR);
		$lists_dash9PO = JualDetail::get_dashboard9PO($cond_PO);
		
		$val = [
			"dash8PR" => $lists_dash8PR,
			"dash9PO" => $lists_dash9PO,
		];
		
		return json_encode($val);
	}
	
	public function ajaxSOAction(){
		$cond = " WHERE so_id = '".$_REQUEST['so_id']."' ";	
		$lists_dash10SO = JualDetail::get_dashboardSO($cond);
		
		$val = [
			"dash10SO" => $lists_dash10SO,
		];
		
		return json_encode($val);
	}
	
	public function loadChatAction(){		
		$link = $this->di['api']['link']."getchat.php";
		$result = '';
		
		$connected = @fsockopen("www.google.com", 80); 	
		if($connected){
			$is_conn = true; 
			fclose($connected);
		}else{
			$is_conn = false; 
		}
		
		if($is_conn){ $result = file_get_contents($link); }
		
		if($result === false){
			return false;
		}else{
			return $result;
		}
	}
	
	// public function sendChatAction(){
	// 	$tmuk = new TMUK();
	// 	$lists = $tmuk::getAll();
		
	// 	if(isset($lists)){
	// 		foreach($lists as $list){
	// 			$tmuk_name = $list['name'];
	// 			$tmuk_code = $list['tmuk'];
	// 		}
	// 	}
		
	// 	$link = $this->di['api']['link']."sendchat.php?user=".$tmuk_name.' - '.$this->session->get("user")['user_name']."&text=".$_REQUEST['text']."&waktu=".date('d-m-Y H:i:s');
	// 	$result = file_get_contents($link);
		
	// 	return $result;
	// }
	
	public function cek_koneksiAction(){
		$connected = @fsockopen("www.google.com", 80); 	
		$is_conn = "";
		
		if($connected){
			$is_conn = true; 
			fclose($connected);
		}else{
			$is_conn = false; 
		}
		
		return json_encode($is_conn);
	}

}
