<?php

use Phalcon\Mvc\Controller;

class ChangePasswordController extends Controller
{

	public function indexAction()
	{
		$user = new User();
		$user->user_id = $this->session->get('user')['user_id'];
		$condition = " WHERE user_id ='". $user->user_id."' "
					." AND is_active = 1 ";
		$lists = $user::getFreeSQL($condition);
		
		$this->view->data = new User();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->user_id = $list['user_id'];	
				$this->view->data->user_name = $list['user_name'];				
				$this->view->data->role_id = $list['role_id'];
				$this->view->data->real_name = $list['real_name'];
				$this->view->data->last_login = $list['last_login'];
				$this->view->data->count_login = $list['count_login'];
				$this->view->data->date_created = $list['date_created'];
				$this->view->data->user_password = $list['user_password'];
				$this->view->data->is_active = $list['is_active'];				
			}
		}
	}
	
	public function updateAction()
	{
		if(base64_encode($this->request->getPost("Old_Password")) == $this->session->get('user')['user_password']){
			if(base64_encode($this->request->getPost("New_Password")) != $this->session->get('user')['user_password']){
				$user = new User();		
				$user->user_id = $this->request->getPost("User_ID");
				$user->user_name = $this->request->getPost("User_Name");
				$user->user_password = base64_encode($this->request->getPost("New_Password"));		
				$success = $user::goUpdate($user);	

				$this->flashSession->success("Berhasil;Password telah diupdate.;success");
			}		
		}else{
			$this->flashSession->success("Gagal;Anda harus re-login terlebih dahulu.;warning");
		}
		
		$this->response->redirect(uri('ChangePassword'));
	}
}
