<?php

use Phalcon\Mvc\Controller;

class PenyesuaianController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'Penyesuaian' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$adj = new Adj();
		$condition = " WHERE \"flag_delete\" = false AND \"flag_post\" = false AND \"flag_delete\" = false ";
		$cond = " WHERE \"flag_delete\" = false AND \"flag_post\" = true AND \"flag_delete\" = false ";
		$this->view->lists = $adj::getFreeSql($condition);
		$this->view->lists_history = $adj::getFreeSql2($cond);
	}

	public function addAction()
	{
		$this->AuthorityAction();
		
		$tipe = null;
		$tipe['Positif'] = 'Positif';
		$tipe['Negatif'] = 'Negatif';
		$this->view->tipe = $tipe;
		
		$detail = null;
		$detail['Bonus'] = 'Bonus';
		$detail['Store TMUK Usage'] = 'Store TMUK Usage';
		$this->view->detail = $detail;
		
		$product = null;
		$product[''] = ' -- Select Product -- ';		
		$this->view->product = $product;
	}
	
	public function ajaxSearchProductAction()
	{
		// get List Product
		$s_product = $_GET['query'];
		$s_product = strtoupper($s_product);
		
		$product = new Product();
		$condition = " WHERE mb_flag = 'B' AND ppob = 0 AND status_flag != '2' AND ( upper(\"uom1_internal_barcode\") LIKE '%".$s_product."%' " 
					." OR upper(\"uom2_internal_barcode\") LIKE '%".$s_product."%' " 
					." OR upper(\"description\") LIKE '%".$s_product."%' ) ";
		$lists_product = $product::getJoin_ProductPrice($condition);
		
		$data_product = null;
		
		for($i = 0, $j = 0; $i < count($lists_product); $i++){
			$data_product[$j]['val'] = $lists_product[$i]['uom1_internal_barcode'];
			$data_product[$j]['display'] = $lists_product[$i]['uom1_internal_barcode'].' - '.$lists_product[$i]['uom1_prod_nm'];	
			$j++;
		}
		
		return json_encode($data_product);
	}
	
	public function ajaxProductAction(){
		$product = new Product();
		$object['r_product'] = $_REQUEST['r_product'];
		$object['count_index'] = $_REQUEST['count_index'];
		$data = $product::getProductAdjustment($object);

		return json_encode($data);
	}
	
	public function ajaxAjustmentAction()
	{
		$adj_detail = new AdjDetail();
		$condition = " WHERE \"adj_id\" = '".$_GET['adj']."' ";
		$data = $adj_detail::getFreeSQL($condition);

		return json_encode($data);
	}
	
	public function insertAction()
	{
		$number_range = new NumberRange();
		$number_range->trans_type = 'AD';
		$number_range->period = date('Ym');
		$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
		$lists = $number_range::getFreeSQL($condition);
		
		if(isset($lists)){
			foreach($lists as $list){
				$number_range->trans_type = $list['trans_type'];
				$number_range->period = $list['period'];
				$number_range->prefix = $list['prefix'];
				$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
			}
		}else{
			$number_range->trans_type = 'AD';
			$number_range->period = date('Ym');
			$number_range->prefix = 'AD';
			$number_range->from_ = '1';
			$number_range->to_ = '9999';
			$number_range->current_no = '1';
			$success = $number_range::goInsert($number_range);			
			$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
		}
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
			}
		}
		
		$adj = new Adj();
		$adj->adj_id = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;
		$adj->create_by = $this->session->get("user")['user_name'];
		$adj->create_date = $this->request->getPost("create_date");		
		$adj->adj_type = $this->request->getPost("adj_type");
		$adj->adj_detail = $this->request->getPost("adj_detail");		
		$success = $adj::goInsert($adj);
		
		if($success){			
			$success_num = $number_range::goAddNumber($number_range);
			
			$json_datas = json_decode($_POST['json_data']);	
			foreach($json_datas as $json_data){
				$adj_detail = new AdjDetail();
				$adj_detail->adj_id = $adj->adj_id;
				$adj_detail->item_code = $json_data[0];
				$adj_detail->barcode_code = $json_data[1];
				$adj_detail->description = $json_data[2];
				$adj_detail->qty = $json_data[3];
				$adj_detail->unit = $json_data[4];				
				$adj_detail->total_price = $json_data[5];				
				$adj_detail->unit_price = $json_data[6];
				$success = $adj_detail::goInsert($adj_detail);
			}
			
			$data['type'] = 'S';
			$data['message'] = '<b>'.$adj->adj_id.'</b> Berhasil dibuat.';
			return json_encode($data);
		}else{						
			$data['type'] = 'E';
			$data['message'] = 'Gagal membuat Penyesuaian!';
			return json_encode($data);
		}
	}
	
	public function editAction()
	{
		$this->AuthorityAction();
		
		$tipe = null;
		$tipe['Positif'] = 'Positif';
		$tipe['Negatif'] = 'Negatif';
		$this->view->tipe = $tipe;				
		
		$product = null;
		$product[''] = ' -- Select Product -- ';
		$this->view->product = $product;
		
		$adj = new Adj();
		$adj->adj_id = $_REQUEST['adj_id'];
		$lists_adj = $adj::getFirst($adj);
		
		foreach($lists_adj as $list_adj){
			$adj->adj_id = $list_adj['adj_id'];
			$adj->create_by = $list_adj['create_by'];
			$adj->create_date = $list_adj['create_date'];
			$adj->status = $list_adj['status'];
			$adj->adj_type = $list_adj['adj_type'];
			$adj->adj_detail = $list_adj['adj_detail'];
			$adj->comment = $list_adj['comment'];
			$adj->flag_post = $list_adj['flag_post'];
			$adj->flag_delete = $list_adj['flag_delete'];
		}		
	
		$this->view->adj = $adj;
		
		$adj_detail = new AdjDetail();
		$adj_detail->adj_id = $_REQUEST['adj_id'];
		$condition = " WHERE \"adj_id\" = '".$adj_detail->adj_id."' ";
		$lists_adj_detail = $adj_detail::getFreeSQL($condition);
		
		$this->view->adj_details = $lists_adj_detail;
		
		$detail = null;
		if($adj->adj_type == 'Positif'){
			$detail['Bonus'] = 'Bonus';
			$detail['Store TMUK Usage'] = 'Store TMUK Usage';
		}else if($adj->adj_type == 'Negatif'){
			$detail['Expired'] = 'Expired';
			$detail['Broken'] = 'Broken';
		}
		$this->view->detail = $detail;
	}
	
	public function printAction()
	{
		$this->AuthorityAction();
		
		$ad = new Adj();
		$ad->adj_id = $_GET['adj_id'];
		$lists_ad = $ad::getFirst($ad);
		
		$lists_ad_view = null;
		if(isset($lists_ad)){
			foreach($lists_ad as $lists_ad){
				$lists_ad_view = $lists_ad;
			}
		}
		
		$this->view->ad = $lists_ad_view;
		
		$ad_detail = new AdjDetail();
		$condition = "WHERE adj_id = '".$ad->adj_id."'";
		$lists_ad_detail = $ad_detail::getJoin_Product($condition);
		
		$this->view->ad_details = $lists_ad_detail;
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
				$tmuk->saldo = $list_tmuk['saldo'];
				$tmuk->telp = $list_tmuk['telp'];
				$tmuk->name = $list_tmuk['name'];
				$tmuk->owner = $list_tmuk['owner'];
				$tmuk->address1 = $list_tmuk['address1'];
				$tmuk->address2 = $list_tmuk['address2'];
				$tmuk->address3 = $list_tmuk['address3'];				
			}
		}
		
		$this->view->tmuk = $tmuk;
	}
	
	public function deleteAction()
	{		
		$this->AuthorityAction();
		
		$adj = new Adj();
		$adj->adj_id = $_REQUEST['adj_id'];
		$lists_adj = $adj::getFirst($adj);
		
		foreach($lists_adj as $list_adj){
			$adj->adj_id = $list_adj['adj_id'];
			$adj->create_by = $list_adj['create_by'];
			$adj->create_date = $list_adj['create_date'];
			$adj->status = $list_adj['status'];
			$adj->adj_type = $list_adj['adj_type'];
			$adj->adj_detail = $list_adj['adj_detail'];
			$adj->comment = $list_adj['comment'];
			$adj->flag_post = $list_adj['flag_post'];
			$adj->flag_delete = $list_adj['flag_delete'];
		}
		
		$adj_delete = new Adj();
		$adj_delete->adj_id = $adj->adj_id;
		$adj_delete->flag_delete = true;
		$success = $adj_delete::goUpdate($adj_delete);
		
		if($success){
			if($adj->flag_post){
				$adj_delete = new Adj();
				$adj_delete->adj_id = $adj->adj_id;
				$adj_delete->status = 'VOID';
				$success = $adj_delete::goUpdate($adj_delete);
		
				// void product move
				$product_move = new ProductMove();
				$product_move->reference = $adj->adj_id;
				$success = $product_move::goVoidRef($product_move);
			}
		}
		
		$this->response->redirect(uri('Penyesuaian'));
	}
	
	public function postAction()
	{				
		$adj = new Adj();
		$adj->adj_id = $_REQUEST['adj_id'];
		$lists_adj = $adj::getFirst($adj);
		
		foreach($lists_adj as $list_adj){
			$adj->adj_id = $list_adj['adj_id'];
			$adj->create_by = $list_adj['create_by'];
			$adj->create_date = $list_adj['create_date'];
			$adj->status = $list_adj['status'];
			$adj->adj_type = $list_adj['adj_type'];
			$adj->adj_detail = $list_adj['adj_detail'];
			$adj->comment = $list_adj['comment'];
			$adj->flag_post = $list_adj['flag_post'];
			$adj->flag_delete = $list_adj['flag_delete'];
		}
		
		$adj_post = new Adj();
		$adj_post->adj_id = $_REQUEST['adj_id'];
		$adj_post->flag_post = true;
		$success = $adj_post::goUpdate($adj_post);
		
		if($success){
			$adj_detail = new AdjDetail();
			$condition = " WHERE \"adj_id\" = '".$adj->adj_id."' ";
			$lists_adj_detail = $adj_detail::getFreeSQL($condition);
		
			$number_range = new NumberRange();
			$number_range->trans_type = 'MV';
			$number_range->period = date('Ym');
			$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
			$lists = $number_range::getFreeSQL($condition);
			
			if(isset($lists)){
				foreach($lists as $list){
					$number_range->trans_type = $list['trans_type'];
					$number_range->period = $list['period'];
					$number_range->prefix = $list['prefix'];
					$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
				}
			}else{
				$number_range->trans_type = 'MV';
				$number_range->period = date('Ym');
				$number_range->prefix = 'MV';
				$number_range->from_ = '1';
				$number_range->to_ = '9999';
				$number_range->current_no = '1';
				$success = $number_range::goInsert($number_range);			
				$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
			}
			
			$tmuk = new TMUK();
			$lists_tmuk = $tmuk::getALL();
			
			if(isset($lists_tmuk)){
				foreach($lists_tmuk as $list_tmuk){
					$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
					$tmuk->tmuk = $list_tmuk['tmuk'];
					$tmuk->supplier_id = $list_tmuk['supplier_id'];
				}
			}
			
			foreach($lists_adj_detail as $list_adj_detail){
				$product_move = new ProductMove();		
				$product_move->trans_no = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;
				$product_move->stock_id = $list_adj_detail['item_code'];
				$product_move->type = 'AD';
				$product_move->reference = $list_adj_detail['adj_id'];
				if($adj->adj_type == 'Positif'){
					$product_move->qty = $list_adj_detail['qty'];
				}else if($adj->adj_type == 'Negatif'){
					$product_move->qty = $list_adj_detail['qty'] * -1;
				}
				$product_move->price = $list_adj_detail['unit_price'];
				$success = $product_move::goInsert($product_move);
			}

			$success_num = $number_range::goAddNumber($number_range);
		}
		
		$data['type'] = 'S';
		$data['message'] = '<b>'.$adj_post->adj_id.'</b> Berhasil diposting.';
		return json_encode($data);
	}
	
	public function updateAction()
	{		
		$adj = new Adj();
		$adj->adj_id = $this->request->getPost("adj_id");
		$adj->create_by = $this->request->getPost("create_by");		
		$adj->create_date = $this->request->getPost("create_date");
		$adj->adj_type = $this->request->getPost("adj_type");
		$adj->adj_detail = $this->request->getPost("adj_detail");		
		$success = $adj::goUpdate($adj);		
		
		if($success){			
			$json_datas = json_decode($_POST['json_data']);	
			
			$adj_detail_delete = new AdjDetail();
			$adj_detail_delete->adj_id = $this->request->getPost("adj_id");
			$success = $adj_detail_delete::goDeleteAdj($adj_detail_delete);
			
			foreach($json_datas as $json_data){
				$adj_detail = new AdjDetail();

				$adj_detail->adj_id = $this->request->getPost("adj_id");
				$adj_detail->item_code = $json_data[0];
				$adj_detail->barcode_code = $json_data[1];
				$adj_detail->description = $json_data[2];
				$adj_detail->qty = $json_data[3];
				$adj_detail->unit = $json_data[4];
				$adj_detail->unit_price = $json_data[5];
				$adj_detail->total_price = $json_data[3] * $json_data[5];
				$adj_detail->adj_detail_item = $json_data[6];
				
				if($adj_detail->adj_detail_item == '0'){
					$success = $adj_detail::goInsert($adj_detail);
				}else{
					$success = $adj_detail::goInsertItem($adj_detail);
				}
			}
			
			$data['type'] = 'S';
			$data['message'] = '<b>'.$adj->adj_id.'</b> Berhasil diubah.';
			return json_encode($data);	
		}else{						
			$data['type'] = 'E';
			$data['message'] = '<b>'.$adj->adj_id.'</b> Gagal diubah.';
			return json_encode($data);
		}
	}
}
