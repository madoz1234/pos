<?php

use Phalcon\Mvc\Controller;

class SupplierController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'Supplier' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$supplier = new Supplier();
		$this->view->lists = $supplier::getAll();
	}
	
	public function ajaxSupplierAction()
	{
		$supplier = new Supplier();
		$condition = " WHERE \"supplier_id\" = '".$_GET['id']."' LIMIT 1 ";
		$data = $supplier::getFreeSQL($condition);

		return json_encode($data);
	}
	
	public function addAction()
	{
		$this->AuthorityAction();
	}
	
	public function editAction()
	{
		$this->AuthorityAction();
	}
	
	public function viewAction()
	{
		$this->AuthorityAction();
	}
	
	public function deleteAction()
	{
		$this->AuthorityAction();
	}
	
	public function insertAction()
	{
		
	}
	
	public function updateAction()
	{
		
	}
}
