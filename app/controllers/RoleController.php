<?php

use Phalcon\Mvc\Controller;

class RoleController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'Role' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		$role = new Role();
		$this->view->lists = $role::getAll();
	}
	
	public function addAction()
	{
		$this->AuthorityAction();
	}
	
	public function editAction()
	{
		$this->AuthorityAction();
		
		$role = new Role();
		$role->role_id = $_REQUEST['Role_ID'];
		$lists = $role::getFirst($role);
		
		$this->view->data = new Role();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->role_id = $list['role_id'];
				$this->view->data->role_name = $list['role_name'];
				$this->view->data->description = $list['description'];
				$this->view->data->is_active = $list['is_active'];
				$this->view->data->pr_approve = $list['pr_approve'];
			}
		}
	}
	
	public function viewAction()
	{
		$this->AuthorityAction();
		
		$role = new Role();
		$role->role_id = $_REQUEST['Role_ID'];
		$lists = $role::getFirst($role);
		
		$this->view->data = new Role();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->role_id = $list['role_id'];
				$this->view->data->role_name = $list['role_name'];
				$this->view->data->description = $list['description'];
				$this->view->data->is_active = $list['is_active'];
				$this->view->data->pr_approve = $list['pr_approve'];
			}
		}
	}
	
	public function deleteAction()
	{
		$this->AuthorityAction();
		
		$role = new Role();
		$role->role_id = $_REQUEST['Role_ID'];
		$role_id = $this->session->get('user')['role_id'];
		if($role_id == 1){
			$success = $role::goDelete($role);
			if($success){
				$data['type'] = 'S';
				$data['message'] = '<i><b>Berhasil Hapus Role</b></i>';			
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = '<i><b>Gagal Hapus Role</b></i>';
				return json_encode($data);			
			}
		}else{
			$data['type'] = 'E';
			$data['message'] = '<i><b>Gagal !! Anda Tidak Mempunyai Akses</b></i>';
			return json_encode($data);
		}
		
	}
	
	public function insertAction()
	{
		$role = new Role();
		$role->role_name = $this->request->getPost("Role_Name");
		$role->description = $this->request->getPost("Description");
		$role->pr_approve = $this->request->getPost("PR_Approve");
		$condition = " WHERE \"role_name\" = '".$this->request->getPost("Role_Name")."' ";
		$cari = $role::getFreeSQL($condition);
		$role_id = $this->session->get('user')['role_id'];
		if($role_id == 1){
			if(count($cari) == 0){
				$success = $role::goInsert($role);
			}
			if($success){
				$data['type'] = 'S';
				$data['message'] = '<i><b>Berhasil Tambah Role</b></i>';			
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = '<i><b>Nama Role Tidak Boleh Sama</b></i>';
				return json_encode($data);			
			}
		}else{
			$data['type'] = 'E';
			$data['message'] = '<i><b>Gagal !! Anda Tidak Mempunyai Akses</b></i>';
			return json_encode($data);
		}
	}
	
	public function updateAction()
	{
		$role = new Role();
		$role->role_id = $this->request->getPost("Role_ID");
		$role->role_name = $this->request->getPost("Role_Name");
		$role->description = $this->request->getPost("Description");
		$role->pr_approve = $this->request->getPost("PR_Approve");
		if($role->pr_approve == '') { $role->pr_approve='f'; }
		
		$cond = " WHERE \"role_id\" = '".$this->request->getPost("Role_ID")."' ";
		$cond .= " AND \"role_name\" = '".$this->request->getPost("Role_Name")."' ";
		$cari = $role::getFreeSQL($cond);
		$role_id = $this->session->get('user')['role_id'];
		if($role_id == 1 OR ($this->session->get('user')['role_id'] == $this->request->getPost("Role_ID"))){
			if(count($cari) == 0){
				$condition = " WHERE \"role_id\" != '".$this->request->getPost("Role_ID")."' ";
				$condition .= " AND \"role_name\" = '".$this->request->getPost("Role_Name")."' ";
				$cari2 = $role::getFreeSQL($condition);
				if($cari2 == 0){
					$success = $role::goUpdate($role);	
				}
			}else{
				$success = $role::goUpdate($role);
			}

			if($success){
				$data['type'] = 'S';
				$data['message'] = '<i><b>Berhasil Ubah Role</b></i>';			
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = '<i><b>Nama Role Tidak Boleh Sama</b></i>';
				return json_encode($data);			
			}
		}else{
			$data['type'] = 'E';
			$data['message'] = '<i><b>Gagal !! Anda Tidak Mempunyai Akses</b></i>';
			return json_encode($data);
		}
	}
	
	public function ajaxRoleAction()
	{
		$role = new Role();
		$condition = " WHERE \"role_id\" = '".$_GET['id']."' LIMIT 1 ";
		$data = $role::getFreeSQL($condition);

		return json_encode($data);
	}

}
