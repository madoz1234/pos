<?php

use Phalcon\Mvc\Controller;
use GuzzleHttp\Client;


class MemberController extends Controller
{
	public function AuthorityAction(){
		if($this->session->get("is_login") == "X"){
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'Member' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction(){
		$this->AuthorityAction();
		
		$cust = new Cust();
		$this->view->lists = $cust::getCustomerManagement();
		
		$member = new Member();
		$tpoint = new TPoint();
		$this->view->lists_member = $member::getMemberManagement();
		$this->view->lists_poin = $tpoint::countPoin();
	}
	
	public function viewAction(){
		$this->AuthorityAction();
		
		$cust = new Cust();
		$cust->debtor_no = $_REQUEST['Debtor_No'];
		$lists = $cust::getFirstCustomerManagement($cust);
		
		$this->view->data = new Cust();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->debtor_no = $list['debtor_no'];
				$this->view->data->name = $list['name'];
				$this->view->data->debtor_ref = $list['debtor_ref'];
				$this->view->data->address = $list['address'];
				$this->view->data->tax_id = $list['tax_id'];
				$this->view->data->curr_code = $list['curr_code'];
				$this->view->data->discount = $list['discount'];
				$this->view->data->pymt_discount = $list['pymt_discount'];
				$this->view->data->credit_limit = $list['credit_limit'];
				$this->view->data->notes = $list['notes'];
				$this->view->data->inactive = $list['inactive'];
				$this->view->data->credit_limit = $list['credit_limit'];
				
				$this->view->data->total_order = $list['total_order'];
				$this->view->data->total_purchase = $list['total_purchase'];
				$this->view->data->last_visit = $list['last_visit'];
				$this->view->data->average_purchase = $list['average_purchase'];
				$this->view->data->point = $list['point'];
				$this->view->data->average_visit_mon = $list['average_visit_mon'];
				$this->view->data->average_visit_year = $list['average_visit_year'];
			}
		}
	}
	
	public function view_memberAction(){
		$this->AuthorityAction();
		
		$member = new Member();
		$member->branch_code = $_REQUEST['Branch_Code'];
		$member->debtor_no = $_REQUEST['Debtor_No'];
		$lists = $member::getFirstMemberManagement($member);
		$liststrans = $member::getMemberManagement();



		$this->view->data = new Member();
		$this->view->datatrans = new Member();
		// var_dump($lists);die;
		if(count($lists) > 0){
			foreach($lists as $list){
				if($list['nomor_member'] == $_REQUEST['Nomor_Member']){
					$this->view->data->branch_code = $list['branch_code'];
					$this->view->data->debtor_no = $list['debtor_no'];
					$this->view->data->br_name = $list['br_name'];
					$this->view->data->branch_ref = $list['branch_ref'];
					$this->view->data->br_address = $list['br_address'];
					$this->view->data->contact_name = $list['contact_name'];
					$this->view->data->default_location = $list['default_location'];
					$this->view->data->tax_group_id = $list['tax_group_id'];
					$this->view->data->br_post_address = $list['br_post_address'];
					$this->view->data->notes = $list['notes'];
					$this->view->data->nomor_member = $list['nomor_member'];
					$this->view->data->inactive = $list['inactive'];
					$this->view->data->credit_limit = $list['credit_limit'];
					$this->view->data->telepon = $list['telepon'];
					$this->view->data->email = $list['email'];
					
					$this->view->data->total_order = $list['total_order'];
					$this->view->data->total_purchase = $list['total_purchase'];
					$this->view->data->last_visit = $list['last_visit'];
					$this->view->data->average_purchase = $list['average_purchase'];
					$this->view->data->point = $list['point'];
					$this->view->data->average_visit_mon = $list['average_visit_mon'];
					$this->view->data->average_visit_year = $list['average_visit_year'];
					$this->view->data->total_credit = ($list['total_credit'] - $list['payment']) > 0 ? ($list['total_credit'] - $list['payment']) : 0;
				}
			}
		}

		if(count($liststrans) > 0){
			foreach($liststrans as $listtrans){
				if($listtrans['nomor_member'] == $_REQUEST['Nomor_Member']){
					$this->view->datatrans->total_order = $listtrans['total_order'];
					$this->view->datatrans->total_purchase = $listtrans['total_purchase'];
					$this->view->datatrans->last_visit = $listtrans['last_visit'];
					$this->view->datatrans->average_purchase = $listtrans['average_purchase'];
					$this->view->datatrans->point = $listtrans['point'];
					$this->view->datatrans->average_visit_mon = $listtrans['average_visit_mon'];
					$this->view->datatrans->average_visit_year = $listtrans['average_visit_year'];
				}
			}
		}

		// var_dump($this->view->data);
		// die;		
		
		$o_member_pay = new MemberPay();
		$condition = " WHERE \"branch_code\" = '".$member->branch_code."' AND \"debtor_no\" = '".$member->debtor_no."' ";
		$this->view->lists_pay = $o_member_pay::getFreeSQL($condition);
	}


	public function view_member_pointAction(){
		$this->AuthorityAction();
		
		$member = new Member();
		$member->branch_code = $_REQUEST['Branch_Code'];
		$member->debtor_no = $_REQUEST['Debtor_No'];
		$lists = $member::getFirstMemberManagement($member);
		
		$this->view->data = new Member();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->branch_code = $list['branch_code'];
				$this->view->data->debtor_no = $list['debtor_no'];
				$this->view->data->br_name = $list['br_name'];
				$this->view->data->branch_ref = $list['branch_ref'];
				$this->view->data->br_address = $list['br_address'];
				$this->view->data->contact_name = $list['contact_name'];
				$this->view->data->default_location = $list['default_location'];
				$this->view->data->tax_group_id = $list['tax_group_id'];
				$this->view->data->br_post_address = $list['br_post_address'];
				$this->view->data->notes = $list['notes'];
				$this->view->data->nomor_member = $list['nomor_member'];
				$this->view->data->inactive = $list['inactive'];
				$this->view->data->credit_limit = $list['credit_limit'];
				$this->view->data->telepon = $list['telepon'];
				$this->view->data->email = $list['email'];
				
				$this->view->data->total_order = $list['total_order'];
				$this->view->data->total_purchase = $list['total_purchase'];
				$this->view->data->last_visit = $list['last_visit'];
				$this->view->data->average_purchase = $list['average_purchase'];
				$this->view->data->point = $list['point'];
				$this->view->data->average_visit_mon = $list['average_visit_mon'];
				$this->view->data->average_visit_year = $list['average_visit_year'];
				$this->view->data->total_credit = ($list['total_credit'] - $list['payment']) > 0 ? ($list['total_credit'] - $list['payment']) : 0;
			}
		}

		
		$o_member_pay = new MemberPay();
		$condition = " WHERE \"branch_code\" = '".$member->branch_code."' AND \"debtor_no\" = '".$member->debtor_no."' ";
		$this->view->lists_pay = $o_member_pay::getFreeSQL($condition);
	}
	
	public function add_memberAction(){
		$this->AuthorityAction();
		$lists = TMUK::getAll();
		$this->view->tmuk = $lists[0]['tmuk'];
	}
	
	public function edit_memberAction(){
		$this->AuthorityAction();
		
		$member = new Member();
		$member->branch_code = $_REQUEST['Branch_Code'];
		$member->debtor_no = $_REQUEST['Debtor_No'];
		$lists = $member::getFirstMemberManagement($member);
		
		$this->view->data = new Member();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->branch_code = $list['branch_code'];
				$this->view->data->debtor_no = $list['debtor_no'];
				$this->view->data->br_name = $list['br_name'];
				$this->view->data->branch_ref = $list['branch_ref'];
				$this->view->data->br_address = $list['br_address'];
				$this->view->data->contact_name = $list['contact_name'];
				$this->view->data->default_location = $list['default_location'];
				$this->view->data->tax_group_id = $list['tax_group_id'];
				$this->view->data->br_post_address = $list['br_post_address'];
				$this->view->data->notes = $list['notes'];
				$this->view->data->nomor_member = $list['nomor_member'];
				$this->view->data->inactive = $list['inactive'];
				$this->view->data->credit_limit = $list['credit_limit'];
				$this->view->data->telepon = $list['telepon'];
				$this->view->data->email = $list['email'];
				
				$this->view->data->total_order = $list['total_order'];
				$this->view->data->total_purchase = $list['total_purchase'];
				$this->view->data->last_visit = $list['last_visit'];
				$this->view->data->average_purchase = $list['average_purchase'];
				$this->view->data->point = $list['point'];
				$this->view->data->average_visit_mon = $list['average_visit_mon'];
				$this->view->data->average_visit_year = $list['average_visit_year'];
			}
		}		
	}
	
	public function insert_memberAction(){
		$this->AuthorityAction();
		
		$o_member = new Member();		
		
		//$o_member->branch_code = $this->request->getPost("Branch_Code");
		$o_member->debtor_no = $this->request->getPost("Debtor_No");
		$o_member->br_name = $this->request->getPost("Name");
		$o_member->branch_ref = $this->request->getPost("Branch_Ref");
		$o_member->br_address = $this->request->getPost("Address");
		$o_member->contact_name = '';
		$o_member->default_location = '';		
		//$o_member->br_post_address = $this->request->getPost("Post_Address");
		$o_member->notes = $this->request->getPost("Notes");
		$o_member->nomor_member = $this->request->getPost("Nomor_Member");		
		$o_member->credit_limit = $this->request->getPost("Credit_Limit");
		$o_member->telepon = $this->request->getPost("Telepon");
		$o_member->email = $this->request->getPost("Email");

		$success = $o_member::goInsert($o_member);
		
		if($success){
			// try {
			// 	$tmuk = new TMUK();
			// 	$lists_tmuk = $tmuk::getAll();
			// 	$tmuk_kode='';
			// 	if(isset($lists_tmuk)){
			// 		foreach($lists_tmuk as $list_tmuk){
			// 			$tmuk_kode = $list_tmuk['tmuk'];
			// 		}
			// 	}
			// 	$curl = new CurlClass();
			// 	$result = $curl->post($this->di['api']['link']."/api/member/send", [
			// 		'tmuk_kode' => $tmuk_kode,
			// 		'nama' => $o_member->br_name,
			// 		'telepon' => $o_member->telepon,
			// 		'email' => $o_member->email,
			// 		'jeniskustomer_id' => $o_member->branch_ref,
			// 		'notes' => $o_member->notes,
			// 		'nomor' => $o_member->nomor_member,
			// 		'alamat' => $o_member->br_address,
			// 		'limit_kredit' => $o_member->credit_limit,
			// 	]);
			// 	$obj = json_decode($result, true);
			// } catch (Exception $e) {
			// }			
			$results['type'] = 'S';
			$results['message'] = 'Berhasil menambah Member.';
			return json_encode($results);
		}else{								
			$results['type'] = 'E';
			$results['message'] = 'Tidak bisa menambah Member.';
			return json_encode($results);
		}
	}
	
	public function update_memberAction(){
		$this->AuthorityAction();
		
		$o_member = new Member();
		
		$o_member->branch_code = $this->request->getPost("Branch_Code");
		$o_member->debtor_no = $this->request->getPost("Debtor_No");
		$o_member->br_name = $this->request->getPost("Name");
		$o_member->branch_ref = $this->request->getPost("Branch_Ref");
		$o_member->br_address = $this->request->getPost("Address");
		$o_member->contact_name = '';
		$o_member->default_location = '';		
		$o_member->br_post_address = $this->request->getPost("Post_Address");
		$o_member->notes = $this->request->getPost("Notes");
		$o_member->nomor_member = $this->request->getPost("Nomor_Member");		
		$o_member->credit_limit = $this->request->getPost("Credit_Limit");
		$o_member->telepon = $this->request->getPost("Telepon");
		$o_member->email = $this->request->getPost("Email");
		
		$success = $o_member::goUpdate($o_member);
		
		if($success){			
			$result['type'] = 'S';
			$result['message'] = 'Data Member berhasil diubah.';
			return json_encode($result);
		}else{								
			$result['type'] = 'E';
			$result['message'] = 'Data Member gagal diubah.';
			return json_encode($result);
		}
	}
	
	public function delete_memberAction(){
		$this->AuthorityAction();
		
		$o_member = new Member();
		
		$o_member->branch_code = $_REQUEST["Branch_Code"];
		$o_member->debtor_no = $_REQUEST["Debtor_No"];

		$success = $o_member::goDelete($o_member);
		
		if($success){			
			$result['type'] = 'S';
			$result['message'] = 'Member berhasil dihapus.';
			return json_encode($result);
		}else{								
			$result['type'] = 'E';
			$result['message'] = 'Member gagal dihapus.';
			return json_encode($result);
		}
	}
	
	public function add_member_payAction(){
		$this->AuthorityAction();
		
		$member = new Member();
		$member->branch_code = $_REQUEST['Branch_Code'];
		$member->debtor_no = $_REQUEST['Debtor_No'];
		$lists = $member::getFirstMemberManagement($member);
		
		$this->view->data = new Member();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->branch_code = $list['branch_code'];
				$this->view->data->debtor_no = $list['debtor_no'];
				$this->view->data->br_name = $list['br_name'];
				$this->view->data->branch_ref = $list['branch_ref'];
				$this->view->data->br_address = $list['br_address'];
				$this->view->data->contact_name = $list['contact_name'];
				$this->view->data->default_location = $list['default_location'];
				$this->view->data->tax_group_id = $list['tax_group_id'];
				$this->view->data->br_post_address = $list['br_post_address'];
				$this->view->data->notes = $list['notes'];
				$this->view->data->nomor_member = $list['nomor_member'];
				$this->view->data->inactive = $list['inactive'];
				$this->view->data->credit_limit = $list['credit_limit'];
				$this->view->data->telepon = $list['telepon'];
				$this->view->data->email = $list['email'];
				
				$this->view->data->total_order = $list['total_order'];
				$this->view->data->total_purchase = $list['total_purchase'];
				$this->view->data->last_visit = $list['last_visit'];
				$this->view->data->average_purchase = $list['average_purchase'];
				$this->view->data->point = $list['point'];
				$this->view->data->average_visit_mon = $list['average_visit_mon'];
				$this->view->data->average_visit_year = $list['average_visit_year'];
				$this->view->data->total_credit = ($list['total_credit'] - $list['payment'])>0?($list['total_credit'] - $list['payment']):0;
			}
		}		
		
		$o_member_pay = new MemberPay();
		$condition = " WHERE \"branch_code\" = '".$member->branch_code."' AND \"debtor_no\" = '".$member->debtor_no."' ";
		$this->view->lists_pay = $o_member_pay::getFreeSQL($condition);
	}
	
	public function edit_member_payAction(){
		$this->AuthorityAction();

		$o_member_pay = new MemberPay();
		$o_member_pay->id = $_REQUEST['ID'];
		$lists = $o_member_pay::getFirst($o_member_pay);
		
		$this->view->data = new MemberPay();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->id = $list['id'];
				$this->view->data->branch_code = $list['branch_code'];
				$this->view->data->debtor_no = $list['debtor_no'];
				$this->view->data->id_member = $list['id_member'];
				$this->view->data->amount = $list['amount'];
			}
		}
	}
	
	public function view_member_payAction(){
		$this->AuthorityAction();

		$o_member_pay = new MemberPay();
		$o_member_pay->id = $_REQUEST['ID'];
		$lists = $o_member_pay::getFirst($o_member_pay);
		
		$this->view->data = new MemberPay();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->id = $list['id'];
				$this->view->data->branch_code = $list['branch_code'];
				$this->view->data->debtor_no = $list['debtor_no'];
				$this->view->data->id_member = $list['id_member'];
				$this->view->data->amount = $list['amount'];
				$this->view->data->create_by = $list['create_by'];
				$this->view->data->create_date = $list['create_date'];
			}
		}
	}
	
	public function insert_member_payAction(){
		$this->AuthorityAction();
		
		if($this->request->getPost("Amount") > $this->request->getPost("Limit")){
			$result['type'] = 'E';
			$result['message'] = 'Limit Kredit Anda hanya Rp '.number_format($this->request->getPost("Limit"));
			return json_encode($result);
		}else{
			$o_member_pay = new MemberPay();		
			
			$o_member_pay->branch_code = $this->request->getPost("Branch_Code");
			$o_member_pay->debtor_no = $this->request->getPost("Debtor_No");
			$o_member_pay->id_member = $this->request->getPost("Nomor_Member");
			$o_member_pay->amount = $this->request->getPost("Amount");
			$o_member_pay->create_by = $this->session->get('user')['user_name'];

			$success = $o_member_pay::goInsert($o_member_pay);
			
			if($success){			
				$result['type'] = 'S';
				$result['message'] = 'Sukses create member payment!';
				return json_encode($result);
			}else{								
				$result['type'] = 'E';
				$result['message'] = 'Gagal create member payment!';
				return json_encode($result);
			}
		}
	}
	
	public function update_member_payAction(){
		$this->AuthorityAction();
		
		$o_member_pay = new MemberPay();
		
		$o_member_pay->id = $this->request->getPost("ID");
		$o_member_pay->branch_code = $this->request->getPost("Branch_Code");
		$o_member_pay->debtor_no = $this->request->getPost("Debtor_No");
		$o_member_pay->id_member = $this->request->getPost("Nomor_Member");
		$o_member_pay->amount = $this->request->getPost("Amount");
		
		$success = $o_member_pay::goUpdate($o_member_pay);
		
		if($success){			
			$result['type'] = 'S';
			$result['message'] = 'Sukses edit member pay!';
			return json_encode($result);
		}else{								
			$result['type'] = 'E';
			$result['message'] = 'Gagal edit member pay!';
			return json_encode($result);
		}
	}

	public function import_kustomerAction(){
		if ($this->request->hasFiles('excelkustomer') == true) {
			foreach ($this->request->getUploadedFiles('excelkustomer') as $file) {
				$lampiran = $file;
				$filepath = curl_file_create( realpath($lampiran->getTempName()), $lampiran->getRealType(), $lampiran->getName() );
				$xfile = fopen($lampiran->getTempName(), 'r');
				$filename = $lampiran->getName();
			}
			$excelReader = PHPExcel_IOFactory::createReaderForFile($lampiran->getTempName());
			$excelObj = $excelReader->load($lampiran->getTempName());
			$worksheet = $excelObj->getSheet(0);
			$lastRow = $worksheet->getHighestRow();

			$upload=0;
			$gagal=0;

			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()
			->setCreator("Admin")
			->setTitle("Log Gagal Import")
			->setSubject("Template excel")
			->setDescription("Template excel permettant la création d'un ou plusieurs relevés d'heures")
			->setKeywords("Template excel");
			$objPHPExcel->setActiveSheetIndex(0);

			for ($row = 2; $row <= $lastRow; $row++) {
				try {
					$cust = new Cust();
					$cust->debtor_no = $worksheet->getCell('A'.$row)->getValue();	
					$cust->name	= $worksheet->getCell('B'.$row)->getValue();
					$cust->debtor_ref =	$worksheet->getCell('C'.$row)->getValue();
					$cust->address = $worksheet->getCell('D'.$row)->getValue();
					$cust->tax_id = $worksheet->getCell('E'.$row)->getValue();
					$cust->curr_code = $worksheet->getCell('F'.$row)->getValue();
					$cust->discount	= $worksheet->getCell('G'.$row)->getValue();
					$cust->pymt_discount = $worksheet->getCell('H'.$row)->getValue();
					$cust->credit_limit	= $worksheet->getCell('I'.$row)->getValue();
					$cust->notes = $worksheet->getCell('J'.$row)->getValue();
					$cust->inactive	= $worksheet->getCell('K'.$row)->getValue();
					$cust->nomor_member = $worksheet->getCell('L'.$row)->getValue();
					
					$success = $cust::goInsert($cust);
					if($success){
						$upload++;
					}
				} catch (Exception $e) {
					$gagal++;

					$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $worksheet->getCell('A'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $worksheet->getCell('B'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $worksheet->getCell('C'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $worksheet->getCell('D'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $worksheet->getCell('E'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $worksheet->getCell('F'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $worksheet->getCell('G'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $worksheet->getCell('H'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, $worksheet->getCell('I'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, $worksheet->getCell('J'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, $worksheet->getCell('K'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, $worksheet->getCell('L'.$row)->getValue());
				}
			}
			$result['message'] = $gagal.' Gagal dan '.$upload.' Berhasil di Import!';
			if($gagal>0){
				$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$filename = 'log-import/'.date("YmdHis").'.xls';
				$writer->save($filename);
				$result['message'] = $gagal.' Gagal dan '.$upload.' Berhasil di Import! <br><a href="'.$filename.'">Download Log</a>';
			}
			$result['type'] = 'S';
			return json_encode($result);
		}else{								
			$result['type'] = 'E';
			$result['message'] = 'Gagal Import Data!';
			return json_encode($result);

		}
	}

	public function import_memberAction(){
		if ($this->request->hasFiles('excelmember') == true) {
			foreach ($this->request->getUploadedFiles('excelmember') as $file) {
				$lampiran = $file;
				$filepath = curl_file_create( realpath($lampiran->getTempName()), $lampiran->getRealType(), $lampiran->getName() );
				$xfile = fopen($lampiran->getTempName(), 'r');
				$filename = $lampiran->getName();
			}
			$excelReader = PHPExcel_IOFactory::createReaderForFile($lampiran->getTempName());
			$excelObj = $excelReader->load($lampiran->getTempName());
			$worksheet = $excelObj->getSheet(0);
			$lastRow = $worksheet->getHighestDataRow();

			$upload = 0;
			$gagal = 0;
			$nomor = 2;

			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()
			->setCreator("Admin")
			->setTitle("Log Gagal Import")
			->setSubject("Template excel")
			->setDescription("")
			->setKeywords("Template excel");
			$objPHPExcel->setActiveSheetIndex(0);

			for ($row = 2; $row <= $lastRow; $row++) {
				try {
					$member = new Member();
					// $member->branch_code = $worksheet->getCell('A'.$row)->getValue();	
					$member->debtor_no	= $worksheet->getCell('A'.$row)->getValue();
					$member->nomor_member	= $worksheet->getCell('B'.$row)->getValue();
					$member->br_name	= preg_replace('/[^A-Za-z0-9\-]/', '', pg_escape_string($worksheet->getCell('C'.$row)->getValue()));
					$member->telepon	= $worksheet->getCell('D'.$row)->getValue();
					$member->email= $worksheet->getCell('E'.$row)->getValue();
					$member->br_address	= $worksheet->getCell('F'.$row)->getValue();
					$member->credit_limit	= $worksheet->getCell('G'.$row)->getValue();
					$member->notes	= $worksheet->getCell('H'.$row)->getValue();
					$member->branch_ref	= '';
					$member->contact_name	= '';
					$member->default_location	= '';
					$member->tax_group_id	= 0;
					$member->br_post_address	= '';
					$member->inactive	= 0;

					$success = $member::goInsert($member);
					if($success){
						$upload++;
					}else{
						$gagal++;
						$objPHPExcel->getActiveSheet()->SetCellValue('A'.$nomor, $worksheet->getCell('A'.$row)->getValue());
						$objPHPExcel->getActiveSheet()->SetCellValue('B'.$nomor, $worksheet->getCell('B'.$row)->getValue());
						$objPHPExcel->getActiveSheet()->SetCellValue('C'.$nomor, $worksheet->getCell('C'.$row)->getValue());
						$objPHPExcel->getActiveSheet()->SetCellValue('D'.$nomor, $worksheet->getCell('D'.$row)->getValue());
						$objPHPExcel->getActiveSheet()->SetCellValue('E'.$nomor, $worksheet->getCell('E'.$row)->getValue());
						$objPHPExcel->getActiveSheet()->SetCellValue('F'.$nomor, $worksheet->getCell('F'.$row)->getValue());
						$objPHPExcel->getActiveSheet()->SetCellValue('G'.$nomor, $worksheet->getCell('G'.$row)->getValue());
						$objPHPExcel->getActiveSheet()->SetCellValue('H'.$nomor, $worksheet->getCell('H'.$row)->getValue());
						$nomor++;
					}
				} catch (Exception $e) {
				}
			}
			$result['message'] = $gagal.' Gagal dan '.$upload.' Berhasil di Import!';
			if($gagal>0){
				$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$filename = 'log-import/'.date("YmdHis").'.xls';
				$writer->save($filename);
				$result['message'] = $gagal.' Gagal dan '.$upload.' Berhasil di Import! <br><a href="'.$filename.'">Download Log</a>';
			}
			$result['type'] = 'S';
			return json_encode($result);
		}else{
			$result['type'] = 'E';
			$result['message'] = 'Gagal Import Data!';
			return json_encode($result);
		}
	}

	public function exportexcelAction() {
		$this->AuthorityAction();
		$lists = Member::getFreeSQL();
		
		function tgl_indonesia($date){
			$Hari = array ("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu",);
			$Bulan = array ("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
			$tahun     = substr($date, 0, 4);
			$bulan     = substr($date, 5, 2);
			$tgl    = substr($date, 8, 2);
			$waktu    = substr($date,11, 5);
			$hari    = date("w", strtotime($date));
			$result = $Hari[$hari].", ".$tgl." ".$Bulan[(int)$bulan-1]." ".$tahun." ".$waktu." ";
			return $result;
		}

		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
				$tmuk->saldo = $list_tmuk['saldo'];
				$tmuk->telp = $list_tmuk['telp'];
				$tmuk->name = $list_tmuk['name'];
				$tmuk->owner = $list_tmuk['owner'];
				$tmuk->address1 = $list_tmuk['address1'];
				$tmuk->address2 = $list_tmuk['address2'];
				$tmuk->address3 = $list_tmuk['address3'];				
			}
		}
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->setTitle('Payment Member');

		foreach(range('A1','G2') as $head)
		{
			$objPHPExcel->getActiveSheet()->getStyle($head)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
		}


		foreach(range('A4','G4') as $set)
		{
			$objPHPExcel->getActiveSheet()->getStyle($set)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
			$objPHPExcel->getActiveSheet()->getColumnDimension($set)->setAutoSize(true);
		}

		$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells('A1:G1')
		->mergeCells('A2:G2')
		->setCellValue('A1', 'PAYMENT MEMBER TMUK '.$tmuk->name)
		->setCellValue('A2', tgl_indonesia(date('Y-m-d')))
		->setCellValue('A4', 'Branch Code')
		->setCellValue('B4', 'Debtor No')
		->setCellValue('C4', 'Nomor Member')
		->setCellValue('D4', 'Nama Member')
		->setCellValue('E4', 'Limit Kredit')
		->setCellValue('F4', 'Payment')
		->setCellValue('G4', 'Kredit Tersedia');
		$objPHPExcel->getActiveSheet()
			->getStyle('A')
			->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()
			->getStyle('B')
			->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()
			->getStyle('C')
			->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()
			->getStyle('D')
			->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()
			->getStyle('E')
			->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()
			->getStyle('F')
			->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()
			->getStyle('G')
			->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$i=5;
		$member = new Member();
		foreach ($lists as $key => $value) {
			$member->branch_code = $value['branch_code'];
			$member->debtor_no = $value['debtor_no'];
			$lists = $member::getFirstMemberManagement($member);
			$sisa_limit = $value['credit_limit'];
			$total_credit = ($lists['0']['total_credit'] - $lists['0']['payment']) > 0 ? ($lists['0']['total_credit'] - $lists['0']['payment']) : 0;
			$sisa = $sisa_limit - $total_credit;

			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, $value['branch_code'])
			->setCellValue('B'.$i, $value['debtor_no'])
			->setCellValue('C'.$i, $value['nomor_member'])
			->setCellValue('D'.$i, $value['br_name'])
			->setCellValue('E'.$i, $value['credit_limit'])
			->setCellValue('F'.$i, '0')
			->setCellValue('G'.$i, $sisa);


			$objValidation = $objPHPExcel->getActiveSheet()->getCell('F'.$i)
			->getDataValidation();

			$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_WHOLE );
			$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
			$objValidation->setAllowBlank(true);
			$objValidation->setShowInputMessage(true);
			$objValidation->setShowErrorMessage(true);
			$objValidation->setErrorTitle('Input Payment error');
			$objValidation->setError('Payment Melebihi Maks Limit!');
			$objValidation->setPrompt('Batas Maksimum Payment sampai '.$total_credit);
			$objValidation->setFormula1(0);
			$objValidation->setFormula2($total_credit);
			$i++;
		}

		$fname = $tmuk->name . "-Payment Member.xlsx";
		$temp_file = tempnam(sys_get_temp_dir(), 'phpexcel');

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($temp_file);

		$response = new \Phalcon\Http\Response();

		$response->setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		$response->setHeader('Content-Disposition', 'attachment;filename="' . $fname . '"');
		$response->setHeader('Cache-Control', 'max-age=0');

		$response->setHeader('Cache-Control', 'max-age=1');

		$response->setContent(file_get_contents($temp_file));

		unlink($temp_file);

		return $response;
	}

	public function uploadAction() {
		$this->AuthorityAction();
		if ($this->request->hasFiles('excelpayment') == true) {
			foreach ($this->request->getUploadedFiles('excelpayment') as $file) {
				$lampiran = $file;
				$filepath = curl_file_create( realpath($lampiran->getTempName()), $lampiran->getRealType(), $lampiran->getName() );
				$xfile = fopen($lampiran->getTempName(), 'r');
				$filename = $lampiran->getName();
			}
			$excelReader = PHPExcel_IOFactory::createReaderForFile($lampiran->getTempName());
			$excelObj = $excelReader->load($lampiran->getTempName());
			$worksheet = $excelObj->getSheet(0);
			$lastRow = $worksheet->getHighestDataRow();

			$upload = 0;
			$gagal = 0;
			$nomor = 5;

			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()
			->setCreator("Admin")
			->setTitle("Log Gagal Import")
			->setSubject("Template excel")
			->setDescription("")
			->setKeywords("Template excel");
			$objPHPExcel->setActiveSheetIndex(0);

			for ($row = 5; $row <= $lastRow; $row++) {
				if($worksheet->getCell('F'.$row)->getValue() > 0){
					try {
						$memberpay = new MemberPay();
						// $memberpay->branch_code = $worksheet->getCell('A'.$row)->getValue();	
						$memberpay->branch_code	= $worksheet->getCell('A'.$row)->getValue();
						$memberpay->debtor_no	= $worksheet->getCell('B'.$row)->getValue();
						$memberpay->id_member	= $worksheet->getCell('C'.$row)->getValue();
						$memberpay->amount		= $worksheet->getCell('F'.$row)->getValue();
						$memberpay->create_by 	= $this->session->get("user")['user_name'];

						$success = $memberpay::goInsert($memberpay);
						if($success){
							$upload++;
						}else{
							$gagal++;
							$objPHPExcel->getActiveSheet()->SetCellValue('A'.$nomor, $worksheet->getCell('A'.$row)->getValue());
							$objPHPExcel->getActiveSheet()->SetCellValue('B'.$nomor, $worksheet->getCell('B'.$row)->getValue());
							$objPHPExcel->getActiveSheet()->SetCellValue('C'.$nomor, $worksheet->getCell('C'.$row)->getValue());
							$objPHPExcel->getActiveSheet()->SetCellValue('F'.$nomor, $worksheet->getCell('F'.$row)->getValue());
							$nomor++;
						}
					} catch (Exception $e) {
					}
				}
			}
			$result['message'] = $gagal.' Gagal dan '.$upload.' Berhasil di Import!';
			if($gagal>0){
				$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$filename = 'log-import/'.date("YmdHis").'.xls';
				$writer->save($filename);
				$result['message'] = $gagal.' Gagal dan '.$upload.' Berhasil di Import! <br><a href="'.$filename.'">Download Log</a>';
			}
			$result['type'] = 'S';
			return json_encode($result);
		}else{
			$result['type'] = 'E';
			$result['message'] = 'Gagal Import Data!';
			return json_encode($result);
		}
	}
}
