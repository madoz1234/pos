<?php

use Phalcon\Mvc\Controller;

class PromoController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'Promo' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$promo = new Promo();
		$conditionDiskon = " WHERE id::varchar IN (SELECT promosi_id FROM m_promosi_diskon) AND flag = 'true'";
		$this->view->data_diskon = $promo::getFreeSql($conditionDiskon);

		$promo = new Promo();
		$conditionDiskon = " WHERE id::varchar IN (SELECT promosi_id FROM m_promosi_diskon) AND flag = 'false'";
		$this->view->data_diskon_history = $promo::getFreeSql($conditionDiskon);	

		$conditionXY = " WHERE id::integer IN (SELECT promosi_id FROM m_promo_x_get_y) AND flag = 'true'";
		$this->view->xy = $promo::getFreeSql($conditionXY);

		$conditionXY = " WHERE id::integer IN (SELECT promosi_id FROM m_promo_x_get_y) AND flag = 'false'";
		$this->view->xy_history = $promo::getFreeSql($conditionXY);

		$conditionPwp = " WHERE id::varchar IN (SELECT kode_promosi FROM m_promosi_pwp) AND flag = 'true'";
		$this->view->pwp = $promo::getFreeSql($conditionPwp);

		$conditionPwp = " WHERE id::varchar IN (SELECT kode_promosi FROM m_promosi_pwp) AND flag = 'false'";
		$this->view->pwp_history = $promo::getFreeSql($conditionPwp);

		$conditionPromoPromo = " WHERE id::varchar IN (SELECT promosi_id FROM m_promosi_promo) AND flag = 'true'";
		$this->view->promosi_promo = $promo::getFreeSql($conditionPromoPromo);

		$conditionPromoPromo = " WHERE id::varchar IN (SELECT promosi_id FROM m_promosi_promo) AND flag = 'false'";
		$this->view->promosi_promo_history = $promo::getFreeSql($conditionPromoPromo);
	}
	
	public function addAction()
	{
		$this->AuthorityAction();
	}
	
	public function editAction()
	{		
		$this->AuthorityAction();
	}
	
	public function deleteAction()
	{		
		$this->AuthorityAction();
	
		$data = new Promo();
		$data->id = $_REQUEST["ID"];
		$success = $data::goDelete($data, $_GET['Status']);
	
		$this->response->redirect(uri('Promo'));
	}
	
	public function insertAction()
	{		
		$data = new Promo();
		
		if(strpos($_SERVER['REQUEST_URI'], 'Status') == false){
			$data->kode_promosi = $this->request->getPost("kode_promosi");
			$data->nama_promosi = $this->request->getPost("nama_promosi");
			$data->from_ = $this->request->getPost("from_");
			$data->to_ = $this->request->getPost("to_");
			$data->loc_code = $this->request->getPost("loc_code");
			$data->jenis_promo = $this->request->getPost("jenis_promo");
			$data->amount = $this->request->getPost("amount");
			
			$success = $data::goInsert($data, '');
		
		}else{
			
			if($_GET["Status"] == 'Barang'){
				$data->kode_promosi = $this->request->getPost("kode_promosi");
				$data->stock_id = $this->request->getPost("stock_id");
				$data->qty = $this->request->getPost("qty");
				$data->discount_max = $this->request->getPost("discount_max");
				$data->discount_amount = $this->request->getPost("discount_amount");
				
				$success = $data::goInsert($data, $_GET['Status']);
				
			}
			
			if($_GET["Status"] == 'Hadiah'){
				$data->kode_promosi = $this->request->getPost("kode_promosi");
				$data->stock_id = $this->request->getPost("stock_id");
				$data->qty = $this->request->getPost("qty");
				
				$success = $data::goInsert($data, $_GET['Status']);
				
			}
			
			if($_GET["Status"] == 'Location'){
				$data->kode_promosi = $this->request->getPost("kode_promosi");
				$data->location = $this->request->getPost("location");
				
				$success = $data::goInsert($data, $_GET['Status']);
				
			}

			if($_GET["Status"] == 'PWP'){
				$data->kode_promosi = $this->request->getPost("kode_promosi");
				$data->stock_id = $this->request->getPost("stock_id");
				$data->price = $this->request->getPost("price");
				
				$success = $data::goInsert($data, $_GET['Status']);
				
			}
		}

		if($success){
			$this->response->redirect(uri('Promo'));
		}else{
			$this->response->redirect(uri('Promo/Add'));
		}
	}
	
	public function updateAction()
	{		
		$data = new Promo();
		
		if(strpos($_SERVER['REQUEST_URI'], 'Status') == false){
			$data->id = $this->request->getPost("id");
			$data->kode_promosi = $this->request->getPost("kode_promosi");
			$data->nama_promosi = $this->request->getPost("nama_promosi");
			$data->from_ = $this->request->getPost("from_");
			$data->to_ = $this->request->getPost("to_");
			$data->loc_code = $this->request->getPost("loc_code");
			$data->jenis_promo = $this->request->getPost("jenis_promo");
			$data->amount = $this->request->getPost("amount");
			
			$success = $data::goUpdate($data, '');
		
		}else{
			
			if($_GET["Status"] == 'Barang'){
				$data->id = $this->request->getPost("id");
				$data->kode_promosi = $this->request->getPost("kode_promosi");
				$data->stock_id = $this->request->getPost("stock_id");
				$data->qty = $this->request->getPost("qty");
				$data->discount_max = $this->request->getPost("discount_max");
				$data->discount_amount = $this->request->getPost("discount_amount");
				
				$success = $data::goUpdate($data, $_GET['Status']);
				
			}
			
			if($_GET["Status"] == 'Hadiah'){
				$data->id = $this->request->getPost("id");
				$data->kode_promosi = $this->request->getPost("kode_promosi");
				$data->stock_id = $this->request->getPost("stock_id");
				$data->qty = $this->request->getPost("qty");
				
				$success = $data::goUpdate($data, $_GET['Status']);
				
			}
			
			if($_GET["Status"] == 'Location'){
				$data->id = $this->request->getPost("id");
				$data->kode_promosi = $this->request->getPost("kode_promosi");
				$data->location = $this->request->getPost("location");
				
				$success = $data::goUpdate($data, $_GET['Status']);
				
			}

			if($_GET["Status"] == 'PWP'){
				$data->id = $this->request->getPost("id");
				$data->kode_promosi = $this->request->getPost("kode_promosi");
				$data->stock_id = $this->request->getPost("stock_id");
				$data->price = $this->request->getPost("price");
				
				$success = $data::goUpdate($data, $_GET['Status']);
				
			}
		}

		if($success){
			$this->response->redirect(uri('Promo'));
		}else{
			$this->response->redirect(uri('Promo/Edit'));
		}
	}
}
