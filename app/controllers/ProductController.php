<?php

use Phalcon\Mvc\Controller;

class ProductController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'Product' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$product = new Product();
		$this->view->lists = $product::getAll();
	}
	
	public function addAction()
	{
		$this->AuthorityAction();
		
		// get list unit
		$unit = new Unit();
		$lists = $unit::getAll();
		
		$data = null;
		foreach($lists as $list){
			$data[$list['unit']] = $list['unit'];
		}
		
		$this->view->unit = $data;
	}
	
	public function editAction()
	{
		$this->AuthorityAction();
		
		$product = new Product();
		$product->stock_id = $_REQUEST['Stock_ID'];
		$lists = $product::getFirst($product);
		
		$this->view->data = new Product();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->stock_id = $list['stock_id'];				
				$this->view->data->category_id = $list['category_id'];
				$this->view->data->tax_type_id = $list['tax_type_id'];
				$this->view->data->description = $list['description'];
				$this->view->data->long_description = $list['long_description'];
				$this->view->data->units = $list['units'];
				$this->view->data->actual_cost = $list['actual_cost'];
				$this->view->data->last_cost = $list['last_cost'];
				$this->view->data->material_cost = $list['material_cost'];
				$this->view->data->labour_cost = $list['labour_cost'];
				$this->view->data->overhead_cost = $list['overhead_cost'];
				$this->view->data->inactive = $list['inactive'];
				$this->view->data->no_sale = $list['no_sale'];
				$this->view->data->editable = $list['editable'];
				$this->view->data->jenis_item = $list['jenis_item'];
				$this->view->data->bumun_cd = $list['bumun_cd'];
				$this->view->data->l1_cd = $list['l1_cd'];
				$this->view->data->l2_cd = $list['l2_cd'];
				$this->view->data->l3_cd = $list['l3_cd'];
				$this->view->data->l4_cd = $list['l4_cd'];
				$this->view->data->margin = $list['margin'];
				$this->view->data->moving_type = $list['moving_type'];
				$this->view->data->ass_type_1 = $list['ass_type_1'];
				$this->view->data->ass_type_2 = $list['ass_type_2'];
				$this->view->data->ass_type_3 = $list['ass_type_3'];
				$this->view->data->expiry_product = $list['expiry_product'];
				$this->view->data->weight_product = $list['weight_product'];
				$this->view->data->batch_product = $list['batch_product'];
				$this->view->data->active_status = $list['active_status'];
				$this->view->data->kode_1 = $list['kode_1'];
				$this->view->data->kode_2 = $list['kode_2'];
				$this->view->data->kode_3 = $list['kode_3'];
				$this->view->data->discount = $list['discount'];
				$this->view->data->supplied_by = $list['supplied_by'];
				$this->view->data->uom1_nm = $list['uom1_nm'];
				$this->view->data->uom1_htype_1 = $list['uom1_htype_1'];
				$this->view->data->uom1_htype_2 = $list['uom1_htype_2'];
				$this->view->data->uom1_htype_3 = $list['uom1_htype_3'];
				$this->view->data->uom1_htype_4 = $list['uom1_htype_4'];
				$this->view->data->uom1_htype_5 = $list['uom1_htype_5'];
				$this->view->data->uom1_rack_type = $list['uom1_rack_type'];
				$this->view->data->uom1_shelfing_rack_type = $list['uom1_shelfing_rack_type'];
				$this->view->data->uom1_internal_barcode = $list['uom1_internal_barcode'];
				$this->view->data->uom1_width = $list['uom1_width'];
				$this->view->data->uom1_length = $list['uom1_length'];
				$this->view->data->uom1_height = $list['uom1_height'];
				$this->view->data->uom1_weight = $list['uom1_weight'];
				$this->view->data->uom1_margin = $list['uom1_margin'];
				$this->view->data->uom1_box_type = $list['uom1_box_type'];
				$this->view->data->uom1_harga = $list['uom1_harga'];
				$this->view->data->uom2_nm = $list['uom2_nm'];
				$this->view->data->uom2_htype_1 = $list['uom2_htype_1'];
				$this->view->data->uom2_htype_2 = $list['uom2_htype_2'];
				$this->view->data->uom2_htype_3 = $list['uom2_htype_3'];
				$this->view->data->uom2_htype_4 = $list['uom2_htype_4'];
				$this->view->data->uom2_htype_5 = $list['uom2_htype_5'];
				$this->view->data->uom2_rack_type = $list['uom2_rack_type'];
				$this->view->data->uom2_shelfing_rack_type = $list['uom2_shelfing_rack_type'];
				$this->view->data->uom2_conversion = $list['uom2_conversion'];
				$this->view->data->uom2_internal_barcode = $list['uom2_internal_barcode'];
				$this->view->data->uom2_width = $list['uom2_width'];
				$this->view->data->uom2_length = $list['uom2_length'];
				$this->view->data->uom2_height = $list['uom2_height'];
				$this->view->data->uom2_weight = $list['uom2_weight'];
				$this->view->data->uom2_margin = $list['uom2_margin'];
				$this->view->data->uom2_box_type = $list['uom2_box_type'];
				$this->view->data->uom2_harga = $list['uom2_harga'];
				$this->view->data->uom3_nm = $list['uom3_nm'];
				$this->view->data->uom3_htype_1 = $list['uom3_htype_1'];
				$this->view->data->uom3_htype_2 = $list['uom3_htype_2'];
				$this->view->data->uom3_htype_3 = $list['uom3_htype_3'];
				$this->view->data->uom3_htype_4 = $list['uom3_htype_4'];
				$this->view->data->uom3_htype_5 = $list['uom3_htype_5'];
				$this->view->data->uom3_rack_type = $list['uom3_rack_type'];
				$this->view->data->uom3_shelfing_rack_type = $list['uom3_shelfing_rack_type'];
				$this->view->data->uom3_conversion = $list['uom3_conversion'];
				$this->view->data->uom3_internal_barcode = $list['uom3_internal_barcode'];
				$this->view->data->uom3_width = $list['uom3_width'];
				$this->view->data->uom3_length = $list['uom3_length'];
				$this->view->data->uom3_height = $list['uom3_height'];
				$this->view->data->uom3_weight = $list['uom3_weight'];
				$this->view->data->uom3_margin = $list['uom3_margin'];
				$this->view->data->uom3_box_type = $list['uom3_box_type'];
				$this->view->data->uom3_harga = $list['uom3_harga'];
				$this->view->data->uom4_nm = $list['uom4_nm'];
				$this->view->data->uom4_htype_1 = $list['uom4_htype_1'];
				$this->view->data->uom4_htype_2 = $list['uom4_htype_2'];
				$this->view->data->uom4_htype_3 = $list['uom4_htype_3'];
				$this->view->data->uom4_htype_4 = $list['uom4_htype_4'];
				$this->view->data->uom4_htype_5 = $list['uom4_htype_5'];
				$this->view->data->uom4_rack_type = $list['uom4_rack_type'];
				$this->view->data->uom4_shelfing_rack_type = $list['uom4_shelfing_rack_type'];
				$this->view->data->uom4_conversion = $list['uom4_conversion'];
				$this->view->data->uom4_internal_barcode = $list['uom4_internal_barcode'];
				$this->view->data->uom4_width = $list['uom4_width'];
				$this->view->data->uom4_length = $list['uom4_length'];
				$this->view->data->uom4_height = $list['uom4_height'];
				$this->view->data->uom4_weight = $list['uom4_weight'];
				$this->view->data->uom4_margin = $list['uom4_margin'];
				$this->view->data->uom4_box_type = $list['uom4_box_type'];
				$this->view->data->uom4_harga = $list['uom4_harga'];
				$this->view->data->uom1_prod_nm = $list['uom1_prod_nm'];
				$this->view->data->uom2_prod_nm = $list['uom2_prod_nm'];
				$this->view->data->uom3_prod_nm = $list['uom3_prod_nm'];
				$this->view->data->uom4_prod_nm = $list['uom4_prod_nm'];			
			}
		}
		
		$product_price = new ProductPrice();
		$product_price->stock_id = $_REQUEST['Stock_ID'];
		$lists = $product_price::getFirst($product_price);
		
		$this->view->price = new ProductPrice();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->price->stock_id = $list['stock_id'];
				$this->view->price->sales_type_id = $list['sales_type_id'];
				$this->view->price->curr_abrev = $list['curr_abrev'];
				$this->view->price->price = $list['price'];
				$this->view->price->margin = $list['margin'];
				$this->view->price->average_cost = $list['average_cost'];
				
				$this->view->price->uom1_suggested_price = $list['uom1_suggested_price'];
				$this->view->price->uom1_rounding_price = $list['uom1_rounding_price'];
				$this->view->price->uom1_changed_price = $list['uom1_changed_price'];
				$this->view->price->uom1_margin_amount = $list['uom1_margin_amount'];
				$this->view->price->uom1_cost_price = $list['uom1_cost_price'];
				
				$this->view->price->uom2_suggested_price = $list['uom2_suggested_price'];
				$this->view->price->uom2_rounding_price = $list['uom2_rounding_price'];
				$this->view->price->uom2_changed_price = $list['uom2_changed_price'];
				$this->view->price->uom2_margin_amount = $list['uom2_margin_amount'];
				$this->view->price->uom2_cost_price = $list['uom2_cost_price'];
				
				$this->view->price->uom3_suggested_price = $list['uom3_suggested_price'];
				$this->view->price->uom3_rounding_price = $list['uom3_rounding_price'];
				$this->view->price->uom3_changed_price = $list['uom3_changed_price'];
				$this->view->price->uom3_margin_amount = $list['uom3_margin_amount'];
				$this->view->price->uom3_cost_price = $list['uom3_cost_price'];
				
				$this->view->price->uom4_suggested_price = $list['uom4_suggested_price'];
				$this->view->price->uom4_rounding_price = $list['uom4_rounding_price'];
				$this->view->price->uom4_changed_price = $list['uom4_changed_price'];
				$this->view->price->uom4_margin_amount = $list['uom4_margin_amount'];
				$this->view->price->uom4_cost_price = $list['uom4_cost_price'];
	
			}
		}
		
		// get list unit
		$unit = new Unit();
		$lists = $unit::getAll();
		
		$data = null;
		foreach($lists as $list){
			$data[$list['unit']] = $list['unit'];
		}
		
		$this->view->unit = $data;
	}
	
	public function viewAction()
	{
		$this->AuthorityAction();
		
		$product = new Product();
		$product->stock_id = $_REQUEST['Stock_ID'];
		$lists = $product::getFirst($product);
		
		$this->view->data = new Product();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->stock_id = $list['stock_id'];				
				$this->view->data->category_id = $list['category_id'];
				$this->view->data->tax_type_id = $list['tax_type_id'];
				$this->view->data->description = $list['description'];
				$this->view->data->long_description = $list['long_description'];
				$this->view->data->units = $list['units'];
				$this->view->data->actual_cost = $list['actual_cost'];
				$this->view->data->last_cost = $list['last_cost'];
				$this->view->data->material_cost = $list['material_cost'];
				$this->view->data->labour_cost = $list['labour_cost'];
				$this->view->data->overhead_cost = $list['overhead_cost'];
				$this->view->data->inactive = $list['inactive'];
				$this->view->data->no_sale = $list['no_sale'];
				$this->view->data->editable = $list['editable'];
				$this->view->data->jenis_item = $list['jenis_item'];
				$this->view->data->bumun_cd = $list['bumun_cd'];
				$this->view->data->l1_cd = $list['l1_cd'];
				$this->view->data->l2_cd = $list['l2_cd'];
				$this->view->data->l3_cd = $list['l3_cd'];
				$this->view->data->l4_cd = $list['l4_cd'];
				$this->view->data->margin = $list['margin'];
				$this->view->data->moving_type = $list['moving_type'];
				$this->view->data->ass_type_1 = $list['ass_type_1'];
				$this->view->data->ass_type_2 = $list['ass_type_2'];
				$this->view->data->ass_type_3 = $list['ass_type_3'];
				$this->view->data->expiry_product = $list['expiry_product'];
				$this->view->data->weight_product = $list['weight_product'];
				$this->view->data->batch_product = $list['batch_product'];
				$this->view->data->active_status = $list['active_status'];
				$this->view->data->kode_1 = $list['kode_1'];
				$this->view->data->kode_2 = $list['kode_2'];
				$this->view->data->kode_3 = $list['kode_3'];
				$this->view->data->discount = $list['discount'];
				$this->view->data->supplied_by = $list['supplied_by'];
				$this->view->data->uom1_nm = $list['uom1_nm'];
				$this->view->data->uom1_htype_1 = $list['uom1_htype_1'];
				$this->view->data->uom1_htype_2 = $list['uom1_htype_2'];
				$this->view->data->uom1_htype_3 = $list['uom1_htype_3'];
				$this->view->data->uom1_htype_4 = $list['uom1_htype_4'];
				$this->view->data->uom1_htype_5 = $list['uom1_htype_5'];
				$this->view->data->uom1_rack_type = $list['uom1_rack_type'];
				$this->view->data->uom1_shelfing_rack_type = $list['uom1_shelfing_rack_type'];
				$this->view->data->uom1_internal_barcode = $list['uom1_internal_barcode'];
				$this->view->data->uom1_width = $list['uom1_width'];
				$this->view->data->uom1_length = $list['uom1_length'];
				$this->view->data->uom1_height = $list['uom1_height'];
				$this->view->data->uom1_weight = $list['uom1_weight'];
				$this->view->data->uom1_margin = $list['uom1_margin'];
				$this->view->data->uom1_box_type = $list['uom1_box_type'];
				$this->view->data->uom1_harga = $list['uom1_harga'];
				$this->view->data->uom2_nm = $list['uom2_nm'];
				$this->view->data->uom2_htype_1 = $list['uom2_htype_1'];
				$this->view->data->uom2_htype_2 = $list['uom2_htype_2'];
				$this->view->data->uom2_htype_3 = $list['uom2_htype_3'];
				$this->view->data->uom2_htype_4 = $list['uom2_htype_4'];
				$this->view->data->uom2_htype_5 = $list['uom2_htype_5'];
				$this->view->data->uom2_rack_type = $list['uom2_rack_type'];
				$this->view->data->uom2_shelfing_rack_type = $list['uom2_shelfing_rack_type'];
				$this->view->data->uom2_conversion = $list['uom2_conversion'];
				$this->view->data->uom2_internal_barcode = $list['uom2_internal_barcode'];
				$this->view->data->uom2_width = $list['uom2_width'];
				$this->view->data->uom2_length = $list['uom2_length'];
				$this->view->data->uom2_height = $list['uom2_height'];
				$this->view->data->uom2_weight = $list['uom2_weight'];
				$this->view->data->uom2_margin = $list['uom2_margin'];
				$this->view->data->uom2_box_type = $list['uom2_box_type'];
				$this->view->data->uom2_harga = $list['uom2_harga'];
				$this->view->data->uom3_nm = $list['uom3_nm'];
				$this->view->data->uom3_htype_1 = $list['uom3_htype_1'];
				$this->view->data->uom3_htype_2 = $list['uom3_htype_2'];
				$this->view->data->uom3_htype_3 = $list['uom3_htype_3'];
				$this->view->data->uom3_htype_4 = $list['uom3_htype_4'];
				$this->view->data->uom3_htype_5 = $list['uom3_htype_5'];
				$this->view->data->uom3_rack_type = $list['uom3_rack_type'];
				$this->view->data->uom3_shelfing_rack_type = $list['uom3_shelfing_rack_type'];
				$this->view->data->uom3_conversion = $list['uom3_conversion'];
				$this->view->data->uom3_internal_barcode = $list['uom3_internal_barcode'];
				$this->view->data->uom3_width = $list['uom3_width'];
				$this->view->data->uom3_length = $list['uom3_length'];
				$this->view->data->uom3_height = $list['uom3_height'];
				$this->view->data->uom3_weight = $list['uom3_weight'];
				$this->view->data->uom3_margin = $list['uom3_margin'];
				$this->view->data->uom3_box_type = $list['uom3_box_type'];
				$this->view->data->uom3_harga = $list['uom3_harga'];
				$this->view->data->uom4_nm = $list['uom4_nm'];
				$this->view->data->uom4_htype_1 = $list['uom4_htype_1'];
				$this->view->data->uom4_htype_2 = $list['uom4_htype_2'];
				$this->view->data->uom4_htype_3 = $list['uom4_htype_3'];
				$this->view->data->uom4_htype_4 = $list['uom4_htype_4'];
				$this->view->data->uom4_htype_5 = $list['uom4_htype_5'];
				$this->view->data->uom4_rack_type = $list['uom4_rack_type'];
				$this->view->data->uom4_shelfing_rack_type = $list['uom4_shelfing_rack_type'];
				$this->view->data->uom4_conversion = $list['uom4_conversion'];
				$this->view->data->uom4_internal_barcode = $list['uom4_internal_barcode'];
				$this->view->data->uom4_width = $list['uom4_width'];
				$this->view->data->uom4_length = $list['uom4_length'];
				$this->view->data->uom4_height = $list['uom4_height'];
				$this->view->data->uom4_weight = $list['uom4_weight'];
				$this->view->data->uom4_margin = $list['uom4_margin'];
				$this->view->data->uom4_box_type = $list['uom4_box_type'];
				$this->view->data->uom4_harga = $list['uom4_harga'];
				$this->view->data->uom1_prod_nm = $list['uom1_prod_nm'];
				$this->view->data->uom2_prod_nm = $list['uom2_prod_nm'];
				$this->view->data->uom3_prod_nm = $list['uom3_prod_nm'];
				$this->view->data->uom4_prod_nm = $list['uom4_prod_nm'];
			}
		}
		
		$product_price = new ProductPrice();
		$product_price->stock_id = $_REQUEST['Stock_ID'];
		$lists = $product_price::getFirst($product_price);
		$this->view->price = $lists;
		// if(count($lists) > 0){
		// 	foreach($lists as $list){
		// 		$this->view->price->stock_id = $list['stock_id'];
		// 		$this->view->price->sales_type_id = $list['sales_type_id'];
		// 		$this->view->price->curr_abrev = $list['curr_abrev'];
		// 		$this->view->price->price = $list['price'];
		// 		$this->view->price->margin = $list['margin'];
		// 		$this->view->price->average_cost = $list['average_cost'];
				
		// 		$this->view->price->uom1_suggested_price = $list['uom1_suggested_price'];
		// 		$this->view->price->uom1_rounding_price = $list['uom1_rounding_price'];
		// 		$this->view->price->uom1_changed_price = $list['uom1_changed_price'];
		// 		$this->view->price->uom1_margin_amount = $list['uom1_margin_amount'];
		// 		$this->view->price->uom1_cost_price = $list['uom1_cost_price'];
				
		// 		$this->view->price->uom2_suggested_price = $list['uom2_suggested_price'];
		// 		$this->view->price->uom2_rounding_price = $list['uom2_rounding_price'];
		// 		$this->view->price->uom2_changed_price = $list['uom2_changed_price'];
		// 		$this->view->price->uom2_margin_amount = $list['uom2_margin_amount'];
		// 		$this->view->price->uom2_cost_price = $list['uom2_cost_price'];
				
		// 		$this->view->price->uom3_suggested_price = $list['uom3_suggested_price'];
		// 		$this->view->price->uom3_rounding_price = $list['uom3_rounding_price'];
		// 		$this->view->price->uom3_changed_price = $list['uom3_changed_price'];
		// 		$this->view->price->uom3_margin_amount = $list['uom3_margin_amount'];
		// 		$this->view->price->uom3_cost_price = $list['uom3_cost_price'];
				
		// 		$this->view->price->uom4_suggested_price = $list['uom4_suggested_price'];
		// 		$this->view->price->uom4_rounding_price = $list['uom4_rounding_price'];
		// 		$this->view->price->uom4_changed_price = $list['uom4_changed_price'];
		// 		$this->view->price->uom4_margin_amount = $list['uom4_margin_amount'];
		// 		$this->view->price->uom4_cost_price = $list['uom4_cost_price'];
	
		// 	}
		// }
	}
	
	public function deleteAction()
	{
		$this->AuthorityAction();
		
		$product = new Product();
		$product->stock_id = $_REQUEST["Stock_ID"];
		$success = $product::goDelete($product);
		
		$this->response->redirect(uri('Product'));
		
	}
	
	public function insertAction()
	{
		$product = new Product();
		$product->stock_id = $this->request->getPost("Stock_ID");
		$product->category_id = $this->request->getPost("Category_ID");
		$product->tax_type_id = $this->request->getPost("Tax_Type_ID");
		$product->description = $this->request->getPost("Description");
		$product->long_description = $this->request->getPost("Long_Description");
		$product->units = $this->request->getPost("Units");
		$product->actual_cost = $this->request->getPost("Actual_Cost");
		$product->last_cost = $this->request->getPost("Last_Cost");
		$product->material_cost = $this->request->getPost("Material_Cost");
		$product->labour_cost = $this->request->getPost("Labour_Cost");
		$product->overhead_cost = $this->request->getPost("Overhead_Cost");
		$product->kode_1 = $this->request->getPost("Kode_1");
		$product->kode_2 = $this->request->getPost("Kode_2");
		$product->kode_3 = $this->request->getPost("Kode_3");
		$product->discount = $this->request->getPost("Discount");
		$product->supplied_by = $this->request->getPost("Supplied_By");
		$success = $product::goInsert($product);
		
		if($success){			
			$this->response->redirect(uri('Product'));
		}else{						
			$this->response->redirect(uri('Product/Add'));
		}
	}
	
	public function updateAction()
	{
		$product = new Product();
		$product->stock_id = $this->request->getPost("Stock_ID");
		$product->category_id = $this->request->getPost("Category_ID");
		$product->tax_type_id = $this->request->getPost("Tax_Type_ID");
		$product->description = $this->request->getPost("Description");
		$product->long_description = $this->request->getPost("Long_Description");
		$product->units = $this->request->getPost("Units");
		$product->actual_cost = $this->request->getPost("Actual_Cost");
		$product->last_cost = $this->request->getPost("Last_Cost");
		$product->material_cost = $this->request->getPost("Material_Cost");
		$product->labour_cost = $this->request->getPost("Labour_Cost");
		$product->overhead_cost = $this->request->getPost("Overhead_Cost");
		$product->kode_1 = $this->request->getPost("Kode_1");
		$product->kode_2 = $this->request->getPost("Kode_2");
		$product->kode_3 = $this->request->getPost("Kode_3");
		$product->discount = $this->request->getPost("Discount");
		$product->supplied_by = $this->request->getPost("Supplied_By");
		$success = $product::goUpdate($product);
		
		if($success){			
			$this->response->redirect(uri('Product'));
		}else{						
			$this->response->redirect(uri('Product/Edit?Stock_ID='.$product->stock_id));
		}
	}
}
