<?php

use Phalcon\Mvc\Controller;

class POController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'PO' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$data = new PO();
		$dataPR = new PR();
		$dataPYR = new PembayaranTransaksi();

		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();

		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk = $list_tmuk['tmuk'];
			}
		}

		$curl = new CurlClass();
		$result = $curl->post($this->di['api']['link']."/api/po/get-opening", [
			'tmuk' => $tmuk->tmuk
		]);

		$obj = json_decode($result, true);
		$listOpening = '';
		if($obj['opening']['0']['flag_transaksi'] == 1){
			$listOpening = $obj['opening']['0']['nomor_transaksi'];
		}

		$curl = new CurlClass();
		$result = $curl->post($this->di['api']['link']."/api/pyr/get-pyrstatus", [
			'tmuk'	=> $tmuk->tmuk
		]);

		$o = json_decode($result, true);
		$conditionPO = " WHERE gr_no_immpos = '' ";
		$conditionPR = " WHERE request_no::varchar NOT IN (SELECT pr_no_immbo FROM t_po) AND reference ='' AND flag_delete = false ";
		$conditionPYR = " WHERE flag = true AND payment_no::varchar NOT IN (SELECT trans_no FROM m_product_move)";
		$conditionPYR1 = " WHERE flag = true AND payment_no::varchar IN (SELECT trans_no FROM m_product_move)";
		$this->view->listsPO = $data::getFreeSql($conditionPO);	
		$this->view->listsPR = $dataPR::getFreeSql($conditionPR);	
		$this->view->listsPyr = $o['message'];	
		$this->view->listsOpening = $listOpening;	
		$this->view->listsHistory = $data::getAll();	
		$this->view->listsHistoryPyr = $dataPYR::getFreeSql2($conditionPYR1);	
	}
	
	public function ajaxPOAction(){
		$data = array();	
		$condition = " WHERE po.po_no_immbo = '".$_GET['order']."' ";
		$data = PO::getJoin($condition);
		return json_encode($data);
	}
	
	public function ajaxPRAction(){
		$data = array();
		$data = PR::getJoin($_GET['request']);
		return json_encode($data);
	}

	public function getPYRAction(){
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';
			return json_encode($data);
		}else{
			$tmuk = new TMUK();
			$lists_tmuk = $tmuk::getALL();

			if(isset($lists_tmuk)){
				foreach($lists_tmuk as $list_tmuk){
					$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
					$tmuk->tmuk = $list_tmuk['tmuk'];
					$tmuk->supplier_id = $list_tmuk['supplier_id'];
				}
			}

			$pyr = $this->request->getPost('payment_no');
			$curl = new CurlClass();
			$result = $curl->post($this->di['api']['link']."/api/pyr/get-pyr", [
				'no_pyr' => $pyr,
				'tmuk'	=> $tmuk->tmuk
			]);
			$obj = json_decode($result, true);
			if($obj['message'] == null){
				$this->flashSession->success("Gagal;Data PYR belum di Proses.;error");
				return $this->response->redirect(uri('PO'));
			}else{
				foreach ($obj['message'] as $value) {
					$pyr = new PembayaranTransaksi();
					$pyr->payment_no = $value['nomor_pyr'];
					$pyr->flag_ho = 1;
					$condition = " WHERE \"payment_no\" = '".$this->request->getPost('payment_no')."' ";
					$cek_pyr = $pyr::getFreeSQL($condition)[0];
					// $cek = $pyr::goUpdate($pyr);
					foreach ($value['detail_pyr'] as $val) {
						$harga = $val['price'];
						$product_move = new ProductMove();
						$product_move->trans_no = $value['nomor_pyr'];
						$product_move->stock_id = $val['produk_kode'];
						$product_move->type = 'PYR';
						$product_move->reference = $value['nomor_pyr'];
						$product_move->qty = $val['qty'];
						$product_move->price = $harga;
						$moveQuery .= $product_move::insertSql($product_move);
						// $success = $product_move::goInsert($product_move);
						
						$product_move->stock_id = $val['produk_kode'];
						$cek_stock = $product_move::getStock($product_move);
						// $list_gr['delivery_date'] = '2018-07-01';
						$stok = new Stock();
						$condition = " WHERE \"tgl_gr\" = '".$cek_pyr['tanggal']."' AND \"stock_id\" = '".$val['produk_kode']."' ";
						$cek = $stok::getFreeSQL($condition);
						if($cek){
							$stok->tgl_gr = $cek_pyr['tanggal'];
							$stok->no_gr = $value['nomor_pyr'];
							$stok->stock_id = $val['produk_kode'];
							$stok->qty =$val['qty'] + $cek['0']['qty'];
							$stok->sisa =$val['qty'] + $cek['0']['sisa'];
							$stok->total = $cek_stock;
							$success = $stok::goUpdate($stok);
						}else{
							$stok->tgl_gr = $cek_pyr['tanggal'];
							$stok->no_gr = $value['nomor_pyr'];
							$stok->stock_id = $val['produk_kode'];
							$stok->qty =$val['qty'];
							$stok->sisa =$val['qty'];
							$stok->total = $cek_stock;
							$success = $stok::goInsert($stok);

						}

						$price = new ProductPrice();
						$condition = " WHERE \"stock_id\" = '".$val['produk_kode']."' ";
						$cek_price = $price::getFreeSQL($condition);


						$price->stock_id = $val['produk_kode'];
						$lists_price = $price::getFirst($price);
						$curl = new CurlClass();
						$result = $curl->post($this->di['api']['link']."/api/hpp/get-hpp", [
							'kode_produk' => $val['produk_kode'],
							'kode_tmuk' => $tmuk->tmuk
						]);
						$cek_hpp = json_decode($result, true);
						if($value['tipe'] == 001){
							$map = 0;
							$margin = 0;
							$suggest = 0;
							$changed = 0;

							if($cek_hpp['message']['map'] > 0){
								$map = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['map']));
							}
							if($cek_hpp['message']['margin_amount'] > 0){
								$margin = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['margin_amount']));
							}
							if($cek_hpp['message']['suggest_price'] > 0){
								$suggest = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['suggest_price']));
							}
							if($cek_hpp['message']['change_price'] > 0){
								$changed = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['change_price']));
							}
						}else{
							$map = ($harga / $val['qty']);
						}
						$abrev = 'IDR';
						$price = new ProductPrice();
						$price->stock_id = $val['produk_kode'];
						$price->sales_type_id = 1;
						$price->curr_abrev = $abrev;

						$harga_price = new CurlClass();
						$data_harga = $harga_price->post($this->di['api']['link']."/api/produk/get-price", [
							'tmuk' => $tmuk->tmuk,
							'kode_produk' => $val['produk_kode']
						]);
						$set_harga = json_decode($data_harga, true);
						$setting = new CurlClass();
						$data_setting = $setting->post($this->di['api']['link']."/api/produk/get-setting", [
							'kode_produk' => $val['produk_kode']
						]);

						$price->price = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($set_harga['message']));

						$set = json_decode($data_setting, true);
						foreach ($set['message'] as $val_ue) {
							$price->margin = $val_ue['margin'];
							$sell = $map*(1+($val_ue['margin']/100));
						}

						$price->average_cost = $map;
						$price->uom1_suggested_price = round($sell);
						$price->uom1_rounding_price = round(round($sell)/100) * 100;
						$price->uom1_changed_price = $changed;
						$price->uom1_margin_amount = $margin;
						$price->uom2_suggested_price = $suggest;
						$price->uom2_rounding_price = 0;
						$price->uom2_changed_price = 0;
						$price->uom2_margin_amount = 0;
						$price->uom3_suggested_price = 0;
						$price->uom3_rounding_price = 0;
						$price->uom3_changed_price = 0;
						$price->uom3_margin_amount = 0;
						$price->uom1_cost_price = 0;
						$price->uom2_cost_price = 0;
						$price->uom3_cost_price = 0;
						$price->uom4_suggested_price = 0;
						$price->uom4_rounding_price = 0;
						$price->uom4_changed_price = 0;
						$price->uom4_margin_amount = 0;
						$price->uom4_cost_price = 0;
						$price->uom1_member_price = 0;
						$price->uom2_member_price = 0;
						$price->uom3_member_price = 0;
						$price->uom4_member_price = 0;

						if($cek_price){
							$priceQuery .= $price::updateSql($price);
						}else{
							$priceQuery .= $price::insertSql($price);
						}
					}

					if($moveQuery != ''){
						$success = $product_move::executeQuery($moveQuery);
					}

					if($priceQuery != ''){
						$success = $price::executeQuery($priceQuery);
					}

					if($success){
						$this->flashSession->success("Sukses;Data PYR Berhasil diterima.;success");
						$hasil['message'] = 'Data <b>'.$value['nomor_pyr'].'</b> Berhasil Diterima';
						$notif = new Notifikasi();
						$notif->data = $hasil['message'];
						$notif->read_at = null;
						$notif->created_at = date('Y-m-d H:i:s');
						$notif->created_by = $this->session->get("user")['user_name'];

						$sukses = $notif::goInsert($notif);
						return $this->response->redirect(uri('PO'));
					}else{
						$this->flashSession->success("Gagal;Data PYR Gagal diterima.;error");
						return $this->response->redirect(uri('PO'));
					}
				}
			}
		}
	}
	
	public function getPOAction(){
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = '<b>Mohon Periksa Kembali Koneksi Anda !!!</b>';
			return json_encode($data);
		}else{
			set_time_limit(0);
			$pr = new PR();
			$pr->request_no = $this->request->getPost('pr_no');
			$lists = $pr::getFirst($pr);
			
			$listsTMUK = TMUK::getAll();
			foreach($listsTMUK as $listTMUK){
				$asn = $listTMUK['asn'];
			}
			
			if(count($lists > 0)){
				foreach($lists as $list){
					$pr->request_no = $list['request_no'];
					$pr->totalpr = $list['total'];
				}
			}		

			if($pr->request_no != ''){
				$nomorpr = $pr->request_no; 
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/po/send-po", [
						'nomorpr' => $nomorpr
				]);
				$obj = json_decode($result, true);

				if(!$result){
					echo 'CURL Error:'.curl_error($curlHandle);
				}else{
					if($asn != 2){
						if($obj['po'] == null){
							$this->flashSession->success("Gagal;Saat ini data PR masih dalam tahap pemrosesan di Lotte Grosir.;error");
						}else{
							$dataPO = new PO();
							$dataPOD = new PODetail();
							$dataProduct = new Product();
							foreach ($obj['po'] as $value) {
								$condition = " WHERE \"po_no_ref\" = '".$value['nomor']."' ";
								$cek = $dataPO::getFreeSQL($condition);
								if($cek == null){
									$dataPO->po_no_immbo = $value['id'];
									$dataPO->po_no_ref = $value['nomor'];
									// $dataPO->reference = $mj['post']['reference'];
									// $dataPO->supplier_id = $mj['post']['supplier_id'];
									$dataPO->order_date = $value['tgl_buat'];
									$dataPO->delivery_date = $value['created_at'];
									$dataPO->delivery_address = $value['created_at'];
									$dataPO->comments = $value['keterangan'];
									$dataPO->total_po = $value['total'];
									$dataPO->total_pr = $obj['pr']['total'];
									$dataPO->tax_included = 1;
									$dataPO->pr_no_immbo = $value['nomor_pr'];
									$dataPO->pr_no_immpos = $value['nomor_pr'];
									$success = $dataPO::goInsert($dataPO);
									foreach($value['detail'] as $mj){
										$dataPOD->po_detail_item = $mj['id'];
										$dataPOD->po_no_immbo = $value['id'];
										$dataPOD->item_code = $mj['produk_kode'];
										$dataPOD->description = $mj['produk']['nama'];
										$dataPOD->qty_po = $mj['qty_po'];
										$dataPOD->unit_po = $mj['unit_po'];
										$dataPOD->po_unit_price = $mj['price'];
										$dataPOD->po_price = $mj['qty_po'] * $mj['price'];
										$dataPOD->flag_gr_ok = 'false';
										$dataPOD->qty_pr = $mj['qty_pr'];

										$dataProduct->stock_id = $mj['produk_kode'];
										$dataProduct->uom1_harga = $mj['price'];

										$success = $dataProduct::goUpdate($dataProduct);
										$success = $dataPOD::goInsert($dataPOD);	

										// $PODQuery .= $dataPOD::insertSql($dataPOD);	
									}

									$pr_update = new PR();
									$pr_update->request_no = $value['nomor_pr'];
									$pr_update->po_no_immbo = $value['id'];
									$success = $pr_update::goUpdate($pr_update);
								}else{
									$this->flashSession->success("Berhasil;PO diterima, dan sudah di GR.;success");
									$hasil['message'] = 'Data <b>'.$value['nomor'].'</b> Berhasil Diterima';
									$notif = new Notifikasi();
									$notif->data = $hasil['message'];
									$notif->read_at = null;
									$notif->created_at = date('Y-m-d H:i:s');
									$notif->created_by = $this->session->get("user")['user_name'];

									$sukses = $notif::goInsert($notif);
								}
							}
							$this->flashSession->success("Berhasil;Data PO Berhasil diterima.;success");
							$hasil['message'] = 'Data <b>'.$value['nomor'].'</b> Berhasil Diterima';
							$notif = new Notifikasi();
							$notif->data = $hasil['message'];
							$notif->read_at = null;
							$notif->created_at = date('Y-m-d H:i:s');
							$notif->created_by = $this->session->get("user")['user_name'];

							$sukses = $notif::goInsert($notif);
						}	
					}else{
						if($obj['po'] == null){
							$this->flashSession->success("Gagal;Saat ini data PR masih dalam tahap pemrosesan di Lotte Grosir.;error");
						}else{
							$dataPO = new PO();
							$dataPOD = new PODetail();
							$dataProduct = new Product();
							foreach ($obj['po'] as $value) {
								try {
									$curl = new CurlClass();
									$result = $curl->post($this->di['api']['link']."/api/po/terima-po", [
										'po' => $value['id'],
									]);
									$obj = json_decode($result, true);
									// var_dump($obj);
									// die;
								} catch (Exception $e) {
									
								}
								$condition = " WHERE \"po_no_ref\" = '".$value['nomor']."' ";
								$cek = $dataPO::getFreeSQL($condition);
								if($cek == null){
									$dataPO->po_no_immbo = $value['id'];
									$dataPO->po_no_ref = $value['nomor'];
									// $dataPO->reference = $mj['post']['reference'];
									// $dataPO->supplier_id = $mj['post']['supplier_id'];
									$dataPO->order_date = $value['tgl_buat'];
									$dataPO->delivery_date = $value['created_at'];
									$dataPO->delivery_address = $value['created_at'];
									$dataPO->comments = $value['keterangan'];
									$dataPO->total_po = $value['total'];
									$dataPO->total_pr = $obj['pr']['total'];
									$dataPO->tax_included = 1;
									$dataPO->pr_no_immbo = $value['nomor_pr'];
									$dataPO->pr_no_immpos = $value['nomor_pr'];
									$success = $dataPO::goInsert($dataPO);
									foreach($value['detail'] as $mj){
										$dataPOD->po_detail_item = $mj['id'];
										$dataPOD->po_no_immbo = $value['id'];
										$dataPOD->item_code = $mj['produk_kode'];
										$dataPOD->description = $mj['produk']['nama'];
										$dataPOD->qty_po = $mj['qty_po'];
										$dataPOD->unit_po = $mj['unit_po'];
										$dataPOD->po_unit_price = $mj['price'];
										$dataPOD->po_price = $mj['qty_po'] * $mj['price'];
										$dataPOD->flag_gr_ok = 'false';
										$dataPOD->qty_pr = $mj['qty_pr'];

										$dataProduct->stock_id = $mj['produk_kode'];
										$dataProduct->uom1_harga = $mj['price'];

										$success = $dataProduct::goUpdate($dataProduct);
										// $PODQuery .= $dataPOD::insertSql($dataPOD);
										$success = $dataPOD::goInsert($dataPOD);			
									}

										$number_range = new NumberRange();
										$number_range->trans_type = 'GR';
										$number_range->period = date('Ym');
										$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
										$lists = $number_range::getFreeSQL($condition);

										if(isset($lists)){
											foreach($lists as $list){
												$number_range->trans_type = $list['trans_type'];
												$number_range->period = $list['period'];
												$number_range->prefix = $list['prefix'];
												$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
											}
										}else{
											$number_range->trans_type = 'GR';
											$number_range->period = date('Ym');
											$number_range->prefix = 'GR';
											$number_range->from_ = '1';
											$number_range->to_ = '9999';
											$number_range->current_no = '1';
											$success = $number_range::goInsert($number_range);			
											$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
										}

										$tmuk = new TMUK();
										$lists_tmuk = $tmuk::getALL();

										if(isset($lists_tmuk)){
											foreach($lists_tmuk as $list_tmuk){
												$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
												$tmuk->tmuk = $list_tmuk['tmuk'];
												$tmuk->supplier_id = $list_tmuk['supplier_id'];
											}
										}

										$gr = new GR();
										$gr->gr_no = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;	
										$gr->create_by = $this->session->get("user")['user_name'];
										$gr->delivery_date = $dataPO->delivery_date;		
										$gr->comments = $dataPO->comments;	
										$gr->pr_no_immpos = $dataPO->pr_no_immpos;
										$gr->pr_no_immbo = $dataPO->pr_no_immbo;
										$gr->po_no_immbo = $dataPO->po_no_immbo;
										$gr->total_pr = $dataPO->total_pr;
										$gr->total_po = $dataPO->total_po;
										$gr->flag_sync = true;
										$gr->gr_no_immbo = $dataPO->gr_no_immbo;
										$success = $gr::goInsert($gr);

										if($success){			
											$success_num = $number_range::goAddNumber($number_range);

											$po = new PO();
											$po->po_no_immbo = $gr->po_no_immbo;
											$po->gr_no_immpos = $gr->gr_no;
											$success = $po::goUpdate($po);	

											$number_range = new NumberRange();
											$number_range->trans_type = 'MV';
											$number_range->period = date('Ym');
											$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
											$lists = $number_range::getFreeSQL($condition);

											if(isset($lists)){
												foreach($lists as $list){
													$number_range->trans_type = $list['trans_type'];
													$number_range->period = $list['period'];
													$number_range->prefix = $list['prefix'];
													$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
												}
											}else{
												$number_range->trans_type = 'MV';
												$number_range->period = date('Ym');
												$number_range->prefix = 'MV';
												$number_range->from_ = '1';
												$number_range->to_ = '9999';
												$number_range->current_no = '1';
												$success = $number_range::goInsert($number_range);			
												$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
											}

											$POD = new PODetail();
											$condition = " WHERE \"po_no_immbo\" = '".$gr->po_no_immbo."' ";
											$lists_po_detail = $POD::getFreeSQL($condition);

											$count = 0;
											$rg_stock_id = '';
											foreach($lists_po_detail as $list_po_detail){
												if($count>0){ $rg_stock_id .= ','; }
												$rg_stock_id .= "'".$list_po_detail['item_code']."'";
												$count++;
											}				

											if(isset($rg_stock_id)){
												$product = new Product();
												$condition = " WHERE \"stock_id\" IN (".$rg_stock_id.") ";
												$lists_product = $product::getFreeSQL($condition);
											}
											foreach($lists_po_detail as $list_po_detail){
												$newDate = date("Y-m-d", strtotime($dataPO->delivery_date));	
												$stock_qty = 0;
												$order_qty = $list_po_detail['qty_po'];
												$stock_con = 1;
												$order_con = 1;
												foreach($lists_product as $list_product){
													if($list_product['stock_id'] == $list_po_detail['item_code']){
														$stock_qty = ( $order_qty * $order_con ) / $stock_con; 
													}
												}

												$prod = new Product();
												$condition = " WHERE \"stock_id\" = '".$list_po_detail['item_code']."' ";
												$data_prod= $prod::getFreeSQL($condition);

												$product_move = new ProductMove();		
												$product_move->trans_no = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;
												$product_move->stock_id = $list_po_detail['item_code'];
												$product_move->type = 'GR';
												$product_move->reference = $gr->gr_no;
												foreach ($data_prod as $key => $val_prod) {
													$product_move->qty = $stock_qty * $val_prod['uom2_conversion'];
												}
												$product_move->price = $list_po_detail['po_unit_price'];
												$moveQuery .= $product_move::insertSql($product_move);
												// $success = $product_move::goInsert($product_move);
												
												$product_move->stock_id = $list_po_detail['item_code'];
												$cek_stock = $product_move::getStock($product_move);
												// $list_gr['delivery_date'] = '2018-07-01';
												$stok = new Stock();
												$condition = " WHERE \"tgl_gr\" = '".$newDate."' AND \"stock_id\" = '".$list_po_detail['item_code']."' ";
												$cek = $stok::getFreeSQL($condition);
												if($cek){
													$stok->tgl_gr = $newDate;
													$stok->no_gr = $gr->gr_no;
													$stok->stock_id = $list_po_detail['item_code'];
													$stok->qty = $list_po_detail['qty_pr'] + $cek['0']['qty'];
													$stok->sisa = $list_po_detail['qty_pr'] + $cek['0']['sisa'];
													$stok->total = $cek_stock;
													$success = $stok::goUpdate($stok);
												}else{
													$stok->tgl_gr = $newDate;
													$stok->no_gr = $gr->gr_no;
													$stok->stock_id = $list_po_detail['item_code'];
													$stok->qty = $list_po_detail['qty_pr'];
													$stok->sisa = $list_po_detail['qty_pr'];
													$stok->total = $cek_stock;
													$success = $stok::goInsert($stok);

												}

												$price = new ProductPrice();
												$condition = " WHERE \"stock_id\" = '".$list_po_detail['item_code']."' ";
												$cek_price = $price::getFreeSQL($condition);

												// if($cek_price){
												$price->stock_id = $list_po_detail['item_code'];
												$lists_price = $price::getFirst($price);
												$curl = new CurlClass();
												$result = $curl->post($this->di['api']['link']."/api/hpp/get-hpp", [
													'kode_produk' => $list_po_detail['item_code'],
													'kode_tmuk' => $tmuk->tmuk
												]);
												$cek_hpp = json_decode($result, true);
												$map = 0;
												$margin = 0;
												$suggest = 0;
												$changed = 0;

												if($cek_hpp['message']['map'] > 0){
													$map = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['map']));
												}
												if($cek_hpp['message']['margin_amount'] > 0){
													$margin = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['margin_amount']));
												}
												if($cek_hpp['message']['suggest_price'] > 0){
													$suggest = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['suggest_price']));
												}
												if($cek_hpp['message']['change_price'] > 0){
													$changed = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['change_price']));
												}

												$abrev = 'IDR';
												$price = new ProductPrice();
												$price->stock_id = $list_po_detail['item_code'];
												$price->sales_type_id = 1;
												$price->curr_abrev = $abrev;

												$harga = new CurlClass();
												$data_harga = $harga->post($this->di['api']['link']."/api/produk/get-price", [
													'tmuk' => $tmuk->tmuk,
													'kode_produk' => $list_po_detail['item_code']
												]);
												$set_harga = json_decode($data_harga, true);
												$setting = new CurlClass();
												$data_setting = $setting->post($this->di['api']['link']."/api/produk/get-setting", [
													'kode_produk' => $list_po_detail['item_code']
												]);

												$price->price = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($set_harga['message']));

												$set = json_decode($data_setting, true);
												foreach ($set['message'] as $val) {
													$price->margin = $val['margin'];
													$sell = $map*(1+($val['margin']/100));
												}

												$price->average_cost = $map;
												$price->uom1_suggested_price = round($sell);
												$price->uom1_rounding_price = round(round($sell)/100) * 100;
												$price->uom1_changed_price = $changed;
												$price->uom1_margin_amount = $margin;
												$price->uom2_suggested_price = $suggest;
												$price->uom2_rounding_price = 0;
												$price->uom2_changed_price = 0;
												$price->uom2_margin_amount = 0;
												$price->uom3_suggested_price = 0;
												$price->uom3_rounding_price = 0;
												$price->uom3_changed_price = 0;
												$price->uom3_margin_amount = 0;
												$price->uom1_cost_price = 0;
												$price->uom2_cost_price = 0;
												$price->uom3_cost_price = 0;
												$price->uom4_suggested_price = 0;
												$price->uom4_rounding_price = 0;
												$price->uom4_changed_price = 0;
												$price->uom4_margin_amount = 0;
												$price->uom4_cost_price = 0;
												$price->uom1_member_price = 0;
												$price->uom2_member_price = 0;
												$price->uom3_member_price = 0;
												$price->uom4_member_price = 0;
												if($cek_price){
													$priceQuery .= $price::updateSql($price);
												}else{
													$priceQuery .= $price::insertSql($price);
												}
											}

											if($moveQuery != ''){
												$success = $product_move::executeQuery($moveQuery);
											}

											if($priceQuery != ''){
												$success = $price::executeQuery($priceQuery);
											}
											// $this->flashSession->success("Berhasil;PO diterima, dan sudah di GR.;success");
											$pr_update = new PR();
											$pr_update->request_no = $value['nomor_pr'];
											$pr_update->po_no_immbo = $value['id'];
											$success = $pr_update::goUpdate($pr_update);
										}else{						
										}				
								}else{
								}
							}
							$this->flashSession->success("Berhasil;PO diterima, dan sudah di GR.;success");
							$hasil['message'] = 'Data <b>'.$value['nomor'].'</b> Berhasil Dirterima';
							$notif = new Notifikasi();
							$notif->data = $hasil['message'];
							$notif->read_at = null;
							$notif->created_at = date('Y-m-d H:i:s');
							$notif->created_by = $this->session->get("user")['user_name'];

							$sukses = $notif::goInsert($notif);
						}						
					}
				}
			}else{
				$this->flashSession->success("Gagal;Nomor PR belum di Sync.;error");
			} 
		}	

		return $this->response->redirect(uri('PO'));
	}

	public function getPOOpeningAction(){
		$nomor = $this->request->getPost('po_opening');
		$tmuk = new TMUK();
		$lists = $tmuk::getAll();

		if(isset($lists)){
			foreach($lists as $list){
				$tmuk->tmuk_id = $list['tmuk_id'];
				$tmuk->tmuk = $list['tmuk'];
			}
		}

		$curl = new CurlClass();
		$result = $curl->post($this->di['api']['link']."/api/po/get-opening", [
			'tmuk' => $tmuk->tmuk
		]);

		$obj = json_decode($result, true);
		if($obj['opening']['0']['flag_transaksi'] == 1){
			if(count($lists) > 0){	
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/po/get-po", [
						'tmuk' => $tmuk->tmuk,
						'kode' => $nomor
				]);

				$obj = json_decode($result, true);
				if(count($obj['po']) != 0){
					foreach ($obj['po'] as $key => $value) {
						$number_range = new NumberRange();
						$number_range->trans_type = 'MV';
						$number_range->period = date('Ym');
						$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
						$lists = $number_range::getFreeSQL($condition);

						if(isset($lists)){
							foreach($lists as $list){
								$number_range->trans_type = $list['trans_type'];
								$number_range->period = $list['period'];
								$number_range->prefix = $list['prefix'];
								$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
							}
						}else{
							$number_range->trans_type = 'MV';
							$number_range->period = date('Ym');
							$number_range->prefix = 'MV';
							$number_range->from_ = '1';
							$number_range->to_ = '9999';
							$number_range->current_no = '1';
							$success = $number_range::goInsert($number_range);			
							$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
						}
						foreach ($value['detail'] as $key => $val) {
							$product_move = new ProductMove();		
							$product_move->trans_no = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;
							$product_move->stock_id = $val['produk_kode'];
							$product_move->type = 'GR';
							$product_move->qty = $val['qty_po'];
							$product_move->price = $val['price'];
							// $success = $product_move::goInsert($product_move);
							$moveQuery .= $product_move::insertSql($product_move);
							$success_num = $number_range::goAddNumber($number_range);
						}
					}
					if($moveQuery != ''){
						$success = $product_move::executeQuery($moveQuery);
					}
					
					$curl = new CurlClass();
					$result = $curl->post($this->di['api']['link']."/api/po/send-opening", [
						'tmuk' => $tmuk->tmuk
					]);
					$product = new Product();
					$product_move = new ProductMove();
					$price = new ProductPrice();

					$curl = new CurlClass();
					$result = $curl->post($this->di['api']['link']."/api/produk/sync-produk", [
						'tmuk' => $tmuk->tmuk
					]);
					$obj = json_decode($result, true);

					$productQuery = '';
					$priceQuery = '';
					$moveQuery = '';
					$count = 0;
					if (isset($obj['message'])) {
						foreach ($obj['message'] as $value) {
							$condition = " WHERE \"stock_id\" = '".$value['produk']['kode']."' ";
							$cek = $product::getFreeSQL($condition);
							$product->stock_id = $value['produk']['kode'];
							$product->description = $value['produk']['nama'];
							$product->bumun_cd = $value['produk']['bumun_cd'];
							$product->l1_cd = $value['produk']['l1_cd'];
							$product->l2_cd = $value['produk']['l2_cd'];
							$product->l3_cd = $value['produk']['l3_cd'];
							$product->l4_cd = $value['produk']['l4_cd'];
							$product->margin = $value['produk']['produksetting']['margin'];
							$product->uom1_nm = $value['produk']['produksetting']['uom1_satuan'];
							$product->uom2_nm = $value['produk']['produksetting']['uom2_satuan'];
							$product->uom3_nm = $value['produk']['produksetting']['uom3_satuan'];
							$product->uom4_nm = $value['produk']['produksetting']['uom4_satuan'];
							$product->uom4_nm = $value['produk']['produksetting']['uom4_satuan'];
							$product->uom1_conversion = 1;
							$product->uom2_conversion = $value['produk']['produksetting']['uom2_conversion'];
							$product->uom3_conversion = $value['produk']['produksetting']['uom3_conversion'];
							$product->uom4_conversion = $value['produk']['produksetting']['uom4_conversion'];
							$product->uom4_conversion = $value['produk']['produksetting']['uom4_conversion'];
							$product->uom1_internal_barcode = $value['produk']['produksetting']['uom1_barcode'];
							$product->uom2_internal_barcode = $value['produk']['produksetting']['uom2_barcode'];
							$product->uom3_internal_barcode = $value['produk']['produksetting']['uom3_barcode'];
							$product->uom4_internal_barcode = $value['produk']['produksetting']['uom4_barcode'];
							$product->uom4_internal_barcode = $value['produk']['produksetting']['uom4_barcode'];
							$product->uom1_width = $value['produk']['produksetting']['uom1_width'];
							$product->uom2_width = $value['produk']['produksetting']['uom2_width'];
							$product->uom3_width = $value['produk']['produksetting']['uom3_width'];
							$product->uom4_width = $value['produk']['produksetting']['uom4_width'];
							$product->uom4_width = $value['produk']['produksetting']['uom4_width'];
							$product->uom1_length = $value['produk']['produksetting']['uom1_length'];
							$product->uom2_length = $value['produk']['produksetting']['uom2_length'];
							$product->uom3_length = $value['produk']['produksetting']['uom3_length'];
							$product->uom4_length = $value['produk']['produksetting']['uom4_length'];
							$product->uom4_length = $value['produk']['produksetting']['uom4_length'];
							$product->uom1_height = $value['produk']['produksetting']['uom1_height'];
							$product->uom2_height = $value['produk']['produksetting']['uom2_height'];
							$product->uom3_height = $value['produk']['produksetting']['uom3_height'];
							$product->uom4_height = $value['produk']['produksetting']['uom4_height'];
							$product->uom4_height = $value['produk']['produksetting']['uom4_height'];
							$product->uom1_weight = $value['produk']['produksetting']['uom1_weight'];
							$product->uom2_weight = $value['produk']['produksetting']['uom2_weight'];
							$product->uom3_weight = $value['produk']['produksetting']['uom3_weight'];
							$product->uom4_weight = $value['produk']['produksetting']['uom4_weight'];
							$product->uom1_htype_1 = $value['produk']['produksetting']['uom1_selling_cek'];
							$product->uom1_htype_2 = $value['produk']['produksetting']['uom1_order_cek'];
							$product->uom1_htype_3 = $value['produk']['produksetting']['uom1_receiving_cek'];
							$product->uom1_htype_4 = $value['produk']['produksetting']['uom1_return_cek'];
							$product->uom1_htype_5 = $value['produk']['produksetting']['uom1_inventory_cek'];
							$product->uom2_htype_1 = $value['produk']['produksetting']['uom2_selling_cek'];
							$product->uom2_htype_2 = $value['produk']['produksetting']['uom2_order_cek'];
							$product->uom2_htype_3 = $value['produk']['produksetting']['uom2_receiving_cek'];
							$product->uom2_htype_4 = $value['produk']['produksetting']['uom2_return_cek'];
							$product->uom2_htype_5 = $value['produk']['produksetting']['uom2_inventory_cek'];
							$product->uom1_prod_nm = $value['produk']['produksetting']['uom1_produk_description'];
							$product->uom2_prod_nm = $value['produk']['produksetting']['uom2_produk_description'];
							$product->uom1_harga = ($value['harga_jual']['cost_price'] != '') ? $value['harga_jual']['cost_price'] : 0;
							$product->uom2_harga = $value['harga_beli']['curr_sale_prc'];
							$product->status = $value['harga_beli']['status'];
							$product->tipe = $value['produk']['produksetting']['tipe_barang_kode'];
							$product->ppob = $value['produk']['produksetting']['ppob'];
							$map = 0;
							$margin = 0;
							$suggest = 0;
							$changed = 0;
							if($value['harga_jual']['map'] > 0){
								$map = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($value['harga_jual']['map']));
							}
							if($value['harga_jual']['margin_amount'] > 0){
								$margin = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($value['harga_jual']['margin_amount']));
							}
							if($value['harga_jual']['suggest_price'] > 0){
								$suggest = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($value['harga_jual']['suggest_price']));
							}
							if($value['harga_jual']['change_price'] > 0){
								$changed = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($value['harga_jual']['change_price']));
							}
							$sell = $map*(1+($value['produk']['produksetting']['margin']/100));
							$abrev = 'IDR';

							$price->stock_id = $value['produk']['kode'];
							$price->sales_type_id = 1;
							$price->curr_abrev = $abrev;
							$price->price = 0;
							if($value['produk']['produksetting']['uom2_conversion'] > 0){
								$price->price = round($value['harga_beli']['curr_sale_prc'] / $value['produk']['produksetting']['uom2_conversion']);
							}
							if($value['produk']['produksetting']['margin'] != ''){
								$price->margin = $value['produk']['produksetting']['margin'];
								$price->uom1_suggested_price = round($sell);
								$price->uom1_rounding_price = round(round($sell)/100) * 100;	
							}

							$price->average_cost = $map;
							$price->uom1_suggested_price = round($sell);
							$price->uom1_rounding_price = round(round($sell)/100) * 100;
							$price->uom1_changed_price = $changed;
							$price->uom1_margin_amount = $margin;
							$price->uom2_suggested_price = $suggest;
							$price->uom2_rounding_price = 0;
							$price->uom2_changed_price = 0;
							$price->uom2_margin_amount = 0;
							$price->uom3_suggested_price = 0;
							$price->uom3_rounding_price = 0;
							$price->uom3_changed_price = 0;
							$price->uom3_margin_amount = 0;
							$price->uom1_cost_price = 0;
							$price->uom2_cost_price = 0;
							$price->uom3_cost_price = 0;
							$price->uom4_suggested_price = 0;
							$price->uom4_rounding_price = 0;
							$price->uom4_changed_price = 0;
							$price->uom4_margin_amount = 0;
							$price->uom4_cost_price = 0;
							$price->uom1_member_price = 0;
							$price->uom2_member_price = 0;
							$price->uom3_member_price = 0;
							$price->uom4_member_price = 0;

							if($cek){
								$productQuery .= $product::updateSql($product);
								$priceQuery .= $price::updateSql($price);
							}else{
								$product_move->stock_id = $value['produk']['kode'];
								$productQuery .= $product::insertSql($product);
								$priceQuery .= $price::insertSql($price);
								$moveQuery .= $product_move::insertSql($product_move);
							}
						}
						if($productQuery != ''){
							$success = $product::executeQuery($productQuery);
						}
						if($moveQuery != ''){
							$success = $product_move::executeQuery($moveQuery);
						}
						if($priceQuery != ''){
							$success = $price::executeQuery($priceQuery);
						}
						$last_sync = new LastSync();
						$a = 'PRODUCT & PRICE';
						$condition = " WHERE \"tipe\" = '".$a."' ";
						$cek = $last_sync::getFreeSQL($condition);
						if($cek){
							$last_sync->tipe = 'PRODUCT & PRICE';
							$last_sync->sync_date = date('Y-m-d');
							$last_sync->sync_time = date('H:i:s');
							$success = $last_sync::goUpdate($last_sync);
						}else{
							$last_sync->id = 4;
							$last_sync->tipe = 'PRODUCT & PRICE';
							$last_sync->sync_date = date('Y-m-d');
							$last_sync->sync_time = date('H:i:s');
							$success = $last_sync::goInsert($last_sync);
						}

						$last_sync = new LastSync();
						$a = 'PO';
						$condition = " WHERE \"tipe\" = '".$a."' ";
						$cek = $last_sync::getFreeSQL($condition);
						if($cek){
							$last_sync->tipe = 'PO';
							$last_sync->sync_date = date('Y-m-d');
							$last_sync->sync_time = date('H:i:s');
							$success = $last_sync::goUpdate($last_sync);
						}else{
							$last_sync->id = 12;
							$last_sync->tipe = 'PO';
							$last_sync->sync_date = date('Y-m-d');
							$last_sync->sync_time = date('H:i:s');
							$success = $last_sync::goInsert($last_sync);
						}
						$this->flashSession->success("Sukses;Opening Toko Berhasil.;success");
						return $this->response->redirect(uri('PO'));	
					}else{
						$this->flashSession->success("Gagal;Gagal Ambil Data Dari Server.;error");
						return $this->response->redirect(uri('PO'));
					}			

				}else{
					$this->flashSession->success("Gagal;Nomor PO Tidak Terdaftar.;error");
					return $this->response->redirect(uri('PO'));
				}
			}else{
				$last_sync = new LastSync();
				$a = 'PO';
				$condition = " WHERE \"tipe\" = '".$a."' ";
				$cek = $last_sync::getFreeSQL($condition);
				if($cek){
					$last_sync->tipe = 'PO';
					$last_sync->sync_date = date('Y-m-d');
					$last_sync->sync_time = date('H:i:s');
					$success = $last_sync::goUpdate($last_sync);
				}else{
					$last_sync->id = 12;
					$last_sync->tipe = 'PO';
					$last_sync->sync_date = date('Y-m-d');
					$last_sync->sync_time = date('H:i:s');
					$success = $last_sync::goInsert($last_sync);
				}
				return 1;
			}
		}else{
			$this->flashSession->success("Gagal;Nomor PO Belum Selesai Di Proses.;error");
			return $this->response->redirect(uri('PO'));
		}
		
	}
	
	public function printAction()
	{
		$po = new PO();
		$condition = " WHERE po_no_immbo = ".$_GET['Order']." ";
		$lists_po = $po::getFreeSQL($condition);

		if(isset($lists_po)){
			foreach($lists_po as $list_po){
				$po->supplier_id = $list_po['supplier_id'];
			}
		}

		$supplier = new Supplier();
		$condition = " WHERE \"supplier_id\" = '".$po->supplier_id."' "; 
		$lists_suppplier = $supplier::getFreeSQL($condition);
		
		if(isset($lists_suppplier)){
			foreach($lists_suppplier as $list_suppplier){
				$supplier->supplier_id = $list_suppplier['supplier_id'];
				$supplier->supp_name = $list_suppplier['supp_name'];	
				$supplier->address = $list_suppplier['address'];
				$supplier->kecamatan = $list_suppplier['kecamatan'];				
				$supplier->supp_telp = $list_suppplier['supp_telp'];
			}
		}
		
		$this->view->supplier = $supplier;
		$this->AuthorityAction();
	}
}
