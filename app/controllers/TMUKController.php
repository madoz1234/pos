<?php

use Phalcon\Mvc\Controller;

require_once ('../app/library/src/Autoloader.php');

PhpXmlRpc\Autoloader::register();
use PhpXmlRpc\Value;
use PhpXmlRpc\Request;
use PhpXmlRpc\Client;

class TMUKController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'TMUK' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$tmuk = new TMUK();		
		$lists = $tmuk::getAll();
		
		$izone = new Izone();		
		$lists_izone = $izone::getAll();
		
		$this->view->data = new TMUK();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->tmuk_id = $list['tmuk_id'];
				$this->view->data->tmuk = $list['tmuk'];
				$this->view->data->supplier_id = $list['supplier_id'];
				$this->view->data->escrow_account = $list['escrow_account'];
				$this->view->data->saldo = $list['saldo'];
				$this->view->data->telp = $list['telp'];
				$this->view->data->name = $list['name'];
				$this->view->data->owner = $list['owner'];
				$this->view->data->address1 = $list['address1'];
				$this->view->data->address2 = $list['address2'];
				$this->view->data->address3 = $list['address3'];
				$this->view->data->length_struk = $list['length_struk'];
				$this->view->data->nama_lsi = $list['nama_lsi'];
			}
		}
		
		$this->view->data_izone = new Izone();
		if(count($lists_izone) > 0){
			foreach($lists_izone as $list){
				$this->view->data_izone->activation_id = $list['activation_id'];
				$this->view->data_izone->terminal_id = $list['terminal_id'];
			}
		}
	}	
	
	public function updateAction(){
		
		if($this->request->getPost("tmuk_id") == ''){
			$tmuk = new TMUK();			
			$tmuk->tmuk = $this->request->getPost("tmuk");
			$tmuk->printer = $this->request->getPost("printer");
			$tmuk->supplier_id = '';
			$tmuk->saldo = '0';
			$tmuk->escrow_account = '';	
			$tmuk->telp = '';
			$tmuk->name = '';
			$tmuk->owner = '';
			$tmuk->address1 = '';
			$tmuk->address2 = '';
			$tmuk->address3 = '';
			$tmuk->tipe = '';
			$tmuk->asn = 0;
			$success = $tmuk::goInsert($tmuk);
		}else{
			$tmuk = new TMUK();
			$tmuk->tmuk_id = $this->request->getPost("tmuk_id");		
			$tmuk->tmuk = $this->request->getPost("tmuk");
			$tmuk->printer = $this->request->getPost("printer");
			$tmuk->length_struk = $this->request->getPost("length_struk");
			$success = $tmuk::goUpdate($tmuk);
		}
		
		$this->response->redirect(uri('TMUK'));
	}
	
	public function update_izoneAction(){
		$izone = new Izone();			
		$izone->activation_id = $this->request->getPost("activation_id");
		$izone->terminal_id = $this->request->getPost("terminal_id");
		
		$condition = "";
		$check_izone = $izone::getCount($condition);
		
		if($check_izone > 0){
			$success = $izone::goDelete($izone);
			$success = $izone::goInsert($izone);
		}else{
			$success = $izone::goInsert($izone);
		}
		
		$this->response->redirect(uri('TMUK'));
	}
	
	public function aktivasiAction(){
		error_reporting(0);
		
		$time = time();
		$traceNo = 1;
		settype($traceNo, "integer");
		
			$p[0] = new PhpXmlRpc\Value(
				array(
					"ActivationDateTime" => new Value($time, 'dateTime.iso8601'),
					"ActivationKey" => new Value($_REQUEST['activation_id']), 
					"TerminalID" => new Value($_REQUEST['terminal_id']), 
					"GroupID" => new Value("8016"),
					"MACAddress" => new Value("00-00-00-00-00-00") 
				),
				"struct"
			);
			
			$p[1] = new Value(intval(1), 'int');
			
		$req = new Request('iGateway.activation', $p);
		$client = new Client('http://localhost:8080/pcclient/client');
		$response = $client->send($req);

		$client->setDebug(1);
		// check response for errors, and take appropriate action
		if(!$response->faultCode()){
			$encoder = new PhpXmlRpc\Encoder();
			$value = $encoder->decode($response->value());
		
			$izone = new Izone();				
			$lists = $izone::getAll();
			
			foreach($lists as $list){
				if($list['pin'] != "" || $list['pin'] != null){
					$val = "Info;Terminal ".$list['terminal_id']." sudah di Aktivasi.;info";
				}else if(isset($value["PIN"])){
					$izone->activation_id = $_REQUEST['activation_id'];
					$izone->pin = htmlspecialchars($value["PIN"]);
					$izone->first_name = htmlspecialchars($value["FirstName"]);
					$success = $izone::goUpdate($izone);
					
					if($success)
						$val = "Sukses;Aktivasi berhasil dilakukan.;success";
				}else{
					$val = "Gagal;Aktivasi gagal dilakukan. <br/><i>Response Code : ".$value['ResponseCode']."</i>;error";
				}
			}			
		}else{				
			$val = "Gagal;<i>Response Code : ".$response->faultCode()."</i><br/><i>Reason : ".$response->faultString()."</i>;error";
		}
		
		return json_encode($val);
		
	}
	public function getSaldo()
	{
		$data = new TMUK();
		$lists = $data->getAll();
		$saldo = 0;
		
		if(count($lists) > 0){
			foreach($lists as $list){
				$tmuk = $list['tmuk'];
				$tmuk_id = $list['tmuk_id'];

				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/tmuk/get-saldo", [
					'tmuk' => $tmuk,
					'tmuk_id' => $tmuk_id,
				]);
				// transform json to object array php
				$obj = json_decode($result, true);
				if ($obj['status'] == 'success') {
					$saldo = $obj['message'];

					if($saldo > 0){
						$tmuk_update = new TMUK();
						$tmuk_update->tmuk_id = $tmuk_id;
						$tmuk_update->saldo = $saldo;
						$success = $tmuk_update::goUpdate($tmuk_update);
					}
					$last_sync = new LastSync();
					$a = 'SALDO';
					$condition = " WHERE \"tipe\" = '".$a."' ";
					$cek = $last_sync::getFreeSQL($condition);
					if($cek){
						$last_sync->tipe = 'SALDO';
						$last_sync->sync_date = date('Y-m-d');
						$last_sync->sync_time = date('H:i:s');
						$success = $last_sync::goUpdate($last_sync);
					}else{
						$last_sync->id = 1;
						$last_sync->tipe = 'SALDO';
						$last_sync->sync_date = date('Y-m-d');
						$last_sync->sync_time = date('H:i:s');
						$success = $last_sync::goInsert($last_sync);
					}
				}
			}						
		}
		return $saldo;
	}

	public function getSaldoEscrow()
	{
		$data = new TMUK();
		$lists = $data->getAll();
		$saldo = 0;
		
		if(count($lists) > 0){
			foreach($lists as $list){
				$tmuk = $list['tmuk'];
				$tmuk_id = $list['tmuk_id'];

				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/tmuk/get-saldo-escrow", [
					'tmuk' => $tmuk,
					'tmuk_id' => $tmuk_id,
				]);
				// transform json to object array php
				$obj = json_decode($result, true);
				if ($obj['status'] == 'success') {
					$saldo = $obj['escrow'];

					if($saldo > 0){
						$tmuk_update = new TMUK();
						$tmuk_update->tmuk_id = $tmuk_id;
						$tmuk_update->saldo = $saldo;
						$success = $tmuk_update::goUpdate($tmuk_update);
						
						$last_sync = new LastSync();
						$last_sync->tipe = 'SALDO';
						$last_sync->sync_date = date('Y-m-d');
						$last_sync->sync_time = date('H:i:s');
						$success = $last_sync::goUpdate($last_sync);
					}
				}
			}						
		}
		return $saldo;
	}

	public function getRekening()
	{
		$data = new TMUK();
		$lists = $data->getAll();
		$rekening = 0;
		
		if(count($lists) > 0){
			foreach($lists as $list){
				$tmuk = $list['tmuk'];
				$tmuk_id = $list['tmuk_id'];

				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/tmuk/get-saldo-escrow", [
					'tmuk' => $tmuk,
					'tmuk_id' => $tmuk_id,
				]);
				// transform json to object array php
				$obj = json_decode($result, true);
				if ($obj['status'] == 'success') {
					$rekening = $obj['rekening'];
				}
			}						
		}
		return $rekening;
	}
}
