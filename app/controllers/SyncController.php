<?php
set_time_limit(0);
use Phalcon\Mvc\Controller;
use \Phalcon\Db\Adapter\Pdo\Postgresql;
	
class SyncController extends Controller
{		
	public function indexAction(){		
		
	}
	
	/********************************************************************
		Method Trigger Sync Data
	********************************************************************/
	public function syncAllAction(){
		$success = '';				
		try{					
			if($success == '') { $success = $this->syncTMUKAction(); }
			if($success == '') { $success = $this->syncSaldoAction(); }
			if($success == '') { $success = $this->syncPromoAction(); }
			if($success == '') { $success = $this->syncSupplierAction(); }
			if($success == '') { $success = $this->syncDivisiAction(); }
			if($success == '') { $success = $this->syncCategoryAction(); }
			//if($success == '') { $success = $this->syncTaxAction(); }
			if($success == '') { $success = $this->syncCustAction(); }
			if($success == '') { $success = $this->syncBankAction(); }
			if($success == '') { $success = $this->syncRackAction(); }
			if($success == '') { $success = $this->syncPlanogramAction(); }
			//if($success == '') { $success = $this->syncPOSAction(); }
			if($success == '') { $success = $this->syncMemberAction(); }
			if($success == '') {
				$this->syncProductAction();	
				$this->syncHargaAction();
				$this->syncPenjualanAction();				
				$this->syncStockAction();
				$this->syncOpnameAction();		
				$this->SystemPRAction();
				$this->ExpiredPRAction();
				$this->ExpiredReturAction();			
				//$this->syncPRAction();
				//$this->syncPOAction();
			}				
		}catch(Exception $e){
			$data['type'] = 'E';
			$data['message'] = 'Error, '.$e;			
			return json_encode($data);
		}
		
		if($success == ''){
			$data['type'] = 'S';
			$data['message'] = 'Sync Semua Berhasil';			
			return json_encode($data);
			$notif = new Notifikasi();
			$notif->data = strip_tags($data['message']);
			$notif->read_at = date('Y-m-d H:i:s');
			$notif->created_at = date('Y-m-d H:i:s');
			$notif->created_by = $this->session->get('user')['real_name'];

			$success = $notif::goInsert($notif);		
			return json_encode($data);
		}else{
			$data['type'] = 'E';
			$data['message'] = 'Error, '.$success;			
			return json_encode($data);
		}		
	}
	
	public function startDayAction(){	
		// check online/offline
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = '<b>Mohon Periksa Kembali Koneksi Anda !!!</b>';			
			return json_encode($data);
		}else{
			$price_info = '';
			$start_day = (new StartEndDay());
			$condition = " WHERE \"flag_start\" = true AND \"flag_end\" = false ";
			$lists_start = $start_day::getFreeSQL($condition);
			if(!isset($lists_start)){
				$start_day = new StartEndDay();
				$start_day->tanggal = date('Y-m-d');
				$start_day->user_id = $this->session->get("user")['user_name'];
				$start_day->flag_start = true;
				$start_day->jam = date('H:m:s');
				$success = $start_day::goInsert($start_day);
				$success = '';
				try{					
					if($success == '') { $success = $this->syncTMUKAction(); }
					if($success == '') { $success = $this->syncPromoAction(); }
					// if($success == '') { $success = $this->syncSaldo(); }
					if($success == '') { $success = $this->syncSupplierAction(); }
					if($success == '') { $success = $this->syncDivisiAction(); }
					if($success == '') { $success = $this->syncCategoryAction(); }
					if($success == '') { $success = $this->syncCustAction(); }
					if($success == '') { $success = $this->syncBankAction(); }
					// if($success == '') { $success = $this->syncRackAction(); }
					// if($success == '') { $success = $this->syncPlanogramAction(); }
					if($success == '') { $success = $this->syncMemberAction(); }
					// if($success == '') { $success = $this->syncJenisKustomerAction(); }
					// if($success == '') { $success = $this->syncKustomerAction(); }
					if($success == '') { $success = $this->syncPointMemberAction(); }
					if($success == '') { $success = $this->syncPiutangMemberAction(); }
					if($success == '') { $success = $this->syncPointAction(); }
					if($success == '') {
						$this->syncProductAction();	
						// $this->syncHargaAction();
						$this->SystemPRAction();
						$this->ExpiredPRAction();
						
						$lists = $this->ajaxChangedAction();
						if($lists == 0){ 
							$price_info = '<p><font style="font-size:10px;font-style:italic"> Tidak ada perubahan Harga.</p>'; 
						}else{
							$price_info = '<p><font style="font-size:10px;font-style:italic"> Ada '.$lists.' Produk yang mengalami perubahan harga. <br/><b><a href="'.assets('LabelHarga?p=true').'">Print Label</a></b></p>';
						}
					}				
				}catch(Exception $e){
					if(strpos($e,'PDOException') !== false){
						$e = 'Koneksi terputus ke server HO. Silahkan cek kembali koneksi internet Anda';
					}
					
					$data['type'] = 'S';
					$data['alert'] = 'info';
					$data['message'] = '
					<p style="font-weight:bold;"><span class="text-success"><i>Start of Day Berhasil</i>.</span><span class="text-danger"> Sync Gagal.</span></p>
					<p>Harap lakukan <a href="'.assets('SyncronMaster').'">Sync Manual</a>.<br/><br/><font style="font-size:10px;font-style:italic">'.$e.'.</font></p>';					
					return json_encode($data);
					$notif = new Notifikasi();
					$notif->data = strip_tags($data['message']);
					$notif->read_at = date('Y-m-d H:i:s');
					$notif->created_at = date('Y-m-d H:i:s');
					$notif->created_by = $this->session->get('user')['real_name'];

					$success = $notif::goInsert($notif);		
					return json_encode($data);
				}

				if($success == ''){
					$PR = new PR();
					$cond1 = " WHERE \"flag_sync\" = 'false' ";
					$jumlah_pr = $PR::getCountPR($cond1);

					$PYR = new PembayaranTransaksi();
					$cond2 = " WHERE \"flag\" = 'false' ";
					$jumlah_pyr = $PYR::getCountPYR($cond2);

					$GR = new GR();
					$cond3 = " WHERE \"flag_sync\" = 'false' ";
					$jumlah_gr = $GR::getCountGR($cond3);

					$RR = new Retur();
					$cond4 = " WHERE \"flag_sync\" = 'false' ";
					$jumlah_retur = $RR::getCountRR($cond4);

					if(isset($jumlah_pr) OR isset($jumlah_pyr) OR isset($jumlah_gr) OR isset($jumlah_retur)){
						if($jumlah_pr != 0){ 
							$pr = '<p><font style="font-size:10px;font-style:italic">Ada '.$jumlah_pr.' PR yang belum di sync. <a href="'.assets('PR').'">PR</a></font></p>';
						}
						if($jumlah_pyr != 0){ 
							$pyr = '<p><font style="font-size:10px;font-style:italic">Ada '.$jumlah_pyr.' PYR yang belum di sync. <a href="'.assets('PembayaranTransaksi').'">PYR</a></font></p>';
						}
						if($jumlah_gr != 0){ 
							$gr = '<p><font style="font-size:10px;font-style:italic">Ada '.$jumlah_gr.' GR yang belum di sync. <a href="'.assets('GR').'">GR</a></font></p></br>';
						}
						if($jumlah_retur != 0){ 
							$rr = '<p><font style="font-size:10px;font-style:italic">Ada '.$jumlah_retur.' RR yang belum di sync. <a href="'.assets('ReturBarang').'">RR</a></font></p>';
						}

						$data['type'] = 'S';
						$data['alert'] = 'success';
						$data['message'] = 'Proses <i><b>Start of Day</b></i> Berhasil. '.$pr.''.$pyr.''.$gr.''.$rr.''.$price_info.'</br>Silahkan <i><b><a href="'.assets('Penjualan/Start').'">Start of Shift</a></b></i>';	
						$notif = new Notifikasi();
						$notif->data = strip_tags($data['message']);
						$notif->read_at = date('Y-m-d H:i:s');
						$notif->created_at = date('Y-m-d H:i:s');
						$notif->created_by = $this->session->get('user')['real_name'];

						$success = $notif::goInsert($notif);		
						return json_encode($data);
					}else{
						$data['type'] = 'S';
						$data['alert'] = 'success';
						$data['message'] = '<p>Proses <i><b>Start of Day</b></i> Berhasil. '.$price_info.'Silahkan <i><b><a href="'.assets('Penjualan/Start').'">Start of Shift</a></b></i></p>';	
						$notif = new Notifikasi();
						$notif->data = strip_tags($data['message']);
						$notif->read_at = date('Y-m-d H:i:s');
						$notif->created_at = date('Y-m-d H:i:s');
						$notif->created_by = $this->session->get('user')['real_name'];

						$success = $notif::goInsert($notif);		
						return json_encode($data);
					}
				}else{
					$data['type'] = 'E';
					$data['alert'] = 'info';
					$data['message'] = '
						<p style="font-weight:bold;"><span class="text-success"><i>Start of Day</i> Berhasil.</span><span class="text-danger"> Sync Gagal.</span></p>
						<p>Harap lakukan <a href="'.assets('SyncronMaster').'">Sync Manual</a>.<br/><br/><font style="font-size:10px;font-style:italic">'.$success.'.</font></p>';	

					$notif = new Notifikasi();
					$notif->data = strip_tags($data['message']);
					$notif->read_at = date('Y-m-d H:i:s');
					$notif->created_at = date('Y-m-d H:i:s');
					$notif->created_by = $this->session->get('user')['real_name'];
					$success = $notif::goInsert($notif);
					return json_encode($data);
				}
			}else{
				$data['type'] = 'E';
				$data['alert'] = 'error';
				$data['message'] = '<b>Start of Day</b> sudah dilakukan sebelumnya.<br/> Silahkan <i><b><a href="'.assets('EndDay').'">End of Day</a></b></i> terlebih dahulu.';			
				return json_encode($data);
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by = $this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);		
				return json_encode($data);
			}
		}
	}
	
	public function endDayAction(){
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = '<b>Mohon Periksa Kembali Koneksi Anda !!!</b>';			
			return json_encode($data);
		}else{	
			$start_day = new StartEndDay();
			$condition = " WHERE \"flag_start\" = true AND \"flag_end\" = false ";
			$lists_start = $start_day::getFreeSQL($condition);
			
			$start_shift = new StartEndShift();
			$conditionSS = " WHERE \"flag_start\" = true AND \"flag_end\" = false ";
			$lists_shift = $start_shift::getFreeSQL($conditionSS);

			if(count($lists_start) > 0){
				
				if($lists_shift > 0){
					$user = ''; $count = 0;
					
					foreach($lists_shift as $list){
						if(count($lists_shift) >= 2 AND (count($lists_shift) - $count) == 1){ $user .= ' & '; }
						$user .= $list['user_id'];
						if(count($lists_shift) > 2 AND (count($lists_shift) - $count) != 1){ $user .= ', '; }
						$count++;
					}

					$data['type'] = 'E';
					$data['alert'] = 'error';
					$data['message'] = $user.' belum melakukan <i><b><a href="'.assets('Penjualan/End').'">End of Shift</a></b></i>';			
					return json_encode($data);
					$notif = new Notifikasi();
					$notif->data = strip_tags($data['message']);
					$notif->read_at = date('Y-m-d H:i:s');
					$notif->created_at = date('Y-m-d H:i:s');
					$notif->created_by = $this->session->get('user')['real_name'];

					$success = $notif::goInsert($notif);		
					return json_encode($data);
				}else{
					$success = '';
					try{
						if($success == '') { $success = $this->InsertHPPAction(); }
						if($success == '') { $success = $this->InsertNilaiPersediaanAction(); }
						if($success == '') { $success = $this->syncPenjualanAction(); }
						if($success == '') { $success = $this->syncStockAction(); }
						if($success == '') { $success = $this->syncOpnameAction(); }
						if($success == '') { $success = $this->syncPromoAction(); }	
						if($success == '') { $success = $this->syncNilaiAllPersediaanAction(); }
					}catch(Exception $e){
						if(strpos($e,'PDOException') !== false){
							$e = 'Koneksi terputus. Silahkan cek kembali koneksi internet Anda';
							$e = $e->getMessage();
						}
						
						$data['type'] = 'E';
						$data['alert'] = 'info';
						$data['message'] = '
							<p style="font-weight:bold;"><span class="text-success"><i>End of Day</i> Berhasil.</span></p>
						';
						$notif = new Notifikasi();
						$notif->data = strip_tags($data['message']);
						$notif->read_at = date('Y-m-d H:i:s');
						$notif->created_at = date('Y-m-d H:i:s');
						$notif->created_by = $this->session->get('user')['real_name'];

						$success = $notif::goInsert($notif);
						return json_encode($data);
					}				
					
					if($success == ''){
						$data['type'] = 'S';
						$data['alert'] = 'success';
						$data['message'] = 'Proses <i><b>End of Day</b></i> Berhasil';
						$notif = new Notifikasi();
						$notif->data = strip_tags($data['message']);
						$notif->read_at = date('Y-m-d H:i:s');
						$notif->created_at = date('Y-m-d H:i:s');
						$notif->created_by = $this->session->get('user')['real_name'];

						$success = $notif::goInsert($notif);		
						return json_encode($data);
					}else{
						$data['type'] = 'E';
						$data['alert'] = 'info';
						$data['message'] = '
							<p style="font-weight:bold;"><span class="text-success"><i>End of Day</i> Berhasil.</span><span class="text-danger"> Sync Gagal.</span></p>
							<p>Harap lakukan <a href="'.assets('SyncronMaster').'">Sync Manual</a>.<br/><br/><font style="font-size:10px;font-style:italic">'.$e.'.</font></p>
						';			
						return json_encode($data);
					}				
				}
			}else{			
				$data['type'] = 'E';
				$data['alert'] = 'error';
				$data['message'] = '<i><b>Start of Day</b></i> belum dilakukan!';			
				return json_encode($data);
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by = $this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);		
				return json_encode($data);
			}
		}
	}
	
	public function goSyncSaldoAction(){
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';
			return json_encode($data);
		}else{

			$success = (new TMUKController())->getSaldo();

			if($success != ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Saldo Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Saldo Gagal';
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}
		}		
	}
	
	public function goSyncPromoAction(){
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{
			$success = $this->syncPromoAction();
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Promo Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Promo Gagal';
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}	
		}	
	}
	
	public function goSyncSupplierAction(){	
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';
			return json_encode($data);
		}else{
			$success = $this->syncSupplierAction();
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Supplier Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Supplier Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}		
		}
	}
	
	public function goSyncPRAction(){	
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{		
			$success = $this->syncPRAction();
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync PR Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync PR Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}
		}
	}
	
	public function goSyncDivisiAction(){	
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{	
			$success = $this->syncDivisiAction();

			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Divisi Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Divisi Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}
		}
	}
	
	public function goSyncCategoryAction(){		
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{		
			$success = $this->syncCategoryAction();
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Kategori Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Kategori Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}	
		}	
	}
	
	public function goSyncTaxAction(){
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{		
			$success = $this->syncTaxAction();
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Tax Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Tax Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}
		}		
	}
	
	public function goSyncCustAction(){
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{			
			$success = $this->syncCustAction();

			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Customer Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Customer Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}	
		}	
	}
	
	public function goSyncMemberAction(){	
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{	
			$success = $this->syncMemberAction();
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Member Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Member Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}	
		}	
	}
	
	public function goSyncPOAction(){	
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{
			$success = $this->syncPOAction();
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync PO Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync PO Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}
		}
	}
	
	public function goSyncProductAction(){	
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{
			$success = $this->syncProductAction();
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Product dan Harga Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Product dan Harga Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}
		}
		
	}

	public function goSyncJenisKustomerAction(){
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{
			$success = $this->syncJenisKustomerAction();
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Jenis Kustomer Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Jenis Kustomer Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}
		}		
	}

	public function goSyncKustomerAction(){
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{
			$success = $this->syncKustomerAction();
		
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Kustomer Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Kustomer Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}
		}		
	}
	
	public function goSyncHargaAction(){	
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{
			$success = $this->syncHargaAction();
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Harga Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Harga Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}
		}
	}
	
	public function goSyncStockAction(){
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{
			$success = $this->syncStockAction();	
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Stock Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Stock Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}
		}
	}
	
	public function goSyncPenjualanAction(){	
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{
			$success = $this->syncPenjualanAction();
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Penjualan Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Penjualan Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}
		}
	}
	
	public function goSyncBankAction(){	
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';
			return json_encode($data);
		}else{
			$success = $this->syncBankAction();
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Bank Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Bank Gagal';
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}	
		}	
	}
	
	public function goSyncRackAction(){
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';
			return json_encode($data);
		}else{	
			$success = $this->syncRackAction();
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Rack Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Rack Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}	
		}	
	}
	
	public function goSyncPOSAction(){	
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{			
			$success = $this->syncPOSAction();	
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync POS Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync POS Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}	
		}	
	}
	
	public function goSyncTMUKAction(){		
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{
			$success = $this->syncTMUKAction();
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync TMUK Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync TMUK Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}
		}		
	}
	
	public function goSyncOpnameAction(){	
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{
			$success = $this->syncOpnameAction();
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Stock Opname Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Stock Opname Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}
		}
	}

	public function goSyncPiutangMemberAction(){	
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{
			$success = $this->syncPiutangMemberAction();
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Piutang Member Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Piutang Member Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}
		}
	}

	public function goSyncPointMemberAction(){	
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{
			$success = $this->syncPointMemberAction();
			// $success = $this->syncPointAction();
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Point Member Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Point Member Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}
		}
	}
	
	public function goSyncPlanogramAction(){
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{
			$success = $this->syncPlanogramAction();
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync Planogram Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync Planogram Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}
		}
	}
	
	public function goSystemPRAction(){	
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{
			$success = $this->systemPRAction();
			
			if($success == ''){
				$data['type'] = 'S';
				$data['message'] = 'Sync System PR Berhasil';			
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Sync System PR Gagal';	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	

				$success = $notif::goInsert($notif);
				return json_encode($data);
			}
		}
	}


	
	/********************************************************************
		Method Sync Data
	********************************************************************/

	public function syncJenisKustomerAction()
	{
		$data = new JenisKustomer();

		// dd($data);
		$lists = $data->getAll();
		// dd($lists);
		$jk = 0;

		$curl = new CurlClass();
		$result = $curl->get($this->di['api']['link']."/api/jenis-kustomer/get-kustomer");
		$obj = json_decode($result, true);

		if($obj['status'] == 'success') {
			foreach ($obj['message'] as $value) {
				$condition = " WHERE \"kode\" = '".$value['kode']."' ";
				$cek = $data::getFreeSQL($condition);

				if($cek){
					$data->kode = $value['kode'];
					$data->jenis = $value['jenis'];
					$data->jenis_kustomer_id = $value['id'];
					$success = $data::goUpdate($data);
				}else{
					$data->kode = $value['kode'];
					$data->jenis = $value['jenis'];
					$data->jenis_kustomer_id = $value['id'];
					$success = $data::goInsert($data);
				}
			}
			$last_sync = new LastSync();
			$a = 'JENSI KUSTOMER';
			$condition = " WHERE \"tipe\" = '".$a."' ";
			$cek = $last_sync::getFreeSQL($condition);
			if($cek){
				$last_sync->tipe = 'JENSI KUSTOMER';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goUpdate($last_sync);
			}else{
				$last_sync->id = 2;
				$last_sync->tipe = 'JENIS KUSTOMER';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goInsert($last_sync);
			}
			return '';
		}else{
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = '<b>Gagal sync Jenis Kustomer dari server !!!</b>';
		}
	}

	public function syncKustomerAction()
	{
		$data = new Cust();
		$lists = $data->getAll();
		$jk = 0;

		$curl = new CurlClass();
		$result = $curl->get($this->di['api']['link']."/api/kustomer/get-kustomer");
		$obj = json_decode($result, true);
		if($obj['status'] == 'success') {
			foreach ($obj['message'] as $val) {
				$condition = " WHERE \"debtor_ref\" LIKE '%".$val['tmuk']['nama']."%' ";
				$cek = $data::getFreeSQL($condition);
				if($cek){
					$data->debtor_no = $val['id'];
					$data->name = $val['membercard']['nama'];
					$data->debtor_ref = $val['tmuk']['nama'];
					$data->address = $val['alamat'];
					$data->tax_id = $val['pajak_id'];
					$data->curr_code = $val['kode'];
					$data->discount = $val['persen_diskon'];
					$data->pymt_discount = $val['status_kredit'];
					$data->credit_limit = $val['limit_kredit'];
					$data->notes = $val['kode'];
					$data->inactive = $val['status_kredit'];
					$data->nomor_member = $val['membercard']['nomor'];
					$success = $data::goUpdate($data);
				}else{
					$data->debtor_no = $val['id'];
					$data->name = $val['membercard']['nama'];
					$data->debtor_ref = $val['tmuk']['nama'];
					$data->address = $val['alamat'];
					$data->tax_id = $val['pajak_id'];
					$data->curr_code = $val['kode'];
					$data->discount = $val['persen_diskon'];
					$data->pymt_discount = $val['status_kredit'];
					$data->credit_limit = $val['limit_kredit'];
					$data->notes = $val['kode'];
					$data->inactive = $val['status_kredit'];
					$data->nomor_member = $val['membercard']['nomor'];
					$success = $data::goInsert($data);
				}
			}
			$last_sync = new LastSync();
			$a = 'KUSTOMER';
			$condition = " WHERE \"tipe\" = '".$a."' ";
			$cek = $last_sync::getFreeSQL($condition);
			if($cek){
				$last_sync->tipe = 'KUSTOMER';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goUpdate($last_sync);
			}else{
				$last_sync->id = 3;
				$last_sync->tipe = 'KUSTOMER';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goInsert($last_sync);
			}
			return '';
		}else{
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = '<b>Gagal sync Kustomer dari server !!!</b>';
		}
	}

	
	public function syncProductAction(){
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = '<b>Mohon Periksa Kembali Koneksi Anda !!!</b>';

			return json_encode($data);
		}else{
			$tmuk = new TMUK();
			$lists = $tmuk::getAll();
			
			if(isset($lists)){
				foreach($lists as $list){
					$tmuk->tmuk_id = $list['tmuk_id'];
					$tmuk->tmuk = $list['tmuk'];
					$tmuk->tipe = $list['tipe'];
				}
			}

			if(isset($lists)){
				$product = new Product();
				$product_move = new ProductMove();
				$price = new ProductPrice();
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/produk/sync-produk", [
					'tmuk' => $tmuk->tmuk
				]);
				$obj = json_decode($result, true);

				$productQuery = '';
				$priceQuery = '';
				$moveQuery = '';
				$count = 0;
				if (isset($obj['message'])) {
					foreach ($obj['message'] as $value) {
						$condition = " WHERE \"stock_id\" = '".$value['produk']['kode']."' ";
						$cek = $product::getFreeSQL($condition);

						$product->stock_id = $value['produk']['kode'];
						$product->description = $value['produk']['nama'];
						$product->bumun_cd = $value['produk']['bumun_cd'];
						$product->l1_cd = $value['produk']['l1_cd'];
						$product->l2_cd = $value['produk']['l2_cd'];
						$product->l3_cd = $value['produk']['l3_cd'];
						$product->l4_cd = $value['produk']['l4_cd'];
						$product->margin = $value['produk']['produksetting']['margin'];
						$product->uom1_nm = $value['produk']['produksetting']['uom1_satuan'];
						$product->uom2_nm = $value['produk']['produksetting']['uom2_satuan'];
						$product->uom3_nm = $value['produk']['produksetting']['uom3_satuan'];
						$product->uom4_nm = $value['produk']['produksetting']['uom4_satuan'];
						$product->uom4_nm = $value['produk']['produksetting']['uom4_satuan'];
						$product->uom1_conversion = 1;
						$product->uom2_conversion = $value['produk']['produksetting']['uom2_conversion'];
						$product->uom3_conversion = $value['produk']['produksetting']['uom3_conversion'];
						$product->uom4_conversion = $value['produk']['produksetting']['uom4_conversion'];
						$product->uom4_conversion = $value['produk']['produksetting']['uom4_conversion'];
						$product->uom1_internal_barcode = $value['produk']['produksetting']['uom1_barcode'];
						$product->uom2_internal_barcode = $value['produk']['produksetting']['uom2_barcode'];
						$product->uom3_internal_barcode = $value['produk']['produksetting']['uom3_barcode'];
						$product->uom4_internal_barcode = $value['produk']['produksetting']['uom4_barcode'];
						$product->uom4_internal_barcode = $value['produk']['produksetting']['uom4_barcode'];
						$product->uom1_width = $value['produk']['produksetting']['uom1_width'];
						$product->uom2_width = $value['produk']['produksetting']['uom2_width'];
						$product->uom3_width = $value['produk']['produksetting']['uom3_width'];
						$product->uom4_width = $value['produk']['produksetting']['uom4_width'];
						$product->uom4_width = $value['produk']['produksetting']['uom4_width'];
						$product->uom1_length = $value['produk']['produksetting']['uom1_length'];
						$product->uom2_length = $value['produk']['produksetting']['uom2_length'];
						$product->uom3_length = $value['produk']['produksetting']['uom3_length'];
						$product->uom4_length = $value['produk']['produksetting']['uom4_length'];
						$product->uom4_length = $value['produk']['produksetting']['uom4_length'];
						$product->uom1_height = $value['produk']['produksetting']['uom1_height'];
						$product->uom2_height = $value['produk']['produksetting']['uom2_height'];
						$product->uom3_height = $value['produk']['produksetting']['uom3_height'];
						$product->uom4_height = $value['produk']['produksetting']['uom4_height'];
						$product->uom4_height = $value['produk']['produksetting']['uom4_height'];
						$product->uom1_weight = $value['produk']['produksetting']['uom1_weight'];
						$product->uom2_weight = $value['produk']['produksetting']['uom2_weight'];
						$product->uom3_weight = $value['produk']['produksetting']['uom3_weight'];
						$product->uom4_weight = $value['produk']['produksetting']['uom4_weight'];
						$product->uom1_htype_1 = $value['produk']['produksetting']['uom1_selling_cek'];
						$product->uom1_htype_2 = $value['produk']['produksetting']['uom1_order_cek'];
						$product->uom1_htype_3 = $value['produk']['produksetting']['uom1_receiving_cek'];
						$product->uom1_htype_4 = $value['produk']['produksetting']['uom1_return_cek'];
						$product->uom1_htype_5 = $value['produk']['produksetting']['uom1_inventory_cek'];
						$product->uom2_htype_1 = $value['produk']['produksetting']['uom2_selling_cek'];
						$product->uom2_htype_2 = $value['produk']['produksetting']['uom2_order_cek'];
						$product->uom2_htype_3 = $value['produk']['produksetting']['uom2_receiving_cek'];
						$product->uom2_htype_4 = $value['produk']['produksetting']['uom2_return_cek'];
						$product->uom2_htype_5 = $value['produk']['produksetting']['uom2_inventory_cek'];
						$product->uom1_prod_nm = $value['produk']['produksetting']['uom1_produk_description'];
						$product->uom2_prod_nm = $value['produk']['produksetting']['uom2_produk_description'];
						$product->uom1_harga = ($value['harga_jual']['cost_price'] != '') ? $value['harga_jual']['cost_price'] : 0;
						$product->uom2_harga = $value['harga_beli']['curr_sale_prc'];
						$product->status = $value['harga_beli']['status'];
						$product->tipe = $value['produk']['produksetting']['tipe_barang_kode'];
						$product->ppob = $value['produk']['produksetting']['ppob'];
						$product->jenis = $value['produk']['produksetting']['jenis_barang_kode'];
						if($value['flag'] == 0){
							$product->status_flag =1;
						}
						if($value['flag'] == 1){
							$product->status_flag =2;
						}

						//PRICE
						$map = 0;
						$margin = 0;
						$suggest = 0;
						$changed = 0;
						if($value['harga_jual']['map'] > 0){
							$map = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($value['harga_jual']['map']));
						}
						if($value['harga_jual']['margin_amount'] > 0){
							$margin = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($value['harga_jual']['margin_amount']));
						}
						if($value['harga_jual']['suggest_price'] > 0){
							$suggest = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($value['harga_jual']['suggest_price']));
						}
						if($value['harga_jual']['change_price'] > 0){
							$changed = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($value['harga_jual']['change_price']));
						}
						$sell = $map*(1+($value['produk']['produksetting']['margin']/100));
						$abrev = 'IDR';

						$price->stock_id = $value['produk']['kode'];
						$price->sales_type_id = 1;
						$price->curr_abrev = $abrev;
						$price->price = 0;
						if($value['produk']['produksetting']['uom2_conversion'] > 0){
							$price->price = round($value['harga_beli']['curr_sale_prc'] / $value['produk']['produksetting']['uom2_conversion']);
						}
						if($value['produk']['produksetting']['margin'] != ''){
							$price->margin = $value['produk']['produksetting']['margin'];
							$price->uom1_suggested_price = round($sell);
							$price->uom1_rounding_price = round(round($sell)/100) * 100;	
						}

						$price->average_cost = $map;
						$price->uom1_suggested_price = round($sell);
						$price->uom1_rounding_price = round(round($sell)/100) * 100;
						$price->uom1_changed_price = $changed;
						$price->uom1_margin_amount = $margin;
						$price->uom2_suggested_price = $suggest;
						$price->uom2_rounding_price = 0;
						$price->uom2_changed_price = 0;
						$price->uom2_margin_amount = 0;
						$price->uom3_suggested_price = 0;
						$price->uom3_rounding_price = 0;
						$price->uom3_changed_price = 0;
						$price->uom3_margin_amount = 0;
						$price->uom1_cost_price = 0;
						$price->uom2_cost_price = 0;
						$price->uom3_cost_price = 0;
						$price->uom4_suggested_price = 0;
						$price->uom4_rounding_price = 0;
						$price->uom4_changed_price = 0;
						$price->uom4_margin_amount = 0;
						$price->uom4_cost_price = 0;
						$price->uom1_member_price = 0;
						$price->uom2_member_price = 0;
						$price->uom3_member_price = 0;
						$price->uom4_member_price = 0;

						if($cek){
							$productQuery .= $product::updateSql($product);
							$priceQuery .= $price::updateSql($price);
						}else{
							$product_move->stock_id = $value['produk']['kode'];
							$productQuery .= $product::insertSql($product);
							$priceQuery .= $price::insertSql($price);
							$moveQuery .= $product_move::insertSql($product_move);
						}
						// $count++;
					}
					if($productQuery != ''){
						$success = $product::executeQuery($productQuery);
					}
					if($moveQuery != ''){
						$success = $product_move::executeQuery($moveQuery);
					}
					if($priceQuery != ''){
						$success = $price::executeQuery($priceQuery);
					}

					$last_sync = new LastSync();
					$a = 'PRODUCT & PRICE';
					$condition = " WHERE \"tipe\" = '".$a."' ";
					$cek = $last_sync::getFreeSQL($condition);
					if($cek){
						$last_sync->tipe = 'PRODUCT & PRICE';
						$last_sync->sync_date = date('Y-m-d');
						$last_sync->sync_time = date('H:i:s');
						$success = $last_sync::goUpdate($last_sync);
					}else{
						$last_sync->id = 4;
						$last_sync->tipe = 'PRODUCT & PRICE';
						$last_sync->sync_date = date('Y-m-d');
						$last_sync->sync_time = date('H:i:s');
						$success = $last_sync::goInsert($last_sync);
					}

					return '';
				} else {
					// error
					$data['type'] = 'E';
					$data['alert'] = 'error';
					$data['message'] = '<b>Gagal ambil produk dari server !!!</b>';
				}
			}
		}
	}
	
	public function syncPromoAction(){
		$tmuk = new TMUK();
		$lists = $tmuk::getAll();
		
		if(isset($lists)){
			foreach($lists as $list){
				$tmuk->tmuk_id = $list['tmuk_id'];
				$tmuk->tmuk = $list['tmuk'];
			}
		}
		
		if(isset($lists)){			
			$tgl = date('Y-m-d');
			$curl = new CurlClass();
			$result = $curl->post($this->di['api']['link']."/api/produk/get-promosi", [
				'tmuk' => $tmuk->tmuk,
				'tgl' => $tgl,
			]);

			$promosi = json_decode($result, true);
			
			foreach ($promosi['message'] as $value) {
				$delete = $value['status'];
				$data_promosi = new Promo();
				$condition = " WHERE \"id\" = '".$value['id']."' ";
				$cek = $data_promosi::getFreeSQL($condition);
				$tgl_cek = date($value['tgl_akhir']);
				$tgl_awal = date($value['tgl_awal']);
				$now = date('Y-m-d');
					if($cek){
						if($value['status'] == 1){
							$data_promosi->id = $value['id'];
							$data_promosi->flag = 'false';
						}else{
							$data_promosi->id = $value['id'];
							$data_promosi->kode_promosi = $value['kode_promo'];
							$data_promosi->nama_promosi = $value['nama'];
							$data_promosi->from_ = date($value['tgl_awal']);
							$data_promosi->to_ = date($value['tgl_akhir']);
							if($now >= $tgl_awal AND $now <= $tgl_cek){
								$data_promosi->flag = 'true';
							}else{
								$data_promosi->flag = 'false';
							}
						}
						$success = $data_promosi::goUpdate($data_promosi);
					}else{
						if($value['status'] == 1){
							$data_promosi->id = $value['id'];
							$data_promosi->kode_promosi = $value['kode_promo'];
							$data_promosi->nama_promosi = $value['nama'];
							$data_promosi->from_ = date($value['tgl_awal']);
							$data_promosi->to_ = date($value['tgl_akhir']);
							$data_promosi->flag = 'false';
						}else{
							$data_promosi->id = $value['id'];
							$data_promosi->kode_promosi = $value['kode_promo'];
							$data_promosi->nama_promosi = $value['nama'];
							$data_promosi->from_ = date($value['tgl_awal']);
							$data_promosi->to_ = date($value['tgl_akhir']);
							if($now >= $tgl_awal AND $now <= $tgl_cek){
								$data_promosi->flag = 'true';
							}else{
								$data_promosi->flag = 'false';
							}
						}
						$success = $data_promosi::goInsert($data_promosi);
					}
					$prod = new Product();
					if($value['detail_diskon']){
						foreach ($value['detail_diskon'] as $val_diskon) {
							$diskon = new Diskon();
							$condition = " WHERE \"promosi_id\" = '".$val_diskon['promosi_id']."' AND \"kode_produk\" = '".$val_diskon['kode_produk']."' ";
							$cek = $diskon::getFreeSQL($condition);
							if(count($cek) > 0){
								if($val_diskon['status'] == 1){
									$diskon->promosi_id = $val_diskon['promosi_id'];
									$diskon->kode_produk = $val_diskon['kode_produk'];
									$diskon->flag = 'false';
								}else{
									$diskon->promosi_id = $val_diskon['promosi_id'];
									$diskon->kode_produk = $val_diskon['kode_produk'];
									$diskon->cost_price = $val_diskon['harga_awal'];
									$diskon->harga_terdiskon = $val_diskon['harga_terdiskon'];
									$diskon->diskon = $val_diskon['diskon'];
									if($now >= $tgl_awal AND $now <= $tgl_cek){
										$diskon->flag = 'true';
									}else{
										$diskon->flag = 'false';
									}
								}
								$success = $diskon::goUpdate($diskon);
							}else{
								$condition = " WHERE \"stock_id\" = '".$val_diskon['kode_produk']."' ";
								$cek_prod = $prod::getFreeSQL($condition)['0']['status_flag'];
								if($cek_prod == 1){
									if($val_diskon['status'] == 1){
										$diskon->promosi_id = $val_diskon['promosi_id'];
										$diskon->kode_produk = $val_diskon['kode_produk'];
										$diskon->cost_price = $val_diskon['harga_awal'];
										$diskon->harga_terdiskon = $val_diskon['harga_terdiskon'];
										$diskon->diskon = $val_diskon['diskon'];
										$diskon->flag = 'false';
									}else{
										$diskon->promosi_id = $val_diskon['promosi_id'];
										$diskon->kode_produk = $val_diskon['kode_produk'];
										$diskon->cost_price = $val_diskon['harga_awal'];
										$diskon->harga_terdiskon = $val_diskon['harga_terdiskon'];
										$diskon->diskon = $val_diskon['diskon'];
										if($now >= $tgl_awal AND $now <= $tgl_cek){
											$diskon->flag = 'true';
										}else{
											$diskon->flag = 'false';
										}
									}
									$success = $diskon::goInsert($diskon);
								}
							}
						}
					}

					if($value['detail_buyxgety']){
						$xy = new XY();
						$condition = " WHERE \"promosi_id\" = '".$value['detail_buyxgety']['promosi_id']."' ";
						$cek = $xy::getFreeSQL($condition);
						if($cek){
							if($value['detail_buyxgety']['status'] == 1){
								$xy->promosi_id = $value['detail_buyxgety']['promosi_id'];
								$xy->kode_produk_x = $value['detail_buyxgety']['kode_produk_x'];
								$xy->flag = 'false';
							}else{
								$xy->promosi_id = $value['detail_buyxgety']['promosi_id'];
								$xy->kode_produk_x = $value['detail_buyxgety']['kode_produk_x'];
								$xy->harga_jual_x = $value['detail_buyxgety']['harga_jual_x'];
								$xy->diskon_x = $value['detail_buyxgety']['diskon_x'];
								$xy->harga_diskon_x = $value['detail_buyxgety']['harga_diskon_x'];
								$xy->kode_produk_y = $value['detail_buyxgety']['kode_produk_y'];
								$xy->harga_jual_y = $value['detail_buyxgety']['harga_jual_y'];
								$xy->diskon_y = $value['detail_buyxgety']['diskon_y'];
								$xy->harga_diskon_y = $value['detail_buyxgety']['harga_diskon_y'];
								if($now >= $tgl_awal AND $now <= $tgl_cek){
									$xy->flag = 'true';
								}else{
									$xy->flag = 'false';
								}
							}
							$success = $xy::goUpdate($xy);
						}else{
							$condition = " WHERE \"stock_id\" = '".$value['detail_buyxgety']['kode_produk_x']."' ";
							$cek_prod_x = $prod::getFreeSQL($condition)['0']['status_flag'];

							$condition = " WHERE \"stock_id\" = '".$value['detail_buyxgety']['kode_produk_y']."' ";
							$cek_prod_y = $prod::getFreeSQL($condition)['0']['status_flag'];

							if($cek_prod_x == 1 && $cek_prod_y == 1){
								if($value['detail_buyxgety']['status'] == 1){
									$xy->promosi_id = $value['detail_buyxgety']['promosi_id'];
									$xy->kode_produk_x = $value['detail_buyxgety']['kode_produk_x'];
									$xy->harga_jual_x = $value['detail_buyxgety']['harga_jual_x'];
									$xy->diskon_x = $value['detail_buyxgety']['diskon_x'];
									$xy->harga_diskon_x = $value['detail_buyxgety']['harga_diskon_x'];
									$xy->kode_produk_y = $value['detail_buyxgety']['kode_produk_y'];
									$xy->harga_jual_y = $value['detail_buyxgety']['harga_jual_y'];
									$xy->diskon_y = $value['detail_buyxgety']['diskon_y'];
									$xy->harga_diskon_y = $value['detail_buyxgety']['harga_diskon_y'];
									$xy->flag = 'false';
								}else{
									$xy->promosi_id = $value['detail_buyxgety']['promosi_id'];
									$xy->kode_produk_x = $value['detail_buyxgety']['kode_produk_x'];
									$xy->harga_jual_x = $value['detail_buyxgety']['harga_jual_x'];
									$xy->diskon_x = $value['detail_buyxgety']['diskon_x'];
									$xy->harga_diskon_x = $value['detail_buyxgety']['harga_diskon_x'];
									$xy->kode_produk_y = $value['detail_buyxgety']['kode_produk_y'];
									$xy->harga_jual_y = $value['detail_buyxgety']['harga_jual_y'];
									$xy->diskon_y = $value['detail_buyxgety']['diskon_y'];
									$xy->harga_diskon_y = $value['detail_buyxgety']['harga_diskon_y'];
									if($now >= $tgl_awal AND $now <= $tgl_cek){
										$xy->flag = 'true';
									}else{
										$xy->flag = 'false';
									}
								}
								$success = $xy::goInsert($xy);
							}
						}
					}

					if($value['detail_purchase']){
						$pwp = new Pwp();
						$condition = " WHERE \"id\" = '".$value['detail_purchase']['id']."' ";
						$cek = $pwp::getFreeSQL($condition);
						if($cek){
							if($value['detail_purchase']['status'] == 1){
								$pwp->kode_promosi = $value['detail_purchase']['promosi_id'];
								$pwp->stock_id = $value['detail_purchase']['kode_produk'];
								$pwp->flag = 'false';
							}else{
								$pwp->id = $value['detail_purchase']['id'];
								$pwp->kode_promosi = $value['detail_purchase']['promosi_id'];
								$pwp->stock_id = $value['detail_purchase']['kode_produk'];
								$pwp->price = $value['detail_purchase']['selling_price'];
								if($now >= $tgl_awal AND $now <= $tgl_cek){
									$pwp->flag = 'true';
								}else{
									$pwp->flag = 'false';
								}
							}
							$success = $pwp::goUpdate($pwp);
						}else{
							$condition = " WHERE \"stock_id\" = '".$value['detail_purchase']['kode_produk']."' ";
							$cek_prod_purchase = $prod::getFreeSQL($condition)['0']['status_flag'];

							if($cek_prod_purchase == 1){
								if($value['detail_purchase']['status'] == 1){
									$pwp->id = $value['detail_purchase']['id'];
									$pwp->kode_promosi = $value['detail_purchase']['promosi_id'];
									$pwp->stock_id = $value['detail_purchase']['kode_produk'];
									$pwp->price = $value['detail_purchase']['selling_price'];
									$pwp->flag = 'false';
								}else{
									$pwp->id = $value['detail_purchase']['id'];
									$pwp->kode_promosi = $value['detail_purchase']['promosi_id'];
									$pwp->stock_id = $value['detail_purchase']['kode_produk'];
									$pwp->price = $value['detail_purchase']['selling_price'];
									if($now >= $tgl_awal AND $now <= $tgl_cek){
										$pwp->flag = 'true';
									}else{
										$pwp->flag = 'false';
									}
								}
								$success = $pwp::goInsert($pwp);
							}
						}

						foreach ($value['detail_purchase']['detail_list'] as $val) {
							$pwp_list = new PwpList();
							$condition = " WHERE \"pwp_id\" = '".$value['detail_purchase']['id']."' AND \"kode_produk\" = '".$val['kode_produk']."' ";
							$cek1 = $pwp_list::getFreeSQL($condition);

							if($cek1){
								if($val['status'] == 1){
									$pwp_list->pwp_id = $value['detail_purchase']['id'];
									$pwp_list->kode_produk = $val['kode_produk'];
									$pwp_list->flag = 'false';
								}else{
									$pwp_list->pwp_id = $value['detail_purchase']['id'];
									$pwp_list->kode_produk = $val['kode_produk'];
									if($now >= $tgl_awal AND $now <= $tgl_cek){
										$pwp_list->flag = 'true';
									}else{
										$pwp_list->flag = 'false';
									}
								}
								$success = $pwp_list::goUpdate($pwp_list);
							}else{
								$condition = " WHERE \"stock_id\" = '".$val['kode_produk']."' ";
								$cek_prod_purchase_list = $prod::getFreeSQL($condition)['0']['status_flag'];
								if($cek_prod_purchase_list == 1){
									if($val['status'] == 1){
										$pwp_list->pwp_id = $value['detail_purchase']['id'];
										$pwp_list->kode_produk = $val['kode_produk'];
										$pwp_list->flag = 'false';
									}else{
										$pwp_list->pwp_id = $value['detail_purchase']['id'];
										$pwp_list->kode_produk = $val['kode_produk'];
										if($now >= $tgl_awal AND $now <= $tgl_cek){
											$pwp_list->flag = 'true';
										}else{
											$pwp_list->flag = 'false';
										}
									}
									$success = $pwp_list::goInsert($pwp_list);
								}
							}
						}
					}

					if($value['detail_promo']){
						foreach ($value['detail_promo'] as $val_promo) {
							$promosipromo = new PromosiPromo();
							$condition = " WHERE \"promosi_id\" = '".$val_promo['promosi_id']."' AND \"kode_produk\" = '".$val_promo['kode_produk']."' ";
							$cek = $promosipromo::getFreeSQL($condition);
							if(count($cek) > 0){
								if($val_promo['status'] == 1){
									$promosipromo->promosi_id = $val_promo['promosi_id'];
									$promosipromo->kode_produk = $val_promo['kode_produk'];
									$promosipromo->flag = 'false';
								}else{
									$promosipromo->promosi_id = $val_promo['promosi_id'];
									$promosipromo->kode_produk = $val_promo['kode_produk'];
									$promosipromo->cost_price = $val_promo['cost_price'];
									$promosipromo->harga_terdiskon = $val_promo['harga_terdiskon'];
									$promosipromo->diskon = $val_promo['diskon'];
									if($now >= $tgl_awal AND $now <= $tgl_cek){
										$promosipromo->flag = 'true';
									}else{
										$promosipromo->flag = 'false';
									}
								}
								$success = $promosipromo::goUpdate($promosipromo);
							}else{
								$condition = " WHERE \"stock_id\" = '".$val_promo['kode_produk']."' ";
								$cek_prod_detail_promo = $prod::getFreeSQL($condition)['0']['status_flag'];

								if($cek_prod_detail_promo == 1){
									if($val_promo['status'] == 1){
										$promosipromo->promosi_id = $val_promo['promosi_id'];
										$promosipromo->kode_produk = $val_promo['kode_produk'];
										$promosipromo->cost_price = $val_promo['cost_price'];
										$promosipromo->harga_terdiskon = $val_promo['harga_terdiskon'];
										$promosipromo->diskon = $val_promo['diskon'];
										$promosipromo->flag = 'false';
									}else{
										$promosipromo->promosi_id = $val_promo['promosi_id'];
										$promosipromo->kode_produk = $val_promo['kode_produk'];
										$promosipromo->cost_price = $val_promo['cost_price'];
										$promosipromo->harga_terdiskon = $val_promo['harga_terdiskon'];
										$promosipromo->diskon = $val_promo['diskon'];
										if($now >= $tgl_awal AND $now <= $tgl_cek){
											$promosipromo->flag = 'true';
										}else{
											$promosipromo->flag = 'false';
										}
									}
									$success = $promosipromo::goInsert($promosipromo);
								}
							}
						}
					}
			}

			$cek_promosi = new Promo();
			$condition =" WHERE \"flag\" = 'true' ";
			$cek = $cek_promosi::getFreeSQL($condition);
			foreach ($cek as $key => $value) {
				$from = $value['from_'];
				$to   = $value['to_'];
				$now  = date('Y-m-d');
				if($now >= $from AND $now <= $to){
				}else{
					$promo = new Promo();
					$promo->flag = 'false';
					$promo->id = $value['id'];
					$success = $promo::goUpdate($promo);

					$diskon = new Diskon();
					$diskon->flag = 'false';
					$diskon->promosi_id = $value['id'];
					$success = $diskon::goUpdate2($diskon);

					$xy = new XY();
					$xy->flag = 'false';
					$xy->promosi_id = $value['id'];
					$success = $xy::goUpdate2($xy);

					$pwp = new Pwp();
					$pwp->flag = 'false';
					$pwp->kode_promosi = $value['id'];
					$success = $pwp::goUpdate2($pwp);

					$promosipromo = new PromosiPromo();
					$promosipromo->flag = 'false';
					$promosipromo->promosi_id = $value['id'];
					$success = $promosipromo::goUpdate2($promosipromo);
				}
			}
			$last_sync = new LastSync();
			$a = 'PROMOSI';
			$condition = " WHERE \"tipe\" = '".$a."' ";
			$cek = $last_sync::getFreeSQL($condition);
			if($cek){
				$last_sync->tipe = 'PROMOSI';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goUpdate($last_sync);
			}else{
				$last_sync->id = 5;
				$last_sync->tipe = 'PROMOSI';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goInsert($last_sync);
			}
			return '';										
		}else{
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = '<b>Gagal sync Promosi dari server !!!</b>';
		}
	}
	
	public function syncSupplierAction(){
		$tmuk = new TMUK();
		$lists = $tmuk::getAll();
		
		if(isset($lists)){
			foreach($lists as $list){
				$tmuk->tmuk_id = $list['tmuk_id'];
				$tmuk->tmuk = $list['tmuk'];
			}
		}
		
		if(isset($lists)){	
			$curl = new CurlClass();
			$result = $curl->post($this->di['api']['link']."/api/tmuk/get-vendor", [
					'tmuk' => $tmuk->tmuk
			]);
			$obj = json_decode($result, true);
			foreach ($obj['vendor'] as $key => $value) {
				$jsons = $obj['tmuk'];
				$supp = new Supplier();
				$cond = " WHERE supplier_id = '".$value['vendor']['id']."' ";
				$cek = $supp::getFreeSQL($cond);
				if(!$cek){
					$supp->supplier_id = $value['vendor']['id'];
					$supp->supp_name = $value['vendor']['nama'];
					$supp->supp_ref = '';
					$supp->address = $value['vendor']['alamat'];
					$supp->supp_address = '';
					$supp->contact = $value['vendor']['telepon'];
					$supp->gst_no = '';
					$supp->supp_account_no = '';
					$supp->website = '';
					$supp->bank_account = '';
					$supp->curr_code = '';
					$supp->payment_terms = 0;
					$supp->dimension_id = 0;
					$supp->dimension2_id = 0;
					$supp->tax_group_id = 0;
					$supp->tax_included = 0;
					$supp->credit_limit = 0;
					$supp->purchase_account = '';
					$supp->payable_account = '';
					$supp->payment_discount_account = '';
					$supp->notes = '';
					$supp->inactive = 0;
					$supp->supp_code = '';
					$supp->supp_telp = '';
					$supp->pic = '';
					$supp->top_days = '';
					$supp->order_mon =$value['vendor']['order_senin'];
					$supp->order_tue =$value['vendor']['order_selasa'];
					$supp->order_wed =$value['vendor']['order_rabu'];
					$supp->order_thu =$value['vendor']['order_kamis'];
					$supp->order_fri =$value['vendor']['order_jumat'];
					$supp->order_sat =$value['vendor']['order_sabtu'];
					$supp->order_sun =$value['vendor']['order_minggu'];
					$supp->lead_time = '';
					$supp->npwp = '';
					$supp->pkp = 0;
					$supp->city = '';
					$supp->id_som = '';
					$supp->zip_code = '';
					$supp->kelurahan = '';
					$supp->provinsi = '';
					$supp->gps_latitude = '';
					$supp->gps_longitude = '';
					$supp->kecamatan = '';
					$supp->company_name = '';
					$supp->company_address = '';
					$supp->escrow_account = '';
					$success = $supp::goInsert($supp);
				}else{
					$supp->supplier_id = $value['vendor']['id'];
					$supp->supp_name = $value['vendor']['nama'];
					$supp->supp_ref = '';
					$supp->address = $value['vendor']['alamat'];
					$supp->supp_address = '';
					$supp->contact = $value['vendor']['telepon'];
					$supp->gst_no = '';
					$supp->supp_account_no = '';
					$supp->website = '';
					$supp->bank_account = '';
					$supp->curr_code = '';
					$supp->payment_terms = 0;
					$supp->dimension_id = 0;
					$supp->dimension2_id = 0;
					$supp->tax_group_id = 0;
					$supp->tax_included = 0;
					$supp->credit_limit = 0;
					$supp->purchase_account = '';
					$supp->payable_account = '';
					$supp->payment_discount_account = '';
					$supp->notes = '';
					$supp->inactive = 0;
					$supp->supp_code = '';
					$supp->supp_telp = '';
					$supp->pic = '';
					$supp->top_days = '';
					$supp->order_mon =$value['vendor']['order_senin'];
					$supp->order_tue =$value['vendor']['order_selasa'];
					$supp->order_wed =$value['vendor']['order_rabu'];
					$supp->order_thu =$value['vendor']['order_kamis'];
					$supp->order_fri =$value['vendor']['order_jumat'];
					$supp->order_sat =$value['vendor']['order_sabtu'];
					$supp->order_sun =$value['vendor']['order_minggu'];
					$supp->lead_time = '';
					$supp->npwp = '';
					$supp->pkp = 0;
					$supp->city = '';
					$supp->id_som = '';
					$supp->zip_code = '';
					$supp->kelurahan = '';
					$supp->provinsi = '';
					$supp->gps_latitude = '';
					$supp->gps_longitude = '';
					$supp->kecamatan = '';
					$supp->company_name = '';
					$supp->company_address = '';
					$supp->escrow_account = '';
					$success = $supp::goUpdate($supp);
				}
			}

			$last_sync = new LastSync();
			$a = 'SUPPLIER';
			$condition = " WHERE \"tipe\" = '".$a."' ";
			$cek = $last_sync::getFreeSQL($condition);
			if($cek){
				$last_sync->tipe = 'SUPPLIER';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goUpdate($last_sync);
			}else{
				$last_sync->id = 6;
				$last_sync->tipe = 'SUPPLIER';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goInsert($last_sync);
			}
			return '';
		}else{
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = '<b>Gagal sync Supplier dari server !!!</b>';
		}
	}
	
	public function syncDivisiAction(){
		$tmuk = new TMUK();
		$lists = $tmuk::getAll();
		
		if(isset($lists)){
			foreach($lists as $list){
				$tmuk->tmuk_id = $list['tmuk_id'];
				$tmuk->tmuk = $list['tmuk'];
			}
		}
		
		if(isset($lists)){	
			$curl = new CurlClass();
			$result = $curl->get($this->di['api']['link']."/api/divisi/get-divisi");
			$obj = json_decode($result, true);
			foreach ($obj['message'] as $key => $value) {
				$divisi = new Divisi();
				$condition = " WHERE \"id\" = '".$value['id']."' ";
				$cek = $divisi::getFreeSQL($condition);
				if($cek){
					$divisi->id = $value['id'];
					$divisi->bumun_cd = $value['kode'];
					$divisi->bumun_nm = $value['nama'];
					$success = $divisi::goUpdate($divisi);
				}else{
					$divisi->id = $value['id'];
					$divisi->bumun_cd = $value['kode'];
					$divisi->bumun_nm = $value['nama'];
					$success = $divisi::goInsert($divisi);
				}
			}

			$last_sync = new LastSync();
			$a = 'DIVISI';
			$condition = " WHERE \"tipe\" = '".$a."' ";
			$cek = $last_sync::getFreeSQL($condition);
			if($cek){
				$last_sync->tipe = 'DIVISI';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goUpdate($last_sync);
			}else{
				$last_sync->id = 7;
				$last_sync->tipe = 'DIVISI';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goInsert($last_sync);
			}
			return '';					
		}else{
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = '<b>Gagal sync Divisi dari server !!!</b>';
		}
	}
	
	public function syncCategoryAction(){
		$tmuk = new TMUK();
		$lists = $tmuk::getAll();
		
		if(isset($lists)){
		foreach($lists as $list){
			$tmuk->tmuk_id = $list['tmuk_id'];
			$tmuk->tmuk = $list['tmuk'];
		}
		}
		
		if(isset($lists)){		
			$curl = new CurlClass();
			$result = $curl->get($this->di['api']['link']."/api/category/get-category");

			// transform json to object array php
			$obj = json_decode($result, true);
			foreach ($obj['message'] as $key => $value) {
				$category = new Category();
				$condition = " WHERE \"id\" = '".$value['id']."' ";
				$cek = $category::getFreeSQL($condition);
				if($cek){
					$divisi = new Divisi();
					$condition = " WHERE \"bumun_cd\" = '".$value['divisi_kode']."' ";
					$data = $divisi::getFreeSQL($condition);
					$category->id = $value['id'];
					$category->bumun_cd = $value['divisi_kode'];
					foreach ($data as $key => $val) {
						$category->bumun_nm = $val['bumun_nm'];
					}
					$category->l1_cd = $value['kode'];
					$category->l1_nm = $value['nama'];
					$success = $category::goUpdate($category);
				}else{
					$divisi = new Divisi();
					$condition = " WHERE \"bumun_cd\" = '".$value['divisi_kode']."' ";
					$data = $divisi::getFreeSQL($condition);
					$category->id = $value['id'];
					$category->bumun_cd = $value['divisi_kode'];
					foreach ($data as $key => $val) {
						$category->bumun_nm = $val['bumun_nm'];
					}
					$category->l1_cd = $value['kode'];
					$category->l1_nm = $value['nama'];
					$success = $category::goInsert($category);
				}
			}
				
			$last_sync = new LastSync();
			$a = 'KATEGORI';
			$condition = " WHERE \"tipe\" = '".$a."' ";
			$cek = $last_sync::getFreeSQL($condition);
			if($cek){
				$last_sync->tipe = 'KATEGORI';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goUpdate($last_sync);
			}else{
				$last_sync->id = 8;
				$last_sync->tipe = 'KATEGORI';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goInsert($last_sync);
			}
		return '';												
		}else{
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = '<b>Gagal sync Kategori dari server !!!</b>';
		}
	}
	
	public function syncTaxAction(){
		$tmuk = new TMUK();
		$lists = $tmuk::getAll();
		
		if(isset($lists)){
			foreach($lists as $list){
				$tmuk->tmuk_id = $list['tmuk_id'];
				$tmuk->tmuk = $list['tmuk'];
			}
		}
		
		if(isset($lists)){					
			$curl = new CurlClass();
			$result = $curl->post($this->di['api']['link']."/api/tax/", [
					'tmuk' => $tmuk->tmuk
			]);

			// transform json to object array php
			$obj = json_decode($result, true);

			if($obj['status'] == true){
				$jsons_array = $obj['message'];
				
				// Insert Tax		
				$tax = new Tax();
				
				$count = 0;
				$query_delete = '';
				$query_insert = '';
				if(isset($jsons_array)){
				foreach ($jsons_array as $json){
					if($count >= 500){
						$success = $tax::goMultiDelete($query_delete);
						$success = $tax::goMultiInsert($query_insert);
						$count = 0;
						$query_delete = '';						
						$query_insert = '';
					}
					
					if($count > 0) { $query_delete .= ","; $query_insert .= ","; }
					$query_delete .= "'".$json['post']['id']."'";					
					$query_insert .= "('".$json['post']['id']."','".$json['post']['rate']."','".$json['post']['name']."','0001-01-01','9999-12-31','".$json['post']['inactive']."')";
					
					$count++;
				}
				}
				
				if($query_delete != '' || $query_insert != ''){
					$success = $tax::goMultiDelete($query_delete);
					$success = $tax::goMultiInsert($query_insert);
					$count = 0;
					$query_delete = '';					
					$query_insert = '';
				}
				
				$last_sync = new LastSync();
				$a = 'TAX';
				$condition = " WHERE \"tipe\" = '".$a."' ";
				$cek = $last_sync::getFreeSQL($condition);
				if($cek){
					$last_sync->tipe = 'TAX';
					$last_sync->sync_date = date('Y-m-d');
					$last_sync->sync_time = date('H:i:s');
					$success = $last_sync::goUpdate($last_sync);
				}else{
					$last_sync->id = 9;
					$last_sync->tipe = 'TAX';
					$last_sync->sync_date = date('Y-m-d');
					$last_sync->sync_time = date('H:i:s');
					$success = $last_sync::goInsert($last_sync);
				}
				return '';
			}
		}
	}
	
	public function syncCustAction(){
		$tmuk = new TMUK();
		$lists = $tmuk::getAll();
		
		if(isset($lists)){
			foreach($lists as $list){
				$tmuk->tmuk_id = $list['tmuk_id'];
				$tmuk->tmuk = $list['tmuk'];
			}
		}
		
		if(count($lists) > 0){	
			$curl = new CurlClass();
			$result = $curl->post($this->di['api']['link']."/api/cust/", [
					'tmuk' => $tmuk->tmuk
			]);

			// transform json to object array php
			$obj = json_decode($result, true);

			if($obj['status'] == true){
				$jsons_array = $obj['message'];
				
				// Insert Cust		
				$cust = new Cust();
				
				$count = 0;
				$query_delete = '';
				$query_insert = '';
				if(isset($jsons_array)){
				foreach ($jsons_array as $json){
					if($count >= 500){
						$success = $cust::goMultiDelete($query_delete);
						$success = $cust::goMultiInsert($query_insert);
						$count = 0;
						$query_delete = '';
						$query_insert = '';
					}
					
					if($count > 0) { $query_delete .= ","; $query_insert .= ","; }
					$query_delete .= "'".$json['post']['debtor_no']."'";					
					$query_insert .= "('".$json['post']['debtor_no']."','".$json['post']['name']."','".$json['post']['debtor_ref']."','".$json['post']['address']
									."','".$json['post']['tax_id']."','".$json['post']['curr_code']."','".$json['post']['discount']."','".$json['post']['pymt_discount']
									."','".$json['post']['credit_limit']."','".$json['post']['notes']."','".$json['post']['inactive']."','".$json['post']['nomor_lotte']."')";
					
					$count++;
				}
				}
				
				if($query_delete != '' || $query_insert != ''){
					$success = $cust::goMultiDelete($query_delete);
					$success = $cust::goMultiInsert($query_insert);
					$count = 0;
					$query_delete = '';					
					$query_insert = '';
				}
				
				$last_sync = new LastSync();
				$a = 'KUSTOMER';
				$condition = " WHERE \"tipe\" = '".$a."' ";
				$cek = $last_sync::getFreeSQL($condition);
				if($cek){
					$last_sync->tipe = 'KUSTOMER';
					$last_sync->sync_date = date('Y-m-d');
					$last_sync->sync_time = date('H:i:s');
					$success = $last_sync::goUpdate($last_sync);
				}else{
					$last_sync->id = 10;
					$last_sync->tipe = 'KUSTOMER';
					$last_sync->sync_date = date('Y-m-d');
					$last_sync->sync_time = date('H:i:s');
					$success = $last_sync::goInsert($last_sync);
				}
				return '';
			}													
		}else{
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = '<b>Gagal sync Kustomer dari server !!!</b>';
		}
	}
	
	
	public function ExpiredPRAction(){
		$pr = new PR();
		$condition = " WHERE \"flag_delete\" = false AND \"flag_sync\" = false AND ( \"po_no_immbo\" = '' OR \"po_no_immbo\" IS NULL ) AND create_date < ( NOW() - INTERVAL '7 DAYS' ) ";
		$lists_pr = $pr::getFreeSQL($condition);
		
		if(isset($lists_pr)){
			foreach($lists_pr as $list_pr){
				$pr_update = new PR();
				$pr_update->request_no = $list_pr['request_no'];
				$pr_update->flag_delete = true;
				$pr_update->reference = 'EXPIRED';
				$success = $pr_update::goUpdate($pr_update);
			}
		}
	
		$condition = " WHERE \"flag_delete\" = false AND \"flag_sync\" = false AND ( \"po_no_immbo\" = '' OR \"po_no_immbo\" IS NULL ) AND create_date < ( NOW() - INTERVAL '1 DAYS' ) AND lower(created_by) = 'system' ";
		$lists_pr = $pr::getFreeSQL($condition);
		
		if(isset($lists_pr)){
			foreach($lists_pr as $list_pr){
				$pr_update = new PR();
				$pr_update->request_no = $list_pr['request_no'];
				$pr_update->flag_delete = true;
				$pr_update->reference = 'EXPIRED';
				$success = $pr_update::goUpdate($pr_update);
			}
		}
		return '';
	}
	
	public function SystemPRAction(){			
		// stok sekarang
		$product_move = new ProductMove();						
		$lists_stock = $product_move::getAllStock();	
		
		// sales kemarin -> list barang yg mau di po
		$jual_detail_sum = new JualDetailSum();
		// $condition = " WHERE \"tanggal\" = '".date('Y-m-d', strtotime('-1 day'))."' AND mb_flag != 'D' AND ppob != 1 AND lower(supplied_by) != 'non lotte' GROUP BY \"item_code\", a.description ";
		$condition = " WHERE \"tanggal\" = '".date('Y-m-d', strtotime('-1 day'))."' AND mb_flag != 'D' GROUP BY \"item_code\", a.description ";
		$lists_detail_sum = $jual_detail_sum::req_scheduledOrder($condition);
		
		// cover stock. ada perubahan dari pak khalief. tanggal gr jadi tanggal po
		$val1 = ''; $val2 = ''; $cot = 0;
		$cond = " ORDER BY po_no_immbo DESC LIMIT 2 ";
		$listsgr = PO::getFreeSQL($cond);
		
		if($listsgr){
		foreach($listsgr as $listgr){
			if($cot == 0){ $val1 = $listgr['order_date']; $cot++; }				
			$val2 = $listgr['pr_no_immpos'];
		}}
		
		$second_date = $val1;
		$first_date = substr($val2,3,4).'-'.substr($val2,7,2).'-'.substr($val2,9,2);
		$selisih_hari = (strtotime($second_date) - strtotime($first_date)) / 86400;
		
		$data_pre_pr = null;
		$count = 0;
		if(isset($lists_detail_sum)){
			foreach($lists_detail_sum as $list_detail_sum){				
				// stok sekarang
				$stock = 0;
				foreach($lists_stock as $list_stock){
					if($list_detail_sum['item_code'] == trim($list_stock['stock_id'])){
						$stock = trim($list_stock['stock_qty']);
					}
				}
				
				if($stock < $list_detail_sum['qty_sum']){					
					// dms & cover stock
					$last_qty = 0; $countz = 0; $dms = 0.05; 
					$cond = " WHERE item_code = '".$list_detail_sum['item_code']."' ";
					$listsjds = JualDetailSum::getDMS($cond);
					if($listsjds){
					foreach($listsjds as $listjds){
						$dms = $dms + ($listjds['qty'] - $dms) * 0.1;
						//$last_qty += $listjds['qty'];
						//$countz++;
					}}
					
					//if($countz == 0){ $countz++; }
					//$dms = $last_qty / $countz;				
					$cover_stock = $dms * $selisih_hari;
					
					// on order
					$qty_onOrder = 0;
					$cond = " WHERE gr_no_immpos = '' AND item_code = '".$list_detail_sum['item_code']."' ";
					$lists_po_notgr = PO::getJoin_PODetail($cond);	

					if($lists_po_notgr){
					foreach($lists_po_notgr as $list_po_notgr){
						if($list_detail_sum['item_code'] == $list_po_notgr['item_code']){
							$qty_onOrder += $list_po_notgr['qty_po'];
						}
					}}
				
					// (dms * lead time) + (cover stock) + ((on order > 0 ? on order * -1 : on order) - stok sekarang)
					//if($qty_onOrder > 0){ $qty_onOrder = $qty_onOrder * -1; }
					//$new_qty = ($dms * 2) + $cover_stock + ($qty_onOrder - $stock);
					$new_qty = ($dms * 2) + $cover_stock - ($qty_onOrder + $stock);
					
					//echo $dms.'#'.$selisih_hari.'#'.$qty_onOrder.'#'.$stock.'<br/>';

					if(round($new_qty) > 0){
						$data_pre_pr[$count]['stock_id'] = $list_detail_sum['item_code'];
						$data_pre_pr[$count]['qty'] = round($new_qty);
					}
					
					$count++;
				}
			}
		}
		
		if(isset($data_pre_pr)){
			$rg_item = '';
			$count = 0;
			foreach($data_pre_pr as $line_pre_pr){
				if($count > 0) { $rg_item .= ','; }
				$rg_item .= "'".$line_pre_pr['stock_id']."'";
				$count++;
			}
			
			if($rg_item != ''){
				$product = new Product();						
				$condition = " WHERE \"stock_id\" IN (".$rg_item.") ";
				$lists_product = $product::getFreeSQL($condition);
			}
			
			$number_range = new NumberRange();
			$number_range->trans_type = 'PR';
			$number_range->period = date('Ym');
			$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
			$lists = $number_range::getFreeSQL($condition);
			
			if(isset($lists)){
				foreach($lists as $list){
					$number_range->trans_type = $list['trans_type'];
					$number_range->period = $list['period'];
					$number_range->prefix = $list['prefix'];
					$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
				}
			}else{
				$number_range->trans_type = 'PR';
				$number_range->period = date('Ym');
				$number_range->prefix = 'PR';
				$number_range->from_ = '1';
				$number_range->to_ = '9999';
				$number_range->current_no = '1';
				$success = $number_range::goInsert($number_range);			
				$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
			}
			
			$tmuk = new TMUK();
			$lists_tmuk = $tmuk::getAll();
			
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
			}
			
			$total = 0;
			foreach($data_pre_pr as $line_pre_pr){
				if(isset($lists_product)){
					foreach($lists_product as $list_product){
						if($list_product['stock_id'] == $line_pre_pr['stock_id']){
							$index_ord = '';
							if($list_product['uom1_htype_3']){ $index_ord = 'uom1_'; }
							else if($list_product['uom2_htype_3']){ $index_ord = 'uom2_'; }
							else if($list_product['uom3_htype_3']){ $index_ord = 'uom3_'; }
							else if($list_product['uom4_htype_3']){ $index_ord = 'uom4_'; }
							
							$index_sell = '';
							if($list_product['uom1_htype_1']){ $index_sell = 'uom1_'; }
							else if($list_product['uom2_htype_1']){ $index_sell = 'uom2_'; }
							else if($list_product['uom3_htype_1']){ $index_sell = 'uom3_'; }
							else if($list_product['uom4_htype_1']){ $index_sell = 'uom4_'; }	
							
							if($index_ord != ''){						
								$qty_order = ceil(($line_pre_pr['qty'] * $list_product[$index_sell.'conversion']) / $list_product[$index_ord.'conversion']) ;
								$order_price = $list_product[$index_ord.'harga'] * $qty_order;
								$total += $order_price;
							}
						}
					}
				}
			}
			
			if($total >= 0){
				$pr = new PR();
				$pr->request_no = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;
				$pr->point_used = 'System';
				$pr->delivery_by = 'LSI';
				$pr->created_by = 'System';
				$pr->delivery_date = date('Y-m-d');
				$pr->comments = 'System PR';
				$pr->total = $total;
				$pr->supplier_id = $tmuk->supplier_id;
				$pr->tmuk = $tmuk->tmuk;
				$pr->status = '0';
				$success = $pr::goInsert($pr);
				
				if($success){
					$number_range->trans_type = 'PR';
					$success_num = $number_range::goAddNumber($number_range);
					
					foreach($data_pre_pr as $line_pre_pr){
						if(isset($lists_product)){
						foreach($lists_product as $list_product){
							if($list_product['stock_id'] == $line_pre_pr['stock_id']){
								$pr_detail = new PRDetail();
								$pr_detail->request_no = $pr->request_no;
								$pr_detail->item_code = $line_pre_pr['stock_id'];
								$pr_detail->description = $list_product['description'];
								$pr_detail->supplied_by = $list_product['supplied_by'];
								
								$index_ord = '';
								if($list_product['uom1_htype_3']){ $index_ord = 'uom1_'; }
								else if($list_product['uom2_htype_3']){ $index_ord = 'uom2_'; }
								else if($list_product['uom3_htype_3']){ $index_ord = 'uom3_'; }
								else if($list_product['uom4_htype_3']){ $index_ord = 'uom4_'; }
								
								$index_sell = '';
								if($list_product['uom1_htype_1']){ $index_sell = 'uom1_'; }
								else if($list_product['uom2_htype_1']){ $index_sell = 'uom2_'; }
								else if($list_product['uom3_htype_1']){ $index_sell = 'uom3_'; }
								else if($list_product['uom4_htype_1']){ $index_sell = 'uom4_'; }							

								if($index_ord != ''){
									$pr_detail->qty_order = ceil(($line_pre_pr['qty'] * $list_product[$index_sell.'conversion']) / $list_product[$index_ord.'conversion']) ;
									$pr_detail->unit_order = $list_product[$index_ord.'nm'];
									$pr_detail->con_order = $list_product[$index_ord.'conversion'];
									$pr_detail->order_price = $list_product[$index_ord.'harga'] * $pr_detail->qty_order;
									$pr_detail->unit_order_price = $list_product[$index_ord.'harga'];				
								}
								
								if($index_sell != ''){
									$pr_detail->qty_sell = ceil(( 1 * $list_product[$index_ord.'conversion']) / $list_product[$index_sell.'conversion']);
									$pr_detail->unit_sell = $list_product[$index_sell.'nm'];
									$pr_detail->con_sell = $list_product[$index_sell.'conversion'];
									
									$pr_detail->qty_pr = $line_pre_pr['qty'];
									$pr_detail->unit_pr = $list_product[$index_sell.'nm'];
									$pr_detail->con_pr = $list_product[$index_sell.'conversion'];
								}							
								
								$success = $pr_detail::goInsert($pr_detail);
							}
						}
						}
					}
				}
			}
		}
		return '';
	}
	
	public function ExpiredReturAction(){
		$tanggal = date('Y-m').'-01';
		
		$retur = new Retur();
		$condition = " WHERE \"flag_delete\" = false AND ( \"gr_no_immpos\" = '' OR \"gr_no_immpos\" IS NULL ) AND create_date < '".$tanggal."' ";
		$lists_retur = $retur::getFreeSQL($condition);
		
		if(isset($lists_retur)){
			foreach($lists_retur as $list_retur){
				$retur_update = new Retur();
				$retur_update->request_no = $list_retur['retur_no'];
				$retur_update->flag_delete = true;
				$retur_update->reference = 'EXPIRED';
				$success = $retur_update::goUpdate($retur_update);
			}
		return '';
		}
	}
	
	public function syncPRAction(){
		$saldo = (new TMUKController())->getSaldo();
		
		$pr_data = new PR();
		$condition = " WHERE \"flag_sync\" = false AND \"flag_delete\" = false AND \"status\" = '1' ";
		$lists_pr = $pr_data::getFreeSQL($condition);
		
		if(isset($lists_pr)){
			foreach($lists_pr as $list_pr){
				$pr = new PR();
				$pr->request_no = $list_pr['request_no'];
				$lists_h = $pr::getFirst($pr);
				
				$pr_detail = new PRDetail();
				$pr_detail->request_no = $list_pr['request_no'];
				$condition = " WHERE \"request_no\" ='".$pr_detail->request_no."' " ;
				$lists_d = $pr_detail::getFreeSQL($condition);
				
				foreach($lists_h as $list_h){
					$pr->request_no = $list_h['request_no'];
					$pr->point_used = $list_h['point_used'];
					$pr->reference = $list_h['reference'];
					$pr->delivery_by = $list_h['delivery_by'];
					$pr->created_by = $list_h['created_by'];
					$pr->create_date = $list_h['create_date'];
					$pr->delivery_date = $list_h['delivery_date'];
					$pr->comments = $list_h['comments'];
					$pr->total = $list_h['total'];
					$pr->supplier_id = $list_h['supplier_id'];
					$pr->tmuk = $list_h['tmuk'];
				}

				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/tmuk/get-mengendap");
				$saldo_mengendap = json_decode($result, true);
				$saldo_locked = new PR();
				$cond = " WHERE request_no::varchar NOT IN (SELECT pr_no_immpos FROM t_po)";
				$locked = $saldo_locked::getSumTotal($cond);
				$available = $saldo - $locked - $saldo_mengendap['message'];
				$cek = $available - $pr->total;

				if($cek >= 0){				
					$data = null;
					$data['sup'] = $pr->supplier_id;
					$data['tmuk'] = $pr->tmuk;
					$data['req'] = $pr->delivery_date;
					$data['pr'] = $pr->request_no;
					$data['total'] = $pr->total;
					$data['date'] = $pr->create_date;
					
					if($lists_d){
						foreach($lists_d as $list_d){
							$xx['id'] = $list_d['item_code'];
							$xx['qty'] = $list_d['qty_order'];
							$xx['price'] = $list_d['unit_order_price'];
							$xx['no'] = $list_d['pr_detail_item'];
							$prods[] = $xx;
						}
					}
					$data['lists'] = $prods;	

					$curl = new CurlClass();
					$result = $curl->post($this->di['api']['link']."/api/pr/send", $data);
				
					if($result != '' && $result != '0'){
						$pr_update = new PR();
						$pr_update->request_no = $list_pr['request_no'];
						$pr_update->requisition_no = $result;
						$pr_update->flag_sync = 't';
						$success = $pr_update::goUpdate($pr_update);
					}
				}
			}
			$last_sync = new LastSync();
			$a = 'PR';
			$condition = " WHERE \"tipe\" = '".$a."' ";
			$cek = $last_sync::getFreeSQL($condition);
			if($cek){
				$last_sync->tipe = 'PR';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goUpdate($last_sync);
			}else{
				$last_sync->id = 11;
				$last_sync->tipe = 'PR';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goInsert($last_sync);
			}
			return '';
		}else{
			$last_sync = new LastSync();
			$a = 'PR';
			$condition = " WHERE \"tipe\" = '".$a."' ";
			$cek = $last_sync::getFreeSQL($condition);
			if($cek){
				$last_sync->tipe = 'PR';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goUpdate($last_sync);
			}else{
				$last_sync->id = 11;
				$last_sync->tipe = 'PR';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goInsert($last_sync);
			}
			return '';
		}
		
	}
	
	public function syncPOAction(){
		$tmuk = new TMUK();
		$lists = $tmuk::getAll();

		foreach($lists as $listTMUK){
			$asn = $listTMUK['asn'];
		}

		if(isset($lists)){
			foreach($lists as $list){
				$tmuk->tmuk_id = $list['tmuk_id'];
				$tmuk->tmuk = $list['tmuk'];
			}
		}
		
		if(count($lists) > 0){	
			$curl = new CurlClass();
			$result = $curl->post($this->di['api']['link']."/api/po/get-po", [
					'tmuk' => $tmuk->tmuk,
					'kode' => 'PO-20180629060040004400001'
			]);

			$obj = json_decode($result, true);
			foreach ($obj['po'] as $key => $value) {

				$number_range = new NumberRange();
				$number_range->trans_type = 'MV';
				$number_range->period = date('Ym');
				$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
				$lists = $number_range::getFreeSQL($condition);

				if(isset($lists)){
					foreach($lists as $list){
						$number_range->trans_type = $list['trans_type'];
						$number_range->period = $list['period'];
						$number_range->prefix = $list['prefix'];
						$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
					}
				}else{
					$number_range->trans_type = 'MV';
					$number_range->period = date('Ym');
					$number_range->prefix = 'MV';
					$number_range->from_ = '1';
					$number_range->to_ = '9999';
					$number_range->current_no = '1';
					$success = $number_range::goInsert($number_range);			
					$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
				}
				foreach ($value['detail'] as $key => $val) {
					$product_move = new ProductMove();		
					$product_move->trans_no = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;
					$product_move->stock_id = $val['produk_kode'];
					$product_move->type = 'GR';
					$product_move->qty = $val['qty_pr'];
					$product_move->price = $val['price'];
					// $success = $product_move::goInsert($product_move);
					$moveQuery .= $product_move::insertSql($product_move);
					$success_num = $number_range::goAddNumber($number_range);
				}
				if($moveQuery != ''){
					$success = $product_move::executeQuery($moveQuery);
				}
			}
			// if(count($obj['po']) != 0){
			// 	foreach ($obj['po'] as $key => $value) {
			// 		$dataPO = new PO();
			// 		$condition = " WHERE \"pr_no_immpos\" = '".$value['nomor_pr']."' ";
			// 		$cek = $dataPO::getFreeSQL($condition);
			// 		if($cek){

			// 		}else{
			// 			$curl = new CurlClass();
			// 			$result = $curl->post($this->di['api']['link']."/api/po/send-po", [
			// 				'nomorpr' => $value['nomor_pr']
			// 			]);
			// 			$obj = json_decode($result, true);

			// 			if(!$result){
			// 				echo 'CURL Error:'.curl_error($curlHandle);
			// 			}else{
			// 				if($asn != 2){
			// 					if($obj['po'] == null){
			// 						$this->flashSession->success("Gagal;Saat ini data PR masih dalam tahap pemrosesan di Lotte Grosir.;error");
			// 					}else{
			// 						$dataPO = new PO();
			// 						$dataPOD = new PODetail();
			// 						$dataProduct = new Product();
			// 						foreach ($obj['po'] as $value) {
			// 							$condition = " WHERE \"po_no_ref\" = '".$value['nomor']."' ";
			// 							$cek = $dataPO::getFreeSQL($condition);
			// 							if($cek == null){
			// 								$dataPO->po_no_immbo = $value['id'];
			// 								$dataPO->po_no_ref = $value['nomor'];
			// 								$dataPO->order_date = $value['tgl_buat'];
			// 								$dataPO->delivery_date = $value['created_at'];
			// 								$dataPO->delivery_address = $value['created_at'];
			// 								$dataPO->comments = $value['keterangan'];
			// 								$dataPO->total_po = $value['total'];
			// 								$dataPO->total_pr = $obj['pr']['total'];
			// 								$dataPO->tax_included = 1;
			// 								$dataPO->pr_no_immbo = $value['nomor_pr'];
			// 								$dataPO->pr_no_immpos = $value['nomor_pr'];
			// 								$success = $dataPO::goInsert($dataPO);
			// 								foreach($value['detail'] as $mj){
			// 									$dataPOD->po_detail_item = $mj['id'];
			// 									$dataPOD->po_no_immbo = $value['id'];
			// 									$dataPOD->item_code = $mj['produk_kode'];
			// 									$dataPOD->description = $mj['produk']['nama'];
			// 									$dataPOD->qty_po = $mj['qty_po'];
			// 									$dataPOD->unit_po = $mj['unit_po'];
			// 									$dataPOD->po_unit_price = $mj['price'];
			// 									$dataPOD->po_price = $mj['qty_po'] * $mj['price'];
			// 									$dataPOD->flag_gr_ok = 'false';
			// 									$dataPOD->qty_pr = $mj['qty_pr'];

			// 									$dataProduct->stock_id = $mj['produk_kode'];
			// 									$dataProduct->uom1_harga = $mj['price'];

			// 									$success = $dataProduct::goUpdate($dataProduct);

			// 									$success = $dataPOD::goInsert($dataPOD);							
			// 								}
			// 								$pr_update = new PR();
			// 								$pr_update->request_no = $value['nomor_pr'];
			// 								$pr_update->po_no_immbo = $value['id'];
			// 								$success = $pr_update::goUpdate($pr_update);
			// 							}else{
			// 								$this->flashSession->success("Berhasil;PO diterima, dan sudah di GR.;success");
			// 							}
			// 						}
			// 						$this->flashSession->success("Berhasil;Data PO Berhasil diterima.;success");
			// 					}	
			// 				}else{
			// 					if($obj['po'] == null){
			// 						$this->flashSession->success("Gagal;Saat ini data PR masih dalam tahap pemrosesan di Lotte Grosir.;error");
			// 					}else{
			// 						$dataPO = new PO();
			// 						$dataPOD = new PODetail();
			// 						$dataProduct = new Product();

			// 						foreach ($obj['po'] as $value) {
			// 							try {
			// 								$curl = new CurlClass();
			// 								$result = $curl->post($this->di['api']['link']."/api/po/terima-po", [
			// 									'po' => $value['id'],
			// 								]);
			// 								$obj = json_decode($result, true);
			// 							// var_dump($obj);
			// 							// die;
			// 							} catch (Exception $e) {

			// 							}
			// 							$condition = " WHERE \"po_no_ref\" = '".$value['nomor']."' ";
			// 							$cek = $dataPO::getFreeSQL($condition);
			// 							if($cek == null){
			// 								$dataPO->po_no_immbo = $value['id'];
			// 								$dataPO->po_no_ref = $value['nomor'];
			// 								$dataPO->order_date = $value['tgl_buat'];
			// 								$dataPO->delivery_date = $value['created_at'];
			// 								$dataPO->delivery_address = $value['created_at'];
			// 								$dataPO->comments = $value['keterangan'];
			// 								$dataPO->total_po = $value['total'];
			// 								$dataPO->total_pr = $obj['pr']['total'];
			// 								$dataPO->tax_included = 1;
			// 								$dataPO->pr_no_immbo = $value['nomor_pr'];
			// 								$dataPO->pr_no_immpos = $value['nomor_pr'];
			// 								$success = $dataPO::goInsert($dataPO);
			// 								foreach($value['detail'] as $mj){
			// 									$dataPOD->po_detail_item = $mj['id'];
			// 									$dataPOD->po_no_immbo = $value['id'];
			// 									$dataPOD->item_code = $mj['produk_kode'];
			// 									$dataPOD->description = $mj['produk']['nama'];
			// 									$dataPOD->qty_po = $mj['qty_po'];
			// 									$dataPOD->unit_po = $mj['unit_po'];
			// 									$dataPOD->po_unit_price = $mj['price'];
			// 									$dataPOD->po_price = $mj['qty_po'] * $mj['price'];
			// 									$dataPOD->flag_gr_ok = 'false';
			// 									$dataPOD->qty_pr = $mj['qty_pr'];

			// 									$dataProduct->stock_id = $mj['produk_kode'];
			// 									$dataProduct->uom1_harga = $mj['price'];

			// 									$success = $dataProduct::goUpdate($dataProduct);

			// 									$success = $dataPOD::goInsert($dataPOD);			
			// 								}

			// 								$number_range = new NumberRange();
			// 								$number_range->trans_type = 'GR';
			// 								$number_range->period = date('Ym');
			// 								$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
			// 								$lists = $number_range::getFreeSQL($condition);

			// 								if(isset($lists)){
			// 									foreach($lists as $list){
			// 										$number_range->trans_type = $list['trans_type'];
			// 										$number_range->period = $list['period'];
			// 										$number_range->prefix = $list['prefix'];
			// 										$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
			// 									}
			// 								}else{
			// 									$number_range->trans_type = 'GR';
			// 									$number_range->period = date('Ym');
			// 									$number_range->prefix = 'GR';
			// 									$number_range->from_ = '1';
			// 									$number_range->to_ = '9999';
			// 									$number_range->current_no = '1';
			// 									$success = $number_range::goInsert($number_range);			
			// 									$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
			// 								}

			// 								$tmuk = new TMUK();
			// 								$lists_tmuk = $tmuk::getALL();

			// 								if(isset($lists_tmuk)){
			// 									foreach($lists_tmuk as $list_tmuk){
			// 										$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
			// 										$tmuk->tmuk = $list_tmuk['tmuk'];
			// 										$tmuk->supplier_id = $list_tmuk['supplier_id'];
			// 									}
			// 								}

			// 								$gr = new GR();
			// 								$gr->gr_no = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;	
			// 								$gr->create_by = $this->session->get("user")['user_name'];
			// 								$gr->delivery_date = $dataPO->delivery_date;		
			// 								$gr->comments = $dataPO->comments;	
			// 								$gr->pr_no_immpos = $dataPO->pr_no_immpos;
			// 								$gr->pr_no_immbo = $dataPO->pr_no_immbo;
			// 								$gr->po_no_immbo = $dataPO->po_no_immbo;
			// 								$gr->total_pr = $dataPO->total_pr;
			// 								$gr->total_po = $dataPO->total_po;
			// 								$gr->flag_sync = true;
			// 								$gr->gr_no_immbo = $dataPO->gr_no_immbo;
			// 								$success = $gr::goInsert($gr);

			// 								if($success){			
			// 									$success_num = $number_range::goAddNumber($number_range);

			// 									$po = new PO();
			// 									$po->po_no_immbo = $gr->po_no_immbo;
			// 									$po->gr_no_immpos = $gr->gr_no;
			// 									$success = $po::goUpdate($po);	

			// 									$number_range = new NumberRange();
			// 									$number_range->trans_type = 'MV';
			// 									$number_range->period = date('Ym');
			// 									$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
			// 									$lists = $number_range::getFreeSQL($condition);

			// 									if(isset($lists)){
			// 										foreach($lists as $list){
			// 											$number_range->trans_type = $list['trans_type'];
			// 											$number_range->period = $list['period'];
			// 											$number_range->prefix = $list['prefix'];
			// 											$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
			// 										}
			// 									}else{
			// 										$number_range->trans_type = 'MV';
			// 										$number_range->period = date('Ym');
			// 										$number_range->prefix = 'MV';
			// 										$number_range->from_ = '1';
			// 										$number_range->to_ = '9999';
			// 										$number_range->current_no = '1';
			// 										$success = $number_range::goInsert($number_range);			
			// 										$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
			// 									}

			// 									$POD = new PODetail();
			// 									$condition = " WHERE \"po_no_immbo\" = '".$gr->po_no_immbo."' ";
			// 									$lists_po_detail = $POD::getFreeSQL($condition);

			// 									$count = 0;
			// 									$rg_stock_id = '';
			// 									foreach($lists_po_detail as $list_po_detail){
			// 										if($count>0){ $rg_stock_id .= ','; }
			// 										$rg_stock_id .= "'".$list_po_detail['item_code']."'";
			// 										$count++;
			// 									}				

			// 									if(isset($rg_stock_id)){
			// 										$product = new Product();
			// 										$condition = " WHERE \"stock_id\" IN (".$rg_stock_id.") ";
			// 										$lists_product = $product::getFreeSQL($condition);
			// 									}
			// 									foreach($lists_po_detail as $list_po_detail){							
			// 										$stock_qty = 0;
			// 										$order_qty = $list_po_detail['qty_po'];
			// 										$stock_con = 1;
			// 										$order_con = 1;
			// 										foreach($lists_product as $list_product){
			// 											if($list_product['stock_id'] == $list_po_detail['item_code']){
			// 												$stock_qty = ( $order_qty * $order_con ) / $stock_con; 
			// 											}
			// 										}
			// 										$product_move = new ProductMove();		
			// 										$product_move->trans_no = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;
			// 										$product_move->stock_id = $list_po_detail['item_code'];
			// 										$product_move->type = 'GR';
			// 										$product_move->reference = $gr->gr_no;
			// 										$product_move->qty = $stock_qty;
			// 										$product_move->price = $list_po_detail['po_unit_price'];
			// 										$success = $product_move::goInsert($product_move);

			// 										$price = new ProductPrice();
			// 										$condition = " WHERE \"stock_id\" = '".$list_po_detail['item_code']."' ";
			// 										$cek_price = $price::getFreeSQL($condition);

			// 										$price->stock_id = $list_po_detail['item_code'];
			// 										$lists_price = $price::getFirst($price);
			// 										$curl = new CurlClass();
			// 										$result = $curl->post($this->di['api']['link']."/api/hpp/get-hpp", [
			// 											'kode_produk' => $list_po_detail['item_code'],
			// 											'kode_tmuk' => $tmuk->tmuk
			// 										]);
			// 										$cek_hpp = json_decode($result, true);
			// 										$map = 0;
			// 										$margin = 0;
			// 										$suggest = 0;
			// 										$changed = 0;

			// 										if($cek_hpp['message']['map'] > 0){
			// 											$map = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['map']));
			// 										}
			// 										if($cek_hpp['message']['margin_amount'] > 0){
			// 											$margin = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['margin_amount']));
			// 										}
			// 										if($cek_hpp['message']['suggest_price'] > 0){
			// 											$suggest = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['suggest_price']));
			// 										}
			// 										if($cek_hpp['message']['change_price'] > 0){
			// 											$changed = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['change_price']));
			// 										}

			// 										$abrev = 'IDR';
			// 										$price->stock_id = $list_po_detail['item_code'];
			// 										$price->sales_type_id = 1;
			// 										$price->curr_abrev = $abrev;

			// 										$harga = new CurlClass();
			// 										$data_harga = $harga->post($this->di['api']['link']."/api/produk/get-price", [
			// 											'tmuk' => $tmuk->tmuk,
			// 											'kode_produk' => $list_po_detail['item_code']
			// 										]);
			// 										$set_harga = json_decode($data_harga, true);

			// 										$setting = new CurlClass();
			// 										$data_setting = $setting->post($this->di['api']['link']."/api/produk/get-setting", [
			// 											'kode_produk' => $list_po_detail['item_code']
			// 										]);

			// 										$price->price = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($set_harga['message']));

			// 										$set = json_decode($data_setting, true);
			// 										foreach ($set['message'] as $val) {
			// 											$price->margin = $val['margin'];
			// 											$sell = $map*(1+($val['margin']/100));
			// 										}

			// 										$price->average_cost = $map;
			// 										$price->uom1_suggested_price = round($sell);
			// 										$price->uom1_rounding_price = round(round($sell)/100) * 100;
			// 										$price->uom1_changed_price = $changed;
			// 										$price->uom1_margin_amount = $margin;
			// 										$price->uom2_suggested_price = $suggest;
			// 										$price->uom2_rounding_price = 0;
			// 										$price->uom2_changed_price = 0;
			// 										$price->uom2_margin_amount = 0;
			// 										$price->uom3_suggested_price = 0;
			// 										$price->uom3_rounding_price = 0;
			// 										$price->uom3_changed_price = 0;
			// 										$price->uom3_margin_amount = 0;
			// 										$price->uom1_cost_price = 0;
			// 										$price->uom2_cost_price = 0;
			// 										$price->uom3_cost_price = 0;
			// 										$price->uom4_suggested_price = 0;
			// 										$price->uom4_rounding_price = 0;
			// 										$price->uom4_changed_price = 0;
			// 										$price->uom4_margin_amount = 0;
			// 										$price->uom4_cost_price = 0;
			// 										$price->uom1_member_price = 0;
			// 										$price->uom2_member_price = 0;
			// 										$price->uom3_member_price = 0;
			// 										$price->uom4_member_price = 0;

			// 										if($cek_price){
			// 											$priceQuery .= $price::updateSql($price);
			// 										}else{
			// 											$priceQuery .= $price::insertSql($price);
			// 										}
			// 									}
			// 									if($priceQuery != ''){
			// 										$success = $price::executeQuery($priceQuery);
			// 									}
			// 									$pr_update = new PR();
			// 									$pr_update->request_no = $value['nomor_pr'];
			// 									$pr_update->po_no_immbo = $value['id'];
			// 									$success = $pr_update::goUpdate($pr_update);
			// 								}else{						
			// 								}				
			// 							}else{
			// 							}
			// 						}
			// 					}						
			// 				}
			// 			}
			// 		}
			// 	}
			// }
			$last_sync = new LastSync();
			$a = 'PO';
			$condition = " WHERE \"tipe\" = '".$a."' ";
			$cek = $last_sync::getFreeSQL($condition);
			if($cek){
				$last_sync->tipe = 'PO';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goUpdate($last_sync);
			}else{
				$last_sync->id = 12;
				$last_sync->tipe = 'PO';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goInsert($last_sync);
			}
			return '';				
		}else{
			$last_sync = new LastSync();
			$a = 'PO';
			$condition = " WHERE \"tipe\" = '".$a."' ";
			$cek = $last_sync::getFreeSQL($condition);
			if($cek){
				$last_sync->tipe = 'PO';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goUpdate($last_sync);
			}else{
				$last_sync->id = 12;
				$last_sync->tipe = 'PO';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goInsert($last_sync);
			}
			return 1;
		}
	}		
	
	public function syncStockAction(){	
		set_time_limit(0); 
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getAll();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
			}
		}
		
		$product = new Product();
		// $condition = " WHERE \"visible\" = '0' ";
		$lists_move = $product::getJoin_ProductMove2();
		
		$data = array();	
		$data['tmuk'] = $tmuk->tmuk;
		
		$count = 0;
		$query_update = '';

		$stok = new Stock();
		$data_stock = $stok::getFreeSQL();

		if(isset($data_stock)){	
			$st = [];	
			foreach($data_stock as $data_stock){
				$data[$count]['stock_id'] = $data_stock['tgl_gr'];
				$data[$count]['tgl_gr'] = $data_stock['tgl_gr'];
				$data[$count]['no_gr'] = $data_stock['no_gr'];
				$data[$count]['stock_id'] = $data_stock['stock_id'];
				$data[$count]['qty'] = $data_stock['qty'];
				$data[$count]['sisa'] = $data_stock['sisa'];
				$data[$count]['total'] = $data_stock['total'];
				$st[] = $data[$count];
				$count++;				
			}
			$cek['tmuk'] = $tmuk->tmuk;
			$cek['lists'] = $st;

			$curl = new CurlClass();
			$result = $curl->post($this->di['api']['link']."/api/produk/send-stock", $cek);
		}

		$last_sync = new LastSync();
		$a = 'STOCK';
		$condition = " WHERE \"tipe\" = '".$a."' ";
		$cek = $last_sync::getFreeSQL($condition);
		if($cek){
			$last_sync->tipe = 'STOCK';
			$last_sync->sync_date = date('Y-m-d');
			$last_sync->sync_time = date('H:i:s');
			$success = $last_sync::goUpdate($last_sync);
		}else{
			$last_sync->id = 13;
			$last_sync->tipe = 'STOCK';
			$last_sync->sync_date = date('Y-m-d');
			$last_sync->sync_time = date('H:i:s');
			$success = $last_sync::goInsert($last_sync);
		}
		return '';
	}

	public function syncNilaiAllPersediaanAction(){
		$start_day     = new StartEndDay();
		$condition     = " WHERE \"flag_start\" = true AND \"flag_end\" = false ";
		$lists_start   = $start_day::getFreeSQL($condition);
		$cond          = $lists_start[0]['tanggal'];
		set_time_limit(0); 

		$product = new Product();
		$lists_jual = $product::getJoin_ProductMove6($cond);
		$lists_gr        = $product::getJoin_ProductMove7($cond);
		$lists_so        = $product::getJoin_ProductMove8($cond);
		$lists_rr        = $product::getJoin_ProductMove9($cond);
		$lists_ad        = $product::getJoin_ProductMove10($cond);
		$lists_pyr       = $product::getJoin_ProductMove11($cond);


		$hitung_jual = count($lists_jual);
		$trans_jual = new PersediaanJual();
		$jualQuery = $trans_jual::queryInsertJual($hitung_jual);

		$hitung_gr = count($lists_gr);
		$trans_gr = new PersediaanGR();
		$grQuery = $trans_gr::queryInsertGR($trans_gr);

		$hitung_so = count($lists_so);
		$trans_so = new PersediaanSO();
		$soQuery = $trans_so::queryInsertSO($trans_so);

		$hitung_rr = count($lists_rr);
		$trans_rr = new PersediaanRR();
		$rrQuery = $trans_rr::queryInsertRR($trans_rr);

		$hitung_ad = count($lists_ad);
		$trans_ad = new PersediaanAD();
		$adQuery = $trans_ad::queryInsertAD($trans_ad);

		$hitung_pyr = count($lists_pyr);
		$trans_pyr = new PersediaanPYR();
		$pyrQuery = $trans_pyr::queryInsertPYR($trans_pyr);


		$n_jual=0;$n_gr=0;$n_so=0;$n_rr=0;$n_ad=0;$n_pyr=0;

		foreach ($lists_jual as $key => $value_jual) {
			$trans_jual->stock_id = $value_jual['stock_id'];
			$trans_jual->qty = $value_jual['qty'];
			$trans_jual->tanggal = $cond;
			$trans_jual->flag_sync = false;
			if($n_jual == ($hitung_jual - 1)){
				$jualQuery .= $trans_jual::InsertEndSql($trans_jual);
			}else{
				$jualQuery .= $trans_jual::insertSql($trans_jual);
			}
			$n_jual++;
		}

		foreach ($lists_gr as $key => $value_gr) {
			$trans_gr->stock_id = $value_gr['stock_id'];
			$trans_gr->qty = $value_gr['qty'];
			$trans_gr->tanggal = $cond;
			$trans_gr->flag_sync = false;
			if($n_gr == ($hitung_gr - 1)){
				$grQuery .= $trans_gr::InsertEndSql($trans_gr);
			}else{
				$grQuery .= $trans_gr::insertSql($trans_gr);
			}
			$n_gr++;
		}

		foreach ($lists_so as $key => $value_so) {
			$trans_so->stock_id = $value_so['stock_id'];
			$trans_so->qty = $value_so['qty'];
			$trans_so->tanggal = $cond;
			$trans_so->flag_sync = false;
			if($n_so == ($hitung_so - 1)){
				$soQuery .= $trans_so::InsertEndSql($trans_so);
			}else{
				$soQuery .= $trans_so::insertSql($trans_so);
			}
			$n_so++;
		}


		foreach ($lists_rr as $key => $value_rr) {
			$trans_rr->stock_id = $value_rr['stock_id'];
			$trans_rr->qty = $value_rr['qty'];
			$trans_rr->tanggal = $cond;
			$trans_rr->flag_sync = false;
			if($n_rr == ($hitung_rr - 1)){
				$rrQuery .= $trans_rr::InsertEndSql($trans_rr);
			}else{
				$rrQuery .= $trans_rr::insertSql($trans_rr);
			}
			$n_rr++;
		}

		foreach ($lists_ad as $key => $value_ad) {
			$trans_ad->stock_id = $value_ad['stock_id'];
			$trans_ad->qty = $value_ad['qty'];
			$trans_ad->tanggal = $cond;
			$trans_ad->flag_sync = false;
			if($n_ad == ($hitung_ad - 1)){
				$adQuery .= $trans_ad::InsertEndSql($trans_ad);
			}else{
				$adQuery .= $trans_ad::insertSql($trans_ad);
			}
			$n_ad++;
		}

		foreach ($lists_pyr as $key => $value_pyr) {
			$trans_pyr->stock_id = $value_pyr['stock_id'];
			$trans_pyr->qty = $value_pyr['qty'];
			$trans_pyr->tanggal = $cond;
			$trans_pyr->flag_sync = false;
			if($n_pyr == ($hitung_pyr - 1)){
				$pyrQuery .= $trans_pyr::InsertEndSql($trans_pyr);
			}else{
				$pyrQuery .= $trans_pyr::insertSql($trans_pyr);
			}
			$n_pyr++;
		}


		$jualQuery .= $trans_jual::endSql($trans_jual);
		$grQuery .= $trans_gr::endSql($trans_gr);
		$soQuery .= $trans_so::endSql($trans_so);
		$rrQuery .= $trans_rr::endSql($trans_rr);
		$adQuery .= $trans_ad::endSql($trans_ad);
		$pyrQuery .= $trans_pyr::endSql($trans_pyr);

		if($jualQuery != ''){
			$success = $trans_jual::executeQuery($jualQuery);
		}
		if($grQuery != ''){
			$success = $trans_gr::executeQuery($grQuery);
		}
		if($soQuery != ''){
			$success = $trans_so::executeQuery($soQuery);
		}
		if($rrQuery != ''){
			$success = $trans_rr::executeQuery($rrQuery);
		}
		if($adQuery != ''){
			$success = $trans_ad::executeQuery($adQuery);
		}
		if($pyrQuery != ''){
			$success = $trans_pyr::executeQuery($pyrQuery);
		}
		
		return '';
	}

	public function InsertHPPAction(){
		$start_day     = new StartEndDay();
		$condition     = " WHERE \"flag_start\" = true AND \"flag_end\" = false ";
		$lists_start   = $start_day::getFreeSQL($condition);
		$cond          = $lists_start[0]['tanggal'];
		set_time_limit(0); 
		$prod = new Product();
		$insert_hpp = $prod::getCariHPP($cond);
		$hitung = $prod::getCount();
		$trans_hpp = new TransHpp();
		$hppQuery = $trans_hpp::queryInsertHPP($trans_hpp);
		$n=0;
		foreach ($insert_hpp as $key => $value) {
			$trans_hpp->stock_id = $value['stock_id'];
			$trans_hpp->qty = $value['qty'];
			$trans_hpp->hpp = $value['hpp'];
			$trans_hpp->tanggal = $cond;
			$trans_hpp->flag_sync = false;
			if($n == ($hitung - 1)){
				$hppQuery .= $trans_hpp::InsertEndSql($trans_hpp);
			}else{
				$hppQuery .= $trans_hpp::insertSql($trans_hpp);
			}
			$n++;
		}
		$hppQuery .= $trans_hpp::endSql($trans_hpp);

		if($hppQuery != ''){
			$success = $trans_hpp::executeQuery($hppQuery);
		}
		return '';
	}

	public function InsertNilaiPersediaanAction(){
		$start_day     = new StartEndDay();
		$condition     = " WHERE \"flag_start\" = true AND \"flag_end\" = false ";
		$lists_start   = $start_day::getFreeSQL($condition);
		$cond          = $lists_start[0]['tanggal'];
		set_time_limit(0); 

		$prod = new Product();
		$nilai_persediaan =$prod::getCariHPP($cond);
		$hitung = $prod::getCount();
		$persediaan = new NilaiPersediaan();
		$persediaanQuery = $persediaan::queryNilaiPersediaan($persediaan);
		$n=0;
		foreach ($nilai_persediaan as $key => $value) {
			$persediaan->stock_id = $value['stock_id'];
			$persediaan->qty = $value['qty'];
			$persediaan->map = $value['hpp'];
			$persediaan->nilai =  $value['qty'] * $value['hpp'];
			$persediaan->tanggal = $cond;
			$persediaan->flag_sync = false;
			if($n == ($hitung - 1)){
				$persediaanQuery .= $persediaan::InsertEndSql($persediaan);
			}else{
				$persediaanQuery .= $persediaan::insertSql($persediaan);
			}
			$n++;
		}
		$persediaanQuery .= $persediaan::endSql($persediaan);
		if($persediaanQuery != ''){
			$success = $persediaan::executeQuery($persediaanQuery);
		}
		return '';
	}
	
	public function syncPenjualanAction(){
		// 
		$arr_tgl = [];
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getAll();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
			}
		}

		$trans_hpp = new TransHpp();
		$condition = " WHERE flag_sync = false ";
		$list_hpp = $trans_hpp::getFreeSQL($condition);

		if(count($list_hpp)>0){
			foreach($list_hpp as $list1){
				// kebutuhan buat jurnal
				$arr_tgl[substr($list1['tanggal'], 0, 10)] = substr($list1['tanggal'], 0, 10);
			}
		}
		/*kimochi sync shift penjualan J-101, J-102, J-103 */
		$shifts = EOS::getFreeSQL(' WHERE "flag_sync" = false ');

		$curl = new CurlClass();
		foreach ($shifts as $row) {
			$req = [
				'tmuk' => $tmuk->tmuk,
				'data' => $row
			];

			$result = $curl->post($this->di['api']['link']."/api/penjualan/sync-end-shift", $req);

			// transform json to object array php
			$obj = json_decode($result, true);

			if($obj['status'] == 'success'){
				// update flag sync end of shift record
				$update = new EOS();
				$update->id        = $row['id'];
				$update->flag_sync = true;
				$success = $update::goUpdate($update);
				$update::goUpdate($update);
			}
		}
		/* end of sync end shift */

		/*kimochi sync total pendapatan penjualan */
		if(count($arr_tgl)>0){
			$curl = new CurlClass();

			foreach ($arr_tgl as $val) { // pertanggal
				$jual = new Jual();
				$jual_detail = new JualDetail();
				$total_penjualan    = $jual_detail->getTotal($val);
				$piutang_member     = $jual->getTotalPembayaranSync(['PIUTANG'], $val);
				$point_member       = $jual->getTotalPembayaranSync(['POINT'], $val);
				$potongan_voucher	= $jual->getTotalPembayaranSync(['VOUCHER'], $val);
				$potongan_diskon 	= $jual_detail->getTotalDiskon($val);
				$potongan_member 	= $jual_detail->getTotalMember($val);
				$potongan_penjualan = $potongan_voucher + $potongan_diskon + $potongan_member;
				// $penjualan_barang   = $total_penjualan + $piutang_member + $point_member + $potongan_penjualan;

				$hpp        = $jual->getHppPenjualanSync($val);
				$persediaan = $hpp;
				$total = $total_penjualan - $potongan_penjualan - $piutang_member - $point_member;
				// proses to ho
				$req = [
					'tmuk' => $tmuk->tmuk,
					'data' => [
						'tanggal'            => $val,
						'jam'                => date('H:i:s'),
						'total_penjualan'    => $total ? $total : 0,
						'piutang_member'     => $piutang_member ? $piutang_member : 0,
						'point_member'       => $point_member ? $point_member : 0,
						'potongan_penjualan' => $potongan_penjualan ? $potongan_penjualan : 0,
						'penjualan_barang'   => $total_penjualan ? $total_penjualan : 0,
						'hpp'                => $hpp ? $hpp : 0,
						'persediaan'         => $persediaan ? $persediaan : 0,
					]
				];
				$result = $curl->post($this->di['api']['link']."/api/penjualan/sync-penjualan", $req);
				$obj = json_decode($result, true);
				if($obj['status'] == 'success'){
					$trans_hpp->tanggal = $val;
					$trans_hpp->flag_sync = true;
					$success = $trans_hpp::goUpdate($trans_hpp);
				}
			}
		}
		/* end sync total pendapatan penjualan */
		
		// jang notif
		$last_sync = new LastSync();
		$last_sync->tipe = 'PENJUALAN';
		$last_sync->sync_date = date('Y-m-d');
		$last_sync->sync_time = date('H:i:s');
		$success = $last_sync::goUpdate($last_sync);
		return '';
	}
	
	public function syncBankAction(){
		$tmuk = new TMUK();
		$lists = $tmuk::getAll();
		
		if(isset($lists)){
			foreach($lists as $list){
				$tmuk->tmuk_id = $list['tmuk_id'];
				$tmuk->tmuk = $list['tmuk'];
			}
		}
		
		if(isset($lists)){
			$curl = new CurlClass();
			$result = $curl->get($this->di['api']['link']."/api/bank/get-bank");
			$obj = json_decode($result, true);
			foreach ($obj['message'] as $key => $value) {
				$bank = new Bank();
				$condition = " WHERE \"bank_id\" = '".$value['id']."' ";
				$cek = $bank::getFreeSQL($condition);
				if($cek){
					$bank->bank_id = $value['id'];
					$bank->bank_name = $value['nama_pemilik'];
					$bank->inactive = 0;
					$success = $bank::goUpdate($bank);
				}else{
					$bank->bank_id = $value['id'];
					$bank->bank_name = $value['nama_pemilik'];
					$bank->inactive = 0;
					$success = $bank::goInsert($bank);
				}
			}

			$last_sync = new LastSync();
			$a = 'BANK';
			$condition = " WHERE \"tipe\" = '".$a."' ";
			$cek = $last_sync::getFreeSQL($condition);
			if($cek){
				$last_sync->tipe = 'BANK';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goUpdate($last_sync);
			}else{
				$last_sync->id = 14;
				$last_sync->tipe = 'BANK';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goInsert($last_sync);
			}					
		return '';
		}
	}
	
	public function syncRackAction(){
		$tmuk = new TMUK();
		$lists = $tmuk::getAll();
		
		if(isset($lists)){
			foreach($lists as $list){
				$tmuk->tmuk_id = $list['tmuk_id'];
				$tmuk->tmuk = $list['tmuk'];
			}
		}
		
		if(isset($lists)){	
			$curl = new CurlClass();
			$result = $curl->post($this->di['api']['link']."/api/rack/", [
					'tmuk' => $tmuk->tmuk
			]);
			$obj = json_decode($result, true);
			
			if ($obj['status'] == true){
				$jsons_array = $obj['message'];
				// Insert Rack		
				$rack = new Rack();
				
				$count = 0;
				$query_delete = '';
				$query_insert = '';
				if(isset($jsons_array)){
				foreach ($jsons_array as $json){
					if($count >= 500){
						$success = $rack::goMultiDelete($query_delete);
						$success = $rack::goMultiInsert($query_insert);
						$count = 0;
						$query_delete = '';
						$query_insert = '';
					}
					
					if($count > 0) { $query_delete .= ","; $query_insert .= ","; }
					$query_delete .= "'".$json['post']['id']."'";
					$query_insert .= "('".$json['post']['id']."','".$json['post']['rack_type']."','".$json['post']['tinggi']."','".$json['post']['panjang'].
									"','".$json['post']['lebar']."','".$json['post']['shelving']."','".$json['post']['hunger']."','".$json['post']['tinggi_hunger'].
									"','".$json['post']['panjang_hunger']."')";
					
					$count++;
				}
				}
				
				if($query_delete != '' || $query_insert != ''){
					$success = $rack::goMultiDelete($query_delete);
					$success = $rack::goMultiInsert($query_insert);
					$count = 0;
					$query_delete = '';
					$query_insert = '';
				}
				
				$last_sync = new LastSync();
				$a = 'RACK';
				$condition = " WHERE \"tipe\" = '".$a."' ";
				$cek = $last_sync::getFreeSQL($condition);
				if($cek){
					$last_sync->tipe = 'RACK';
					$last_sync->sync_date = date('Y-m-d');
					$last_sync->sync_time = date('H:i:s');
					$success = $last_sync::goUpdate($last_sync);
				}else{
					$last_sync->id = 15;
					$last_sync->tipe = 'RACK';
					$last_sync->sync_date = date('Y-m-d');
					$last_sync->sync_time = date('H:i:s');
					$success = $last_sync::goInsert($last_sync);
				}
			}
		return '';
		}
	}
	
	public function syncPOSAction(){
		$tmuk = new TMUK();
		$lists = $tmuk::getAll();
		
		if(isset($lists)){
			foreach($lists as $list){
				$tmuk->tmuk_id = $list['tmuk_id'];
				$tmuk->tmuk = $list['tmuk'];
			}
		}
		
		if(isset($lists)){					
			$curl = new CurlClass();
			$result = $curl->post($this->di['api']['link']."/api/pos/", [
					'tmuk' => $tmuk->tmuk
			]);
			$obj = json_decode($result, true);
			
			if ($obj['status'] == true){
				$jsons_array = $obj['message'];
				
				// Insert Kassa		
				$kassa = new Kassa();
				
				$count = 0;
				$query_delete = '';
				$query_insert = '';
				if(isset($jsons_array)){
				foreach ($jsons_array as $json){
					if($count >= 500){
						$success = $kassa::goMultiDelete($query_delete);
						$success = $kassa::goMultiInsert($query_insert);
						$count = 0;
						$query_delete = '';
						$query_insert = '';
					}
					
					if($count > 0) { $query_delete .= ","; $query_insert .= ","; }
					$query_delete .= "'".$json['post']['id']."'";
					$query_insert .= "('".$json['post']['id']."','".$json['post']['machine_name']."')";
					
					$count++;
				}
				}
				
				if($query_delete != '' || $query_insert != ''){
					$success = $kassa::goMultiDelete($query_delete);
					$success = $kassa::goMultiInsert($query_insert);
					$count = 0;
					$query_delete = '';
					$query_insert = '';
				}
				
				$last_sync = new LastSync();
				$a = 'POS';
				$condition = " WHERE \"tipe\" = '".$a."' ";
				$cek = $last_sync::getFreeSQL($condition);
				if($cek){
					$last_sync->tipe = 'POS';
					$last_sync->sync_date = date('Y-m-d');
					$last_sync->sync_time = date('H:i:s');
					$success = $last_sync::goUpdate($last_sync);
				}else{
					$last_sync->id = 16;
					$last_sync->tipe = 'POS';
					$last_sync->sync_date = date('Y-m-d');
					$last_sync->sync_time = date('H:i:s');
					$success = $last_sync::goInsert($last_sync);
				}
			}
		return '';	
		}
	}
	
	public function syncTMUKAction(){
		$tmuk = new TMUK();
		$lists = $tmuk::getAll();
		
		if(isset($lists)){
			foreach($lists as $list){
				$tmuk->tmuk_id = $list['tmuk_id'];
				$tmuk->tmuk = $list['tmuk'];
			}
		}
		
		if(isset($lists)){					
			$curl = new CurlClass();
			$result = $curl->post($this->di['api']['link']."/api/tmuk/get-tmuk", [
				'tmuk' => $tmuk->tmuk
			]);
			if($result){
				$obj = json_decode($result, true);
				$data = new TMUK();
				$condition = " WHERE \"tmuk\" = '".$tmuk->tmuk."' ";
				$cek = $data::getFreeSQL($condition);
				$data->tmuk_id = $tmuk->tmuk_id; 
				$data->tmuk = $obj['tmuk']['kode']; 
				$data->telp = $obj['tmuk']['telepon']; 
				$data->name = $obj['tmuk']['nama']; 
				$data->owner = $obj['tmuk']['rekeningescrow']['nama_pemilik']; 
				$data->address1 = $obj['tmuk']['alamat'];  
				$data->address2 = $obj['tmuk']['kecamatan']['nama'].', '. $obj['tmuk']['kota']['nama'];  
				$data->address3 = $obj['tmuk']['provinsi']['nama'];  
				$data->tipe = $obj['tmuk']['kode']; 
				$data->escrow_account = $obj['tmuk']['rekeningescrow']['nomor_rekening']; 
				$data->nama_owner = $obj['tmuk']['rekeningescrow']['nama_pemilik']; 
				$data->kode_bank = $obj['tmuk']['bankescrow']['kode_bank']; 
				$data->nama_bank = $obj['tmuk']['bankescrow']['nama'];
				$data->nama_lsi = $obj['tmuk']['lsi']['nama'];
				$auto = $obj['tmuk']['auto_approve'];
				if($auto == 0){
					$data->auto_approve = 1;
				}else{
					$data->auto_approve = 2;
				}
				$data_asn = $obj['tmuk']['asn'];
				if($data_asn == 0){
					$data->asn = 1;
				}else{
					$data->asn = 2;
				}

				if($cek){
					$success = $data::goUpdate($data);
				}else{
					$success = $data::goInsert($data);
				}	

				$last_sync = new LastSync();
				$a = 'TMUK';
				$condition = " WHERE \"tipe\" = '".$a."' ";
				$cek = $last_sync::getFreeSQL($condition);
				if($cek){
					$last_sync->tipe = 'TMUK';
					$last_sync->sync_date = date('Y-m-d');
					$last_sync->sync_time = date('H:i:s');
					$success = $last_sync::goUpdate($last_sync);
				}else{
					$last_sync->id = 17;
					$last_sync->tipe = 'TMUK';
					$last_sync->sync_date = date('Y-m-d');
					$last_sync->sync_time = date('H:i:s');
					$success = $last_sync::goInsert($last_sync);
				}
			}
		return '';										
		}
	}
	
	// public function syncOpnameAction(){		
	// 	$so = new SO();
	// 	$condition = " WHERE \"flag_sync\" = false AND status = 'COMPLETE' ";
	// 	$lists_so = $so::getFreeSQL($condition);
		
	// 	$query_list = '';
	// 	$count = 0;
	// 	if(isset($lists_so)){
	// 		foreach($lists_so as $list_so){
	// 			if($count > 0) { $query_list .= ','; }
	// 			$query_list .= "'".$list_so['so_id']."'";
	// 			$count++;
	// 		}
	// 	}
		
	// 	$lists_so_detail = null;
	// 	if($query_list != ''){
	// 		$so_detail = new SODetail();
	// 		$condition = " WHERE \"so_id\" IN (".$query_list.") ";
	// 		$lists_so_detail = $so_detail::getFreeSQL($condition);
	// 	}
		
	// 	$tmuk = new TMUK();
	// 	$lists_tmuk = $tmuk::getAll();
		
	// 	if(isset($lists_tmuk)){
	// 		foreach($lists_tmuk as $list_tmuk){
	// 			$tmuk->tmuk = $list_tmuk['tmuk'];
	// 		}
	// 	}
		
	// 	$data = null;	
	// 	if(isset($lists_so)){
	// 		$data = [];	
	// 		foreach($lists_so as $list_sos){
	// 			$data['tmuk'] = $tmuk->tmuk;
	// 			$data['so_id'] = $list_sos['so_id'];
	// 			$data['so_date'] = $list_sos['so_date'];
	// 			$data['so_type'] = $list_sos['so_type'];

	// 			$so_detail = new SODetail();
	// 			$condition = " WHERE \"so_id\" = '".$list_sos['so_id']."' ";
	// 			$hitung = $so_detail::getCount2($condition);
	// 			$data['qty_tersedia'] = $hitung['0']['qty_tersedia'];
	// 			$data['qty_barcode'] = $hitung['0']['qty_barcode'];
	// 			$prods[] = $data;

	// 			$so = new SO();
	// 			$so->so_id = $list_sos['so_id'];
	// 			$so->flag_sync = true;
	// 			$success = $so::goUpdate($so);
	// 		}
	// 		$cek['tmuk'] = $tmuk->tmuk;
	// 		$cek['lists'] = $prods;
	// 		$curl = new CurlClass();
	// 		$result = $curl->post($this->di['api']['link']."/api/produk/send-opname", $cek);
	// 		return '';
	// 	}
		
	// 	$last_sync = new LastSync();
	// 	$a = 'OPNAME';
	// 	$condition = " WHERE \"tipe\" = '".$a."' ";
	// 	$cek = $last_sync::getFreeSQL($condition);
	// 	if($cek){
	// 		$last_sync->tipe = 'OPNAME';
	// 		$last_sync->sync_date = date('Y-m-d');
	// 		$last_sync->sync_time = date('H:i:s');
	// 		$success = $last_sync::goUpdate($last_sync);
	// 	}else{
	// 		$last_sync->id = 18;
	// 		$last_sync->tipe = 'OPNAME';
	// 		$last_sync->sync_date = date('Y-m-d');
	// 		$last_sync->sync_time = date('H:i:s');
	// 		$success = $last_sync::goInsert($last_sync);
	// 	}
	// 	return '';
	// }

	public function syncOpnameAction(){		
		$so = new SO();
		$condition = " WHERE \"flag_sync\" = false AND status = 'COMPLETE' ";
		$lists_so = $so::getFreeSQL($condition);
		
		$query_list = '';
		$count = 0;
		if(isset($lists_so)){
			foreach($lists_so as $list_so){
				if($count > 0) { $query_list .= ','; }
				$query_list .= "'".$list_so['so_id']."'";
				$count++;
			}
		}
		
		$lists_so_detail = null;
		if($query_list != ''){
			$so_detail = new SODetail();
			$condition = " WHERE \"so_id\" IN (".$query_list.") ";
			$lists_so_detail = $so_detail::getFreeSQL($condition);
		}
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getAll();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk = $list_tmuk['tmuk'];
			}
		}
		
		$data = null;	
		if(isset($lists_so)){
			$data = [];	
			foreach($lists_so as $list_sos){
				$data['tmuk'] = $tmuk->tmuk;
				$data['so_id'] = $list_sos['so_id'];
				$data['so_date'] = $list_sos['so_date'];
				$data['so_type'] = $list_sos['so_type'];
				$so_detail = new SODetail();
				$condition = " WHERE \"so_id\" = '".$list_sos['so_id']."' ";
				$hitung = $so_detail::getFreeSQL($condition);
				$cek = $so_detail::getSum($condition)[0];
				$data['qty_system'] = $cek['qty_tersedia'];
				$data['qty_barcode'] = $cek['qty_barcode'];
				if(isset($hitung)){
					foreach($hitung as $hitung){
						$price = new ProductPrice();
						$condition = " WHERE \"stock_id\" = '".$hitung['item_code']."' ";
						$cek_price = $price::getFreeSQL($condition)[0];
						if($cek_price['average_cost'] > 0){
							$harga = $cek_price['average_cost'];
						}else{
							$harga = 0;
						}
						$xx['item_code']    			= $hitung['item_code'];
						$xx['qty_barcode']   			= $hitung['qty_barcode'];
						$xx['qty_tersedia']   			= $hitung['qty_tersedia'];
						$xx['map']   					= $harga;
						$prods[] = $xx;
					}
				}

				$data['lists'] = $prods;
				$so = new SO();
				$so->so_id = $list_sos['so_id'];
				$so->flag_sync = true;
				$success = $so::goUpdate($so);
				$cek['cek'] = $data;
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/produk/send-opname", $cek);
				$cek = null;
				$data = null;
			}
			return '';
		}
		
		$last_sync = new LastSync();
		$a = 'OPNAME';
		$condition = " WHERE \"tipe\" = '".$a."' ";
		$cek = $last_sync::getFreeSQL($condition);
		if($cek){
			$last_sync->tipe = 'OPNAME';
			$last_sync->sync_date = date('Y-m-d');
			$last_sync->sync_time = date('H:i:s');
			$success = $last_sync::goUpdate($last_sync);
		}else{
			$last_sync->id = 18;
			$last_sync->tipe = 'OPNAME';
			$last_sync->sync_date = date('Y-m-d');
			$last_sync->sync_time = date('H:i:s');
			$success = $last_sync::goInsert($last_sync);
		}
		return '';
	}


	public function syncPiutangMemberAction(){		
		$member = new Member();
		$utang = $member::getMemberPiutangAll();
		foreach ($utang as $value) {
			try {
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/member/send-piutang", [
					'tmuk_kode' => $value['tmuk_kode'],
					'nomor_member' => $value['nomor_member'],
					'nama_member' => $value['nama_member'],
					'total' => $value['total'],
					'tanggal' => $value['tanggal'],
				]);
				$obj = json_decode($result, true);
			} catch (Exception $e) {
				
			}
		}

		$last_sync = new LastSync();
		$a = 'PIUTANG';
		$condition = " WHERE \"tipe\" = '".$a."' ";
		$cek = $last_sync::getFreeSQL($condition);
		if($cek){
			$last_sync->tipe = 'PIUTANG';
			$last_sync->sync_date = date('Y-m-d');
			$last_sync->sync_time = date('H:i:s');
			$success = $last_sync::goUpdate($last_sync);
		}else{
			$last_sync->id = 21;
			$last_sync->tipe = 'PIUTANG';
			$last_sync->sync_date = date('Y-m-d');
			$last_sync->sync_time = date('H:i:s');
			$success = $last_sync::goInsert($last_sync);
		}

		return '';
	}

	public function syncPointAction(){		
			try {
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/point/get-kalkulasi-point");
				$obj = json_decode($result, true);
				if(count($obj['data'])>0){
					foreach ($obj['data'] as $value) {
						$point = new PointTmuk();
						$cek = $point::checkAda($value['id']);
						if($cek==true){
							$point->id = $value['id'];
							$point->tgl_berlaku = $value['tgl_berlaku'];
							$point->konversi = $value['konversi'];
							$point->faktor_konversi = $value['faktor_konversi'];
							$point->faktor_reedem = $value['faktor_reedem'];
							$point->status = $value['status'];
							$success = $point::goUpdate($point);
						}else{
							$point->id = $value['id'];
							$point->tgl_berlaku = $value['tgl_berlaku'];
							$point->konversi = $value['konversi'];
							$point->faktor_konversi = $value['faktor_konversi'];
							$point->faktor_reedem = $value['faktor_reedem'];
							$point->status = $value['status'];
							$success = $point::goInsert($point);
						}
					}
				}
			} catch (Exception $e) {
				
			}

		return '';
	}

	public function syncPointMemberAction(){		
		$member = new Member();
		$point = $member::getMemberPointAll();
		foreach ($point as $value) {
			try {
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/member/send-point-member", [
					'tmuk_kode' => $value['tmuk_kode'],
					'nomor_member' => $value['nomor_member'],
					'nama_member' => $value['nama_member'],
					'point' => $value['point'],
					'tanggal' => $value['tanggal'],
				]);
				$obj = json_decode($result, true);
			} catch (Exception $e) {
				
			}
		}

		$last_sync = new LastSync();
		$a = 'POINT';
		$condition = " WHERE \"tipe\" = '".$a."' ";
		$cek = $last_sync::getFreeSQL($condition);
		if($cek){
			$last_sync->tipe = 'POINT';
			$last_sync->sync_date = date('Y-m-d');
			$last_sync->sync_time = date('H:i:s');
			$success = $last_sync::goUpdate($last_sync);
		}else{
			$last_sync->id = 22;
			$last_sync->tipe = 'POINT';
			$last_sync->sync_date = date('Y-m-d');
			$last_sync->sync_time = date('H:i:s');
			$success = $last_sync::goInsert($last_sync);
		}

		return '';
	}
	
	public function ajaxChangedAction(){
		$data = new ProductPrice();		
		$condition = " WHERE flag_price = true AND mb_flag != 'D' AND ppob = 0 AND inactive = 0 AND status_flag != '2' AND tanggal_change > ( NOW() - INTERVAL '5 DAYS' ) ";
		$lists_product = $data::getCountLabel($condition);
		return $lists_product;
	}
	
	public function syncPlanogramAction(){
		$tmuk = new TMUK();
		$lists = $tmuk::getAll();
		
		if(isset($lists)){
			foreach($lists as $list){
				$tmuk->tmuk_id = $list['tmuk_id'];
				$tmuk->tmuk = $list['tmuk'];
			}
		}
		
		$rack = new Rack();
		$lists_rack = $rack::getAll();
		
		if(isset($lists)){	
			if(isset($lists_rack)){
				foreach($lists_rack as $list_rack){
					$api_link =  $this->di['api']['link'];
					$split_link = explode('API', $api_link);
					for($i=1; $i <= 5; $i++){
						$image_link = $split_link[0].'planogram/'.$tmuk->tmuk.'_'.$list_rack['rack_type'].'_'.$i.'.png';
						$excel_link = $split_link[0].'purchasing/'.$tmuk->tmuk.'_'.$list_rack['rack_type'].'_'.$i.'.xls';
						
						$file_headers = @get_headers($image_link);
						if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {}
						else{
							set_time_limit(0); 
							$file = file_get_contents($image_link);
							file_put_contents('sync/planogram/'.$list_rack['rack_type'].'_'.$i.'.png', $file);
						}
						
						$file_headers = @get_headers($excel_link);
						if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {}
						else{
							set_time_limit(0); 
							$file = file_get_contents($excel_link);
							file_put_contents('sync/planogram/'.$list_rack['rack_type'].'_'.$i.'.xls', $file);
						}
					}
				}
			}
			
			$last_sync = new LastSync();
			$a = 'PLANOGRAM';
			$condition = " WHERE \"tipe\" = '".$a."' ";
			$cek = $last_sync::getFreeSQL($condition);
			if($cek){
				$last_sync->tipe = 'PLANOGRAM';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goUpdate($last_sync);
			}else{
				$last_sync->id = 19;
				$last_sync->tipe = 'PLANOGRAM';
				$last_sync->sync_date = date('Y-m-d');
				$last_sync->sync_time = date('H:i:s');
				$success = $last_sync::goInsert($last_sync);
			}
		}
		
		return '';
	}
	
	public function initializeAction(){
		$tmuk = $_GET['tmuk_id'];
		if($tmuk){
			$curl = new CurlClass();
			$result = $curl->post($this->di['api']['link']."/api/aktivasi/aktivasi-tmuk", [
				'tmuk' => $tmuk
			]);
			$obj = json_decode($result, true);
			if($obj){
				$data_tmuk = new TMUK();
				$condition = " WHERE \"tmuk\" = '".$tmuk."' ";
				$cek = $data_tmuk::getFreeSQL($condition);

				$data_tmuk->tmuk = $obj['tmuk']['kode']; 
				$data_tmuk->telp = $obj['tmuk']['telepon']; 
				$data_tmuk->name = $obj['tmuk']['nama']; 
				if($obj['tmuk']['saldo_deposit'] == null){
					$data_tmuk->saldo = 0; 
				}else{
					$data_tmuk->saldo = $obj['tmuk']['saldo_deposit']; 
				}
				$data_tmuk->owner = $obj['tmuk']['rekeningescrow']['nama_pemilik']; 
				$data_tmuk->address1 = $obj['tmuk']['alamat'];  
				$data_tmuk->address2 = $obj['tmuk']['kecamatan']['nama'].', '. $obj['tmuk']['kota']['nama'];  
				$data_tmuk->address3 = $obj['tmuk']['provinsi']['nama'];  
				$data_tmuk->tipe = $obj['tmuk']['kode']; 
				$data_tmuk->escrow_account = $obj['tmuk']['rekeningescrow']['nomor_rekening']; 
				$data_tmuk->nama_owner = $obj['tmuk']['rekeningescrow']['nama_pemilik']; 
				$data_tmuk->kode_bank = $obj['tmuk']['bankescrow']['kode_bank']; 
				$data_tmuk->nama_bank = $obj['tmuk']['bankescrow']['nama'];
				$data_tmuk->nama_lsi = $obj['tmuk']['lsi']['nama'];
				$data_tmuk->length_struk = 48;
				$auto = $obj['tmuk']['auto_approve'];
				if($auto == 0){
					$data_tmuk->auto_approve = 1;
				}else{
					$data_tmuk->auto_approve = 2;
				}
				$data_asn = $obj['tmuk']['asn'];
				if($data_asn == 0){
					$data_tmuk->asn = 1;
				}else{
					$data_tmuk->asn = 2;
				}

				if($cek){
					$success = $data_tmuk::goUpdate($data_tmuk);
					$data['type'] = 'S';
					$data['message'] = 'TMUK Sudah Aktif !!!';
					return json_encode($data);
				}else{
					$success = $data_tmuk::goInsert($data_tmuk);
					$data['type'] = 'S';
					$data['message'] = 'Berhasil Aktivasi TMUK !!!';
					return json_encode($data);
				}
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Gagal Ambil Data TMUK !!!';
				return json_encode($data);
			}	
		}else{
			$data['type'] = 'W';
			$data['message'] = 'Mohon Inputkan Kode TMUK !!!';
			return json_encode($data);
		}
	}

	// public function goSyncKustomerAction(){				
	// 	$success = '';		
	// 	try{
	// 		$cek = (new KustomerController())->getKustomer();
	// 	}catch(Exception $e){
	// 		$data['type'] = 'E';
	// 		$data['message'] = 'Error, '.$e;			
	// 		return json_encode($data);
	// 	}	
		
		// if($success == ''){
		// 	$data['type'] = 'S';
		// 	$data['message'] = 'Sync Kustomer Berhasil';			
		// 	$notif = new Notifikasi();
		// 	$notif->data = strip_tags($data['message']);
		// 	$notif->read_at = date('Y-m-d H:i:s');
		// 	$notif->created_at = date('Y-m-d H:i:s');
		// 	$notif->created_by =$this->session->get('user')['real_name'];

		// 	$success = $notif::goInsert($notif);
		// 	return json_encode($data);
		// }else{
		// 	$data['type'] = 'E';
		// 	$data['message'] = 'Error, '.$success;	
		// 	$notif = new Notifikasi();
		// 	$notif->data = strip_tags($data['message']);
		// 	$notif->read_at = date('Y-m-d H:i:s');
		// 	$notif->created_at = date('Y-m-d H:i:s');
		// 	$notif->created_by =$this->session->get('user')['real_name'];	

		// 	$success = $notif::goInsert($notif);
		// 	return json_encode($data);
		// }		
	// }

	public function syncMemberAction(){
		$data = new Member();
		$lists = $data->getAll();

		
		if(count($lists) > 0){
			foreach($lists as $list){
				$debtor_no = $list['debtor_no'];
				$br_name = $list['br_name'];
				$telepon = $list['telepon'];
				$email = $list['email'];
				$branch_ref = $list['branch_ref'];
				$contact_name = $list['contact_name'];
				$br_address = $list['br_address'];
				$notes = $list['notes'];
				$credit_limit = $list['credit_limit'];
				$nomor_member = $list['nomor_member'];
				$default_location = $list['default_location'];
				$tax_group_id = $list['tax_group_id'];
				$br_post_address = $list['br_post_address'];
				$inactive = $list['inactive'];
				try {
					$curl = new CurlClass();
					$result = $curl->post($this->di['api']['link']."/api/member/member-tmuk-sync", [
						'debtor_no' => $debtor_no,
						'br_name' => $br_name,
						'telepon' => $telepon,
						'email' => $email,
						'branch_ref' => $branch_ref,
						'notes' => $notes,
						'nomor_member' => $nomor_member,
						'alamat' => $br_address,
						'credit_limit' => $credit_limit,
						'contact_name' => $contact_name,
						'default_location' => $default_location,
						'tax_group_id' => $tax_group_id,
						'br_post_address' => $br_post_address,
						'inactive' => $inactive,
					]);
					$obj = json_decode($result, true);
				} catch (Exception $e) {
					
				}

				// try {
				// 	$curl = new CurlClass();
				// 	$result = $curl->post($this->di['api']['link']."/api/member/send", [
				// 		'tmuk_kode' => $debtor_no,
				// 		'nama' => $br_name,
				// 		'telepon' => $telepon,
				// 		'email' => $email,
				// 		'jeniskustomer_id' => $branch_ref,
				// 		'notes' => $notes,
				// 		'nomor' => $nomor_member,
				// 		'alamat' => $br_address,
				// 		'limit_kredit' => $credit_limit,
				// 	]);
				// 	$obj = json_decode($result, true);
				// } catch (Exception $e) {
					
				// }
			}
		}

		// $curl = new CurlClass();
		// $result = $curl->post($this->di['api']['link']."/api/member/get-all");
		// $obj = json_decode($result, true);
		// if ($obj['status'] == 'success') {
		// 	$member = $obj['message'];
		// 	foreach ($member as $key => $value) {
		// 		// return $value['id'];
		// 		try {
		// 			$o_member = new Member();		
		
		// 			$o_member->debtor_no = $value['tmuk_kode'];
		// 			$o_member->br_name = $value['nama'];
		// 			$o_member->branch_ref = $value['jeniskustomer_id'];
		// 			$o_member->br_address = $value['alamat'];
		// 			$o_member->contact_name = '';
		// 			$o_member->default_location = '';		
		// 			//$o_member->br_post_address = $this->request->getPost("Post_Address");
		// 			$o_member->notes = $value['notes'];
		// 			$o_member->nomor_member = $value['nomor'];		
		// 			$o_member->credit_limit = $value['limit_kredit'];
		// 			$o_member->telepon = $value['telepon'];
		// 			$o_member->email = $value['email'];

		// 			$success = $o_member::goInsert($o_member);

		// 		} catch (Exception $e) {
					
		// 		}
		// 	}
		// }

		return '';						
	}
}
