<?php

use Phalcon\Mvc\Controller;

class GRController extends Controller
{
	public function AuthorityAction(){
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'GR' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction(){
		$this->AuthorityAction();
		
		$po = new PO();
		$condition = " WHERE \"gr_no_immpos\" IS NULL OR \"gr_no_immpos\" = '' ";
		$this->view->lists = $po::getFreeSQL($condition);
		
		$retur = new Retur();
		$condition_retur = " WHERE ( \"gr_retur_immpos\" IS NULL OR \"gr_retur_immpos\" = '' ) AND \"flag_delete\" = false AND \"flag_sync\" = true ";
		$this->view->lists_retur = $retur::getFreeSQL($condition_retur);
		
		$gr = new GR();
		$condition_gr = " WHERE \"flag_sync\" = false AND \"flag_delete\" = false ";
		$condition_gr2 = " WHERE \"flag_sync\" = true AND \"flag_delete\" = false ";
		$this->view->lists_sync = $gr::getFreeSQL($condition_gr);
		$this->view->listsHistory = $gr::getFreeSQL2($condition_gr2);
	}
	
	public function addAction(){		
		$this->AuthorityAction();
		
		$pr = new PR();
		$pr->request_no = $_REQUEST['PR_NO_Immpos'];
		$lists_pr = $pr::getFirst($pr);

		$this->view->data_pr = new PR();
		if(count($lists_pr) > 0){
			foreach($lists_pr as $list_pr){
				$this->view->data_pr->request_no = $list_pr['request_no'];
				$this->view->data_pr->supplier_id = $list_pr['supplier_id'];
				$this->view->data_pr->point_used = $list_pr['point_used'];
				$this->view->data_pr->delivery_by = $list_pr['delivery_by'];
				$this->view->data_pr->created_by = $list_pr['created_by'];
				$this->view->data_pr->comments = $list_pr['comments'];
				$this->view->data_pr->create_date = $list_pr['create_date'];
				$this->view->data_pr->delivery_date = $list_pr['delivery_date'];
				$this->view->data_pr->reference = $list_pr['reference'];
				$this->view->data_pr->requisition_no = $list_pr['requisition_no'];
				$this->view->data_pr->delivery_address = $list_pr['delivery_address'];
				$this->view->data_pr->total = $list_pr['total'];
				$this->view->data_pr->tax_included = $list_pr['tax_included'];
				$this->view->data_pr->tmuk = $list_pr['tmuk'];
				$this->view->data_pr->po_no_immbo = $list_pr['po_no_immbo'];
				$this->view->data_pr->flag_sync = $list_pr['flag_sync'];
			}
		}
		
		$po = new PO();
		$po->po_no_immbo = $_REQUEST['PO_NO_immbo'];
		$lists_po = $po::getFirst($po);
		
		$this->view->data_po = new PO();
		if(count($lists_po) > 0){
			foreach($lists_po as $list_po){
				$this->view->data_po->po_no_immbo = $list_po['po_no_immbo'];
				$this->view->data_po->po_no_ref = $list_po['po_no_ref'];
				$this->view->data_po->reference = $list_po['reference'];
				$this->view->data_po->supplier_id = $list_po['supplier_id'];
				$this->view->data_po->order_date = $list_po['order_date'];
				$this->view->data_po->delivery_date = $list_po['delivery_date'];
				$this->view->data_po->delivery_address = $list_po['delivery_address'];
				$this->view->data_po->comments = $list_po['comments'];
				$this->view->data_po->pr_no_immpos = $list_po['pr_no_immpos'];
				$this->view->data_po->pr_no_immbo = $list_po['pr_no_immbo'];
				$this->view->data_po->gr_no_immpos = $list_po['gr_no_immpos'];
				$this->view->data_po->total_pr = $list_po['total_pr'];
				$this->view->data_po->total_po = $list_po['total_po'];
				$this->view->data_po->flag_sync = $list_po['flag_sync'];
			}
		}
		
		$po_detail = new PODetail();
		$po_detail->po_no_immbo = $_REQUEST['PO_NO_immbo'];
		$condition = " WHERE \"po_no_immbo\" ='".$po_detail->po_no_immbo."' ";
		$lists_po_detail = $po_detail::getFreeSQL($condition);
		// // dd($lists_po_detail);
		// $pr_detail = new PRDetail();
		// $pr_detail->request_no = $_REQUEST['PR_NO_Immpos'];
		// $condition = " WHERE \"request_no\" ='".$pr_detail->request_no."' ";
		// $lists_pr_detail = $pr_detail::getFreeSQL($condition);
		for($i=0; $i < count($lists_po_detail); $i++ ){
			$lists_po_detail[$i]['item_code'];
			$produk = new Product();
			$condition = " WHERE \"stock_id\" = '".$lists_po_detail[$i]['item_code']."' ";
			$data_product = $produk::getFreeSQL($condition);
			foreach ($data_product as $key => $value) {
				$qty_po = $value['uom2_conversion'] * $lists_po_detail[$i]['qty_po'];
				$lists_po_detail[$i]['level_service'] = '0';
				$lists_po_detail[$i]['level_service'] = number_format( $qty_po / $lists_po_detail[$i]['qty_pr'] * 100 , 2 );
			}
		}
		
		$this->view->lists_po_detail = $lists_po_detail;
	}
	
	public function addReturAction(){		
		$this->AuthorityAction();
		
		$retur = new Retur();
		$retur->retur_no = $_REQUEST['Retur_No'];
		$lists_retur = $retur::getFirst($retur);
		
		$this->view->data_retur = new Retur();
		if(count($lists_retur) > 0){
			foreach($lists_retur as $list_retur){
				$this->view->data_retur->retur_no = $list_retur['retur_no'];
				$this->view->data_retur->retur_type = $list_retur['retur_type'];
				$this->view->data_retur->reference = $list_retur['reference'];
				$this->view->data_retur->supplier_id = $list_retur['supplier_id'];
				$this->view->data_retur->create_date = $list_retur['create_date'];
				$this->view->data_retur->delivery_date = $list_retur['delivery_date'];
				$this->view->data_retur->delivery_address = $list_retur['delivery_address'];
				$this->view->data_retur->comments = $list_retur['comments'];
				$this->view->data_retur->gr_no_immpos = $list_retur['gr_no_immpos'];
				$this->view->data_retur->retur_no_immbo = $list_retur['retur_no_immbo'];
				$this->view->data_retur->total_gr = $list_retur['total_gr'];
				$this->view->data_retur->total_retur = $list_retur['total_retur'];
				$this->view->data_retur->tax_included = $list_retur['tax_included'];
				$this->view->data_retur->flag_sync = $list_retur['flag_sync'];
			}
		}
		
		$retur_detail = new ReturDetail();
		$retur_detail->retur_no = $_REQUEST['Retur_No'];
		$note = 'Barang tidak sesuai (ditukar)';
		$condition = " WHERE \"retur_no\" ='".$retur_detail->retur_no."' AND \"note\" ='".$note."' ";
		$lists_retur_detail = $retur_detail::getFreeSQL($condition);
		
		$this->view->lists_retur_detail= $lists_retur_detail;
	}
	
	public function ajaxPOAction(){
		$data = array();	
		$condition = " WHERE po.po_no_immbo = '".$_GET['order']."' ";
		$data = PO::getJoin($condition);

		$count = 0;
		for($i=0; $i<count($data); $i++){
			$data[$i]['qty_pr'];
			$data[$i]['unit_po'];
		}
		
		return json_encode($data);
	}
	
	public function ajaxPRAction(){
		$pr_detail = new PRDetail();
		$condition = " WHERE \"request_no\" = '".$_GET['request']."' ";
		$data = $pr_detail::getFreeSQL($condition);
		
		return json_encode($data);
	}
	
	public function ajaxGRAction(){
		$gr = new GR();
		$gr->gr_no = $_GET['gr'];
		$lists_gr = $gr::getFirst($gr);
		
		if(count($lists_gr) > 0){
			foreach($lists_gr as $list_gr){
				$gr->gr_no = $list_gr['gr_no'];
				$gr->create_by = $list_gr['create_by'];
				$gr->create_date = $list_gr['create_date'];
				$gr->delivery_date = $list_gr['delivery_date'];
				$gr->comments = $list_gr['comments'];
				$gr->pr_no_immpos = $list_gr['pr_no_immpos'];
				$gr->pr_no_immbo = $list_gr['pr_no_immbo'];
				$gr->po_no_immbo = $list_gr['po_no_immbo'];
				$gr->retur_no_immpos = $list_gr['retur_no_immpos'];
				$gr->retur_no_immbo = $list_gr['retur_no_immbo'];
				$gr->total_pr = $list_gr['total_pr'];
				$gr->total_po = $list_gr['total_po'];
				$gr->retur_doc_immpos = $list_gr['retur_doc_immpos'];
				$gr->retur_doc_immbo = $list_gr['retur_doc_immbo'];
				$gr->total_retur = $list_gr['total_retur'];
			}			
		}
		
		$data = null;
		
		// GR PO/PR
		if($gr->po_no_immbo != '' && $gr->po_no_immbo != null){
			// $po_detail = new PODetail();
			// $condition = " WHERE \"po_no_immbo\" = '".$gr->po_no_immbo."' ";
			// $data = $po_detail::getFreeSQL($condition);

			$data = array();	
			$condition = " WHERE po.po_no_immbo = '".$gr->po_no_immbo."' ";
			$data = PO::getJoin($condition);
		}
		
		if($gr->pr_no_immpos != '' && $gr->pr_no_immpos != null){
			$pr_detail = new PRDetail();
			$condition = " WHERE \"request_no\" = '".$gr->pr_no_immpos."' ";
			$data_pr = $pr_detail::getFreeSQL($condition);
		}
		
		for($i=0; $i<count($data); $i++){
			$data[$i]['qty_pr'];
			$data[$i]['unit_pr'];
			$data[$i]['unit_po'];
			$data[$i]['qty_retur'] = '0';
			$data[$i]['unit_retur'] = '';
		}
		
		// GR Retur
		if($gr->retur_no_immpos != '' && $gr->retur_no_immpos != null){
			$retur_detail = new ReturDetail();
			$condition = " WHERE \"retur_no\" = '".$gr->retur_no_immpos."' ";
			$data = $retur_detail::getFreeSQL($condition);
			
			for($i=0; $i<count($data); $i++){
				$data[$i]['qty_pr'] = '0';
				$data[$i]['unit_pr'] = '';
				$data[$i]['qty_po'] = '0';
				$data[$i]['unit_po'] = '';
				$data[$i]['po_unit_price'] = $data[$i]['retur_unit_price'];
				$data[$i]['po_price'] = '0';
			}	
		}				
		
		return json_encode($data);
	}
	
	public function ajaxReturAction(){
		$retur_detail = new ReturDetail();
		$condition = " WHERE \"retur_no\" = '".$_GET['retur']."' ";
		$data = $retur_detail::getFreeSQL($condition);
		
		return json_encode($data);
	}
	
	public function insertAction(){
		$number_range = new NumberRange();
		$number_range->trans_type = 'GR';
		$number_range->period = date('Ym');
		$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
		$lists = $number_range::getFreeSQL($condition);
		
		if(isset($lists)){
			foreach($lists as $list){
				$number_range->trans_type = $list['trans_type'];
				$number_range->period = $list['period'];
				$number_range->prefix = $list['prefix'];
				$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
			}
		}else{
			$number_range->trans_type = 'GR';
			$number_range->period = date('Ym');
			$number_range->prefix = 'GR';
			$number_range->from_ = '1';
			$number_range->to_ = '9999';
			$number_range->current_no = '1';
			$success = $number_range::goInsert($number_range);			
			$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
		}
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
			}
		}
		
		
		$gr = new GR();
		$condition = " WHERE \"pr_no_immpos\" = '".$this->request->getPost("pr_no_immpos")."' ";
		$cek = $gr::getFreeSQL($condition);
		
		if($cek){
			$gr->gr_no = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;	
			$gr->create_by = $this->request->getPost("create_by");
			$gr->delivery_date = $this->request->getPost("delivery_date");		
			$gr->comments = $this->request->getPost("comments");	
			$gr->pr_no_immpos = $this->request->getPost("pr_no_immpos");
			$gr->pr_no_immbo = $this->request->getPost("pr_no_immbo");
			$gr->po_no_immbo = $this->request->getPost("po_no_immbo");
			$gr->total_pr = $this->request->getPost("total_pr");
			$gr->total_po = $this->request->getPost("total_po");
			$success = $gr::goUpdate($gr);
		}else{
			$gr->gr_no = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;	
			$gr->create_by = $this->request->getPost("create_by");
			$gr->delivery_date = $this->request->getPost("delivery_date");		
			$gr->comments = $this->request->getPost("comments");	
			$gr->pr_no_immpos = $this->request->getPost("pr_no_immpos");
			$gr->pr_no_immbo = $this->request->getPost("pr_no_immbo");
			$gr->po_no_immbo = $this->request->getPost("po_no_immbo");
			$gr->total_pr = $this->request->getPost("total_pr");
			$gr->total_po = $this->request->getPost("total_po");
			$success = $gr::goInsert($gr);
		}
		
		if($success){			
			$success_num = $number_range::goAddNumber($number_range);
			
			$po = new PO();
			$po->po_no_immbo = $gr->po_no_immbo;
			$po->gr_no_immpos = $gr->gr_no;
			$success = $po::goUpdate($po);			
			
			$json_datas = json_decode($_POST['json_data']);			
			foreach($json_datas as $json_data){
				$po_detail = new PODetail();
				$po_detail->po_no_immbo = $gr->po_no_immbo;
				$po_detail->po_detail_item = $json_data[0];
				$po_detail->flag_gr_ok = $json_data[5];
				$success = $po_detail::goUpdate($po_detail);
			}
			
			$data['type'] = 'S';
			$data['message'] = 'GR berhasil Dibuat!<br/> Nomor GR : <b>'.$gr->gr_no.'</b>';

			$notif = new Notifikasi();
			$notif->data = $data['message'];
			$notif->read_at = null;
			$notif->created_at = date('Y-m-d H:i:s');
			$notif->created_by = $this->session->get("user")['user_name'];

			$sukses = $notif::goInsert($notif);
			return json_encode($data);
		}else{						
			$data['type'] = 'E';
			$data['message'] = 'Gagal membuat GR!';
			return json_encode($data);
		}
	}
	
	public function insertReturAction(){
		$number_range = new NumberRange();
		$number_range->trans_type = 'RR';
		$number_range->period = date('Ym');
		$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
		$lists = $number_range::getFreeSQL($condition);
		
		if(isset($lists)){
			foreach($lists as $list){
				$number_range->trans_type = $list['trans_type'];
				$number_range->period = $list['period'];
				$number_range->prefix = $list['prefix'];
				$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
			}
		}else{
			$number_range->trans_type = 'RR';
			$number_range->period = date('Ym');
			$number_range->prefix = 'RR';
			$number_range->from_ = '1';
			$number_range->to_ = '9999';
			$number_range->current_no = '1';
			$success = $number_range::goInsert($number_range);			
			$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
		}
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
			}
		}
		
		$gr = new GR();
		$gr->gr_no = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;		
		$gr->create_by = $this->request->getPost("create_by");
		$gr->delivery_date = $this->request->getPost("delivery_date");		
		$gr->comments = $this->request->getPost("comments");	
		$gr->retur_no_immpos = $this->request->getPost("retur_no");
		$gr->retur_no_immbo = $this->request->getPost("retur_no_immbo");						
		$success = $gr::goUpdate($gr);
		
		if($success){
			$number_range->trans_type = 'GR';
			$success_num = $number_range::goAddNumber($number_range);
			
			$retur = new Retur();
			$retur->retur_no = $gr->retur_no_immpos;
			$retur->gr_retur_immpos = $gr->gr_no;			
			$success = $retur::goUpdate($retur);			
			
			$json_datas = json_decode($_POST['json_data']);	
			foreach($json_datas as $json_data){
				$retur_detail = new ReturDetail();
				$retur_detail->retur_no = $gr->retur_no_immpos;
				$retur_detail->retur_detail_item = $json_data[0];
				$retur_detail->flag_gr_ok = $json_data[4];
				$success = $retur_detail::goUpdate($retur_detail);

				$cari_gr = new GR();
				$condition = " WHERE \"gr_no\" = '".$this->request->getPost("no_gr")."' ";
				$cek = $cari_gr::getFreeSQL($condition);

				if(isset($cek)){
					foreach($cek as $data){
						$detail = $data['po_no_immbo'];
					}
				}

				$POD = new PODetail();
				$condition = " WHERE \"po_no_immbo\" = '".$detail."' ";
				$lists_po_detail = $POD::getFreeSQL($condition);

				$count = 0;
				$rg_stock_id = '';
				foreach($lists_po_detail as $list_po_detail){
					if($count>0){ $rg_stock_id .= ','; }
					$rg_stock_id .= "'".$list_po_detail['item_code']."'";

					$qty_po = $list_po_detail['qty_po'];
					$count++;
				}

				if(isset($rg_stock_id)){
					$product = new Product();
					$condition = " WHERE \"stock_id\" IN (".$rg_stock_id.") ";
					$lists_product = $product::getFreeSQL($condition);
				}

				$stock_qty = 0;
				$order_qty = $qty_po;
				$stock_con = 1;
				$order_con = 1;

				foreach($lists_product as $list_product){
					if($list_product['stock_id'] == $list_po_detail['item_code']){
						$stock_qty = ( $order_qty * $order_con ) / $stock_con; 
					}
				}

				$prod = new Product();
				$condition = " WHERE \"stock_id\" = '".$json_data[1]."' ";
				$data_prod= $prod::getFreeSQL($condition);

				$product_move = new ProductMove();		
				$product_move->trans_no = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;
				$product_move->stock_id = $json_data[1];
				$product_move->type = 'RR';
				$product_move->reference = $gr->gr_no;
				foreach ($data_prod as $key => $value) {
					$qty = $stock_qty * $value['uom2_conversion'];
					$product_move->qty = $stock_qty * $value['uom2_conversion'];
				}
				$product_move->price = $list_po_detail['po_unit_price'];
				$moveQuery .= $product_move::insertSql($product_move);
				// $success = $product_move::goInsert($product_move);
				
				$product_move->stock_id = $list_po_detail['item_code'];
				$cek_stock = $product_move::getStock($product_move);
				// $list_gr['delivery_date'] = '2018-07-01';
				$stok = new Stock();
				$condition = " WHERE \"tgl_gr\" = '".$gr->delivery_date."' AND \"stock_id\" = '".$json_data[1]."' ";
				$cek = $stok::getFreeSQL($condition);
				if($cek){
					$stok->tgl_gr = $gr->delivery_date;
					$stok->no_gr = $gr->gr_no;
					$stok->stock_id = $json_data[1];
					$stok->qty = $qty + $cek['0']['qty'];
					$stok->sisa = $qty + $cek['0']['sisa'];
					$stok->total = $cek_stock;
					$success = $stok::goUpdate($stok);
				}else{
					$stok->tgl_gr = $gr->delivery_date;
					$stok->no_gr = $gr->gr_no;
					$stok->stock_id = $json_data[1];
					$stok->qty = $qty;
					$stok->sisa = $qty;
					$stok->total = $cek_stock;
					$success = $stok::goInsert($stok);

				}

				$price = new ProductPrice();
				$condition = " WHERE \"stock_id\" = '".$json_data[1]."' ";
				$cek_price = $price::getFreeSQL($condition);

				// if($cek_price){
				$price->stock_id = $json_data[1];
				$lists_price = $price::getFirst($price);
				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/hpp/get-hpp", [
					'kode_produk' => $json_data[1],
					'kode_tmuk' => $tmuk->tmuk
				]);
				$cek_hpp = json_decode($result, true);

				$map = 0;
				$margin = 0;
				$suggest = 0;
				$changed = 0;

				if($cek_hpp['message']['map'] > 0){
					$map = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['map']));
				}
				if($cek_hpp['message']['margin_amount'] > 0){
					$margin = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['margin_amount']));
				}
				if($cek_hpp['message']['suggest_price'] > 0){
					$suggest = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['suggest_price']));
				}
				if($cek_hpp['message']['change_price'] > 0){
					$changed = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['change_price']));
				}

				$abrev = 'IDR';
				$price = new ProductPrice();
				$price->stock_id = $json_data[1];
				$price->sales_type_id = 1;
				$price->curr_abrev = $abrev;

				$harga = new CurlClass();
				$data_harga = $harga->post($this->di['api']['link']."/api/produk/get-price", [
					'tmuk' => $tmuk->tmuk,
					'kode_produk' => $json_data[1]
				]);
				$set_harga = json_decode($data_harga, true);
				$setting = new CurlClass();
				$data_setting = $setting->post($this->di['api']['link']."/api/produk/get-setting", [
					'kode_produk' => $json_data[1]
				]);

				$price->price = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($set_harga['message']));

				$set = json_decode($data_setting, true);
				foreach ($set['message'] as $val) {
					$price->margin = $val['margin'];
					$sell = $map*(1+($val['margin']/100));
				}

				$price->average_cost = $map;
				$price->uom1_suggested_price = round($sell);
				$price->uom1_rounding_price = round(round($sell)/100) * 100;
				$price->uom1_changed_price = $changed;
				$price->uom1_margin_amount = $margin;
				$price->uom2_suggested_price = $suggest;
				$price->uom2_rounding_price = 0;
				$price->uom2_changed_price = 0;
				$price->uom2_margin_amount = 0;
				$price->uom3_suggested_price = 0;
				$price->uom3_rounding_price = 0;
				$price->uom3_changed_price = 0;
				$price->uom3_margin_amount = 0;
				$price->uom1_cost_price = 0;
				$price->uom2_cost_price = 0;
				$price->uom3_cost_price = 0;
				$price->uom4_suggested_price = 0;
				$price->uom4_rounding_price = 0;
				$price->uom4_changed_price = 0;
				$price->uom4_margin_amount = 0;
				$price->uom4_cost_price = 0;
				$price->uom1_member_price = 0;
				$price->uom2_member_price = 0;
				$price->uom3_member_price = 0;
				$price->uom4_member_price = 0;
				if($cek_price){
					$priceQuery .= $price::updateSql($price);
				}else{
					$priceQuery .= $price::insertSql($price);
				}
				// $lists_price = $price::goUpdate($price);
				// }else{
				// 	$price->stock_id = $json_data[1];
				// 	$lists_price = $price::getFirst($price);
				// 	$curl = new CurlClass();
				// 	$result = $curl->post($this->di['api']['link']."/api/hpp/get-hpp", [
				// 		'kode_produk' => $json_data[1],
				// 		'kode_tmuk' => $tmuk->tmuk
				// 	]);
				// 	$cek_hpp = json_decode($result, true);

				// 	$map = 0;
				// 	$margin = 0;
				// 	$suggest = 0;
				// 	$changed = 0;

				// 	if($cek_hpp['message']['map'] > 0){
				// 		$map = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['map']));
				// 	}
				// 	if($cek_hpp['message']['margin_amount'] > 0){
				// 		$margin = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['margin_amount']));
				// 	}
				// 	if($cek_hpp['message']['suggest_price'] > 0){
				// 		$suggest = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['suggest_price']));
				// 	}
				// 	if($cek_hpp['message']['change_price'] > 0){
				// 		$changed = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['change_price']));
				// 	}

				// 	$abrev = 'IDR';
				// 	$price = new ProductPrice();
				// 	$price->stock_id = $json_data[1];
				// 	$price->sales_type_id = 1;
				// 	$price->curr_abrev = $abrev;

				// 	$harga = new CurlClass();
				// 	$data_harga = $harga->post($this->di['api']['link']."/api/produk/get-price", [
				// 		'tmuk' => $tmuk->tmuk,
				// 		'kode_produk' => $json_data[1]
				// 	]);
				// 	$set_harga = json_decode($data_harga, true);
				// 	$setting = new CurlClass();
				// 	$data_setting = $setting->post($this->di['api']['link']."/api/produk/get-setting", [
				// 		'kode_produk' => $json_data[1]
				// 	]);

				// 	$price->price = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($set_harga['message']));

				// 	$set = json_decode($data_setting, true);
				// 	foreach ($set['message'] as $val) {
				// 		$price->margin = $val['margin'];
				// 		$sell = $map*(1+($val['margin']/100));
				// 	}

				// 	$price->average_cost = $map;
				// 	$price->uom1_suggested_price = round($sell);
				// 	$price->uom1_rounding_price = round(round($sell)/100) * 100;
				// 	$price->uom1_changed_price = $changed;
				// 	$price->uom1_margin_amount = $margin;
				// 	$price->uom2_suggested_price = $suggest;
				// 	$price->uom2_rounding_price = 0;
				// 	$price->uom2_changed_price = 0;
				// 	$price->uom2_margin_amount = 0;
				// 	$price->uom3_suggested_price = 0;
				// 	$price->uom3_rounding_price = 0;
				// 	$price->uom3_changed_price = 0;
				// 	$price->uom3_margin_amount = 0;
				// 	$price->uom1_cost_price = 0;
				// 	$price->uom2_cost_price = 0;
				// 	$price->uom3_cost_price = 0;
				// 	$price->uom4_suggested_price = 0;
				// 	$price->uom4_rounding_price = 0;
				// 	$price->uom4_changed_price = 0;
				// 	$price->uom4_margin_amount = 0;
				// 	$price->uom4_cost_price = 0;
				// 	$price->uom1_member_price = 0;
				// 	$price->uom2_member_price = 0;
				// 	$price->uom3_member_price = 0;
				// 	$price->uom4_member_price = 0;
				// 	$lists_price = $price::goInsert($price);
				// }
			}

			if($moveQuery != ''){
				$success = $product_move::executeQuery($moveQuery);
			}

			if($priceQuery != ''){
				$success = $price::executeQuery($priceQuery);
			}
			
			$data['type'] = 'S';
			$data['message'] = '<b>'.$gr->gr_no.'</b> Berhasil dibuat.';

			$notif = new Notifikasi();
			$notif->data = $data['message'];
			$notif->read_at = null;
			$notif->created_at = date('Y-m-d H:i:s');
			$notif->created_by = $this->session->get("user")['user_name'];

			$sukses = $notif::goInsert($notif);
			return json_encode($data);
		}else{						
			$data['type'] = 'E';
			$data['message'] = 'Gagal membuat GR!';
			return json_encode($data);
		}		
	}
	
	public function editAction(){
		$this->AuthorityAction();
		
		$gr = new GR();
		$gr->gr_no = $_GET['GR_No'];
		$lists_gr = $gr::getFirst($gr);
		
		if(count($lists_gr) > 0){
			foreach($lists_gr as $list_gr){
				$gr->gr_no = $list_gr['gr_no'];
				$gr->create_by = $list_gr['create_by'];
				$gr->create_date = $list_gr['create_date'];
				$gr->delivery_date = $list_gr['delivery_date'];
				$gr->comments = $list_gr['comments'];
				$gr->pr_no_immpos = $list_gr['pr_no_immpos'];
				$gr->pr_no_immbo = $list_gr['pr_no_immbo'];
				$gr->po_no_immbo = $list_gr['po_no_immbo'];
				$gr->retur_no_immpos = $list_gr['retur_no_immpos'];
				$gr->retur_no_immbo = $list_gr['retur_no_immbo'];
				$gr->total_pr = $list_gr['total_pr'];
				$gr->total_po = $list_gr['total_po'];
				$gr->retur_doc_immpos = $list_gr['retur_doc_immpos'];
				$gr->retur_doc_immbo = $list_gr['retur_doc_immbo'];
				$gr->total_retur = $list_gr['total_retur'];
			}			
		}
		$this->view->data_gr = $gr;
		
		$pr = new PR();
		$pr->request_no = $gr->pr_no_immpos;
		$lists_pr = $pr::getFirst($pr);
		
		$this->view->data_pr = new PR();
		if(count($lists_pr) > 0){
			foreach($lists_pr as $list_pr){
				$this->view->data_pr->request_no = $list_pr['request_no'];
				$this->view->data_pr->supplier_id = $list_pr['supplier_id'];
				$this->view->data_pr->point_used = $list_pr['point_used'];
				$this->view->data_pr->delivery_by = $list_pr['delivery_by'];
				$this->view->data_pr->created_by = $list_pr['created_by'];
				$this->view->data_pr->comments = $list_pr['comments'];
				$this->view->data_pr->create_date = $list_pr['create_date'];
				$this->view->data_pr->delivery_date = $list_pr['delivery_date'];
				$this->view->data_pr->reference = $list_pr['reference'];
				$this->view->data_pr->requisition_no = $list_pr['requisition_no'];
				$this->view->data_pr->delivery_address = $list_pr['delivery_address'];
				$this->view->data_pr->total = $list_pr['total'];
				$this->view->data_pr->tax_included = $list_pr['tax_included'];
				$this->view->data_pr->tmuk = $list_pr['tmuk'];
				$this->view->data_pr->po_no_immbo = $list_pr['po_no_immbo'];
				$this->view->data_pr->flag_sync = $list_pr['flag_sync'];
			}
		}
		
		$po = new PO();
		$po->po_no_immbo = $gr->po_no_immbo;
		$lists_po = $po::getFirst($po);
		
		$this->view->data_po = new PO();
		if(count($lists_po) > 0){
			foreach($lists_po as $list_po){
				$this->view->data_po->po_no_immbo = $list_po['po_no_immbo'];
				$this->view->data_po->po_no_ref = $list_po['po_no_ref'];
				$this->view->data_po->reference = $list_po['reference'];
				$this->view->data_po->supplier_id = $list_po['supplier_id'];
				$this->view->data_po->order_date = $list_po['order_date'];
				$this->view->data_po->delivery_date = $list_po['delivery_date'];
				$this->view->data_po->delivery_address = $list_po['delivery_address'];
				$this->view->data_po->comments = $list_po['comments'];
				$this->view->data_po->pr_no_immpos = $list_po['pr_no_immpos'];
				$this->view->data_po->pr_no_immbo = $list_po['pr_no_immbo'];
				$this->view->data_po->gr_no_immpos = $list_po['gr_no_immpos'];
				$this->view->data_po->total_pr = $list_po['total_pr'];
				$this->view->data_po->total_po = $list_po['total_po'];
				$this->view->data_po->flag_sync = $list_po['flag_sync'];
			}
		}
		
		$po_detail = new PODetail();
		$po_detail->po_no_immbo = $gr->po_no_immbo;
		$condition = " WHERE \"po_no_immbo\" ='".$po_detail->po_no_immbo."' ";
		$lists_po_detail = $po_detail::getFreeSQL($condition);

		for($i=0; $i < count($lists_po_detail); $i++ ){
			$lists_po_detail[$i]['item_code'];
			$produk = new Product();
			$condition = " WHERE \"stock_id\" = '".$lists_po_detail[$i]['item_code']."' ";
			$data_product = $produk::getFreeSQL($condition);
			foreach ($data_product as $key => $value) {
				$qty_po = $value['uom2_conversion'] * $lists_po_detail[$i]['qty_po'];
				$lists_po_detail[$i]['level_service'] = '0';
				$lists_po_detail[$i]['level_service'] = number_format( $qty_po / $lists_po_detail[$i]['qty_pr'] * 100 , 2 );
			}
		}
		
		$this->view->lists_po_detail = $lists_po_detail;
	}
	
	public function editReturAction(){
		$this->AuthorityAction();
		
		$gr = new GR();
		$gr->gr_no = $_GET['GR_No'];
		$lists_gr = $gr::getFirst($gr);
		
		if(count($lists_gr) > 0){
			foreach($lists_gr as $list_gr){
				$gr->gr_no = $list_gr['gr_no'];
				$gr->create_by = $list_gr['create_by'];
				$gr->create_date = $list_gr['create_date'];
				$gr->delivery_date = $list_gr['delivery_date'];
				$gr->comments = $list_gr['comments'];
				$gr->pr_no_immpos = $list_gr['pr_no_immpos'];
				$gr->pr_no_immbo = $list_gr['pr_no_immbo'];
				$gr->po_no_immbo = $list_gr['po_no_immbo'];
				$gr->retur_no_immpos = $list_gr['retur_no_immpos'];
				$gr->retur_no_immbo = $list_gr['retur_no_immbo'];
				$gr->total_pr = $list_gr['total_pr'];
				$gr->total_po = $list_gr['total_po'];
				$gr->retur_doc_immpos = $list_gr['retur_doc_immpos'];
				$gr->retur_doc_immbo = $list_gr['retur_doc_immbo'];
				$gr->total_retur = $list_gr['total_retur'];
			}			
		}
		$this->view->data_gr = $gr;
		
		$retur = new Retur();
		$retur->retur_no = $gr->retur_no_immpos;
		$lists_retur = $retur::getFirst($retur);
		
		$this->view->data_retur = new Retur();
		if(count($lists_retur) > 0){
			foreach($lists_retur as $list_retur){
				$this->view->data_retur->retur_no = $list_retur['retur_no'];
				$this->view->data_retur->retur_type = $list_retur['retur_type'];
				$this->view->data_retur->reference = $list_retur['reference'];
				$this->view->data_retur->supplier_id = $list_retur['supplier_id'];
				$this->view->data_retur->create_date = $list_retur['create_date'];
				$this->view->data_retur->delivery_date = $list_retur['delivery_date'];
				$this->view->data_retur->delivery_address = $list_retur['delivery_address'];
				$this->view->data_retur->comments = $list_retur['comments'];
				$this->view->data_retur->gr_no_immpos = $list_retur['gr_no_immpos'];
				$this->view->data_retur->retur_no_immbo = $list_retur['retur_no_immbo'];
				$this->view->data_retur->total_gr = $list_retur['total_gr'];
				$this->view->data_retur->total_retur = $list_retur['total_retur'];
				$this->view->data_retur->tax_included = $list_retur['tax_included'];
				$this->view->data_retur->flag_sync = $list_retur['flag_sync'];
			}
		}
		
		$retur_detail = new ReturDetail();
		$retur_detail->retur_no = $gr->retur_no_immpos;
		$condition = " WHERE \"retur_no\" ='".$retur_detail->retur_no."' ";
		$lists_retur_detail = $retur_detail::getFreeSQL($condition);
		
		$this->view->lists_retur_detail= $lists_retur_detail;
	}
	
	public function syncAction(){	
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = '<b>Mohon Periksa Kembali Koneksi Anda !!!</b>';

			return json_encode($data);
		}else{	
			$gr = new GR();
			$gr->gr_no = $_REQUEST['GR_No'];
			$lists_gr = $gr::getFirst($gr);		
			// dd($lists_gr);
			if(count($lists_gr) > 0){
				foreach($lists_gr as $list_gr){
					$gr->gr_no = $list_gr['gr_no'];
					$gr->create_by = $list_gr['create_by'];
					$gr->create_date = $list_gr['create_date'];
					$gr->delivery_date = $list_gr['delivery_date'];
					$gr->comments = $list_gr['comments'];
					$gr->pr_no_immpos = $list_gr['pr_no_immpos'];
					$gr->pr_no_immbo = $list_gr['pr_no_immbo'];
					$gr->po_no_immbo = $list_gr['po_no_immbo'];
					$gr->retur_no_immpos = $list_gr['retur_no_immpos'];
					$gr->retur_no_immbo = $list_gr['retur_no_immbo'];
					$gr->total_pr = $list_gr['total_pr'];
					$gr->total_po = $list_gr['total_po'];
					$gr->retur_doc_immpos = $list_gr['retur_doc_immpos'];
					$gr->retur_doc_immbo = $list_gr['retur_doc_immbo'];
					$gr->total_retur = $list_gr['total_retur'];
				}			
			}
		
			// stock move for sync GR from PO
			if($gr->po_no_immbo != ''){
				$nomorpoimmbo = $gr->po_no_immbo;	
				$nomorpr = $gr->pr_no_immpos;	
				
				$success = true;
				
				if($success){
					try {
						$curl = new CurlClass();
						$result = $curl->post($this->di['api']['link']."/api/po/terima-po", [
							'po' => $gr->po_no_immbo,
						]);
						$obj = json_decode($result, true);
						// var_dump($obj);
						// die;
					} catch (Exception $e) {
						
					}
					$po_detail = new PODetail();
					$condition = " WHERE \"po_no_immbo\" = '".$gr->po_no_immbo."' ";
					$lists_po_detail = $po_detail::getFreeSQL($condition);
					
					$number_range = new NumberRange();
					$number_range->trans_type = 'MV';
					$number_range->period = date('Ym');
					$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
					$lists = $number_range::getFreeSQL($condition);
					
					if(isset($lists)){
						foreach($lists as $list){
							$number_range->trans_type = $list['trans_type'];
							$number_range->period = $list['period'];
							$number_range->prefix = $list['prefix'];
							$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
						}
					}else{
						$number_range->trans_type = 'MV';
						$number_range->period = date('Ym');
						$number_range->prefix = 'MV';
						$number_range->from_ = '1';
						$number_range->to_ = '9999';
						$number_range->current_no = '1';
						$success = $number_range::goInsert($number_range);			
						$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
					}
					
					$tmuk = new TMUK();
					$lists_tmuk = $tmuk::getALL();
					
					if(isset($lists_tmuk)){
						foreach($lists_tmuk as $list_tmuk){
							$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
							$tmuk->tmuk = $list_tmuk['tmuk'];
							$tmuk->supplier_id = $list_tmuk['supplier_id'];
						}
					}
					
					$count = 0;
					$rg_stock_id = '';
					foreach($lists_po_detail as $list_po_detail){
						if($count>0){ $rg_stock_id .= ','; }
						$rg_stock_id .= "'".$list_po_detail['item_code']."'";
						$count++;
					}				
					
					if(isset($rg_stock_id)){
						$product = new Product();
						$condition = " WHERE \"stock_id\" IN (".$rg_stock_id.") ";
						$lists_product = $product::getFreeSQL($condition);
					}
					
					foreach($lists_po_detail as $list_po_detail){
						if($list_po_detail['flag_gr_ok'] == 'true'){
							$stock_qty = 0;
							$order_qty = $list_po_detail['qty_po'];
							$stock_con = 1;
							$order_con = 1;
							
							$price = new ProductPrice();
							$condition = " WHERE \"stock_id\" = '".$list_po_detail['item_code']."' ";
							$cek_price = $price::getFreeSQL($condition);

							// if($cek_price){
							$price->stock_id = $list_po_detail['item_code'];
							$lists_price = $price::getFirst($price);
							$curl = new CurlClass();
							$result = $curl->post($this->di['api']['link']."/api/hpp/get-hpp", [
								'kode_produk' => $list_po_detail['item_code'],
								'kode_tmuk' => $tmuk->tmuk
							]);
							$cek_hpp = json_decode($result, true);

							$map = 0;
							$margin = 0;
							$suggest = 0;
							$changed = 0;

							if($cek_hpp['message']['map']){
								$map = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['map']));
							}
							if($cek_hpp['message']['margin_amount']){
								$margin = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['margin_amount']));
							}
							if($cek_hpp['message']['suggest_price']){
								$suggest = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['suggest_price']));
							}
							if($cek_hpp['message']['change_price']){
								$changed = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($cek_hpp['message']['change_price']));
							}

							$abrev = 'IDR';
							$price = new ProductPrice();
							$price->stock_id = $list_po_detail['item_code'];
							$price->sales_type_id = 1;
							$price->curr_abrev = $abrev;

							$harga = new CurlClass();
							$data_harga = $harga->post($this->di['api']['link']."/api/produk/get-price", [
								'tmuk' => $tmuk->tmuk,
								'kode_produk' => $list_po_detail['item_code']
							]);
							$set_harga = json_decode($data_harga, true);
							$setting = new CurlClass();
							$data_setting = $setting->post($this->di['api']['link']."/api/produk/get-setting", [
								'kode_produk' => $list_po_detail['item_code']
							]);

							$price->price = preg_replace('/[^A-Za-z0-9\-]/', '', number_format($set_harga['message']));

							$set = json_decode($data_setting, true);
							foreach ($set['message'] as $val) {
								$price->margin = $val['margin'];
								$sell = $map*(1+($val['margin']/100));
							}

							$price->average_cost = $map;
							$price->uom1_suggested_price = round($sell);
							$price->uom1_rounding_price = round(round($sell)/100) * 100;
							$price->uom1_changed_price = $changed;
							$price->uom1_margin_amount = $margin;
							$price->uom2_suggested_price = $suggest;
							$price->uom2_rounding_price = 0;
							$price->uom2_changed_price = 0;
							$price->uom2_margin_amount = 0;
							$price->uom3_suggested_price = 0;
							$price->uom3_rounding_price = 0;
							$price->uom3_changed_price = 0;
							$price->uom3_margin_amount = 0;
							$price->uom1_cost_price = 0;
							$price->uom2_cost_price = 0;
							$price->uom3_cost_price = 0;
							$price->uom4_suggested_price = 0;
							$price->uom4_rounding_price = 0;
							$price->uom4_changed_price = 0;
							$price->uom4_margin_amount = 0;
							$price->uom4_cost_price = 0;
							$price->uom1_member_price = 0;
							$price->uom2_member_price = 0;
							$price->uom3_member_price = 0;
							$price->uom4_member_price = 0;
							
							if($cek_price){
								$priceQuery .= $price::updateSql($price);
							}else{
								$priceQuery .= $price::insertSql($price);
							}

							foreach($lists_product as $list_product){
								if($list_product['stock_id'] == $list_po_detail['item_code']){
									$stock_con = $list_product['uom1_conversion'];
									
									$stock_qty = ( $order_qty * $order_con ) / $stock_con; 
								}
							}

							$produk = new Product();
							$condition = " WHERE \"stock_id\" = '".$list_po_detail['item_code']."' ";
							$data_product = $produk::getFreeSQL($condition);
							
							$product_move = new ProductMove();		
							$product_move->trans_no = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;
							$product_move->stock_id = $list_po_detail['item_code'];
							$product_move->type = 'GR';
							$product_move->reference = $gr->gr_no;
							foreach ($data_product as $key => $value) {
								$product_move->qty = $stock_qty * $value['uom2_conversion'];
							}
							$product_move->price = $list_po_detail['po_unit_price'];
							$product_move->standard_cost = $lists_price[0]['uom1_cost_price'];
							$moveQuery .= $product_move::insertSql($product_move);
							// $success = $product_move::goInsert($product_move);

							$product_move->stock_id = $list_po_detail['item_code'];
							$cek_stock = $product_move::getStock($product_move);

							// $list_gr['delivery_date'] = '2018-07-01';

							$stok = new Stock();
							$condition = " WHERE \"tgl_gr\" = '".$list_gr['delivery_date']."' AND \"stock_id\" = '".$list_po_detail['item_code']."' ";
							$cek = $stok::getFreeSQL($condition);
							if($cek){
								$stok->tgl_gr = $list_gr['delivery_date'];
								$stok->no_gr = $gr->gr_no;
								$stok->stock_id = $list_po_detail['item_code'];
								$stok->qty = $list_po_detail['qty_pr'] + $cek['0']['qty'];
								$stok->sisa = $list_po_detail['qty_pr'] + $cek['0']['sisa'];
								$stok->total = $cek_stock;
								$success = $stok::goUpdate($stok);
							}else{
								$stok->tgl_gr = $list_gr['delivery_date'];
								$stok->no_gr = $gr->gr_no;
								$stok->stock_id = $list_po_detail['item_code'];
								$stok->qty = $list_po_detail['qty_pr'];
								$stok->sisa = $list_po_detail['qty_pr'];
								$stok->total = $cek_stock;
								$success = $stok::goInsert($stok);

							}
							$stok->stock_id = $list_po_detail['item_code'];
							$stok->total = $cek_stock;
							$success = $stok::goUpdate3($stok);
						}
					}
					if($moveQuery != ''){
						$success = $product_move::executeQuery($moveQuery);
					}
					if($priceQuery != ''){
						$success = $price::executeQuery($priceQuery);
					}

					$success_num = $number_range::goAddNumber($number_range);
					
					$gr_update = new GR();
					$gr_update->gr_no = $gr->gr_no;		
					$gr_update->flag_sync = 't';
					$success = $gr_update::goUpdate($gr_update);
					
					$data['type'] = 'S';
					$data['message'] = 'Berhasil Sync <b>'.$gr->gr_no.'</b>.';

					$notif = new Notifikasi();
					$notif->data = $data['message'];
					$notif->read_at = null;
					$notif->created_at = date('Y-m-d H:i:s');
					$notif->created_by = $this->session->get("user")['user_name'];

					$sukses = $notif::goInsert($notif);
					return json_encode($data);
				}else{
					$data['type'] = 'E';
					$data['message'] = 'Gagal Sync <b>'.$gr->gr_no.'</b>.';

					$notif = new Notifikasi();
					$notif->data = $data['message'];
					$notif->read_at = null;
					$notif->created_at = date('Y-m-d H:i:s');
					$notif->created_by = $this->session->get("user")['user_name'];

					$sukses = $notif::goInsert($notif);
					return json_encode($data);
				}
			}else{	
			}
			
			$data['type'] = 'S';
			$data['message'] = 'Berhasil Sync <b>'.$gr->gr_no.'</b>';

			$notif = new Notifikasi();
			$notif->data = $data['message'];
			$notif->read_at = null;
			$notif->created_at = date('Y-m-d H:i:s');
			$notif->created_by = $this->session->get("user")['user_name'];

			$sukses = $notif::goInsert($notif);
			return json_encode($data);
		}
	}
	
	public function updateAction(){		
		$gr = new GR();
		$gr->gr_no = $this->request->getPost("gr_no");;
		$lists_gr = $gr::getFirst($gr);
		
		if(isset($lists_gr)){
			foreach($lists_gr as $list_gr){
				$gr->gr_no = $list_gr['gr_no'];
				$gr->create_by = $list_gr['create_by'];
				$gr->create_date = $list_gr['create_date'];
				$gr->delivery_date = $list_gr['delivery_date'];
				$gr->comments = $list_gr['comments'];
				$gr->pr_no_immpos = $list_gr['pr_no_immpos'];
				$gr->pr_no_immbo = $list_gr['pr_no_immbo'];
				$gr->po_no_immbo = $list_gr['po_no_immbo'];
				$gr->retur_no_immpos = $list_gr['retur_no_immpos'];
				$gr->retur_no_immbo = $list_gr['retur_no_immbo'];
				$gr->total_pr = $list_gr['total_pr'];
				$gr->total_po = $list_gr['total_po'];
				$gr->retur_doc_immpos = $list_gr['retur_doc_immpos'];
				$gr->retur_doc_immbo = $list_gr['retur_doc_immbo'];
				$gr->total_retur = $list_gr['total_retur'];
			}			
		}
	
		$json_datas = json_decode($_POST['json_data']);	

		if(isset($json_datas)){
			foreach($json_datas as $json_data){
				$po_detail = new PODetail();
				$po_detail->po_no_immbo = $gr->po_no_immbo;
				$po_detail->po_detail_item = $json_data[0];
				$po_detail->flag_gr_ok = $json_data[5];
				$success = $po_detail::goUpdate($po_detail);
			}
		}

		if($success){
			$data['type'] = 'S';
			$data['message'] = '<b>'.$gr->gr_no.'</b> Berhasil diubah.';
			return json_encode($data);
		}else{						
			$data['type'] = 'E';
			$data['message'] = '<b>'.$gr->gr_no.'</b> Gagal diubah.';
			return json_encode($data);
		}		
	}
	
	public function updateReturAction(){		
		$gr = new GR();
		$gr->gr_no = $this->request->getPost("gr_no");;
		$lists_gr = $gr::getFirst($gr);
		
		if(count($lists_gr) > 0){
			foreach($lists_gr as $list_gr){
				$gr->gr_no = $list_gr['gr_no'];
				$gr->create_by = $list_gr['create_by'];
				$gr->create_date = $list_gr['create_date'];
				$gr->delivery_date = $list_gr['delivery_date'];
				$gr->comments = $list_gr['comments'];
				$gr->pr_no_immpos = $list_gr['pr_no_immpos'];
				$gr->pr_no_immbo = $list_gr['pr_no_immbo'];
				$gr->po_no_immbo = $list_gr['po_no_immbo'];
				$gr->retur_no_immpos = $list_gr['retur_no_immpos'];
				$gr->retur_no_immbo = $list_gr['retur_no_immbo'];
				$gr->total_pr = $list_gr['total_pr'];
				$gr->total_po = $list_gr['total_po'];
				$gr->retur_doc_immpos = $list_gr['retur_doc_immpos'];
				$gr->retur_doc_immbo = $list_gr['retur_doc_immbo'];
				$gr->total_retur = $list_gr['total_retur'];
			}			
		}
	
		$json_datas = json_decode($_POST['json_data']);			
		foreach($json_datas as $json_data){
			$retur_detail = new ReturDetail();
			$retur_detail->retur_no = $gr->retur_no_immpos;
			$retur_detail->retur_detail_item = $json_data[0];
			$retur_detail->flag_gr_ok = $json_data[4];
			$success = $retur_detail::goUpdate($retur_detail);
		}
		
		if($success){
			$data['type'] = 'S';
			$data['message'] = '<b>'.$gr->gr_no.'</b> Berhasil diubah.';

			$notif = new Notifikasi();
			$notif->data = $data['message'];
			$notif->read_at = null;
			$notif->created_at = date('Y-m-d H:i:s');
			$notif->created_by = $this->session->get("user")['user_name'];

			$sukses = $notif::goInsert($notif);
			return json_encode($data);
		}else{
			$data['type'] = 'E';
			$data['message'] = '<b>'.$gr->gr_no.'</b> Gagal diubah.';
			
			$notif = new Notifikasi();
			$notif->data = $data['message'];
			$notif->read_at = null;
			$notif->created_at = date('Y-m-d H:i:s');
			$notif->created_by = $this->session->get("user")['user_name'];

			$sukses = $notif::goInsert($notif);
			return json_encode($data);
		}
	}
	
	public function deleteAction(){		
		$this->AuthorityAction();
		
		$gr = new GR();
		$gr->gr_no = $_GET['GR_No'];
		$lists_gr = $gr::getFirst($gr);
		
		if(count($lists_gr) > 0){
			foreach($lists_gr as $list_gr){
				$gr->gr_no = $list_gr['gr_no'];
				$gr->create_by = $list_gr['create_by'];
				$gr->create_date = $list_gr['create_date'];
				$gr->delivery_date = $list_gr['delivery_date'];
				$gr->comments = $list_gr['comments'];
				$gr->pr_no_immpos = $list_gr['pr_no_immpos'];
				$gr->pr_no_immbo = $list_gr['pr_no_immbo'];
				$gr->po_no_immbo = $list_gr['po_no_immbo'];
				$gr->retur_no_immpos = $list_gr['retur_no_immpos'];
				$gr->retur_no_immbo = $list_gr['retur_no_immbo'];
				$gr->total_pr = $list_gr['total_pr'];
				$gr->total_po = $list_gr['total_po'];
				$gr->retur_doc_immpos = $list_gr['retur_doc_immpos'];
				$gr->retur_doc_immbo = $list_gr['retur_doc_immbo'];
				$gr->total_retur = $list_gr['total_retur'];
			}			
		}
		
		$gr_update = new GR();
		$gr_update->gr_no = $_GET['GR_No'];
		$gr_update->flag_delete = 't';
		$success = $gr_update::goUpdate($gr_update);

		if($success){
			if($gr->po_no_immbo != ''){
				$po = new PO();
				$po->po_no_immbo = $gr->po_no_immbo;
				$po->gr_no_immpos = 'X';
				$success = $po::goUpdateClear($po);
				
				$po_detail = new PODetail();
				$po_detail->po_no_immbo = $gr->po_no_immbo;
				$po_detail->flag_gr_ok = 'f';
				$success = $po_detail::goUpdatePO($po_detail);
			}
			
			if($gr->retur_no_immpos != ''){
				$retur = new Retur();
				$retur->retur_no = $gr->retur_no_immpos;
				$retur->gr_retur_immpos = 'X';
				$success = $retur::goUpdateClear($retur);
				
				$retur_detail = new ReturDetail();
				$retur_detail->retur_no = $gr->retur_no_immpos;
				$retur_detail->flag_gr_ok = 'f';
				$success = $retur_detail::goUpdateRetur($retur_detail);
			}
		}
		
		$this->response->redirect(uri('GR'));		
	}
	
	public function printAction(){
		$this->AuthorityAction();
		
		$gr = new GR();
		$gr->gr_no = $_GET['GR_No'];
		$lists_gr = $gr::getFirsts($gr);
		
		if(count($lists_gr) > 0){
			foreach($lists_gr as $list_gr){
				$gr->gr_no = $list_gr['gr_no'];
				$gr->create_by = $list_gr['create_by'];
				$gr->create_date = $list_gr['create_date'];
				$gr->delivery_date = $list_gr['delivery_date'];
				$gr->comments = $list_gr['comments'];
				$gr->pr_no_immpos = $list_gr['pr_no_immpos'];
				$gr->pr_no_immbo = $list_gr['pr_no_immbo'];
				$gr->po_no_immbo = $list_gr['po_no_immbo'];
				$gr->retur_no_immpos = $list_gr['retur_no_immpos'];
				$gr->retur_no_immbo = $list_gr['retur_no_immbo'];
				$gr->total_pr = $list_gr['total_pr'];
				$gr->total_po = $list_gr['total_po'];
				$gr->retur_doc_immpos = $list_gr['retur_doc_immpos'];
				$gr->retur_doc_immbo = $list_gr['retur_doc_immbo'];
				$gr->total_retur = $list_gr['total_retur'];
				$gr->po_no_ref = $list_gr['po_no_ref'];
				$gr->order_date = $list_gr['order_date'];
			}			
		}
		$this->view->gr = $gr;
		
		$data = null;
		
		// GR PO/PR
		if($gr->po_no_immbo != '' && $gr->po_no_immbo != null){
			// $po_detail = new PODetail();
			// $condition = " WHERE \"po_no_immbo\" = '".$gr->po_no_immbo."' ";
			// $data = $po_detail::getFreeSQL($condition);

			$data = array();	
			$condition = " WHERE po.po_no_immbo = '".$gr->po_no_immbo."' ";
			$data = PO::getJoin($condition);
		}


		if($gr->pr_no_immpos != '' && $gr->pr_no_immpos != null){
			$pr_detail = new PRDetail();
			$condition = " WHERE \"request_no\" = '".$gr->pr_no_immpos."' ";
			$data_pr = $pr_detail::getFreeSQL($condition);
		}
		
		for($i=0; $i<count($data); $i++){
			$data[$i]['qty_pr'];
			$data[$i]['unit_pr'];
			$data[$i]['unit_po'];
			$data[$i]['qty_retur'];
			$data[$i]['unit_retur'];
			$data[$i]['uom_o_barcode'];
			$data[$i]['uom2_conversion'];

			// for($j=0; $j<count($data_pr); $j++){
			// 	if($data[$i]['po_detail_item'] == $data_pr[$j]['pr_detail_item']){
			// 		$data[$i]['qty_pr'] = $data_pr[$j]['qty_order'];
			// 		$data[$i]['unit_pr'] = $data_pr[$j]['unit_order'];
			// 		$data[$i]['unit_po'] = $data_pr[$j]['unit_order'];
			// 		$data[$i]['barcode'] = $data_pr[$j]['barcode'];
			// 		$data[$i]['supplied_by'] = $data_pr[$j]['supplied_by'];
			// 		$data[$i]['uom2_internal_barcode'] = $data_pr[$j]['uom2_internal_barcode'];
			// 		$data[$i]['uom2_conversion'] = $data_pr[$j]['uom2_conversion'];
			// 		$data[$i]['uom2_nm'] = $data_pr[$j]['uom2_nm'];
			// 		$data[$i]['uom1_internal_barcode'] = $data_pr[$j]['uom1_internal_barcode'];
			// 		$data[$i]['uom1_nm'] = $data_pr[$j]['uom1_nm'];
			// 	}
			// }
		}
		
		// GR Retur
		if($gr->retur_no_immpos != '' && $gr->retur_no_immpos != null){
			$retur_detail = new ReturDetail();
			$condition = " WHERE \"retur_no\" = '".$gr->retur_no_immpos."' ";
			echo $condition;
			$data = $retur_detail::getFreeSQL($condition);
			
			for($i=0; $i<count($data); $i++){
				$data[$i]['qty_pr'] = '0';
				$data[$i]['unit_pr'] = '';
				$data[$i]['qty_po'] = '0';
				$data[$i]['unit_po'] = '';
				$data[$i]['po_unit_price'] = $data[$i]['retur_unit_price'];
				$data[$i]['po_price'] = '0';
				$data[$i]['barcode'] = '';
				$data[$i]['supplied_by'] = '';
			}	
		}
		
		$this->view->data = $data;
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
				$tmuk->saldo = $list_tmuk['saldo'];
				$tmuk->telp = $list_tmuk['telp'];
				$tmuk->name = $list_tmuk['name'];
				$tmuk->owner = $list_tmuk['owner'];
				$tmuk->address1 = $list_tmuk['address1'];
				$tmuk->address2 = $list_tmuk['address2'];
				$tmuk->address3 = $list_tmuk['address3'];
				$tmuk->lsi = $list_tmuk['nama_lsi'];				
			}
		}
		
		$this->view->tmuk = $tmuk;
		
		// $supplier = new Supplier();
		// $condition = " WHERE \"supplier_id\" = '".$tmuk->supplier_id."' "; 
		// $lists_suppplier = $supplier::getFreeSQL($condition);
		
		// if(isset($lists_suppplier)){
		// 	foreach($lists_suppplier as $list_suppplier){
		// 		$supplier->supplier_id = $list_suppplier['supplier_id'];
		// 		$supplier->supp_name = $list_suppplier['supp_name'];	
		// 		$supplier->address = $list_suppplier['address'];
		// 		$supplier->kecamatan = $list_suppplier['kecamatan'];				
		// 		$supplier->supp_telp = $list_suppplier['supp_telp'];
		// 	}
		// }
		
		// $this->view->supplier = $supplier;
	}
}