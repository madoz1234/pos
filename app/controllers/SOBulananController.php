<?php

use Phalcon\Mvc\Controller;
use GuzzleHttp\Client;

class SOBulananController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'SOBulanan' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$so = new SO();
		$condition = " WHERE ( \"status\" <> 'DELETE' AND \"status\" <> 'COMPLETE'  ) AND \"so_type\" = 'MONTH' ";
		$this->view->lists = $so::getFreeSQL($condition);
		
		$condition2 = " WHERE \"so_type\" = 'MONTH' AND \"status\" = 'COMPLETE' ";
		$this->view->lists_history = $so::getFreeSQL($condition2);
	}
	
	public function addAction()
	{		
		$this->AuthorityAction();
		
		$product = new Product();
		$cond = " WHERE mb_flag = 'B' AND ppob = 0 AND inactive = 0 AND mpp.average_cost != 0 AND status_flag != '2' ORDER BY stock_id ";
		$lists_product = $product::getJoin_ProductPrice($cond);

		$data_product = null;
		
		for($i = 0, $j = 0; $i < count($lists_product); $i++){			
			$data_product[$j]['stock_id'] = $lists_product[$i]['stock_id'];
			$data_product[$j]['description'] = $lists_product[$i]['description'];
			$data_product[$j]['harga'] = $lists_product[$i]['average_cost'];
			$data_product[$j]['barcode'] = $lists_product[$i]['uom1_internal_barcode'];
			$data_product[$j]['satuan'] = $lists_product[$i]['uom1_nm'];
			$j++;
		}
		
		$this->view->data_products = $data_product;
	}		
	
	public function ajaxSOAction(){
		$so_detail = new SODetail();
		$condition = " WHERE \"so_id\" = '".$_GET['so_id']."' ";
		$data = $so_detail::getFreeSQL($condition);
		
		return json_encode($data);
	}
	
	public function insertAction()
	{
		$number_range = new NumberRange();
		$number_range->trans_type = 'SO';
		$number_range->period = date('Ym');
		$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
		$lists = $number_range::getFreeSQL($condition);
		
		if(isset($lists)){
			foreach($lists as $list){
				$number_range->trans_type = $list['trans_type'];
				$number_range->period = $list['period'];
				$number_range->prefix = $list['prefix'];
				$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
			}
		}else{
			$number_range->trans_type = 'SO';
			$number_range->period = date('Ym');
			$number_range->prefix = 'SO';
			$number_range->from_ = '1';
			$number_range->to_ = '9999';
			$number_range->current_no = '1';
			$success = $number_range::goInsert($number_range);			
			$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
		}
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
			}
		}
		
		$so = new SO();
		$so->so_id = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;
		$so->kode_verifikasi = date("Ymd").(substr($so->so_id, 0, 2)).(substr($so->so_id, -3));
		$so->user_id = $this->request->getPost("user_id");
		$so->status = $this->request->getPost("status");
		$so->so_type = $this->request->getPost("so_type");
		$so->flag = false;
		$success = $so::goInsert($so);
		
		if($success){			
			$success_num = $number_range::goAddNumber($number_range);
			
			$json_datas = json_decode($_POST['json_data']);	
			$so_detail = new SODetail();
			$count = 0;			
			$query_insert = '';
			$n =0;
			$hitung = count($json_datas);
			$soQuery = $so_detail::queryInsertHPP($so_detail);

			foreach($json_datas as $json_data){
				$price = new ProductPrice();
				$condition = " WHERE \"stock_id\" = '".$json_data[0]."' ";
				$data = $price::getFreeSQL($condition)[0];

				$so_detail->so_id = $so->so_id;
				$so_detail->item_code = $json_data[0];
				$so_detail->barcode_code = $json_data[1];
				$so_detail->description = pg_escape_string($json_data[2]);
				$so_detail->unit_item = $json_data[3];
				$so_detail->unit_price = $data['average_cost'];
				if($n == ($hitung - 1)){
					$soQuery .= $so_detail::InsertEndSql($so_detail);
				}else{
					$soQuery .= $so_detail::insertSql($so_detail);
				}
				$n++;
			}
			$soQuery .= $so_detail::endSql($so_detail);
			if($soQuery != ''){
				$success = $so_detail::executeQuery($soQuery);
			}

			$data['type'] = 'S';
			$data['message'] = '<b>'.$so->so_id.'</b> Berhasil dibuat.';
			$notif = new Notifikasi();
			$notif->data = $data['message'];
			$notif->read_at = null;
			$notif->created_at = date('Y-m-d H:i:s');
			$notif->created_by = $this->request->getPost("user_id");

			$success = $notif::goInsert($notif);
			
			return json_encode($data);
		}else{						
			$data['type'] = 'E';
			$data['message'] = 'Gagal membuat SO Bulanan!';
			return json_encode($data);
		}
	}	
	
	public function deleteAction()
	{				
		$this->AuthorityAction();
		
		$so = new SO();
		$so->so_id = $_REQUEST['SO_ID'];
		$so->status = 'DELETE';
		$success = $so::goUpdate($so);

		$data['message'] = '<b>'.$so->so_id.'</b> Berhasil dihapus.';
		$notif = new Notifikasi();
		$notif->data = $data['message'];
		$notif->read_at = null;
		$notif->created_at = date('Y-m-d H:i:s');
		$notif->created_by = $this->request->getPost("user_id");

		$sukses = $notif::goInsert($notif);	

		$this->response->redirect(uri('SOBulanan'));
	}
	
	public function prosesAction()
	{			
		$this->AuthorityAction();
		
		// get data SO Bulanan
		$so = new SO();
		$so->so_id = $_REQUEST['SO_ID'];
		$lists_so = $so::getFirst($so);
		
		foreach($lists_so as $list_so){
			$so->so_id = $list_so['so_id'];
			$so->kode_verifikasi = $list_so['kode_verifikasi'];
			$so->user_id = $list_so['user_id'];
			$so->so_date = $list_so['so_date'];
			$so->status = $list_so['status'];
			$so->so_type = $list_so['so_type'];
		}
		
		$this->view->so = $so;
		
		$so_detail = new SODetail();
		$so_detail->so_id = $_REQUEST['SO_ID'];
		$condition = " WHERE \"so_id\" = '".$so_detail->so_id."' ";
		$lists_so_detail = $so_detail::getFreeSQL($condition);
		
		$this->view->so_details = $lists_so_detail;
	}

	public function saveProsesAction()
	{	
		$so = new SO();
		$so->so_id = $this->request->getPost("so_id");
		$so->status = $this->request->getPost("status");
		$success = $so::goUpdate($so);
		
		if($success){
			$json_datas = json_decode($_POST['json_data']);
			$so_detail = new SODetail();
			
				$product_move = new ProductMove();						
				$lists_stock = $product_move::getAllStock($product_move);
				
			$count = 0;			
			$query_update = '';
			if($json_datas){
				foreach($json_datas as $json_data){
					if($count >= 300){
						$success = $so_detail::goFreeSql($query_update);
						$count = 0;					
						$query_update = '';
					}
					
					$so_detail->so_id = $this->request->getPost("so_id");			
					$so_detail->so_detail_item = $json_data[0];				
					$so_detail->qty_barcode = $json_data[4];
					$so_detail->flag_so_ok = $json_data[7];	

					if(count($lists_stock) > 0){
						foreach($lists_stock as $list_stock){
							if(trim($list_stock['stock_id']) == $json_data[1]){
								$so_detail->qty_tersedia = $list_stock['stock_qty'];
							}
							
							if($so_detail->qty_tersedia == ''){
								$so_detail->qty_tersedia = 0;
							}
						}
					}
					
					if($count > 0) { $query_update .= ";"; }				
						$query_update .= " UPDATE \"t_so_detail\" SET \"qty_barcode\" = '".$so_detail->qty_barcode."', \"flag_so_ok\" = '".$so_detail->flag_so_ok."' "
										.", \"qty_tersedia\" = '".$so_detail->qty_tersedia."' "
										." WHERE \"so_detail_item\" = '".$so_detail->so_detail_item."'";
					
					$count++;
				}
			}
			
			if($query_update != ''){
				$success = $so_detail::goFreeSql($query_update);
				$count = 0;	
				$query_update = '';
				
				$data['type'] = 'S';
				$data['message'] = '<b>'.$so->so_id.'</b> Berhasil diproses.';
				$notif = new Notifikasi();
				$notif->data = $data['message'];
				$notif->read_at = null;
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by = $this->request->getPost("user_id");

				$success = $notif::goInsert($notif);

				return json_encode($data);
			}
		}
		
		return json_encode($data);
	}
	
	public function postAction()
	{				
		$this->AuthorityAction();
		
		// get data SO Bulanan
		$so = new SO();
		$so->so_id = $_REQUEST['SO_ID'];
		$lists_so = $so::getFirst($so);
		
		foreach($lists_so as $list_so){
			$so->so_id = $list_so['so_id'];
			$so->kode_verifikasi = $list_so['kode_verifikasi'];
			$so->user_id = $list_so['user_id'];
			$so->so_date = $list_so['so_date'];
			$so->status = $list_so['status'];
			$so->so_type = $list_so['so_type'];
		}
		
		$this->view->so = $so;
		
		$so_detail = new SODetail();
		$so_detail->so_id = $_REQUEST['SO_ID'];
		$condition = " WHERE \"so_id\" = '".$so_detail->so_id."' ";
		$lists_so_detail = $so_detail::getFreeSQL($condition);
		
		$this->view->so_details = $lists_so_detail;
	}
	
	public function savePostAction()
	{		
		$so = new SO();
		$so->so_id = $this->request->getPost("so_id");
		$so->status = $this->request->getPost("status");
		$success = $so::goUpdate($so);
		
		if($success){
			$number_range = new NumberRange();
			$number_range->trans_type = 'MV';
			$number_range->period = date('Ym');
			$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
			$lists = $number_range::getFreeSQL($condition);
			
			if(isset($lists)){
				foreach($lists as $list){
					$number_range->trans_type = $list['trans_type'];
					$number_range->period = $list['period'];
					$number_range->prefix = $list['prefix'];
					$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
				}
			}else{
				$number_range->trans_type = 'MV';
				$number_range->period = date('Ym');
				$number_range->prefix = 'MV';
				$number_range->from_ = '1';
				$number_range->to_ = '9999';
				$number_range->current_no = '1';
				$success = $number_range::goInsert($number_range);			
				$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
			}
			
			$so_detail = new SODetail();
			$condition = " WHERE \"so_id\" = '".$so->so_id."' ";
			$lists_so_detail = $so_detail::getFreeSQL($condition);
			
			$product_move = new ProductMove();
			
			$count = 0;			
			$query_insert = '';
			foreach($lists_so_detail as $list_so_detail){
				if($count >= 300){
					$success = $product_move::goMultiInsert($query_insert);
					$count = 0;					
					$query_insert = '';
				}
				
				if($list_so_detail['qty_barcode'] != $list_so_detail['qty_tersedia'] && $list_so_detail['flag_so_ok']){
					$product_move->trans_no = $number_range->prefix.$number_range->current_no;
					$product_move->stock_id = $list_so_detail['item_code'];
					$product_move->type = 'SO';
					$product_move->reference = $so->so_id;
					$product_move->qty = ( $list_so_detail['qty_barcode'] - $list_so_detail['qty_tersedia'] );
					$product_move->price = $list_so_detail['unit_price'];
					$moveQuery .= $product_move::insertSql($product_move);					
				}
			}

			if($moveQuery != ''){
				$success = $product_move::executeQuery($moveQuery);
			}

			$success_num = $number_range::goAddNumber($number_range);
		}
		$data['type'] = 'S';
		$data['message'] = '<b>'.$so->so_id.'</b> Berhasil diposting.';
		$notif = new Notifikasi();
		$notif->data = $data['message'];
		$notif->read_at = null;
		$notif->created_at = date('Y-m-d H:i:s');
		$notif->created_by = $this->request->getPost("user_id");

		$success = $notif::goInsert($notif);

		return json_encode($data);
	}
	
	public function excelAction(){ 
		$this->AuthorityAction();
		
		$so = new SO();
		$so->so_id = $_GET['SO_ID'];
		$lists_so = $so::getFirst($so);
		
		$list_so_view = null;
		if(isset($lists_so)){
			foreach($lists_so as $list_so){
				$list_so_view = $list_so;
			}
		}
		
		$this->view->so = $list_so_view;
		
		$so_detail = new SODetail();
		$condition = "WHERE so_id = '".$so->so_id."'";
		$lists_so_detail = $so_detail::getJoin($condition);
		
		$this->view->so_details = $lists_so_detail;
		$this->view->sales_SO = $so_detail::getSalesSO();
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
				$tmuk->saldo = $list_tmuk['saldo'];
				$tmuk->telp = $list_tmuk['telp'];
				$tmuk->name = $list_tmuk['name'];
				$tmuk->owner = $list_tmuk['owner'];
				$tmuk->address1 = $list_tmuk['address1'];
				$tmuk->address2 = $list_tmuk['address2'];
				$tmuk->address3 = $list_tmuk['address3'];				
			}
		}
		
		$this->view->tmuk = $tmuk;
	}
	
	public function printAction(){
		$this->AuthorityAction();
		
		$so = new SO();
		$so->so_id = $_GET['SO_ID'];
		$lists_so = $so::getFirst($so);
		
		$list_so_view = null;
		if(isset($lists_so)){
			foreach($lists_so as $list_so){
				$list_so_view = $list_so;
			}
		}
		
		$this->view->so = $list_so_view;
		
		$so_detail = new SODetail();
		$condition = "WHERE so_id = '".$so->so_id."'";
		$lists_so_detail = $so_detail::getJoin($condition);
		
		$this->view->so_details = $lists_so_detail;
		// $this->view->sales_SO = $so_detail::getSalesSO();
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
				$tmuk->saldo = $list_tmuk['saldo'];
				$tmuk->telp = $list_tmuk['telp'];
				$tmuk->name = $list_tmuk['name'];
				$tmuk->owner = $list_tmuk['owner'];
				$tmuk->address1 = $list_tmuk['address1'];
				$tmuk->address2 = $list_tmuk['address2'];
				$tmuk->address3 = $list_tmuk['address3'];				
			}
		}
		
		$this->view->tmuk = $tmuk;
	}
	
	// public function exportexcelAction(){
	// 	// get data SO Bulanan
	// 	$so = new SO();
	// 	$so->so_id = $_REQUEST['SO_ID'];
	// 	$lists_so = $so::getFirst($so);
	// 	foreach($lists_so as $list_so){
	// 		$so->so_id = $list_so['so_id'];
	// 		$so->kode_verifikasi = $list_so['kode_verifikasi'];
	// 		$so->user_id = $list_so['user_id'];
	// 		$so->so_date = $list_so['so_date'];
	// 		$so->status = $list_so['status'];
	// 		$so->so_type = $list_so['so_type'];
	// 	}
		
	// 	$this->view->so = $so;
		
	// 	$so_detail = new SODetail();
	// 	$so_detail->so_id = $_REQUEST['SO_ID'];
	// 	$condition = " WHERE \"so_id\" = '".$so_detail->so_id."' ORDER BY so_detail_item ";
	// 	$lists_so_detail = $so_detail::getFreeSQL($condition);
		
	// 	$this->view->so_details = $lists_so_detail;
	// }
	
	// public function importexcelAction(){
	// 	$this->AuthorityAction();
		
	// 	// get data SO Harian
	// 	$so = new SO();
	// 	$so->so_id = $_REQUEST['SO_ID'];
	// 	$lists_so = $so::getFirst($so);
		
	// 	foreach($lists_so as $list_so){
	// 		$so->so_id = $list_so['so_id'];
	// 		$so->kode_verifikasi = $list_so['kode_verifikasi'];
	// 		$so->user_id = $list_so['user_id'];
	// 		$so->so_date = $list_so['so_date'];
	// 		$so->status = $list_so['status'];
	// 		$so->so_type = $list_so['so_type'];
	// 	}
		
	// 	$this->view->so = $so;
	// }

	public function exportexcelAction() {

		$this->AuthorityAction();
		$so = new SO();
		$so->so_id = $_GET['SO_ID'];
		$lists_so = $so::getFirst($so);
		
		$list_so_view = null;
		if(isset($lists_so)){
			foreach($lists_so as $list_so){
				$list_so_view = $list_so;
			}
		}

		function tgl_indonesia($date){
			$Hari = array ("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu",);
			$Bulan = array ("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
			$tahun     = substr($date, 0, 4);
			$bulan     = substr($date, 5, 2);
			$tgl    = substr($date, 8, 2);
			$waktu    = substr($date,11, 5);
			$hari    = date("w", strtotime($date));
			$result = $Hari[$hari].", ".$tgl." ".$Bulan[(int)$bulan-1]." ".$tahun." ".$waktu." ";
			return $result;
		}
				
		$so_detail = new SODetail();
		$condition = "WHERE so_id = '".$so->so_id."'";
		$lists_so_detail = $so_detail::getJoin($condition);

		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
				$tmuk->saldo = $list_tmuk['saldo'];
				$tmuk->telp = $list_tmuk['telp'];
				$tmuk->name = $list_tmuk['name'];
				$tmuk->owner = $list_tmuk['owner'];
				$tmuk->address1 = $list_tmuk['address1'];
				$tmuk->address2 = $list_tmuk['address2'];
				$tmuk->address3 = $list_tmuk['address3'];				
			}
		}
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->setTitle($so->so_id);

		foreach(range('A1','F2') as $head)
		{
			$objPHPExcel->getActiveSheet()->getStyle($head)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
		}


		foreach(range('A4','F4') as $set)
		{
			$objPHPExcel->getActiveSheet()->getStyle($set)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
			$objPHPExcel->getActiveSheet()->getColumnDimension($set)->setAutoSize(true);
		}

		$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells('A1:M1')
		->mergeCells('A2:M2')
		->setCellValue('A1', 'SO BULANAN TMUK '.$tmuk->name)
		->setCellValue('A2', tgl_indonesia(date('Y-m-d')))
		->setCellValue('A4', 'SO Detail Item')
		->setCellValue('B4', 'SO ID')
		->setCellValue('C4', 'Kode Barang')
		->setCellValue('D4', 'Barcode')
		->setCellValue('E4', 'Nama Barang')
		->setCellValue('F4', 'Qty')
		->setCellValue('G4', 'Satuan');
		$i=5;
		foreach ($lists_so_detail as $key => $value) {
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, $value['so_detail_item'])
			->setCellValue('B'.$i, $value['so_id'])
			->setCellValue('C'.$i, $value['item_code'])
			->setCellValue('D'.$i, $value['barcode_code'])
			->setCellValue('E'.$i, $value['description'])
			->setCellValue('F'.$i, $value['qty_barcode'])
			->setCellValue('G'.$i, $value['unit_item']);
			$i++;
		}
		$fname = $so->so_id . ".xlsx";
		$temp_file = tempnam(sys_get_temp_dir(), 'phpexcel');

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($temp_file);

		$response = new \Phalcon\Http\Response();

		$response->setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		$response->setHeader('Content-Disposition', 'attachment;filename="' . $fname . '"');
		$response->setHeader('Cache-Control', 'max-age=0');

		$response->setHeader('Cache-Control', 'max-age=1');

		$response->setContent(file_get_contents($temp_file));

		unlink($temp_file);

		return $response;
	}
	
	// public function saveImportAction(){
	// 	if ($this->request->hasFiles($_REQUEST['id']) == true) {
	// 		foreach ($this->request->getUploadedFiles($_REQUEST['id']) as $file) {
	// 			$lampiran = $file;
	// 			$filepath = curl_file_create( realpath($lampiran->getTempName()), $lampiran->getRealType(), $lampiran->getName() );
	// 			$xfile = fopen($lampiran->getTempName(), 'r');
	// 			$filename = $lampiran->getName();
	// 		}
	// 		$excelReader = PHPExcel_IOFactory::createReaderForFile($lampiran->getTempName());
	// 		$excelObj = $excelReader->load($lampiran->getTempName());
	// 		$worksheet = $excelObj->getSheet(0);
	// 		$lastRow = $worksheet->getHighestRow();

	// 		$upload = 0;
	// 		$gagal = 0;
	// 		$nomor = 2;

	// 		$objPHPExcel = new PHPExcel();
	// 		$objPHPExcel->getProperties()
	// 		->setCreator("Admin")
	// 		->setTitle("Log Gagal Import")
	// 		->setSubject("Template excel")
	// 		->setDescription("")
	// 		->setKeywords("Template excel");
	// 		$objPHPExcel->setActiveSheetIndex(0);

	// 		for ($row = 2; $row <= $lastRow; $row++) {
	// 			try {
	// 			$SOBulanan = new SODetail();
	// 			$SOBulanan->so_detail_item	= $worksheet->getCell('A'.$row)->getValue();
	// 			$SOBulanan->so_id	= $worksheet->getCell('B'.$row)->getValue();
	// 			$SOBulanan->item_code	= $worksheet->getCell('C'.$row)->getValue();
	// 			$SOBulanan->barcode_code	= preg_replace('/[^A-Za-z0-9\-]/', '', pg_escape_string($worksheet->getCell('D'.$row)->getValue()));
	// 			$SOBulanan->description= $worksheet->getCell('E'.$row)->getValue();
	// 			$SOBulanan->qty_barcode	= $worksheet->getCell('F'.$row)->getValue();
	// 			$SOBulanan->unit_item	= $worksheet->getCell('G'.$row)->getValue();
	// 			$success = $SOBulanan::goUpdate2($SOBulanan);
	// 			if($success){
	// 					$upload++;
	// 				}
	// 			} catch (Exception $e) {
	// 				$gagal++;

	// 				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $worksheet->getCell('A'.$row)->getValue());
	// 				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $worksheet->getCell('B'.$row)->getValue());
	// 				$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $worksheet->getCell('C'.$row)->getValue());
	// 				$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $worksheet->getCell('D'.$row)->getValue());
	// 				$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $worksheet->getCell('E'.$row)->getValue());
	// 				$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $worksheet->getCell('F'.$row)->getValue());
	// 				$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $worksheet->getCell('G'.$row)->getValue());
	// 			}
	// 		}
	// 		$result['message'] = $gagal.' Gagal dan '.$upload.' Berhasil di Import!';
	// 		if($gagal>0){
	// 			$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	// 			$filename = 'log-import/'.date("YmdHis").'.xls';
	// 			$writer->save($filename);
	// 			$result['message'] = $gagal.' Gagal dan '.$upload.' Berhasil di Import! <br><a href="'.$filename.'">Download Log</a>';
	// 		}
	// 		$result['type'] = 'S';
	// 		return json_encode($result);
	// 	}else{
	// 		$result['type'] = 'E';
	// 		$result['message'] = 'Gagal Import Data!';
	// 		return json_encode($result);
	// 	}
		
	// }

	public function importexcelAction(){
		$this->AuthorityAction();
		
		// get data SO Harian
		$so = new SO();
		$so->so_id = $_REQUEST['SO_ID'];
		$lists_so = $so::getFirst($so);
		
		foreach($lists_so as $list_so){
			$so->so_id = $list_so['so_id'];
			$so->kode_verifikasi = $list_so['kode_verifikasi'];
			$so->user_id = $list_so['user_id'];
			$so->so_date = $list_so['so_date'];
			$so->status = $list_so['status'];
			$so->so_type = $list_so['so_type'];
		}
		
		$this->view->so = $so;
	}

	public function saveImportAction(){
		if ($this->request->hasFiles($_REQUEST['id']) == true) {
			$so_delete = new SODetail();
			$so_delete->so_id = $this->request->getPost("request_no");
			$sukses = $so_delete::goDeleteSO($so_delete);
			
			foreach ($this->request->getUploadedFiles($_REQUEST['id']) as $file) {
				$lampiran = $file;
				$filepath = curl_file_create( realpath($lampiran->getTempName()), $lampiran->getRealType(), $lampiran->getName() );
				$xfile = fopen($lampiran->getTempName(), 'r');
				$filename = $lampiran->getName();
			}
			$excelReader = PHPExcel_IOFactory::createReaderForFile($lampiran->getTempName());
			$excelObj = $excelReader->load($lampiran->getTempName());
			$worksheet = $excelObj->getSheet(0);
			$lastRow = $worksheet->getHighestRow();

			$upload = 0;
			$gagal = 0;
			$nomor = 2;

			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()
			->setCreator("Admin")
			->setTitle("Log Gagal Import")
			->setSubject("Template excel")
			->setDescription("")
			->setKeywords("Template excel");
			$objPHPExcel->setActiveSheetIndex(0);

			for ($row = 5; $row <= $lastRow; $row++) {
				try {
				$n = preg_replace('/[^A-Za-z0-9\-]/', '', pg_escape_string($worksheet->getCell('F'.$row)->getValue()));
				$SOBulanan = new SODetail();
				$SOBulanan->so_detail_item	= $worksheet->getCell('A'.$row)->getValue();
				$SOBulanan->so_id	= $worksheet->getCell('B'.$row)->getValue();
				$SOBulanan->item_code	= $worksheet->getCell('C'.$row)->getValue();
				$SOBulanan->barcode_code	= preg_replace('/[^A-Za-z0-9\-]/', '', pg_escape_string($worksheet->getCell('D'.$row)->getValue()));
				$SOBulanan->description= $worksheet->getCell('E'.$row)->getValue();
				$SOBulanan->qty_barcode	= $n<=0?0:$n;
				$SOBulanan->unit_item	= $worksheet->getCell('G'.$row)->getValue();
				$success = $SOBulanan::goUpdate2($SOBulanan);
				if($success){
						$upload++;
					}
				} catch (Exception $e) {
					$gagal++;

					$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $worksheet->getCell('A'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $worksheet->getCell('B'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $worksheet->getCell('C'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $worksheet->getCell('D'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $worksheet->getCell('E'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $worksheet->getCell('F'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $worksheet->getCell('G'.$row)->getValue());
				}
			}
			$result['message'] = $gagal.' Gagal dan '.$upload.' Berhasil di Import!';
			if($gagal>0){
				$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$filename = 'log-import/'.date("YmdHis").'.xls';
				$writer->save($filename);
				$result['message'] = $gagal.' Gagal dan '.$upload.' Berhasil di Import! <br><a href="'.$filename.'">Download Log</a>';
			}
			$result['type'] = 'S';
			return json_encode($result);
		}else{
			$result['type'] = 'E';
			$result['message'] = 'Gagal Import Data!';
			return json_encode($result);
		}
		
	}
	
}
