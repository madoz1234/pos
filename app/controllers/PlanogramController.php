<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;

class PlanogramController extends Controller
{	
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'Planogram' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction(){
		$this->AuthorityAction();
		$tmuk = new TMUK();
		$lists = $tmuk::getAll();
		
		if(isset($lists)){
			foreach($lists as $list){
				$tmuk->tmuk_id = $list['tmuk_id'];
				$tmuk->tmuk = $list['tmuk'];
			}
		}

		$this->view->kode = $tmuk->tmuk;

	}

	public function goPrintAction(){	
		if (is_connected() === false) {
			$data['type'] = 'E';
			$data['alert'] = 'error';
			$data['message'] = 'Mohon Periksa Kembali Koneksi Anda !!!';

			return json_encode($data);
		}else{
			$success = $this->printPlanogramAction();
			if($success['status'] == ''){
				$data['type'] = 'E';
				$data['message'] = $success['message'];	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	
				$success = $notif::goInsert($notif);
				return json_encode($data);
			}else{
				$data['type'] = 'S';
				$data['message'] = $success['message'];	
				$data['data'] = $success['data']['pdf_path'];	
				$notif = new Notifikasi();
				$notif->data = strip_tags($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by =$this->session->get('user')['real_name'];	
				$success = $notif::goInsert($notif);
				return json_encode($data);
			}
		}
	}
	
	public function printPlanogramAction(){
		$this->AuthorityAction();
		$tmuk = new TMUK();
		$lists = $tmuk::getAll();
		
		if(isset($lists)){
			foreach($lists as $list){
				$tmuk->tmuk_id = $list['tmuk_id'];
				$tmuk->tmuk = $list['tmuk'];
			}
		}

		// $kode = '0602200043';
		$kode = $tmuk->tmuk;
		$curl = new CurlClass();
		$result = $curl->get($this->di['api']['link']."/api/planogram/".$kode."");
		$hasil = json_decode($result, true);
		return $hasil;
	}
	
	// public function excelAction(){	
	// 	$tmuk = new TMUK();
	// 	$lists = $tmuk::getAll();
		
	// 	if(isset($lists)){
	// 		foreach($lists as $list){
	// 			$tmuk->tmuk_id = $list['tmuk_id'];
	// 			$tmuk->tmuk = $list['tmuk'];
	// 		}
	// 	}
		
	// 	$api_link =  $this->di['api']['link'];
	// 	$split_link = explode('API', $api_link);
	// 	$excel_link = $split_link[0].'purchasing/'.$tmuk->tmuk.'_'.$_GET['rack'].'_'.$_GET['nomor'].'.xls';
		
	// 	$file_headers = @get_headers($excel_link);
	// 	if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') { echo 'gak ada'; }
	// 	else{
	// 		header('Location: '.$excel_link.'');
	// 	}
			
	// 	require (__DIR__ . '/../library/excel_reader.php');
	// 	$filename = __DIR__ . '/../../public/sync/planogram/'.$_GET['rack'].'_'.$_GET['nomor'].'.xls';
	// 	if(is_readable($filename)){}else{
	// 		echo 'File tidak ditemukan.';
	// 		return false;
	// 	}
		
	// 	return $filename;
	// }

	// public function viewAction(){
	// 	$this->view->setRenderLevel(
 //            View::LEVEL_MAIN_LAYOUT
 //        );
	// 	$this->AuthorityAction();
		
	// 	$rack = new Rack();
	// 	$rack->id = $_REQUEST['id'];
	// 	$lists = $rack::getFirst($rack);
		
	// 	$this->view->data = new Rack();
	// 	if(count($lists) > 0){
	// 		foreach($lists as $list){
	// 			$this->view->data->id = $list['id'];				
	// 			$this->view->data->rack_type = $list['rack_type'];
	// 			$this->view->data->tinggi = $list['tinggi'];
	// 			$this->view->data->panjang = $list['panjang'];
	// 			$this->view->data->lebar = $list['lebar'];
	// 			$this->view->data->shelving = $list['shelving'];
	// 			$this->view->data->hunger = $list['hunger'];
	// 			$this->view->data->tinggi_hunger = $list['tinggi_hunger'];
	// 			$this->view->data->panjang_hunger = $list['panjang_hunger'];
	// 			$this->view->data->nomor = $_REQUEST['no'];
	// 		}
	// 	}
	// }
	
}
