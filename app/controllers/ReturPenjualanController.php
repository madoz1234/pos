<?php

use Phalcon\Mvc\Controller;

class ReturPenjualanController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'ReturPenjualan' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function AuthoritySoDAction()
	{		
		$SoD = new StartEndDay();
		$condition = " WHERE \"tanggal\" = '".date('Y-m-d')."' AND \"flag_start\" = true AND \"flag_end\" = false ";
		$lists_SoD = $SoD::getFreeSQL($condition);

		if(!isset($lists_SoD)){			
			$this->response->redirect(uri('Auth/SoD'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		$this->AuthoritySoDAction();
		
		$jual = new Jual();
		$condition = " WHERE \"flag_delete\" = false AND \"flag_sum\" = false AND \"tanggal\" BETWEEN '".date('Y-m-d')." 00:00:00' AND '".date('Y-m-d')." 23:59:59' ";
		$this->view->lists_jual = $jual::getFreeSql($condition);
	}
	
	public function ajaxJualAction()
	{
		$jual_detail = new JualDetail();
		$condition = " WHERE \"id_jual\" = '".$_GET['hold']."' ";
		$data = $jual_detail::getFreeSQL($condition);

		return json_encode($data);
	}
}
