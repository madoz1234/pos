<?php
error_reporting(0);
use Phalcon\Mvc\Controller;
use Phalcon\Config;
include __DIR__ . "/../library/print_format.php";
require_once ('../app/library/src/Autoloader.php');

PhpXmlRpc\Autoloader::register();
use PhpXmlRpc\Value;
use PhpXmlRpc\Request;
use PhpXmlRpc\Client;

class PenjualanController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'Penjualan' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function AuthoritySoDAction()
	{		
		$SoD = new StartEndDay();
		$condition = " WHERE \"tanggal\" = '".date('Y-m-d')."' AND \"flag_start\" = true AND \"flag_end\" = false ";
		$lists_SoD = $SoD::getFreeSQL($condition);

		if(!isset($lists_SoD)){			
			$this->response->redirect(uri('Auth/SoD'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		$this->AuthoritySoDAction();
		
		$start_end_shift = new StartEndShift();
		$condition = " WHERE \"user_id\" = '".$this->session->get("user")['user_name']."' ";
		$condition .= " AND \"flag_start\" = true AND \"flag_end\" = false ";
		$lists_seShift = $start_end_shift::getFreeSQL($condition);
		if(count($lists_seShift) > 0){
			$this->response->redirect(uri('Penjualan/Sell'));
		}else{
			$this->response->redirect(uri('Penjualan/Start'));
		}
	}
	
	public function ajaxSearchProductAction(){
		$product = new Product();
		$query = strtoupper($_GET['query']);
		
		if($query != ""){
			$condition = " WHERE (upper(\"uom1_internal_barcode\") LIKE '%".$query."%' OR upper(\"uom2_internal_barcode\") LIKE '%".$query."%' "
			." OR upper(\"uom3_internal_barcode\") LIKE '%".$query."%' OR upper(\"uom4_internal_barcode\") LIKE '%".$query."%' "
			." OR upper(\"uom1_prod_nm\") LIKE '%".$query."%' OR upper(\"uom2_prod_nm\") LIKE '%".$query."%' OR upper(\"uom3_prod_nm\") LIKE '%".$query."%' "
			." OR upper(\"uom4_prod_nm\") LIKE '%".$query."%') AND mb_flag != 'D' AND inactive = 0 ORDER BY uom1_prod_nm";
		$lists_product = $product::getFreeSQLs2($query);	/*if(strtotime(date('Y-m-d')) > '1503680400')
		for($x=0; $x < product_date(); $x++){ $data_product = null; } else*/ $data_product = null;
		// echo (new \Phalcon\Debug\Dump())->variable(count($lists_product));
			for($i = 0, $j = 0; $i < count($lists_product); $i++){
				$data_product[$j+1]['val'] = $lists_product[$i]['uom1_internal_barcode'];
				$data_product[$j+1]['display'] = $lists_product[$i]['uom1_internal_barcode'].' - '.$lists_product[$i]['description'];		
				$j++;
			}

			return json_encode($data_product);}
		}

		public function startAction()
		{
			$this->AuthorityAction();
			$this->AuthoritySoDAction();

		// get data Kassa
			$kassa = new Kassa();
			$condition = " WHERE kassa_id::varchar NOT IN (SELECT kassa_id FROM t_start_end_shift WHERE tanggal = '".date('Y-m-d')."' )  LIMIT 1";
			$lists_kassa = $kassa::getFreeSQL($condition);

			if($lists_kassa){
				foreach($lists_kassa as $list_kassa){
					$data_kassa[$list_kassa['kassa_id']] = $list_kassa['description'];
				}	
			}else{
				$data_kassa = null;
				$data_kassa[''] = '-- Pilih POS --';
			}

			$this->view->kassa = $data_kassa;

		// get data Shift
			$shift = new Shift();
			$cond = " WHERE shift_id::varchar NOT IN (SELECT shift_id FROM t_start_end_shift WHERE tanggal = '".date('Y-m-d')."' )  LIMIT 1";
			$lists_shift = $shift::getFreeSQL($cond);

			if($lists_shift){
				foreach($lists_shift as $list_shift){
					$data_shift[$list_shift['shift_id']] = $list_shift['description'];
				}	
			}else{		
				$data_shift = null;
				$data_shift[''] = '-- Pilih Shift --';
			}

			$this->view->shift = $data_shift;

			$valid_ses = new StartEndShift();
			$con = " WHERE \"flag_start\" = true AND \"flag_end\" = true ORDER BY id DESC LIMIT 1";
			$data_cash = $valid_ses::getFreeSQL($con);
			foreach($data_cash as $list){
				$data[$list['cash_cashier']] = $list['cash_cashier'];
			}

			$this->view->cash = $data_cash[0]['cash_cashier'];
		}

		public function startJualAction()
		{			
			$kassa = new Kassa();
			$kassa->kassa_id = $this->request->getPost("kassa_id");
			$lists_kassa = $kassa::getFirst($kassa);

			foreach($lists_kassa as $list_kassa){
				$kassa->kassa_id = $list_kassa['kassa_id'];
				$kassa->description = $list_kassa['description'];
				$kassa->inactive = $list_kassa['inactive'];			
			}

			$shift = new Shift();
			$shift->shift_id = $this->request->getPost("shift_id");
			$lists_shift = $shift::getFirst($shift);

			foreach($lists_shift as $list_shift){
				$shift->shift_id = $list_shift['shift_id'];
				$shift->description = $list_shift['description'];
				$shift->inactive = $list_shift['inactive'];			
			}

			$valid_ses = new StartEndShift();
			$condition = " WHERE \"kassa_id\" = '".$kassa->kassa_id."' ";
			$condition .= " AND \"flag_start\" = true AND \"flag_end\" = false ";
			$lists_seShift_pos = $valid_ses::getFreeSQL($condition);

			if(isset($lists_seShift_pos)){
				$data['type'] = 'E';
				$data['message'] = 'Start of Shift Gagal, POS sedang digunakan.';
				return json_encode($data);	
			}

			$start_end_Shift = new StartEndShift();
			$start_end_Shift->tanggal = date("Y-m-d");
			$start_end_Shift->jam = date("H:m:s");
			$start_end_Shift->user_id = $this->session->get("user")['user_name'];
			$start_end_Shift->kassa_id = $kassa->kassa_id;
			$start_end_Shift->kassa_desc = $kassa->description;
			$start_end_Shift->shift_id = $shift->shift_id;
			$start_end_Shift->shift_desc = $shift->description;
			$start_end_Shift->setoran = $this->request->getPost("setoran");
			$start_end_Shift->setoran = str_replace(',', '', $start_end_Shift->setoran);
			$start_end_Shift->flag_start = 't';
			$success = $start_end_Shift::goInsert($start_end_Shift);		

			if($success){
				$data['type'] = 'S';
				$data['message'] = 'Proses Start of Shift Berhasil';
				$notif = new Notifikasi();
				$notif->data = htmlspecialchars($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by = $this->session->get('user')['real_name'];
				$success = $notif::goInsert($notif);			
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = 'Proses Start of Shift Gagal';
				$notif = new Notifikasi();
				$notif->data = htmlspecialchars($data['message']);
				$notif->read_at = date('Y-m-d H:i:s');
				$notif->created_at = date('Y-m-d H:i:s');
				$notif->created_by = $this->session->get('user')['real_name'];

				$success = $notif::goInsert($notif);
				return json_encode($data);			
			}
		}

		public function ajaxProductAction(){
			$product = new Product();
			$object['barcode'] = $_REQUEST['barcode'];
			$object['count_index'] = $_REQUEST['count_index'];
			$data = $product::getProductSale($object); 
			if(count($data) > 0){
				foreach($data as $line_data){
					if($line_data['diskon'] == null){
						$line_data['diskon'] = 0;
					}
				}
			}

			return json_encode($data);
		}
	
	// public function ajaxProductPromoAction(){
	// 	$product = new Product();
	// 	$condition = " WHERE (\"uom1_internal_barcode\" = '".$_REQUEST['barcode']."' OR \"uom2_internal_barcode\" = '".$_REQUEST['barcode']
	// 	."' OR \"uom3_internal_barcode\" = '".$_REQUEST['barcode']."' OR \"uom4_internal_barcode\" = '".$_REQUEST['barcode']."') AND mb_flag != 'D' LIMIT 1 ";
	// 	$lists_product = $product::getFreeSQL($condition);
		
	// 	if(count($lists_product) > 0){
	// 		foreach($lists_product as $list_product){
	// 			$product->stock_id = $list_product['stock_id'];
	// 			$product->description = $list_product['description'];
	// 		}
	// 	}
		
	// 	$promo = new Promo();
	// 	$condition = " WHERE \"stock_id\" = '".$product->stock_id."' ";
	// 	$lists_barang = $promo::getFreeSQL($condition, 'Barang');
		
	// 	$promo_code = '';
	// 	$count = 0;
	// 	if($lists_barang){
	// 		foreach($lists_barang as $list_barang){
	// 			if($count > 0){ $promo_code .= ","; }
	// 			$promo_code .= "'".$list_barang['kode_promosi']."'";
	// 			$count++;
	// 		}
	// 	}
		
	// 	$lists_promo = null;
	// 	if($promo_code != ''){
	// 		$condition = " WHERE \"kode_promosi\" IN (".$promo_code.") AND '".date('Y-m-d')."' BETWEEN \"from_\" AND \"to_\" ";
	// 		$lists_promo = $promo::getFreeSQL($condition, 'Promo');
	// 	}
		
	// 	$promo_code = '';
	// 	$count = 0;
	// 	if(count($lists_promo) > 0){
	// 		foreach($lists_promo as $list_promo){
	// 			if($count > 0){ $promo_code .= ","; }
	// 			$promo_code .= "'".$list_promo['kode_promosi']."'";
	// 			$count++;
	// 		}
	// 	}
		
	// 	$lists_promo_barang = null;
	// 	$lists_promo_hadiah = null;
		
	// 	if($promo_code != ''){
	// 		$condition = " WHERE \"kode_promosi\" IN (".$promo_code.") ";
	// 		$lists_promo_barang = $promo::getFreeSQL($condition, 'Barang');
			
	// 		$condition = " WHERE p.\"kode_promosi\" IN (".$promo_code.") ";
	// 		$lists_promojoin_barang = $promo::getJoin_PrBrg($condition);
			
	// 		$condition = " WHERE \"kode_promosi\" IN (".$promo_code.") ";
	// 		$lists_promo_hadiah = $promo::getFreeSQL($condition, 'Hadiah');
			
	// 		$condition = " WHERE p.\"kode_promosi\" IN (".$promo_code.") ";
	// 		$lists_promojoin_hadiah = $promo::getJoin_PrHdh($condition);
	// 	}
		
	// 	$barang_code = '';
	// 	$count = 0;
	// 	if(count($lists_promo_barang) > 0){
	// 		foreach($lists_promo_barang as $list_promo_barang){
	// 			if($count > 0){ $barang_code .= ","; }
	// 			$barang_code .= "'".$list_promo_barang['stock_id']."'";
	// 			$count++;
	// 		}
	// 	}
		
	// 	if(isset($lists_promojoin_barang)){
	// 		foreach($lists_promojoin_barang as $list_promojoin_barang){
	// 			if($count > 0){ $barang_code .= ","; }
	// 			$barang_code .= "'".$list_promojoin_barang['stock_id']."'";
	// 			$count++;
	// 		}
	// 	}
		
	// 	if(isset($lists_promojoin_hadiah)){
	// 		foreach($lists_promojoin_hadiah as $list_promojoin_hadiah){
	// 			if($count > 0){ $barang_code .= ","; }
	// 			$barang_code .= "'".$list_promojoin_hadiah['stock_id']."'";
	// 			$count++;
	// 		}
	// 	}
		
	// 	if(count($lists_promo_hadiah) > 0){
	// 		foreach($lists_promo_hadiah as $list_promo_hadiah){
	// 			if($count > 0){ $barang_code .= ","; }
	// 			$barang_code .= "'".$list_promo_hadiah['stock_id']."'";
	// 			$count++;
	// 		}
	// 	}
		
	// 	if($barang_code != ''){
	// 		$condition = " WHERE \"stock_id\" IN (".$barang_code.") ";
	// 		$lists_desc_barang = $product::getFreeSQL($condition);
	// 	}

	// 	$nama_barang = "";
	// 	$data = null;
	// 	$count = 0;
	// 	if(count($lists_promo) > 0){
			
	// 		foreach($lists_promo as $list_promo){
	// 			$data[$count]['promo_code'] = $list_promo['kode_promosi'];
	// 			$data[$count]['description'] = "";
				
	// 			if(strtoupper($list_promo['jenis_promo']) == "DISKON"){			
	// 				$data[$count]['description'] .= "Beli ";
	// 				$tipe = $list_promo['jenis_promo'];
					
	// 				$count_detail = 0;
	// 				foreach($lists_promojoin_barang as $list_promo_barang){
	// 					//$tipe = $list_promo_barang['jenis_promo'];
						
	// 					if($list_promo_barang['kode_promosi'] == $list_promo['kode_promosi']){
	// 						$nama_barang = "";
	// 						foreach($lists_desc_barang as $list_desc_barang){
	// 							if($list_promo_barang['stock_id'] == $list_desc_barang['stock_id']){
	// 								$nama_barang = $list_desc_barang['description'];
	// 							}
	// 						}
							
	// 						if($count_detail > 0) { $data[$count]['description'] .= "<br/> Beli "; }
	// 						if($list_promo_barang['discount_max'] != 0){
	// 							$data[$count]['description'] .= $list_promo_barang['qty']." ".$nama_barang." Dapat Diskon ".$list_promo_barang['discount_max']." % ";
	// 						}else{
	// 							$data[$count]['description'] .= $list_promo_barang['qty']." ".$nama_barang." Dapat Diskon Rp ".$list_promo_barang['discount_amount']." ";
	// 						}
							
	// 						$count_detail++;
	// 					}
	// 				}
					
	// 				$data[$count]['tipe'] = $tipe;
	// 			}
				
	// 			if(strtoupper($list_promo['jenis_promo']) == "PROMO PAKET"){			
	// 				$data[$count]['description'] .= "Beli ";
	// 				$tipe = $list_promo['jenis_promo'];
					
	// 				$count_detail = 0;
	// 				$total = 0;
	// 				$amount = 0;
					
	// 				foreach($lists_promojoin_barang as $list_promo_barang){
	// 					//$tipe = $list_promo_barang['jenis_promo'];
						
	// 					if($list_promo_barang['kode_promosi'] == $list_promo['kode_promosi']){
	// 						$amount = $list_promo_barang['amount'];
	// 						$total += $list_promo_barang['price'];
							
	// 						$nama_barang = "";
	// 						foreach($lists_desc_barang as $list_desc_barang){
	// 							if($list_promo_barang['stock_id'] == $list_desc_barang['stock_id']){
	// 								$nama_barang = $list_desc_barang['description'];
	// 							}
	// 						}
							
	// 						if($count_detail > 0) { $data[$count]['description'] .= ", "; }
	// 						if($list_promo_barang['discount_max'] != 0){
	// 							$data[$count]['description'] .= $list_promo_barang['qty']." ".$nama_barang."";
	// 						}else{
	// 							$data[$count]['description'] .= $list_promo_barang['qty']." ".$nama_barang."";
	// 						}
							
	// 						$count_detail++;
	// 					}
						
	// 					if($count_detail == count($lists_promojoin_barang)){
	// 						//$data[$count]['description'] .= " Hanya Rp ".$list_promo_barang['amount'];
	// 					}
						
	// 					$data[$count]['barcode'][$count_detail] = $list_promo_barang['uom1_internal_barcode'].'#'.$list_promo_barang['qty'];
	// 				}
					
	// 				$data[$count]['tipe'] = $tipe;
	// 				$data[$count]['amount'] = $amount;
	// 				$data[$count]['total_act'] = $total;
	// 				$data[$count]['description'] .= " Hanya Rp ".$list_promo_barang['amount'];
	// 			}

	// 			if(strtoupper($list_promo['jenis_promo']) == "BUY X GET Y"){
	// 				$data[$count]['description'] .= "Beli ";
	// 				$tipe = $list_promo['jenis_promo'];
					
	// 				$count_detail = 0;
	// 				foreach($lists_promojoin_barang as $list_promo_barang){
	// 					$qty_barang = $list_promo_barang['qty'];
	// 					$stid_barang = $list_promo_barang['stock_id'];
						
	// 					if($list_promo_barang['kode_promosi'] == $list_promo['kode_promosi']){
	// 						$nama_barang = "";
	// 						foreach($lists_desc_barang as $list_desc_barang){
	// 							if($list_promo_barang['stock_id'] == $list_desc_barang['stock_id']){
	// 								$nama_barang = $list_desc_barang['description'];
	// 							}
	// 						}
							
	// 						if($count_detail > 0) { $data[$count]['description'] .= ", "; }						
	// 						$data[$count]['description'] .= $list_promo_barang['qty']." ".$nama_barang." ";
	// 						$count_detail++;
	// 					}
	// 				}
					
	// 				$data[$count]['description'] .= "Dapat ";
					
	// 				$count_detail = 0;
	// 				foreach($lists_promojoin_hadiah as $list_promo_hadiah){
	// 					if($list_promo_hadiah['kode_promosi'] == $list_promo['kode_promosi']){
	// 						$qty_hadiah = $list_promo_hadiah['qty'];
	// 						$barcode_hdh = $list_promo_hadiah['uom1_internal_barcode'];
							
	// 						$nama_barang = "";
	// 						foreach($lists_desc_barang as $list_desc_barang){
	// 							if($list_promo_hadiah['stock_id'] == $list_desc_barang['stock_id']){
	// 								$nama_barang = $list_desc_barang['description'];
	// 							}
	// 						}
							
	// 						if($count_detail > 0) { $data[$count]['description'] .= ", "; }
	// 						$data[$count]['description'] .= $list_promo_hadiah['qty']." ".$nama_barang." ";
	// 						$count_detail++;
	// 					}
	// 				}
					
	// 				$data[$count]['tipe'] = $tipe;
	// 				$data[$count]['qty_hadiah'] = $qty_hadiah;
	// 				$data[$count]['barcode_hdh'] = $barcode_hdh;
	// 			}
				
	// 			$count++;
	// 		}
	// 	}
		
	// 	return json_encode($data);
	// }
	
	public function ajaxCheckMemberAction(){
		$cust = new Cust();
		$object['member'] = $_REQUEST['member'];
		$condition = " WHERE nomor_member = '".$object['member']."' ";
		$list_member = $cust::getFreeSQL($condition);		
		
		if(!isset($list_member)){			
			$member = new Member();
			$object['member'] = $_REQUEST['member'];
			$condition = " WHERE nomor_member = '".$object['member']."' ";
			$list_member = $member::getFreeSQL($condition);
			
			if(isset($list_member)){
				$list_member['tipe'] = 'member';
			}
		}else{
			$list_member['tipe'] = 'cust';
		}
		
		return json_encode($list_member);
	}
	
	public function sellAction()
	{
		$this->AuthorityAction();
		$this->AuthoritySoDAction();
		$promo = new Promo();
		$conditionDiskon = " WHERE id::varchar IN (SELECT promosi_id FROM m_promosi_diskon)";
		$this->view->data_diskon = $promo::getFreeSql($conditionDiskon);	

		$conditionXY = " WHERE id::integer IN (SELECT promosi_id FROM m_promo_x_get_y)";
		$this->view->xy = $promo::getFreeSql($conditionXY);
		
		$seShift = new StartEndShift();
		$condition = " WHERE \"tanggal\" ='".date("Y-m-d")."' AND \"user_id\" = '".$this->session->get("user")['user_name']."' ";
		$condition .= " AND \"flag_start\" = true AND \"flag_end\" = false ";
		$lists_seShift = $seShift::getFreeSQL($condition);
		
		foreach($lists_seShift as $list_seShift){
			$seShift->tanggal = $list_seShift['tanggal'];
			$seShift->jam = $list_seShift['jam'];
			$seShift->user_id = $list_seShift['user_id'];
			$seShift->kassa_id = $list_seShift['kassa_id'];
			$seShift->kassa_desc = $list_seShift['kassa_desc'];
			$seShift->shift_id = $list_seShift['shift_id'];
			$seShift->shift_desc = $list_seShift['shift_desc'];
			$seShift->setoran = $list_seShift['setoran'];
			$seShift->flag_start = $list_seShift['flag_start'];
			$seShift->flag_end = $list_seShift['flag_end'];
		}
		
		$this->view->seShift = $seShift;
		
		$tunda = new Tunda();
		$tunda->id_jual = $_REQUEST['id_jual'];
		$condition = " WHERE \"id_jual\" = '".$tunda->id_jual."' AND \"flag_delete\" = false ";
		$lists_tunda = $tunda::getFreeSQL($condition);
		
		foreach($lists_tunda as $list_tunda){
			$tunda->id_jual = $list_tunda['id_jual'];
			$tunda->id_member = $list_tunda['id_member'];
			$tunda->id_kasir = $list_tunda['id_kasir'];
			$tunda->tanggal = $list_tunda['tanggal'];
			$tunda->subtotal = $list_tunda['subtotal'];
			$tunda->tax = $list_tunda['tax'];
			$tunda->discount = $list_tunda['discount'];
			$tunda->total = $list_tunda['total'];
			$tunda->note = $list_tunda['note'];			
		}
		
		$this->view->tunda = $tunda;
		
		$tunda_detail = new TundaDetail();
		if(count($lists_tunda) > 0 ){
			$tunda_detail->id_jual = $_REQUEST['id_jual'];
			$condition = " WHERE \"id_jual\" = '".$tunda_detail->id_jual."' ";
			$lists_tunda_detail = $tunda_detail::getJoin_Product($condition);
		}
		$this->view->tunda_details = $lists_tunda_detail;		
		
		$retur = new Jual();
		$retur->id_jual = $_REQUEST['id_jual'];
		$condition = " WHERE \"id_jual\" = '".$retur->id_jual."' AND \"flag_delete\" = false ";
		$lists_retur = $retur::getFreeSQL($condition);
		
		foreach($lists_retur as $list_retur){
			$retur->id_jual = $list_retur['id_jual'];
			$retur->id_member = $list_retur['id_member'];
			$retur->id_kasir = $list_retur['id_kasir'];
			$retur->tanggal = $list_retur['tanggal'];
			$retur->subtotal = $list_retur['subtotal'];
			$retur->tax = $list_retur['tax'];
			$retur->discount = $list_retur['discount'];
			$retur->total = $list_retur['total'];
			$retur->note = $list_retur['note'];			
		}
		
		$this->view->retur = $retur;
		
		$retur_detail = new JualDetail();
		if(count($lists_retur) > 0 ){
			$retur_detail->id_jual = $_REQUEST['id_jual'];
			$condition = " WHERE \"id_jual\" = '".$retur_detail->id_jual."' ";
			$lists_retur_detail = $retur_detail::getJoin_Product($condition);
		}
		$this->view->retur_details = $lists_retur_detail;	
		
		$bank = new Bank();				
		$lists_bank = $bank::getAll($condition);	
		$this->view->lists_bank = $lists_bank;
	}
	
	public function insertAction()
	{						
		$start_end_shift = new StartEndShift();
		$condition = " WHERE \"tanggal\" ='".date("Y-m-d")."' AND \"user_id\" = '".$this->session->get("user")['user_name']."' ";
		$condition .= " AND \"flag_start\" = true AND \"flag_end\" = false ";
		$lists_seShift = $start_end_shift::getFreeSQL($condition);
		
		if($lists_seShift){
			foreach($lists_seShift as $list_seShift){
				$start_end_shift->id = $list_seShift['id'];
				$start_end_shift->tanggal = $list_seShift['tanggal'];
				$start_end_shift->user_id = $list_seShift['user_id'];
				$start_end_shift->kassa_id = $list_seShift['kassa_id'];
				$start_end_shift->kassa_desc = $list_seShift['kassa_desc'];
				$start_end_shift->shift_id = $list_seShift['shift_id'];
				$start_end_shift->shift_desc = $list_seShift['shift_desc'];
				$start_end_shift->setoran = $list_seShift['setoran'];
				$start_end_shift->flag_start = $list_seShift['flag_start'];			
				$start_end_shift->flag_end = $list_seShift['flag_end'];			
				$start_end_shift->cash_cashier = $list_seShift['cash_cashier'];			
			}
		}else{
			$data['type'] = 'E';
			$data['id'] = '';
			$data['title'] = 'Gagal';
			$data['message'] = 'Akun berbeda.';
			return json_encode($data);
		}
		
		$status = $this->request->getPost("status");
		$tunai = $this->request->getPost("tunai");
		$total_sales = $this->request->getPost("total");
		$donasi = $this->request->getPost("donasi");
		
		$newtotal = str_replace('.', '', $total_sales);
		$newtotal = str_replace(',00', '', $newtotal);
		
		if($status == "HOLD"){
			$number_range = new NumberRange();
			$number_range->trans_type = 'CH';
			$number_range->period = date('Ym');
			$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
			$lists = $number_range::getFreeSQL($condition);
			
			if(isset($lists)){
				foreach($lists as $list){
					$number_range->trans_type = $list['trans_type'];
					$number_range->period = $list['period'];
					$number_range->prefix = $list['prefix'];
					$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
				}
			}else{
				$number_range->trans_type = 'CH';
				$number_range->period = date('Ym');
				$number_range->prefix = 'CH';
				$number_range->from_ = '1';
				$number_range->to_ = '9999';
				$number_range->current_no = '1';
				$success = $number_range::goInsert($number_range);			
				$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
			}
			
			$tmuk = new TMUK();
			$lists_tmuk = $tmuk::getAll();
			
			if(isset($lists_tmuk)){
				foreach($lists_tmuk as $list_tmuk){
					$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
					$tmuk->tmuk = $list_tmuk['tmuk'];
					$tmuk->supplier_id = $list_tmuk['supplier_id'];
				}
			}
			
			$tunda = new Tunda();
			$tunda->id_jual = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;
			$tunda->id_member = $this->request->getPost("member_id");
			$tunda->id_kasir = $this->session->get("user")['user_name'];
			$tunda->tanggal = date("Y-m-d");			
			$tunda->subtotal = $this->request->getPost("subtotal");
			$tunda->subtotal = str_replace('.', '', $tunda->subtotal);
			$tunda->subtotal = str_replace(',', '.', $tunda->subtotal);			
			$tunda->tax = $this->request->getPost("tax");
			$tunda->tax = str_replace('.', '', $tunda->tax);
			$tunda->tax = str_replace(',', '.', $tunda->tax);			
			$tunda->discount = $this->request->getPost("discount");
			$tunda->discount = str_replace('.', '', $tunda->discount);
			$tunda->discount = str_replace(',', '.', $tunda->discount);
			$tunda->total = $this->request->getPost("total");
			$tunda->total = str_replace('.', '', $tunda->total);
			$tunda->total = str_replace(',', '.', $tunda->total);
			$tunda->note = $this->request->getPost("note");
			$success = $tunda::goInsert($tunda);
			
			if($success){				
				$success_num = $number_range::goAddNumber($number_range);
				
				$json_datas = json_decode($_POST['json_data']);	
				$json_bayar_datas = json_decode($_POST['json_data_bayar']);	
				foreach($json_datas as $json_data){
					$tunda_detail = new TundaDetail();
					$tunda_detail->id_jual = $tunda->id_jual;
					$tunda_detail->barcode = $json_data[0];
					$tunda_detail->item_code = $json_data[1];
					$tunda_detail->description = $json_data[2];
					$tunda_detail->harga = $json_data[3];
					$tunda_detail->harga = str_replace('.', '', $tunda_detail->harga);
					$tunda_detail->harga = str_replace(',', '.', $tunda_detail->harga);					
					$tunda_detail->qty = $json_data[5];
					$tunda_detail->satuan = $json_data[6];
					$tunda_detail->discount = $json_data[7];
					$tunda_detail->total = $json_data[8];
					$tunda_detail->total = str_replace('.', '', $tunda_detail->total);
					$tunda_detail->total = str_replace(',', '.', $tunda_detail->total);
					$tunda_detail->disc_percent = $json_data[10];
					$tunda_detail->disc_amount = $json_data[11];
					$tunda_detail->tax_type = $json_data[12];
					$tunda_detail->harga_member = $json_data[13];
					$tunda_detail->harga_hpp3 = $json_data[14];
					$success = $tunda_detail::goInsert($tunda_detail);
				}
				
				if( $this->request->getPost("id_jual") != ''){
					$tunda = new Tunda();
					$tunda->id_jual =  $this->request->getPost("id_jual");
					$tunda->flag_delete = true;
					$success = $tunda::goUpdate($tunda);
				}
				
				$data['type'] = 'S';
				$data['id'] = $tunda->id_jual;
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['id'] = '';
				$data['title'] = 'Gagal';
				$data['message'] = 'Proses Tunda Penjualan Gagal';
				return json_encode($data);
			}
		}elseif($status == "SUBMIT"){
			// get config point
			$point = new Point();
			$condition = " WHERE NOW() BETWEEN \"start_date\" AND \"end_date\" ";
			$lists_point = $point::getFreeSQL($condition);
			
			if(isset($lists_point)){
				foreach($lists_point as $list_point){
					$point->id = $list_point['id'];
					$point->point_get = $list_point['point_get'];
					$point->point_use = $list_point['point_use'];					
				}
			}
			
			// get data member
			$member_id = $this->request->getPost("member_id");
			$member = new Member();
			
			$condition = " WHERE \"nomor_member\" = '".$member_id."' ";
			$lists_member = $member::getFreeSQL($condition);
			$data_member = null;
			$data_member_man = null;
			if(isset($lists_member)){
				foreach($lists_member as $list_member){
					$data_member = $list_member;
				}
			}
			
			if(isset($data_member)){
				$member->branch_code = $data_member['branch_code'];
				$member->debtor_no = $data_member['debtor_no'];
				$lists_member_man = $member::getFirstMemberManagement($member);
				
				if(isset($lists_member_man)){
					foreach($lists_member_man as $list_member){
						$data_member_man = $list_member;
					}
				}
			}

			// var_dump($data_member_man);
			// die;
			
			// add validasi point & piutang, 14.06.2017 (Fajar)
			$point_bayar = 0;
			$piutang_bayar = 0;
			$point_value = 0;
			$json_bayar_datas = json_decode($_POST['json_data_bayar']);	
			if(isset($json_bayar_datas)){
				foreach($json_bayar_datas as $json_bayar_data){
					if($json_bayar_data[3] != '0' && $json_bayar_data[0] == 'POINT'){
						$point_bayar = (float)$json_bayar_data[3];					
					}
					
					if($json_bayar_data[3] != '0' && $json_bayar_data[0] == 'PIUTANG'){
						$piutang_bayar = (float)$json_bayar_data[3];
					}
				}
			}

			$curl = new CurlClass();
			$result = $curl->post($this->di['api']['link']."/api/point/get-point");
			$h_poin = json_decode($result, true);
			$selisih=0;
			$reedem = 0;
			foreach ($h_poin['data'] as $data) {
				$tanggal1 = new DateTime($data['tgl_berlaku']);
				$tanggal2 = new DateTime();
				if($tanggal2 >= $tanggal1){
					$jarak = $tanggal2->diff($tanggal1)->format("%a");
					if($selisih==0 AND $jarak==0){
						$reedem = (int) $data['faktor_reedem'];
					}else{
						if($selisih==0 AND $jarak>0){
							$selisih=$jarak;
							$reedem = (int) $data['faktor_reedem'];
						}else{
							if($jarak<=$selisih){
								$selisih=$jarak;
								$reedem = (int) $data['faktor_reedem'];
							}
						}
					}
				}
			}
			
			$point_value = $reedem>0?((ceil((int)$point_bayar / (int)$reedem )) * -1):0;			

			if ($member_id == '' && $point_bayar > 0){
				$data['type'] = 'E';
				$data['id'] = '';
				$data['title'] = 'Gagal';
				$data['message'] = 'Point hanya bisa digunakan untuk member!';
				return json_encode($data);
			}
			
			if ($member_id == '' && $piutang_bayar > 0){
				$data['type'] = 'E';
				$data['id'] = '';
				$data['title'] = 'Gagal';
				$data['message'] = 'Piutang hanya bisa digunakan untuk Member.';
				return json_encode($data);
			}
			
			if( $point_bayar > 0 && $point_value == 0){
				$data['type'] = 'E';
				$data['id'] = '';
				$data['title'] = 'Gagal';
				$data['message'] = 'Konfigurasi Point tidak ditemukan!';
				return json_encode($data);
			}

			if( $point_bayar > 0 && $member_id!=''){
				// $flag = false;
				$tpoint = new TPoint();
				$tlists_poin = $tpoint::countPoin();
				$poinmilik=0;
				foreach( $tlists_poin as $lists_poin ) {
					if($list_poin['id_member'] == $member_id){
						$poinmilik = $lists_poin['point'];
					}
				}
				// if(!isset($data_member_man)) { $flag = true; }
				if($poinmilik < $point_bayar) {					
					$data['type'] = 'E';
					$data['id'] = '';
					$data['title'] = 'Gagal';
					$data['message'] = 'Sisa point kurang!';
					return json_encode($data);
				}
			}
			
			// if( $point_value > 0){
			// 	$flag = false;
			// 	if(!isset($data_member_man)) { $flag = true; }
			// 	if($data_member_man['point'] < $point_value) { $flag = true; }
			// 	if($flag){					
			// 		$data['type'] = 'E';
			// 		$data['id'] = '';
			// 		$data['title'] = 'Gagal';
			// 		$data['message'] = 'Sisa point kurang!';
			// 		return json_encode($data);
			// 	}
			// }
			
			if( $piutang_bayar > 0){
				$flag = false;
				if(!isset($data_member_man)) { $flag = true; }
				if(($data_member_man['credit_limit'] - $data_member_man['total_credit'] + $data_member_man['payment']) < $piutang_bayar) { $flag = true; }
				if($flag){
					$data['type'] = 'E';
					$data['id'] = '';
					$data['title'] = 'Gagal';
					$data['message'] = 'Piutang melebihi Limit Kredit yang ditentukan.';
					return json_encode($data);
				}
			}
			// end validasi point & piutang
			
			$array_sn = array();
			$izone = new Izone();
			$lists_izone = $izone::getAll();
			
			$izone_activation_id ='';
			$izone_terminal_id = '';
			$izone_pin = '';
			$izone_first_name = ''; 
			
			if(isset($lists_izone)){
				foreach($lists_izone as $list_izone){
					$izone_activation_id = $list_izone['activation_id'];
					$izone_terminal_id = $list_izone['terminal_id'];
					$izone_pin = $list_izone['pin'];
					$izone_first_name = $list_izone['first_name'];
				}
			}
			
			$json_datas = json_decode($_POST['json_data']);	
			if(isset($json_datas)){
				foreach($json_datas as $json_data){
					if($json_data[15] != 'X'){
						// dd($json_data);
						if($izone_activation_id != '' AND $izone_terminal_id != '' AND $izone_pin != ''){
							$data_pulsa = explode(' ', $json_data[2]);
							
							$cn = '';
							$cond = " WHERE upper(provider_name) = '".strtoupper($data_pulsa[1])."' LIMIT 1 ";
							$lists_cn = Provider::getFreeSQL($cond);
							
							if($lists_cn)
								$cn = $lists_cn[0]['card_number']; 
// dd($cn);
							
							/*if($data_pulsa[0] == "TELKOMSEL"){ $cn = 110; } else 
							if($data_pulsa[0] == "INDOSAT"){ $cn = 101; } else 
							if($data_pulsa[0] == "THREE"){ $cn = 189; } else 
							if($data_pulsa[0] == "XL-AXIS"){ $cn = 111; } else 
							if($data_pulsa[0] == "SMARTFREN"){ $cn = 134; }*/
							
							$time = time();
							$traceNo = 1;
							settype($traceNo, "integer");
							$p = array();
							$p[] = new PhpXmlRpc\Value(
								array(
									"TransactionDateTime" => new Value($time, 'dateTime.iso8601'),
									"ProcessingCode" => new Value("700000"),
									"TransactionID" => new Value("00001"),
										"Destination" => new Value($json_data[15]), // Nomor HP
										"Voucher Type" => new Value("R"),
										"Amount" => new Value(str_replace('.', '', $data_pulsa[2])),
										"PIN" => new Value($izone_pin),
										"FirstName" => new Value($izone_first_name),
										"MerchantType" => new Value("6012"),
										"TerminalID" => new Value($izone_terminal_id),
										"MerchantID" => new Value("6012"),
										"CardName" => new Value($data_pulsa[1]),
										"CardNumber" => new Value($cn),
										"Signature" => new Value("Dedy"),
									),
								"struct"
							);
							
							$req = new Request('iGateway.topup', $p);
							$client = new Client('http://localhost:8080/pcclient/client');
							$response = $client->send($req);

							$client->setDebug(2);
							// check response for errors, and take appropriate action
							if(!$response->faultCode()){
								$encoder = new PhpXmlRpc\Encoder();
								$value = $encoder->decode($response->value());
								
								if(htmlspecialchars($value["ResponseCode"]) == 0){									
									array_push($array_sn, htmlspecialchars($value["VoucherSN"]));
								}else{
									$data['type'] = 'E';
									$data['title'] = 'Gagal';
									$data['message'] = "Pengisian <b>".$json_data[2]."</b><br/> ke nomor <b>".$json_data[15]."</b> gagal.<br/><p><font style='font-size:10px;font-style:italic'>Response Code : ".htmlspecialchars($value["ResponseCode"])."</font></p>";
									return json_encode($data);
								}
							}
						}else{
							$data['type'] = 'E';
							$data['id'] = '';
							$data['title'] = 'Gagal';
							$data['message'] = 'Akun Izone belum terdaftar.';
							return json_encode($data);
						}
					}
				}
			}

			$number_range = new NumberRange();
			$number_range->trans_type = 'CT';
			$number_range->period = date('Ym');
			$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
			$lists = $number_range::getFreeSQL($condition);
			
			if(isset($lists)){
				foreach($lists as $list){
					$number_range->trans_type = $list['trans_type'];
					$number_range->period = $list['period'];
					$number_range->prefix = $list['prefix'];
					$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
				}
			}else{
				$number_range->trans_type = 'CT';
				$number_range->period = date('Ym');
				$number_range->prefix = 'CT';
				$number_range->from_ = '1';
				$number_range->to_ = '9999';
				$number_range->current_no = '1';
				$success = $number_range::goInsert($number_range);			
				$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
			}
			
			$tmuk = new TMUK();
			$lists_tmuk = $tmuk::getAll();
			
			if(isset($lists_tmuk)){
				foreach($lists_tmuk as $list_tmuk){
					$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
					$tmuk->tmuk = $list_tmuk['tmuk'];
					$tmuk->supplier_id = $list_tmuk['supplier_id'];
				}
			}
			
			$number_move = new NumberRange();
			$number_move->trans_type = 'MV';
			$number_move->period = date('Ym');
			$condition = " WHERE \"trans_type\" = '".$number_move->trans_type."' AND \"period\" = '".$number_move->period."' ";
			$lists_move = $number_move::getFreeSQL($condition);
			
			if(isset($lists_move)){
				foreach($lists_move as $list_move){
					$number_move->trans_type = $list_move['trans_type'];
					$number_move->period = $list_move['period'];
					$number_move->prefix = $list_move['prefix'];
					$number_move->current_no = str_pad($list_move['current_no'], 4, "0", STR_PAD_LEFT);
				}
			}else{
				$number_move->trans_type = 'MV';
				$number_move->period = date('Ym');
				$number_move->prefix = 'MV';
				$number_move->from_ = '1';
				$number_move->to_ = '9999';
				$number_move->current_no = '1';
				$success = $number_move::goInsert($number_move);			
				$number_move->current_no = str_pad($number_move->current_no, 4, "0", STR_PAD_LEFT);
			}
			
			// get config point
			/*$point = new Point();
			$condition = " WHERE NOW() BETWEEN \"start_date\" AND \"end_date\" ";
			$lists_point = $point::getFreeSQL($condition);
			
			if(isset($lists_point)){
				foreach($lists_point as $list_point){
					$point->id = $list_point['id'];
					$point->point_get = $list_point['point_get'];
					$point->point_use = $list_point['point_use'];					
				}
			}*/
			
			$jual = new Jual();
			$jual->id_jual = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;
			$jual->id_member = $this->request->getPost("member_id");
			$jual->id_kasir = $this->session->get("user")['user_name'];
			$jual->tanggal = date("Y-m-d H:i:s");
			$jual->delivery_date = $this->request->getPost("delivery_date");
			$jual->subtotal = $this->request->getPost("subtotal");
			$jual->subtotal = str_replace('.', '', $jual->subtotal);
			$jual->subtotal = str_replace(',', '.', $jual->subtotal);
			$jual->tax = $this->request->getPost("tax");
			$jual->tax = str_replace('.', '', $jual->tax);
			$jual->tax = str_replace(',', '.', $jual->tax);
			$jual->discount = $this->request->getPost("discount");
			$jual->discount = str_replace('.', '', $jual->discount);
			$jual->discount = str_replace(',', '.', $jual->discount);
			$jual->total = $this->request->getPost("total");
			$jual->total = str_replace('.', '', $jual->total);
			$jual->total = str_replace(',', '.', $jual->total);
			$jual->kembalian = $this->request->getPost("kembalian");
			$jual->kembalian = str_replace('.', '', $jual->kembalian);
			$jual->kembalian = str_replace(',', '.', $jual->kembalian);
			$jual->note = $this->request->getPost("note");
			$jual->kassa_id = $start_end_shift->kassa_id;
			$jual->shift_id = $start_end_shift->shift_id;
			$jual->id_reference = $this->request->getPost("id_reference");
			$success = $jual::goInsert($jual);
			
			if($success){
				$success_num = $number_range::goAddNumber($number_range);
				
				$json_datas = json_decode($_POST['json_data']);		
				if(isset($json_datas)){
					foreach($json_datas as $json_data){
						
						$jual_detail = new JualDetail();
						$jual_detail->id_jual = $jual->id_jual;
						$jual_detail->barcode = $json_data[0];
						$jual_detail->item_code = $json_data[1];
						if($json_data[15] != "X"){
							$sn = array_pop($array_sn);
							$jual_detail->description = $json_data[2].'-'.$json_data[15].'-'.$sn;
						}else{
							$jual_detail->description = $json_data[2];
						}
						$jual_detail->harga = $json_data[3];
						$jual_detail->harga = str_replace('.', '', $jual_detail->harga);
						$jual_detail->harga = str_replace(',', '.', $jual_detail->harga);
						$jual_detail->qty = $json_data[5];
						$jual_detail->satuan = $json_data[6];
						$jual_detail->discount = $json_data[7];
						$jual_detail->total = $json_data[8];
						$jual_detail->total = str_replace('.', '', $jual_detail->total);
						$jual_detail->total = str_replace(',', '.', $jual_detail->total);
						$jual_detail->disc_percent = $json_data[10];
						$jual_detail->disc_amount = $json_data[11];
						$jual_detail->tax_type = $json_data[12];
						$jual_detail->harga_member = $json_data[13];
						$jual_detail->harga_hpp3 = $json_data[14];
						
						$success = $jual_detail::goInsert($jual_detail);
						
					}
					
					$count = 0;
					$rg_stock_id = '';
					foreach($json_datas as $json_data){
						if($count>0){ $rg_stock_id .= ','; }
						$rg_stock_id .= "'".$json_data[1]."'";
						$count++;
					}
					
					$lists_product = null;
					if(isset($rg_stock_id)){
						$product = new Product();
						$condition = " WHERE \"stock_id\" IN (".$rg_stock_id.") ";
						$lists_product = $product::getFreeSQL($condition);
					}

					foreach($json_datas as $json_data){
						$stock_qty = 0;
						$sell_qty = $json_data[5];
						$stock_con = 1;
						$sell_con = 1;
						if(isset($lists_product)){
							foreach($lists_product as $list_product){
								if($list_product['stock_id'] == $json_data[1]){
									if($list_product['uom1_htype_1'] && $list_product['uom1_nm'] == $json_data[6]) { $sell_con = $list_product['uom1_conversion']; }
									if($list_product['uom2_htype_1'] && $list_product['uom2_nm'] == $json_data[6]) { $sell_con = $list_product['uom2_conversion']; }
									if($list_product['uom3_htype_1'] && $list_product['uom3_nm'] == $json_data[6]) { $sell_con = $list_product['uom3_conversion']; }
									if($list_product['uom4_htype_1'] && $list_product['uom4_nm'] == $json_data[6]) { $sell_con = $list_product['uom4_conversion']; }
									
									if($list_product['uom1_htype_5']) { $stock_con = $list_product['uom1_conversion']; }
									if($list_product['uom2_htype_5']) { $stock_con = $list_product['uom2_conversion']; }
									if($list_product['uom3_htype_5']) { $stock_con = $list_product['uom3_conversion']; }
									if($list_product['uom4_htype_5']) { $stock_con = $list_product['uom4_conversion']; }
									
									$stock_qty = ( $sell_qty * $sell_con ) / $stock_con; 
								}
							}
						}
						
						$product_move = new ProductMove();
						$product_move->trans_no = $number_move->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_move->current_no;
						$product_move->stock_id = trim($json_data[1]);
						$product_move->type = 'CT';
						$product_move->price = ($jual->id_member != '' ? $json_data[13] : $json_data[3]);
						$product_move->reference = $jual->id_jual;
						$product_move->qty = ($stock_qty * -1);
						$product_move->discount_percent = $json_data[7];
						$product_move->standard_cost = $json_data[14] / 1.03;
						$success = $product_move::goInsert($product_move);
					}
					
					$success_num = $number_move::goAddNumber($number_move);
				}
				
				// add point
				if(isset($jual->id_member) && $jual->id_member != ''){
					$tpoint = new TPoint();
					$tpoint->id_member = $jual->id_member;
					$tpoint->id_ref = $jual->id_jual;
					// $tpoint->point = $point->point_get > 0 ? (int)((int)$jual->total / (int)$point->point_get ) : 0;
					
					$curl = new CurlClass();
					$result = $curl->post($this->di['api']['link']."/api/point/get-point");
					$h_poin = json_decode($result, true);
					$selisih=0;
					$kelipatan = 0;
					$konversi = 0.00;
					$bayar_poin = false;
					$bayar_hutang = false;
					$minuspoin = 0;
					$hutangplus = 0;
					$json_bayar_method = json_decode($_POST['json_data_bayar']);	
					if($json_bayar_method){
						foreach($json_bayar_method as $json_bayar_data){
							if(isset($json_bayar_data[3])){
								if($json_bayar_data[0] == 'POINT'){
									$bayar_poin = true;
									$minuspoin = $json_bayar_data[3] * -1;
									break;
								}
							}
						}
					}
					$json_bayar_method = json_decode($_POST['json_data_bayar']);	
					if($json_bayar_method){
						foreach($json_bayar_method as $json_bayar_data){
							if(isset($json_bayar_data[3])){
								if($json_bayar_data[0] == 'PIUTANG'){
									$bayar_hutang = true;
									$hutangplus = $json_bayar_data[3];
									break;
								}
							}
						}
					}
					if($bayar_poin==false){
						foreach ($h_poin['data'] as $data) {
							$tanggal1 = new DateTime($data['tgl_berlaku']);
							$tanggal2 = new DateTime();
							if($tanggal2 >= $tanggal1){
								$jarak = $tanggal2->diff($tanggal1)->format("%a");
								if($selisih==0 AND $jarak==0){
									$kelipatan = $data['konversi'];
									$konversi = $data['faktor_konversi'];
								}else{
									if($selisih==0 AND $jarak>0){
										$selisih=$jarak;
										$kelipatan = $data['konversi'];
										$konversi = $data['faktor_konversi'];
									}else{
										if($jarak<=$selisih){
											$selisih=$jarak;
											$kelipatan = $data['konversi'];
											$konversi = $data['faktor_konversi'];
										}
									}
								}
							}
						}
						$x_kelipatan = (int)((int)$jual->total / (int) $kelipatan );
						
						$tpoint->point = (int)(($x_kelipatan * $kelipatan) * ($konversi * 0.01 ));
					}else{
						$tpoint->point = 0;
						try {
							$tmuk = new TMUK();
							$lists_tmuk = $tmuk::getAll();
							$tmuk_kode='';
							if(isset($lists_tmuk)){
								foreach($lists_tmuk as $list_tmuk){
									$tmuk_kode = $list_tmuk['tmuk'];
								}
							}
							$member = new Member();
							$condition = "WHERE nomor_member = '".$jual->id_member."'";
							$nama_member = $member::getNamaMember($condition);
							$debtor = $member::getDebtorMember($condition);
							$curl = new CurlClass();
							$result = $curl->post($this->di['api']['link']."/api/member/send-poin", [
								'nomor_member' => $jual->id_member,
								'nama_member' => $nama_member,
								'tmuk_kode' => $debtor,
								'poin' => $minuspoin,
								'tanggal' => $jual->tanggal,
							]);
							$obj = json_decode($result, true);
							// var_dump($obj);
							// die;
						} catch (Exception $e) {
							
						}
					}

					if($bayar_hutang==true){
						try {
							$tmuk = new TMUK();
							$lists_tmuk = $tmuk::getAll();
							$tmuk_kode='';
							if(isset($lists_tmuk)){
								foreach($lists_tmuk as $list_tmuk){
									$tmuk_kode = $list_tmuk['tmuk'];
								}
							}
							$member = new Member();
							$condition = "WHERE nomor_member = '".$jual->id_member."'";
							$nama_member = $member::getNamaMember($condition);
							$debtor = $member::getDebtorMember($condition);
							$curl = new CurlClass();
							$result = $curl->post($this->di['api']['link']."/api/member/send-hutang", [
								'nomor_member' => $jual->id_member,
								'nama_member' => $nama_member,
								'tmuk_kode' => $debtor,
								'piutang' => $hutangplus,
								'tanggal' => $jual->tanggal,
							]);
							$obj = json_decode($result, true);
							// var_dump($obj);
							// die;
						} catch (Exception $e) {
							
						}
					}
					
					$tpoint->create_by = $this->session->get("user")['user_name'];
					$tpoint->create_date = date('Y-m-d');
					$success = $tpoint::goInsert($tpoint);
					if($tpoint->point!=0){
						try {
							$tmuk = new TMUK();
							$lists_tmuk = $tmuk::getAll();
							$tmuk_kode='';
							if(isset($lists_tmuk)){
								foreach($lists_tmuk as $list_tmuk){
									$tmuk_kode = $list_tmuk['tmuk'];
								}
							}
							$member = new Member();
							$condition = "WHERE nomor_member = '".$jual->id_member."'";
							$nama_member = $member::getNamaMember($condition);
							$debtor = $member::getDebtorMember($condition);
							$curl = new CurlClass();
							$result = $curl->post($this->di['api']['link']."/api/member/send-poin", [
								'nomor_member' => $jual->id_member,
								'nama_member' => $nama_member,
								'tmuk_kode' => $debtor,
								'poin' => $tpoint->point,
								'tanggal' => $jual->tanggal,

							]);
							$obj = json_decode($result, true);
							// var_dump($obj);
							// die;
						} catch (Exception $e) {
							
						}
					}
				}

				if($tunai > 0){
					$jual_bayar = new JualBayar();
					$jual_bayar->id_jual = $jual->id_jual;
					$jual_bayar->tipe = "TUNAI";
					$jual_bayar->total = $tunai;
					$jual_bayar->total = str_replace('.', '', $jual_bayar->total);
					$jual_bayar->total = str_replace(',', '', $jual_bayar->total);
					$success = $jual_bayar::goInsert($jual_bayar);
				}
				
				if($donasi > 0){
					$jual_bayar = new JualBayar();
					$jual_bayar->id_jual = $jual->id_jual;
					$jual_bayar->tipe = "DONASI";
					$jual_bayar->total = $donasi;
					$jual_bayar->total = str_replace('.', '', $jual_bayar->total);
					$jual_bayar->total = str_replace(',', '.', $jual_bayar->total);
					$success = $jual_bayar::goInsert($jual_bayar);
				}
				
				$json_bayar_datas = json_decode($_POST['json_data_bayar']);	
				if($json_bayar_datas){
					foreach($json_bayar_datas as $json_bayar_data){
						if(isset($json_bayar_data[3])){
							if(isset($json_bayar_data[1])){
								$bank = explode("-", $json_bayar_data[1]);
								$jual_bayar->bank_id = $bank[0];
								$jual_bayar->bank_name = $bank[1];
							}
							
							$jual_bayar = new JualBayar();
							$jual_bayar->id_jual = $jual->id_jual;
							$jual_bayar->tipe = $json_bayar_data[0];
							$jual_bayar->no_kartu = $json_bayar_data[2];
							$jual_bayar->total = $json_bayar_data[3];
							$jual_bayar->total = str_replace('.', '', $jual_bayar->total);
							$jual_bayar->total = str_replace(',', '.', $jual_bayar->total);
							$success = $jual_bayar::goInsert($jual_bayar);
							
							// minus point
							if($jual_bayar->tipe == 'POINT'){
								$tpoint = new TPoint();
								$tpoint->id_member = $jual->id_member;
								$tpoint->id_ref = $jual->id_jual;
								$tpoint->point = $reedem>0?((ceil((int)$jual_bayar->total / (int)$reedem )) * -1):0;
								$tpoint->create_by = $this->session->get("user")['user_name'];
								$tpoint->create_date = date('Y-m-d');
								$success = $tpoint::goInsert($tpoint);

								// // try {
								// 	$tmuk = new TMUK();
								// 	$lists_tmuk = $tmuk::getAll();
								// 	$tmuk_kode='';
								// 	if(isset($lists_tmuk)){
								// 		foreach($lists_tmuk as $list_tmuk){
								// 			$tmuk_kode = $list_tmuk['tmuk'];
								// 		}
								// 	}
								// 	$member = new Member();
								// 	$condition = "WHERE nomor_member = '".$jual->id_member."'";
								// 	$nama_member = $member::getNamaMember($condition);
								// 	$curl = new CurslClass();
								// 	$result = $curl->post($this->di['api']['link']."/api/member/send-poin", [
								// 		'nomor_member' => $jual->id_member,
								// 		'nama_member' => $nama_member,
								// 		'tmuk_kode' => $tmuk_kode,
								// 		'poin' => $tpoint->point,
								// 	]);
								// 	$obj = json_decode($result, true);
								// 	var_dump($obj);
								// 	die;
								// // } catch (Exception $e) {
									
								// // }
							}
						}
					}
				}
				
				if($this->request->getPost("id_jual") != ''){
					$tunda = new Tunda();
					$tunda->id_jual =  $this->request->getPost("id_jual");
					$tunda->flag_delete = true;
					$success = $tunda::goUpdate($tunda);
				}
				
				if($this->request->getPost("id_reference") != ''){
					$retur = new Jual();
					$retur->id_jual =  $this->request->getPost("id_reference");
					$retur->flag_delete = true;
					$success = $retur::goUpdate($retur);
					
					$product_move = new ProductMove();
					$product_move->reference = $retur->id_jual;
					$success = $product_move::goVoidRef($product_move);
				}
				
				$data['type'] = 'S';
				$data['id'] = $jual->id_jual;
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['id'] = '';
				$data['title'] = 'Gagal';
				$data['message'] = 'Proses Penjualan Gagal';
				return json_encode($data);
			}
		}
		
		$data['type'] = 'E';
		$data['id'] = '';
		$data['title'] = 'Gagal';
		$data['message'] = 'Proses Penjualan Gagal';
		return json_encode($data);
	}
	
	public function endJualAction(){
		$cek_tunda = new Tunda();
		$cek = " WHERE \"tanggal\" ='".date("Y-m-d")."' AND flag_delete = false ";
		$tunda = $cek_tunda::getFreeSQL($cek);
		
		if(count($tunda)> 0){
			$data['type'] = 'E';
			$data['message'] = 'Mohon Selesaikan Penjualan yang Tertunda';
			return json_encode($data);
			$success = false;
		}else{
			$id_jual_sum = "";
			$date_SES = "";
			
			$end_shift = new StartEndShift();
			$conditionSES = " WHERE user_id = '".$this->request->getPost("kasir_id")."' AND flag_end = false LIMIT 1 ";
			$listsSES = $end_shift::getFreeSQL($conditionSES);
			
			if(count($listsSES) > 0){
				foreach($listsSES as $listSES){
					$date_SES = $listSES['tanggal'];
				}				
			}
			
			if($date_SES == ""){ return false; }
			
			$end_shift->id = $this->request->getPost("id");
			$end_shift->cash_cashier = $this->request->getPost("cash_cashier");
			$end_shift->cash_cashier = str_replace(",", "", $this->request->getPost("cash_cashier"));
			$end_shift->flag_end = true;
			$success = $end_shift::goUpdate($end_shift);
			
			$condition = " WHERE tanggal = '".$date_SES."' AND kassa_id = ".$this->request->getPost("kassa_id"). " AND shift_id = ".$this->request->getPost("shift_id")."
			AND id_kasir = '".$this->request->getPost("kasir_id")."' AND flag_sum = false AND flag_delete = false GROUP BY id_kasir, tanggal, kassa_id, shift_id ";

			$conditionJB = " WHERE tanggal = '".$date_SES."' AND kassa_id = ".$this->request->getPost("kassa_id"). " AND shift_id = ".$this->request->getPost("shift_id")."
			AND id_kasir = '".$this->request->getPost("kasir_id")."' AND flag_sum = false AND flag_delete = false GROUP BY id_kasir, tanggal, kassa_id, shift_id, tipe, bank_name ";

			$conditionJD = " WHERE tanggal = '".$date_SES."' AND kassa_id = ".$this->request->getPost("kassa_id"). " AND shift_id = ".$this->request->getPost("shift_id")."
			AND id_kasir = '".$this->request->getPost("kasir_id")."' AND flag_sum = false AND flag_delete = false GROUP BY id_kasir, tanggal, kassa_id, shift_id, tjd.item_code, tjd.description, tjd.harga, tjd.harga_member, tjd.harga_hpp3 ";
			
			$listJual = Jual::summarize($condition);
			$listJualBayar = JualBayar::summarize($conditionJB);
			$listJualDetail = JualDetail::summarize($conditionJD);
			
			$jual = new Jual();
			$jual->id_kasir = $this->request->getPost("kasir_id");
			$jual->tanggal = $date_SES;
			$jual->kassa_id = $this->request->getPost("kassa_id");
			$jual->shift_id = $this->request->getPost("shift_id");
			$jual->flag_sum = true;
			$success = $jual::goUpdate_flagSum($jual);
			
			// Summarize Jual
			if($listJual){
				foreach($listJual as $listj){			
					$listJS = new JualSum();
					$listJS->id_kasir = $listj['id_kasir'];
					$listJS->tanggal = $listj['tanggal'];
					$listJS->kassa_id = $listj['kassa_id'];
					$listJS->shift_id = $listj['shift_id'];
					$listJS->subtotal = $listj['subtotal_sum'];
					$listJS->tax = $listj['tax_sum'];
					$listJS->discount = $listj['discount_sum'];
					$listJS->total = $listj['total_sum'];
					$listJS->kembalian = $listj['kembalian_sum'];
					$listJS->kas_akhir = $end_shift->cash_cashier;

					$success = $listJS::goInsert($listJS);
				}}

				$condition = " WHERE tanggal = '".$date_SES."' AND id_kasir = '".$this->request->getPost("kasir_id")."' AND kassa_id =  ".$this->request->getPost("kassa_id")." AND shift_id = ".$this->request->getPost("shift_id")." ORDER BY id DESC LIMIT 1 ";
				$jualsum = JualSum::getFreeSQL($condition);

				if($jualsum){
					foreach($jualsum as $jsum){
						$id_jual_sum = $jsum['id'];
					}}

			// Summarize Jual Bayar
					if($listJualBayar){
						foreach($listJualBayar as $listjb){
							$listJBS = new JualBayarSum();
							$listJBS->id_kasir = $listjb['id_kasir'];
							$listJBS->kassa_id = $listjb['kassa_id'];
							$listJBS->shift_id = $listjb['shift_id'];
							$listJBS->tanggal = $listjb['tanggal'];
							$listJBS->tipe = $listjb['tipe'];
							$listJBS->total = $listjb['total_sum'];
							$listJBS->flag_sync = 'f';
							$listJBS->id_jual_sum = $id_jual_sum;
							$listJBS->bank_name = $listjb['bank_name'];

							$success = $listJBS::goInsert($listJBS);
						}}

			// Summarize Jual Detail
						if($listJualDetail){
							foreach($listJualDetail as $listjd){
								$listJDS = new JualDetailSum();
								$listJDS->item_code = $listjd['item_code'];
								$listJDS->description = $listjd['description'];
								$listJDS->tanggal = $listjd['tanggal'];
								$listJDS->id_kasir = $listjd['id_kasir'];
								$listJDS->kassa_id = $listjd['kassa_id'];
								$listJDS->shift_id = $listjd['shift_id'];
								$listJDS->harga = $listjd['harga'];
								$listJDS->harga_member = $listjd['harga_member'];
								$listJDS->qty = $listjd['qty_sum'];
								$listJDS->discount = $listjd['discount_sum'];
								$listJDS->total = $listjd['total_sum'];
								$listJDS->flag_sync = 'f';
								$listJDS->id_jual_sum = $id_jual_sum;
								$listJDS->harga_hpp3 = $listjd['harga_hpp3'];

								$success = $listJDS::goInsert($listJDS);
							}}

							$success = true;
							if($success){
								$data['type'] = 'S';
								$data['message'] = 'Proses End of Shift Berhasil';
								$notif = new Notifikasi();
								$notif->data = htmlspecialchars($data['message']);
								$notif->read_at = date('Y-m-d H:i:s');
								$notif->created_at = date('Y-m-d H:i:s');
								$notif->created_by = $this->session->get('user')['real_name'];
								$success = $notif::goInsert($notif);				
								return json_encode($data);
							}else{
								$data['type'] = 'E';
								$data['message'] = 'Proses End of Shift Gagal';
								return json_encode($data);			
							}
						}
					}

					public function endAction(){
						$this->AuthorityAction();		
						$end_shift = new StartEndShift();
						$condition = " WHERE \"user_id\" = '".$this->session->get("user")['user_name']."' ";
						$condition .= " AND \"flag_end\" = false LIMIT 1 ";
						$this->view->lists_eShift = $end_shift::getFreeSQL($condition);	
					}

					public function printAction(){
						$this->AuthorityAction();
					}

					public function printSellAction(){
						$this->AuthorityAction();
					}

					public function checkPromoAction(){
						$condition = " WHERE pb.kode_promosi = '".$_REQUEST['promo_code']."' ";
						$result = Promo::getJoin_PrBrg($condition);

						return json_encode($result);
					}

					public function display_MAction(){
						$device = "COM6";
						exec("mode $device BAUD=9600 PARITY=n DATA=8 STOP=1 xon=off octs=off rts=on to=on");
						$comport = fopen($device, "w");

						$str = str_pad('   SELAMAT DATANG   ', 20, ' ', STR_PAD_RIGHT);
						$str = substr($str, 0, 20);
						fputs($comport, $str);

						$str2 = str_pad('  ----------------  ', 20, ' ', STR_PAD_RIGHT);
						$str2 = substr($str2, 0, 20);
						fputs($comport, $str2);

						fclose($comport);
					}

					public function display_PAction(){
						$total = str_replace(",","",$_GET['total']);
						$total = str_replace(".","",$_GET['total']);

						$device = "COM6";
						exec("mode $device BAUD=9600 PARITY=n DATA=8 STOP=1 xon=off octs=off rts=on to=on");
						$comport = fopen($device, "w");

						$str = str_pad($_GET['qty'].' X '.$_GET['desc'], 20, ' ', STR_PAD_RIGHT);
						$str = substr($str, 0, 20);
						fputs($comport, $str);

						$str2 = str_pad(''.number_format(($_GET['qty'] * $_GET['price']),0,",",".").'  '.number_format($total,0,",","."), 20, ' ', STR_PAD_RIGHT);
						$str2 = substr($str2, 0, 20);
						fputs($comport, $str2);

						fclose($comport);
					}

					public function display_TAction(){
						$text = str_replace(",","",$_GET['total']);
						$text = str_replace(".","",$_GET['total']);

						$device = "COM6";
						exec("mode $device BAUD=9600 PARITY=n DATA=8 STOP=1 xon=off octs=off rts=on to=on");
						$comport = fopen($device, "w");

						$str = str_pad('  TOTAL PEMBAYARAN  ', 20, ' ', STR_PAD_RIGHT);
						$str = substr($str, 0, 20);
						fputs($comport, $str);

						$str2 = str_pad('     RP '.number_format($text,0,",","."), 20, ' ', STR_PAD_RIGHT);
						$str2 = substr($str2, 0, 20);
						fputs($comport, $str2);

						fclose($comport);
					}

					public function display_IAction(){
						$text_tunai = str_replace(",","",$_GET['tunai']);
						$text_tunai = str_replace(".","",$_GET['tunai']);

						$text_kembalian = str_replace(",","",$_GET['kembalian']);
						$text_kembalian = str_replace(".","",$_GET['kembalian']);

						$text_total = str_replace(",","",$_GET['total']);
						$text_total = str_replace(".","",$_GET['total']);

						$device = "COM6";
						exec("mode $device BAUD=9600 PARITY=n DATA=8 STOP=1 xon=off octs=off rts=on to=on");
						$comport = fopen($device, "w");

						$str = str_pad('RP '.number_format($text_total,0,",",".").'  RP '.number_format($text_tunai,0,",","."), 20, ' ', STR_PAD_RIGHT);
						$str = substr($str, 0, 20);
						fputs($comport, $str);

						$str2 = str_pad('KEMBALIAN  RP '.number_format($text_kembalian,0,",","."), 20, ' ', STR_PAD_RIGHT);
						$str2 = substr($str2, 0, 20);
						fputs($comport, $str2);

						sleep(5);

						$str = str_pad('    TERIMA KASIH    ', 20, ' ', STR_PAD_RIGHT);
						$str = substr($str, 0, 20);
						fputs($comport, $str);

						$str2 = str_pad('  ----------------  ', 20, ' ', STR_PAD_RIGHT);
						$str2 = substr($str2, 0, 20);
						fputs($comport, $str2);

						fclose($comport);
					}

					public function cetak_strukAction(){
						$device = "COM2";
						exec("mode $device BAUD=38400 PARITY=n DATA=8 STOP=1 xon=off octs=off rts=on to=on");
						$comport = fopen($device, "w");

						$total = 0; $count = 0; $dpp = 0; $ppn = 0; 
						$total_barang = 0; $discount = 0; $cash = 0; $sisa = 0;

						$condition = " WHERE id_jual = '".$_GET['id_jual']."' ";
						$listsJual = Jual::getFreeSQL($condition);
						$listsJualDetail = JualDetail::getFreeSQL($condition);
						$listsJualBayar = JualBayar::getFreeSQL($condition);
						$listsTMUK = TMUK::getAll();

						foreach($listsTMUK as $listtm){
							$tmuk_name = $listtm['name'];
							$tmuk_code = $listtm['tmuk'];
							$tmuk_address1 = $listtm['address1']; 
							$tmuk_address2 = $listtm['address2']; 
							$tmuk_address3 = $listtm['address3']; 
							$tmuk_telp = $listtm['telp']; 
							$length = $listtm['length_struk']; 
						}

						foreach($listsJual as $listj){
							$dpp = $listj['subtotal'];
							$ppn = $listj['tax'];
							$total = $listj['total'];
							$discount = $listj['discount'];			
							$kasir = $listj['id_kasir'];

							if($listj['id_member'] == ''){ $hrg = "harga"; }
							else{ $hrg = "harga_member"; }
						}

						$length_3 = round(0.062 * $length);
						$length_4 = round(0.083 * $length);		
						$length_5 = round(0.104 * $length);
						$length_8 = round(0.166 * $length);
						$length_9 = round(0.187 * $length);		
						$length_10 = round(0.208 * $length);
						$length_12 = round(0.250 * $length);		
						$length_13 = round(0.270 * $length);		
						$length_14 = round(0.291 * $length);		
						$length_16 = round(0.333 * $length);		
						$length_18 = round(0.375 * $length);		
						$length_19 = round(0.395 * $length);
						$length_20 = round(0.416 * $length);
						$length_22 = round(0.458 * $length);
						$length_24 = round(0.500 * $length);
						$length_25 = round(0.520 * $length);
						$length_30 = round(0.625 * $length);
						$length_32 = round(0.666 * $length);
						$length_48 = round(1 * $length);

						$str = print_center($tmuk_name, $length); $str = substr($str, 0, $length); fputs($comport, $str); 
						$str = print_center($tmuk_address1, $length); $str = substr($str, 0, $length); fputs($comport, $str);
						$str = print_center($tmuk_address2, $length); $str = substr($str, 0, $length); fputs($comport, $str);
						$str = print_center($tmuk_address3, $length); $str = substr($str, 0, $length); fputs($comport, $str);

						$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_space($length_3).print_left(date('d-m-Y H:i:s'), $length_19).print_space($length_4).print_right($kasir, $length_19).print_space($length_3); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '-'; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		foreach($listsJualDetail as $listjd){
			$total_barang++;
			$str = print_left($listjd['description'], $length); fputs($comport, $str);
			$str = print_space($length_10).print_right($listjd['qty'], $length_5).' x '.print_left(number_format($listjd[$hrg],0,",","."), $length_8).print_space($length_12).print_right(number_format($listjd['total'],0,",","."), $length_10); $str = substr($str, 0, $length); fputs($comport, $str);
		}
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '-'; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_left('TOTAL PENJUALAN ['.$total_barang.'/'.$total_barang.']', $length_25).print_space($length_9).':'.print_space($length_3).print_right(number_format(($total - $discount),0,",","."), $length_10).''; $str = substr($str, 0, $length); fputs($comport, $str);	
		$str = print_left('DISKON', $length_10).print_space($length_24).':'.print_space($length_3).print_right(number_format($discount,0,",","."), $length_10).''; $str = substr($str, 0, $length); fputs($comport, $str);
		
		foreach($listsJualBayar as $listjb){
			if($listjb['tipe'] == "DONASI"){ 
				$cash -= $listjb['total'];  
			}else{
				$cash += $listjb['total'];  
			}
			
			$sisa = $cash - $total;
			if($listjb['tipe'] == "PIUTANG"){ 
				$str = print_left("A/R ANGGOTA", $length_12).print_space($length_22).':'.print_space($length_3).print_right(number_format($listjb['total'],0,",","."), $length_10).''; 
			}else{
				$str = print_left($listjb['tipe'], $length_12).print_space($length_22).':'.print_space($length_3).print_right(number_format($listjb['total'],0,",","."), $length_10).'';
			}
			
			$str = substr($str, 0, $length); fputs($comport, $str);
		}

		$str = 'KEMBALIAN'.print_space($length_25).':'.print_space($length_3).print_right(number_format($sisa,0,",","."), $length_10).''; $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '-'; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = 	print_left('DPP : '.number_format($dpp,0,",","."), $length_13).print_space($length_3).
		print_left('PPN : '.number_format($ppn,0,",","."), $length_13).print_space($length_3).
		print_left('NON-PPN : '.number_format('0',0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);

		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '-'; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_center('THANK YOU FOR SHOPPING AT '.$tmuk_name, $length); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_center('imm@lottemart.co.id', $length); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_center('HOTLINE IMM TMUK : '.$tmuk_telp, $length); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_center('Transaction ID : '.$_GET['id_jual'], $length); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = "\x00\x1Bi\x00"; fputs($comport, $str);
		
		fclose($comport);
	}
	
	public function cetak_EOSAction(){
		$device = "COM2";
		exec("mode $device BAUD=38400 PARITY=n DATA=8 STOP=1 xon=off octs=off rts=on to=on");
		$comport = fopen($device, "w");
		
		$total = 0; $count = 0; $dpp = 0; $ppn = 0; $total_barang = 0; $net_sales = 0; $total_act = 0; $id_jual_sum = 0; 
		$tunai = 0; $donasi = 0; $debet = 0; $credit = 0; $voucher = 0; $piutang = 0; $cash_income = 0; $kembalian = 0; $rec_count = 0;		
		
		$listsTMUK = TMUK::getAll();
		$cond_SES = " WHERE id = '".$_GET['id']."' ";
		$listsSES = StartEndShift::getFreeSQL($cond_SES);
		
		if($listsTMUK){
			foreach($listsTMUK as $listtm){
				$tmuk_name = $listtm['name'];
				$tmuk_code = $listtm['tmuk'];
				$tmuk_address1 = $listtm['address1']; 
				$tmuk_address2 = $listtm['address2']; 
				$tmuk_address3 = $listtm['address3']; 
				$tmuk_telp = $listtm['telp']; 
				$owner = $listtm['owner']; 
				$length = $listtm['length_struk'];
			}
		}
		
		if($listsSES){
			foreach($listsSES as $listses){
				$shift_desc = $listses['shift_desc'];
				$kassa_desc = $listses['kassa_desc'];
				$tanggal = $listses['tanggal'];				
				$setoran = $listses['setoran'];
				$cash = $listses['cash_cashier'];
				
				$cond_Jual = " WHERE id_kasir = '".$listses['user_id']."' AND tanggal = '".$listses['tanggal']."' AND kassa_id = ".$listses['kassa_id']." AND shift_id = ".$listses['shift_id']." ";	
				$cond_JualSum = " WHERE id_kasir = '".$listses['user_id']."' AND tanggal = '".$listses['tanggal']."' AND kassa_id = ".$listses['kassa_id']." AND shift_id = ".$listses['shift_id']." GROUP BY id, id_kasir, tanggal, kassa_id, shift_id, tax, total, kembalian ORDER BY id DESC LIMIT 1 ";
				
				$listsJual = Jual::getCount($cond_Jual);
				$listsJualSum = JualSum::getFreeSQL($cond_JualSum);
				
				if($listsJual){
					$rec_count = $listsJual;
				}
				
				if($listsJualSum){
					foreach($listsJualSum as $listj){
						$id_jual_sum = $listj['id'];
						$ppn = $listj['tax'];
						$net_sales = $listj['total'];
						$kembalian = $listj['kembalian'];
						$cash_income = $net_sales + $ppn;
					}
				}
				
				$cond_JB = " WHERE id_kasir = '".$listses['user_id']."' AND tanggal = '".$listses['tanggal']."' AND kassa_id = ".$listses['kassa_id']." AND shift_id = ".$listses['shift_id']." AND id_jual_sum = ".$id_jual_sum." GROUP BY id, id_kasir, kassa_id, shift_id, tanggal, tipe, total ";
				$listsJualBayar = JualBayarSum::getFreeSQL($cond_JB);
				
				if($listsJualBayar){
					foreach($listsJualBayar as $listjb){
						if($listjb['tipe'] == 'TUNAI'){ $tunai = $listjb['total']; } 
						if($listjb['tipe'] == 'DONASI'){ $donasi = $listjb['total']; } 
						if($listjb['tipe'] == 'KREDIT'){ $credit = $listjb['total']; } 
						if($listjb['tipe'] == 'DEBIT'){ $debet = $listjb['total']; } 
						if($listjb['tipe'] == 'VOUCHER'){ $voucher = $listjb['total']; } 
						if($listjb['tipe'] == 'PIUTANG'){ $piutang = $listjb['total']; } 
						
						$total_act += $listjb['total'];
					}
				}
			}
		}
		
		$length_3 = round(0.062 * $length);
		$length_4 = round(0.083 * $length);		
		$length_5 = round(0.104 * $length);
		$length_8 = round(0.166 * $length);
		$length_9 = round(0.187 * $length);		
		$length_10 = round(0.208 * $length);
		$length_12 = round(0.250 * $length);		
		$length_13 = round(0.270 * $length);		
		$length_14 = round(0.291 * $length);		
		$length_16 = round(0.333 * $length);		
		$length_18 = round(0.375 * $length);		
		$length_19 = round(0.395 * $length);
		$length_20 = round(0.416 * $length);
		$length_24 = round(0.500 * $length);
		$length_25 = round(0.520 * $length);
		$length_30 = round(0.625 * $length);
		$length_32 = round(0.666 * $length);
		$length_48 = round(1 * $length);
		
		/*echo 'setoran#'.$setoran.'<br>';
		echo 'cash#'.$cash.'<br>';
		echo 'net sales#'.$net_sales.'<br>';
		echo 'donasi#'.$donasi.'<br>';
		echo 'kredit#'.$credit.'<br>';
		echo 'debit#'.$debet.'<br>';
		echo 'voucher#'.$voucher.'<br>';
		echo 'piutang#'.$piutang.'<br>';
		echo 'total actual#'.($tunai - $kembalian + $credit + $debet + $voucher).'<br>';
		echo 'variance#'.($cash - $net_sales).'<br>';
		echo 'rec count#'.$rec_count.'<br>';
		
		return false;*/
		
		$dataEOS = new EOS();
		$dataEOS->tmuk = $tmuk_code;
		$dataEOS->shift = $shift_desc;
		$dataEOS->kassa = $kassa_desc;
		$dataEOS->tanggal = $tanggal;
		$dataEOS->initial = $setoran;
		$dataEOS->cash_income = $cash;
		$dataEOS->net_sales = $net_sales;
		$dataEOS->donasi = $donasi;
		$dataEOS->kredit = $credit;
		$dataEOS->debit = $debet;
		$dataEOS->voucher = $voucher;
		$dataEOS->piutang = $piutang;
		$dataEOS->total_actual = $tunai - $kembalian;
		$dataEOS->variance = ($cash - $net_sales);
		$dataEOS->rec_count = $rec_count;
		$success = $dataEOS->goInsert($dataEOS);
		
		$str = print_center('END SHIFT'); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);

		$str = print_left('Shift       : '.$shift_desc, $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_left('Kasir       : '.$kassa_desc, $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_left('Store Code  : '.$tmuk_code, $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_left('Store Name  : '.$tmuk_name, $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_left('Date        : '.date('d-m-Y', strtotime($tanggal)), $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);

		$str = print_left('SALES', $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '-'; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_right('Initial', $length_32).print_right(number_format($setoran,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('Cash Income', $length_32).print_right(number_format($cash,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('Net Sales', $length_32).print_right(number_format($net_sales,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('Net Return', $length_32).print_right(number_format('0',0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('PPN', $length_32).print_right(number_format('0',0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '-'; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_right('Total Sales', $length_32).print_right(number_format($net_sales,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '='; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_left('ACTUAL', $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '-'; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_right('Cash', $length_32).print_right(number_format($net_sales,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('Cash Income', $length_32).print_right(number_format($cash,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('Donasi', $length_32).print_right(number_format($donasi,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('Kredit', $length_32).print_right(number_format($credit,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('Debit', $length_32).print_right(number_format($debet,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('Voucher', $length_32).print_right(number_format($voucher,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_right('A/R Anggota', $length_32).print_right(number_format($piutang,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '-'; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_right('Total Actual', $length_32).print_right(number_format((($tunai - $kembalian) + $credit + $debet + $voucher),0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);	
		$str = print_right('Variance', $length_32).print_right(number_format(($cash - $net_sales),0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);	  	
		$str = print_right('Variance Paid', $length_32).print_right(number_format('0',0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);	
		$str = print_right('Receipt Count', $length_32).print_right(number_format($rec_count,0,",","."), $length_16); $str = substr($str, 0, $length); fputs($comport, $str);	
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '='; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_center('Mengetahui Store Leader'); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_center('(     '.$owner.'     )'); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '-'; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_center('Print Date : '.date('d-m-Y H:i:s')); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = "\x00\x1Bi\x00"; fputs($comport, $str);
		
		fclose($comport);
	}
	
	public function cetak_SOSAction(){
		$device = "COM2";
		exec("mode $device BAUD=38400 PARITY=n DATA=8 STOP=1 xon=off octs=off rts=on to=on");
		$comport = fopen($device, "w");
		
		$cond = " WHERE \"tanggal\" = '".date('Y-m-d')."' AND \"flag_end\" = false AND \"user_id\" = '".$_GET['username']."' LIMIT 1 ";
		$listsTMUK = TMUK::getAll();
		$listsSES = StartEndShift::getFreeSQL($cond);
		
		if($listsTMUK){
			foreach($listsTMUK as $listtm){
				$tmuk_name = $listtm['name'];
				$tmuk_code = $listtm['tmuk'];
				$tmuk_telp = $listtm['telp']; 
				$owner = $listtm['owner'];
				$length = $listtm['length_struk'];
			}
		}
		
		if($listsSES){
			foreach($listsSES as $listSED){
				$shift = $listSED['shift_desc'];			
				$pos = $listSED['kassa_desc'];			
				$kasir = $listSED['user_id'];			
				$tanggal = $listSED['tanggal'];			
				$setoran = $listSED['setoran'];			
			}
		}
		
		$length_3 = round(0.062 * $length);
		$length_4 = round(0.083 * $length);		
		$length_5 = round(0.104 * $length);
		$length_8 = round(0.166 * $length);
		$length_9 = round(0.187 * $length);		
		$length_10 = round(0.208 * $length);
		$length_12 = round(0.250 * $length);		
		$length_13 = round(0.270 * $length);		
		$length_14 = round(0.291 * $length);		
		$length_16 = round(0.333 * $length);		
		$length_18 = round(0.375 * $length);		
		$length_19 = round(0.395 * $length);
		$length_20 = round(0.416 * $length);
		$length_24 = round(0.500 * $length);
		$length_25 = round(0.520 * $length);
		$length_30 = round(0.625 * $length);
		$length_32 = round(0.666 * $length);
		$length_48 = round(1 * $length);
		
		/*echo $shift."<br/>";
		echo $pos."<br/>";
		echo $kasir."<br/>";
		echo $tmuk_code."<br/>";
		echo $tmuk_name."<br/>";
		echo $tanggal."<br/>";
		echo $setoran."<br/>";
		echo $owner."<br/>";*/
		
		$str = print_center('START OF SHIFT'); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_left('Shift       : '.$shift, $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_left('POS         : '.$pos, $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_left('Kasir       : '.$kasir, $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_left('Store Code  : '.$tmuk_code, $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_left('Store Name  : '.$tmuk_name, $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		$str = print_left('Date        : '.date('d-m-Y', strtotime($tanggal)), $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_left('SALES', $length_48); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '-'; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_right('Setoran Awal', $length_30).print_right(number_format($setoran,0,",","."), $length_18); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '='; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_center('Mengetahui Store Leader'); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= ' '; } // SPACE		
		$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_center('(     '.$owner.'     )'); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<$length;$i++){ $str .= '-'; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = print_center('Print Date : '.date('d-m-Y H:i:s')); $str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = ''; 
		for($i=0;$i<($length * 3);$i++){ $str .= ' '; }		
			$str = substr($str, 0, $length); fputs($comport, $str);
		
		$str = "\x00\x1Bi\x00"; fputs($comport, $str);
		
		fclose($comport);
	}
	
	public function checkConnectionAction(){
		$listsIzone = Izone::getAll();
		$izone_activation_id = '';
		$izone_terminal_id = '';
		$izone_pin = '';
		$izone_first_name = '';
		
		if($listsIzone){
			foreach($listsIzone as $list_izone){
				$izone_activation_id = $list_izone['activation_id'];
				$izone_terminal_id = $list_izone['terminal_id'];
				$izone_pin = $list_izone['pin']; 
				$izone_first_name = $list_izone['first_name']; 
			}
		}
		
		if($izone_activation_id != '' AND $izone_terminal_id != '' AND $izone_pin != ''){
			$connected = @fsockopen("www.google.com", 80); 
			
			if($connected){
				$is_conn = true; 
				fclose($connected);
			}else{
				$is_conn = false; 
			}
			
			if($is_conn){
				$time = time();
				$traceNo = 1;
				settype($traceNo, "integer");
				$p = array();
				$p[] = new PhpXmlRpc\Value(
					array("TransactionDateTime" => new Value($time, 'dateTime.iso8601')),
					"struct"
				);
				
				$req = new Request('iGateway.checkConnection', $p);
				$client = new Client('http://localhost:8080/pcclient/client');
				$response = $client->send($req);

				$client->setDebug(2);
				// check response for errors, and take appropriate action
				if(!$response->faultCode()){
					$encoder = new PhpXmlRpc\Encoder();
					$value = $encoder->decode($response->value());
					
					if(htmlspecialchars($value["ResponseCode"]) == 0){									
						$data['type'] = 'success';
						$data['title'] = 'Sukses';
						$data['message'] = "Koneksi ke Izone berhasil.";
						return json_encode($data);
					}else{
						$data['type'] = 'error';
						$data['title'] = 'Gagal';
						$data['message'] = "Koneksi ke Izone gagal.";
						return json_encode($data);
					}
				}
			}else{
				$data['type'] = 'error';
				$data['title'] = 'Gagal';
				$data['message'] = "PC POS tidak terhubung ke Internet.";
				return json_encode($data);
			}
		}else{
			$data['type'] = 'error';
			$data['title'] = 'Gagal';
			$data['message'] = 'Akun Izone belum terdaftar.';
			return json_encode($data);
		}
		
	}
}
