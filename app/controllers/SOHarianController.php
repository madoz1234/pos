<?php

use Phalcon\Mvc\Controller;

class SOHarianController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'SOHarian' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$so = new SO();
		$condition = " WHERE ( \"status\" <> 'DELETE' AND \"status\" <> 'COMPLETE'  ) AND \"so_type\" = 'DAY' ";
		$this->view->lists = $so::getFreeSQL($condition);
		
		$condition2 = " WHERE \"so_type\" = 'DAY' AND \"status\" = 'COMPLETE' ";
		$this->view->lists_history = $so::getFreeSQL($condition2);

	}
	
	public function ajaxSearchProductAction()
	{
		// get List Product
		$divisi = new Divisi();
		$condition = " WHERE \"bumun_cd\" = '".$_GET['divisi']."' ";
		$lists_divisi = $divisi::getFreeSQL($condition);
		foreach ($lists_divisi as $key => $value) {
			$bumun_cd = $value['bumun_cd'];
		}
		if($_GET['category'] != ''){ $p_category = explode('_',$_GET['category']); }

		$s_product = $_GET['query'];
		$s_product = strtoupper($s_product);
		$product = new Product();
		$condition = " WHERE mb_flag = 'B' AND ppob = 0 AND inactive = 0 AND status_flag != '2' AND ( upper(\"uom1_internal_barcode\") LIKE '%".$s_product."%' " 
					." OR upper(\"uom2_internal_barcode\") LIKE '%".$s_product."%' " 
					." OR upper(\"description\") LIKE '%".$s_product."%' ) ";
		$lists_product = $product::getFreeSQL($condition);
		$data_product = null;
		
		for($i = 0, $j = 0; $i < count($lists_product); $i++){
			if($lists_product[$i]['uom1_internal_barcode']){
				$data_product[$j]['val'] = $lists_product[$i]['uom1_internal_barcode'];
				$data_product[$j]['display'] = $lists_product[$i]['uom1_internal_barcode'].' - '.$lists_product[$i]['description'];	
				$j++;
			}else if($lists_product[$i]['uom1_internal_barcode']){
				$data_product[$j]['val'] = $lists_product[$i]['uom2_internal_barcode'];
				$data_product[$j]['display'] = $lists_product[$i]['uom2_internal_barcode'].' - '.$lists_product[$i]['description'];		
				$j++;				
			}else if($lists_product[$i]['uom1_internal_barcode']){
				$data_product[$j]['val'] = $lists_product[$i]['uom3_internal_barcode'];
				$data_product[$j]['display'] = $lists_product[$i]['uom3_internal_barcode'].' - '.$lists_product[$i]['description'];	
				$j++;
			}else if($lists_product[$i]['uom1_internal_barcode']){
				$data_product[$j]['val'] = $lists_product[$i]['uom4_internal_barcode'];
				$data_product[$j]['display'] = $lists_product[$i]['uom4_internal_barcode'].' - '.$lists_product[$i]['description'];		
				$j++;
			}
		}
		
		return json_encode($data_product);
	}
	
	public function ajaxProductAction(){
		$product = new Product();
		$object['r_product'] = $_REQUEST['r_product'];
		$object['count_index'] = $_REQUEST['count_index'];
		$data = $product::getProductAD($object);

		return json_encode($data);
	}
	
	public function addAction()
	{		
		$this->AuthorityAction();
		
		$divisi = new Divisi();
		$lists_divisi = $divisi::getAll();
		
		$data_divisi = null;
		$data_divisi[''] = '-- Pilih Divisi --';
		foreach($lists_divisi as $list_divisi){
			$data_divisi[$list_divisi['bumun_cd']] = $list_divisi['bumun_nm'];
		}
		
		$this->view->divisi = $data_divisi;
		
		$category = new Category();
		$lists_category = $category::getAll();
		
		$data_category = null;
		$data_category[''] = '-- Pilih Kategori --';
		foreach($lists_category as $list_category){
			$data_category[$list_category['l1_cd']] = $list_category['l1_nm'];
		}
		$this->view->category = $data_category;
	}
	
	public function editAction()
	{
		$this->AuthorityAction();
		
		// get data selection
		$divisi = new Divisi();
		$lists_divisi = $divisi::getAll();
		
		$data_divisi = null;
		$data_divisi[''] = '-- Pilih Divisi --';
		foreach($lists_divisi as $list_divisi){
			$data_divisi[$list_divisi['bumun_cd']] = $list_divisi['bumun_nm'];
		}		
		$this->view->divisi = $data_divisi;
		
		$data_category = null;
		$data_category[''] = '-- Pilih Kategori --';
		$this->view->category = $data_category;	
		
		// get data SO Harian
		$so = new SO();
		$so->so_id = $_REQUEST['SO_ID'];
		$lists_so = $so::getFirst($so);
		
		foreach($lists_so as $list_so){
			$so->so_id = $list_so['so_id'];
			$so->kode_verifikasi = $list_so['kode_verifikasi'];
			$so->user_id = $list_so['user_id'];
			$so->so_date = $list_so['so_date'];
			$so->status = $list_so['status'];
			$so->so_type = $list_so['so_type'];
		}
		
		$this->view->so = $so;
		
		$so_detail = new SODetail();
		$so_detail->so_id = $_REQUEST['SO_ID'];
		$condition = " WHERE \"so_id\" = '".$so_detail->so_id."' ";
		$lists_so_detail = $so_detail::getFreeSQL($condition);
		
		$this->view->so_details = $lists_so_detail;
	}
	
	public function ajaxCategoryAction()
	{
		// get List Category
		$category = new Category();
		$condition = " WHERE \"bumun_cd\" = '".$_GET['divisi']."' ";
		$lists_category = $category::getFreeSQL($condition);
		
		$data_category = null;
		$data_category[0]['val'] = '';
		$data_category[0]['display'] = '-- Pilih Kategori --';
		
		
		for($i = 0; $i < count($lists_category); $i++){
			$data_category[$i+1]['val'] = $lists_category[$i]['bumun_cd'].'_'.$lists_category[$i]['l1_cd'];
			$data_category[$i+1]['display'] = $lists_category[$i]['l1_nm'];			
		}
		
		return json_encode($data_category);
	}
	
	public function ajaxAddAction()
	{
		// get List Product
		// $p_divisi = $_GET['divisi'];		
		if($_GET['category'] != ''){ $p_category = explode('_',$_GET['category']); }				

		$divisi = $_GET['divisi'];
		$product = new Product();
		if($divisi != null && $category != null){
			$condition = " WHERE average_cost > 0 AND mb_flag = 'B' AND ppob = 0 AND inactive = 0 AND status_flag != '2' AND bumun_cd = '".$divisi."' AND l1_cd = '".$p_category[1]."' ";
			$lists_product = $product::getJoin_ProductPrice($condition);
		}

		if($divisi != null && $category == null){
			$condition = " WHERE average_cost > 0 AND mb_flag = 'B' AND ppob = 0 AND inactive = 0 AND status_flag != '2' AND bumun_cd  = '".$divisi."' ";
			$lists_product = $product::getJoin_ProductPrice($condition);
		}

		if($divisi == null && $category != null){
			$condition = " WHERE average_cost > 0 AND mb_flag = 'B' AND ppob = 0 AND status_flag != '2' AND inactive = 0 ";
			$lists_product = $product::getJoin_ProductPrice($condition);
		}

		$s_product = $_GET['query'];
		if($s_product){
			$s_product = strtoupper($s_product);
			$product = new Product();
			$condition = " WHERE mb_flag = 'B' AND ppob = 0 AND inactive = 0 AND status_flag != '2' AND ( upper(\"uom1_internal_barcode\") LIKE '%".$s_product."%' " 
						." OR upper(\"uom2_internal_barcode\") LIKE '%".$s_product."%' " 
						." OR upper(\"description\") LIKE '%".$s_product."%' ) ";
			$lists_product = $product::getJoin_ProductPrice($condition);
		}
		
		$data_product = null;
		for($i = 0, $j = 0; $i < count($lists_product); $i++){	
			if($lists_product[$i]['stock_id']){  
				$data_product[$j]['stock_id'] = $lists_product[$i]['stock_id'];
				$data_product[$j]['description'] = $lists_product[$i]['description'];
				$data_product[$j]['harga'] = round($lists_product[$i]['average_cost']);
				$data_product[$j]['barcode'] = $lists_product[$i]['uom1_internal_barcode'];
				$data_product[$j]['satuan'] = $lists_product[$i]['uom1_nm'];
				$j++;
			}
		}	
		
		return json_encode($data_product);
	}
	
	public function ajaxSOAction(){
		$so_detail = new SODetail();
		$condition = " WHERE \"so_id\" = '".$_GET['so_id']."' ";
		$data = $so_detail::getFreeSQL($condition);
		
		return json_encode($data);
	}
	
	public function insertAction()
	{
		ini_set('max_execution_time', 300);
		$number_range = new NumberRange();
		$number_range->trans_type = 'SO';
		$number_range->period = date('Ym');
		$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
		$lists = $number_range::getFreeSQL($condition);
		
		if(isset($lists)){
			foreach($lists as $list){
				$number_range->trans_type = $list['trans_type'];
				$number_range->period = $list['period'];
				$number_range->prefix = $list['prefix'];
				$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
			}
		}else{
			$number_range->trans_type = 'SO';
			$number_range->period = date('Ym');
			$number_range->prefix = 'SO';
			$number_range->from_ = '1';
			$number_range->to_ = '9999';
			$number_range->current_no = '1';
			$success = $number_range::goInsert($number_range);			
			$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
		}
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
			}
		}
		
		$so = new SO();
		$so->so_id = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;
		$so->kode_verifikasi = date("Ymd").(substr($so->so_id, 0, 2)).(substr($so->so_id, -3));
		$so->user_id = $this->request->getPost("user_id");
		$so->status = $this->request->getPost("status");
		$so->so_type = $this->request->getPost("so_type");
		$success = $so::goInsert($so);

		if($success){			
			$success_num = $number_range::goAddNumber($number_range);
			
			$json_datas = json_decode($_POST['json_data']);	
			foreach($json_datas as $json_data){
				$product_move = new ProductMove();
				$product_move->stock_id = $json_data[0];				
				
				$so_detail = new SODetail();
				$so_detail->so_id = $so->so_id;
				$so_detail->item_code = $json_data[0];
				$so_detail->barcode_code = $json_data[1];	
				$so_detail->description = $json_data[2];
				$so_detail->qty_tersedia = $product_move::getStock($product_move);
				$so_detail->unit_item = $json_data[3];
				$so_detail->unit_price = $json_data[4];		
				$success = $so_detail::goInsert($so_detail);
			}
			
			$data['type'] = 'S';
			$data['message'] = '<b>'.$so->so_id.'</b> Berhasil dibuat.';
			$notif = new Notifikasi();
			$notif->data = $data['message'];
			$notif->read_at = null;
			$notif->created_at = date('Y-m-d H:i:s');
			$notif->created_by = $this->request->getPost("user_id");

			$success = $notif::goInsert($notif);
			return json_encode($data);
		}else{					
			$data['type'] = 'E';
			$data['message'] = 'Gagal membuat SO Harian!';
			$notif = new Notifikasi();
			$notif->data = $data['message'];
			$notif->read_at = null;
			$notif->created_at = date('Y-m-d H:i:s');
			$notif->created_by = $this->request->getPost("user_id");

			$success = $notif::goInsert($notif);
			return json_encode($data);
		}
	}
	
	public function updateAction()
	{
		$so_detail_delete = new SODetail();
		$so_detail_delete->so_id = $this->request->getPost("so_id");
		$success = $so_detail_delete::goDeleteSO($so_detail_delete);
		
		$json_datas = json_decode($_POST['json_data']);	
		foreach($json_datas as $json_data){
			$product_move = new ProductMove();
			$product_move->stock_id = $json_data[0];
			
			$so_detail = new SODetail();
			$so_detail->so_id = $this->request->getPost("so_id");
			$so_detail->item_code = $json_data[0];
			$so_detail->barcode_code = $json_data[1];
			$so_detail->description = $json_data[2];
			$so_detail->qty_tersedia = $product_move::getStock($product_move);
			$so_detail->unit_item = $json_data[3];
			$so_detail->unit_price = $json_data[4];
			$so_detail->so_detail_item = $json_data[5];

			if($so_detail->so_detail_item == ''){
				$success = $so_detail::goInsert($so_detail);
			}else{
				$success = $so_detail::goInsertItem($so_detail);
			}				
		}
		
		$data['type'] = 'S';
		$data['message'] = '<b>'.$so_detail->so_id.'</b> Berhasil diubah.';
		return json_encode($data);	
	}
	
	public function deleteAction()
	{				
		$this->AuthorityAction();
		
		$so = new SO();
		$so->so_id = $_REQUEST['SO_ID'];
		$so->status = 'DELETE';
		$success = $so::goUpdate($so);		

		$this->response->redirect(uri('SOHarian'));
	}
	
	public function prosesAction()
	{				
		$this->AuthorityAction();
		
		// get data SO Harian
		$so = new SO();
		$so->so_id = $_REQUEST['SO_ID'];
		$lists_so = $so::getFirst($so);
		
		foreach($lists_so as $list_so){
			$so->so_id = $list_so['so_id'];
			$so->kode_verifikasi = $list_so['kode_verifikasi'];
			$so->user_id = $list_so['user_id'];
			$so->so_date = $list_so['so_date'];
			$so->status = $list_so['status'];
			$so->so_type = $list_so['so_type'];
		}
		
		$this->view->so = $so;
		
		$so_detail = new SODetail();
		$so_detail->so_id = $_REQUEST['SO_ID'];
		$condition = " WHERE \"so_id\" = '".$so_detail->so_id."' ";
		$lists_so_detail = $so_detail::getFreeSQL($condition);
		
		$this->view->so_details = $lists_so_detail;
	}

	public function saveProsesAction()
	{		
		ini_set('max_execution_time', 300);
		$so = new SO();
		$so->so_id = $this->request->getPost("so_id");
		$so->status = $this->request->getPost("status");
		$success = $so::goUpdate($so);
		
		if($success){
			$json_datas = json_decode($_POST['json_data']);	
			
			if($json_datas){
				foreach($json_datas as $json_data){
					$product_move = new ProductMove();
					$product_move->stock_id = $json_data[1];
				
					$so_detail = new SODetail();
					$so_detail->so_id = $this->request->getPost("so_id");			
					$so_detail->so_detail_item = $json_data[0];
					$so_detail->qty_tersedia = $product_move::getStock($product_move);
					$so_detail->qty_barcode = $json_data[4];
					$so_detail->flag_so_ok = $json_data[7];
					$success = $so_detail::goUpdate($so_detail);
				}
			}
		}
		
		$data['type'] = 'S';
		$data['message'] = '<b>'.$so->so_id.'</b> Berhasil diproses.';
		return json_encode($data);
	}
	
	public function postAction()
	{				
		$this->AuthorityAction();
		
		// get data SO Harian
		$so = new SO();
		$so->so_id = $_REQUEST['SO_ID'];
		$lists_so = $so::getFirst($so);
		
		foreach($lists_so as $list_so){
			$so->so_id = $list_so['so_id'];
			$so->kode_verifikasi = $list_so['kode_verifikasi'];
			$so->user_id = $list_so['user_id'];
			$so->so_date = $list_so['so_date'];
			$so->status = $list_so['status'];
			$so->so_type = $list_so['so_type'];
		}
		
		$this->view->so = $so;
		
		$so_detail = new SODetail();
		$so_detail->so_id = $_REQUEST['SO_ID'];
		$condition = " WHERE \"so_id\" = '".$so_detail->so_id."' ";
		$lists_so_detail = $so_detail::getFreeSQL($condition);
		
		$this->view->so_details = $lists_so_detail;
	}
	
	public function savePostAction()
	{		
		$so = new SO();
		$so->so_id = $this->request->getPost("so_id");
		$so->status = $this->request->getPost("status");
		$success = $so::goUpdate($so);
		
		if($success){
			$number_range = new NumberRange();
			$number_range->trans_type = 'MV';
			$number_range->period = date('Ym');
			$condition = " WHERE \"trans_type\" = '".$number_range->trans_type."' AND \"period\" = '".$number_range->period."' ";
			$lists = $number_range::getFreeSQL($condition);
			
			if(isset($lists)){
				foreach($lists as $list){
					$number_range->trans_type = $list['trans_type'];
					$number_range->period = $list['period'];
					$number_range->prefix = $list['prefix'];
					$number_range->current_no = str_pad($list['current_no'], 4, "0", STR_PAD_LEFT);
				}
			}else{
				$number_range->trans_type = 'MV';
				$number_range->period = date('Ym');
				$number_range->prefix = 'MV';
				$number_range->from_ = '1';
				$number_range->to_ = '9999';
				$number_range->current_no = '1';
				$success = $number_range::goInsert($number_range);			
				$number_range->current_no = str_pad($number_range->current_no, 4, "0", STR_PAD_LEFT);
			}
			
			$tmuk = new TMUK();
			$lists_tmuk = $tmuk::getALL();
			
			if(isset($lists_tmuk)){
				foreach($lists_tmuk as $list_tmuk){
					$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
					$tmuk->tmuk = $list_tmuk['tmuk'];
					$tmuk->supplier_id = $list_tmuk['supplier_id'];
				}
			}
			
			$so_detail = new SODetail();
			$condition = " WHERE \"so_id\" = '".$so->so_id."' ";
			$lists_so_detail = $so_detail::getFreeSQL($condition);
			
			$product_move = new ProductMove();
			
			$count = 0;			
			$query_insert = '';
			foreach($lists_so_detail as $list_so_detail){
				if($count >= 300){
					$success = $product_move::goMultiInsert($query_insert);
					$count = 0;					
					$query_insert = '';
				}
				
				if($list_so_detail['qty_barcode'] != $list_so_detail['qty_tersedia'] && $list_so_detail['flag_so_ok']){
					$product_move->trans_no = $number_range->prefix.'-'.date('Ymd').$tmuk->tmuk.$number_range->current_no;
					$product_move->stock_id = $list_so_detail['item_code'];
					$product_move->type = 'SO';
					$product_move->reference = $so->so_id;
					$product_move->qty = ( $list_so_detail['qty_barcode'] - $list_so_detail['qty_tersedia'] );
					$product_move->price = $list_so_detail['unit_price'];
					$product_move->tran_date = date("Y-m-d");

					if($count > 0) { $query_insert .= ","; }				
					$query_insert .= "('".$product_move->trans_no."','".$product_move->stock_id."','".$product_move->type."','','".$product_move->tran_date
									."','0','".$product_move->price."','".$product_move->reference."','".$product_move->qty."','0','0','0')";				
					$count++;						
				}
			}
			
			if($query_insert != ''){				
				$success = $product_move::goMultiInsert($query_insert);
				$count = 0;	
				$query_insert = '';
			}

			$success_num = $number_range::goAddNumber($number_range);
		}
		
		$data['type'] = 'S';
		$data['message'] = '<b>'.$so->so_id.'</b> Berhasil diposting.';
		$data['id'] = $so_detail->so_id;
		return json_encode($data);
	}
	
	public function excelAction(){ 
		$this->AuthorityAction();
		$so = new SO();
		$so->so_id = $_GET['SO_ID'];
		$lists_so = $so::getFirst($so);
		
		$list_so_view = null;
		if(isset($lists_so)){
			foreach($lists_so as $list_so){
				$list_so_view = $list_so;
			}
		}
		
		$this->view->so = $list_so_view;
		
		$so_detail = new SODetail();
		$condition = "WHERE so_id = '".$so->so_id."'";
		$lists_so_detail = $so_detail::getJoin($condition);
		
		$this->view->so_details = $lists_so_detail;
		$this->view->sales_SO = $so_detail::getSalesSO();
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
				$tmuk->saldo = $list_tmuk['saldo'];
				$tmuk->telp = $list_tmuk['telp'];
				$tmuk->name = $list_tmuk['name'];
				$tmuk->owner = $list_tmuk['owner'];
				$tmuk->address1 = $list_tmuk['address1'];
				$tmuk->address2 = $list_tmuk['address2'];
				$tmuk->address3 = $list_tmuk['address3'];				
			}
		}
		
		$this->view->tmuk = $tmuk; 
	}
	
	public function printAction(){
		$this->AuthorityAction();
		
		$so = new SO();
		$so->so_id = $_GET['SO_ID'];
		$lists_so = $so::getFirst($so);
		
		$list_so_view = null;
		if(isset($lists_so)){
			foreach($lists_so as $list_so){
				$list_so_view = $list_so;
			}
		}
		
		$this->view->so = $list_so_view;
		
		$so_detail = new SODetail();
		$condition = "WHERE so_id = '".$so->so_id."'";
		$lists_so_detail = $so_detail::getJoin($condition);
		$this->view->so_details = $lists_so_detail;		
		// $this->view->sales_SO = $so_detail::getSalesSO();
		
		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
				$tmuk->saldo = $list_tmuk['saldo'];
				$tmuk->telp = $list_tmuk['telp'];
				$tmuk->name = $list_tmuk['name'];
				$tmuk->owner = $list_tmuk['owner'];
				$tmuk->address1 = $list_tmuk['address1'];
				$tmuk->address2 = $list_tmuk['address2'];
				$tmuk->address3 = $list_tmuk['address3'];				
			}
		}
		
		$this->view->tmuk = $tmuk;
	}
	
	// public function exportexcelAction(){
	// 	// get data SO Harian
	// 	$so = new SO();
	// 	$so->so_id = $_REQUEST['SO_ID'];
	// 	$lists_so = $so::getFirst($so);
		
	// 	foreach($lists_so as $list_so){
	// 		$so->so_id = $list_so['so_id'];
	// 		$so->kode_verifikasi = $list_so['kode_verifikasi'];
	// 		$so->user_id = $list_so['user_id'];
	// 		$so->so_date = $list_so['so_date'];
	// 		$so->status = $list_so['status'];
	// 		$so->so_type = $list_so['so_type'];
	// 	}
		
	// 	$this->view->so = $so;
		
	// 	$so_detail = new SODetail();
	// 	$so_detail->so_id = $_REQUEST['SO_ID'];
	// 	$condition = " WHERE \"so_id\" = '".$so_detail->so_id."' ";
	// 	$lists_so_detail = $so_detail::getFreeSQL($condition);
		
	// 	$this->view->so_details = $lists_so_detail;
	// }

	public function exportexcelAction() {

		$this->AuthorityAction();
		$so = new SO();
		$so->so_id = $_GET['SO_ID'];
		$lists_so = $so::getFirst($so);
		
		$list_so_view = null;
		if(isset($lists_so)){
			foreach($lists_so as $list_so){
				$list_so_view = $list_so;
			}
		}

		function tgl_indonesia($date){
			$Hari = array ("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu",);
			$Bulan = array ("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
			$tahun     = substr($date, 0, 4);
			$bulan     = substr($date, 5, 2);
			$tgl    = substr($date, 8, 2);
			$waktu    = substr($date,11, 5);
			$hari    = date("w", strtotime($date));
			$result = $Hari[$hari].", ".$tgl." ".$Bulan[(int)$bulan-1]." ".$tahun." ".$waktu." ";
			return $result;
		}
				
		$so_detail = new SODetail();
		$condition = "WHERE so_id = '".$so->so_id."'";
		$lists_so_detail = $so_detail::getJoin($condition);

		$tmuk = new TMUK();
		$lists_tmuk = $tmuk::getALL();
		
		if(isset($lists_tmuk)){
			foreach($lists_tmuk as $list_tmuk){
				$tmuk->tmuk_id = $list_tmuk['tmuk_id'];
				$tmuk->tmuk = $list_tmuk['tmuk'];
				$tmuk->supplier_id = $list_tmuk['supplier_id'];
				$tmuk->saldo = $list_tmuk['saldo'];
				$tmuk->telp = $list_tmuk['telp'];
				$tmuk->name = $list_tmuk['name'];
				$tmuk->owner = $list_tmuk['owner'];
				$tmuk->address1 = $list_tmuk['address1'];
				$tmuk->address2 = $list_tmuk['address2'];
				$tmuk->address3 = $list_tmuk['address3'];				
			}
		}
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->setTitle($so->so_id);

		foreach(range('A1','F2') as $head)
		{
			$objPHPExcel->getActiveSheet()->getStyle($head)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
		}


		foreach(range('A4','F4') as $set)
		{
			$objPHPExcel->getActiveSheet()->getStyle($set)->getAlignment()->applyFromArray(
				array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'rotation'   => 0,
					'wrap'       => true
				)
			);
			$objPHPExcel->getActiveSheet()->getColumnDimension($set)->setAutoSize(true);
		}

		$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells('A1:M1')
		->mergeCells('A2:M2')
		->setCellValue('A1', 'SO HARIAN TMUK '.$tmuk->name)
		->setCellValue('A2', tgl_indonesia(date('Y-m-d')))
		->setCellValue('A4', 'SO Detail Item')
		->setCellValue('B4', 'SO ID')
		->setCellValue('C4', 'Kode Barang')
		->setCellValue('D4', 'Barcode')
		->setCellValue('E4', 'Nama Barang')
		->setCellValue('F4', 'Qty')
		->setCellValue('G4', 'Satuan');
		$i=5;
		foreach ($lists_so_detail as $key => $value) {
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, $value['so_detail_item'])
			->setCellValue('B'.$i, $value['so_id'])
			->setCellValue('C'.$i, $value['item_code'])
			->setCellValue('D'.$i, $value['barcode_code'])
			->setCellValue('E'.$i, $value['description'])
			->setCellValue('F'.$i, $value['qty_barcode'])
			->setCellValue('G'.$i, $value['unit_item']);
			$i++;
		}
		$fname = $so->so_id . ".xlsx";
		$temp_file = tempnam(sys_get_temp_dir(), 'phpexcel');

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($temp_file);

		$response = new \Phalcon\Http\Response();

		$response->setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		$response->setHeader('Content-Disposition', 'attachment;filename="' . $fname . '"');
		$response->setHeader('Cache-Control', 'max-age=0');

		$response->setHeader('Cache-Control', 'max-age=1');

		$response->setContent(file_get_contents($temp_file));

		unlink($temp_file);

		return $response;
	}

	
	public function importexcelAction(){
		$this->AuthorityAction();
		
		// get data SO Harian
		$so = new SO();
		$so->so_id = $_REQUEST['SO_ID'];
		$lists_so = $so::getFirst($so);
		
		foreach($lists_so as $list_so){
			$so->so_id = $list_so['so_id'];
			$so->kode_verifikasi = $list_so['kode_verifikasi'];
			$so->user_id = $list_so['user_id'];
			$so->so_date = $list_so['so_date'];
			$so->status = $list_so['status'];
			$so->so_type = $list_so['so_type'];
		}
		
		$this->view->so = $so;
	}
	
	public function saveImportAction(){
		if ($this->request->hasFiles($_REQUEST['id']) == true) {
			foreach ($this->request->getUploadedFiles($_REQUEST['id']) as $file) {
				$lampiran = $file;
				$filepath = curl_file_create( realpath($lampiran->getTempName()), $lampiran->getRealType(), $lampiran->getName() );
				$xfile = fopen($lampiran->getTempName(), 'r');
				$filename = $lampiran->getName();
			}
			$excelReader = PHPExcel_IOFactory::createReaderForFile($lampiran->getTempName());
			$excelObj = $excelReader->load($lampiran->getTempName());
			$worksheet = $excelObj->getSheet(0);
			$lastRow = $worksheet->getHighestRow();

			$upload = 0;
			$gagal = 0;
			$nomor = 2;

			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()
			->setCreator("Admin")
			->setTitle("Log Gagal Import")
			->setSubject("Template excel")
			->setDescription("")
			->setKeywords("Template excel");
			$objPHPExcel->setActiveSheetIndex(0);

			for ($row = 5; $row <= $lastRow; $row++) {
				try {
				$n = $worksheet->getCell('F'.$row)->getValue();
				$SOHarian = new SODetail();
				$SOHarian->so_detail_item	= $worksheet->getCell('A'.$row)->getValue();
				$SOHarian->so_id	= $worksheet->getCell('B'.$row)->getValue();
				$SOHarian->item_code	= $worksheet->getCell('C'.$row)->getValue();
				$SOHarian->barcode_code	= preg_replace('/[^A-Za-z0-9\-]/', '', pg_escape_string($worksheet->getCell('D'.$row)->getValue()));
				$SOHarian->description= $worksheet->getCell('E'.$row)->getValue();
				$SOHarian->qty_barcode	= $n<=0?0:$n;
				$SOHarian->unit_item	= $worksheet->getCell('G'.$row)->getValue();
				$success = $SOHarian::goUpdate2($SOHarian);
				if($success){
						$upload++;
					}
				} catch (Exception $e) {
					$gagal++;

					$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $worksheet->getCell('A'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $worksheet->getCell('B'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $worksheet->getCell('C'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $worksheet->getCell('D'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $worksheet->getCell('E'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $worksheet->getCell('F'.$row)->getValue());
					$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $worksheet->getCell('G'.$row)->getValue());
				}
			}
			$result['message'] = $gagal.' Gagal dan '.$upload.' Berhasil di Import!';
			if($gagal>0){
				$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$filename = 'log-import/'.date("YmdHis").'.xls';
				$writer->save($filename);
				$result['message'] = $gagal.' Gagal dan '.$upload.' Berhasil di Import! <br><a href="'.$filename.'">Download Log</a>';
			}
			$result['type'] = 'S';
			return json_encode($result);
		}else{
			$result['type'] = 'E';
			$result['message'] = 'Gagal Import Data!';
			return json_encode($result);
		}
		
	}
}
