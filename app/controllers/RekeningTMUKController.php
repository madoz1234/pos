<?php
set_time_limit(0);
use Phalcon\Mvc\Controller;

class RekeningTMUKController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'RekeningTMUK' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{		
		$this->AuthorityAction();
		$this->view->saldo = 0;
		$this->view->mengendap = 0;
		$this->view->locked = 0;
		$this->view->available = 0;
		
		$data = new TMUK();
		$lists = $data::getAll();
		
		if(count($lists) > 0){
			foreach($lists as $list){
				$tmuk = $list['tmuk']; //contoh. 600500001 1234567764
				$saldo = (new TMUKController())->getSaldo();	

				$curl = new CurlClass();
				$result = $curl->get($this->di['api']['link']."/api/tmuk/get-mengendap");
				$saldo_mengendap = json_decode($result, true);

				$cek = new CurlClass();
				$data = $cek->post($this->di['api']['link']."/api/tmuk/get-scn", [
					'tmuk' => $tmuk
				]);
				$scn = json_decode($data, true);

				$cek_locked = new CurlClass();
				$data_locked = $cek_locked->post($this->di['api']['link']."/api/tmuk/get-locked", [
					'tmuk' => $tmuk
				]);
				$locked = json_decode($data_locked, true);
				// dd($data_locked);
				$pr = new PR();
				// $condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
				// $lists_role_menu = $role_menu::getFreeSQL($condition);
				$this->view->saldo = $saldo;
				$this->view->myList = $lists;
				$this->view->locked = $locked['locked'];
				$this->view->mengendap = $saldo_mengendap['message'];
				$this->view->escrow = $scn['escrow'];
				
				
				if(isset($_POST['from']) || isset($_POST['to'])){
					$from = $_POST['from'];
					$to = $_POST['to'];
					// var_dump($from);
					// die;
					$b =  $this->di['api']['link']."mutasi.php?mac=".$mac."&from=".$from."&to=".$to."&tmuk=".$tmuk;
					$b = new CurlClass();
					$c = $b->post($this->di['api']['link']."/api/topup/get-topup", [
						'tmuk' => $tmuk,
						'from' => $from,
						'to' => $to
					]);

					$e = $this->di['api']['link']."mutasi.php?mac=".$mac."&from=".$from."&to=".$to."&tmuk=".$tmuk;
					$e = new CurlClass();
					$d = $e->post($this->di['api']['link']."/api/topup/get-topup-deposit", [
						'tmuk' => $tmuk,
						'from' => $from,
						'to' => $to
					]);
					
					$this->view->mutasi = $c;
					$this->view->deposit = $d;
				}else{
					$this->view->mutasi = null;
					$this->view->deposit = null;
				}
			}
			$available = ($saldo + $scn['escrow']) - $locked['locked'] - $saldo_mengendap['message'];
			$this->view->available = $available;
		}else{
			
		}	
	}
}
