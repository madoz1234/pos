<?php

use Phalcon\Mvc\Controller;

class ManajemenHargaController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'ManajemenHarga' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$data = new Product();
		$condition = " WHERE mp.stock_id = '".$_POST['stock_id']."' AND mp.status_flag !='2' ";
		$this->view->lists = $data::getJoin_ProductPrice($condition);
		
		$condition = "ORDER BY uom1_prod_nm ";
		$this->view->listProduct = $data::getFreeSQL($condition);
	}
	
	public function ajaxSearchProductAction(){
		$product = new Product();
		$query = strtoupper($_GET['query']);
	
		// $condition = " WHERE \"uom1_internal_barcode\" LIKE '%".$query."%' OR \"uom2_internal_barcode\" LIKE '%".$query."%' OR \"uom3_internal_barcode\" LIKE '%".$query."%' OR \"uom4_internal_barcode\" LIKE '%".$query."%' OR \"uom1_prod_nm\" LIKE '%".$query."%' AND mb_flag = 'B' AND inactive = 0 ORDER BY uom1_prod_nm";
		$lists_product = $product::getFreeSQL2($query);
		
		$data_product = null;			
		for($i = 0; $i < count($lists_product); $i++){
			/*$data_product[$i+1]['val'] = $lists_product[$i]['stock_id'];
			$data_product[$i+1]['display'] = $lists_product[$i]['uom1_prod_nm'];*/
			
			if($lists_product[$i]['uom1_internal_barcode'] != ''){
				$data_product[$i+1]['val'] = $lists_product[$i]['stock_id'];
				$data_product[$i+1]['display'] = $lists_product[$i]['uom1_internal_barcode'].' - '.$lists_product[$i]['uom1_prod_nm'];	
			}else if($lists_product[$i]['uom2_internal_barcode'] != ''){
				$data_product[$i+1]['val'] = $lists_product[$i]['stock_id'];
				$data_product[$i+1]['display'] = $lists_product[$i]['uom2_internal_barcode'].' - '.$lists_product[$i]['uom1_prod_nm'];	
			}else if($lists_product[$i]['uom3_internal_barcode'] != ''){
				$data_product[$i+1]['val'] = $lists_product[$i]['stock_id'];
				$data_product[$i+1]['display'] = $lists_product[$i]['uom3_internal_barcode'].' - '.$lists_product[$i]['uom1_prod_nm'];	
			}else{
				$data_product[$i+1]['val'] = $lists_product[$i]['stock_id'];
				$data_product[$i+1]['display'] = $lists_product[$i]['uom4_internal_barcode'].' - '.$lists_product[$i]['uom1_prod_nm'];	
			}
		}
		
		return json_encode($data_product);
	}
	
	public function ajaxProductAction(){
		$myArray = array();	
		$data = new Product();
		$condition = " WHERE mp.stock_id = '".$_GET['stock_id']."' AND status_flag !='2' ";
		$myArray = $data::getJoin_ProductPrice($condition);
		return json_encode($myArray);
	}
	
	public function updateAction()
	{	
		if($this->request->getPost("uom1_changed_price") != ''){
			$data = new ProductPrice();
			$data->stock_id = $this->request->getPost("stock_id");
			$change = $this->request->getPost("uom1_changed_price");
			$data->uom1_changed_price = preg_replace('/[^A-Za-z0-9\-]/', '', $change);
	        $data->uom2_changed_price = $this->request->getPost("uom2_changed_price");
	        $data->uom3_changed_price = $this->request->getPost("uom3_changed_price");
	        $data->uom4_changed_price = $this->request->getPost("uom4_changed_price");
			
			$data->uom1_member_price = $this->request->getPost("uom1_member_price");
	        $data->uom2_member_price = $this->request->getPost("uom2_member_price");
	        $data->uom3_member_price = $this->request->getPost("uom3_member_price");
	        $data->uom4_member_price = $this->request->getPost("uom4_member_price");
	        $data->tanggal_change = date('Y-m-d');
	        $data->flag_price = true;
			
			$cek = $data::goUpdate($data);

			if($cek){
				$tmuk = new TMUK();
				$lists = $tmuk::getAll();

				if(isset($lists)){
					foreach($lists as $list){
						$tmuk->tmuk_id = $list['tmuk_id'];
						$tmuk->tmuk = $list['tmuk'];
					}
				}

				$curl = new CurlClass();
				$result = $curl->post($this->di['api']['link']."/api/produk/send-change", [
					'tmuk' => $tmuk->tmuk,
					'stock_id' => $this->request->getPost("stock_id"),
					'change' => preg_replace('/[^A-Za-z0-9\-]/', '', $change)
				]);
			}

			if($result){
				$this->flashSession->success("Berhasil;Product <b>".$this->request->getPost("desc")."</b> berhasil diupdate.;success");
				return $this->response->redirect(uri('ManajemenHarga'));
			}else{
				$this->flashSession->success("Gagal;Product <b>".$this->request->getPost("desc")."</b> gagal diupdate.;error");
				return $this->response->redirect(uri('ManajemenHarga'));
			}	
		}else{
			$this->flashSession->success("Gagal;Mohon Inputkan Change Selling Price.;error");
			return $this->response->redirect(uri('ManajemenHarga'));
		}
	}

}
