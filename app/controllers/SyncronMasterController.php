<?php

use Phalcon\Mvc\Controller;

class SyncronMasterController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){	
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'SyncronMaster' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();	
		
		$last_sync = new LastSync();
		$condition = " ORDER BY sync_time DESC, sync_date DESC LIMIT 50 ";
		$this->view->lists = $last_sync::getFreeSQL($condition);
		
		$m_data = null;
		$m_data['ALL'] = 'SYNC ALL';
		$m_data['TMUK'] = 'SYNC TMUK';
		$m_data['SALDO'] = 'SYNC SALDO';
		$m_data['BANK'] = 'SYNC BANK';
		$m_data['PROMOSI'] = 'SYNC PROMOSI';
		$m_data['SUPPLIER'] = 'SYNC SUPPLIER';
		$m_data['DIVISI'] = 'SYNC DIVISI';
		$m_data['KATEGORI'] = 'SYNC KATEGORI';
		// $m_data['CUST'] = 'SYNC CUST';
		$m_data['KUSTOMER'] = 'SYNC KUSTOMER';
		$m_data['MEMBER'] = 'SYNC MEMBER';
		$m_data['POINT'] = 'SYNC POINT MEMBER';
		$m_data['PIUTANG'] = 'SYNC PIUTANG MEMBER';
		$m_data['JENISKUSTOMER'] = 'SYNC JENIS KUSTOMER';
		$m_data['PRODUCT'] = 'SYNC PRODUCT & PRICE';
		// $m_data['HARGA'] = 'SYNC HARGA';
		$m_data['PLANOGRAM'] = 'SYNC PLANOGRAM';
		$m_data['RACK'] = 'SYNC RACK';
		$m_data['PENJUALAN'] = 'SYNC PENJUALAN';
		$m_data['STOCK'] = 'SYNC STOCK';
		$m_data['OPNAME'] = 'SYNC OPNAME';
		$m_data['PR'] = 'SYNC PR';
		$m_data['PO'] = 'SYNC PO';
		
		
		//$m_data['POS'] = 'SYNC POS';
		//$m_data['TAX'] = 'SYNC TAX';
		$this->view->m_data = $m_data;
	}	
}
