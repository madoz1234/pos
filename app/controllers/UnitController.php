<?php

use Phalcon\Mvc\Controller;

class UnitController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'Unit' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$unit = new Unit();
		$this->view->lists = $unit::getAll();
	}
	
	public function ajaxUnitAction()
	{
		$unit = new Unit();
		$condition = " WHERE \"unit\" = '".$_GET['id']."' LIMIT 1 ";
		$data = $unit::getFreeSQL($condition);

		return json_encode($data);
	}
	
	public function addAction()
	{
		$this->AuthorityAction();
	}
	
	public function editAction()
	{		
		$this->AuthorityAction();
	}
	
	public function viewAction()
	{		
		$this->AuthorityAction();
	}
	
	public function deleteAction()
	{		
		$this->AuthorityAction();
		
		$data = new Unit();
		$data->unit = $_REQUEST["Unit"];
		$success = $data::goDelete($data);
	
		$this->response->redirect(uri('Unit'));
	}
	
	public function insertAction()
	{		
		$data = new Unit();
		$data->unit = $this->request->getPost("unit");
        $data->name = $this->request->getPost("name");
        $data->decimals = $this->request->getPost("decimals");
		$success = $data::goInsert($data);

		if($success){
			$this->response->redirect(uri('Unit'));
		}else{
			$this->response->redirect(uri('Unit/Add'));
		}
	}
	
	public function updateAction()
	{		
		$data = new Unit();
		$data->unit = $this->request->getPost("unit");
        $data->name = $this->request->getPost("name");
        $data->decimals = $this->request->getPost("decimals");
		$success = $data::goUpdate($data);

		if($success){
			$this->response->redirect(uri('Unit'));
		}else{
			$this->response->redirect(uri('Unit/Edit?Unit='.$data->unit));
		}
	}
	
}
