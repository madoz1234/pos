<?php

use Phalcon\Mvc\Controller;

class TundaPenjualanController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'TundaPenjualan' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function AuthoritySoDAction()
	{		
		$SoD = new StartEndDay();
		$condition = " WHERE \"tanggal\" = '".date('Y-m-d')."' AND \"flag_start\" = true AND \"flag_end\" = false ";
		$lists_SoD = $SoD::getFreeSQL($condition);

		if(!isset($lists_SoD)){			
			$this->response->redirect(uri('Auth/SoD'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		$this->AuthoritySoDAction();
		
		$start_end_shift = new StartEndShift();
		$condition = " WHERE \"tanggal\" ='".date("Y-m-d")."' AND \"user_id\" = '".$this->session->get("user")['user_name']."' ";
		$condition .= " AND \"flag_start\" = true AND \"flag_end\" = false ";
		$lists_seShift = $start_end_shift::getFreeSQL($condition);
		
		$tunda = new Tunda();
		$condition = " WHERE \"flag_delete\" = false ";
		$this->view->lists = $tunda::getFreeSql($condition);
		
		if(count($lists_seShift) > 0){
			
		}else{
			$this->response->redirect(uri('Penjualan/Start'));
		}
	}
	
	public function deleteAction()
	{		
		$this->AuthorityAction();
		$this->AuthoritySoDAction();
		
		$tunda = new Tunda();
		$tunda->id_jual = $_REQUEST['id_jual'];
		$tunda->flag_delete = true;
		$success = $tunda::goUpdate($tunda);
		
		$this->response->redirect(uri('TundaPenjualan'));
	}
	
	public function ajaxTundaAction()
	{
		$tunda_detail = new TundaDetail();
		$condition = " WHERE \"id_jual\" = '".$_GET['hold']."' ";
		$data = $tunda_detail::getFreeSQL($condition);

		return json_encode($data);
	}
}
