<?php

use Phalcon\Mvc\Controller;

class ShiftController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'Shift' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$shift = new Shift();
		$this->view->lists = $shift::getAll();
	}
	
	public function ajaxShiftAction()
	{
		$shift = new Shift();
		$condition = " WHERE \"shift_id\" = '".$_GET['id']."' LIMIT 1 ";
		$data = $shift::getFreeSQL($condition);

		return json_encode($data);
	}
	
	public function addAction()
	{
		$this->AuthorityAction();
	}
	
	public function editAction()
	{
		$this->AuthorityAction();
		
		$shift = new Shift();
		$shift->shift_id = $_REQUEST['Shift_ID'];
		$lists = $shift::getFirst($shift);
		
		$this->view->data = new Shift();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->shift_id = $list['shift_id'];
				$this->view->data->description = $list['description'];				
				$this->view->data->inactive = $list['inactive'];
			}
		}
	}
	
	public function viewAction()
	{
		$this->AuthorityAction();
		
		$shift = new Shift();
		$shift->shift_id = $_REQUEST['Shift_ID'];
		$lists = $shift::getFirst($shift);
		
		$this->view->data = new Shift();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->shift_id = $list['shift_id'];
				$this->view->data->description = $list['description'];				
				$this->view->data->inactive = $list['inactive'];
			}
		}
	}
	
	public function deleteAction()
	{
		$this->AuthorityAction();
		
		$shift = new Shift();
		$shift->shift_id = $_REQUEST["Shift_ID"];

		$condd = " WHERE shift_id::varchar NOT IN (SELECT shift_id FROM t_start_end_shift WHERE tanggal = '".date('Y-m-d')."' )";
		$lists_shift = $shift::getFreeSQL($condd);
		foreach($lists_shift as $data_shift){
			if($data_shift['shift_id'] == $shift->shift_id){
				$count = 1;
			}else{
				$count = 0;
			}
			$akhir += $count;
		}

		if($akhir != 0){
			$success = $shift::goDelete($shift);
			if($success){
				$data['type'] = 'S';
				$data['message'] = '<i><b>Berhasil Hapus Shift</b></i>';			
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = '<i><b>Gagal Hapus Shift</b></i>';
				return json_encode($data);			
			}
		}else{
			$data['type'] = 'E';
			$data['message'] = '<i><b>Shift Sedang Digunakan</b></i>';
			return json_encode($data);		
		}
		
		$this->response->redirect(uri('Shift'));
	}
	
	public function insertAction()
	{
		$shift = new Shift();		
		$shift->description = $this->request->getPost("Description");
		$cond = " WHERE \"description\" = '".$this->request->getPost("Description")."' ";
		$cari = $shift::getFreeSQL($cond);
		if(count($cari) == 0){
			$success = $shift::goInsert($shift);
		}
		if($success){
			$data['type'] = 'S';
			$data['message'] = '<i><b>Berhasil Tambah Shift</b></i>';			
			return json_encode($data);
		}else{
			$data['type'] = 'E';
			$data['message'] = '<i><b>Nama Shift Tidak Boleh Sama</b></i>';
			return json_encode($data);			
		}
		// if($success){			
		// 	$this->response->redirect('Shift');
		// }else{						
		// 	$this->response->redirect('Shift/Add');
		// }
	}
	
	public function updateAction()
	{
		$shift = new Shift();
		$shift->shift_id = $this->request->getPost("Shift_ID");		
		$shift->description = $this->request->getPost("Description");

		$cond = " WHERE \"shift_id\" = '".$this->request->getPost("Shift_ID")."' ";
		$cond .= " AND \"description\" = '".$this->request->getPost("Description")."' ";
		$cari = $shift::getFreeSQL($cond);

		$condd = " WHERE shift_id::varchar NOT IN (SELECT shift_id FROM t_start_end_shift WHERE tanggal = '".date('Y-m-d')."' )";
		$lists_shift = $shift::getFreeSQL($condd);
		foreach($lists_shift as $data_shift){
			if($data_shift['shift_id'] == $this->request->getPost("Shift_ID")){
				$count = 1;
			}else{
				$count = 0;
			}
			$akhir += $count;
		}

		if($akhir != 0){
			if(count($cari) == 0){
				$condition = " WHERE \"shift_id\" != '".$this->request->getPost("Shift_ID")."' ";
				$condition .= " AND \"description\" = '".$this->request->getPost("Description")."' ";
				$cari2 = $shift::getFreeSQL($condition);
				if($cari2 == 0){
					$success = $shift::goUpdate($shift);	
				}
			}else{
				$success = $shift::goUpdate($shift);
			}
			
			if($success){
				$data['type'] = 'S';
				$data['message'] = '<i><b>Berhasil Ubah Shift</b></i>';			
				return json_encode($data);
			}else{
				$data['type'] = 'E';
				$data['message'] = '<i><b>Nama Shift Tidak Boleh Sama</b></i>';
				return json_encode($data);			
			}
		}else{
			$data['type'] = 'E';
			$data['message'] = '<i><b>Shift Sedang Digunakan</b></i>';
			return json_encode($data);		
		}
		
		if($success){			
			$this->response->redirect(uri('Shift'));
		}else{						
			$this->response->redirect(uri('Shift/Edit?Shift_ID='.$shift->shift_id));
		}
	}
}
