<?php

use Phalcon\Mvc\Controller;

class RoleMenuController extends Controller
{
	public function AuthorityAction()
	{
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'RoleMenu' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction()
	{
		$this->AuthorityAction();
		
		$role = new Role();
		$this->view->lists = $role::getAll();
	}
	
	public function editAction()
	{
		$this->AuthorityAction();
		
		$role = new Role();
		$role->role_id = $_REQUEST['Role_ID'];
		$lists = $role::getFirst($role);
		
		$this->view->data = new Role();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data->role_id = $list['role_id'];
				$this->view->data->role_name = $list['role_name'];
				$this->view->data->description = $list['description'];
				$this->view->data->is_active = $list['is_active'];
			}
		}
		
		$role_menu = new RoleMenu();
		$condition = " WHERE \"role_id\" = '".$_REQUEST['Role_ID']."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		$this->view->lists_role_menu = $lists_role_menu;
		
		$menu = new Menu();
		$lists_menu = $menu::getALL();
		
		$this->view->lists_menu = $lists_menu;
	}
	
	public function updateAction()
	{	$role_menu = new RoleMenu();
		$role_id = $this->session->get('user')['role_id'];
		if($role_id == 1){
			$json_datas = json_decode($_POST['json_data']);
			if(isset($json_datas)){
				$role_menu_del = new RoleMenu();
				$role_menu_del->role_id = $this->request->getPost("Role_ID");
				$success = $role_menu_del::goDeleteRole($role_menu_del);

				$query_insert = '';
				foreach($json_datas as $json_data){
					if($json_data[2] == 't'){
						$role_menu = new RoleMenu();
						$role_menu->role_id = $this->request->getPost("Role_ID");
						$role_menu->menu_id = $json_data[0];
						$sukses = $role_menu::goInsert($role_menu);
					}
				}

				if($sukses){
					$data['type'] = 'S';
					$data['message'] = '<i><b>Berhasil Update Data</b></i>';			
					return json_encode($data);
				}else{
					$data['type'] = 'E';
					$data['message'] = '<i><b>Gagal Update Data</b></i>';
					return json_encode($data);			
				}
			}

		}else{
			$data['type'] = 'E';
			$data['message'] = '<i><b>Gagal !! Anda Tidak Mempunyai Akses</b></i>';
			return json_encode($data);
		}
	}
}
