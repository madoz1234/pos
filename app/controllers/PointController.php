<?php

use Phalcon\Mvc\Controller;

class PointController extends Controller
{
	public function AuthorityAction(){
		if($this->session->get("is_login") == "X"){					
		}else{
			$this->response->redirect(uri('Login'));
		}
		
		$menu = new Menu();
		$condition = " WHERE \"view\" = 'Point' ";
		$lists_menu = $menu::getFreeSQL($condition);
		
		if(isset($lists_menu)){
			foreach($lists_menu as $list_menu){
				$menu->menu_id = $list_menu['menu_id'];
			}
		}
		
		$role_menu = new RoleMenu();
		$role_id = $this->session->get("user")['role_id'];
		$menu_id = $menu->menu_id;
		if($role_id == '') { $role_id = 0; } 
		if($menu_id == '') { $menu_id = 0; }
		$condition = " WHERE \"role_id\" = '".$role_id."' AND \"menu_id\" = '".$menu_id."' ";
		$lists_role_menu = $role_menu::getFreeSQL($condition);
		
		if(!isset($lists_role_menu)){			
			$this->response->redirect(uri('Auth'));
		}
	}
	
	public function indexAction(){
		$this->AuthorityAction();
		
		$point = new Point();
		$this->view->lists = $point::getAll();
	}
	
	public function addAction(){
		$this->AuthorityAction();
	}
	
	public function editAction(){
		$this->AuthorityAction();
		
		$point = new Point();
		$point->id = $_REQUEST['ID'];
		$lists = $point::getFirst($point);
		
		$this->view->data = new Shift();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data = $list;				
			}
		}
	}
	
	public function viewAction(){
		$this->AuthorityAction();
		
		$point = new Point();
		$point->id = $_REQUEST['ID'];
		$lists = $point::getFirst($point);
		
		$this->view->data = new Shift();
		if(count($lists) > 0){
			foreach($lists as $list){
				$this->view->data = $list;				
			}
		}
	}
	
	public function deleteAction(){
		$this->AuthorityAction();
		
		$point = new Point();
		$point->id = $_REQUEST["ID"];
		$success = $point::goDelete($point);
		
		$this->response->redirect(uri('Point'));
	}
	
	public function insertAction(){
		$point = new Point();		
		$point->point_get = $this->request->getPost("point_get");
		$point->point_use = $this->request->getPost("point_use");
		$point->start_date = $this->request->getPost("start_date");
		$point->end_date = $this->request->getPost("end_date");
		
		$success = $point::goInsert($point);
		
		if($success){			
			$result['type'] = 'S';
			$result['message'] = 'Success to insert point!';
			return json_encode($result);
		}else{						
			$result['type'] = 'E';
			$result['message'] = $message;
			return json_encode($result);
		}
	}
	
	public function updateAction(){
		$point = new Point();
		$point->id = $this->request->getPost("id");		
		$point->point_get = $this->request->getPost("point_get");
		$point->point_use = $this->request->getPost("point_use");
		$point->start_date = $this->request->getPost("start_date");
		$point->end_date = $this->request->getPost("end_date");
		
		$success = $point::goUpdate($point);
		
		if($success){			
			$result['type'] = 'S';
			$result['message'] = 'Success to update point!';
			return json_encode($result);
		}else{						
			$result['type'] = 'E';
			$result['message'] = $message;
			return json_encode($result);
		}
	}
	
}
