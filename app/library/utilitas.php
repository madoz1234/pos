<?php

if (!function_exists('is_connected')) {
    function is_connected()
    {
        $connected = ping("stackoverflow.com", 80, 1);
                                            //website, port  (try 80 or 443)
        if ($connected){
            $is_conn = true; //action when connected
        }else{
            $is_conn = false; //action in connection failure
        }
        return $is_conn;
    }

    function ping($host, $port, $timeout) {
        $a = gethostbyname('idontexist.tld');
        $b = gethostbyname($host);

        if ($a == $b)
            return false;

        $time = microtime(true);
        $fp = @fsockopen($host, $port, $errCode, $errStr, $timeout);
        $time = microtime(true) - $time;
        if ($fp) {
            fclose($fp);
            return $time;
        } else {
            return false;
        }
    }
}

if (!function_exists('url')) {
    function url($uri)
    {
        $config = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
        $config .= "://".$_SERVER['HTTP_HOST'];
        $config .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
        $config = str_replace("public/", "", $config);
        return $config . $uri;
        // return 'http://' . $_SERVER['HTTP_HOST'] . '/' . $uri;
    }
}

if (!function_exists('post')) {
    function post($uri)
    {
        $config = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
        $config .= "://".$_SERVER['HTTP_HOST'];
        $config .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
        $config = str_replace("public/", "", $config);
        return $config . $uri;
        // return 'http://' . $_SERVER['HTTP_HOST'] . '/' . $uri;
    }
}

if (!function_exists('dd')) {
    function dd($var)
    {
        print_r($var); exit;
    }
}