<?php

class CurlClass
{
    public $ch;

    public function get($url)
    {
        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 86400); // 1 day
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);

        // session
        // curl_setopt ($this->ch, CURLOPT_COOKIEJAR, COOKIE_FILE); 
        // curl_setopt ($this->ch, CURLOPT_COOKIEFILE, COOKIE_FILE); 

        $result = curl_exec($this->ch);

        return $result;
    }

    public function post($url, $datapost, $headers=[])
    {
        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 86400); // 1 day
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SAFE_UPLOAD, false);

        // header
        if ($headers) {
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);
        }

        // build array data pos
        $new_data;
        $this->build_query($datapost, $new_data);

        // post
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $new_data);

        // session
        // curl_setopt ($this->ch, CURLOPT_COOKIEJAR, COOKIE_FILE); 
        // curl_setopt ($this->ch, CURLOPT_COOKIEFILE, COOKIE_FILE); 

        $result = curl_exec($this->ch);

        return $result;
    }

    public function upload($url, $datapost, $headers=[])
    {
        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 86400); // 1 day
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);

        // header
        if ($headers) {
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);
        }

        // post
        // curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $datapost);

        // session
        // curl_setopt ($this->ch, CURLOPT_COOKIEJAR, COOKIE_FILE); 
        // curl_setopt ($this->ch, CURLOPT_COOKIEFILE, COOKIE_FILE); 

        $result = curl_exec($this->ch);

        return $result;
    }

    public function file($file, $name = null){
        if (function_exists('curl_file_create')) { // php 5.5+
          $cFile = curl_file_create($file);
        } else { // 
          $cFile = '@' . realpath($file) . ';filename=' . (is_null($name) ? $file : $name);
        }

        return $cFile;
    }

    function build_query( $arrays, &$new = array(), $prefix = null ) {

        if ( is_object( $arrays ) ) {
            $arrays = get_object_vars( $arrays );
        }

        foreach ( $arrays AS $key => $value ) {
            $k = isset( $prefix ) ? $prefix . '[' . $key . ']' : $key;
            if ( is_array( $value ) OR is_object( $value )  ) {
                $this->build_query( $value, $new, $k );
            } else {
                $new[$k] = $value;
            }
        }
    }

    public function exec($url, $datapost, $files, $headers=[])
    {
        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 86400); // 1 day
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SAFE_UPLOAD, false);

        // header
        if ($headers) {
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);
        }

        // build array data pos
        $boundary = uniqid();
        $delimiter = '-------------' . $boundary;

        $new_data = build_data_files($boundary, $datapost, $files);

        // post
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $new_data);

        $result = curl_exec($this->ch);

        return $result;
    }

    function build_data_files($boundary, $fields, $files){
        $data = '';
        $eol = "\r\n";

        $delimiter = '-------------' . $boundary;

        foreach ($fields as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $name . "\"".$eol.$eol
                . $content . $eol;
        }


        foreach ($files as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $name . '"; filename="' . $name . '"' . $eol
                //. 'Content-Type: image/png'.$eol
                . 'Content-Transfer-Encoding: binary'.$eol
                ;

            $data .= $eol;
            $data .= $content . $eol;
        }
        $data .= "--" . $delimiter . "--".$eol;


        return $data;
    }

}