<?php

class CurlClass
{
    public $ch;

    public function get($url)
    {
        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 86400); // 1 day
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);

        // session
        // curl_setopt ($this->ch, CURLOPT_COOKIEJAR, COOKIE_FILE); 
        // curl_setopt ($this->ch, CURLOPT_COOKIEFILE, COOKIE_FILE); 

        $result = curl_exec($this->ch);

        return $result;
    }

    public function post($url, $datapost, $headers=[])
    {
        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 86400); // 1 day
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);

        // header
        if ($headers) {
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);
        }

        // build array data pos
        $new_data;
        $this->build_query($datapost, $new_data);

        // post
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $new_data);

        // session
        // curl_setopt ($this->ch, CURLOPT_COOKIEJAR, COOKIE_FILE); 
        // curl_setopt ($this->ch, CURLOPT_COOKIEFILE, COOKIE_FILE); 

        $result = curl_exec($this->ch);

        return $result;
    }

    public function upload($url, $datapost, $headers=[])
    {
        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 86400); // 1 day
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);

        // header
        if ($headers) {
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);
        }

        // post
        // curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $datapost);

        // session
        // curl_setopt ($this->ch, CURLOPT_COOKIEJAR, COOKIE_FILE); 
        // curl_setopt ($this->ch, CURLOPT_COOKIEFILE, COOKIE_FILE); 

        $result = curl_exec($this->ch);

        return $result;
    }

    function build_query( $arrays, &$new = array(), $prefix = null ) {

        if ( is_object( $arrays ) ) {
            $arrays = get_object_vars( $arrays );
        }

        foreach ( $arrays AS $key => $value ) {
            $k = isset( $prefix ) ? $prefix . '[' . $key . ']' : $key;
            if ( is_array( $value ) OR is_object( $value )  ) {
                $this->build_query( $value, $new, $k );
            } else {
                $new[$k] = $value;
            }
        }
    }

}