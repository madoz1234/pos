<?php

	function print_center($text){
		$result = "";
		
		$ctr = (48 - strlen($text)) / 2;
		
		for($i=0;$i<$ctr;$i++){
			$result .= " "; 
		}
		
		$result .= $text;
		
		if(strlen($text) %2 != 0){
			for($i=1;$i<$ctr;$i++){
				$result .= " "; 
			}
		}else{
			for($i=0;$i<$ctr;$i++){
				$result .= " "; 
			}
		}
		
		return $result;
	}

	function print_right($text, $length){
		$result = "";
		$ctr = $length - strlen($text);
		
		for($i=0;$i<$ctr;$i++){
			$result .= " ";
		}
		
		if($ctr < 0){
			$result .= substr($text, 0, $length);
		}else{
			$result .= $text;
		}
		
		return $result;
	}

	function print_left($text, $length){
		$result = "";
		$ctr = $length - strlen($text);
		
		if($ctr < 0){
			$result .= substr($text, 0, $length);
		}else{
			$result .= $text;
		}
		
		for($i=0;$i<$ctr;$i++){
			$result .= " ";
		}
		
		return $result;
	}

	function print_space($length){
		$result = "";
		
		for($i=0;$i<$length;$i++){
			$result .= " ";
		}
		
		return $result;
	}
	
	function product_date(){
		return 25174065;
	}

?>